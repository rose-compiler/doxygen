/******************************************************************************
 *
 * 
 *
 * Copyright (C) 1997-2013 by Dimitri van Heesch.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation under the terms of the GNU General Public License is hereby 
 * granted. No representations are made about the suitability of this software 
 * for any purpose. It is provided "as is" without express or implied warranty.
 * See the GNU General Public License for more details.
 *
 * Documents produced by Doxygen are derivative works derived from the
 * input used in their production; they are not affected by this license.
 *
 */

#include <stdlib.h>

#include <qdir.h>
#include <qregexp.h>
#include "message.h"
#include "htmlgen.h"
#include "config.h"
#include "util.h"
#include "doxygen.h"
#include "logos.h"
#include "diagram.h"
#include "version.h"
#include "dot.h"
#include "language.h"
#include "htmlhelp.h"
#include "docparser.h"
#include "htmldocvisitor.h"
#include "searchindex.h"
#include "pagedef.h"
#include "debug.h"
#include "dirdef.h"
#include "vhdldocgen.h"
#include "layout.h"
#include "image.h"
#include "ftvhelp.h"
#include "bufstr.h"


//#define DBG_HTML(x) x;
#define DBG_HTML(x) 

static const char defaultHtmlHeader[] =
// #include "header_html.h"
"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
"<head>\n"
"<meta http-equiv=\"Content-Type\" content=\"text/xhtml;charset=UTF-8\"/>\n"
"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=9\"/>\n"
"<meta name=\"generator\" content=\"Doxygen $doxygenversion\"/>\n"
"<!--BEGIN PROJECT_NAME--><title>$projectname: $title</title><!--END PROJECT_NAME-->\n"
"<!--BEGIN !PROJECT_NAME--><title>$title</title><!--END !PROJECT_NAME-->\n"
"<link href=\"$relpath^tabs.css\" rel=\"stylesheet\" type=\"text/css\"/>\n"
"<script type=\"text/javascript\" src=\"$relpath^jquery.js\"></script>\n"
"<script type=\"text/javascript\" src=\"$relpath^dynsections.js\"></script>\n"
"$treeview\n"
"$search\n"
"$mathjax\n"
"<link href=\"$relpath^$stylesheet\" rel=\"stylesheet\" type=\"text/css\" />\n"
"$extrastylesheet\n"
"</head>\n"
"<body>\n"
"<div id=\"top\"><!-- do not remove this div, it is closed by doxygen! -->\n"
"\n"
"<!--BEGIN TITLEAREA-->\n"
"<div id=\"titlearea\">\n"
"<table cellspacing=\"0\" cellpadding=\"0\">\n"
" <tbody>\n"
" <tr style=\"height: 56px;\">\n"
"  <!--BEGIN PROJECT_LOGO-->\n"
"  <td id=\"projectlogo\"><img alt=\"Logo\" src=\"$relpath^$projectlogo\"/></td>\n"
"  <!--END PROJECT_LOGO-->\n"
"  <!--BEGIN PROJECT_NAME-->\n"
"  <td style=\"padding-left: 0.5em;\">\n"
"   <div id=\"projectname\">$projectname\n"
"   <!--BEGIN PROJECT_NUMBER-->&#160;<span id=\"projectnumber\">$projectnumber</span><!--END PROJECT_NUMBER-->\n"
"   </div>\n"
"   <!--BEGIN PROJECT_BRIEF--><div id=\"projectbrief\">$projectbrief</div><!--END PROJECT_BRIEF-->\n"
"  </td>\n"
"  <!--END PROJECT_NAME-->\n"
"  <!--BEGIN !PROJECT_NAME-->\n"
"   <!--BEGIN PROJECT_BRIEF-->\n"
"    <td style=\"padding-left: 0.5em;\">\n"
"    <div id=\"projectbrief\">$projectbrief</div>\n"
"    </td>\n"
"   <!--END PROJECT_BRIEF-->\n"
"  <!--END !PROJECT_NAME-->\n"
"  <!--BEGIN DISABLE_INDEX-->\n"
"   <!--BEGIN SEARCHENGINE-->\n"
"   <td>$searchbox</td>\n"
"   <!--END SEARCHENGINE-->\n"
"  <!--END DISABLE_INDEX-->\n"
" </tr>\n"
" </tbody>\n"
"</table>\n"
"</div>\n"
"<!--END TITLEAREA-->\n"
"<!-- end header part -->\n"
;

static const char defaultHtmlFooter[] =
// #include "footer_html.h"
"<!-- start footer part -->\n"
"<!--BEGIN GENERATE_TREEVIEW-->\n"
"<div id=\"nav-path\" class=\"navpath\"><!-- id is needed for treeview function! -->\n"
"  <ul>\n"
"    $navpath\n"
"    <li class=\"footer\">$generatedby\n"
"    <a href=\"http://www.doxygen.org/index.html\">\n"
"    <img class=\"footer\" src=\"$relpath^doxygen.png\" alt=\"doxygen\"/></a> $doxygenversion </li>\n"
"  </ul>\n"
"</div>\n"
"<!--END GENERATE_TREEVIEW-->\n"
"<!--BEGIN !GENERATE_TREEVIEW-->\n"
"<hr class=\"footer\"/><address class=\"footer\"><small>\n"
"$generatedby &#160;<a href=\"http://www.doxygen.org/index.html\">\n"
"<img class=\"footer\" src=\"$relpath^doxygen.png\" alt=\"doxygen\"/>\n"
"</a> $doxygenversion\n"
"</small></address>\n"
"<!--END !GENERATE_TREEVIEW-->\n"
"</body>\n"
"</html>\n"
;

static const char defaultStyleSheet[] = 
// #include "doxygen_css.h"
"/* The standard CSS for doxygen $doxygenversion */\n"
"\n"
"body, table, div, p, dl {\n"
"	font: 400 14px/19px Roboto,sans-serif;\n"
"}\n"
"\n"
"/* @group Heading Levels */\n"
"\n"
"h1.groupheader {\n"
"	font-size: 150%;\n"
"}\n"
"\n"
".title {\n"
"	font-size: 150%;\n"
"	font-weight: bold;\n"
"	margin: 10px 2px;\n"
"}\n"
"\n"
"h2.groupheader {\n"
"	border-bottom: 1px solid ##99;\n"
"	color: ##44;\n"
"	font-size: 150%;\n"
"	font-weight: normal;\n"
"	margin-top: 1.75em;\n"
"	padding-top: 8px;\n"
"	padding-bottom: 4px;\n"
"	width: 100%;\n"
"}\n"
"\n"
"h3.groupheader {\n"
"	font-size: 100%;\n"
"}\n"
"\n"
"h1, h2, h3, h4, h5, h6 {\n"
"	-webkit-transition: text-shadow 0.5s linear;\n"
"	-moz-transition: text-shadow 0.5s linear;\n"
"	-ms-transition: text-shadow 0.5s linear;\n"
"	-o-transition: text-shadow 0.5s linear;\n"
"	transition: text-shadow 0.5s linear;\n"
"	margin-right: 15px;\n"
"}\n"
"\n"
"h1.glow, h2.glow, h3.glow, h4.glow, h5.glow, h6.glow {\n"
"	text-shadow: 0 0 15px cyan;\n"
"}\n"
"\n"
"dt {\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"div.multicol {\n"
"	-moz-column-gap: 1em;\n"
"	-webkit-column-gap: 1em;\n"
"	-moz-column-count: 3;\n"
"	-webkit-column-count: 3;\n"
"}\n"
"\n"
"p.startli, p.startdd, p.starttd {\n"
"	margin-top: 2px;\n"
"}\n"
"\n"
"p.endli {\n"
"	margin-bottom: 0px;\n"
"}\n"
"\n"
"p.enddd {\n"
"	margin-bottom: 4px;\n"
"}\n"
"\n"
"p.endtd {\n"
"	margin-bottom: 2px;\n"
"}\n"
"\n"
"/* @end */\n"
"\n"
"caption {\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"span.legend {\n"
"        font-size: 70%;\n"
"        text-align: center;\n"
"}\n"
"\n"
"h3.version {\n"
"        font-size: 90%;\n"
"        text-align: center;\n"
"}\n"
"\n"
"div.qindex, div.navtab{\n"
"	background-color: ##ee;\n"
"	border: 1px solid ##b0;\n"
"	text-align: center;\n"
"}\n"
"\n"
"div.qindex, div.navpath {\n"
"	width: 100%;\n"
"	line-height: 140%;\n"
"}\n"
"\n"
"div.navtab {\n"
"	margin-right: 15px;\n"
"}\n"
"\n"
"/* @group Link Styling */\n"
"\n"
"a {\n"
"	color: ##50;\n"
"	font-weight: normal;\n"
"	text-decoration: none;\n"
"}\n"
"\n"
".contents a:visited {\n"
"	color: ##60;\n"
"}\n"
"\n"
"a:hover {\n"
"	text-decoration: underline;\n"
"}\n"
"\n"
"a.qindex {\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"a.qindexHL {\n"
"	font-weight: bold;\n"
"	background-color: ##AA;\n"
"	color: #ffffff;\n"
"	border: 1px double ##98;\n"
"}\n"
"\n"
".contents a.qindexHL:visited {\n"
"        color: #ffffff;\n"
"}\n"
"\n"
"a.el {\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"a.elRef {\n"
"}\n"
"\n"
"a.code, a.code:visited {\n"
"	color: #4665A2; \n"
"}\n"
"\n"
"a.codeRef, a.codeRef:visited {\n"
"	color: #4665A2; \n"
"}\n"
"\n"
"/* @end */\n"
"\n"
"dl.el {\n"
"	margin-left: -1cm;\n"
"}\n"
"\n"
"pre.fragment {\n"
"        border: 1px solid #C4CFE5;\n"
"        background-color: #FBFCFD;\n"
"        padding: 4px 6px;\n"
"        margin: 4px 8px 4px 2px;\n"
"        overflow: auto;\n"
"        word-wrap: break-word;\n"
"        font-size:  9pt;\n"
"        line-height: 125%;\n"
"        font-family: monospace, fixed;\n"
"        font-size: 105%;\n"
"}\n"
"\n"
"div.fragment {\n"
"        padding: 4px;\n"
"        margin: 4px;\n"
"	background-color: ##FC;\n"
"	border: 1px solid ##CC;\n"
"}\n"
"\n"
"div.line {\n"
"	font-family: monospace, fixed;\n"
"        font-size: 13px;\n"
"	min-height: 13px;\n"
"	line-height: 1.0;\n"
"	text-wrap: unrestricted;\n"
"	white-space: -moz-pre-wrap; /* Moz */\n"
"	white-space: -pre-wrap;     /* Opera 4-6 */\n"
"	white-space: -o-pre-wrap;   /* Opera 7 */\n"
"	white-space: pre-wrap;      /* CSS3  */\n"
"	word-wrap: break-word;      /* IE 5.5+ */\n"
"	text-indent: -53px;\n"
"	padding-left: 53px;\n"
"	padding-bottom: 0px;\n"
"	margin: 0px;\n"
"	-webkit-transition-property: background-color, box-shadow;\n"
"	-webkit-transition-duration: 0.5s;\n"
"	-moz-transition-property: background-color, box-shadow;\n"
"	-moz-transition-duration: 0.5s;\n"
"	-ms-transition-property: background-color, box-shadow;\n"
"	-ms-transition-duration: 0.5s;\n"
"	-o-transition-property: background-color, box-shadow;\n"
"	-o-transition-duration: 0.5s;\n"
"	transition-property: background-color, box-shadow;\n"
"	transition-duration: 0.5s;\n"
"}\n"
"\n"
"div.line.glow {\n"
"	background-color: cyan;\n"
"	box-shadow: 0 0 10px cyan;\n"
"}\n"
"\n"
"\n"
"span.lineno {\n"
"	padding-right: 4px;\n"
"	text-align: right;\n"
"	border-right: 2px solid #0F0;\n"
"	background-color: #E8E8E8;\n"
"        white-space: pre;\n"
"}\n"
"span.lineno a {\n"
"	background-color: #D8D8D8;\n"
"}\n"
"\n"
"span.lineno a:hover {\n"
"	background-color: #C8C8C8;\n"
"}\n"
"\n"
"div.ah {\n"
"	background-color: black;\n"
"	font-weight: bold;\n"
"	color: #ffffff;\n"
"	margin-bottom: 3px;\n"
"	margin-top: 3px;\n"
"	padding: 0.2em;\n"
"	border: solid thin #333;\n"
"	border-radius: 0.5em;\n"
"	-webkit-border-radius: .5em;\n"
"	-moz-border-radius: .5em;\n"
"	box-shadow: 2px 2px 3px #999;\n"
"	-webkit-box-shadow: 2px 2px 3px #999;\n"
"	-moz-box-shadow: rgba(0, 0, 0, 0.15) 2px 2px 2px;\n"
"	background-image: -webkit-gradient(linear, left top, left bottom, from(#eee), to(#000),color-stop(0.3, #444));\n"
"	background-image: -moz-linear-gradient(center top, #eee 0%, #444 40%, #000);\n"
"}\n"
"\n"
"div.groupHeader {\n"
"	margin-left: 16px;\n"
"	margin-top: 12px;\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"div.groupText {\n"
"	margin-left: 16px;\n"
"	font-style: italic;\n"
"}\n"
"\n"
"body {\n"
"	background-color: white;\n"
"	color: black;\n"
"        margin: 0;\n"
"}\n"
"\n"
"div.contents {\n"
"	margin-top: 10px;\n"
"	margin-left: 12px;\n"
"	margin-right: 8px;\n"
"}\n"
"\n"
"td.indexkey {\n"
"	background-color: ##ee;\n"
"	font-weight: bold;\n"
"	border: 1px solid ##cc;\n"
"	margin: 2px 0px 2px 0;\n"
"	padding: 2px 10px;\n"
"        white-space: nowrap;\n"
"        vertical-align: top;\n"
"}\n"
"\n"
"td.indexvalue {\n"
"	background-color: ##ee;\n"
"	border: 1px solid ##cc;\n"
"	padding: 2px 10px;\n"
"	margin: 2px 0px;\n"
"}\n"
"\n"
"tr.memlist {\n"
"	background-color: ##f0;\n"
"}\n"
"\n"
"p.formulaDsp {\n"
"	text-align: center;\n"
"}\n"
"\n"
"img.formulaDsp {\n"
"	\n"
"}\n"
"\n"
"img.formulaInl {\n"
"	vertical-align: middle;\n"
"}\n"
"\n"
"div.center {\n"
"	text-align: center;\n"
"        margin-top: 0px;\n"
"        margin-bottom: 0px;\n"
"        padding: 0px;\n"
"}\n"
"\n"
"div.center img {\n"
"	border: 0px;\n"
"}\n"
"\n"
"address.footer {\n"
"	text-align: right;\n"
"	padding-right: 12px;\n"
"}\n"
"\n"
"img.footer {\n"
"	border: 0px;\n"
"	vertical-align: middle;\n"
"}\n"
"\n"
"/* @group Code Colorization */\n"
"\n"
"span.keyword {\n"
"	color: #008000\n"
"}\n"
"\n"
"span.keywordtype {\n"
"	color: #604020\n"
"}\n"
"\n"
"span.keywordflow {\n"
"	color: #e08000\n"
"}\n"
"\n"
"span.comment {\n"
"	color: #800000\n"
"}\n"
"\n"
"span.preprocessor {\n"
"	color: #806020\n"
"}\n"
"\n"
"span.stringliteral {\n"
"	color: #002080\n"
"}\n"
"\n"
"span.charliteral {\n"
"	color: #008080\n"
"}\n"
"\n"
"span.vhdldigit { \n"
"	color: #ff00ff \n"
"}\n"
"\n"
"span.vhdlchar { \n"
"	color: #000000 \n"
"}\n"
"\n"
"span.vhdlkeyword { \n"
"	color: #700070 \n"
"}\n"
"\n"
"span.vhdllogic { \n"
"	color: #ff0000 \n"
"}\n"
"\n"
"blockquote {\n"
"        background-color: ##F8;\n"
"        border-left: 2px solid ##AA;\n"
"        margin: 0 24px 0 4px;\n"
"        padding: 0 12px 0 16px;\n"
"}\n"
"\n"
"/* @end */\n"
"\n"
"/*\n"
".search {\n"
"	color: #003399;\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"form.search {\n"
"	margin-bottom: 0px;\n"
"	margin-top: 0px;\n"
"}\n"
"\n"
"input.search {\n"
"	font-size: 75%;\n"
"	color: #000080;\n"
"	font-weight: normal;\n"
"	background-color: #e8eef2;\n"
"}\n"
"*/\n"
"\n"
"td.tiny {\n"
"	font-size: 75%;\n"
"}\n"
"\n"
".dirtab {\n"
"	padding: 4px;\n"
"	border-collapse: collapse;\n"
"	border: 1px solid ##b0;\n"
"}\n"
"\n"
"th.dirtab {\n"
"	background: ##ee;\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"hr {\n"
"	height: 0px;\n"
"	border: none;\n"
"	border-top: 1px solid ##66;\n"
"}\n"
"\n"
"hr.footer {\n"
"	height: 1px;\n"
"}\n"
"\n"
"/* @group Member Descriptions */\n"
"\n"
"table.memberdecls {\n"
"	border-spacing: 0px;\n"
"	padding: 0px;\n"
"}\n"
"\n"
".memberdecls td, .fieldtable tr {\n"
"	-webkit-transition-property: background-color, box-shadow;\n"
"	-webkit-transition-duration: 0.5s;\n"
"	-moz-transition-property: background-color, box-shadow;\n"
"	-moz-transition-duration: 0.5s;\n"
"	-ms-transition-property: background-color, box-shadow;\n"
"	-ms-transition-duration: 0.5s;\n"
"	-o-transition-property: background-color, box-shadow;\n"
"	-o-transition-duration: 0.5s;\n"
"	transition-property: background-color, box-shadow;\n"
"	transition-duration: 0.5s;\n"
"}\n"
"\n"
".memberdecls td.glow, .fieldtable tr.glow {\n"
"	background-color: cyan;\n"
"	box-shadow: 0 0 15px cyan;\n"
"}\n"
"\n"
".mdescLeft, .mdescRight,\n"
".memItemLeft, .memItemRight,\n"
".memTemplItemLeft, .memTemplItemRight, .memTemplParams {\n"
"	background-color: ##FA;\n"
"	border: none;\n"
"	margin: 4px;\n"
"	padding: 1px 0 0 8px;\n"
"}\n"
"\n"
".mdescLeft, .mdescRight {\n"
"	padding: 0px 8px 4px 8px;\n"
"	color: #555;\n"
"}\n"
"\n"
".memSeparator {\n"
"        border-bottom: 1px solid #DEE4F0;\n"
"        line-height: 1px;\n"
"        margin: 0px;\n"
"        padding: 0px;\n"
"}\n"
"\n"
".memItemLeft, .memTemplItemLeft {\n"
"        white-space: nowrap;\n"
"}\n"
"\n"
".memItemRight {\n"
"	width: 100%;\n"
"}\n"
"\n"
".memTemplParams {\n"
"	color: ##60;\n"
"        white-space: nowrap;\n"
"	font-size: 80%;\n"
"}\n"
"\n"
"/* @end */\n"
"\n"
"/* @group Member Details */\n"
"\n"
"/* Styles for detailed member documentation */\n"
"\n"
".memtemplate {\n"
"	font-size: 80%;\n"
"	color: ##60;\n"
"	font-weight: normal;\n"
"	margin-left: 9px;\n"
"}\n"
"\n"
".memnav {\n"
"	background-color: ##ee;\n"
"	border: 1px solid ##b0;\n"
"	text-align: center;\n"
"	margin: 2px;\n"
"	margin-right: 15px;\n"
"	padding: 2px;\n"
"}\n"
"\n"
".mempage {\n"
"	width: 100%;\n"
"}\n"
"\n"
".memitem {\n"
"	padding: 0;\n"
"	margin-bottom: 10px;\n"
"	margin-right: 5px;\n"
"        -webkit-transition: box-shadow 0.5s linear;\n"
"        -moz-transition: box-shadow 0.5s linear;\n"
"        -ms-transition: box-shadow 0.5s linear;\n"
"        -o-transition: box-shadow 0.5s linear;\n"
"        transition: box-shadow 0.5s linear;\n"
"        display: table !important;\n"
"        width: 100%;\n"
"}\n"
"\n"
".memitem.glow {\n"
"         box-shadow: 0 0 15px cyan;\n"
"}\n"
"\n"
".memname {\n"
"        font-weight: bold;\n"
"        margin-left: 6px;\n"
"}\n"
"\n"
".memname td {\n"
"	vertical-align: bottom;\n"
"}\n"
"\n"
".memproto, dl.reflist dt {\n"
"        border-top: 1px solid ##B4;\n"
"        border-left: 1px solid ##B4;\n"
"        border-right: 1px solid ##B4;\n"
"        padding: 6px 0px 6px 0px;\n"
"        color: ##2b;\n"
"        font-weight: bold;\n"
"        text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.9);\n"
"        background-image:url('nav_f.png');\n"
"        background-repeat:repeat-x;\n"
"        background-color: ##E6;\n"
"        /* opera specific markup */\n"
"        box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);\n"
"        border-top-right-radius: 4px;\n"
"        border-top-left-radius: 4px;\n"
"        /* firefox specific markup */\n"
"        -moz-box-shadow: rgba(0, 0, 0, 0.15) 5px 5px 5px;\n"
"        -moz-border-radius-topright: 4px;\n"
"        -moz-border-radius-topleft: 4px;\n"
"        /* webkit specific markup */\n"
"        -webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);\n"
"        -webkit-border-top-right-radius: 4px;\n"
"        -webkit-border-top-left-radius: 4px;\n"
"\n"
"}\n"
"\n"
".memdoc, dl.reflist dd {\n"
"        border-bottom: 1px solid ##B4;      \n"
"        border-left: 1px solid ##B4;      \n"
"        border-right: 1px solid ##B4; \n"
"        padding: 6px 10px 2px 10px;\n"
"        background-color: ##FC;\n"
"        border-top-width: 0;\n"
"        background-image:url('nav_g.png');\n"
"        background-repeat:repeat-x;\n"
"        background-color: #FFFFFF;\n"
"        /* opera specific markup */\n"
"        border-bottom-left-radius: 4px;\n"
"        border-bottom-right-radius: 4px;\n"
"        box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);\n"
"        /* firefox specific markup */\n"
"        -moz-border-radius-bottomleft: 4px;\n"
"        -moz-border-radius-bottomright: 4px;\n"
"        -moz-box-shadow: rgba(0, 0, 0, 0.15) 5px 5px 5px;\n"
"        /* webkit specific markup */\n"
"        -webkit-border-bottom-left-radius: 4px;\n"
"        -webkit-border-bottom-right-radius: 4px;\n"
"        -webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);\n"
"}\n"
"\n"
"dl.reflist dt {\n"
"        padding: 5px;\n"
"}\n"
"\n"
"dl.reflist dd {\n"
"        margin: 0px 0px 10px 0px;\n"
"        padding: 5px;\n"
"}\n"
"\n"
".paramkey {\n"
"	text-align: right;\n"
"}\n"
"\n"
".paramtype {\n"
"	white-space: nowrap;\n"
"}\n"
"\n"
".paramname {\n"
"	color: #602020;\n"
"	white-space: nowrap;\n"
"}\n"
".paramname em {\n"
"	font-style: normal;\n"
"}\n"
".paramname code {\n"
"        line-height: 14px;\n"
"}\n"
"\n"
".params, .retval, .exception, .tparams {\n"
"        margin-left: 0px;\n"
"        padding-left: 0px;\n"
"}       \n"
"\n"
".params .paramname, .retval .paramname {\n"
"        font-weight: bold;\n"
"        vertical-align: top;\n"
"}\n"
"        \n"
".params .paramtype {\n"
"        font-style: italic;\n"
"        vertical-align: top;\n"
"}       \n"
"        \n"
".params .paramdir {\n"
"        font-family: \"courier new\",courier,monospace;\n"
"        vertical-align: top;\n"
"}\n"
"\n"
"table.mlabels {\n"
"	border-spacing: 0px;\n"
"}\n"
"\n"
"td.mlabels-left {\n"
"	width: 100%;\n"
"	padding: 0px;\n"
"}\n"
"\n"
"td.mlabels-right {\n"
"	vertical-align: bottom;\n"
"	padding: 0px;\n"
"	white-space: nowrap;\n"
"}\n"
"\n"
"span.mlabels {\n"
"        margin-left: 8px;\n"
"}\n"
"\n"
"span.mlabel {\n"
"        background-color: ##88;\n"
"        border-top:1px solid ##70;\n"
"        border-left:1px solid ##70;\n"
"        border-right:1px solid ##CC;\n"
"        border-bottom:1px solid ##CC;\n"
"	text-shadow: none;\n"
"	color: white;\n"
"	margin-right: 4px;\n"
"	padding: 2px 3px;\n"
"	border-radius: 3px;\n"
"	font-size: 7pt;\n"
"	white-space: nowrap;\n"
"	vertical-align: middle;\n"
"}\n"
"\n"
"\n"
"\n"
"/* @end */\n"
"\n"
"/* these are for tree view when not used as main index */\n"
"\n"
"div.directory {\n"
"        margin: 10px 0px;\n"
"        border-top: 1px solid #A8B8D9;\n"
"        border-bottom: 1px solid #A8B8D9;\n"
"        width: 100%;\n"
"}\n"
"\n"
".directory table {\n"
"        border-collapse:collapse;\n"
"}\n"
"\n"
".directory td {\n"
"        margin: 0px;\n"
"        padding: 0px;\n"
"	vertical-align: top;\n"
"}\n"
"\n"
".directory td.entry {\n"
"        white-space: nowrap;\n"
"        padding-right: 6px;\n"
"}\n"
"\n"
".directory td.entry a {\n"
"        outline:none;\n"
"}\n"
"\n"
".directory td.entry a img {\n"
"        border: none;\n"
"}\n"
"\n"
".directory td.desc {\n"
"        width: 100%;\n"
"        padding-left: 6px;\n"
"	padding-right: 6px;\n"
"	padding-top: 3px;\n"
"	border-left: 1px solid rgba(0,0,0,0.05);\n"
"}\n"
"\n"
".directory tr.even {\n"
"	padding-left: 6px;\n"
"	background-color: ##F8;\n"
"}\n"
"\n"
".directory img {\n"
"	vertical-align: -30%;\n"
"}\n"
"\n"
".directory .levels {\n"
"        white-space: nowrap;\n"
"        width: 100%;\n"
"        text-align: right;\n"
"        font-size: 9pt;\n"
"}\n"
"\n"
".directory .levels span {\n"
"        cursor: pointer;\n"
"        padding-left: 2px;\n"
"        padding-right: 2px;\n"
"	color: ##50;\n"
"}\n"
"\n"
"div.dynheader {\n"
"        margin-top: 8px;\n"
"	-webkit-touch-callout: none;\n"
"	-webkit-user-select: none;\n"
"	-khtml-user-select: none;\n"
"	-moz-user-select: none;\n"
"	-ms-user-select: none;\n"
"	user-select: none;\n"
"}\n"
"\n"
"address {\n"
"	font-style: normal;\n"
"	color: ##33;\n"
"}\n"
"\n"
"table.doxtable {\n"
"	border-collapse:collapse;\n"
"        margin-top: 4px;\n"
"        margin-bottom: 4px;\n"
"}\n"
"\n"
"table.doxtable td, table.doxtable th {\n"
"	border: 1px solid ##37;\n"
"	padding: 3px 7px 2px;\n"
"}\n"
"\n"
"table.doxtable th {\n"
"	background-color: ##47;\n"
"	color: #FFFFFF;\n"
"	font-size: 110%;\n"
"	padding-bottom: 4px;\n"
"	padding-top: 5px;\n"
"}\n"
"\n"
"table.fieldtable {\n"
"        /*width: 100%;*/\n"
"        margin-bottom: 10px;\n"
"        border: 1px solid ##B4;\n"
"        border-spacing: 0px;\n"
"        -moz-border-radius: 4px;\n"
"        -webkit-border-radius: 4px;\n"
"        border-radius: 4px;\n"
"        -moz-box-shadow: rgba(0, 0, 0, 0.15) 2px 2px 2px;\n"
"        -webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.15);\n"
"        box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.15);\n"
"}\n"
"\n"
".fieldtable td, .fieldtable th {\n"
"        padding: 3px 7px 2px;\n"
"}\n"
"\n"
".fieldtable td.fieldtype, .fieldtable td.fieldname {\n"
"        white-space: nowrap;\n"
"        border-right: 1px solid ##B4;\n"
"        border-bottom: 1px solid ##B4;\n"
"        vertical-align: top;\n"
"}\n"
"\n"
".fieldtable td.fieldname {\n"
"        padding-top: 3px;\n"
"}\n"
"\n"
".fieldtable td.fielddoc {\n"
"        border-bottom: 1px solid ##B4;\n"
"        /*width: 100%;*/\n"
"}\n"
"\n"
".fieldtable td.fielddoc p:first-child {\n"
"        margin-top: 0px;\n"
"}       \n"
"        \n"
".fieldtable td.fielddoc p:last-child {\n"
"        margin-bottom: 2px;\n"
"}\n"
"\n"
".fieldtable tr:last-child td {\n"
"        border-bottom: none;\n"
"}\n"
"\n"
".fieldtable th {\n"
"        background-image:url('nav_f.png');\n"
"        background-repeat:repeat-x;\n"
"        background-color: ##E6;\n"
"        font-size: 90%;\n"
"        color: ##2B;\n"
"        padding-bottom: 4px;\n"
"        padding-top: 5px;\n"
"        text-align:left;\n"
"        -moz-border-radius-topleft: 4px;\n"
"        -moz-border-radius-topright: 4px;\n"
"        -webkit-border-top-left-radius: 4px;\n"
"        -webkit-border-top-right-radius: 4px;\n"
"        border-top-left-radius: 4px;\n"
"        border-top-right-radius: 4px;\n"
"        border-bottom: 1px solid ##B4;\n"
"}\n"
"\n"
"\n"
".tabsearch {\n"
"	top: 0px;\n"
"	left: 10px;\n"
"	height: 36px;\n"
"	background-image: url('tab_b.png');\n"
"	z-index: 101;\n"
"	overflow: hidden;\n"
"	font-size: 13px;\n"
"}\n"
"\n"
".navpath ul\n"
"{\n"
"	font-size: 11px;\n"
"	background-image:url('tab_b.png');\n"
"	background-repeat:repeat-x;\n"
"	background-position: 0 -5px;\n"
"	height:30px;\n"
"	line-height:30px;\n"
"	color:##9b;\n"
"	border:solid 1px ##ca;\n"
"	overflow:hidden;\n"
"	margin:0px;\n"
"	padding:0px;\n"
"}\n"
"\n"
".navpath li\n"
"{\n"
"	list-style-type:none;\n"
"	float:left;\n"
"	padding-left:10px;\n"
"	padding-right:15px;\n"
"	background-image:url('bc_s.png');\n"
"	background-repeat:no-repeat;\n"
"	background-position:right;\n"
"	color:##45;\n"
"}\n"
"\n"
".navpath li.navelem a\n"
"{\n"
"	height:32px;\n"
"	display:block;\n"
"	text-decoration: none;\n"
"	outline: none;\n"
"	color: ##30;\n"
"	font-family: 'Lucida Grande',Geneva,Helvetica,Arial,sans-serif;\n"
"	text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.9);\n"
"	text-decoration: none;        \n"
"}\n"
"\n"
".navpath li.navelem a:hover\n"
"{\n"
"	color:##80;\n"
"}\n"
"\n"
".navpath li.footer\n"
"{\n"
"        list-style-type:none;\n"
"        float:right;\n"
"        padding-left:10px;\n"
"        padding-right:15px;\n"
"        background-image:none;\n"
"        background-repeat:no-repeat;\n"
"        background-position:right;\n"
"        color:##45;\n"
"        font-size: 8pt;\n"
"}\n"
"\n"
"\n"
"div.summary\n"
"{\n"
"	float: right;\n"
"	font-size: 8pt;\n"
"	padding-right: 5px;\n"
"	width: 50%;\n"
"	text-align: right;\n"
"}       \n"
"\n"
"div.summary a\n"
"{\n"
"	white-space: nowrap;\n"
"}\n"
"\n"
"div.ingroups\n"
"{\n"
"	font-size: 8pt;\n"
"	width: 50%;\n"
"	text-align: left;\n"
"}\n"
"\n"
"div.ingroups a\n"
"{\n"
"	white-space: nowrap;\n"
"}\n"
"\n"
"div.header\n"
"{\n"
"        background-image:url('nav_h.png');\n"
"        background-repeat:repeat-x;\n"
"	background-color: ##FA;\n"
"	margin:  0px;\n"
"	border-bottom: 1px solid ##CC;\n"
"}\n"
"\n"
"div.headertitle\n"
"{\n"
"	padding: 5px 5px 5px 10px;\n"
"}\n"
"\n"
"dl\n"
"{\n"
"        padding: 0 0 0 10px;\n"
"}\n"
"\n"
"/* dl.note, dl.warning, dl.attention, dl.pre, dl.post, dl.invariant, dl.deprecated, dl.todo, dl.test, dl.bug */\n"
"dl.section\n"
"{\n"
"	margin-left: 0px;\n"
"	padding-left: 0px;\n"
"}\n"
"\n"
"dl.note\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #D0C000;\n"
"}\n"
"\n"
"dl.warning, dl.attention\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #FF0000;\n"
"}\n"
"\n"
"dl.pre, dl.post, dl.invariant\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #00D000;\n"
"}\n"
"\n"
"dl.deprecated\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #505050;\n"
"}\n"
"\n"
"dl.todo\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #00C0E0;\n"
"}\n"
"\n"
"dl.test\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #3030E0;\n"
"}\n"
"\n"
"dl.bug\n"
"{\n"
"        margin-left:-7px;\n"
"        padding-left: 3px;\n"
"        border-left:4px solid;\n"
"        border-color: #C08050;\n"
"}\n"
"\n"
"dl.section dd {\n"
"	margin-bottom: 6px;\n"
"}\n"
"\n"
"\n"
"#projectlogo\n"
"{\n"
"	text-align: center;\n"
"	vertical-align: bottom;\n"
"	border-collapse: separate;\n"
"}\n"
" \n"
"#projectlogo img\n"
"{ \n"
"	border: 0px none;\n"
"}\n"
" \n"
"#projectname\n"
"{\n"
"	font: 300% Tahoma, Arial,sans-serif;\n"
"	margin: 0px;\n"
"	padding: 2px 0px;\n"
"}\n"
"    \n"
"#projectbrief\n"
"{\n"
"	font: 120% Tahoma, Arial,sans-serif;\n"
"	margin: 0px;\n"
"	padding: 0px;\n"
"}\n"
"\n"
"#projectnumber\n"
"{\n"
"	font: 50% Tahoma, Arial,sans-serif;\n"
"	margin: 0px;\n"
"	padding: 0px;\n"
"}\n"
"\n"
"#titlearea\n"
"{\n"
"	padding: 0px;\n"
"	margin: 0px;\n"
"	width: 100%;\n"
"	border-bottom: 1px solid ##70;\n"
"}\n"
"\n"
".image\n"
"{\n"
"        text-align: center;\n"
"}\n"
"\n"
".dotgraph\n"
"{\n"
"        text-align: center;\n"
"}\n"
"\n"
".mscgraph\n"
"{\n"
"        text-align: center;\n"
"}\n"
"\n"
".caption\n"
"{\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"div.zoom\n"
"{\n"
"	border: 1px solid ##A0;\n"
"}\n"
"\n"
"dl.citelist {\n"
"        margin-bottom:50px;\n"
"}\n"
"\n"
"dl.citelist dt {\n"
"        color:##40;\n"
"        float:left;\n"
"        font-weight:bold;\n"
"        margin-right:10px;\n"
"        padding:5px;\n"
"}\n"
"\n"
"dl.citelist dd {\n"
"        margin:2px 0;\n"
"        padding:5px 0;\n"
"}\n"
"\n"
"div.toc {\n"
"        padding: 14px 25px;\n"
"        background-color: ##F6;\n"
"        border: 1px solid ##DD;\n"
"        border-radius: 7px 7px 7px 7px;\n"
"        float: right;\n"
"        height: auto;\n"
"        margin: 0 20px 10px 10px;\n"
"        width: 200px;\n"
"}\n"
"\n"
"div.toc li {\n"
"        background: url(\"bdwn.png\") no-repeat scroll 0 5px transparent;\n"
"        font: 10px/1.2 Verdana,DejaVu Sans,Geneva,sans-serif;\n"
"        margin-top: 5px;\n"
"        padding-left: 10px;\n"
"        padding-top: 2px;\n"
"}\n"
"\n"
"div.toc h3 {\n"
"        font: bold 12px/1.2 Arial,FreeSans,sans-serif;\n"
"	color: ##60;\n"
"        border-bottom: 0 none;\n"
"        margin: 0;\n"
"}\n"
"\n"
"div.toc ul {\n"
"        list-style: none outside none;\n"
"        border: medium none;\n"
"        padding: 0px;\n"
"}       \n"
"\n"
"div.toc li.level1 {\n"
"        margin-left: 0px;\n"
"}\n"
"\n"
"div.toc li.level2 {\n"
"        margin-left: 15px;\n"
"}\n"
"\n"
"div.toc li.level3 {\n"
"        margin-left: 30px;\n"
"}\n"
"\n"
"div.toc li.level4 {\n"
"        margin-left: 45px;\n"
"}\n"
"\n"
".inherit_header {\n"
"        font-weight: bold;\n"
"        color: gray;\n"
"        cursor: pointer;\n"
"	-webkit-touch-callout: none;\n"
"	-webkit-user-select: none;\n"
"	-khtml-user-select: none;\n"
"	-moz-user-select: none;\n"
"	-ms-user-select: none;\n"
"	user-select: none;\n"
"}\n"
"\n"
".inherit_header td {\n"
"        padding: 6px 0px 2px 5px;\n"
"}\n"
"\n"
".inherit {\n"
"        display: none;\n"
"}\n"
"\n"
"tr.heading h2 {\n"
"        margin-top: 12px;\n"
"        margin-bottom: 4px;\n"
"}\n"
"\n"
"@media print\n"
"{\n"
"  #top { display: none; }\n"
"  #side-nav { display: none; }\n"
"  #nav-path { display: none; }\n"
"  body { overflow:visible; }\n"
"  h1, h2, h3, h4, h5, h6 { page-break-after: avoid; }\n"
"  .summary { display: none; }\n"
"  .memitem { page-break-inside: avoid; }\n"
"  #doc-content\n"
"  {\n"
"    margin-left:0 !important;\n"
"    height:auto !important;\n"
"    width:auto !important;\n"
"    overflow:inherit;\n"
"    display:inline;\n"
"  }\n"
"}\n"
"\n"
;

static const char search_functions_script[]=
// #include "search_functions_php.h"
"<script language=\"PHP\">\n"
"require_once \"search-config.php\";\n"
"\n"
"function end_form($value)\n"
"{\n"
"  global $config;\n"
"  global $translator;\n"
"  if ($config['DISABLE_INDEX'] == false)\n"
"  {\n"
"  echo \"            <input type=\\\"text\\\" id=\\\"MSearchField\\\" name=\\\"query\\\" value=\\\"$value\\\" size=\\\"20\\\" accesskey=\\\"S\\\" onfocus=\\\"searchBox.OnSearchFieldFocus(true)\\\" onblur=\\\"searchBox.OnSearchFieldFocus(false)\\\"/>\\n            </form>\\n          </div><div class=\\\"right\\\"></div>\\n        </div>\\n      </li>\\n    </ul>\\n  </div>\\n</div>\\n\";\n"
"  }\n"
"  if ($config['GENERATE_TREEVIEW'])\n"
"  {\n"
"    echo $translator['split_bar'];\n"
"  }\n"
"}\n"
"\n"
"function end_page()\n"
"{\n"
"  echo \"</body></html>\";\n"
"}\n"
"\n"
"function search_results()\n"
"{\n"
"  global $translator;\n"
"  return $translator['search_results_title'];\n"
"}\n"
"\n"
"function matches_text($num)\n"
"{\n"
"  global $translator;\n"
"  $string = $translator['search_results'][($num>2)?2:$num];\n"
"  // The eval is used so that translator strings can contain $num.\n"
"  eval(\"\\$result = \\\"$string\\\";\");\n"
"  return $result;\n"
"}\n"
"\n"
"function report_matches()\n"
"{\n"
"  global $translator;\n"
"  return $translator['search_matches'];\n"
"}\n"
"\n"
"function readInt($file)\n"
"{\n"
"  $b1 = ord(fgetc($file)); $b2 = ord(fgetc($file));\n"
"  $b3 = ord(fgetc($file)); $b4 = ord(fgetc($file));\n"
"  return ($b1<<24)|($b2<<16)|($b3<<8)|$b4;\n"
"}\n"
"\n"
"function readString($file)\n"
"{\n"
"  $result=\"\";\n"
"  while (ord($c=fgetc($file))) $result.=$c;\n"
"  return $result;\n"
"}\n"
"\n"
"function readHeader($file)\n"
"{\n"
"  $header =fgetc($file); $header.=fgetc($file);\n"
"  $header.=fgetc($file); $header.=fgetc($file);\n"
"  return $header;\n"
"}\n"
"\n"
"function computeIndex($word)\n"
"{\n"
"  // Simple hashing that allows for substring search\n"
"  if (strlen($word)<2) return -1;\n"
"  // high char of the index\n"
"  $hi = ord($word{0});\n"
"  if ($hi==0) return -1;\n"
"  // low char of the index\n"
"  $lo = ord($word{1});\n"
"  if ($lo==0) return -1;\n"
"  // return index\n"
"  return $hi*256+$lo;\n"
"}\n"
"\n"
"function search($file,$word,&$statsList)\n"
"{\n"
"  $index = computeIndex($word);\n"
"  if ($index!=-1) // found a valid index\n"
"  {\n"
"    fseek($file,$index*4+4); // 4 bytes per entry, skip header\n"
"    $index = readInt($file);\n"
"    if ($index) // found words matching the hash key\n"
"    {\n"
"      $start=sizeof($statsList);\n"
"      $count=$start;\n"
"      fseek($file,$index);\n"
"      $w = readString($file);\n"
"      while ($w)\n"
"      {\n"
"        $statIdx = readInt($file);\n"
"        if ($word==substr($w,0,strlen($word)))\n"
"        { // found word that matches (as substring)\n"
"          $statsList[$count++]=array(\n"
"              \"word\"=>$word,\n"
"              \"match\"=>$w,\n"
"              \"index\"=>$statIdx,\n"
"              \"full\"=>strlen($w)==strlen($word),\n"
"              \"docs\"=>array()\n"
"              );\n"
"        }\n"
"        $w = readString($file);\n"
"      }\n"
"      $totalHi=0;\n"
"      $totalFreqHi=0;\n"
"      $totalFreqLo=0;\n"
"      for ($count=$start;$count<sizeof($statsList);$count++)\n"
"      {\n"
"        $statInfo = &$statsList[$count];\n"
"        $multiplier = 1;\n"
"        // whole word matches have a double weight\n"
"        if ($statInfo[\"full\"]) $multiplier=2;\n"
"        fseek($file,$statInfo[\"index\"]); \n"
"        $numDocs = readInt($file);\n"
"        $docInfo = array();\n"
"        // read docs info + occurrence frequency of the word\n"
"        for ($i=0;$i<$numDocs;$i++)\n"
"        {\n"
"          $idx=readInt($file); \n"
"          $freq=readInt($file); \n"
"          $docInfo[$i]=array(\"idx\"  => $idx,\n"
"                             \"freq\" => $freq>>1,\n"
"                             \"rank\" => 0.0,\n"
"                             \"hi\"   => $freq&1\n"
"                            );\n"
"          if ($freq&1) // word occurs in high priority doc\n"
"          {\n"
"            $totalHi++;\n"
"            $totalFreqHi+=$freq*$multiplier;\n"
"          }\n"
"          else // word occurs in low priority doc\n"
"          {\n"
"            $totalFreqLo+=$freq*$multiplier;\n"
"          }\n"
"        }\n"
"        // read name and url info for the doc\n"
"        for ($i=0;$i<$numDocs;$i++)\n"
"        {\n"
"          fseek($file,$docInfo[$i][\"idx\"]);\n"
"          $docInfo[$i][\"name\"]=readString($file);\n"
"          $docInfo[$i][\"url\"]=readString($file);\n"
"        }\n"
"        $statInfo[\"docs\"]=$docInfo;\n"
"      }\n"
"      $totalFreq=($totalHi+1)*$totalFreqLo + $totalFreqHi;\n"
"      for ($count=$start;$count<sizeof($statsList);$count++)\n"
"      {\n"
"        $statInfo = &$statsList[$count];\n"
"        $multiplier = 1;\n"
"        // whole word matches have a double weight\n"
"        if ($statInfo[\"full\"]) $multiplier=2;\n"
"        for ($i=0;$i<sizeof($statInfo[\"docs\"]);$i++)\n"
"        {\n"
"          $docInfo = &$statInfo[\"docs\"];\n"
"          // compute frequency rank of the word in each doc\n"
"          $freq=$docInfo[$i][\"freq\"];\n"
"          if ($docInfo[$i][\"hi\"])\n"
"          {\n"
"            $statInfo[\"docs\"][$i][\"rank\"]=\n"
"              (float)($freq*$multiplier+$totalFreqLo)/$totalFreq;\n"
"          }\n"
"          else\n"
"          {\n"
"            $statInfo[\"docs\"][$i][\"rank\"]=\n"
"              (float)($freq*$multiplier)/$totalFreq;\n"
"          }\n"
"        }\n"
"      }\n"
"    }\n"
"  }\n"
"  return $statsList;\n"
"}\n"
"\n"
"function combine_results($results,&$docs)\n"
"{\n"
"  foreach ($results as $wordInfo)\n"
"  {\n"
"    $docsList = &$wordInfo[\"docs\"];\n"
"    foreach ($docsList as $di)\n"
"    {\n"
"      $key=$di[\"url\"];\n"
"      $rank=$di[\"rank\"];\n"
"      if (isset($docs[$key]))\n"
"      {\n"
"        $docs[$key][\"rank\"]+=$rank;\n"
"      }\n"
"      else\n"
"      {\n"
"        $docs[$key] = array(\"url\"=>$key,\n"
"            \"name\"=>$di[\"name\"],\n"
"            \"rank\"=>$rank\n"
"            );\n"
"      }\n"
"      $docs[$key][\"words\"][] = array(\n"
"               \"word\"=>$wordInfo[\"word\"],\n"
"               \"match\"=>$wordInfo[\"match\"],\n"
"               \"freq\"=>$di[\"freq\"]\n"
"               );\n"
"    }\n"
"  }\n"
"  return $docs;\n"
"}\n"
"\n"
"function filter_results($docs,&$requiredWords,&$forbiddenWords)\n"
"{\n"
"  $filteredDocs=array();\n"
"  while (list ($key, $val) = each ($docs)) \n"
"  {\n"
"    $words = &$docs[$key][\"words\"];\n"
"    $copy=1; // copy entry by default\n"
"    if (sizeof($requiredWords)>0)\n"
"    {\n"
"      foreach ($requiredWords as $reqWord)\n"
"      {\n"
"        $found=0;\n"
"        foreach ($words as $wordInfo)\n"
"        { \n"
"          $found = $wordInfo[\"word\"]==$reqWord;\n"
"          if ($found) break;\n"
"        }\n"
"        if (!$found) \n"
"        {\n"
"          $copy=0; // document contains none of the required words\n"
"          break;\n"
"        }\n"
"      }\n"
"    }\n"
"    if (sizeof($forbiddenWords)>0)\n"
"    {\n"
"      foreach ($words as $wordInfo)\n"
"      {\n"
"        if (in_array($wordInfo[\"word\"],$forbiddenWords))\n"
"        {\n"
"          $copy=0; // document contains a forbidden word\n"
"          break;\n"
"        }\n"
"      }\n"
"    }\n"
"    if ($copy) $filteredDocs[$key]=$docs[$key];\n"
"  }\n"
"  return $filteredDocs;\n"
"}\n"
"\n"
"function compare_rank($a,$b)\n"
"{\n"
"  if ($a[\"rank\"] == $b[\"rank\"]) \n"
"  {\n"
"    return 0;\n"
"  }\n"
"  return ($a[\"rank\"]>$b[\"rank\"]) ? -1 : 1; \n"
"}\n"
"\n"
"function sort_results($docs,&$sorted)\n"
"{\n"
"  $sorted = $docs;\n"
"  usort($sorted,\"compare_rank\");\n"
"  return $sorted;\n"
"}\n"
"\n"
"function report_results(&$docs)\n"
"{\n"
"  echo \"<div class=\\\"header\\\">\";\n"
"  echo \"  <div class=\\\"headertitle\\\">\\n\";\n"
"  echo \"    <h1>\".search_results().\"</h1>\\n\";\n"
"  echo \"  </div>\\n\";\n"
"  echo \"</div>\\n\";\n"
"  echo \"<div class=\\\"searchresults\\\">\\n\";\n"
"  echo \"<table cellspacing=\\\"2\\\">\\n\";\n"
"  $numDocs = sizeof($docs);\n"
"  if ($numDocs==0)\n"
"  {\n"
"    echo \"  <tr>\\n\";\n"
"    echo \"    <td colspan=\\\"2\\\">\".matches_text(0).\"</td>\\n\";\n"
"    echo \"  </tr>\\n\";\n"
"  }\n"
"  else\n"
"  {\n"
"    echo \"  <tr>\\n\";\n"
"    echo \"    <td colspan=\\\"2\\\">\".matches_text($numDocs);\n"
"    echo \"\\n\";\n"
"    echo \"    </td>\\n\";\n"
"    echo \"  </tr>\\n\";\n"
"    $num=1;\n"
"    foreach ($docs as $doc)\n"
"    {\n"
"      echo \"  <tr>\\n\";\n"
"      echo \"    <td align=\\\"right\\\">$num.</td>\";\n"
"      echo     \"<td><a class=\\\"el\\\" href=\\\"\".$doc[\"url\"].\"\\\">\".$doc[\"name\"].\"</a></td>\\n\";\n"
"      echo \"  <tr>\\n\";\n"
"      echo \"    <td></td><td class=\\\"tiny\\\">\".report_matches().\" \";\n"
"      foreach ($doc[\"words\"] as $wordInfo)\n"
"      {\n"
"        $word = $wordInfo[\"word\"];\n"
"        $matchRight = substr($wordInfo[\"match\"],strlen($word));\n"
"        echo \"<b>$word</b>$matchRight(\".$wordInfo[\"freq\"].\") \";\n"
"      }\n"
"      echo \"    </td>\\n\";\n"
"      echo \"  </tr>\\n\";\n"
"      $num++;\n"
"    }\n"
"  }\n"
"  echo \"</table>\\n\";\n"
"  echo \"</div>\\n\";\n"
"}\n"
"\n"
"function run_query($query)\n"
"{\n"
"  if(strcmp('4.1.0', phpversion()) > 0) \n"
"  {\n"
"    die(\"Error: PHP version 4.1.0 or above required!\");\n"
"  }\n"
"  if (!($file=fopen(\"search/search.idx\",\"rb\"))) \n"
"  {\n"
"    die(\"Error: Search index file could NOT be opened!\");\n"
"  }\n"
"  if (readHeader($file)!=\"DOXS\")\n"
"  {\n"
"    die(\"Error: Header of index file is invalid!\");\n"
"  }\n"
"  $results = array();\n"
"  $requiredWords = array();\n"
"  $forbiddenWords = array();\n"
"  $foundWords = array();\n"
"  $word=strtok($query,\" \");\n"
"  while ($word) // for each word in the search query\n"
"  {\n"
"    if (($word{0}=='+')) { $word=substr($word,1); $requiredWords[]=$word; }\n"
"    if (($word{0}=='-')) { $word=substr($word,1); $forbiddenWords[]=$word; }\n"
"    if (!in_array($word,$foundWords))\n"
"    {\n"
"      $foundWords[]=$word;\n"
"      search($file,strtolower($word),$results);\n"
"    }\n"
"    $word=strtok(\" \");\n"
"  }\n"
"  fclose($file);\n"
"  $docs = array();\n"
"  combine_results($results,$docs);\n"
"  // filter out documents with forbidden word or that do not contain\n"
"  // required words\n"
"  $filteredDocs = filter_results($docs,$requiredWords,$forbiddenWords);\n"
"  // sort the results based on rank\n"
"  $sorted = array();\n"
"  sort_results($filteredDocs,$sorted);\n"
"  return $sorted;\n"
"}\n"
"\n"
"function main()\n"
"{\n"
"  $query = \"\";\n"
"  if (array_key_exists(\"query\", $_GET))\n"
"  {\n"
"    $query=$_GET[\"query\"];\n"
"  }\n"
"  $sorted = run_query($query);\n"
"  // Now output the HTML stuff...\n"
"  // End the HTML form\n"
"  end_form(preg_replace(\"/[^a-zA-Z0-9\\-\\_\\.]/i\", \" \", $query ));\n"
"  // report results to the user\n"
"  report_results($sorted);\n"
"  end_page();\n"
"}\n"
"</script>\n"
;

static const char search_opensearch_script[]=
// #include "search_opensearch_php.h"
"<script language=\"PHP\">\n"
"require \"search-functions.php\";\n"
"\n"
"$mode = array_key_exists('v', $_GET)?$_GET['v']:\"\";\n"
"$query = array_key_exists('query', $_GET)?$_GET['query']:\"\";\n"
"\n"
"$query_results = run_query($query);\n"
"\n"
"switch ($mode)\n"
"{\n"
"  case \"opensearch.xml\":\n"
"    opensearch_description();\n"
"    break;\n"
"  case \"json\":\n"
"    opensearch_json_results($query, $query_results);\n"
"    break;\n"
"  case \"xml\":\n"
"    opensearch_xml_results($query, $query_results);\n"
"    break;\n"
"  default:\n"
"    invalid_format($query, $query_results);\n"
"    break;\n"
"}\n"
"\n"
"function opensearch_description()\n"
"{\n"
"  global $config;\n"
"  global $translator;\n"
"\n"
"  $shortname = $translator['search'].\" \".$config['PROJECT_NAME'];\n"
"  $link = \"http://\".$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']);\n"
"  header(\"Content-Type: application/xml\");\n"
"  echo <<<END_OPENSEARCH\n"
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
"<OpenSearchDescription xmlns=\"http://a9.com/-/spec/opensearch/1.1/\">\n"
"<ShortName>$shortname</ShortName>\n"
"<Description>Doxygen Search</Description>\n"
"<InputEncoding>UTF-8</InputEncoding>\n"
"<!--\n"
"<Image height=\"16\" width=\"16\" type=\"image/x-icon\">\n"
"http://dev.squello.com/doc/html/favicon.ico</Image>\n"
"-->\n"
"<Url type=\"text/html\" method=\"GET\"\n"
"template=\"$link/search.php?query={searchTerms}\" />\n"
"<Url type=\"application/x-suggestions+json\" method=\"GET\"\n"
"template=\"$link/search-opensearch.php?v=json&amp;query={searchTerms}\" />\n"
"<Url type=\"application/x-suggestions+xml\" method=\"GET\"\n"
"template=\"$link/search-opensearch.php?v=xml&amp;query={searchTerms}\" />\n"
"</OpenSearchDescription>\n"
"END_OPENSEARCH;\n"
"}\n"
"\n"
"function opensearch_xml_results($query, array $results)\n"
"{\n"
"  // Much as I hate copy'n'paste code re-use, this is for testing;\n"
"  // I expect a richer version to come soon.\n"
"  // Although I hate that IE does this richer than FF more...\n"
"  $qs_results = array();\n"
"  foreach ($results as $i => $val)\n"
"  {\n"
"    foreach ($val['words'] as $j => $word)\n"
"    {\n"
"      if (array_key_exists($word, $qs_results))\n"
"        $qs_results[$word['match']]++;\n"
"      else\n"
"        $qs_results[$word['match']] = 1;\n"
"    }\n"
"  }\n"
"  $result = <<<END_FRAG\n"
"<?xml version=\"1.0\"?>\n"
"<SearchSuggestion xmlns=\"http://schemas.microsoft.com/Search/2008/suggestions\">\n"
"<Query>$query</Query>\n"
"<Section>\n"
"END_FRAG;\n"
"  foreach ($qs_results as $word => $count)\n"
"  {\n"
"    $result .= <<<END_FRAG\n"
"<Item>\n"
"<Text>$word</Text>\n"
"<Description>$count results</Description>\n"
"</Item>\n"
"END_FRAG;\n"
"  }\n"
"  $result .= <<<END_FRAG\n"
"</Section>\n"
"</SearchSuggestion>\n"
"END_FRAG;\n"
"  echo $result;\n"
"}\n"
"\n"
"function opensearch_json_results($query, array $results)\n"
"{\n"
"  $qs_results = array();\n"
"  foreach ($results as $i => $val)\n"
"  {\n"
"    foreach ($val['words'] as $j => $word)\n"
"    {\n"
"      if (array_key_exists($word, $qs_results))\n"
"        $qs_results[$word['match']]++;\n"
"      else\n"
"        $qs_results[$word['match']] = 1;\n"
"    }\n"
"  }\n"
"  $result = '[\"'.$query.'\", [';\n"
"  $json_words = \"\";\n"
"  $json_descriptions = \"\";\n"
"  $i = 0;\n"
"  foreach ($qs_results as $word => $count)\n"
"  {\n"
"    if ($i != 0)\n"
"    {\n"
"      $json_words .= \", \";\n"
"      $json_descriptions .= \", \";\n"
"    }\n"
"    $json_words .= '\"'.$word.'\"';\n"
"    $json_descriptions .= '\"'.$count.' result'.($count==1?'':'s').'\"';\n"
"    $i++;\n"
"  }\n"
"  print \"[\\\"$query\\\", [$json_words],[$json_descriptions]]\";\n"
"}\n"
"\n"
"function invalid_format($query, array $results)\n"
"{\n"
"  print \"Search results for '$query':\\n\\n\";\n"
"  print_r($results);\n"
"}\n"
"</script>\n"
;

static const char search_styleSheet[] =
// #include "search_css.h"
"/*---------------- Search Box */\n"
"\n"
"#FSearchBox {\n"
"    float: left;\n"
"}\n"
"\n"
"#MSearchBox {\n"
"    white-space : nowrap;\n"
"    position: absolute;\n"
"    float: none;\n"
"    display: inline;\n"
"    margin-top: 8px;\n"
"    right: 0px;\n"
"    width: 170px;\n"
"    z-index: 102;\n"
"    background-color: white;\n"
"}\n"
"\n"
"#MSearchBox .left\n"
"{\n"
"    display:block;\n"
"    position:absolute;\n"
"    left:10px;\n"
"    width:20px;\n"
"    height:19px;\n"
"    background:url('search_l.png') no-repeat;\n"
"    background-position:right;\n"
"}\n"
"\n"
"#MSearchSelect {\n"
"    display:block;\n"
"    position:absolute;\n"
"    width:20px;\n"
"    height:19px;\n"
"}\n"
"\n"
".left #MSearchSelect {\n"
"    left:4px;\n"
"}\n"
"\n"
".right #MSearchSelect {\n"
"    right:5px;\n"
"}\n"
"\n"
"#MSearchField {\n"
"    display:block;\n"
"    position:absolute;\n"
"    height:19px;\n"
"    background:url('search_m.png') repeat-x;\n"
"    border:none;\n"
"    width:111px;\n"
"    margin-left:20px;\n"
"    padding-left:4px;\n"
"    color: #909090;\n"
"    outline: none;\n"
"    font: 9pt Arial, Verdana, sans-serif;\n"
"}\n"
"\n"
"#FSearchBox #MSearchField {\n"
"    margin-left:15px;\n"
"}\n"
"\n"
"#MSearchBox .right {\n"
"    display:block;\n"
"    position:absolute;\n"
"    right:10px;\n"
"    top:0px;\n"
"    width:20px;\n"
"    height:19px;\n"
"    background:url('search_r.png') no-repeat;\n"
"    background-position:left;\n"
"}\n"
"\n"
"#MSearchClose {\n"
"    display: none;\n"
"    position: absolute;\n"
"    top: 4px;\n"
"    background : none;\n"
"    border: none;\n"
"    margin: 0px 4px 0px 0px;\n"
"    padding: 0px 0px;\n"
"    outline: none;\n"
"}\n"
"\n"
".left #MSearchClose {\n"
"    left: 6px;\n"
"}\n"
"\n"
".right #MSearchClose {\n"
"    right: 2px;\n"
"}\n"
"\n"
".MSearchBoxActive #MSearchField {\n"
"    color: #000000;\n"
"}\n"
"\n"
"/*---------------- Search filter selection */\n"
"\n"
"#MSearchSelectWindow {\n"
"    display: none;\n"
"    position: absolute;\n"
"    left: 0; top: 0;\n"
"    border: 1px solid ##A0;\n"
"    background-color: ##FA;\n"
"    z-index: 1;\n"
"    padding-top: 4px;\n"
"    padding-bottom: 4px;\n"
"    -moz-border-radius: 4px;\n"
"    -webkit-border-top-left-radius: 4px;\n"
"    -webkit-border-top-right-radius: 4px;\n"
"    -webkit-border-bottom-left-radius: 4px;\n"
"    -webkit-border-bottom-right-radius: 4px;\n"
"    -webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);\n"
"}\n"
"\n"
".SelectItem {\n"
"    font: 8pt Arial, Verdana, sans-serif;\n"
"    padding-left:  2px;\n"
"    padding-right: 12px;\n"
"    border: 0px;\n"
"}\n"
"\n"
"span.SelectionMark {\n"
"    margin-right: 4px;\n"
"    font-family: monospace;\n"
"    outline-style: none;\n"
"    text-decoration: none;\n"
"}\n"
"\n"
"a.SelectItem {\n"
"    display: block;\n"
"    outline-style: none;\n"
"    color: #000000; \n"
"    text-decoration: none;\n"
"    padding-left:   6px;\n"
"    padding-right: 12px;\n"
"}\n"
"\n"
"a.SelectItem:focus,\n"
"a.SelectItem:active {\n"
"    color: #000000; \n"
"    outline-style: none;\n"
"    text-decoration: none;\n"
"}\n"
"\n"
"a.SelectItem:hover {\n"
"    color: #FFFFFF;\n"
"    background-color: ##50;\n"
"    outline-style: none;\n"
"    text-decoration: none;\n"
"    cursor: pointer;\n"
"    display: block;\n"
"}\n"
"\n"
"/*---------------- Search results window */\n"
"\n"
"iframe#MSearchResults {\n"
"    width: 60ex;\n"
"    height: 15em;\n"
"}\n"
"\n"
"#MSearchResultsWindow {\n"
"    display: none;\n"
"    position: absolute;\n"
"    left: 0; top: 0;\n"
"    border: 1px solid #000;\n"
"    background-color: ##F0;\n"
"}\n"
"\n"
"/* ----------------------------------- */\n"
"\n"
"\n"
"#SRIndex {\n"
"    clear:both; \n"
"    padding-bottom: 15px;\n"
"}\n"
"\n"
".SREntry {\n"
"    font-size: 10pt;\n"
"    padding-left: 1ex;\n"
"}\n"
"\n"
".SRPage .SREntry {\n"
"    font-size: 8pt;\n"
"    padding: 1px 5px;\n"
"}\n"
"\n"
"body.SRPage {\n"
"    margin: 5px 2px;\n"
"}\n"
"\n"
".SRChildren {\n"
"    padding-left: 3ex; padding-bottom: .5em \n"
"}\n"
"\n"
".SRPage .SRChildren {\n"
"    display: none;\n"
"}\n"
"\n"
".SRSymbol {\n"
"    font-weight: bold; \n"
"    color: ##58;\n"
"    font-family: Arial, Verdana, sans-serif;\n"
"    text-decoration: none;\n"
"    outline: none;\n"
"}\n"
"\n"
"a.SRScope {\n"
"    display: block;\n"
"    color: ##58; \n"
"    font-family: Arial, Verdana, sans-serif;\n"
"    text-decoration: none;\n"
"    outline: none;\n"
"}\n"
"\n"
"a.SRSymbol:focus, a.SRSymbol:active,\n"
"a.SRScope:focus, a.SRScope:active {\n"
"    text-decoration: underline;\n"
"}\n"
"\n"
"span.SRScope {\n"
"    padding-left: 4px;\n"
"}\n"
"\n"
".SRPage .SRStatus {\n"
"    padding: 2px 5px;\n"
"    font-size: 8pt;\n"
"    font-style: italic;\n"
"}\n"
"\n"
".SRResult {\n"
"    display: none;\n"
"}\n"
"\n"
"DIV.searchresults {\n"
"    margin-left: 10px;\n"
"    margin-right: 10px;\n"
"}\n"
"\n"
"/*---------------- External search page results */\n"
"\n"
".searchresult {\n"
"    background-color: ##F2;\n"
"}\n"
"\n"
".pages b {\n"
"   color: white;\n"
"   padding: 5px 5px 3px 5px;\n"
"   background-image: url(\"../tab_a.png\");\n"
"   background-repeat: repeat-x;\n"
"   text-shadow: 0 1px 1px #000000;\n"
"}\n"
"\n"
".pages {\n"
"    line-height: 17px;\n"
"    margin-left: 4px;\n"
"    text-decoration: none;\n"
"}\n"
"\n"
".hl {\n"
"    font-weight: bold;\n"
"}\n"
"\n"
"#searchresults {\n"
"    margin-bottom: 20px;\n"
"}\n"
"\n"
".searchpages {\n"
"    margin-top: 10px;\n"
"}\n"
"\n"
;

static const char search_jquery_script1[]=
// #include "jquery_p1_js.h"
"/*! jQuery v1.7.1 jquery.com | jquery.org/license */\n"
"(function(a,b){function cy(a){return f.isWindow(a)?a:a.nodeType===9?a.defaultView||a.parentWindow:!1}function cv(a){if(!ck[a]){var b=c.body,d=f(\"<\"+a+\">\").appendTo(b),e=d.css(\"display\");d.remove();if(e===\"none\"||e===\"\"){cl||(cl=c.createElement(\"iframe\"),cl.frameBorder=cl.width=cl.height=0),b.appendChild(cl);if(!cm||!cl.createElement)cm=(cl.contentWindow||cl.contentDocument).document,cm.write((c.compatMode===\"CSS1Compat\"?\"<!doctype html>\":\"\")+\"<html><body>\"),cm.close();d=cm.createElement(a),cm.body.appendChild(d),e=f.css(d,\"display\"),b.removeChild(cl)}ck[a]=e}return ck[a]}function cu(a,b){var c={};f.each(cq.concat.apply([],cq.slice(0,b)),function(){c[this]=a});return c}function ct(){cr=b}function cs(){setTimeout(ct,0);return cr=f.now()}function cj(){try{return new a.ActiveXObject(\"Microsoft.XMLHTTP\")}catch(b){}}function ci(){try{return new a.XMLHttpRequest}catch(b){}}function cc(a,c){a.dataFilter&&(c=a.dataFilter(c,a.dataType));var d=a.dataTypes,e={},g,h,i=d.length,j,k=d[0],l,m,n,o,p;for(g=1;g<i;g++){if(g===1)for(h in a.converters)typeof h==\"string\"&&(e[h.toLowerCase()]=a.converters[h]);l=k,k=d[g];if(k===\"*\")k=l;else if(l!==\"*\"&&l!==k){m=l+\" \"+k,n=e[m]||e[\"* \"+k];if(!n){p=b;for(o in e){j=o.split(\" \");if(j[0]===l||j[0]===\"*\"){p=e[j[1]+\" \"+k];if(p){o=e[o],o===!0?n=p:p===!0&&(n=o);break}}}}!n&&!p&&f.error(\"No conversion from \"+m.replace(\" \",\" to \")),n!==!0&&(c=n?n(c):p(o(c)))}}return c}function cb(a,c,d){var e=a.contents,f=a.dataTypes,g=a.responseFields,h,i,j,k;for(i in g)i in d&&(c[g[i]]=d[i]);while(f[0]===\"*\")f.shift(),h===b&&(h=a.mimeType||c.getResponseHeader(\"content-type\"));if(h)for(i in e)if(e[i]&&e[i].test(h)){f.unshift(i);break}if(f[0]in d)j=f[0];else{for(i in d){if(!f[0]||a.converters[i+\" \"+f[0]]){j=i;break}k||(k=i)}j=j||k}if(j){j!==f[0]&&f.unshift(j);return d[j]}}function ca(a,b,c,d){if(f.isArray(b))f.each(b,function(b,e){c||bE.test(a)?d(a,e):ca(a+\"[\"+(typeof e==\"object\"||f.isArray(e)?b:\"\")+\"]\",e,c,d)});else if(!c&&b!=null&&typeof b==\"object\")for(var e in b)ca(a+\"[\"+e+\"]\",b[e],c,d);else d(a,b)}function b_(a,c){var d,e,g=f.ajaxSettings.flatOptions||{};for(d in c)c[d]!==b&&((g[d]?a:e||(e={}))[d]=c[d]);e&&f.extend(!0,a,e)}function b$(a,c,d,e,f,g){f=f||c.dataTypes[0],g=g||{},g[f]=!0;var h=a[f],i=0,j=h?h.length:0,k=a===bT,l;for(;i<j&&(k||!l);i++)l=h[i](c,d,e),typeof l==\"string\"&&(!k||g[l]?l=b:(c.dataTypes.unshift(l),l=b$(a,c,d,e,l,g)));(k||!l)&&!g[\"*\"]&&(l=b$(a,c,d,e,\"*\",g));return l}function bZ(a){return function(b,c){typeof b!=\"string\"&&(c=b,b=\"*\");if(f.isFunction(c)){var d=b.toLowerCase().split(bP),e=0,g=d.length,h,i,j;for(;e<g;e++)h=d[e],j=/^\\+/.test(h),j&&(h=h.substr(1)||\"*\"),i=a[h]=a[h]||[],i[j?\"unshift\":\"push\"](c)}}}function bC(a,b,c){var d=b===\"width\"?a.offsetWidth:a.offsetHeight,e=b===\"width\"?bx:by,g=0,h=e.length;if(d>0){if(c!==\"border\")for(;g<h;g++)c||(d-=parseFloat(f.css(a,\"padding\"+e[g]))||0),c===\"margin\"?d+=parseFloat(f.css(a,c+e[g]))||0:d-=parseFloat(f.css(a,\"border\"+e[g]+\"Width\"))||0;return d+\"px\"}d=bz(a,b,b);if(d<0||d==null)d=a.style[b]||0;d=parseFloat(d)||0;if(c)for(;g<h;g++)d+=parseFloat(f.css(a,\"padding\"+e[g]))||0,c!==\"padding\"&&(d+=parseFloat(f.css(a,\"border\"+e[g]+\"Width\"))||0),c===\"margin\"&&(d+=parseFloat(f.css(a,c+e[g]))||0);return d+\"px\"}function bp(a,b){b.src?f.ajax({url:b.src,async:!1,dataType:\"script\"}):f.globalEval((b.text||b.textContent||b.innerHTML||\"\").replace(bf,\"/*$0*/\")),b.parentNode&&b.parentNode.removeChild(b)}function bo(a){var b=c.createElement(\"div\");bh.appendChild(b),b.innerHTML=a.outerHTML;return b.firstChild}function bn(a){var b=(a.nodeName||\"\").toLowerCase();b===\"input\"?bm(a):b!==\"script\"&&typeof a.getElementsByTagName!=\"undefined\"&&f.grep(a.getElementsByTagName(\"input\"),bm)}function bm(a){if(a.type===\"checkbox\"||a.type===\"radio\")a.defaultChecked=a.checked}function bl(a){return typeof a.getElementsByTagName!=\"undefined\"?a.getElementsByTagName(\"*\"):typeof a.querySelectorAll!=\"undefined\"?a.querySelectorAll(\"*\"):[]}function bk(a,b){var c;if(b.nodeType===1){b.clearAttributes&&b.clearAttributes(),b.mergeAttributes&&b.mergeAttributes(a),c=b.nodeName.toLowerCase();if(c===\"object\")b.outerHTML=a.outerHTML;else if(c!==\"input\"||a.type!==\"checkbox\"&&a.type!==\"radio\"){if(c===\"option\")b.selected=a.defaultSelected;else if(c===\"input\"||c===\"textarea\")b.defaultValue=a.defaultValue}else a.checked&&(b.defaultChecked=b.checked=a.checked),b.value!==a.value&&(b.value=a.value);b.removeAttribute(f.expando)}}function bj(a,b){if(b.nodeType===1&&!!f.hasData(a)){var c,d,e,g=f._data(a),h=f._data(b,g),i=g.events;if(i){delete h.handle,h.events={};for(c in i)for(d=0,e=i[c].length;d<e;d++)f.event.add(b,c+(i[c][d].namespace?\".\":\"\")+i[c][d].namespace,i[c][d],i[c][d].data)}h.data&&(h.data=f.extend({},h.data))}}function bi(a,b){return f.nodeName(a,\"table\")?a.getElementsByTagName(\"tbody\")[0]||a.appendChild(a.ownerDocument.createElement(\"tbody\")):a}function U(a){var b=V.split(\"|\"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}function T(a,b,c){b=b||0;if(f.isFunction(b))return f.grep(a,function(a,d){var e=!!b.call(a,d,a);return e===c});if(b.nodeType)return f.grep(a,function(a,d){return a===b===c});if(typeof b==\"string\"){var d=f.grep(a,function(a){return a.nodeType===1});if(O.test(b))return f.filter(b,d,!c);b=f.filter(b,d)}return f.grep(a,function(a,d){return f.inArray(a,b)>=0===c})}function S(a){return!a||!a.parentNode||a.parentNode.nodeType===11}function K(){return!0}function J(){return!1}function n(a,b,c){var d=b+\"defer\",e=b+\"queue\",g=b+\"mark\",h=f._data(a,d);h&&(c===\"queue\"||!f._data(a,e))&&(c===\"mark\"||!f._data(a,g))&&setTimeout(function(){!f._data(a,e)&&!f._data(a,g)&&(f.removeData(a,d,!0),h.fire())},0)}function m(a){for(var b in a){if(b===\"data\"&&f.isEmptyObject(a[b]))continue;if(b!==\"toJSON\")return!1}return!0}function l(a,c,d){if(d===b&&a.nodeType===1){var e=\"data-\"+c.replace(k,\"-$1\").toLowerCase();d=a.getAttribute(e);if(typeof d==\"string\"){try{d=d===\"true\"?!0:d===\"false\"?!1:d===\"null\"?null:f.isNumeric(d)?parseFloat(d):j.test(d)?f.parseJSON(d):d}catch(g){}f.data(a,c,d)}else d=b}return d}function h(a){var b=g[a]={},c,d;a=a.split(/\\s+/);for(c=0,d=a.length;c<d;c++)b[a[c]]=!0;return b}var c=a.document,d=a.navigator,e=a.location,f=function(){function J(){if(!e.isReady){try{c.documentElement.doScroll(\"left\")}catch(a){setTimeout(J,1);return}e.ready()}}var e=function(a,b){return new e.fn.init(a,b,h)},f=a.jQuery,g=a.$,h,i=/^(?:[^#<]*(<[\\w\\W]+>)[^>]*$|#([\\w\\-]*)$)/,j=/\\S/,k=/^\\s+/,l=/\\s+$/,m=/^<(\\w+)\\s*\\/?>(?:<\\/\\1>)?$/,n=/^[\\],:{}\\s]*$/,o=/\\\\(?:[\"\\\\\\/bfnrt]|u[0-9a-fA-F]{4})/g,p=/\"[^\"\\\\\\n\\r]*\"|true|false|null|-?\\d+(?:\\.\\d*)?(?:[eE][+\\-]?\\d+)?/g,q=/(?:^|:|,)(?:\\s*\\[)+/g,r=/(webkit)[ \\/]([\\w.]+)/,s=/(opera)(?:.*version)?[ \\/]([\\w.]+)/,t=/(msie) ([\\w.]+)/,u=/(mozilla)(?:.*? rv:([\\w.]+))?/,v=/-([a-z]|[0-9])/ig,w=/^-ms-/,x=function(a,b){return(b+\"\").toUpperCase()},y=d.userAgent,z,A,B,C=Object.prototype.toString,D=Object.prototype.hasOwnProperty,E=Array.prototype.push,F=Array.prototype.slice,G=String.prototype.trim,H=Array.prototype.indexOf,I={};e.fn=e.prototype={constructor:e,init:function(a,d,f){var g,h,j,k;if(!a)return this;if(a.nodeType){this.context=this[0]=a,this.length=1;return this}if(a===\"body\"&&!d&&c.body){this.context=c,this[0]=c.body,this.selector=a,this.length=1;return this}if(typeof a==\"string\"){a.charAt(0)!==\"<\"||a.charAt(a.length-1)!==\">\"||a.length<3?g=i.exec(a):g=[null,a,null];if(g&&(g[1]||!d)){if(g[1]){d=d instanceof e?d[0]:d,k=d?d.ownerDocument||d:c,j=m.exec(a),j?e.isPlainObject(d)?(a=[c.createElement(j[1])],e.fn.attr.call(a,d,!0)):a=[k.createElement(j[1])]:(j=e.buildFragment([g[1]],[k]),a=(j.cacheable?e.clone(j.fragment):j.fragment).childNodes);return e.merge(this,a)}h=c.getElementById(g[2]);if(h&&h.parentNode){if(h.id!==g[2])return f.find(a);this.length=1,this[0]=h}this.context=c,this.selector=a;return this}return!d||d.jquery?(d||f).find(a):this.constructor(d).find(a)}if(e.isFunction(a))return f.ready(a);a.selector!==b&&(this.selector=a.selector,this.context=a.context);return e.makeArray(a,this)},selector:\"\",jquery:\"1.7.1\",length:0,size:function(){return this.length},toArray:function(){return F.call(this,0)},get:function(a){return a==null?this.toArray():a<0?this[this.length+a]:this[a]},pushStack:function(a,b,c){var d=this.constructor();e.isArray(a)?E.apply(d,a):e.merge(d,a),d.prevObject=this,d.context=this.context,b===\"find\"?d.selector=this.selector+(this.selector?\" \":\"\")+c:b&&(d.selector=this.selector+\".\"+b+\"(\"+c+\")\");return d},each:function(a,b){return e.each(this,a,b)},ready:function(a){e.bindReady(),A.add(a);return this},eq:function(a){a=+a;return a===-1?this.slice(a):this.slice(a,a+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(F.apply(this,arguments),\"slice\",F.call(arguments).join(\",\"))},map:function(a){return this.pushStack(e.map(this,function(b,c){return a.call(b,c,b)}))},end:function(){return this.prevObject||this.constructor(null)},push:E,sort:[].sort,splice:[].splice},e.fn.init.prototype=e.fn,e.extend=e.fn.extend=function(){var a,c,d,f,g,h,i=arguments[0]||{},j=1,k=arguments.length,l=!1;typeof i==\"boolean\"&&(l=i,i=arguments[1]||{},j=2),typeof i!=\"object\"&&!e.isFunction(i)&&(i={}),k===j&&(i=this,--j);for(;j<k;j++)if((a=arguments[j])!=null)for(c in a){d=i[c],f=a[c];if(i===f)continue;l&&f&&(e.isPlainObject(f)||(g=e.isArray(f)))?(g?(g=!1,h=d&&e.isArray(d)?d:[]):h=d&&e.isPlainObject(d)?d:{},i[c]=e.extend(l,h,f)):f!==b&&(i[c]=f)}return i},e.extend({noConflict:function(b){a.$===e&&(a.$=g),b&&a.jQuery===e&&(a.jQuery=f);return e},isReady:!1,readyWait:1,holdReady:function(a){a?e.readyWait++:e.ready(!0)},ready:function(a){if(a===!0&&!--e.readyWait||a!==!0&&!e.isReady){if(!c.body)return setTimeout(e.ready,1);e.isReady=!0;if(a!==!0&&--e.readyWait>0)return;A.fireWith(c,[e]),e.fn.trigger&&e(c).trigger(\"ready\").off(\"ready\")}},bindReady:function(){if(!A){A=e.Callbacks(\"once memory\");if(c.readyState===\"complete\")return setTimeout(e.ready,1);if(c.addEventListener)c.addEventListener(\"DOMContentLoaded\",B,!1),a.addEventListener(\"load\",e.ready,!1);else if(c.attachEvent){c.attachEvent(\"onreadystatechange\",B),a.attachEvent(\"onload\",e.ready);var b=!1;try{b=a.frameElement==null}catch(d){}c.documentElement.doScroll&&b&&J()}}},isFunction:function(a){return e.type(a)===\"function\"},isArray:Array.isArray||function(a){return e.type(a)===\"array\"},isWindow:function(a){return a&&typeof a==\"object\"&&\"setInterval\"in a},isNumeric:function(a){return!isNaN(parseFloat(a))&&isFinite(a)},type:function(a){return a==null?String(a):I[C.call(a)]||\"object\"},isPlainObject:function(a){if(!a||e.type(a)!==\"object\"||a.nodeType||e.isWindow(a))return!1;try{if(a.constructor&&!D.call(a,\"constructor\")&&!D.call(a.constructor.prototype,\"isPrototypeOf\"))return!1}catch(c){return!1}var d;for(d in a);return d===b||D.call(a,d)},isEmptyObject:function(a){for(var b in a)return!1;return!0},error:function(a){throw new Error(a)},parseJSON:function(b){if(typeof b!=\"string\"||!b)return null;b=e.trim(b);if(a.JSON&&a.JSON.parse)return a.JSON.parse(b);if(n.test(b.replace(o,\"@\").replace(p,\"]\").replace(q,\"\")))return(new Function(\"return \"+b))();e.error(\"Invalid JSON: \"+b)},parseXML:function(c){var d,f;try{a.DOMParser?(f=new DOMParser,d=f.parseFromString(c,\"text/xml\")):(d=new ActiveXObject(\"Microsoft.XMLDOM\"),d.async=\"false\",d.loadXML(c))}catch(g){d=b}(!d||!d.documentElement||d.getElementsByTagName(\"parsererror\").length)&&e.error(\"Invalid XML: \"+c);return d},noop:function(){},globalEval:function(b){b&&j.test(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(w,\"ms-\").replace(v,x)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toUpperCase()===b.toUpperCase()},each:function(a,c,d){var f,g=0,h=a.length,i=h===b||e.isFunction(a);if(d){if(i){for(f in a)if(c.apply(a[f],d)===!1)break}else for(;g<h;)if(c.apply(a[g++],d)===!1)break}else if(i){for(f in a)if(c.call(a[f],f,a[f])===!1)break}else for(;g<h;)if(c.call(a[g],g,a[g++])===!1)break;return a},trim:G?function(a){return a==null?\"\":G.call(a)}:function(a){return a==null?\"\":(a+\"\").replace(k,\"\").replace(l,\"\")},makeArray:function(a,b){var c=b||[];if(a!=null){var d=e.type(a);a.length==null||d===\"string\"||d===\"function\"||d===\"regexp\"||e.isWindow(a)?E.call(c,a):e.merge(c,a)}return c},inArray:function(a,b,c){var d;if(b){if(H)return H.call(b,a,c);d=b.length,c=c?c<0?Math.max(0,d+c):c:0;for(;c<d;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,c){var d=a.length,e=0;if(typeof c.length==\"number\")for(var f=c.length;e<f;e++)a[d++]=c[e];else while(c[e]!==b)a[d++]=c[e++];a.length=d;return a},grep:function(a,b,c){var d=[],e;c=!!c;for(var f=0,g=a.length;f<g;f++)e=!!b(a[f],f),c!==e&&d.push(a[f]);return d},map:function(a,c,d){var f,g,h=[],i=0,j=a.length,k=a instanceof e||j!==b&&typeof j==\"number\"&&(j>0&&a[0]&&a[j-1]||j===0||e.isArray(a));if(k)for(;i<j;i++)f=c(a[i],i,d),f!=null&&(h[h.length]=f);else for(g in a)f=c(a[g],g,d),f!=null&&(h[h.length]=f);return h.concat.apply([],h)},guid:1,proxy:function(a,c){if(typeof c==\"string\"){var d=a[c];c=a,a=d}if(!e.isFunction(a))return b;var f=F.call(arguments,2),g=function(){return a.apply(c,f.concat(F.call(arguments)))};g.guid=a.guid=a.guid||g.guid||e.guid++;return g},access:function(a,c,d,f,g,h){var i=a.length;if(typeof c==\"object\"){for(var j in c)e.access(a,j,c[j],f,g,d);return a}if(d!==b){f=!h&&f&&e.isFunction(d);for(var k=0;k<i;k++)g(a[k],c,f?d.call(a[k],k,g(a[k],c)):d,h);return a}return i?g(a[0],c):b},now:function(){return(new Date).getTime()},uaMatch:function(a){a=a.toLowerCase();var b=r.exec(a)||s.exec(a)||t.exec(a)||a.indexOf(\"compatible\")<0&&u.exec(a)||[];return{browser:b[1]||\"\",version:b[2]||\"0\"}},sub:function(){function a(b,c){return new a.fn.init(b,c)}e.extend(!0,a,this),a.superclass=this,a.fn=a.prototype=this(),a.fn.constructor=a,a.sub=this.sub,a.fn.init=function(d,f){f&&f instanceof e&&!(f instanceof a)&&(f=a(f));return e.fn.init.call(this,d,f,b)},a.fn.init.prototype=a.fn;var b=a(c);return a},browser:{}}),e.each(\"Boolean Number String Function Array Date RegExp Object\".split(\" \"),function(a,b){I[\"[object \"+b+\"]\"]=b.toLowerCase()}),z=e.uaMatch(y),z.browser&&(e.browser[z.browser]=!0,e.browser.version=z.version),e.browser.webkit&&(e.browser.safari=!0),j.test(\" \")&&(k=/^[\\s\\xA0]+/,l=/[\\s\\xA0]+$/),h=e(c),c.addEventListener?B=function(){c.removeEventListener(\"DOMContentLoaded\",B,!1),e.ready()}:c.attachEvent&&(B=function(){c.readyState===\"complete\"&&(c.detachEvent(\"onreadystatechange\",B),e.ready())});return e}(),g={};f.Callbacks=function(a){a=a?g[a]||h(a):{};var c=[],d=[],e,i,j,k,l,m=function(b){var d,e,g,h,i;for(d=0,e=b.length;d<e;d++)g=b[d],h=f.type(g),h===\"array\"?m(g):h===\"function\"&&(!a.unique||!o.has(g))&&c.push(g)},n=function(b,f){f=f||[],e=!a.memory||[b,f],i=!0,l=j||0,j=0,k=c.length;for(;c&&l<k;l++)if(c[l].apply(b,f)===!1&&a.stopOnFalse){e=!0;break}i=!1,c&&(a.once?e===!0?o.disable():c=[]:d&&d.length&&(e=d.shift(),o.fireWith(e[0],e[1])))},o={add:function(){if(c){var a=c.length;m(arguments),i?k=c.length:e&&e!==!0&&(j=a,n(e[0],e[1]))}return this},remove:function(){if(c){var b=arguments,d=0,e=b.length;for(;d<e;d++)for(var f=0;f<c.length;f++)if(b[d]===c[f]){i&&f<=k&&(k--,f<=l&&l--),c.splice(f--,1);if(a.unique)break}}return this},has:function(a){if(c){var b=0,d=c.length;for(;b<d;b++)if(a===c[b])return!0}return!1},empty:function(){c=[];return this},disable:function(){c=d=e=b;return this},disabled:function(){return!c},lock:function(){d=b,(!e||e===!0)&&o.disable();return this},locked:function(){return!d},fireWith:function(b,c){d&&(i?a.once||d.push([b,c]):(!a.once||!e)&&n(b,c));return this},fire:function(){o.fireWith(this,arguments);return this},fired:function(){return!!e}};return o};var i=[].slice;f.extend({Deferred:function(a){var b=f.Callbacks(\"once memory\"),c=f.Callbacks(\"once memory\"),d=f.Callbacks(\"memory\"),e=\"pending\",g={resolve:b,reject:c,notify:d},h={done:b.add,fail:c.add,progress:d.add,state:function(){return e},isResolved:b.fired,isRejected:c.fired,then:function(a,b,c){i.done(a).fail(b).progress(c);return this},always:function(){i.done.apply(i,arguments).fail.apply(i,arguments);return this},pipe:function(a,b,c){return f.Deferred(function(d){f.each({done:[a,\"resolve\"],fail:[b,\"reject\"],progress:[c,\"notify\"]},function(a,b){var c=b[0],e=b[1],g;f.isFunction(c)?i[a](function()\n"
"{g=c.apply(this,arguments),g&&f.isFunction(g.promise)?g.promise().then(d.resolve,d.reject,d.notify):d[e+\"With\"](this===i?d:this,[g])}):i[a](d[e])})}).promise()},promise:function(a){if(a==null)a=h;else for(var b in h)a[b]=h[b];return a}},i=h.promise({}),j;for(j in g)i[j]=g[j].fire,i[j+\"With\"]=g[j].fireWith;i.done(function(){e=\"resolved\"},c.disable,d.lock).fail(function(){e=\"rejected\"},b.disable,d.lock),a&&a.call(i,i);return i},when:function(a){function m(a){return function(b){e[a]=arguments.length>1?i.call(arguments,0):b,j.notifyWith(k,e)}}function l(a){return function(c){b[a]=arguments.length>1?i.call(arguments,0):c,--g||j.resolveWith(j,b)}}var b=i.call(arguments,0),c=0,d=b.length,e=Array(d),g=d,h=d,j=d<=1&&a&&f.isFunction(a.promise)?a:f.Deferred(),k=j.promise();if(d>1){for(;c<d;c++)b[c]&&b[c].promise&&f.isFunction(b[c].promise)?b[c].promise().then(l(c),j.reject,m(c)):--g;g||j.resolveWith(j,b)}else j!==a&&j.resolveWith(j,d?[a]:[]);return k}}),f.support=function(){var b,d,e,g,h,i,j,k,l,m,n,o,p,q=c.createElement(\"div\"),r=c.documentElement;q.setAttribute(\"className\",\"t\"),q.innerHTML=\"   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>\",d=q.getElementsByTagName(\"*\"),e=q.getElementsByTagName(\"a\")[0];if(!d||!d.length||!e)return{};g=c.createElement(\"select\"),h=g.appendChild(c.createElement(\"option\")),i=q.getElementsByTagName(\"input\")[0],b={leadingWhitespace:q.firstChild.nodeType===3,tbody:!q.getElementsByTagName(\"tbody\").length,htmlSerialize:!!q.getElementsByTagName(\"link\").length,style:/top/.test(e.getAttribute(\"style\")),hrefNormalized:e.getAttribute(\"href\")===\"/a\",opacity:/^0.55/.test(e.style.opacity),cssFloat:!!e.style.cssFloat,checkOn:i.value===\"on\",optSelected:h.selected,getSetAttribute:q.className!==\"t\",enctype:!!c.createElement(\"form\").enctype,html5Clone:c.createElement(\"nav\").cloneNode(!0).outerHTML!==\"<:nav></:nav>\",submitBubbles:!0,changeBubbles:!0,focusinBubbles:!1,deleteExpando:!0,noCloneEvent:!0,inlineBlockNeedsLayout:!1,shrinkWrapBlocks:!1,reliableMarginRight:!0},i.checked=!0,b.noCloneChecked=i.cloneNode(!0).checked,g.disabled=!0,b.optDisabled=!h.disabled;try{delete q.test}catch(s){b.deleteExpando=!1}!q.addEventListener&&q.attachEvent&&q.fireEvent&&(q.attachEvent(\"onclick\",function(){b.noCloneEvent=!1}),q.cloneNode(!0).fireEvent(\"onclick\")),i=c.createElement(\"input\"),i.value=\"t\",i.setAttribute(\"type\",\"radio\"),b.radioValue=i.value===\"t\",i.setAttribute(\"checked\",\"checked\"),q.appendChild(i),k=c.createDocumentFragment(),k.appendChild(q.lastChild),b.checkClone=k.cloneNode(!0).cloneNode(!0).lastChild.checked,b.appendChecked=i.checked,k.removeChild(i),k.appendChild(q),q.innerHTML=\"\",a.getComputedStyle&&(j=c.createElement(\"div\"),j.style.width=\"0\",j.style.marginRight=\"0\",q.style.width=\"2px\",q.appendChild(j),b.reliableMarginRight=(parseInt((a.getComputedStyle(j,null)||{marginRight:0}).marginRight,10)||0)===0);if(q.attachEvent)for(o in{submit:1,change:1,focusin:1})n=\"on\"+o,p=n in q,p||(q.setAttribute(n,\"return;\"),p=typeof q[n]==\"function\"),b[o+\"Bubbles\"]=p;k.removeChild(q),k=g=h=j=q=i=null,f(function(){var a,d,e,g,h,i,j,k,m,n,o,r=c.getElementsByTagName(\"body\")[0];!r||(j=1,k=\"position:absolute;top:0;left:0;width:1px;height:1px;margin:0;\",m=\"visibility:hidden;border:0;\",n=\"style='\"+k+\"border:5px solid #000;padding:0;'\",o=\"<div \"+n+\"><div></div></div>\"+\"<table \"+n+\" cellpadding='0' cellspacing='0'>\"+\"<tr><td></td></tr></table>\",a=c.createElement(\"div\"),a.style.cssText=m+\"width:0;height:0;position:static;top:0;margin-top:\"+j+\"px\",r.insertBefore(a,r.firstChild),q=c.createElement(\"div\"),a.appendChild(q),q.innerHTML=\"<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>\",l=q.getElementsByTagName(\"td\"),p=l[0].offsetHeight===0,l[0].style.display=\"\",l[1].style.display=\"none\",b.reliableHiddenOffsets=p&&l[0].offsetHeight===0,q.innerHTML=\"\",q.style.width=q.style.paddingLeft=\"1px\",f.boxModel=b.boxModel=q.offsetWidth===2,typeof q.style.zoom!=\"undefined\"&&(q.style.display=\"inline\",q.style.zoom=1,b.inlineBlockNeedsLayout=q.offsetWidth===2,q.style.display=\"\",q.innerHTML=\"<div style='width:4px;'></div>\",b.shrinkWrapBlocks=q.offsetWidth!==2),q.style.cssText=k+m,q.innerHTML=o,d=q.firstChild,e=d.firstChild,h=d.nextSibling.firstChild.firstChild,i={doesNotAddBorder:e.offsetTop!==5,doesAddBorderForTableAndCells:h.offsetTop===5},e.style.position=\"fixed\",e.style.top=\"20px\",i.fixedPosition=e.offsetTop===20||e.offsetTop===15,e.style.position=e.style.top=\"\",d.style.overflow=\"hidden\",d.style.position=\"relative\",i.subtractsBorderForOverflowNotVisible=e.offsetTop===-5,i.doesNotIncludeMarginInBodyOffset=r.offsetTop!==j,r.removeChild(a),q=a=null,f.extend(b,i))});return b}();var j=/^(?:\\{.*\\}|\\[.*\\])$/,k=/([A-Z])/g;f.extend({cache:{},uuid:0,expando:\"jQuery\"+(f.fn.jquery+Math.random()).replace(/\\D/g,\"\"),noData:{embed:!0,object:\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\",applet:!0},hasData:function(a){a=a.nodeType?f.cache[a[f.expando]]:a[f.expando];return!!a&&!m(a)},data:function(a,c,d,e){if(!!f.acceptData(a)){var g,h,i,j=f.expando,k=typeof c==\"string\",l=a.nodeType,m=l?f.cache:a,n=l?a[j]:a[j]&&j,o=c===\"events\";if((!n||!m[n]||!o&&!e&&!m[n].data)&&k&&d===b)return;n||(l?a[j]=n=++f.uuid:n=j),m[n]||(m[n]={},l||(m[n].toJSON=f.noop));if(typeof c==\"object\"||typeof c==\"function\")e?m[n]=f.extend(m[n],c):m[n].data=f.extend(m[n].data,c);g=h=m[n],e||(h.data||(h.data={}),h=h.data),d!==b&&(h[f.camelCase(c)]=d);if(o&&!h[c])return g.events;k?(i=h[c],i==null&&(i=h[f.camelCase(c)])):i=h;return i}},removeData:function(a,b,c){if(!!f.acceptData(a)){var d,e,g,h=f.expando,i=a.nodeType,j=i?f.cache:a,k=i?a[h]:h;if(!j[k])return;if(b){d=c?j[k]:j[k].data;if(d){f.isArray(b)||(b in d?b=[b]:(b=f.camelCase(b),b in d?b=[b]:b=b.split(\" \")));for(e=0,g=b.length;e<g;e++)delete d[b[e]];if(!(c?m:f.isEmptyObject)(d))return}}if(!c){delete j[k].data;if(!m(j[k]))return}f.support.deleteExpando||!j.setInterval?delete j[k]:j[k]=null,i&&(f.support.deleteExpando?delete a[h]:a.removeAttribute?a.removeAttribute(h):a[h]=null)}},_data:function(a,b,c){return f.data(a,b,c,!0)},acceptData:function(a){if(a.nodeName){var b=f.noData[a.nodeName.toLowerCase()];if(b)return b!==!0&&a.getAttribute(\"classid\")===b}return!0}}),f.fn.extend({data:function(a,c){var d,e,g,h=null;if(typeof a==\"undefined\"){if(this.length){h=f.data(this[0]);if(this[0].nodeType===1&&!f._data(this[0],\"parsedAttrs\")){e=this[0].attributes;for(var i=0,j=e.length;i<j;i++)g=e[i].name,g.indexOf(\"data-\")===0&&(g=f.camelCase(g.substring(5)),l(this[0],g,h[g]));f._data(this[0],\"parsedAttrs\",!0)}}return h}if(typeof a==\"object\")return this.each(function(){f.data(this,a)});d=a.split(\".\"),d[1]=d[1]?\".\"+d[1]:\"\";if(c===b){h=this.triggerHandler(\"getData\"+d[1]+\"!\",[d[0]]),h===b&&this.length&&(h=f.data(this[0],a),h=l(this[0],a,h));return h===b&&d[1]?this.data(d[0]):h}return this.each(function(){var b=f(this),e=[d[0],c];b.triggerHandler(\"setData\"+d[1]+\"!\",e),f.data(this,a,c),b.triggerHandler(\"changeData\"+d[1]+\"!\",e)})},removeData:function(a){return this.each(function(){f.removeData(this,a)})}}),f.extend({_mark:function(a,b){a&&(b=(b||\"fx\")+\"mark\",f._data(a,b,(f._data(a,b)||0)+1))},_unmark:function(a,b,c){a!==!0&&(c=b,b=a,a=!1);if(b){c=c||\"fx\";var d=c+\"mark\",e=a?0:(f._data(b,d)||1)-1;e?f._data(b,d,e):(f.removeData(b,d,!0),n(b,c,\"mark\"))}},queue:function(a,b,c){var d;if(a){b=(b||\"fx\")+\"queue\",d=f._data(a,b),c&&(!d||f.isArray(c)?d=f._data(a,b,f.makeArray(c)):d.push(c));return d||[]}},dequeue:function(a,b){b=b||\"fx\";var c=f.queue(a,b),d=c.shift(),e={};d===\"inprogress\"&&(d=c.shift()),d&&(b===\"fx\"&&c.unshift(\"inprogress\"),f._data(a,b+\".run\",e),d.call(a,function(){f.dequeue(a,b)},e)),c.length||(f.removeData(a,b+\"queue \"+b+\".run\",!0),n(a,b,\"queue\"))}}),f.fn.extend({queue:function(a,c){typeof a!=\"string\"&&(c=a,a=\"fx\");if(c===b)return f.queue(this[0],a);return this.each(function(){var b=f.queue(this,a,c);a===\"fx\"&&b[0]!==\"inprogress\"&&f.dequeue(this,a)})},dequeue:function(a){return this.each(function(){f.dequeue(this,a)})},delay:function(a,b){a=f.fx?f.fx.speeds[a]||a:a,b=b||\"fx\";return this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},clearQueue:function(a){return this.queue(a||\"fx\",[])},promise:function(a,c){function m(){--h||d.resolveWith(e,[e])}typeof a!=\"string\"&&(c=a,a=b),a=a||\"fx\";var d=f.Deferred(),e=this,g=e.length,h=1,i=a+\"defer\",j=a+\"queue\",k=a+\"mark\",l;while(g--)if(l=f.data(e[g],i,b,!0)||(f.data(e[g],j,b,!0)||f.data(e[g],k,b,!0))&&f.data(e[g],i,f.Callbacks(\"once memory\"),!0))h++,l.add(m);m();return d.promise()}});var o=/[\\n\\t\\r]/g,p=/\\s+/,q=/\\r/g,r=/^(?:button|input)$/i,s=/^(?:button|input|object|select|textarea)$/i,t=/^a(?:rea)?$/i,u=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,v=f.support.getSetAttribute,w,x,y;f.fn.extend({attr:function(a,b){return f.access(this,a,b,!0,f.attr)},removeAttr:function(a){return this.each(function(){f.removeAttr(this,a)})},prop:function(a,b){return f.access(this,a,b,!0,f.prop)},removeProp:function(a){a=f.propFix[a]||a;return this.each(function(){try{this[a]=b,delete this[a]}catch(c){}})},addClass:function(a){var b,c,d,e,g,h,i;if(f.isFunction(a))return this.each(function(b){f(this).addClass(a.call(this,b,this.className))});if(a&&typeof a==\"string\"){b=a.split(p);for(c=0,d=this.length;c<d;c++){e=this[c];if(e.nodeType===1)if(!e.className&&b.length===1)e.className=a;else{g=\" \"+e.className+\" \";for(h=0,i=b.length;h<i;h++)~g.indexOf(\" \"+b[h]+\" \")||(g+=b[h]+\" \");e.className=f.trim(g)}}}return this},removeClass:function(a){var c,d,e,g,h,i,j;if(f.isFunction(a))return this.each(function(b){f(this).removeClass(a.call(this,b,this.className))});if(a&&typeof a==\"string\"||a===b){c=(a||\"\").split(p);for(d=0,e=this.length;d<e;d++){g=this[d];if(g.nodeType===1&&g.className)if(a){h=(\" \"+g.className+\" \").replace(o,\" \");for(i=0,j=c.length;i<j;i++)h=h.replace(\" \"+c[i]+\" \",\" \");g.className=f.trim(h)}else g.className=\"\"}}return this},toggleClass:function(a,b){var c=typeof a,d=typeof b==\"boolean\";if(f.isFunction(a))return this.each(function(c){f(this).toggleClass(a.call(this,c,this.className,b),b)});return this.each(function(){if(c===\"string\"){var e,g=0,h=f(this),i=b,j=a.split(p);while(e=j[g++])i=d?i:!h.hasClass(e),h[i?\"addClass\":\"removeClass\"](e)}else if(c===\"undefined\"||c===\"boolean\")this.className&&f._data(this,\"__className__\",this.className),this.className=this.className||a===!1?\"\":f._data(this,\"__className__\")||\"\"})},hasClass:function(a){var b=\" \"+a+\" \",c=0,d=this.length;for(;c<d;c++)if(this[c].nodeType===1&&(\" \"+this[c].className+\" \").replace(o,\" \").indexOf(b)>-1)return!0;return!1},val:function(a){var c,d,e,g=this[0];{if(!!arguments.length){e=f.isFunction(a);return this.each(function(d){var g=f(this),h;if(this.nodeType===1){e?h=a.call(this,d,g.val()):h=a,h==null?h=\"\":typeof h==\"number\"?h+=\"\":f.isArray(h)&&(h=f.map(h,function(a){return a==null?\"\":a+\"\"})),c=f.valHooks[this.nodeName.toLowerCase()]||f.valHooks[this.type];if(!c||!(\"set\"in c)||c.set(this,h,\"value\")===b)this.value=h}})}if(g){c=f.valHooks[g.nodeName.toLowerCase()]||f.valHooks[g.type];if(c&&\"get\"in c&&(d=c.get(g,\"value\"))!==b)return d;d=g.value;return typeof d==\"string\"?d.replace(q,\"\"):d==null?\"\":d}}}}),f.extend({valHooks:{option:{get:function(a){var b=a.attributes.value;return!b||b.specified?a.value:a.text}},select:{get:function(a){var b,c,d,e,g=a.selectedIndex,h=[],i=a.options,j=a.type===\"select-one\";if(g<0)return null;c=j?g:0,d=j?g+1:i.length;for(;c<d;c++){e=i[c];if(e.selected&&(f.support.optDisabled?!e.disabled:e.getAttribute(\"disabled\")===null)&&(!e.parentNode.disabled||!f.nodeName(e.parentNode,\"optgroup\"))){b=f(e).val();if(j)return b;h.push(b)}}if(j&&!h.length&&i.length)return f(i[g]).val();return h},set:function(a,b){var c=f.makeArray(b);f(a).find(\"option\").each(function(){this.selected=f.inArray(f(this).val(),c)>=0}),c.length||(a.selectedIndex=-1);return c}}},attrFn:{val:!0,css:!0,html:!0,text:!0,data:!0,width:!0,height:!0,offset:!0},attr:function(a,c,d,e){var g,h,i,j=a.nodeType;if(!!a&&j!==3&&j!==8&&j!==2){if(e&&c in f.attrFn)return f(a)[c](d);if(typeof a.getAttribute==\"undefined\")return f.prop(a,c,d);i=j!==1||!f.isXMLDoc(a),i&&(c=c.toLowerCase(),h=f.attrHooks[c]||(u.test(c)?x:w));if(d!==b){if(d===null){f.removeAttr(a,c);return}if(h&&\"set\"in h&&i&&(g=h.set(a,d,c))!==b)return g;a.setAttribute(c,\"\"+d);return d}if(h&&\"get\"in h&&i&&(g=h.get(a,c))!==null)return g;g=a.getAttribute(c);return g===null?b:g}},removeAttr:function(a,b){var c,d,e,g,h=0;if(b&&a.nodeType===1){d=b.toLowerCase().split(p),g=d.length;for(;h<g;h++)e=d[h],e&&(c=f.propFix[e]||e,f.attr(a,e,\"\"),a.removeAttribute(v?e:c),u.test(e)&&c in a&&(a[c]=!1))}},attrHooks:{type:{set:function(a,b){if(r.test(a.nodeName)&&a.parentNode)f.error(\"type property can't be changed\");else if(!f.support.radioValue&&b===\"radio\"&&f.nodeName(a,\"input\")){var c=a.value;a.setAttribute(\"type\",b),c&&(a.value=c);return b}}},value:{get:function(a,b){if(w&&f.nodeName(a,\"button\"))return w.get(a,b);return b in a?a.value:null},set:function(a,b,c){if(w&&f.nodeName(a,\"button\"))return w.set(a,b,c);a.value=b}}},propFix:{tabindex:\"tabIndex\",readonly:\"readOnly\",\"for\":\"htmlFor\",\"class\":\"className\",maxlength:\"maxLength\",cellspacing:\"cellSpacing\",cellpadding:\"cellPadding\",rowspan:\"rowSpan\",colspan:\"colSpan\",usemap:\"useMap\",frameborder:\"frameBorder\",contenteditable:\"contentEditable\"},prop:function(a,c,d){var e,g,h,i=a.nodeType;if(!!a&&i!==3&&i!==8&&i!==2){h=i!==1||!f.isXMLDoc(a),h&&(c=f.propFix[c]||c,g=f.propHooks[c]);return d!==b?g&&\"set\"in g&&(e=g.set(a,d,c))!==b?e:a[c]=d:g&&\"get\"in g&&(e=g.get(a,c))!==null?e:a[c]}},propHooks:{tabIndex:{get:function(a){var c=a.getAttributeNode(\"tabindex\");return c&&c.specified?parseInt(c.value,10):s.test(a.nodeName)||t.test(a.nodeName)&&a.href?0:b}}}}),f.attrHooks.tabindex=f.propHooks.tabIndex,x={get:function(a,c){var d,e=f.prop(a,c);return e===!0||typeof e!=\"boolean\"&&(d=a.getAttributeNode(c))&&d.nodeValue!==!1?c.toLowerCase():b},set:function(a,b,c){var d;b===!1?f.removeAttr(a,c):(d=f.propFix[c]||c,d in a&&(a[d]=!0),a.setAttribute(c,c.toLowerCase()));return c}},v||(y={name:!0,id:!0},w=f.valHooks.button={get:function(a,c){var d;d=a.getAttributeNode(c);return d&&(y[c]?d.nodeValue!==\"\":d.specified)?d.nodeValue:b},set:function(a,b,d){var e=a.getAttributeNode(d);e||(e=c.createAttribute(d),a.setAttributeNode(e));return e.nodeValue=b+\"\"}},f.attrHooks.tabindex.set=w.set,f.each([\"width\",\"height\"],function(a,b){f.attrHooks[b]=f.extend(f.attrHooks[b],{set:function(a,c){if(c===\"\"){a.setAttribute(b,\"auto\");return c}}})}),f.attrHooks.contenteditable={get:w.get,set:function(a,b,c){b===\"\"&&(b=\"false\"),w.set(a,b,c)}}),f.support.hrefNormalized||f.each([\"href\",\"src\",\"width\",\"height\"],function(a,c){f.attrHooks[c]=f.extend(f.attrHooks[c],{get:function(a){var d=a.getAttribute(c,2);return d===null?b:d}})}),f.support.style||(f.attrHooks.style={get:function(a){return a.style.cssText.toLowerCase()||b},set:function(a,b){return a.style.cssText=\"\"+b}}),f.support.optSelected||(f.propHooks.selected=f.extend(f.propHooks.selected,{get:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex);return null}})),f.support.enctype||(f.propFix.enctype=\"encoding\"),f.support.checkOn||f.each([\"radio\",\"checkbox\"],function(){f.valHooks[this]={get:function(a){return a.getAttribute(\"value\")===null?\"on\":a.value}}}),f.each([\"radio\",\"checkbox\"],function(){f.valHooks[this]=f.extend(f.valHooks[this],{set:function(a,b){if(f.isArray(b))return a.checked=f.inArray(f(a).val(),b)>=0}})});var z=/^(?:textarea|input|select)$/i,A=/^([^\\.]*)?(?:\\.(.+))?$/,B=/\\bhover(\\.\\S+)?\\b/,C=/^key/,D=/^(?:mouse|contextmenu)|click/,E=/^(?:focusinfocus|focusoutblur)$/,F=/^(\\w*)(?:#([\\w\\-]+))?(?:\\.([\\w\\-]+))?$/,G=function(a){var b=F.exec(a);b&&(b[1]=(b[1]||\"\").toLowerCase(),b[3]=b[3]&&new RegExp(\"(?:^|\\\\s)\"+b[3]+\"(?:\\\\s|$)\"));return b},H=function(a,b){var c=a.attributes||{};return(!b[1]||a.nodeName.toLowerCase()===b[1])&&(!b[2]||(c.id||{}).value===b[2])&&(!b[3]||b[3].test((c[\"class\"]||{}).value))},I=function(a){return f.event.special.hover?a:a.replace(B,\"mouseenter$1 mouseleave$1\")};\n"
;

static const char search_jquery_script2[]=
// #include "jquery_p2_js.h"
"f.event={add:function(a,c,d,e,g){var h,i,j,k,l,m,n,o,p,q,r,s;if(!(a.nodeType===3||a.nodeType===8||!c||!d||!(h=f._data(a)))){d.handler&&(p=d,d=p.handler),d.guid||(d.guid=f.guid++),j=h.events,j||(h.events=j={}),i=h.handle,i||(h.handle=i=function(a){return typeof f!=\"undefined\"&&(!a||f.event.triggered!==a.type)?f.event.dispatch.apply(i.elem,arguments):b},i.elem=a),c=f.trim(I(c)).split(\" \");for(k=0;k<c.length;k++){l=A.exec(c[k])||[],m=l[1],n=(l[2]||\"\").split(\".\").sort(),s=f.event.special[m]||{},m=(g?s.delegateType:s.bindType)||m,s=f.event.special[m]||{},o=f.extend({type:m,origType:l[1],data:e,handler:d,guid:d.guid,selector:g,quick:G(g),namespace:n.join(\".\")},p),r=j[m];if(!r){r=j[m]=[],r.delegateCount=0;if(!s.setup||s.setup.call(a,e,n,i)===!1)a.addEventListener?a.addEventListener(m,i,!1):a.attachEvent&&a.attachEvent(\"on\"+m,i)}s.add&&(s.add.call(a,o),o.handler.guid||(o.handler.guid=d.guid)),g?r.splice(r.delegateCount++,0,o):r.push(o),f.event.global[m]=!0}a=null}},global:{},remove:function(a,b,c,d,e){var g=f.hasData(a)&&f._data(a),h,i,j,k,l,m,n,o,p,q,r,s;if(!!g&&!!(o=g.events)){b=f.trim(I(b||\"\")).split(\" \");for(h=0;h<b.length;h++){i=A.exec(b[h])||[],j=k=i[1],l=i[2];if(!j){for(j in o)f.event.remove(a,j+b[h],c,d,!0);continue}p=f.event.special[j]||{},j=(d?p.delegateType:p.bindType)||j,r=o[j]||[],m=r.length,l=l?new RegExp(\"(^|\\\\.)\"+l.split(\".\").sort().join(\"\\\\.(?:.*\\\\.)?\")+\"(\\\\.|$)\"):null;for(n=0;n<r.length;n++)s=r[n],(e||k===s.origType)&&(!c||c.guid===s.guid)&&(!l||l.test(s.namespace))&&(!d||d===s.selector||d===\"**\"&&s.selector)&&(r.splice(n--,1),s.selector&&r.delegateCount--,p.remove&&p.remove.call(a,s));r.length===0&&m!==r.length&&((!p.teardown||p.teardown.call(a,l)===!1)&&f.removeEvent(a,j,g.handle),delete o[j])}f.isEmptyObject(o)&&(q=g.handle,q&&(q.elem=null),f.removeData(a,[\"events\",\"handle\"],!0))}},customEvent:{getData:!0,setData:!0,changeData:!0},trigger:function(c,d,e,g){if(!e||e.nodeType!==3&&e.nodeType!==8){var h=c.type||c,i=[],j,k,l,m,n,o,p,q,r,s;if(E.test(h+f.event.triggered))return;h.indexOf(\"!\")>=0&&(h=h.slice(0,-1),k=!0),h.indexOf(\".\")>=0&&(i=h.split(\".\"),h=i.shift(),i.sort());if((!e||f.event.customEvent[h])&&!f.event.global[h])return;c=typeof c==\"object\"?c[f.expando]?c:new f.Event(h,c):new f.Event(h),c.type=h,c.isTrigger=!0,c.exclusive=k,c.namespace=i.join(\".\"),c.namespace_re=c.namespace?new RegExp(\"(^|\\\\.)\"+i.join(\"\\\\.(?:.*\\\\.)?\")+\"(\\\\.|$)\"):null,o=h.indexOf(\":\")<0?\"on\"+h:\"\";if(!e){j=f.cache;for(l in j)j[l].events&&j[l].events[h]&&f.event.trigger(c,d,j[l].handle.elem,!0);return}c.result=b,c.target||(c.target=e),d=d!=null?f.makeArray(d):[],d.unshift(c),p=f.event.special[h]||{};if(p.trigger&&p.trigger.apply(e,d)===!1)return;r=[[e,p.bindType||h]];if(!g&&!p.noBubble&&!f.isWindow(e)){s=p.delegateType||h,m=E.test(s+h)?e:e.parentNode,n=null;for(;m;m=m.parentNode)r.push([m,s]),n=m;n&&n===e.ownerDocument&&r.push([n.defaultView||n.parentWindow||a,s])}for(l=0;l<r.length&&!c.isPropagationStopped();l++)m=r[l][0],c.type=r[l][1],q=(f._data(m,\"events\")||{})[c.type]&&f._data(m,\"handle\"),q&&q.apply(m,d),q=o&&m[o],q&&f.acceptData(m)&&q.apply(m,d)===!1&&c.preventDefault();c.type=h,!g&&!c.isDefaultPrevented()&&(!p._default||p._default.apply(e.ownerDocument,d)===!1)&&(h!==\"click\"||!f.nodeName(e,\"a\"))&&f.acceptData(e)&&o&&e[h]&&(h!==\"focus\"&&h!==\"blur\"||c.target.offsetWidth!==0)&&!f.isWindow(e)&&(n=e[o],n&&(e[o]=null),f.event.triggered=h,e[h](),f.event.triggered=b,n&&(e[o]=n));return c.result}},dispatch:function(c){c=f.event.fix(c||a.event);var d=(f._data(this,\"events\")||{})[c.type]||[],e=d.delegateCount,g=[].slice.call(arguments,0),h=!c.exclusive&&!c.namespace,i=[],j,k,l,m,n,o,p,q,r,s,t;g[0]=c,c.delegateTarget=this;if(e&&!c.target.disabled&&(!c.button||c.type!==\"click\")){m=f(this),m.context=this.ownerDocument||this;for(l=c.target;l!=this;l=l.parentNode||this){o={},q=[],m[0]=l;for(j=0;j<e;j++)r=d[j],s=r.selector,o[s]===b&&(o[s]=r.quick?H(l,r.quick):m.is(s)),o[s]&&q.push(r);q.length&&i.push({elem:l,matches:q})}}d.length>e&&i.push({elem:this,matches:d.slice(e)});for(j=0;j<i.length&&!c.isPropagationStopped();j++){p=i[j],c.currentTarget=p.elem;for(k=0;k<p.matches.length&&!c.isImmediatePropagationStopped();k++){r=p.matches[k];if(h||!c.namespace&&!r.namespace||c.namespace_re&&c.namespace_re.test(r.namespace))c.data=r.data,c.handleObj=r,n=((f.event.special[r.origType]||{}).handle||r.handler).apply(p.elem,g),n!==b&&(c.result=n,n===!1&&(c.preventDefault(),c.stopPropagation()))}}return c.result},props:\"attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which\".split(\" \"),fixHooks:{},keyHooks:{props:\"char charCode key keyCode\".split(\" \"),filter:function(a,b){a.which==null&&(a.which=b.charCode!=null?b.charCode:b.keyCode);return a}},mouseHooks:{props:\"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement\".split(\" \"),filter:function(a,d){var e,f,g,h=d.button,i=d.fromElement;a.pageX==null&&d.clientX!=null&&(e=a.target.ownerDocument||c,f=e.documentElement,g=e.body,a.pageX=d.clientX+(f&&f.scrollLeft||g&&g.scrollLeft||0)-(f&&f.clientLeft||g&&g.clientLeft||0),a.pageY=d.clientY+(f&&f.scrollTop||g&&g.scrollTop||0)-(f&&f.clientTop||g&&g.clientTop||0)),!a.relatedTarget&&i&&(a.relatedTarget=i===a.target?d.toElement:i),!a.which&&h!==b&&(a.which=h&1?1:h&2?3:h&4?2:0);return a}},fix:function(a){if(a[f.expando])return a;var d,e,g=a,h=f.event.fixHooks[a.type]||{},i=h.props?this.props.concat(h.props):this.props;a=f.Event(g);for(d=i.length;d;)e=i[--d],a[e]=g[e];a.target||(a.target=g.srcElement||c),a.target.nodeType===3&&(a.target=a.target.parentNode),a.metaKey===b&&(a.metaKey=a.ctrlKey);return h.filter?h.filter(a,g):a},special:{ready:{setup:f.bindReady},load:{noBubble:!0},focus:{delegateType:\"focusin\"},blur:{delegateType:\"focusout\"},beforeunload:{setup:function(a,b,c){f.isWindow(this)&&(this.onbeforeunload=c)},teardown:function(a,b){this.onbeforeunload===b&&(this.onbeforeunload=null)}}},simulate:function(a,b,c,d){var e=f.extend(new f.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?f.event.trigger(e,null,b):f.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},f.event.handle=f.event.dispatch,f.removeEvent=c.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){a.detachEvent&&a.detachEvent(\"on\"+b,c)},f.Event=function(a,b){if(!(this instanceof f.Event))return new f.Event(a,b);a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||a.returnValue===!1||a.getPreventDefault&&a.getPreventDefault()?K:J):this.type=a,b&&f.extend(this,b),this.timeStamp=a&&a.timeStamp||f.now(),this[f.expando]=!0},f.Event.prototype={preventDefault:function(){this.isDefaultPrevented=K;var a=this.originalEvent;!a||(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){this.isPropagationStopped=K;var a=this.originalEvent;!a||(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=K,this.stopPropagation()},isDefaultPrevented:J,isPropagationStopped:J,isImmediatePropagationStopped:J},f.each({mouseenter:\"mouseover\",mouseleave:\"mouseout\"},function(a,b){f.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c=this,d=a.relatedTarget,e=a.handleObj,g=e.selector,h;if(!d||d!==c&&!f.contains(c,d))a.type=e.origType,h=e.handler.apply(this,arguments),a.type=b;return h}}}),f.support.submitBubbles||(f.event.special.submit={setup:function(){if(f.nodeName(this,\"form\"))return!1;f.event.add(this,\"click._submit keypress._submit\",function(a){var c=a.target,d=f.nodeName(c,\"input\")||f.nodeName(c,\"button\")?c.form:b;d&&!d._submit_attached&&(f.event.add(d,\"submit._submit\",function(a){this.parentNode&&!a.isTrigger&&f.event.simulate(\"submit\",this.parentNode,a,!0)}),d._submit_attached=!0)})},teardown:function(){if(f.nodeName(this,\"form\"))return!1;f.event.remove(this,\"._submit\")}}),f.support.changeBubbles||(f.event.special.change={setup:function(){if(z.test(this.nodeName)){if(this.type===\"checkbox\"||this.type===\"radio\")f.event.add(this,\"propertychange._change\",function(a){a.originalEvent.propertyName===\"checked\"&&(this._just_changed=!0)}),f.event.add(this,\"click._change\",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1,f.event.simulate(\"change\",this,a,!0))});return!1}f.event.add(this,\"beforeactivate._change\",function(a){var b=a.target;z.test(b.nodeName)&&!b._change_attached&&(f.event.add(b,\"change._change\",function(a){this.parentNode&&!a.isSimulated&&!a.isTrigger&&f.event.simulate(\"change\",this.parentNode,a,!0)}),b._change_attached=!0)})},handle:function(a){var b=a.target;if(this!==b||a.isSimulated||a.isTrigger||b.type!==\"radio\"&&b.type!==\"checkbox\")return a.handleObj.handler.apply(this,arguments)},teardown:function(){f.event.remove(this,\"._change\");return z.test(this.nodeName)}}),f.support.focusinBubbles||f.each({focus:\"focusin\",blur:\"focusout\"},function(a,b){var d=0,e=function(a){f.event.simulate(b,a.target,f.event.fix(a),!0)};f.event.special[b]={setup:function(){d++===0&&c.addEventListener(a,e,!0)},teardown:function(){--d===0&&c.removeEventListener(a,e,!0)}}}),f.fn.extend({on:function(a,c,d,e,g){var h,i;if(typeof a==\"object\"){typeof c!=\"string\"&&(d=c,c=b);for(i in a)this.on(i,c,d,a[i],g);return this}d==null&&e==null?(e=c,d=c=b):e==null&&(typeof c==\"string\"?(e=d,d=b):(e=d,d=c,c=b));if(e===!1)e=J;else if(!e)return this;g===1&&(h=e,e=function(a){f().off(a);return h.apply(this,arguments)},e.guid=h.guid||(h.guid=f.guid++));return this.each(function(){f.event.add(this,a,e,d,c)})},one:function(a,b,c,d){return this.on.call(this,a,b,c,d,1)},off:function(a,c,d){if(a&&a.preventDefault&&a.handleObj){var e=a.handleObj;f(a.delegateTarget).off(e.namespace?e.type+\".\"+e.namespace:e.type,e.selector,e.handler);return this}if(typeof a==\"object\"){for(var g in a)this.off(g,c,a[g]);return this}if(c===!1||typeof c==\"function\")d=c,c=b;d===!1&&(d=J);return this.each(function(){f.event.remove(this,a,d,c)})},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},live:function(a,b,c){f(this.context).on(a,this.selector,b,c);return this},die:function(a,b){f(this.context).off(a,this.selector||\"**\",b);return this},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return arguments.length==1?this.off(a,\"**\"):this.off(b,a,c)},trigger:function(a,b){return this.each(function(){f.event.trigger(a,b,this)})},triggerHandler:function(a,b){if(this[0])return f.event.trigger(a,b,this[0],!0)},toggle:function(a){var b=arguments,c=a.guid||f.guid++,d=0,e=function(c){var e=(f._data(this,\"lastToggle\"+a.guid)||0)%d;f._data(this,\"lastToggle\"+a.guid,e+1),c.preventDefault();return b[e].apply(this,arguments)||!1};e.guid=c;while(d<b.length)b[d++].guid=c;return this.click(e)},hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),f.each(\"blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu\".split(\" \"),function(a,b){f.fn[b]=function(a,c){c==null&&(c=a,a=null);return arguments.length>0?this.on(b,null,a,c):this.trigger(b)},f.attrFn&&(f.attrFn[b]=!0),C.test(b)&&(f.event.fixHooks[b]=f.event.keyHooks),D.test(b)&&(f.event.fixHooks[b]=f.event.mouseHooks)}),function(){function x(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}if(j.nodeType===1){g||(j[d]=c,j.sizset=h);if(typeof b!=\"string\"){if(j===b){k=!0;break}}else if(m.filter(b,[j]).length>0){k=j;break}}j=j[a]}e[h]=k}}}function w(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}j.nodeType===1&&!g&&(j[d]=c,j.sizset=h);if(j.nodeName.toLowerCase()===b){k=j;break}j=j[a]}e[h]=k}}}var a=/((?:\\((?:\\([^()]+\\)|[^()]+)+\\)|\\[(?:\\[[^\\[\\]]*\\]|['\"][^'\"]*['\"]|[^\\[\\]'\"]+)+\\]|\\\\.|[^ >+~,(\\[\\\\]+)+|[>+~])(\\s*,\\s*)?((?:.|\\r|\\n)*)/g,d=\"sizcache\"+(Math.random()+\"\").replace(\".\",\"\"),e=0,g=Object.prototype.toString,h=!1,i=!0,j=/\\\\/g,k=/\\r\\n/g,l=/\\W/;[0,0].sort(function(){i=!1;return 0});var m=function(b,d,e,f){e=e||[],d=d||c;var h=d;if(d.nodeType!==1&&d.nodeType!==9)return[];if(!b||typeof b!=\"string\")return e;var i,j,k,l,n,q,r,t,u=!0,v=m.isXML(d),w=[],x=b;do{a.exec(\"\"),i=a.exec(x);if(i){x=i[3],w.push(i[1]);if(i[2]){l=i[3];break}}}while(i);if(w.length>1&&p.exec(b))if(w.length===2&&o.relative[w[0]])j=y(w[0]+w[1],d,f);else{j=o.relative[w[0]]?[d]:m(w.shift(),d);while(w.length)b=w.shift(),o.relative[b]&&(b+=w.shift()),j=y(b,j,f)}else{!f&&w.length>1&&d.nodeType===9&&!v&&o.match.ID.test(w[0])&&!o.match.ID.test(w[w.length-1])&&(n=m.find(w.shift(),d,v),d=n.expr?m.filter(n.expr,n.set)[0]:n.set[0]);if(d){n=f?{expr:w.pop(),set:s(f)}:m.find(w.pop(),w.length===1&&(w[0]===\"~\"||w[0]===\"+\")&&d.parentNode?d.parentNode:d,v),j=n.expr?m.filter(n.expr,n.set):n.set,w.length>0?k=s(j):u=!1;while(w.length)q=w.pop(),r=q,o.relative[q]?r=w.pop():q=\"\",r==null&&(r=d),o.relative[q](k,r,v)}else k=w=[]}k||(k=j),k||m.error(q||b);if(g.call(k)===\"[object Array]\")if(!u)e.push.apply(e,k);else if(d&&d.nodeType===1)for(t=0;k[t]!=null;t++)k[t]&&(k[t]===!0||k[t].nodeType===1&&m.contains(d,k[t]))&&e.push(j[t]);else for(t=0;k[t]!=null;t++)k[t]&&k[t].nodeType===1&&e.push(j[t]);else s(k,e);l&&(m(l,h,e,f),m.uniqueSort(e));return e};m.uniqueSort=function(a){if(u){h=i,a.sort(u);if(h)for(var b=1;b<a.length;b++)a[b]===a[b-1]&&a.splice(b--,1)}return a},m.matches=function(a,b){return m(a,null,null,b)},m.matchesSelector=function(a,b){return m(b,null,null,[a]).length>0},m.find=function(a,b,c){var d,e,f,g,h,i;if(!a)return[];for(e=0,f=o.order.length;e<f;e++){h=o.order[e];if(g=o.leftMatch[h].exec(a)){i=g[1],g.splice(1,1);if(i.substr(i.length-1)!==\"\\\\\"){g[1]=(g[1]||\"\").replace(j,\"\"),d=o.find[h](g,b,c);if(d!=null){a=a.replace(o.match[h],\"\");break}}}}d||(d=typeof b.getElementsByTagName!=\"undefined\"?b.getElementsByTagName(\"*\"):[]);return{set:d,expr:a}},m.filter=function(a,c,d,e){var f,g,h,i,j,k,l,n,p,q=a,r=[],s=c,t=c&&c[0]&&m.isXML(c[0]);while(a&&c.length){for(h in o.filter)if((f=o.leftMatch[h].exec(a))!=null&&f[2]){k=o.filter[h],l=f[1],g=!1,f.splice(1,1);if(l.substr(l.length-1)===\"\\\\\")continue;s===r&&(r=[]);if(o.preFilter[h]){f=o.preFilter[h](f,s,d,r,e,t);if(!f)g=i=!0;else if(f===!0)continue}if(f)for(n=0;(j=s[n])!=null;n++)j&&(i=k(j,f,n,s),p=e^i,d&&i!=null?p?g=!0:s[n]=!1:p&&(r.push(j),g=!0));if(i!==b){d||(s=r),a=a.replace(o.match[h],\"\");if(!g)return[];break}}if(a===q)if(g==null)m.error(a);else break;q=a}return s},m.error=function(a){throw new Error(\"Syntax error, unrecognized expression: \"+a)};var n=m.getText=function(a){var b,c,d=a.nodeType,e=\"\";if(d){if(d===1||d===9){if(typeof a.textContent==\"string\")return a.textContent;if(typeof a.innerText==\"string\")return a.innerText.replace(k,\"\");for(a=a.firstChild;a;a=a.nextSibling)e+=n(a)}else if(d===3||d===4)return a.nodeValue}else for(b=0;c=a[b];b++)c.nodeType!==8&&(e+=n(c));return e},o=m.selectors={order:[\"ID\",\"NAME\",\"TAG\"],match:{ID:/#((?:[\\w\\u00c0-\\uFFFF\\-]|\\\\.)+)/,CLASS:/\\.((?:[\\w\\u00c0-\\uFFFF\\-]|\\\\.)+)/,NAME:/\\[name=['\"]*((?:[\\w\\u00c0-\\uFFFF\\-]|\\\\.)+)['\"]*\\]/,ATTR:/\\[\\s*((?:[\\w\\u00c0-\\uFFFF\\-]|\\\\.)+)\\s*(?:(\\S?=)\\s*(?:(['\"])(.*?)\\3|(#?(?:[\\w\\u00c0-\\uFFFF\\-]|\\\\.)*)|)|)\\s*\\]/,TAG:/^((?:[\\w\\u00c0-\\uFFFF\\*\\-]|\\\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\\(\\s*(even|odd|(?:[+\\-]?\\d+|(?:[+\\-]?\\d*)?n\\s*(?:[+\\-]\\s*\\d+)?))\\s*\\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\\((\\d*)\\))?(?=[^\\-]|$)/,PSEUDO:/:((?:[\\w\\u00c0-\\uFFFF\\-]|\\\\.)+)(?:\\((['\"]?)((?:\\([^\\)]+\\)|[^\\(\\)]*)+)\\2\\))?/},leftMatch:{},attrMap:{\"class\":\"className\",\"for\":\"htmlFor\"},attrHandle:{href:function(a){return a.getAttribute(\"href\")},type:function(a){return a.getAttribute(\"type\")}},relative:{\"+\":function(a,b){var c=typeof b==\"string\",d=c&&!l.test(b),e=c&&!d;d&&(b=b.toLowerCase());for(var f=0,g=a.length,h;f<g;f++)if(h=a[f]){while((h=h.previousSibling)&&h.nodeType!==1);a[f]=e||h&&h.nodeName.toLowerCase()===b?h||!1:h===b}e&&m.filter(b,a,!0)},\">\":function(a,b){var c,d=typeof b==\"string\",e=0,f=a.length;if(d&&!l.test(b)){b=b.toLowerCase();for(;e<f;e++){c=a[e];if(c){var g=c.parentNode;a[e]=g.nodeName.toLowerCase()===b?g:!1}}}else{for(;e<f;e++)c=a[e],c&&(a[e]=d?c.parentNode:c.parentNode===b);d\n"
"&&m.filter(b,a,!0)}},\"\":function(a,b,c){var d,f=e++,g=x;typeof b==\"string\"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g(\"parentNode\",b,f,a,d,c)},\"~\":function(a,b,c){var d,f=e++,g=x;typeof b==\"string\"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g(\"previousSibling\",b,f,a,d,c)}},find:{ID:function(a,b,c){if(typeof b.getElementById!=\"undefined\"&&!c){var d=b.getElementById(a[1]);return d&&d.parentNode?[d]:[]}},NAME:function(a,b){if(typeof b.getElementsByName!=\"undefined\"){var c=[],d=b.getElementsByName(a[1]);for(var e=0,f=d.length;e<f;e++)d[e].getAttribute(\"name\")===a[1]&&c.push(d[e]);return c.length===0?null:c}},TAG:function(a,b){if(typeof b.getElementsByTagName!=\"undefined\")return b.getElementsByTagName(a[1])}},preFilter:{CLASS:function(a,b,c,d,e,f){a=\" \"+a[1].replace(j,\"\")+\" \";if(f)return a;for(var g=0,h;(h=b[g])!=null;g++)h&&(e^(h.className&&(\" \"+h.className+\" \").replace(/[\\t\\n\\r]/g,\" \").indexOf(a)>=0)?c||d.push(h):c&&(b[g]=!1));return!1},ID:function(a){return a[1].replace(j,\"\")},TAG:function(a,b){return a[1].replace(j,\"\").toLowerCase()},CHILD:function(a){if(a[1]===\"nth\"){a[2]||m.error(a[0]),a[2]=a[2].replace(/^\\+|\\s*/g,\"\");var b=/(-?)(\\d*)(?:n([+\\-]?\\d*))?/.exec(a[2]===\"even\"&&\"2n\"||a[2]===\"odd\"&&\"2n+1\"||!/\\D/.test(a[2])&&\"0n+\"+a[2]||a[2]);a[2]=b[1]+(b[2]||1)-0,a[3]=b[3]-0}else a[2]&&m.error(a[0]);a[0]=e++;return a},ATTR:function(a,b,c,d,e,f){var g=a[1]=a[1].replace(j,\"\");!f&&o.attrMap[g]&&(a[1]=o.attrMap[g]),a[4]=(a[4]||a[5]||\"\").replace(j,\"\"),a[2]===\"~=\"&&(a[4]=\" \"+a[4]+\" \");return a},PSEUDO:function(b,c,d,e,f){if(b[1]===\"not\")if((a.exec(b[3])||\"\").length>1||/^\\w/.test(b[3]))b[3]=m(b[3],null,null,c);else{var g=m.filter(b[3],c,d,!0^f);d||e.push.apply(e,g);return!1}else if(o.match.POS.test(b[0])||o.match.CHILD.test(b[0]))return!0;return b},POS:function(a){a.unshift(!0);return a}},filters:{enabled:function(a){return a.disabled===!1&&a.type!==\"hidden\"},disabled:function(a){return a.disabled===!0},checked:function(a){return a.checked===!0},selected:function(a){a.parentNode&&a.parentNode.selectedIndex;return a.selected===!0},parent:function(a){return!!a.firstChild},empty:function(a){return!a.firstChild},has:function(a,b,c){return!!m(c[3],a).length},header:function(a){return/h\\d/i.test(a.nodeName)},text:function(a){var b=a.getAttribute(\"type\"),c=a.type;return a.nodeName.toLowerCase()===\"input\"&&\"text\"===c&&(b===c||b===null)},radio:function(a){return a.nodeName.toLowerCase()===\"input\"&&\"radio\"===a.type},checkbox:function(a){return a.nodeName.toLowerCase()===\"input\"&&\"checkbox\"===a.type},file:function(a){return a.nodeName.toLowerCase()===\"input\"&&\"file\"===a.type},password:function(a){return a.nodeName.toLowerCase()===\"input\"&&\"password\"===a.type},submit:function(a){var b=a.nodeName.toLowerCase();return(b===\"input\"||b===\"button\")&&\"submit\"===a.type},image:function(a){return a.nodeName.toLowerCase()===\"input\"&&\"image\"===a.type},reset:function(a){var b=a.nodeName.toLowerCase();return(b===\"input\"||b===\"button\")&&\"reset\"===a.type},button:function(a){var b=a.nodeName.toLowerCase();return b===\"input\"&&\"button\"===a.type||b===\"button\"},input:function(a){return/input|select|textarea|button/i.test(a.nodeName)},focus:function(a){return a===a.ownerDocument.activeElement}},setFilters:{first:function(a,b){return b===0},last:function(a,b,c,d){return b===d.length-1},even:function(a,b){return b%2===0},odd:function(a,b){return b%2===1},lt:function(a,b,c){return b<c[3]-0},gt:function(a,b,c){return b>c[3]-0},nth:function(a,b,c){return c[3]-0===b},eq:function(a,b,c){return c[3]-0===b}},filter:{PSEUDO:function(a,b,c,d){var e=b[1],f=o.filters[e];if(f)return f(a,c,b,d);if(e===\"contains\")return(a.textContent||a.innerText||n([a])||\"\").indexOf(b[3])>=0;if(e===\"not\"){var g=b[3];for(var h=0,i=g.length;h<i;h++)if(g[h]===a)return!1;return!0}m.error(e)},CHILD:function(a,b){var c,e,f,g,h,i,j,k=b[1],l=a;switch(k){case\"only\":case\"first\":while(l=l.previousSibling)if(l.nodeType===1)return!1;if(k===\"first\")return!0;l=a;case\"last\":while(l=l.nextSibling)if(l.nodeType===1)return!1;return!0;case\"nth\":c=b[2],e=b[3];if(c===1&&e===0)return!0;f=b[0],g=a.parentNode;if(g&&(g[d]!==f||!a.nodeIndex)){i=0;for(l=g.firstChild;l;l=l.nextSibling)l.nodeType===1&&(l.nodeIndex=++i);g[d]=f}j=a.nodeIndex-e;return c===0?j===0:j%c===0&&j/c>=0}},ID:function(a,b){return a.nodeType===1&&a.getAttribute(\"id\")===b},TAG:function(a,b){return b===\"*\"&&a.nodeType===1||!!a.nodeName&&a.nodeName.toLowerCase()===b},CLASS:function(a,b){return(\" \"+(a.className||a.getAttribute(\"class\"))+\" \").indexOf(b)>-1},ATTR:function(a,b){var c=b[1],d=m.attr?m.attr(a,c):o.attrHandle[c]?o.attrHandle[c](a):a[c]!=null?a[c]:a.getAttribute(c),e=d+\"\",f=b[2],g=b[4];return d==null?f===\"!=\":!f&&m.attr?d!=null:f===\"=\"?e===g:f===\"*=\"?e.indexOf(g)>=0:f===\"~=\"?(\" \"+e+\" \").indexOf(g)>=0:g?f===\"!=\"?e!==g:f===\"^=\"?e.indexOf(g)===0:f===\"$=\"?e.substr(e.length-g.length)===g:f===\"|=\"?e===g||e.substr(0,g.length+1)===g+\"-\":!1:e&&d!==!1},POS:function(a,b,c,d){var e=b[2],f=o.setFilters[e];if(f)return f(a,c,b,d)}}},p=o.match.POS,q=function(a,b){return\"\\\\\"+(b-0+1)};for(var r in o.match)o.match[r]=new RegExp(o.match[r].source+/(?![^\\[]*\\])(?![^\\(]*\\))/.source),o.leftMatch[r]=new RegExp(/(^(?:.|\\r|\\n)*?)/.source+o.match[r].source.replace(/\\\\(\\d+)/g,q));var s=function(a,b){a=Array.prototype.slice.call(a,0);if(b){b.push.apply(b,a);return b}return a};try{Array.prototype.slice.call(c.documentElement.childNodes,0)[0].nodeType}catch(t){s=function(a,b){var c=0,d=b||[];if(g.call(a)===\"[object Array]\")Array.prototype.push.apply(d,a);else if(typeof a.length==\"number\")for(var e=a.length;c<e;c++)d.push(a[c]);else for(;a[c];c++)d.push(a[c]);return d}}var u,v;c.documentElement.compareDocumentPosition?u=function(a,b){if(a===b){h=!0;return 0}if(!a.compareDocumentPosition||!b.compareDocumentPosition)return a.compareDocumentPosition?-1:1;return a.compareDocumentPosition(b)&4?-1:1}:(u=function(a,b){if(a===b){h=!0;return 0}if(a.sourceIndex&&b.sourceIndex)return a.sourceIndex-b.sourceIndex;var c,d,e=[],f=[],g=a.parentNode,i=b.parentNode,j=g;if(g===i)return v(a,b);if(!g)return-1;if(!i)return 1;while(j)e.unshift(j),j=j.parentNode;j=i;while(j)f.unshift(j),j=j.parentNode;c=e.length,d=f.length;for(var k=0;k<c&&k<d;k++)if(e[k]!==f[k])return v(e[k],f[k]);return k===c?v(a,f[k],-1):v(e[k],b,1)},v=function(a,b,c){if(a===b)return c;var d=a.nextSibling;while(d){if(d===b)return-1;d=d.nextSibling}return 1}),function(){var a=c.createElement(\"div\"),d=\"script\"+(new Date).getTime(),e=c.documentElement;a.innerHTML=\"<a name='\"+d+\"'/>\",e.insertBefore(a,e.firstChild),c.getElementById(d)&&(o.find.ID=function(a,c,d){if(typeof c.getElementById!=\"undefined\"&&!d){var e=c.getElementById(a[1]);return e?e.id===a[1]||typeof e.getAttributeNode!=\"undefined\"&&e.getAttributeNode(\"id\").nodeValue===a[1]?[e]:b:[]}},o.filter.ID=function(a,b){var c=typeof a.getAttributeNode!=\"undefined\"&&a.getAttributeNode(\"id\");return a.nodeType===1&&c&&c.nodeValue===b}),e.removeChild(a),e=a=null}(),function(){var a=c.createElement(\"div\");a.appendChild(c.createComment(\"\")),a.getElementsByTagName(\"*\").length>0&&(o.find.TAG=function(a,b){var c=b.getElementsByTagName(a[1]);if(a[1]===\"*\"){var d=[];for(var e=0;c[e];e++)c[e].nodeType===1&&d.push(c[e]);c=d}return c}),a.innerHTML=\"<a href='#'></a>\",a.firstChild&&typeof a.firstChild.getAttribute!=\"undefined\"&&a.firstChild.getAttribute(\"href\")!==\"#\"&&(o.attrHandle.href=function(a){return a.getAttribute(\"href\",2)}),a=null}(),c.querySelectorAll&&function(){var a=m,b=c.createElement(\"div\"),d=\"__sizzle__\";b.innerHTML=\"<p class='TEST'></p>\";if(!b.querySelectorAll||b.querySelectorAll(\".TEST\").length!==0){m=function(b,e,f,g){e=e||c;if(!g&&!m.isXML(e)){var h=/^(\\w+$)|^\\.([\\w\\-]+$)|^#([\\w\\-]+$)/.exec(b);if(h&&(e.nodeType===1||e.nodeType===9)){if(h[1])return s(e.getElementsByTagName(b),f);if(h[2]&&o.find.CLASS&&e.getElementsByClassName)return s(e.getElementsByClassName(h[2]),f)}if(e.nodeType===9){if(b===\"body\"&&e.body)return s([e.body],f);if(h&&h[3]){var i=e.getElementById(h[3]);if(!i||!i.parentNode)return s([],f);if(i.id===h[3])return s([i],f)}try{return s(e.querySelectorAll(b),f)}catch(j){}}else if(e.nodeType===1&&e.nodeName.toLowerCase()!==\"object\"){var k=e,l=e.getAttribute(\"id\"),n=l||d,p=e.parentNode,q=/^\\s*[+~]/.test(b);l?n=n.replace(/'/g,\"\\\\$&\"):e.setAttribute(\"id\",n),q&&p&&(e=e.parentNode);try{if(!q||p)return s(e.querySelectorAll(\"[id='\"+n+\"'] \"+b),f)}catch(r){}finally{l||k.removeAttribute(\"id\")}}}return a(b,e,f,g)};for(var e in a)m[e]=a[e];b=null}}(),function(){var a=c.documentElement,b=a.matchesSelector||a.mozMatchesSelector||a.webkitMatchesSelector||a.msMatchesSelector;if(b){var d=!b.call(c.createElement(\"div\"),\"div\"),e=!1;try{b.call(c.documentElement,\"[test!='']:sizzle\")}catch(f){e=!0}m.matchesSelector=function(a,c){c=c.replace(/\\=\\s*([^'\"\\]]*)\\s*\\]/g,\"='$1']\");if(!m.isXML(a))try{if(e||!o.match.PSEUDO.test(c)&&!/!=/.test(c)){var f=b.call(a,c);if(f||!d||a.document&&a.document.nodeType!==11)return f}}catch(g){}return m(c,null,null,[a]).length>0}}}(),function(){var a=c.createElement(\"div\");a.innerHTML=\"<div class='test e'></div><div class='test'></div>\";if(!!a.getElementsByClassName&&a.getElementsByClassName(\"e\").length!==0){a.lastChild.className=\"e\";if(a.getElementsByClassName(\"e\").length===1)return;o.order.splice(1,0,\"CLASS\"),o.find.CLASS=function(a,b,c){if(typeof b.getElementsByClassName!=\"undefined\"&&!c)return b.getElementsByClassName(a[1])},a=null}}(),c.documentElement.contains?m.contains=function(a,b){return a!==b&&(a.contains?a.contains(b):!0)}:c.documentElement.compareDocumentPosition?m.contains=function(a,b){return!!(a.compareDocumentPosition(b)&16)}:m.contains=function(){return!1},m.isXML=function(a){var b=(a?a.ownerDocument||a:0).documentElement;return b?b.nodeName!==\"HTML\":!1};var y=function(a,b,c){var d,e=[],f=\"\",g=b.nodeType?[b]:b;while(d=o.match.PSEUDO.exec(a))f+=d[0],a=a.replace(o.match.PSEUDO,\"\");a=o.relative[a]?a+\"*\":a;for(var h=0,i=g.length;h<i;h++)m(a,g[h],e,c);return m.filter(f,e)};m.attr=f.attr,m.selectors.attrMap={},f.find=m,f.expr=m.selectors,f.expr[\":\"]=f.expr.filters,f.unique=m.uniqueSort,f.text=m.getText,f.isXMLDoc=m.isXML,f.contains=m.contains}();var L=/Until$/,M=/^(?:parents|prevUntil|prevAll)/,N=/,/,O=/^.[^:#\\[\\.,]*$/,P=Array.prototype.slice,Q=f.expr.match.POS,R={children:!0,contents:!0,next:!0,prev:!0};f.fn.extend({find:function(a){var b=this,c,d;if(typeof a!=\"string\")return f(a).filter(function(){for(c=0,d=b.length;c<d;c++)if(f.contains(b[c],this))return!0});var e=this.pushStack(\"\",\"find\",a),g,h,i;for(c=0,d=this.length;c<d;c++){g=e.length,f.find(a,this[c],e);if(c>0)for(h=g;h<e.length;h++)for(i=0;i<g;i++)if(e[i]===e[h]){e.splice(h--,1);break}}return e},has:function(a){var b=f(a);return this.filter(function(){for(var a=0,c=b.length;a<c;a++)if(f.contains(this,b[a]))return!0})},not:function(a){return this.pushStack(T(this,a,!1),\"not\",a)},filter:function(a){return this.pushStack(T(this,a,!0),\"filter\",a)},is:function(a){return!!a&&(typeof a==\"string\"?Q.test(a)?f(a,this.context).index(this[0])>=0:f.filter(a,this).length>0:this.filter(a).length>0)},closest:function(a,b){var c=[],d,e,g=this[0];if(f.isArray(a)){var h=1;while(g&&g.ownerDocument&&g!==b){for(d=0;d<a.length;d++)f(g).is(a[d])&&c.push({selector:a[d],elem:g,level:h});g=g.parentNode,h++}return c}var i=Q.test(a)||typeof a!=\"string\"?f(a,b||this.context):0;for(d=0,e=this.length;d<e;d++){g=this[d];while(g){if(i?i.index(g)>-1:f.find.matchesSelector(g,a)){c.push(g);break}g=g.parentNode;if(!g||!g.ownerDocument||g===b||g.nodeType===11)break}}c=c.length>1?f.unique(c):c;return this.pushStack(c,\"closest\",a)},index:function(a){if(!a)return this[0]&&this[0].parentNode?this.prevAll().length:-1;if(typeof a==\"string\")return f.inArray(this[0],f(a));return f.inArray(a.jquery?a[0]:a,this)},add:function(a,b){var c=typeof a==\"string\"?f(a,b):f.makeArray(a&&a.nodeType?[a]:a),d=f.merge(this.get(),c);return this.pushStack(S(c[0])||S(d[0])?d:f.unique(d))},andSelf:function(){return this.add(this.prevObject)}}),f.each({parent:function(a){var b=a.parentNode;return b&&b.nodeType!==11?b:null},parents:function(a){return f.dir(a,\"parentNode\")},parentsUntil:function(a,b,c){return f.dir(a,\"parentNode\",c)},next:function(a){return f.nth(a,2,\"nextSibling\")},prev:function(a){return f.nth(a,2,\"previousSibling\")},nextAll:function(a){return f.dir(a,\"nextSibling\")},prevAll:function(a){return f.dir(a,\"previousSibling\")},nextUntil:function(a,b,c){return f.dir(a,\"nextSibling\",c)},prevUntil:function(a,b,c){return f.dir(a,\"previousSibling\",c)},siblings:function(a){return f.sibling(a.parentNode.firstChild,a)},children:function(a){return f.sibling(a.firstChild)},contents:function(a){return f.nodeName(a,\"iframe\")?a.contentDocument||a.contentWindow.document:f.makeArray(a.childNodes)}},function(a,b){f.fn[a]=function(c,d){var e=f.map(this,b,c);L.test(a)||(d=c),d&&typeof d==\"string\"&&(e=f.filter(d,e)),e=this.length>1&&!R[a]?f.unique(e):e,(this.length>1||N.test(d))&&M.test(a)&&(e=e.reverse());return this.pushStack(e,a,P.call(arguments).join(\",\"))}}),f.extend({filter:function(a,b,c){c&&(a=\":not(\"+a+\")\");return b.length===1?f.find.matchesSelector(b[0],a)?[b[0]]:[]:f.find.matches(a,b)},dir:function(a,c,d){var e=[],g=a[c];while(g&&g.nodeType!==9&&(d===b||g.nodeType!==1||!f(g).is(d)))g.nodeType===1&&e.push(g),g=g[c];return e},nth:function(a,b,c,d){b=b||1;var e=0;for(;a;a=a[c])if(a.nodeType===1&&++e===b)break;return a},sibling:function(a,b){var c=[];for(;a;a=a.nextSibling)a.nodeType===1&&a!==b&&c.push(a);return c}});var V=\"abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video\",W=/ jQuery\\d+=\"(?:\\d+|null)\"/g,X=/^\\s+/,Y=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\\w:]+)[^>]*)\\/>/ig,Z=/<([\\w:]+)/,$=/<tbody/i,_=/<|&#?\\w+;/,ba=/<(?:script|style)/i,bb=/<(?:script|object|embed|option|style)/i,bc=new RegExp(\"<(?:\"+V+\")\",\"i\"),bd=/checked\\s*(?:[^=]|=\\s*.checked.)/i,be=/\\/(java|ecma)script/i,bf=/^\\s*<!(?:\\[CDATA\\[|\\-\\-)/,bg={option:[1,\"<select multiple='multiple'>\",\"</select>\"],legend:[1,\"<fieldset>\",\"</fieldset>\"],thead:[1,\"<table>\",\"</table>\"],tr:[2,\"<table><tbody>\",\"</tbody></table>\"],td:[3,\"<table><tbody><tr>\",\"</tr></tbody></table>\"],col:[2,\"<table><tbody></tbody><colgroup>\",\"</colgroup></table>\"],area:[1,\"<map>\",\"</map>\"],_default:[0,\"\",\"\"]},bh=U(c);bg.optgroup=bg.option,bg.tbody=bg.tfoot=bg.colgroup=bg.caption=bg.thead,bg.th=bg.td,f.support.htmlSerialize||(bg._default=[1,\"div<div>\",\"</div>\"]),f.fn.extend({text:function(a){if(f.isFunction(a))return this.each(function(b){var c=f(this);c.text(a.call(this,b,c.text()))});if(typeof a!=\"object\"&&a!==b)return this.empty().append((this[0]&&this[0].ownerDocument||c).createTextNode(a));return f.text(this)},wrapAll:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapAll(a.call(this,b))});if(this[0]){var b=f(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&a.firstChild.nodeType===1)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapInner(a.call(this,b))});return this.each(function(){var b=f(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=f.isFunction(a);return this.each(function(c){f(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){f.nodeName(this,\"body\")||f(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.appendChild(a)})},prepend:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.insertBefore(a,this.firstChild)})},before:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this)});if(arguments.length){var a=f.clean(arguments);a.push.apply(a,this.toArray());return this.pushStack(a,\"before\",arguments)}},after:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this.nextSibling)});if(arguments.length){var a=this.pushStack(this,\"after\",arguments);a.push.apply(a,f.clean(arguments));return a}},remove:function(a,b){for(var c=0,d;(d=this[c])!=null;c++)if(!a||f.filter(a,[d]).length)!b&&d.nodeType===1&&(f.cleanData(d.getElementsByTagName(\"*\")),\n"
"f.cleanData([d])),d.parentNode&&d.parentNode.removeChild(d);return this},empty:function()\n"
;

static const char search_jquery_script3[]=
// #include "jquery_p3_js.h"
"{for(var a=0,b;(b=this[a])!=null;a++){b.nodeType===1&&f.cleanData(b.getElementsByTagName(\"*\"));while(b.firstChild)b.removeChild(b.firstChild)}return this},clone:function(a,b){a=a==null?!1:a,b=b==null?a:b;return this.map(function(){return f.clone(this,a,b)})},html:function(a){if(a===b)return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(W,\"\"):null;if(typeof a==\"string\"&&!ba.test(a)&&(f.support.leadingWhitespace||!X.test(a))&&!bg[(Z.exec(a)||[\"\",\"\"])[1].toLowerCase()]){a=a.replace(Y,\"<$1></$2>\");try{for(var c=0,d=this.length;c<d;c++)this[c].nodeType===1&&(f.cleanData(this[c].getElementsByTagName(\"*\")),this[c].innerHTML=a)}catch(e){this.empty().append(a)}}else f.isFunction(a)?this.each(function(b){var c=f(this);c.html(a.call(this,b,c.html()))}):this.empty().append(a);return this},replaceWith:function(a){if(this[0]&&this[0].parentNode){if(f.isFunction(a))return this.each(function(b){var c=f(this),d=c.html();c.replaceWith(a.call(this,b,d))});typeof a!=\"string\"&&(a=f(a).detach());return this.each(function(){var b=this.nextSibling,c=this.parentNode;f(this).remove(),b?f(b).before(a):f(c).append(a)})}return this.length?this.pushStack(f(f.isFunction(a)?a():a),\"replaceWith\",a):this},detach:function(a){return this.remove(a,!0)},domManip:function(a,c,d){var e,g,h,i,j=a[0],k=[];if(!f.support.checkClone&&arguments.length===3&&typeof j==\"string\"&&bd.test(j))return this.each(function(){f(this).domManip(a,c,d,!0)});if(f.isFunction(j))return this.each(function(e){var g=f(this);a[0]=j.call(this,e,c?g.html():b),g.domManip(a,c,d)});if(this[0]){i=j&&j.parentNode,f.support.parentNode&&i&&i.nodeType===11&&i.childNodes.length===this.length?e={fragment:i}:e=f.buildFragment(a,this,k),h=e.fragment,h.childNodes.length===1?g=h=h.firstChild:g=h.firstChild;if(g){c=c&&f.nodeName(g,\"tr\");for(var l=0,m=this.length,n=m-1;l<m;l++)d.call(c?bi(this[l],g):this[l],e.cacheable||m>1&&l<n?f.clone(h,!0,!0):h)}k.length&&f.each(k,bp)}return this}}),f.buildFragment=function(a,b,d){var e,g,h,i,j=a[0];b&&b[0]&&(i=b[0].ownerDocument||b[0]),i.createDocumentFragment||(i=c),a.length===1&&typeof j==\"string\"&&j.length<512&&i===c&&j.charAt(0)===\"<\"&&!bb.test(j)&&(f.support.checkClone||!bd.test(j))&&(f.support.html5Clone||!bc.test(j))&&(g=!0,h=f.fragments[j],h&&h!==1&&(e=h)),e||(e=i.createDocumentFragment(),f.clean(a,i,e,d)),g&&(f.fragments[j]=h?e:1);return{fragment:e,cacheable:g}},f.fragments={},f.each({appendTo:\"append\",prependTo:\"prepend\",insertBefore:\"before\",insertAfter:\"after\",replaceAll:\"replaceWith\"},function(a,b){f.fn[a]=function(c){var d=[],e=f(c),g=this.length===1&&this[0].parentNode;if(g&&g.nodeType===11&&g.childNodes.length===1&&e.length===1){e[b](this[0]);return this}for(var h=0,i=e.length;h<i;h++){var j=(h>0?this.clone(!0):this).get();f(e[h])[b](j),d=d.concat(j)}return this.pushStack(d,a,e.selector)}}),f.extend({clone:function(a,b,c){var d,e,g,h=f.support.html5Clone||!bc.test(\"<\"+a.nodeName)?a.cloneNode(!0):bo(a);if((!f.support.noCloneEvent||!f.support.noCloneChecked)&&(a.nodeType===1||a.nodeType===11)&&!f.isXMLDoc(a)){bk(a,h),d=bl(a),e=bl(h);for(g=0;d[g];++g)e[g]&&bk(d[g],e[g])}if(b){bj(a,h);if(c){d=bl(a),e=bl(h);for(g=0;d[g];++g)bj(d[g],e[g])}}d=e=null;return h},clean:function(a,b,d,e){var g;b=b||c,typeof b.createElement==\"undefined\"&&(b=b.ownerDocument||b[0]&&b[0].ownerDocument||c);var h=[],i;for(var j=0,k;(k=a[j])!=null;j++){typeof k==\"number\"&&(k+=\"\");if(!k)continue;if(typeof k==\"string\")if(!_.test(k))k=b.createTextNode(k);else{k=k.replace(Y,\"<$1></$2>\");var l=(Z.exec(k)||[\"\",\"\"])[1].toLowerCase(),m=bg[l]||bg._default,n=m[0],o=b.createElement(\"div\");b===c?bh.appendChild(o):U(b).appendChild(o),o.innerHTML=m[1]+k+m[2];while(n--)o=o.lastChild;if(!f.support.tbody){var p=$.test(k),q=l===\"table\"&&!p?o.firstChild&&o.firstChild.childNodes:m[1]===\"<table>\"&&!p?o.childNodes:[];for(i=q.length-1;i>=0;--i)f.nodeName(q[i],\"tbody\")&&!q[i].childNodes.length&&q[i].parentNode.removeChild(q[i])}!f.support.leadingWhitespace&&X.test(k)&&o.insertBefore(b.createTextNode(X.exec(k)[0]),o.firstChild),k=o.childNodes}var r;if(!f.support.appendChecked)if(k[0]&&typeof (r=k.length)==\"number\")for(i=0;i<r;i++)bn(k[i]);else bn(k);k.nodeType?h.push(k):h=f.merge(h,k)}if(d){g=function(a){return!a.type||be.test(a.type)};for(j=0;h[j];j++)if(e&&f.nodeName(h[j],\"script\")&&(!h[j].type||h[j].type.toLowerCase()===\"text/javascript\"))e.push(h[j].parentNode?h[j].parentNode.removeChild(h[j]):h[j]);else{if(h[j].nodeType===1){var s=f.grep(h[j].getElementsByTagName(\"script\"),g);h.splice.apply(h,[j+1,0].concat(s))}d.appendChild(h[j])}}return h},cleanData:function(a){var b,c,d=f.cache,e=f.event.special,g=f.support.deleteExpando;for(var h=0,i;(i=a[h])!=null;h++){if(i.nodeName&&f.noData[i.nodeName.toLowerCase()])continue;c=i[f.expando];if(c){b=d[c];if(b&&b.events){for(var j in b.events)e[j]?f.event.remove(i,j):f.removeEvent(i,j,b.handle);b.handle&&(b.handle.elem=null)}g?delete i[f.expando]:i.removeAttribute&&i.removeAttribute(f.expando),delete d[c]}}}});var bq=/alpha\\([^)]*\\)/i,br=/opacity=([^)]*)/,bs=/([A-Z]|^ms)/g,bt=/^-?\\d+(?:px)?$/i,bu=/^-?\\d/,bv=/^([\\-+])=([\\-+.\\de]+)/,bw={position:\"absolute\",visibility:\"hidden\",display:\"block\"},bx=[\"Left\",\"Right\"],by=[\"Top\",\"Bottom\"],bz,bA,bB;f.fn.css=function(a,c){if(arguments.length===2&&c===b)return this;return f.access(this,a,c,!0,function(a,c,d){return d!==b?f.style(a,c,d):f.css(a,c)})},f.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=bz(a,\"opacity\",\"opacity\");return c===\"\"?\"1\":c}return a.style.opacity}}},cssNumber:{fillOpacity:!0,fontWeight:!0,lineHeight:!0,opacity:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{\"float\":f.support.cssFloat?\"cssFloat\":\"styleFloat\"},style:function(a,c,d,e){if(!!a&&a.nodeType!==3&&a.nodeType!==8&&!!a.style){var g,h,i=f.camelCase(c),j=a.style,k=f.cssHooks[i];c=f.cssProps[i]||i;if(d===b){if(k&&\"get\"in k&&(g=k.get(a,!1,e))!==b)return g;return j[c]}h=typeof d,h===\"string\"&&(g=bv.exec(d))&&(d=+(g[1]+1)*+g[2]+parseFloat(f.css(a,c)),h=\"number\");if(d==null||h===\"number\"&&isNaN(d))return;h===\"number\"&&!f.cssNumber[i]&&(d+=\"px\");if(!k||!(\"set\"in k)||(d=k.set(a,d))!==b)try{j[c]=d}catch(l){}}},css:function(a,c,d){var e,g;c=f.camelCase(c),g=f.cssHooks[c],c=f.cssProps[c]||c,c===\"cssFloat\"&&(c=\"float\");if(g&&\"get\"in g&&(e=g.get(a,!0,d))!==b)return e;if(bz)return bz(a,c)},swap:function(a,b,c){var d={};for(var e in b)d[e]=a.style[e],a.style[e]=b[e];c.call(a);for(e in b)a.style[e]=d[e]}}),f.curCSS=f.css,f.each([\"height\",\"width\"],function(a,b){f.cssHooks[b]={get:function(a,c,d){var e;if(c){if(a.offsetWidth!==0)return bC(a,b,d);f.swap(a,bw,function(){e=bC(a,b,d)});return e}},set:function(a,b){if(!bt.test(b))return b;b=parseFloat(b);if(b>=0)return b+\"px\"}}}),f.support.opacity||(f.cssHooks.opacity={get:function(a,b){return br.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||\"\")?parseFloat(RegExp.$1)/100+\"\":b?\"1\":\"\"},set:function(a,b){var c=a.style,d=a.currentStyle,e=f.isNumeric(b)?\"alpha(opacity=\"+b*100+\")\":\"\",g=d&&d.filter||c.filter||\"\";c.zoom=1;if(b>=1&&f.trim(g.replace(bq,\"\"))===\"\"){c.removeAttribute(\"filter\");if(d&&!d.filter)return}c.filter=bq.test(g)?g.replace(bq,e):g+\" \"+e}}),f(function(){f.support.reliableMarginRight||(f.cssHooks.marginRight={get:function(a,b){var c;f.swap(a,{display:\"inline-block\"},function(){b?c=bz(a,\"margin-right\",\"marginRight\"):c=a.style.marginRight});return c}})}),c.defaultView&&c.defaultView.getComputedStyle&&(bA=function(a,b){var c,d,e;b=b.replace(bs,\"-$1\").toLowerCase(),(d=a.ownerDocument.defaultView)&&(e=d.getComputedStyle(a,null))&&(c=e.getPropertyValue(b),c===\"\"&&!f.contains(a.ownerDocument.documentElement,a)&&(c=f.style(a,b)));return c}),c.documentElement.currentStyle&&(bB=function(a,b){var c,d,e,f=a.currentStyle&&a.currentStyle[b],g=a.style;f===null&&g&&(e=g[b])&&(f=e),!bt.test(f)&&bu.test(f)&&(c=g.left,d=a.runtimeStyle&&a.runtimeStyle.left,d&&(a.runtimeStyle.left=a.currentStyle.left),g.left=b===\"fontSize\"?\"1em\":f||0,f=g.pixelLeft+\"px\",g.left=c,d&&(a.runtimeStyle.left=d));return f===\"\"?\"auto\":f}),bz=bA||bB,f.expr&&f.expr.filters&&(f.expr.filters.hidden=function(a){var b=a.offsetWidth,c=a.offsetHeight;return b===0&&c===0||!f.support.reliableHiddenOffsets&&(a.style&&a.style.display||f.css(a,\"display\"))===\"none\"},f.expr.filters.visible=function(a){return!f.expr.filters.hidden(a)});var bD=/%20/g,bE=/\\[\\]$/,bF=/\\r?\\n/g,bG=/#.*$/,bH=/^(.*?):[ \\t]*([^\\r\\n]*)\\r?$/mg,bI=/^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,bJ=/^(?:about|app|app\\-storage|.+\\-extension|file|res|widget):$/,bK=/^(?:GET|HEAD)$/,bL=/^\\/\\//,bM=/\\?/,bN=/<script\\b[^<]*(?:(?!<\\/script>)<[^<]*)*<\\/script>/gi,bO=/^(?:select|textarea)/i,bP=/\\s+/,bQ=/([?&])_=[^&]*/,bR=/^([\\w\\+\\.\\-]+:)(?:\\/\\/([^\\/?#:]*)(?::(\\d+))?)?/,bS=f.fn.load,bT={},bU={},bV,bW,bX=[\"*/\"]+[\"*\"];try{bV=e.href}catch(bY){bV=c.createElement(\"a\"),bV.href=\"\",bV=bV.href}bW=bR.exec(bV.toLowerCase())||[],f.fn.extend({load:function(a,c,d){if(typeof a!=\"string\"&&bS)return bS.apply(this,arguments);if(!this.length)return this;var e=a.indexOf(\" \");if(e>=0){var g=a.slice(e,a.length);a=a.slice(0,e)}var h=\"GET\";c&&(f.isFunction(c)?(d=c,c=b):typeof c==\"object\"&&(c=f.param(c,f.ajaxSettings.traditional),h=\"POST\"));var i=this;f.ajax({url:a,type:h,dataType:\"html\",data:c,complete:function(a,b,c){c=a.responseText,a.isResolved()&&(a.done(function(a){c=a}),i.html(g?f(\"<div>\").append(c.replace(bN,\"\")).find(g):c)),d&&i.each(d,[c,b,a])}});return this},serialize:function(){return f.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?f.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||bO.test(this.nodeName)||bI.test(this.type))}).map(function(a,b){var c=f(this).val();return c==null?null:f.isArray(c)?f.map(c,function(a,c){return{name:b.name,value:a.replace(bF,\"\\r\\n\")}}):{name:b.name,value:c.replace(bF,\"\\r\\n\")}}).get()}}),f.each(\"ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend\".split(\" \"),function(a,b){f.fn[b]=function(a){return this.on(b,a)}}),f.each([\"get\",\"post\"],function(a,c){f[c]=function(a,d,e,g){f.isFunction(d)&&(g=g||e,e=d,d=b);return f.ajax({type:c,url:a,data:d,success:e,dataType:g})}}),f.extend({getScript:function(a,c){return f.get(a,b,c,\"script\")},getJSON:function(a,b,c){return f.get(a,b,c,\"json\")},ajaxSetup:function(a,b){b?b_(a,f.ajaxSettings):(b=a,a=f.ajaxSettings),b_(a,b);return a},ajaxSettings:{url:bV,isLocal:bJ.test(bW[1]),global:!0,type:\"GET\",contentType:\"application/x-www-form-urlencoded\",processData:!0,async:!0,accepts:{xml:\"application/xml, text/xml\",html:\"text/html\",text:\"text/plain\",json:\"application/json, text/javascript\",\"*\":bX},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:\"responseXML\",text:\"responseText\"},converters:{\"* text\":a.String,\"text html\":!0,\"text json\":f.parseJSON,\"text xml\":f.parseXML},flatOptions:{context:!0,url:!0}},ajaxPrefilter:bZ(bT),ajaxTransport:bZ(bU),ajax:function(a,c){function w(a,c,l,m){if(s!==2){s=2,q&&clearTimeout(q),p=b,n=m||\"\",v.readyState=a>0?4:0;var o,r,u,w=c,x=l?cb(d,v,l):b,y,z;if(a>=200&&a<300||a===304){if(d.ifModified){if(y=v.getResponseHeader(\"Last-Modified\"))f.lastModified[k]=y;if(z=v.getResponseHeader(\"Etag\"))f.etag[k]=z}if(a===304)w=\"notmodified\",o=!0;else try{r=cc(d,x),w=\"success\",o=!0}catch(A){w=\"parsererror\",u=A}}else{u=w;if(!w||a)w=\"error\",a<0&&(a=0)}v.status=a,v.statusText=\"\"+(c||w),o?h.resolveWith(e,[r,w,v]):h.rejectWith(e,[v,w,u]),v.statusCode(j),j=b,t&&g.trigger(\"ajax\"+(o?\"Success\":\"Error\"),[v,d,o?r:u]),i.fireWith(e,[v,w]),t&&(g.trigger(\"ajaxComplete\",[v,d]),--f.active||f.event.trigger(\"ajaxStop\"))}}typeof a==\"object\"&&(c=a,a=b),c=c||{};var d=f.ajaxSetup({},c),e=d.context||d,g=e!==d&&(e.nodeType||e instanceof f)?f(e):f.event,h=f.Deferred(),i=f.Callbacks(\"once memory\"),j=d.statusCode||{},k,l={},m={},n,o,p,q,r,s=0,t,u,v={readyState:0,setRequestHeader:function(a,b){if(!s){var c=a.toLowerCase();a=m[c]=m[c]||a,l[a]=b}return this},getAllResponseHeaders:function(){return s===2?n:null},getResponseHeader:function(a){var c;if(s===2){if(!o){o={};while(c=bH.exec(n))o[c[1].toLowerCase()]=c[2]}c=o[a.toLowerCase()]}return c===b?null:c},overrideMimeType:function(a){s||(d.mimeType=a);return this},abort:function(a){a=a||\"abort\",p&&p.abort(a),w(0,a);return this}};h.promise(v),v.success=v.done,v.error=v.fail,v.complete=i.add,v.statusCode=function(a){if(a){var b;if(s<2)for(b in a)j[b]=[j[b],a[b]];else b=a[v.status],v.then(b,b)}return this},d.url=((a||d.url)+\"\").replace(bG,\"\").replace(bL,bW[1]+\"//\"),d.dataTypes=f.trim(d.dataType||\"*\").toLowerCase().split(bP),d.crossDomain==null&&(r=bR.exec(d.url.toLowerCase()),d.crossDomain=!(!r||r[1]==bW[1]&&r[2]==bW[2]&&(r[3]||(r[1]===\"http:\"?80:443))==(bW[3]||(bW[1]===\"http:\"?80:443)))),d.data&&d.processData&&typeof d.data!=\"string\"&&(d.data=f.param(d.data,d.traditional)),b$(bT,d,c,v);if(s===2)return!1;t=d.global,d.type=d.type.toUpperCase(),d.hasContent=!bK.test(d.type),t&&f.active++===0&&f.event.trigger(\"ajaxStart\");if(!d.hasContent){d.data&&(d.url+=(bM.test(d.url)?\"&\":\"?\")+d.data,delete d.data),k=d.url;if(d.cache===!1){var x=f.now(),y=d.url.replace(bQ,\"$1_=\"+x);d.url=y+(y===d.url?(bM.test(d.url)?\"&\":\"?\")+\"_=\"+x:\"\")}}(d.data&&d.hasContent&&d.contentType!==!1||c.contentType)&&v.setRequestHeader(\"Content-Type\",d.contentType),d.ifModified&&(k=k||d.url,f.lastModified[k]&&v.setRequestHeader(\"If-Modified-Since\",f.lastModified[k]),f.etag[k]&&v.setRequestHeader(\"If-None-Match\",f.etag[k])),v.setRequestHeader(\"Accept\",d.dataTypes[0]&&d.accepts[d.dataTypes[0]]?d.accepts[d.dataTypes[0]]+(d.dataTypes[0]!==\"*\"?\", \"+bX+\"; q=0.01\":\"\"):d.accepts[\"*\"]);for(u in d.headers)v.setRequestHeader(u,d.headers[u]);if(d.beforeSend&&(d.beforeSend.call(e,v,d)===!1||s===2)){v.abort();return!1}for(u in{success:1,error:1,complete:1})v[u](d[u]);p=b$(bU,d,c,v);if(!p)w(-1,\"No Transport\");else{v.readyState=1,t&&g.trigger(\"ajaxSend\",[v,d]),d.async&&d.timeout>0&&(q=setTimeout(function(){v.abort(\"timeout\")},d.timeout));try{s=1,p.send(l,w)}catch(z){if(s<2)w(-1,z);else throw z}}return v},param:function(a,c){var d=[],e=function(a,b){b=f.isFunction(b)?b():b,d[d.length]=encodeURIComponent(a)+\"=\"+encodeURIComponent(b)};c===b&&(c=f.ajaxSettings.traditional);if(f.isArray(a)||a.jquery&&!f.isPlainObject(a))f.each(a,function(){e(this.name,this.value)});else for(var g in a)ca(g,a[g],c,e);return d.join(\"&\").replace(bD,\"+\")}}),f.extend({active:0,lastModified:{},etag:{}});var cd=f.now(),ce=/(\\=)\\?(&|$)|\\?\\?/i;f.ajaxSetup({jsonp:\"callback\",jsonpCallback:function(){return f.expando+\"_\"+cd++}}),f.ajaxPrefilter(\"json jsonp\",function(b,c,d){var e=b.contentType===\"application/x-www-form-urlencoded\"&&typeof b.data==\"string\";if(b.dataTypes[0]===\"jsonp\"||b.jsonp!==!1&&(ce.test(b.url)||e&&ce.test(b.data))){var g,h=b.jsonpCallback=f.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,i=a[h],j=b.url,k=b.data,l=\"$1\"+h+\"$2\";b.jsonp!==!1&&(j=j.replace(ce,l),b.url===j&&(e&&(k=k.replace(ce,l)),b.data===k&&(j+=(/\\?/.test(j)?\"&\":\"?\")+b.jsonp+\"=\"+h))),b.url=j,b.data=k,a[h]=function(a){g=[a]},d.always(function(){a[h]=i,g&&f.isFunction(i)&&a[h](g[0])}),b.converters[\"script json\"]=function(){g||f.error(h+\" was not called\");return g[0]},b.dataTypes[0]=\"json\";return\"script\"}}),f.ajaxSetup({accepts:{script:\"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript\"},contents:{script:/javascript|ecmascript/},converters:{\"text script\":function(a){f.globalEval(a);return a}}}),f.ajaxPrefilter(\"script\",function(a){a.cache===b&&(a.cache=!1),a.crossDomain&&(a.type=\"GET\",a.global=!1)}),f.ajaxTransport(\"script\",function(a){if(a.crossDomain){var d,e=c.head||c.getElementsByTagName(\"head\")[0]||c.documentElement;return{send:function(f,g){d=c.createElement(\"script\"),d.async=\"async\",a.scriptCharset&&(d.charset=a.scriptCharset),d.src=a.url,d.onload=d.onreadystatechange=function(a,c){if(c||!d.readyState||/loaded|complete/.test(d.readyState))d.onload=d.onreadystatechange=null,e&&d.parentNode&&e.removeChild(d),d=b,c||g(200,\"success\")},e.insertBefore(d,e.firstChild)},abort:function(){d&&d.onload(0,1)}}}});var cf=a.ActiveXObject?function(){for(var a in ch)ch[a](0,1)}:!1,cg=0,ch;f.ajaxSettings.xhr=a.ActiveXObject?function(){return!this.isLocal&&ci()||cj()}:ci,function(a){f.extend(f.support,{ajax:!!a,cors:!!a&&\"withCredentials\"in a})}(f.ajaxSettings.xhr()),f.support.ajax&&f.ajaxTransport(function(c)\n"
"{if(!c.crossDomain||f.support.cors){var d;return{send:function(e,g){var h=c.xhr(),i,j;c.username?h.open(c.type,c.url,c.async,c.username,c.password):h.open(c.type,c.url,c.async);if(c.xhrFields)for(j in c.xhrFields)h[j]=c.xhrFields[j];c.mimeType&&h.overrideMimeType&&h.overrideMimeType(c.mimeType),!c.crossDomain&&!e[\"X-Requested-With\"]&&(e[\"X-Requested-With\"]=\"XMLHttpRequest\");try{for(j in e)h.setRequestHeader(j,e[j])}catch(k){}h.send(c.hasContent&&c.data||null),d=function(a,e){var j,k,l,m,n;try{if(d&&(e||h.readyState===4)){d=b,i&&(h.onreadystatechange=f.noop,cf&&delete ch[i]);if(e)h.readyState!==4&&h.abort();else{j=h.status,l=h.getAllResponseHeaders(),m={},n=h.responseXML,n&&n.documentElement&&(m.xml=n),m.text=h.responseText;try{k=h.statusText}catch(o){k=\"\"}!j&&c.isLocal&&!c.crossDomain?j=m.text?200:404:j===1223&&(j=204)}}}catch(p){e||g(-1,p)}m&&g(j,k,m,l)},!c.async||h.readyState===4?d():(i=++cg,cf&&(ch||(ch={},f(a).unload(cf)),ch[i]=d),h.onreadystatechange=d)},abort:function(){d&&d(0,1)}}}});var ck={},cl,cm,cn=/^(?:toggle|show|hide)$/,co=/^([+\\-]=)?([\\d+.\\-]+)([a-z%]*)$/i,cp,cq=[[\"height\",\"marginTop\",\"marginBottom\",\"paddingTop\",\"paddingBottom\"],[\"width\",\"marginLeft\",\"marginRight\",\"paddingLeft\",\"paddingRight\"],[\"opacity\"]],cr;f.fn.extend({show:function(a,b,c){var d,e;if(a||a===0)return this.animate(cu(\"show\",3),a,b,c);for(var g=0,h=this.length;g<h;g++)d=this[g],d.style&&(e=d.style.display,!f._data(d,\"olddisplay\")&&e===\"none\"&&(e=d.style.display=\"\"),e===\"\"&&f.css(d,\"display\")===\"none\"&&f._data(d,\"olddisplay\",cv(d.nodeName)));for(g=0;g<h;g++){d=this[g];if(d.style){e=d.style.display;if(e===\"\"||e===\"none\")d.style.display=f._data(d,\"olddisplay\")||\"\"}}return this},hide:function(a,b,c){if(a||a===0)return this.animate(cu(\"hide\",3),a,b,c);var d,e,g=0,h=this.length;for(;g<h;g++)d=this[g],d.style&&(e=f.css(d,\"display\"),e!==\"none\"&&!f._data(d,\"olddisplay\")&&f._data(d,\"olddisplay\",e));for(g=0;g<h;g++)this[g].style&&(this[g].style.display=\"none\");return this},_toggle:f.fn.toggle,toggle:function(a,b,c){var d=typeof a==\"boolean\";f.isFunction(a)&&f.isFunction(b)?this._toggle.apply(this,arguments):a==null||d?this.each(function(){var b=d?a:f(this).is(\":hidden\");f(this)[b?\"show\":\"hide\"]()}):this.animate(cu(\"toggle\",3),a,b,c);return this},fadeTo:function(a,b,c,d){return this.filter(\":hidden\").css(\"opacity\",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){function g(){e.queue===!1&&f._mark(this);var b=f.extend({},e),c=this.nodeType===1,d=c&&f(this).is(\":hidden\"),g,h,i,j,k,l,m,n,o;b.animatedProperties={};for(i in a){g=f.camelCase(i),i!==g&&(a[g]=a[i],delete a[i]),h=a[g],f.isArray(h)?(b.animatedProperties[g]=h[1],h=a[g]=h[0]):b.animatedProperties[g]=b.specialEasing&&b.specialEasing[g]||b.easing||\"swing\";if(h===\"hide\"&&d||h===\"show\"&&!d)return b.complete.call(this);c&&(g===\"height\"||g===\"width\")&&(b.overflow=[this.style.overflow,this.style.overflowX,this.style.overflowY],f.css(this,\"display\")===\"inline\"&&f.css(this,\"float\")===\"none\"&&(!f.support.inlineBlockNeedsLayout||cv(this.nodeName)===\"inline\"?this.style.display=\"inline-block\":this.style.zoom=1))}b.overflow!=null&&(this.style.overflow=\"hidden\");for(i in a)j=new f.fx(this,b,i),h=a[i],cn.test(h)?(o=f._data(this,\"toggle\"+i)||(h===\"toggle\"?d?\"show\":\"hide\":0),o?(f._data(this,\"toggle\"+i,o===\"show\"?\"hide\":\"show\"),j[o]()):j[h]()):(k=co.exec(h),l=j.cur(),k?(m=parseFloat(k[2]),n=k[3]||(f.cssNumber[i]?\"\":\"px\"),n!==\"px\"&&(f.style(this,i,(m||1)+n),l=(m||1)/j.cur()*l,f.style(this,i,l+n)),k[1]&&(m=(k[1]===\"-=\"?-1:1)*m+l),j.custom(l,m,n)):j.custom(l,h,\"\"));return!0}var e=f.speed(b,c,d);if(f.isEmptyObject(a))return this.each(e.complete,[!1]);a=f.extend({},a);return e.queue===!1?this.each(g):this.queue(e.queue,g)},stop:function(a,c,d){typeof a!=\"string\"&&(d=c,c=a,a=b),c&&a!==!1&&this.queue(a||\"fx\",[]);return this.each(function(){function h(a,b,c){var e=b[c];f.removeData(a,c,!0),e.stop(d)}var b,c=!1,e=f.timers,g=f._data(this);d||f._unmark(!0,this);if(a==null)for(b in g)g[b]&&g[b].stop&&b.indexOf(\".run\")===b.length-4&&h(this,g,b);else g[b=a+\".run\"]&&g[b].stop&&h(this,g,b);for(b=e.length;b--;)e[b].elem===this&&(a==null||e[b].queue===a)&&(d?e[b](!0):e[b].saveState(),c=!0,e.splice(b,1));(!d||!c)&&f.dequeue(this,a)})}}),f.each({slideDown:cu(\"show\",1),slideUp:cu(\"hide\",1),slideToggle:cu(\"toggle\",1),fadeIn:{opacity:\"show\"},fadeOut:{opacity:\"hide\"},fadeToggle:{opacity:\"toggle\"}},function(a,b){f.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),f.extend({speed:function(a,b,c){var d=a&&typeof a==\"object\"?f.extend({},a):{complete:c||!c&&b||f.isFunction(a)&&a,duration:a,easing:c&&b||b&&!f.isFunction(b)&&b};d.duration=f.fx.off?0:typeof d.duration==\"number\"?d.duration:d.duration in f.fx.speeds?f.fx.speeds[d.duration]:f.fx.speeds._default;if(d.queue==null||d.queue===!0)d.queue=\"fx\";d.old=d.complete,d.complete=function(a){f.isFunction(d.old)&&d.old.call(this),d.queue?f.dequeue(this,d.queue):a!==!1&&f._unmark(this)};return d},easing:{linear:function(a,b,c,d){return c+d*a},swing:function(a,b,c,d){return(-Math.cos(a*Math.PI)/2+.5)*d+c}},timers:[],fx:function(a,b,c){this.options=b,this.elem=a,this.prop=c,b.orig=b.orig||{}}}),f.fx.prototype={update:function(){this.options.step&&this.options.step.call(this.elem,this.now,this),(f.fx.step[this.prop]||f.fx.step._default)(this)},cur:function(){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null))return this.elem[this.prop];var a,b=f.css(this.elem,this.prop);return isNaN(a=parseFloat(b))?!b||b===\"auto\"?0:b:a},custom:function(a,c,d){function h(a){return e.step(a)}var e=this,g=f.fx;this.startTime=cr||cs(),this.end=c,this.now=this.start=a,this.pos=this.state=0,this.unit=d||this.unit||(f.cssNumber[this.prop]?\"\":\"px\"),h.queue=this.options.queue,h.elem=this.elem,h.saveState=function(){e.options.hide&&f._data(e.elem,\"fxshow\"+e.prop)===b&&f._data(e.elem,\"fxshow\"+e.prop,e.start)},h()&&f.timers.push(h)&&!cp&&(cp=setInterval(g.tick,g.interval))},show:function(){var a=f._data(this.elem,\"fxshow\"+this.prop);this.options.orig[this.prop]=a||f.style(this.elem,this.prop),this.options.show=!0,a!==b?this.custom(this.cur(),a):this.custom(this.prop===\"width\"||this.prop===\"height\"?1:0,this.cur()),f(this.elem).show()},hide:function(){this.options.orig[this.prop]=f._data(this.elem,\"fxshow\"+this.prop)||f.style(this.elem,this.prop),this.options.hide=!0,this.custom(this.cur(),0)},step:function(a){var b,c,d,e=cr||cs(),g=!0,h=this.elem,i=this.options;if(a||e>=i.duration+this.startTime){this.now=this.end,this.pos=this.state=1,this.update(),i.animatedProperties[this.prop]=!0;for(b in i.animatedProperties)i.animatedProperties[b]!==!0&&(g=!1);if(g){i.overflow!=null&&!f.support.shrinkWrapBlocks&&f.each([\"\",\"X\",\"Y\"],function(a,b){h.style[\"overflow\"+b]=i.overflow[a]}),i.hide&&f(h).hide();if(i.hide||i.show)for(b in i.animatedProperties)f.style(h,b,i.orig[b]),f.removeData(h,\"fxshow\"+b,!0),f.removeData(h,\"toggle\"+b,!0);d=i.complete,d&&(i.complete=!1,d.call(h))}return!1}i.duration==Infinity?this.now=e:(c=e-this.startTime,this.state=c/i.duration,this.pos=f.easing[i.animatedProperties[this.prop]](this.state,c,0,1,i.duration),this.now=this.start+(this.end-this.start)*this.pos),this.update();return!0}},f.extend(f.fx,{tick:function(){var a,b=f.timers,c=0;for(;c<b.length;c++)a=b[c],!a()&&b[c]===a&&b.splice(c--,1);b.length||f.fx.stop()},interval:13,stop:function(){clearInterval(cp),cp=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(a){f.style(a.elem,\"opacity\",a.now)},_default:function(a){a.elem.style&&a.elem.style[a.prop]!=null?a.elem.style[a.prop]=a.now+a.unit:a.elem[a.prop]=a.now}}}),f.each([\"width\",\"height\"],function(a,b){f.fx.step[b]=function(a){f.style(a.elem,b,Math.max(0,a.now)+a.unit)}}),f.expr&&f.expr.filters&&(f.expr.filters.animated=function(a){return f.grep(f.timers,function(b){return a===b.elem}).length});var cw=/^t(?:able|d|h)$/i,cx=/^(?:body|html)$/i;\"getBoundingClientRect\"in c.documentElement?f.fn.offset=function(a){var b=this[0],c;if(a)return this.each(function(b){f.offset.setOffset(this,a,b)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return f.offset.bodyOffset(b);try{c=b.getBoundingClientRect()}catch(d){}var e=b.ownerDocument,g=e.documentElement;if(!c||!f.contains(g,b))return c?{top:c.top,left:c.left}:{top:0,left:0};var h=e.body,i=cy(e),j=g.clientTop||h.clientTop||0,k=g.clientLeft||h.clientLeft||0,l=i.pageYOffset||f.support.boxModel&&g.scrollTop||h.scrollTop,m=i.pageXOffset||f.support.boxModel&&g.scrollLeft||h.scrollLeft,n=c.top+l-j,o=c.left+m-k;return{top:n,left:o}}:f.fn.offset=function(a){var b=this[0];if(a)return this.each(function(b){f.offset.setOffset(this,a,b)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return f.offset.bodyOffset(b);var c,d=b.offsetParent,e=b,g=b.ownerDocument,h=g.documentElement,i=g.body,j=g.defaultView,k=j?j.getComputedStyle(b,null):b.currentStyle,l=b.offsetTop,m=b.offsetLeft;while((b=b.parentNode)&&b!==i&&b!==h){if(f.support.fixedPosition&&k.position===\"fixed\")break;c=j?j.getComputedStyle(b,null):b.currentStyle,l-=b.scrollTop,m-=b.scrollLeft,b===d&&(l+=b.offsetTop,m+=b.offsetLeft,f.support.doesNotAddBorder&&(!f.support.doesAddBorderForTableAndCells||!cw.test(b.nodeName))&&(l+=parseFloat(c.borderTopWidth)||0,m+=parseFloat(c.borderLeftWidth)||0),e=d,d=b.offsetParent),f.support.subtractsBorderForOverflowNotVisible&&c.overflow!==\"visible\"&&(l+=parseFloat(c.borderTopWidth)||0,m+=parseFloat(c.borderLeftWidth)||0),k=c}if(k.position===\"relative\"||k.position===\"static\")l+=i.offsetTop,m+=i.offsetLeft;f.support.fixedPosition&&k.position===\"fixed\"&&(l+=Math.max(h.scrollTop,i.scrollTop),m+=Math.max(h.scrollLeft,i.scrollLeft));return{top:l,left:m}},f.offset={bodyOffset:function(a){var b=a.offsetTop,c=a.offsetLeft;f.support.doesNotIncludeMarginInBodyOffset&&(b+=parseFloat(f.css(a,\"marginTop\"))||0,c+=parseFloat(f.css(a,\"marginLeft\"))||0);return{top:b,left:c}},setOffset:function(a,b,c){var d=f.css(a,\"position\");d===\"static\"&&(a.style.position=\"relative\");var e=f(a),g=e.offset(),h=f.css(a,\"top\"),i=f.css(a,\"left\"),j=(d===\"absolute\"||d===\"fixed\")&&f.inArray(\"auto\",[h,i])>-1,k={},l={},m,n;j?(l=e.position(),m=l.top,n=l.left):(m=parseFloat(h)||0,n=parseFloat(i)||0),f.isFunction(b)&&(b=b.call(a,c,g)),b.top!=null&&(k.top=b.top-g.top+m),b.left!=null&&(k.left=b.left-g.left+n),\"using\"in b?b.using.call(a,k):e.css(k)}},f.fn.extend({position:function(){if(!this[0])return null;var a=this[0],b=this.offsetParent(),c=this.offset(),d=cx.test(b[0].nodeName)?{top:0,left:0}:b.offset();c.top-=parseFloat(f.css(a,\"marginTop\"))||0,c.left-=parseFloat(f.css(a,\"marginLeft\"))||0,d.top+=parseFloat(f.css(b[0],\"borderTopWidth\"))||0,d.left+=parseFloat(f.css(b[0],\"borderLeftWidth\"))||0;return{top:c.top-d.top,left:c.left-d.left}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||c.body;while(a&&!cx.test(a.nodeName)&&f.css(a,\"position\")===\"static\")a=a.offsetParent;return a})}}),f.each([\"Left\",\"Top\"],function(a,c){var d=\"scroll\"+c;f.fn[d]=function(c){var e,g;if(c===b){e=this[0];if(!e)return null;g=cy(e);return g?\"pageXOffset\"in g?g[a?\"pageYOffset\":\"pageXOffset\"]:f.support.boxModel&&g.document.documentElement[d]||g.document.body[d]:e[d]}return this.each(function(){g=cy(this),g?g.scrollTo(a?f(g).scrollLeft():c,a?c:f(g).scrollTop()):this[d]=c})}}),f.each([\"Height\",\"Width\"],function(a,c){var d=c.toLowerCase();f.fn[\"inner\"+c]=function(){var a=this[0];return a?a.style?parseFloat(f.css(a,d,\"padding\")):this[d]():null},f.fn[\"outer\"+c]=function(a){var b=this[0];return b?b.style?parseFloat(f.css(b,d,a?\"margin\":\"border\")):this[d]():null},f.fn[d]=function(a){var e=this[0];if(!e)return a==null?null:this;if(f.isFunction(a))return this.each(function(b){var c=f(this);c[d](a.call(this,b,c[d]()))});if(f.isWindow(e)){var g=e.document.documentElement[\"client\"+c],h=e.document.body;return e.document.compatMode===\"CSS1Compat\"&&g||h&&h[\"client\"+c]||g}if(e.nodeType===9)return Math.max(e.documentElement[\"client\"+c],e.body[\"scroll\"+c],e.documentElement[\"scroll\"+c],e.body[\"offset\"+c],e.documentElement[\"offset\"+c]);if(a===b){var i=f.css(e,d),j=parseFloat(i);return f.isNumeric(j)?j:i}return this.css(d,typeof a==\"string\"?a:a+\"px\")}}),a.jQuery=a.$=f,typeof define==\"function\"&&define.amd&&define.amd.jQuery&&define(\"jquery\",[],function(){return f})})(window);\n"
;

static const char search_jquery_script4[]=
// #include "jquery_ui_js.h"
"/*!\n"
" * jQuery UI 1.8.18\n"
" *\n"
" * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)\n"
" * Dual licensed under the MIT or GPL Version 2 licenses.\n"
" * http://jquery.org/license\n"
" *\n"
" * http://docs.jquery.com/UI\n"
" */\n"
"(function(a,b){function d(b){return!a(b).parents().andSelf().filter(function(){return a.curCSS(this,\"visibility\")===\"hidden\"||a.expr.filters.hidden(this)}).length}function c(b,c){var e=b.nodeName.toLowerCase();if(\"area\"===e){var f=b.parentNode,g=f.name,h;if(!b.href||!g||f.nodeName.toLowerCase()!==\"map\")return!1;h=a(\"img[usemap=#\"+g+\"]\")[0];return!!h&&d(h)}return(/input|select|textarea|button|object/.test(e)?!b.disabled:\"a\"==e?b.href||c:c)&&d(b)}a.ui=a.ui||{};a.ui.version||(a.extend(a.ui,{version:\"1.8.18\",keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}}),a.fn.extend({propAttr:a.fn.prop||a.fn.attr,_focus:a.fn.focus,focus:function(b,c){return typeof b==\"number\"?this.each(function(){var d=this;setTimeout(function(){a(d).focus(),c&&c.call(d)},b)}):this._focus.apply(this,arguments)},scrollParent:function(){var b;a.browser.msie&&/(static|relative)/.test(this.css(\"position\"))||/absolute/.test(this.css(\"position\"))?b=this.parents().filter(function(){return/(relative|absolute|fixed)/.test(a.curCSS(this,\"position\",1))&&/(auto|scroll)/.test(a.curCSS(this,\"overflow\",1)+a.curCSS(this,\"overflow-y\",1)+a.curCSS(this,\"overflow-x\",1))}).eq(0):b=this.parents().filter(function(){return/(auto|scroll)/.test(a.curCSS(this,\"overflow\",1)+a.curCSS(this,\"overflow-y\",1)+a.curCSS(this,\"overflow-x\",1))}).eq(0);return/fixed/.test(this.css(\"position\"))||!b.length?a(document):b},zIndex:function(c){if(c!==b)return this.css(\"zIndex\",c);if(this.length){var d=a(this[0]),e,f;while(d.length&&d[0]!==document){e=d.css(\"position\");if(e===\"absolute\"||e===\"relative\"||e===\"fixed\"){f=parseInt(d.css(\"zIndex\"),10);if(!isNaN(f)&&f!==0)return f}d=d.parent()}}return 0},disableSelection:function(){return this.bind((a.support.selectstart?\"selectstart\":\"mousedown\")+\".ui-disableSelection\",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(\".ui-disableSelection\")}}),a.each([\"Width\",\"Height\"],function(c,d){function h(b,c,d,f){a.each(e,function(){c-=parseFloat(a.curCSS(b,\"padding\"+this,!0))||0,d&&(c-=parseFloat(a.curCSS(b,\"border\"+this+\"Width\",!0))||0),f&&(c-=parseFloat(a.curCSS(b,\"margin\"+this,!0))||0)});return c}var e=d===\"Width\"?[\"Left\",\"Right\"]:[\"Top\",\"Bottom\"],f=d.toLowerCase(),g={innerWidth:a.fn.innerWidth,innerHeight:a.fn.innerHeight,outerWidth:a.fn.outerWidth,outerHeight:a.fn.outerHeight};a.fn[\"inner\"+d]=function(c){if(c===b)return g[\"inner\"+d].call(this);return this.each(function(){a(this).css(f,h(this,c)+\"px\")})},a.fn[\"outer\"+d]=function(b,c){if(typeof b!=\"number\")return g[\"outer\"+d].call(this,b);return this.each(function(){a(this).css(f,h(this,b,!0,c)+\"px\")})}}),a.extend(a.expr[\":\"],{data:function(b,c,d){return!!a.data(b,d[3])},focusable:function(b){return c(b,!isNaN(a.attr(b,\"tabindex\")))},tabbable:function(b){var d=a.attr(b,\"tabindex\"),e=isNaN(d);return(e||d>=0)&&c(b,!e)}}),a(function(){var b=document.body,c=b.appendChild(c=document.createElement(\"div\"));c.offsetHeight,a.extend(c.style,{minHeight:\"100px\",height:\"auto\",padding:0,borderWidth:0}),a.support.minHeight=c.offsetHeight===100,a.support.selectstart=\"onselectstart\"in c,b.removeChild(c).style.display=\"none\"}),a.extend(a.ui,{plugin:{add:function(b,c,d){var e=a.ui[b].prototype;for(var f in d)e.plugins[f]=e.plugins[f]||[],e.plugins[f].push([c,d[f]])},call:function(a,b,c){var d=a.plugins[b];if(!!d&&!!a.element[0].parentNode)for(var e=0;e<d.length;e++)a.options[d[e][0]]&&d[e][1].apply(a.element,c)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(b,c){if(a(b).css(\"overflow\")===\"hidden\")return!1;var d=c&&c===\"left\"?\"scrollLeft\":\"scrollTop\",e=!1;if(b[d]>0)return!0;b[d]=1,e=b[d]>0,b[d]=0;return e},isOverAxis:function(a,b,c){return a>b&&a<b+c},isOver:function(b,c,d,e,f,g){return a.ui.isOverAxis(b,d,f)&&a.ui.isOverAxis(c,e,g)}}))})(jQuery);\n"
"/*!\n"
" * jQuery UI Widget 1.8.18\n"
" *\n"
" * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)\n"
" * Dual licensed under the MIT or GPL Version 2 licenses.\n"
" * http://jquery.org/license\n"
" *\n"
" * http://docs.jquery.com/UI/Widget\n"
" */\n"
"(function(a,b){if(a.cleanData){var c=a.cleanData;a.cleanData=function(b){for(var d=0,e;(e=b[d])!=null;d++)try{a(e).triggerHandler(\"remove\")}catch(f){}c(b)}}else{var d=a.fn.remove;a.fn.remove=function(b,c){return this.each(function(){c||(!b||a.filter(b,[this]).length)&&a(\"*\",this).add([this]).each(function(){try{a(this).triggerHandler(\"remove\")}catch(b){}});return d.call(a(this),b,c)})}}a.widget=function(b,c,d){var e=b.split(\".\")[0],f;b=b.split(\".\")[1],f=e+\"-\"+b,d||(d=c,c=a.Widget),a.expr[\":\"][f]=function(c){return!!a.data(c,b)},a[e]=a[e]||{},a[e][b]=function(a,b){arguments.length&&this._createWidget(a,b)};var g=new c;g.options=a.extend(!0,{},g.options),a[e][b].prototype=a.extend(!0,g,{namespace:e,widgetName:b,widgetEventPrefix:a[e][b].prototype.widgetEventPrefix||b,widgetBaseClass:f},d),a.widget.bridge(b,a[e][b])},a.widget.bridge=function(c,d){a.fn[c]=function(e){var f=typeof e==\"string\",g=Array.prototype.slice.call(arguments,1),h=this;e=!f&&g.length?a.extend.apply(null,[!0,e].concat(g)):e;if(f&&e.charAt(0)===\"_\")return h;f?this.each(function(){var d=a.data(this,c),f=d&&a.isFunction(d[e])?d[e].apply(d,g):d;if(f!==d&&f!==b){h=f;return!1}}):this.each(function(){var b=a.data(this,c);b?b.option(e||{})._init():a.data(this,c,new d(e,this))});return h}},a.Widget=function(a,b){arguments.length&&this._createWidget(a,b)},a.Widget.prototype={widgetName:\"widget\",widgetEventPrefix:\"\",options:{disabled:!1},_createWidget:function(b,c){a.data(c,this.widgetName,this),this.element=a(c),this.options=a.extend(!0,{},this.options,this._getCreateOptions(),b);var d=this;this.element.bind(\"remove.\"+this.widgetName,function(){d.destroy()}),this._create(),this._trigger(\"create\"),this._init()},_getCreateOptions:function(){return a.metadata&&a.metadata.get(this.element[0])[this.widgetName]},_create:function(){},_init:function(){},destroy:function(){this.element.unbind(\".\"+this.widgetName).removeData(this.widgetName),this.widget().unbind(\".\"+this.widgetName).removeAttr(\"aria-disabled\").removeClass(this.widgetBaseClass+\"-disabled \"+\"ui-state-disabled\")},widget:function(){return this.element},option:function(c,d){var e=c;if(arguments.length===0)return a.extend({},this.options);if(typeof c==\"string\"){if(d===b)return this.options[c];e={},e[c]=d}this._setOptions(e);return this},_setOptions:function(b){var c=this;a.each(b,function(a,b){c._setOption(a,b)});return this},_setOption:function(a,b){this.options[a]=b,a===\"disabled\"&&this.widget()[b?\"addClass\":\"removeClass\"](this.widgetBaseClass+\"-disabled\"+\" \"+\"ui-state-disabled\").attr(\"aria-disabled\",b);return this},enable:function(){return this._setOption(\"disabled\",!1)},disable:function(){return this._setOption(\"disabled\",!0)},_trigger:function(b,c,d){var e,f,g=this.options[b];d=d||{},c=a.Event(c),c.type=(b===this.widgetEventPrefix?b:this.widgetEventPrefix+b).toLowerCase(),c.target=this.element[0],f=c.originalEvent;if(f)for(e in f)e in c||(c[e]=f[e]);this.element.trigger(c,d);return!(a.isFunction(g)&&g.call(this.element[0],c,d)===!1||c.isDefaultPrevented())}}})(jQuery);\n"
"/*!\n"
" * jQuery UI Mouse 1.8.18\n"
" *\n"
" * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)\n"
" * Dual licensed under the MIT or GPL Version 2 licenses.\n"
" * http://jquery.org/license\n"
" *\n"
" * http://docs.jquery.com/UI/Mouse\n"
" *\n"
" * Depends:\n"
" *	jquery.ui.widget.js\n"
" */\n"
"(function(a,b){var c=!1;a(document).mouseup(function(a){c=!1}),a.widget(\"ui.mouse\",{options:{cancel:\":input,option\",distance:1,delay:0},_mouseInit:function(){var b=this;this.element.bind(\"mousedown.\"+this.widgetName,function(a){return b._mouseDown(a)}).bind(\"click.\"+this.widgetName,function(c){if(!0===a.data(c.target,b.widgetName+\".preventClickEvent\")){a.removeData(c.target,b.widgetName+\".preventClickEvent\"),c.stopImmediatePropagation();return!1}}),this.started=!1},_mouseDestroy:function(){this.element.unbind(\".\"+this.widgetName)},_mouseDown:function(b){if(!c){this._mouseStarted&&this._mouseUp(b),this._mouseDownEvent=b;var d=this,e=b.which==1,f=typeof this.options.cancel==\"string\"&&b.target.nodeName?a(b.target).closest(this.options.cancel).length:!1;if(!e||f||!this._mouseCapture(b))return!0;this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){d.mouseDelayMet=!0},this.options.delay));if(this._mouseDistanceMet(b)&&this._mouseDelayMet(b)){this._mouseStarted=this._mouseStart(b)!==!1;if(!this._mouseStarted){b.preventDefault();return!0}}!0===a.data(b.target,this.widgetName+\".preventClickEvent\")&&a.removeData(b.target,this.widgetName+\".preventClickEvent\"),this._mouseMoveDelegate=function(a){return d._mouseMove(a)},this._mouseUpDelegate=function(a){return d._mouseUp(a)},a(document).bind(\"mousemove.\"+this.widgetName,this._mouseMoveDelegate).bind(\"mouseup.\"+this.widgetName,this._mouseUpDelegate),b.preventDefault(),c=!0;return!0}},_mouseMove:function(b){if(a.browser.msie&&!(document.documentMode>=9)&&!b.button)return this._mouseUp(b);if(this._mouseStarted){this._mouseDrag(b);return b.preventDefault()}this._mouseDistanceMet(b)&&this._mouseDelayMet(b)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,b)!==!1,this._mouseStarted?this._mouseDrag(b):this._mouseUp(b));return!this._mouseStarted},_mouseUp:function(b){a(document).unbind(\"mousemove.\"+this.widgetName,this._mouseMoveDelegate).unbind(\"mouseup.\"+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,b.target==this._mouseDownEvent.target&&a.data(b.target,this.widgetName+\".preventClickEvent\",!0),this._mouseStop(b));return!1},_mouseDistanceMet:function(a){return Math.max(Math.abs(this._mouseDownEvent.pageX-a.pageX),Math.abs(this._mouseDownEvent.pageY-a.pageY))>=this.options.distance},_mouseDelayMet:function(a){return this.mouseDelayMet},_mouseStart:function(a){},_mouseDrag:function(a){},_mouseStop:function(a){},_mouseCapture:function(a){return!0}})})(jQuery);\n"
"/*\n"
" * jQuery UI Resizable 1.8.18\n"
" *\n"
" * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)\n"
" * Dual licensed under the MIT or GPL Version 2 licenses.\n"
" * http://jquery.org/license\n"
" *\n"
" * http://docs.jquery.com/UI/Resizables\n"
" *\n"
" * Depends:\n"
" *	jquery.ui.core.js\n"
" *	jquery.ui.mouse.js\n"
" *	jquery.ui.widget.js\n"
" */\n"
"(function(a,b){a.widget(\"ui.resizable\",a.ui.mouse,{widgetEventPrefix:\"resize\",options:{alsoResize:!1,animate:!1,animateDuration:\"slow\",animateEasing:\"swing\",aspectRatio:!1,autoHide:!1,containment:!1,ghost:!1,grid:!1,handles:\"e,s,se\",helper:!1,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:1e3},_create:function(){var b=this,c=this.options;this.element.addClass(\"ui-resizable\"),a.extend(this,{_aspectRatio:!!c.aspectRatio,aspectRatio:c.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:c.helper||c.ghost||c.animate?c.helper||\"ui-resizable-helper\":null}),this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)&&(this.element.wrap(a('<div class=\"ui-wrapper\" style=\"overflow: hidden;\"></div>').css({position:this.element.css(\"position\"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css(\"top\"),left:this.element.css(\"left\")})),this.element=this.element.parent().data(\"resizable\",this.element.data(\"resizable\")),this.elementIsWrapper=!0,this.element.css({marginLeft:this.originalElement.css(\"marginLeft\"),marginTop:this.originalElement.css(\"marginTop\"),marginRight:this.originalElement.css(\"marginRight\"),marginBottom:this.originalElement.css(\"marginBottom\")}),this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0}),this.originalResizeStyle=this.originalElement.css(\"resize\"),this.originalElement.css(\"resize\",\"none\"),this._proportionallyResizeElements.push(this.originalElement.css({position:\"static\",zoom:1,display:\"block\"})),this.originalElement.css({margin:this.originalElement.css(\"margin\")}),this._proportionallyResize()),this.handles=c.handles||(a(\".ui-resizable-handle\",this.element).length?{n:\".ui-resizable-n\",e:\".ui-resizable-e\",s:\".ui-resizable-s\",w:\".ui-resizable-w\",se:\".ui-resizable-se\",sw:\".ui-resizable-sw\",ne:\".ui-resizable-ne\",nw:\".ui-resizable-nw\"}:\"e,s,se\");if(this.handles.constructor==String){this.handles==\"all\"&&(this.handles=\"n,e,s,w,se,sw,ne,nw\");var d=this.handles.split(\",\");this.handles={};for(var e=0;e<d.length;e++){var f=a.trim(d[e]),g=\"ui-resizable-\"+f,h=a('<div class=\"ui-resizable-handle '+g+'\"></div>');/sw|se|ne|nw/.test(f)&&h.css({zIndex:++c.zIndex}),\"se\"==f&&h.addClass(\"ui-icon ui-icon-gripsmall-diagonal-se\"),this.handles[f]=\".ui-resizable-\"+f,this.element.append(h)}}this._renderAxis=function(b){b=b||this.element;for(var c in this.handles){this.handles[c].constructor==String&&(this.handles[c]=a(this.handles[c],this.element).show());if(this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)){var d=a(this.handles[c],this.element),e=0;e=/sw|ne|nw|se|n|s/.test(c)?d.outerHeight():d.outerWidth();var f=[\"padding\",/ne|nw|n/.test(c)?\"Top\":/se|sw|s/.test(c)?\"Bottom\":/^e$/.test(c)?\"Right\":\"Left\"].join(\"\");b.css(f,e),this._proportionallyResize()}if(!a(this.handles[c]).length)continue}},this._renderAxis(this.element),this._handles=a(\".ui-resizable-handle\",this.element).disableSelection(),this._handles.mouseover(function(){if(!b.resizing){if(this.className)var a=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);b.axis=a&&a[1]?a[1]:\"se\"}}),c.autoHide&&(this._handles.hide(),a(this.element).addClass(\"ui-resizable-autohide\").hover(function(){c.disabled||(a(this).removeClass(\"ui-resizable-autohide\"),b._handles.show())},function(){c.disabled||b.resizing||(a(this).addClass(\"ui-resizable-autohide\"),b._handles.hide())})),this._mouseInit()},destroy:function(){this._mouseDestroy();var b=function(b){a(b).removeClass(\"ui-resizable ui-resizable-disabled ui-resizable-resizing\").removeData(\"resizable\").unbind(\".resizable\").find(\".ui-resizable-handle\").remove()};if(this.elementIsWrapper){b(this.element);var c=this.element;c.after(this.originalElement.css({position:c.css(\"position\"),width:c.outerWidth(),height:c.outerHeight(),top:c.css(\"top\"),left:c.css(\"left\")})).remove()}this.originalElement.css(\"resize\",this.originalResizeStyle),b(this.originalElement);return this},_mouseCapture:function(b){var c=!1;for(var d in this.handles)a(this.handles[d])[0]==b.target&&(c=!0);return!this.options.disabled&&c},_mouseStart:function(b){var d=this.options,e=this.element.position(),f=this.element;this.resizing=!0,this.documentScroll={top:a(document).scrollTop(),left:a(document).scrollLeft()},(f.is(\".ui-draggable\")||/absolute/.test(f.css(\"position\")))&&f.css({position:\"absolute\",top:e.top,left:e.left}),this._renderProxy();var g=c(this.helper.css(\"left\")),h=c(this.helper.css(\"top\"));d.containment&&(g+=a(d.containment).scrollLeft()||0,h+=a(d.containment).scrollTop()||0),this.offset=this.helper.offset(),this.position={left:g,top:h},this.size=this._helper?{width:f.outerWidth(),height:f.outerHeight()}:{width:f.width(),height:f.height()},this.originalSize=this._helper?{width:f.outerWidth(),height:f.outerHeight()}:{width:f.width(),height:f.height()},this.originalPosition={left:g,top:h},this.sizeDiff={width:f.outerWidth()-f.width(),height:f.outerHeight()-f.height()},this.originalMousePosition={left:b.pageX,top:b.pageY},this.aspectRatio=typeof d.aspectRatio==\"number\"?d.aspectRatio:this.originalSize.width/this.originalSize.height||1;var i=a(\".ui-resizable-\"+this.axis).css(\"cursor\");a(\"body\").css(\"cursor\",i==\"auto\"?this.axis+\"-resize\":i),f.addClass(\"ui-resizable-resizing\"),this._propagate(\"start\",b);return!0},_mouseDrag:function(b){var c=this.helper,d=this.options,e={},f=this,g=this.originalMousePosition,h=this.axis,i=b.pageX-g.left||0,j=b.pageY-g.top||0,k=this._change[h];if(!k)return!1;var l=k.apply(this,[b,i,j]),m=a.browser.msie&&a.browser.version<7,n=this.sizeDiff;this._updateVirtualBoundaries(b.shiftKey);if(this._aspectRatio||b.shiftKey)l=this._updateRatio(l,b);l=this._respectSize(l,b),this._propagate(\"resize\",b),c.css({top:this.position.top+\"px\",left:this.position.left+\"px\",width:this.size.width+\"px\",height:this.size.height+\"px\"}),!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize(),this._updateCache(l),this._trigger(\"resize\",b,this.ui());return!1},_mouseStop:function(b){this.resizing=!1;var c=this.options,d=this;if(this._helper){var e=this._proportionallyResizeElements,f=e.length&&/textarea/i.test(e[0].nodeName),g=f&&a.ui.hasScroll(e[0],\"left\")?0:d.sizeDiff.height,h=f?0:d.sizeDiff.width,i={width:d.helper.width()-h,height:d.helper.height()-g},j=parseInt(d.element.css(\"left\"),10)+(d.position.left-d.originalPosition.left)||null,k=parseInt(d.element.css(\"top\"),10)+(d.position.top-d.originalPosition.top)||null;c.animate||this.element.css(a.extend(i,{top:k,left:j})),d.helper.height(d.size.height),d.helper.width(d.size.width),this._helper&&!c.animate&&this._proportionallyResize()}a(\"body\").css(\"cursor\",\"auto\"),this.element.removeClass(\"ui-resizable-resizing\"),this._propagate(\"stop\",b),this._helper&&this.helper.remove();return!1},_updateVirtualBoundaries:function(a){var b=this.options,c,e,f,g,h;h={minWidth:d(b.minWidth)?b.minWidth:0,maxWidth:d(b.maxWidth)?b.maxWidth:Infinity,minHeight:d(b.minHeight)?b.minHeight:0,maxHeight:d(b.maxHeight)?b.maxHeight:Infinity};if(this._aspectRatio||a)c=h.minHeight*this.aspectRatio,f=h.minWidth/this.aspectRatio,e=h.maxHeight*this.aspectRatio,g=h.maxWidth/this.aspectRatio,c>h.minWidth&&(h.minWidth=c),f>h.minHeight&&(h.minHeight=f),e<h.maxWidth&&(h.maxWidth=e),g<h.maxHeight&&(h.maxHeight=g);this._vBoundaries=h},_updateCache:function(a){var b=this.options;this.offset=this.helper.offset(),d(a.left)&&(this.position.left=a.left),d(a.top)&&(this.position.top=a.top),d(a.height)&&(this.size.height=a.height),d(a.width)&&(this.size.width=a.width)},_updateRatio:function(a,b){var c=this.options,e=this.position,f=this.size,g=this.axis;d(a.height)?a.width=a.height*this.aspectRatio:d(a.width)&&(a.height=a.width/this.aspectRatio),g==\"sw\"&&(a.left=e.left+(f.width-a.width),a.top=null),g==\"nw\"&&(a.top=e.top+(f.height-a.height),a.left=e.left+(f.width-a.width));return a},_respectSize:function(a,b){var c=this.helper,e=this._vBoundaries,f=this._aspectRatio||b.shiftKey,g=this.axis,h=d(a.width)&&e.maxWidth&&e.maxWidth<a.width,i=d(a.height)&&e.maxHeight&&e.maxHeight<a.height,j=d(a.width)&&e.minWidth&&e.minWidth>a.width,k=d(a.height)&&e.minHeight&&e.minHeight>a.height;j&&(a.width=e.minWidth),k&&(a.height=e.minHeight),h&&(a.width=e.maxWidth),i&&(a.height=e.maxHeight);var l=this.originalPosition.left+this.originalSize.width,m=this.position.top+this.size.height,n=/sw|nw|w/.test(g),o=/nw|ne|n/.test(g);j&&n&&(a.left=l-e.minWidth),h&&n&&(a.left=l-e.maxWidth),k&&o&&(a.top=m-e.minHeight),i&&o&&(a.top=m-e.maxHeight);var p=!a.width&&!a.height;p&&!a.left&&a.top?a.top=null:p&&!a.top&&a.left&&(a.left=null);return a},_proportionallyResize:function(){var b=this.options;if(!!this._proportionallyResizeElements.length){var c=this.helper||this.element;for(var d=0;d<this._proportionallyResizeElements.length;d++){var e=this._proportionallyResizeElements[d];if(!this.borderDif){var f=[e.css(\"borderTopWidth\"),e.css(\"borderRightWidth\"),e.css(\"borderBottomWidth\"),e.css(\"borderLeftWidth\")],g=[e.css(\"paddingTop\"),e.css(\"paddingRight\"),e.css(\"paddingBottom\"),e.css(\"paddingLeft\")];this.borderDif=a.map(f,function(a,b){var c=parseInt(a,10)||0,d=parseInt(g[b],10)||0;return c+d})}if(a.browser.msie&&(!!a(c).is(\":hidden\")||!!a(c).parents(\":hidden\").length))continue;e.css({height:c.height()-this.borderDif[0]-this.borderDif[2]||0,width:c.width()-this.borderDif[1]-this.borderDif[3]||0})}}},_renderProxy:function(){var b=this.element,c=this.options;this.elementOffset=b.offset();if(this._helper){this.helper=this.helper||a('<div style=\"overflow:hidden;\"></div>');var d=a.browser.msie&&a.browser.version<7,e=d?1:0,f=d?2:-1;this.helper.addClass(this._helper).css({width:this.element.outerWidth()+f,height:this.element.outerHeight()+f,position:\"absolute\",left:this.elementOffset.left-e+\"px\",top:this.elementOffset.top-e+\"px\",zIndex:++c.zIndex}),this.helper.appendTo(\"body\").disableSelection()}else this.helper=this.element},_change:{e:function(a,b,c){return{width:this.originalSize.width+b}},w:function(a,b,c){var d=this.options,e=this.originalSize,f=this.originalPosition;return{left:f.left+b,width:e.width-b}},n:function(a,b,c){var d=this.options,e=this.originalSize,f=this.originalPosition;return{top:f.top+c,height:e.height-c}},s:function(a,b,c){return{height:this.originalSize.height+c}},se:function(b,c,d){return a.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[b,c,d]))},sw:function(b,c,d){return a.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[b,c,d]))},ne:function(b,c,d){return a.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[b,c,d]))},nw:function(b,c,d){return a.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[b,c,d]))}},_propagate:function(b,c){a.ui.plugin.call(this,b,[c,this.ui()]),b!=\"resize\"&&this._trigger(b,c,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}}}),a.extend(a.ui.resizable,{version:\"1.8.18\"}),a.ui.plugin.add(\"resizable\",\"alsoResize\",{start:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=function(b){a(b).each(function(){var b=a(this);b.data(\"resizable-alsoresize\",{width:parseInt(b.width(),10),height:parseInt(b.height(),10),left:parseInt(b.css(\"left\"),10),top:parseInt(b.css(\"top\"),10)})})};typeof e.alsoResize==\"object\"&&!e.alsoResize.parentNode?e.alsoResize.length?(e.alsoResize=e.alsoResize[0],f(e.alsoResize)):a.each(e.alsoResize,function(a){f(a)}):f(e.alsoResize)},resize:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=d.originalSize,g=d.originalPosition,h={height:d.size.height-f.height||0,width:d.size.width-f.width||0,top:d.position.top-g.top||0,left:d.position.left-g.left||0},i=function(b,d){a(b).each(function(){var b=a(this),e=a(this).data(\"resizable-alsoresize\"),f={},g=d&&d.length?d:b.parents(c.originalElement[0]).length?[\"width\",\"height\"]:[\"width\",\"height\",\"top\",\"left\"];a.each(g,function(a,b){var c=(e[b]||0)+(h[b]||0);c&&c>=0&&(f[b]=c||null)}),b.css(f)})};typeof e.alsoResize==\"object\"&&!e.alsoResize.nodeType?a.each(e.alsoResize,function(a,b){i(a,b)}):i(e.alsoResize)},stop:function(b,c){a(this).removeData(\"resizable-alsoresize\")}}),a.ui.plugin.add(\"resizable\",\"animate\",{stop:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=d._proportionallyResizeElements,g=f.length&&/textarea/i.test(f[0].nodeName),h=g&&a.ui.hasScroll(f[0],\"left\")?0:d.sizeDiff.height,i=g?0:d.sizeDiff.width,j={width:d.size.width-i,height:d.size.height-h},k=parseInt(d.element.css(\"left\"),10)+(d.position.left-d.originalPosition.left)||null,l=parseInt(d.element.css(\"top\"),10)+(d.position.top-d.originalPosition.top)||null;d.element.animate(a.extend(j,l&&k?{top:l,left:k}:{}),{duration:e.animateDuration,easing:e.animateEasing,step:function(){var c={width:parseInt(d.element.css(\"width\"),10),height:parseInt(d.element.css(\"height\"),10),top:parseInt(d.element.css(\"top\"),10),left:parseInt(d.element.css(\"left\"),10)};f&&f.length&&a(f[0]).css({width:c.width,height:c.height}),d._updateCache(c),d._propagate(\"resize\",b)}})}}),a.ui.plugin.add(\"resizable\",\"containment\",{start:function(b,d){var e=a(this).data(\"resizable\"),f=e.options,g=e.element,h=f.containment,i=h instanceof a?h.get(0):/parent/.test(h)?g.parent().get(0):h;if(!!i){e.containerElement=a(i);if(/document/.test(h)||h==document)e.containerOffset={left:0,top:0},e.containerPosition={left:0,top:0},e.parentData={element:a(document),left:0,top:0,width:a(document).width(),height:a(document).height()||document.body.parentNode.scrollHeight};else{var j=a(i),k=[];a([\"Top\",\"Right\",\"Left\",\"Bottom\"]).each(function(a,b){k[a]=c(j.css(\"padding\"+b))}),e.containerOffset=j.offset(),e.containerPosition=j.position(),e.containerSize={height:j.innerHeight()-k[3],width:j.innerWidth()-k[1]};var l=e.containerOffset,m=e.containerSize.height,n=e.containerSize.width,o=a.ui.hasScroll(i,\"left\")?i.scrollWidth:n,p=a.ui.hasScroll(i)?i.scrollHeight:m;e.parentData={element:i,left:l.left,top:l.top,width:o,height:p}}}},resize:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=d.containerSize,g=d.containerOffset,h=d.size,i=d.position,j=d._aspectRatio||b.shiftKey,k={top:0,left:0},l=d.containerElement;l[0]!=document&&/static/.test(l.css(\"position\"))&&(k=g),i.left<(d._helper?g.left:0)&&(d.size.width=d.size.width+(d._helper?d.position.left-g.left:d.position.left-k.left),j&&(d.size.height=d.size.width/e.aspectRatio),d.position.left=e.helper?g.left:0),i.top<(d._helper?g.top:0)&&(d.size.height=d.size.height+(d._helper?d.position.top-g.top:d.position.top),j&&(d.size.width=d.size.height*e.aspectRatio),d.position.top=d._helper?g.top:0),d.offset.left=d.parentData.left+d.position.left,d.offset.top=d.parentData.top+d.position.top;var m=Math.abs((d._helper?d.offset.left-k.left:d.offset.left-k.left)+d.sizeDiff.width),n=Math.abs((d._helper?d.offset.top-k.top:d.offset.top-g.top)+d.sizeDiff.height),o=d.containerElement.get(0)==d.element.parent().get(0),p=/relative|absolute/.test(d.containerElement.css(\"position\"));o&&p\n"
"&&(m-=d.parentData.left),m+d.size.width>=d.parentData.width&&(d.size.width=d.parentData.width-m,j&&(d.size.height=d.size.width/d.aspectRatio)),n+d.size.height>=d.parentData.height&&(d.size.height=d.parentData.height-n,j&&(d.size.width=d.size.height*d.aspectRatio))},stop:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=d.position,g=d.containerOffset,h=d.containerPosition,i=d.containerElement,j=a(d.helper),k=j.offset(),l=j.outerWidth()-d.sizeDiff.width,m=j.outerHeight()-d.sizeDiff.height;d._helper&&!e.animate&&/relative/.test(i.css(\"position\"))&&a(this).css({left:k.left-h.left-g.left,width:l,height:m}),d._helper&&!e.animate&&/static/.test(i.css(\"position\"))&&a(this).css({left:k.left-h.left-g.left,width:l,height:m})}}),a.ui.plugin.add(\"resizable\",\"ghost\",{start:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=d.size;d.ghost=d.originalElement.clone(),d.ghost.css({opacity:.25,display:\"block\",position:\"relative\",height:f.height,width:f.width,margin:0,left:0,top:0}).addClass(\"ui-resizable-ghost\").addClass(typeof e.ghost==\"string\"?e.ghost:\"\"),d.ghost.appendTo(d.helper)},resize:function(b,c){var d=a(this).data(\"resizable\"),e=d.options;d.ghost&&d.ghost.css({position:\"relative\",height:d.size.height,width:d.size.width})},stop:function(b,c){var d=a(this).data(\"resizable\"),e=d.options;d.ghost&&d.helper&&d.helper.get(0).removeChild(d.ghost.get(0))}}),a.ui.plugin.add(\"resizable\",\"grid\",{resize:function(b,c){var d=a(this).data(\"resizable\"),e=d.options,f=d.size,g=d.originalSize,h=d.originalPosition,i=d.axis,j=e._aspectRatio||b.shiftKey;e.grid=typeof e.grid==\"number\"?[e.grid,e.grid]:e.grid;var k=Math.round((f.width-g.width)/(e.grid[0]||1))*(e.grid[0]||1),l=Math.round((f.height-g.height)/(e.grid[1]||1))*(e.grid[1]||1);/^(se|s|e)$/.test(i)?(d.size.width=g.width+k,d.size.height=g.height+l):/^(ne)$/.test(i)?(d.size.width=g.width+k,d.size.height=g.height+l,d.position.top=h.top-l):/^(sw)$/.test(i)?(d.size.width=g.width+k,d.size.height=g.height+l,d.position.left=h.left-k):(d.size.width=g.width+k,d.size.height=g.height+l,d.position.top=h.top-l,d.position.left=h.left-k)}});var c=function(a){return parseInt(a,10)||0},d=function(a){return!isNaN(parseInt(a,10))}})(jQuery);\n"
"/*\n"
" * jQuery hashchange event - v1.3 - 7/21/2010\n"
" * http://benalman.com/projects/jquery-hashchange-plugin/\n"
" * \n"
" * Copyright (c) 2010 \"Cowboy\" Ben Alman\n"
" * Dual licensed under the MIT and GPL licenses.\n"
" * http://benalman.com/about/license/\n"
" */\n"
"(function($,e,b){var c=\"hashchange\",h=document,f,g=$.event.special,i=h.documentMode,d=\"on\"+c in e&&(i===b||i>7);function a(j){j=j||location.href;return\"#\"+j.replace(/^[^#]*#?(.*)$/,\"$1\")}$.fn[c]=function(j){return j?this.bind(c,j):this.trigger(c)};$.fn[c].delay=50;g[c]=$.extend(g[c],{setup:function(){if(d){return false}$(f.start)},teardown:function(){if(d){return false}$(f.stop)}});f=(function(){var j={},p,m=a(),k=function(q){return q},l=k,o=k;j.start=function(){p||n()};j.stop=function(){p&&clearTimeout(p);p=b};function n(){var r=a(),q=o(m);if(r!==m){l(m=r,q);$(e).trigger(c)}else{if(q!==m){location.href=location.href.replace(/#.*/,\"\")+q}}p=setTimeout(n,$.fn[c].delay)}$.browser.msie&&!d&&(function(){var q,r;j.start=function(){if(!q){r=$.fn[c].src;r=r&&r+a();q=$('<iframe tabindex=\"-1\" title=\"empty\"/>').hide().one(\"load\",function(){r||l(a());n()}).attr(\"src\",r||\"javascript:0\").insertAfter(\"body\")[0].contentWindow;h.onpropertychange=function(){try{if(event.propertyName===\"title\"){q.document.title=h.title}}catch(s){}}}};j.stop=k;o=function(){return a(q.location.href)};l=function(v,s){var u=q.document,t=$.fn[c].domain;if(v!==s){u.title=h.title;u.open();t&&u.write('<script>document.domain=\"'+t+'\"<\\/script>');u.close();q.location.hash=v}}})();return j})()})(jQuery,this);\n"
;

static const char search_jquery_script5[]=
// #include "jquery_fx_js.h"
"/**\n"
" * jQuery.ScrollTo - Easy element scrolling using jQuery.\n"
" * Copyright (c) 2007-2009 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com\n"
" * Dual licensed under MIT and GPL.\n"
" * Date: 5/25/2009\n"
" * @author Ariel Flesler\n"
" * @version 1.4.2\n"
" *\n"
" * http://flesler.blogspot.com/2007/10/jqueryscrollto.html\n"
" */\n"
";(function(d){var k=d.scrollTo=function(a,i,e){d(window).scrollTo(a,i,e)};k.defaults={axis:'xy',duration:parseFloat(d.fn.jquery)>=1.3?0:1};k.window=function(a){return d(window)._scrollable()};d.fn._scrollable=function(){return this.map(function(){var a=this,i=!a.nodeName||d.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!i)return a;var e=(a.contentWindow||a).document||a.ownerDocument||a;return d.browser.safari||e.compatMode=='BackCompat'?e.body:e.documentElement})};d.fn.scrollTo=function(n,j,b){if(typeof j=='object'){b=j;j=0}if(typeof b=='function')b={onAfter:b};if(n=='max')n=9e9;b=d.extend({},k.defaults,b);j=j||b.speed||b.duration;b.queue=b.queue&&b.axis.length>1;if(b.queue)j/=2;b.offset=p(b.offset);b.over=p(b.over);return this._scrollable().each(function(){var q=this,r=d(q),f=n,s,g={},u=r.is('html,body');switch(typeof f){case'number':case'string':if(/^([+-]=)?\\d+(\\.\\d+)?(px|%)?$/.test(f)){f=p(f);break}f=d(f,this);case'object':if(f.is||f.style)s=(f=d(f)).offset()}d.each(b.axis.split(''),function(a,i){var e=i=='x'?'Left':'Top',h=e.toLowerCase(),c='scroll'+e,l=q[c],m=k.max(q,i);if(s){g[c]=s[h]+(u?0:l-r.offset()[h]);if(b.margin){g[c]-=parseInt(f.css('margin'+e))||0;g[c]-=parseInt(f.css('border'+e+'Width'))||0}g[c]+=b.offset[h]||0;if(b.over[h])g[c]+=f[i=='x'?'width':'height']()*b.over[h]}else{var o=f[h];g[c]=o.slice&&o.slice(-1)=='%'?parseFloat(o)/100*m:o}if(/^\\d+$/.test(g[c]))g[c]=g[c]<=0?0:Math.min(g[c],m);if(!a&&b.queue){if(l!=g[c])t(b.onAfterFirst);delete g[c]}});t(b.onAfter);function t(a){r.animate(g,j,b.easing,a&&function(){a.call(this,n,b)})}}).end()};k.max=function(a,i){var e=i=='x'?'Width':'Height',h='scroll'+e;if(!d(a).is('html,body'))return a[h]-d(a)[e.toLowerCase()]();var c='client'+e,l=a.ownerDocument.documentElement,m=a.ownerDocument.body;return Math.max(l[h],m[h])-Math.min(l[c],m[c])};function p(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);\n"
;

static const char svgpan_script[]=
// #include "svgpan_js.h"
"/**\n"
" * The code below is based on SVGPan Library 1.2 and was modified for doxygen\n"
" * to support both zooming and panning via the mouse and via embedded bottons.\n"
" *\n"
" * This code is licensed under the following BSD license:\n"
" *\n"
" * Copyright 2009-2010 Andrea Leofreddi <a.leofreddi@itcharm.com>. All rights reserved.\n"
" * \n"
" * Redistribution and use in source and binary forms, with or without modification, are\n"
" * permitted provided that the following conditions are met:\n"
" * \n"
" *    1. Redistributions of source code must retain the above copyright notice, this list of\n"
" *       conditions and the following disclaimer.\n"
" * \n"
" *    2. Redistributions in binary form must reproduce the above copyright notice, this list\n"
" *       of conditions and the following disclaimer in the documentation and/or other materials\n"
" *       provided with the distribution.\n"
" * \n"
" * THIS SOFTWARE IS PROVIDED BY Andrea Leofreddi ``AS IS'' AND ANY EXPRESS OR IMPLIED\n"
" * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND\n"
" * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Andrea Leofreddi OR\n"
" * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR\n"
" * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR\n"
" * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON\n"
" * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING\n"
" * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF\n"
" * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n"
" * \n"
" * The views and conclusions contained in the software and documentation are those of the\n"
" * authors and should not be interpreted as representing official policies, either expressed\n"
" * or implied, of Andrea Leofreddi.\n"
" */\n"
"\n"
"var root = document.documentElement;\n"
"var state = 'none';\n"
"var stateOrigin;\n"
"var stateTf = root.createSVGMatrix();\n"
"var cursorGrab = ' url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAMAAAAolt3jAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAlQTFRFAAAA////////c3ilYwAAAAN0Uk5T//8A18oNQQAAAD1JREFUeNp0zlEKACAIA9Bt9z90bZBZkQj29qFBEuBOzQHSnWTTyckEfqUuZgFvslH4ch3qLCO/Kr8cAgwATw4Ax6XRCcoAAAAASUVORK5CYII=\"), move';\n"
"var zoomSteps = 10;\n"
"var zoomInFactor;\n"
"var zoomOutFactor;\n"
"var windowWidth;\n"
"var windowHeight;\n"
"var svgDoc;\n"
"var minZoom;\n"
"var maxZoom;\n"
"if (!window) window=this;\n"
"\n"
"/**\n"
" * Show the graph in the middle of the view, scaled to fit \n"
" */\n"
"function show()\n"
"{\n"
"  if (window.innerHeight) // Firefox\n"
"  {\n"
"    windowWidth = window.innerWidth;\n"
"    windowHeight = window.innerHeight;\n"
"  }\n"
"  else if (document.documentElement.clientWidth) // Chrome/Safari\n"
"  {\n"
"    windowWidth = document.documentElement.clientWidth\n"
"    windowHeight = document.documentElement.clientHeight\n"
"  }\n"
"  if (!windowWidth || !windowHeight) // failsafe\n"
"  {\n"
"    windowWidth = 800;\n"
"    windowHeight = 600;\n"
"  }\n"
"  minZoom = Math.min(Math.min(viewHeight,windowHeight)/viewHeight,Math.min(viewWidth,windowWidth)/viewWidth);\n"
"  maxZoom = minZoom+1.5;\n"
"  zoomInFactor = Math.pow(maxZoom/minZoom,1.0/zoomSteps);\n"
"  zoomOutFactor = 1.0/zoomInFactor;\n"
"\n"
"  var g = svgDoc.getElementById('viewport');\n"
"  try\n"
"  {\n"
"    var bb = g.getBBox(); // this can throw an exception if css { display: none }\n"
"    var tx = (windowWidth-viewWidth*minZoom+8)/(2*minZoom);\n"
"    var ty = viewHeight+(windowHeight-viewHeight*minZoom)/(2*minZoom);\n"
"    var a = 'scale('+minZoom+') rotate(0) translate('+tx+' '+ty+')';\n"
"    g.setAttribute('transform',a);\n"
"  }\n"
"  catch(e) {}\n"
"}\n"
"\n"
"/**\n"
" * Register handlers\n"
" */\n"
"function init(evt) \n"
"{\n"
"  svgDoc = evt.target.ownerDocument;\n"
"  if (top.window && top.window.registerShow) // register show function in html doc for dynamic sections\n"
"  {\n"
"    top.window.registerShow(sectionId,show);\n"
"  }\n"
"  show();\n"
"\n"
"  setAttributes(root, {\n"
"     \"onmousedown\" : \"handleMouseDown(evt)\",\n"
"     \"onmousemove\" : \"handleMouseMove(evt)\",\n"
"     \"onmouseup\"   : \"handleMouseUp(evt)\"\n"
"  });\n"
"\n"
"  if (window.addEventListener)\n"
"  {\n"
"    if (navigator.userAgent.toLowerCase().indexOf('webkit') >= 0 || \n"
"        navigator.userAgent.toLowerCase().indexOf(\"opera\") >= 0 || \n"
"        navigator.appVersion.indexOf(\"MSIE\") != -1)\n"
"    {\n"
"      window.addEventListener('mousewheel', handleMouseWheel, false); // Chrome/Safari/IE9\n"
"    }\n"
"    else\n"
"    {\n"
"      window.addEventListener('DOMMouseScroll', handleMouseWheel, false); // Others\n"
"    }\n"
"  }\n"
"}\n"
"\n"
"window.onresize=function()\n"
"{\n"
"  if (svgDoc) { show(); }\n"
"}\n"
"\n"
"/**\n"
" * Instance an SVGPoint object with given event coordinates.\n"
" */\n"
"function getEventPoint(evt) \n"
"{\n"
"  var p = root.createSVGPoint();\n"
"  p.x = evt.clientX;\n"
"  p.y = evt.clientY;\n"
"  return p;\n"
"}\n"
"\n"
"/**\n"
" * Sets the current transform matrix of an element.\n"
" */\n"
"function setCTM(element, matrix) \n"
"{\n"
"  var s = \"matrix(\" + matrix.a + \",\" + matrix.b + \",\" + matrix.c + \",\" + matrix.d + \",\" + matrix.e + \",\" + matrix.f + \")\";\n"
"  element.setAttribute(\"transform\", s);\n"
"}\n"
"\n"
"/**\n"
" * Sets attributes of an element.\n"
" */\n"
"function setAttributes(element, attributes)\n"
"{\n"
"  for (i in attributes)\n"
"    element.setAttributeNS(null, i, attributes[i]);\n"
"}\n"
"\n"
"function doZoom(g,point,zoomFactor)\n"
"{\n"
"  var p = point.matrixTransform(g.getCTM().inverse());\n"
"  var k = root.createSVGMatrix().translate(p.x, p.y).scale(zoomFactor).translate(-p.x, -p.y);\n"
"  var n = g.getCTM().multiply(k);\n"
"  var s = Math.max(n.a,n.d);\n"
"  if      (s>maxZoom) n=n.translate(p.x,p.y).scale(maxZoom/s).translate(-p.x,-p.y);\n"
"  else if (s<minZoom) n=n.translate(p.x,p.y).scale(minZoom/s).translate(-p.x,-p.y);\n"
"  setCTM(g, n);\n"
"  stateTf = stateTf.multiply(n.inverse());\n"
"}\n"
"\n"
"/**\n"
" * Handle mouse move event.\n"
" */\n"
"function handleMouseWheel(evt) \n"
"{\n"
"  if (!evt) evt = window.evt;\n"
"  if (!evt.shiftKey) return; // only zoom when shift is pressed\n"
"  if (evt.preventDefault) evt.preventDefault();\n"
"  evt.returnValue = false;\n"
"\n"
"  if (state!='pan')\n"
"  {\n"
"    var delta;\n"
"    if (evt.wheelDelta)\n"
"    {\n"
"      delta = evt.wheelDelta / 7200; // Opera/Chrome/IE9/Safari\n"
"    }\n"
"    else\n"
"    {\n"
"      delta = evt.detail / -180; // Mozilla\n"
"    }\n"
"    var svgDoc = evt.target.ownerDocument;\n"
"    var g = svgDoc.getElementById(\"viewport\");\n"
"    var p = getEventPoint(evt);\n"
"    doZoom(g,p,1+delta);\n"
"  }\n"
"}\n"
"\n"
"/**\n"
" * Handle mouse move event.\n"
" */\n"
"function handleMouseMove(evt) \n"
"{\n"
"  if(evt.preventDefault)\n"
"    evt.preventDefault();\n"
"\n"
"  evt.returnValue = false;\n"
"\n"
"  var g = svgDoc.getElementById(\"viewport\");\n"
"\n"
"  if (state == 'pan') \n"
"  {\n"
"    // Pan mode\n"
"    var p = getEventPoint(evt).matrixTransform(stateTf);\n"
"    setCTM(g,stateTf.inverse().translate(p.x - stateOrigin.x, p.y - stateOrigin.y));\n"
"  } \n"
"}\n"
"\n"
"/**\n"
" * Handle click event.\n"
" */\n"
"function handleMouseDown(evt) \n"
"{\n"
"  if(evt.preventDefault)\n"
"    evt.preventDefault();\n"
"  evt.returnValue = false;\n"
"  var g = svgDoc.getElementById(\"viewport\");\n"
"  state = 'pan';\n"
"  stateTf = g.getCTM().inverse();\n"
"  stateOrigin = getEventPoint(evt).matrixTransform(stateTf);\n"
"  g.style.cursor = cursorGrab;\n"
"}\n"
"\n"
"/**\n"
" * Handle mouse button release event.\n"
" */\n"
"function handleMouseUp(evt) \n"
"{\n"
"  if (evt.preventDefault) evt.preventDefault();\n"
"  evt.returnValue = false;\n"
"  var g = svgDoc.getElementById(\"viewport\");\n"
"  g.style.cursor = \"default\";\n"
"  // Quit pan mode\n"
"  state = '';\n"
"}\n"
"\n"
"/**\n"
" * Dumps a matrix to a string (useful for debug).\n"
" */\n"
"function dumpMatrix(matrix) \n"
"{\n"
"  var s = \"[ \" + matrix.a + \", \" + matrix.c + \", \" + matrix.e + \"\\n  \" + matrix.b + \", \" + matrix.d + \", \" + matrix.f + \"\\n  0, 0, 1 ]\";\n"
"  return s;\n"
"}\n"
"\n"
"/**\n"
" * Handler for pan buttons\n"
" */\n"
"function handlePan(x,y)\n"
"{\n"
"  var g = svgDoc.getElementById(\"viewport\");\n"
"  setCTM(g,g.getCTM().translate(x*20/minZoom,y*20/minZoom));\n"
"}\n"
"\n"
"/**\n"
" * Handle reset button\n"
" */\n"
"function handleReset()\n"
"{\n"
"  show();\n"
"}\n"
"\n"
"/**\n"
" * Handler for zoom buttons\n"
" */\n"
"function handleZoom(evt,direction)\n"
"{\n"
"  var g = svgDoc.getElementById(\"viewport\");\n"
"  var factor = direction=='in' ? zoomInFactor : zoomOutFactor;\n"
"  var m = g.getCTM();\n"
"  var p = root.createSVGPoint();\n"
"  p.x = windowWidth/2;\n"
"  p.y = windowHeight/2;\n"
"  doZoom(g,p,factor);\n"
"}\n"
"\n"
"function serializeXmlNode(xmlNode) \n"
"{\n"
"  if (typeof window.XMLSerializer != \"undefined\") {\n"
"    return (new window.XMLSerializer()).serializeToString(xmlNode);\n"
"  } else if (typeof xmlNode.xml != \"undefined\") {\n"
"    return xmlNode.xml;\n"
"  }\n"
"  return \"\";\n"
"}\n"
"\n"
"/** \n"
" * Handler for print function\n"
" */\n"
"function handlePrint(evt)\n"
"{\n"
"  evt.returnValue = false;\n"
"  var g = svgDoc.getElementById(\"graph\");\n"
"  var xs = serializeXmlNode(g);\n"
"  try {\n"
"    var w = window.open('about:blank','_blank','width='+windowWidth+',height='+windowHeight+\n"
"                        ',toolbar=0,status=0,menubar=0,scrollbars=0,resizable=0,location=0,directories=0');\n"
"    var d = w.document;\n"
"    d.write('<html xmlns=\"http://www.w3.org/1999/xhtml\" '+\n"
"            'xmlns:svg=\"http://www.w3.org/2000/svg\" '+\n"
"            'xmlns:xlink=\"http://www.w3.org/1999/xlink\">');\n"
"    d.write('<head><title>Print SVG</title></head>');\n"
"    d.write('<body style=\"margin: 0px; padding: 0px;\" onload=\"window.print();\">');\n"
"    d.write('<div id=\"svg\" style=\"width:'+windowWidth+'px; height:'+windowHeight+'px;\">'+xs+'</div>');\n"
"    d.write('</body>');\n"
"    d.write('</html>');\n"
"    d.close();\n"
"  } catch(e) {\n"
"    alert('Failed to open popup window needed for printing!\\n'+e.message);\n"
"  }\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
;

static const char dynsections_script[]=
// #include "dynsections_js.h"
"function toggleVisibility(linkObj)\n"
"{\n"
" var base = $(linkObj).attr('id');\n"
" var summary = $('#'+base+'-summary');\n"
" var content = $('#'+base+'-content');\n"
" var trigger = $('#'+base+'-trigger');\n"
" var src=$(trigger).attr('src');\n"
" if (content.is(':visible')===true) {\n"
"   content.hide();\n"
"   summary.show();\n"
"   $(linkObj).addClass('closed').removeClass('opened');\n"
"   $(trigger).attr('src',src.substring(0,src.length-8)+'closed.png');\n"
" } else {\n"
"   content.show();\n"
"   summary.hide();\n"
"   $(linkObj).removeClass('closed').addClass('opened');\n"
"   $(trigger).attr('src',src.substring(0,src.length-10)+'open.png');\n"
" } \n"
" return false;\n"
"}\n"
"\n"
"function updateStripes()\n"
"{\n"
"  $('table.directory tr').\n"
"       removeClass('even').filter(':visible:even').addClass('even');\n"
"}\n"
"function toggleLevel(level)\n"
"{\n"
"  $('table.directory tr').each(function(){ \n"
"    var l = this.id.split('_').length-1;\n"
"    var i = $('#img'+this.id.substring(3));\n"
"    var a = $('#arr'+this.id.substring(3));\n"
"    if (l<level+1) {\n"
"      i.attr('src','ftv2folderopen.png');\n"
"      a.attr('src','ftv2mnode.png');\n"
"      $(this).show();\n"
"    } else if (l==level+1) {\n"
"      i.attr('src','ftv2folderclosed.png');\n"
"      a.attr('src','ftv2pnode.png');\n"
"      $(this).show();\n"
"    } else {\n"
"      $(this).hide();\n"
"    }\n"
"  });\n"
"  updateStripes();\n"
"}\n"
"\n"
"function toggleFolder(id)\n"
"{\n"
"  //The clicked row\n"
"  var currentRow = $('#row_'+id);\n"
"  var currentRowImages = currentRow.find(\"img\");\n"
"\n"
"  //All rows after the clicked row\n"
"  var rows = currentRow.nextAll(\"tr\");\n"
"\n"
"  //Only match elements AFTER this one (can't hide elements before)\n"
"  var childRows = rows.filter(function() {\n"
"    var re = new RegExp('^row_'+id+'\\\\d+_$', \"i\"); //only one sub\n"
"    return this.id.match(re);\n"
"  });\n"
"\n"
"  //First row is visible we are HIDING\n"
"  if (childRows.filter(':first').is(':visible')===true) {\n"
"    currentRowImages.filter(\"[id^=arr]\").attr('src', 'ftv2pnode.png');\n"
"    currentRowImages.filter(\"[id^=img]\").attr('src', 'ftv2folderclosed.png');\n"
"    rows.filter(\"[id^=row_\"+id+\"]\").hide();\n"
"  } else { //We are SHOWING\n"
"    //All sub images\n"
"    var childImages = childRows.find(\"img\");\n"
"    var childImg = childImages.filter(\"[id^=img]\");\n"
"    var childArr = childImages.filter(\"[id^=arr]\");\n"
"\n"
"    currentRow.find(\"[id^=arr]\").attr('src', 'ftv2mnode.png'); //open row\n"
"    currentRow.find(\"[id^=img]\").attr('src', 'ftv2folderopen.png'); //open row\n"
"    childImg.attr('src','ftv2folderclosed.png'); //children closed\n"
"    childArr.attr('src','ftv2pnode.png'); //children closed\n"
"    childRows.show(); //show all children\n"
"  }\n"
"  updateStripes();\n"
"}\n"
"\n"
"\n"
"function toggleInherit(id)\n"
"{\n"
"  var rows = $('tr.inherit.'+id);\n"
"  var img = $('tr.inherit_header.'+id+' img');\n"
"  var src = $(img).attr('src');\n"
"  if (rows.filter(':first').is(':visible')===true) {\n"
"    rows.css('display','none');\n"
"    $(img).attr('src',src.substring(0,src.length-8)+'closed.png');\n"
"  } else {\n"
"    rows.css('display','table-row'); // using show() causes jump in firefox\n"
"    $(img).attr('src',src.substring(0,src.length-10)+'open.png');\n"
"  }\n"
"}\n"
"\n"
;

static const char extsearch_script[]=
// #include "extsearch_js.h"
"function SearchBox(name, resultsPath, inFrame, label)\n"
"{\n"
"  this.searchLabel = label;\n"
"  this.DOMSearchField = function()\n"
"  {  return document.getElementById(\"MSearchField\");  }\n"
"  this.DOMSearchBox = function()\n"
"  {  return document.getElementById(\"MSearchBox\");  }\n"
"  this.OnSearchFieldFocus = function(isActive)\n"
"  {\n"
"    if (isActive)\n"
"    {\n"
"      this.DOMSearchBox().className = 'MSearchBoxActive';\n"
"      var searchField = this.DOMSearchField();\n"
"      if (searchField.value == this.searchLabel) \n"
"      {\n"
"        searchField.value = '';\n"
"      }\n"
"    }\n"
"    else\n"
"    {\n"
"      this.DOMSearchBox().className = 'MSearchBoxInactive';\n"
"      this.DOMSearchField().value   = this.searchLabel;\n"
"    }\n"
"  }\n"
"}\n"
"\n"
"function trim(s) {\n"
"  return s?s.replace(/^\\s\\s*/, '').replace(/\\s\\s*$/, ''):'';\n"
"}\n"
"\n"
"function getURLParameter(name) {\n"
"  return decodeURIComponent((new RegExp('[?|&]'+name+\n"
"         '='+'([^&;]+?)(&|#|;|$)').exec(location.search)\n"
"         ||[,\"\"])[1].replace(/\\+/g, '%20'))||null;\n"
"}\n"
"\n"
"var entityMap = {\n"
"  \"&\": \"&amp;\",\n"
"  \"<\": \"&lt;\",\n"
"  \">\": \"&gt;\",\n"
"  '\"': '&quot;',\n"
"  \"'\": '&#39;',\n"
"  \"/\": '&#x2F;'\n"
"};\n"
"\n"
"function escapeHtml(s) {\n"
"  return String(s).replace(/[&<>\"'\\/]/g, function (s) {\n"
"    return entityMap[s];\n"
"  });\n"
"}\n"
"\n"
"function searchFor(query,page,count) {\n"
"  $.getJSON(serverUrl+\"?cb=?\",\n"
"  {\n"
"    n:count,\n"
"    p:page,\n"
"    q:query\n"
"  },\n"
"  function(data) {\n"
"    var results = $('#searchresults');\n"
"    $('#MSearchField').val(query);\n"
"    if (data.hits>0) {\n"
"      if (data.hits==1) {\n"
"        results.html('<p>'+searchResultsText[1]+'</p>');\n"
"      } else {\n"
"        results.html('<p>'+searchResultsText[2].replace(/\\$num/,data.hits)+'</p>');\n"
"      }\n"
"      var r='<table>';\n"
"      $.each(data.items, function(i,item){\n"
"        var prefix = tagMap[item.tag];\n"
"        if (prefix) prefix+='/'; else prefix='';\n"
"        r+='<tr class=\"searchresult\">'+\n"
"           '<td align=\"right\">'+(data.first+i+1)+'.</td>'+\n"
"           '<td>'+escapeHtml(item.type)+'&#160;'+\n"
"                '<a href=\"'+escapeHtml(prefix+item.url)+\n"
"                '\">'+escapeHtml(item.name)+'</a>';\n"
"        if (item.type==\"source\") {\n"
"          var l=item.url.match(/[1-9][0-9]*$/);\n"
"          if (l) r+=' at line '+parseInt(l[0]);\n"
"        }\n"
"        r+='</td>';\n"
"        for (var i=0;i<item.fragments.length;i++)\n"
"        {\n"
"          r+='<tr><td></td><td>'+item.fragments[i]+'</td></tr>';\n"
"        }\n"
"        r+='</tr>';\n"
"      });\n"
"      r+='</table>';\n"
"      if (data.pages>1) // write multi page navigation bar\n"
"      {\n"
"        r+='<div class=\"searchpages\">';\n"
"        if (data.page>0)\n"
"        {\n"
"          r+='<span class=\"pages\"><a href=\"javascript:searchFor(\\''+escapeHtml(query)+'\\','+(page-1).toString()+','+count.toString()+')\">&laquo;</a></span>&nbsp;';\n"
"        }\n"
"        var firstPage = data.page-5;\n"
"        var lastPage  = data.page+5;\n"
"        if (firstPage<0)\n"
"        {\n"
"          lastPage-=firstPage;\n"
"          firstPage=0;\n"
"        }  \n"
"        if (lastPage>data.pages)\n"
"        {\n"
"          lastPage=data.pages;\n"
"        }\n"
"        for(var i=firstPage;i<lastPage;i++)\n"
"        {\n"
"          if (i==data.page)\n"
"          {\n"
"            r+='<span class=\"pages\"><b>'+(i+1).toString()+'</b></span>&nbsp;';\n"
"          }\n"
"          else\n"
"          {\n"
"            r+='<span class=\"pages\"><a href=\"javascript:searchFor(\\''+escapeHtml(query)+'\\','+i.toString()+','+count.toString()+')\">'+(i+1).toString()+'</a></span>&nbsp;';\n"
"          }\n"
"        }\n"
"        if (data.page+1<data.pages)\n"
"        {\n"
"          r+='<span class=\"pages\"><a href=\"javascript:searchFor(\\''+escapeHtml(query)+'\\','+(page+1).toString()+','+count.toString()+')\">&raquo;</a></span>';\n"
"        }\n"
"        r+='</div>';\n"
"      }\n"
"      results.append(r);\n"
"    } else {\n"
"      results.html('<p>'+searchResultsText[0]+'</p>');\n"
"    }\n"
"  });\n"
"}\n"
;

static QCString g_header;
static QCString g_footer;

//------------------------- Pictures for the Tabs ------------------------

// active tab background luma
static unsigned char tab_a_png[36] =
{
   31,  42,  59,  69,  73,  74,  75,  77,  77,
   77,  79,  80,  80,  82,  81,  83,  84,  86,
   87,  88,  89,  90,  91,  91,  93,  94,  94,
   96,  96,  97,  98,  98,  99,  99,  99, 100
};

// normal tab background luma
static unsigned char tab_b_png[36] =
{
    218, 228, 235, 233, 230, 227, 225, 222, 221,
    218, 217, 215, 214, 213, 212, 211, 210, 209,
    209, 197, 198, 199, 200, 201, 202, 203, 204,
    205, 207, 209, 211, 213, 217, 219, 206, 188 
};

// hovering tab background luma
static unsigned char tab_h_png[36] =
{
    181, 191, 198, 196, 193, 190, 188, 185, 184,
    181, 180, 178, 177, 176, 175, 174, 173, 172,
    172, 154, 155, 156, 157, 158, 159, 160, 161,
    162, 164, 166, 168, 170, 174, 176, 163, 145
};

// shadowed header
static unsigned char header_png[12] = 
{
  255, 240, 241, 242, 243, 244, 
  245, 246, 247, 248, 249, 250
};

// function header
static unsigned char func_header_png[56] =
{
  248, 247, 246, 245, 244, 243, 242, 241,
  240, 239, 238, 237, 236, 235, 234, 233,
  232, 231, 230, 229, 228, 223, 223, 223,
  223, 223, 223, 223, 223, 223, 223, 223,
  224, 224, 224, 224, 225, 225, 225, 225,
  225, 226, 226, 226, 227, 227, 227, 227,
  228, 228, 228, 229, 229, 229, 229, 229
};

// tab separator
static unsigned char tab_s_png[36] =
{
  187, 186, 185, 183, 182, 181, 180, 178, 176,
  174, 173, 171, 169, 167, 164, 163, 161, 158,
  156, 154, 152, 150, 148, 145, 143, 141, 140,
  138, 136, 134, 131, 131, 128, 126, 125, 124
};

// breadcrumbs luma
static unsigned char bc_s_png[240] =
{
  150,187,187,148,148,148,148,148,
  147,175,186,147,147,147,147,147,
  146,153,185,185,146,146,146,146,
  144,144,177,183,144,144,144,144,
  144,144,159,182,144,144,144,144,
  143,143,144,179,181,143,143,143,
  142,142,142,165,180,142,142,142,
  141,141,141,144,178,178,141,141,
  139,139,139,139,167,176,139,139,
  137,137,137,137,146,174,137,137,
  137,137,137,137,137,169,173,137,
  135,135,135,135,135,150,171,135,
  133,133,133,133,133,135,167,169,
  132,132,132,132,132,132,154,167,
  129,129,129,129,129,129,140,164,
  129,129,129,129,129,129,154,163,
  127,127,127,127,127,128,161,161,
  125,125,125,125,125,141,158,125,
  123,123,123,123,123,152,156,123,
  121,121,121,121,129,154,121,121,
  120,120,120,120,143,152,120,120,
  118,118,118,120,150,150,118,118,
  117,117,117,132,148,117,117,117,
  114,114,114,142,145,114,114,114,
  113,113,120,143,113,113,113,113,
  111,111,133,141,111,111,111,111,
  110,112,140,140,110,110,110,110,
  109,124,138,109,109,109,109,109,
  107,133,136,107,107,107,107,107,
  111,134,106,106,106,106,106,106
};

// breadcrumbs alpha map
static unsigned char bc_s_a_png[240] =
{
  241,241, 21,  0,  0,  0,  0,  0,
  162,205,117,  0,  0,  0,  0,  0,
   54,231,225,  3,  0,  0,  0,  0,
    0,198,215, 78,  0,  0,  0,  0,
    0, 93,211,186,  0,  0,  0,  0,
    0,  6,232,235, 42,  0,  0,  0,
    0,  0,132,203,147,  0,  0,  0,
    0,  0, 27,242,241, 15,  0,  0,
    0,  0,  0,168,205,108,  0,  0,
    0,  0,  0, 63,228,219,  0,  0,
    0,  0,  0,  0,207,221, 72,  0,
    0,  0,  0,  0,102,208,177,  0,
    0,  0,  0,  0,  9,238,240, 36,
    0,  0,  0,  0,  0,138,201,138,
    0,  0,  0,  0,  0, 77,187,158,
    0,  0,  0,  0,  0,159,204,120,
    0,  0,  0,  0, 15,241,241, 21,
    0,  0,  0,  0,111,208,171,  0,
    0,  0,  0,  0,210,222, 66,  0,
    0,  0,  0, 60,227,219,  0,  0,
    0,  0,  0,162,204,114,  0,  0,
    0,  0, 18,238,238, 21,  0,  0,
    0,  0,114,205,165,  0,  0,  0,
    0,  0,216,225, 60,  0,  0,  0,
    0, 66,226,216,  0,  0,  0,  0,
    0,165,204,111,  0,  0,  0,  0,
   21,241,241, 18,  0,  0,  0,  0,
  117,203,159,  0,  0,  0,  0,  0,
  219,227, 57,  0,  0,  0,  0,  0,
  211,201,  0,  0,  0,  0,  0,  0
};

// doxygen logo luma
static unsigned char doxygen_png[3224] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,255,255,255,255,255,255,255,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 91, 91, 91, 91, 32, 32,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32,255,255,255,255, 32, 32,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32,253,253,253,253, 32, 32,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,253,255,255,255,255,255,255,255,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32,251,251,251,251, 32, 32,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,251,255,255,255,255,255,255,255,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32,249,249,249,249, 32, 32,249,249,249,249, 32, 32, 32, 32, 32, 32,249,249,249,249, 32, 32, 32, 32, 32, 32,249,249,249,249, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,249,249,249, 32, 32, 32, 32, 32,249,249,249,249, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,249,249,249,249,249,249, 32, 32, 32, 32, 32, 32, 32,249,249,249,249,249, 32, 32, 32, 32, 32,249, 32, 32, 32, 32, 32,255,255,255,
   32, 32, 32, 32, 46,132,190,190,147, 61,247,247,247,247, 32, 32,247,247, 32, 32,118,161,190,190,161,118, 32, 32,247, 32, 46, 89, 89, 89, 89, 46, 32,247,247, 32, 89, 89, 89, 89, 61, 89, 89, 89, 89, 46, 32,247, 32, 46, 89, 89, 89, 89, 32,247, 32, 32,118,175,190,161, 89, 61, 89, 89, 89, 61, 32,247,247,247, 32, 32,104,147,190,190,190,132, 89, 32, 32,247,247, 32, 46, 89, 89, 89, 75, 32, 89,161,190,161, 75, 32,255,255,
   32, 32, 32, 74,230,244,244,244,244,244,244,244,244,244, 32, 32,244, 32, 74,216,244,244,244,244,244,244,216, 74, 32,244, 32,187,244,244,244,159, 32,244, 32,117,244,244,244,230, 46,173,244,244,244,131, 32,244, 32,131,244,244,244,173, 32, 32, 46,173,244,244,244,244,244,230,244,244,244,131, 32,244,244, 32, 74,202,244,244,244,244,244,244,244,173, 46, 32,244, 32, 89,244,244,244,187,145,244,244,244,244,244, 89, 32,255,
   32, 32, 46,213,241,241,241,241,241,241,241,241,241,241, 32, 32, 32, 60,227,241,241,241,241,241,241,241,241,227, 60, 32, 32, 46,227,241,241,241,102, 32, 60,227,241,241,241, 88, 32,116,241,241,241,199, 32,241, 32,185,241,241,241,116, 32, 32,143,241,241,241,241,241,241,241,241,241,241,130, 32,241, 32, 74,227,241,241,241,199,185,241,241,241,241,171, 32,241, 32, 88,241,241,241,241,241,241,241,241,241,241,199, 32,255,
   32, 32,128,237,237,237,223,128, 87,128,237,237,237,237, 32, 32, 32,182,237,237,237,196,100,100,196,237,237,237,182, 32,237, 32,100,237,237,237,223, 59,196,237,237,237,141, 32, 32, 46,237,237,237,237, 59, 32, 46,237,237,237,237, 46, 32, 59,237,237,237,237,169, 87, 87,182,237,237,237,128, 32,237, 32,196,237,237,237, 87, 32, 32, 73,223,237,237,237, 73, 32, 32, 87,237,237,237,237,223,182,223,237,237,237,237, 46, 32,
   32, 32,207,234,234,234,113, 32, 32, 32,234,234,234,234, 32, 32, 59,234,234,234,221, 45, 32, 32, 45,221,234,234,234, 59, 32,234, 32,140,234,234,234,221,234,234,234,194, 32, 32,234, 32,167,234,234,234,126, 32, 99,234,234,234,167, 32, 32,126,234,234,234,180, 32, 32, 32,126,234,234,234,126, 32, 32, 99,234,234,234,167, 32, 32, 32, 32,153,234,234,234,126, 32, 32, 86,234,234,234,207, 45, 32, 45,234,234,234,234, 86, 32,
   32, 45,231,231,231,218, 32, 32, 32, 32,231,231,231,231, 32, 32, 98,231,231,231,165, 32,231,231, 32,165,231,231,231, 98, 32,231, 32, 45,191,231,231,231,231,231,218, 72, 32,231,231, 32, 98,231,231,231,165, 32,151,231,231,231,112, 32, 32,165,231,231,231,112, 32,231, 32,125,231,231,231,125, 32, 32,138,231,231,231,178,125,125,125,125,178,231,231,231,178, 32, 32, 85,231,231,231,178, 32,255, 32,191,231,231,231, 85, 32,
   32, 84,227,227,227,175, 32, 32, 32, 32,227,227,227,227, 32, 32,123,227,227,227,123, 32,227,227, 32,123,227,227,227,123, 32,227,227, 32, 71,227,227,227,227,227,123, 32,227,227,227,227, 32,214,227,227,227, 45,201,227,227,227, 45, 32, 32,175,227,227,227, 84, 32,227, 32,123,227,227,227,123, 32, 32,175,227,227,227,227,227,227,227,227,227,227,227,227,175, 32, 32, 84,227,227,227,175, 32,255, 32,175,227,227,227, 84, 32,
   32, 83,223,223,223,172, 32, 32, 32, 32,223,223,223,223, 32, 32,121,223,223,223,121, 32,223,223, 32,121,223,223,223,121, 32,223,223,223, 32,172,223,223,223,210, 45, 32,223,223,223,223, 32,147,223,223,223,134,223,223,223,147, 32,223, 32,172,223,223,223, 83, 32,223, 32,121,223,223,223,121, 32, 32,172,223,223,223,223,223,223,223,223,223,223,223,223,172, 32, 32, 83,223,223,223,172, 32,255, 32,172,223,223,223, 83, 32,
   32, 82,220,220,220,170, 32, 32, 32, 32,220,220,220,220, 32, 32,120,220,220,220,120, 32,220,220, 32,120,220,220,220,120, 32,220,220, 32, 95,220,220,220,220,220,132, 32,220,220,220,220, 32, 95,220,220,220,207,220,220,220, 95, 32,220, 32,170,220,220,220,107, 32,220, 32,120,220,220,220,120, 32, 32,170,220,220,220,132, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 82,220,220,220,170, 32,255, 32,170,220,220,220, 82, 32,
   32, 57,216,216,216,216, 32, 32, 32, 32,216,216,216,216, 32, 32, 81,216,216,216,167, 32,216,216, 32,155,216,216,216, 81, 32,216, 32, 57,204,216,216,216,216,216,216, 93, 32,216,216,216,216, 32,204,216,216,216,216,216,204, 32,216,216, 32,118,216,216,216,167, 32, 32, 32,130,216,216,216,118, 32, 32,118,216,216,216,191, 32, 32,216,216,216, 32, 32, 44, 57, 32, 32, 81,216,216,216,167, 32,255, 32,167,216,216,216, 81, 32,
   32, 32,189,213,213,213,116, 32, 32, 80,213,213,213,213, 32, 32, 44,201,213,213,213, 68, 32, 32, 68,213,213,213,213, 44, 32, 32, 32,165,213,213,213,165,213,213,213,201, 44, 32,213,213,213, 32,129,213,213,213,213,213,141, 32,213,213, 32, 80,213,213,213,213,165,116,153,213,213,213,213,116, 32, 32, 56,213,213,213,213,153, 56, 32, 32, 32, 44,104,189,116, 32, 32, 80,213,213,213,165, 32,255, 32,165,213,213,213, 80, 32,
   32, 32,139,210,210,210,210,174,174,210,210,210,210,210, 32, 32, 32,127,210,210,210,198,127,127,198,210,210,210,127, 32,210, 32,115,210,210,210,174, 44,139,210,210,210,163, 32, 32,210,210, 32, 68,210,210,210,210,210, 91, 32,210,210,210, 32,174,210,210,210,210,210,210,210,210,210,210,115, 32,210, 32,127,210,210,210,210,210,174,163,163,210,210,210,115, 32, 32, 79,210,210,210,163, 32,255, 32,163,210,210,210, 79, 32,
   32, 32, 55,194,206,206,206,206,206,194,206,206,206,206, 32, 32, 32, 44,171,206,206,206,206,206,206,206,206,171, 44, 32, 32, 67,206,206,206,206, 67, 32, 44,183,206,206,206,113, 32,206,206,206, 32,183,206,206,206,194, 32,206,206,206,206, 32, 67,194,206,206,206,206,206,171,206,206,206,113, 32,206, 32, 32,136,206,206,206,206,206,206,206,206,206,206,113, 32, 32, 78,206,206,206,160, 32,255, 32,160,206,206,206, 78, 32,
   32, 32, 32,100,192,203,203,203,157, 55,203,203,203,203, 32, 32,203, 32, 43,135,203,203,203,203,203,203,135, 43, 32, 32, 43,180,203,203,203,112, 32,203, 32, 66,203,203,203,203, 66, 32,203,203, 32,157,203,203,203,135, 32,203,203,203,203,203, 32, 43,112,157,157,123, 55,112,203,203,203,112, 32,203,203, 32, 32, 78,146,203,203,203,203,203,203,169,123, 55, 32, 32, 78,203,203,203,157, 32,255, 32,157,203,203,203, 78, 32,
   32, 32, 32, 32, 54,110,110, 88, 32, 32, 32, 32, 32, 32, 32, 32,200,200, 32, 32, 54, 99,110,110, 99, 54, 32, 32,200,200, 32, 32, 32, 32, 32, 32, 32,200,200, 32, 32, 32, 32, 32, 32,200,200, 32, 54,200,200,200,200, 77, 32,200,200,200,200,200, 32, 32, 32, 32, 32, 32, 32,166,200,200,200, 88, 32,200,200,200,200, 32, 32, 32, 66, 77, 77, 77, 32, 32, 32, 32,200,200, 32, 32, 32, 32, 32, 32,255, 32, 32, 32, 32, 32, 32,255,
   32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,198,198,198,198, 32, 32, 32, 32, 32, 32,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198, 32,109,198,198,198,176, 32,198,198,198,198,198, 32, 98,121, 76, 32, 32, 54,109,198,198,198,198, 43, 32,198,198,198,198,198,198,198, 32, 32, 32, 32,198,198,198,198,198,198,198,198,198,198,198,198,255,255,255,255,255,255,255,255,
   32, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 33,159,191,191,191,117, 36, 41, 41, 41, 41, 41, 34,108,191,191,191,191,191,191,191,191,191,117, 36, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41,255,
   32, 41, 97,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128, 78, 38, 64,190,192,192,192, 66, 66, 41, 41, 85,128, 65, 34,107,190,192,192,192,192,192,192,192,139, 48, 39, 41, 41,105,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128, 97, 41,255,
   32, 41, 97,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128, 96, 36, 95,147,148,148,139, 55, 41, 41, 85,121,128, 91, 38, 75,137,158,190,190,190,170,139, 97, 49, 37, 41, 41,105,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128, 97, 41,255,
   32, 41, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 41, 36, 45, 45, 45, 48, 38, 41, 41, 76, 76, 76, 76, 76, 37, 34, 42, 33, 33, 33, 39, 48, 59, 41, 41, 41, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 41,255,
   32, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

// doxygen logo alpha map
static unsigned char doxygen_a_png[3224] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 66, 66, 66, 66,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,145,247,247,247,247,145,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,247,247,247,247,247,247,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,247,247,247,247,247,247,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,247,247,247,247,247,247,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0, 16,115,181,181,132,247,247,247,247,247,247,  0,  0,  0,  0,  0, 99,148,181,181,148, 99,  0,  0,  0,  0, 16, 66, 66, 66, 66, 16,  0,  0,  0,  0, 66, 66, 66, 66, 33, 66, 66, 66, 66, 16,  0,  0,  0, 16, 66, 66, 66, 66,  0,  0,  0,  0, 99,165,181,148, 66, 33, 66, 66, 66, 33,  0,  0,  0,  0,  0,  0, 82,132,181,181,181,115, 66,  0,  0,  0,  0,  0, 16, 66, 66, 66, 49,  0, 66,148,181,148, 49,  0,  0,  0,
    0,  0,  0,129,247,247,247,247,247,247,247,247,247,247,247,  0,  0,  0,112,214,247,247,247,247,247,247,214,112,  0, 16,247,247,247,247,247,247, 46,  0,  0,145,247,247,247,247,247,247,247,247,247,247, 16,  0, 16,247,247,247,247,247, 66,  0, 63,165,247,247,247,247,247,247,247,247,247,247, 33,  0,  0,  0, 96,198,247,247,247,247,247,247,247,165, 63,  0,  0, 16,247,247,247,247,247,145,247,247,247,247,247,145,  0,  0,
    0,  0,112,247,247,247,247,247,247,247,247,247,247,247,247,  0,  0,129,247,247,247,247,247,247,247,247,247,247,129,  0,181,247,247,247,247,247,148,  0,129,247,247,247,247,247,247,247,247,247,247,247,115,  0,115,247,247,247,247,247,165, 30,247,247,247,247,247,247,247,247,247,247,247,247,115,  0,  0,129,247,247,247,247,247,247,247,247,247,247,247, 63,  0, 66,247,247,247,247,247,247,247,247,247,247,247,247, 96,  0,
    0, 16,247,247,247,247,247,247,247,247,247,247,247,247,247,  0, 79,247,247,247,247,247,247,247,247,247,247,247,247, 79, 79,247,247,247,247,247,247,129,247,247,247,247,247,247,129,247,247,247,247,247,198,  0,181,247,247,247,247,247, 99,145,247,247,247,247,247,247,247,247,247,247,247,247,115,  0, 96,247,247,247,247,247,247,247,247,247,247,247,247,165,  0, 66,247,247,247,247,247,247,247,247,247,247,247,247,198,  0,
    0,115,247,247,247,247,247,247,247,247,247,247,247,247,247,  0,181,247,247,247,247,247,247,247,247,247,247,247,247,181,  0,129,247,247,247,247,247,247,247,247,247,247,247,145, 16,247,247,247,247,247,247, 33,247,247,247,247,247,247, 33,247,247,247,247,247,247,247,247,247,247,247,247,247,115,  0,198,247,247,247,247,247,198,181,247,247,247,247,247,247, 49, 66,247,247,247,247,247,247,247,247,247,247,247,247,247, 16,
    0,214,247,247,247,247,247,129, 66,247,247,247,247,247,247, 33,247,247,247,247,247,247, 96, 96,247,247,247,247,247,247, 33,  0,145,247,247,247,247,247,247,247,247,247,198, 30,  0,165,247,247,247,247,247,115,247,247,247,247,247,165,115,247,247,247,247,247,181, 66,115,247,247,247,247,247,115, 82,247,247,247,247,247,165,115,115,148,247,247,247,247,247,115, 66,247,247,247,247,247,247,181,247,247,247,247,247,247, 66,
   16,247,247,247,247,247,231,  0,  0,247,247,247,247,247,247, 82,247,247,247,247,247,165,  0,  0,165,247,247,247,247,247, 82,  0, 30,247,247,247,247,247,247,247,247,247, 96,  0,  0, 82,247,247,247,247,247,165,247,247,247,247,247, 99,165,247,247,247,247,247, 99,  0,115,247,247,247,247,247,115,132,247,247,247,247,247,247,247,247,247,247,247,247,247,247,181, 66,247,247,247,247,247,181,  0,198,247,247,247,247,247, 66,
   66,247,247,247,247,247,181,  0,  0,247,247,247,247,247,247,115,247,247,247,247,247,115,  0,  0,115,247,247,247,247,247,115,  0,  0, 96,247,247,247,247,247,247,247,129,  0,  0,  0,  0,231,247,247,247,247,247,247,247,247,247,247, 16,181,247,247,247,247,247, 66,  0,115,247,247,247,247,247,115,181,247,247,247,247,247,247,247,247,247,247,247,247,247,247,181, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
   66,247,247,247,247,247,181,  0,  0,247,247,247,247,247,247,115,247,247,247,247,247,115,  0,  0,115,247,247,247,247,247,115,  0,  0,  0,181,247,247,247,247,247,247, 30,  0,  0,  0,  0,148,247,247,247,247,247,247,247,247,247,148,  0,181,247,247,247,247,247, 66,  0,115,247,247,247,247,247,115,181,247,247,247,247,247,247,247,247,247,247,247,247,247,247,181, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
   66,247,247,247,247,247,181,  0,  0,247,247,247,247,247,247,115,247,247,247,247,247,115,  0,  0,115,247,247,247,247,247,115,  0,  0,129,247,247,247,247,247,247,247,145,  0,  0,  0,  0, 82,247,247,247,247,247,247,247,247,247, 82,  0,181,247,247,247,247,247, 99,  0,115,247,247,247,247,247,115,181,247,247,247,247,247,247,247,247,247,247,247,247,247,181, 79, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
   33,247,247,247,247,247,247, 14,  0,247,247,247,247,247,247, 66,247,247,247,247,247,181,  0,  0,165,247,247,247,247,247, 66,  0, 79,247,247,247,247,247,247,247,247,247,129,  0,  0,  0,  0,231,247,247,247,247,247,247,247,231,  0,  0,115,247,247,247,247,247,181,115,165,247,247,247,247,247,115,115,247,247,247,247,247,214, 63,  0,  0,  0, 16,112,247,247, 33, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
    0,214,247,247,247,247,247,198,198,247,247,247,247,247,247, 16,247,247,247,247,247,247,132,132,247,247,247,247,247,247, 16, 14,181,247,247,247,247,247,247,247,247,247,247, 79,  0,  0,  0,132,247,247,247,247,247,247,247,148,  0,  0, 66,247,247,247,247,247,247,247,247,247,247,247,247,247,115, 33,247,247,247,247,247,247,247,198,181,181,247,247,247,247,115, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
    0,148,247,247,247,247,247,247,247,247,247,247,247,247,247,  0,132,247,247,247,247,247,247,247,247,247,247,247,247,145,  0,145,247,247,247,247,247,247,247,247,247,247,247,181, 14,  0,  0, 49,247,247,247,247,247,247,247, 82,  0,  0,  0,198,247,247,247,247,247,247,247,247,247,247,247,247,115,  0,145,247,247,247,247,247,247,247,247,247,247,247,247,247,115, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
    0, 46,247,247,247,247,247,247,247,247,247,247,247,247,247,  0, 30,247,247,247,247,247,247,247,247,247,247,247,247, 30,112,247,247,247,247,247,247, 96,247,247,247,247,247,247,145,  0,  0,  0,214,247,247,247,247,247,231,  0,  0,  0,  0, 96,247,247,247,247,247,247,247,247,247,247,247,247,115,  0, 30,148,247,247,247,247,247,247,247,247,247,247,247,247,115, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
    0,  0,129,247,247,247,247,247,247,247,247,247,247,247,247,  0,  0, 96,247,247,247,247,247,247,247,247,247,247, 96, 16,247,247,247,247,247,247,145,  0,112,247,247,247,247,247,247, 49,  0,  0,181,247,247,247,247,247,148,  0,  0,  0,  0,  0,129,247,247,247,247,247,247,247,247,247,247,247,115,  0,  0, 46,148,247,247,247,247,247,247,247,247,247,247,247, 33, 66,247,247,247,247,247,181,  0,181,247,247,247,247,247, 66,
    0,  0,  0,129,247,247,247,247,181,145,247,247,247,247,145,  0,  0,  0, 46,148,247,247,247,247,247,247,148, 46,  0,  0,112,214,247,247,247,145, 14,  0,  0,145,247,247,247,247,145,  0,  0, 33,247,247,247,247,247,247, 66,  0,  0,  0,  0,  0, 99,132,115,181,181,132,198,247,247,247,247,247, 82,  0,  0,  0,  0, 66,165,247,247,247,247,247,247,198,132, 33,  0,  0,145,247,247,247,181, 79,  0, 79,181,247,247,247,145,  0,
    0,  0,  0,  0, 33,115,115, 82,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 33, 99,115,115, 99, 33,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,115,247,247,247,247,247,214,  0,  0,  0,  0,  0, 99,247,247,247,247,247,247,247,247,247,247,247,247, 16,  0,  0,  0,  0,  0,  0,  0, 49, 66, 66, 66,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,165,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,108,224,255,255,255,255,255,255,101,164,255,255,255,143,250,255,255,255,255,255,255,255,255,255,255,255, 98,170,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,165,  0,
    0,165,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,136,251,255,255,255,255,255,255,101,130,255,255,255,153,250,255,255,255,255,255,255,255,255,255,255,121, 98,189,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,165,  0,
    0,165,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,198,252,255,255,255,255,255,164,164,255,255,255,255,176,249,251,255,255,255,255,255,255,255,255,150, 86,192,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,165,  0,
    0,165,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,164,198,255,255,255,255,201,133,164,255,255,255,255,255,145,203,255,255,255,255,255,255,255,117, 79,194,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,165,  0,
    0, 66,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102, 73, 73,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102, 47, 70,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102,102, 66,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

// magnifying glass icon (raw png)
unsigned char mag_sel_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
  0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x13,
  0x08, 0x06, 0x00, 0x00, 0x00, 0x90, 0x8c, 0x2d, 0xb5, 0x00, 0x00, 0x00,
  0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0b, 0x13, 0x00, 0x00, 0x0b,
  0x13, 0x01, 0x00, 0x9a, 0x9c, 0x18, 0x00, 0x00, 0x00, 0x20, 0x63, 0x48,
  0x52, 0x4d, 0x00, 0x00, 0x6d, 0x98, 0x00, 0x00, 0x73, 0x8e, 0x00, 0x00,
  0xe0, 0x38, 0x00, 0x00, 0x82, 0xd5, 0x00, 0x00, 0x7a, 0x07, 0x00, 0x00,
  0xca, 0xb4, 0x00, 0x00, 0x33, 0x44, 0x00, 0x00, 0x1c, 0x76, 0x84, 0x36,
  0x2a, 0xbd, 0x00, 0x00, 0x01, 0xb9, 0x49, 0x44, 0x41, 0x54, 0x78, 0xda,
  0xe4, 0x94, 0xbb, 0x8a, 0x22, 0x41, 0x14, 0x86, 0xbf, 0xda, 0x16, 0x3a,
  0x10, 0xba, 0x03, 0x2f, 0x78, 0x03, 0x51, 0x11, 0x4c, 0xd4, 0x40, 0xd4,
  0x37, 0x30, 0x31, 0x30, 0xe9, 0x07, 0xf0, 0x15, 0x14, 0x7c, 0x1e, 0x31,
  0x37, 0x33, 0x11, 0x73, 0xe9, 0x56, 0x44, 0x84, 0x36, 0xe9, 0x40, 0x50,
  0x54, 0x14, 0xc4, 0xc0, 0xa8, 0x6d, 0x50, 0x6a, 0x92, 0x1d, 0xd9, 0x9d,
  0x99, 0x75, 0x0d, 0x26, 0x58, 0xd8, 0x3f, 0xaa, 0xe2, 0xfc, 0xf5, 0xd5,
  0x39, 0x9c, 0x53, 0x25, 0xa4, 0x94, 0x7c, 0xa7, 0x7e, 0xf0, 0xcd, 0xfa,
  0xf7, 0x81, 0xbe, 0xf7, 0xc5, 0xf9, 0x7c, 0x96, 0x93, 0xc9, 0x84, 0xe5,
  0x72, 0xc9, 0x66, 0xb3, 0x21, 0x99, 0x4c, 0x92, 0xcf, 0xe7, 0xa9, 0x54,
  0x2a, 0x04, 0x02, 0x01, 0xf1, 0x2a, 0x50, 0x48, 0x29, 0x39, 0x9d, 0x4e,
  0x72, 0x30, 0x18, 0x60, 0x59, 0xd6, 0x27, 0x43, 0xb5, 0x5a, 0xa5, 0xd1,
  0x68, 0x10, 0x0c, 0x06, 0xc5, 0xcb, 0x19, 0x4e, 0xa7, 0x53, 0x2c, 0xcb,
  0x22, 0x95, 0x4a, 0x51, 0x2a, 0x95, 0xc8, 0x64, 0x32, 0xac, 0x56, 0x2b,
  0x66, 0xb3, 0x19, 0x93, 0xc9, 0x84, 0x48, 0x24, 0x42, 0xbd, 0x5e, 0x7f,
  0xbd, 0x64, 0xdb, 0xb6, 0x01, 0x28, 0x97, 0xcb, 0x54, 0x2a, 0x15, 0x34,
  0x4d, 0x13, 0xa1, 0x50, 0x48, 0x2a, 0x8a, 0xc2, 0x7a, 0xbd, 0xc6, 0xb6,
  0x6d, 0xea, 0xf5, 0x3a, 0xa3, 0xd1, 0x48, 0xf6, 0xfb, 0xfd, 0xc7, 0x61,
  0xc3, 0x30, 0xa8, 0xd5, 0x6a, 0xe2, 0x53, 0x53, 0xb6, 0xdb, 0x2d, 0x00,
  0xc5, 0x62, 0x11, 0x4d, 0xd3, 0x04, 0x80, 0xa6, 0x69, 0xa2, 0x50, 0x28,
  0xf0, 0x6b, 0x1c, 0x10, 0x86, 0x61, 0x3c, 0x60, 0x80, 0xf8, 0xb2, 0xcb,
  0x89, 0x44, 0x02, 0x00, 0xc7, 0x71, 0x00, 0xde, 0x27, 0x5d, 0xfe, 0xdc,
  0x3f, 0xe2, 0x1f, 0xa0, 0xe2, 0x8f, 0x63, 0x93, 0xcb, 0xe5, 0x00, 0x18,
  0x8f, 0xc7, 0x98, 0xa6, 0x89, 0xeb, 0xba, 0xd2, 0x34, 0x4d, 0xc6, 0xe3,
  0x31, 0x00, 0xe9, 0x74, 0x1a, 0x80, 0x5a, 0xad, 0xf6, 0x80, 0x3e, 0xed,
  0xf2, 0x7a, 0xbd, 0x96, 0xc3, 0xe1, 0x90, 0xf9, 0x7c, 0xfe, 0xa5, 0x29,
  0x1c, 0x0e, 0xd3, 0xe9, 0x74, 0xd0, 0x75, 0x5d, 0x00, 0x8c, 0x46, 0xa3,
  0x8f, 0x17, 0xfc, 0x0e, 0xf4, 0x3c, 0x4f, 0xee, 0x76, 0x3b, 0x16, 0x8b,
  0x05, 0x8e, 0xe3, 0xb0, 0xdf, 0xef, 0x89, 0xc7, 0xe3, 0xa4, 0xd3, 0x69,
  0x6c, 0xdb, 0xe6, 0x74, 0x3a, 0x11, 0x8d, 0x46, 0x69, 0xb7, 0xdb, 0x0f,
  0xe8, 0xd3, 0x0c, 0x01, 0x3c, 0xcf, 0x93, 0xae, 0xeb, 0xe2, 0x79, 0x1e,
  0xb7, 0xdb, 0x0d, 0x9f, 0xcf, 0x87, 0xa2, 0x28, 0x5c, 0x2e, 0x17, 0x7a,
  0xbd, 0x1e, 0xc7, 0xe3, 0x91, 0x58, 0x2c, 0x46, 0xab, 0xd5, 0x7a, 0x0a,
  0x7d, 0xbc, 0x14, 0x55, 0x55, 0x85, 0xaa, 0xaa, 0x9f, 0x0c, 0x7e, 0xbf,
  0x5f, 0x36, 0x9b, 0x4d, 0xba, 0xdd, 0x2e, 0xd7, 0xeb, 0x95, 0xeb, 0xf5,
  0x8a, 0xae, 0xeb, 0x7f, 0xcf, 0xf0, 0x99, 0x5c, 0xd7, 0x95, 0x87, 0xc3,
  0x81, 0xfb, 0xfd, 0x4e, 0x36, 0x9b, 0x7d, 0xad, 0xe4, 0xff, 0xe7, 0xfb,
  0x7a, 0x1b, 0x00, 0x59, 0xa8, 0xba, 0x68, 0xca, 0x4f, 0xc5, 0xa7, 0x00,
  0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};
unsigned int mag_sel_png_len = 563;

unsigned char mag_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
  0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x13,
  0x08, 0x06, 0x00, 0x00, 0x00, 0x90, 0x8c, 0x2d, 0xb5, 0x00, 0x00, 0x00,
  0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0b, 0x13, 0x00, 0x00, 0x0b,
  0x13, 0x01, 0x00, 0x9a, 0x9c, 0x18, 0x00, 0x00, 0x00, 0x20, 0x63, 0x48,
  0x52, 0x4d, 0x00, 0x00, 0x6d, 0x98, 0x00, 0x00, 0x73, 0x8e, 0x00, 0x00,
  0xe0, 0x38, 0x00, 0x00, 0x82, 0xd5, 0x00, 0x00, 0x7a, 0x07, 0x00, 0x00,
  0xca, 0xb4, 0x00, 0x00, 0x33, 0x44, 0x00, 0x00, 0x1c, 0x76, 0x84, 0x36,
  0x2a, 0xbd, 0x00, 0x00, 0x01, 0x92, 0x49, 0x44, 0x41, 0x54, 0x78, 0xda,
  0xe4, 0x94, 0xbb, 0xaa, 0xea, 0x50, 0x10, 0x86, 0xbf, 0xec, 0x08, 0x29,
  0x36, 0x24, 0x85, 0x17, 0xbc, 0x81, 0x18, 0x11, 0x6c, 0xd4, 0x42, 0x8c,
  0x0f, 0x61, 0xe1, 0x2b, 0xf8, 0x0a, 0x0a, 0x3e, 0x8f, 0xf8, 0x0c, 0x36,
  0x62, 0x1f, 0x92, 0x88, 0x88, 0x10, 0x9b, 0x14, 0x42, 0x44, 0x45, 0x41,
  0x2c, 0xac, 0x92, 0x80, 0xb2, 0x4e, 0x73, 0x94, 0x03, 0xfb, 0xb0, 0x4d,
  0xb1, 0x8b, 0x03, 0xe7, 0xaf, 0xd6, 0x62, 0xfe, 0xf5, 0x31, 0xc3, 0xcc,
  0x1a, 0x49, 0x08, 0xc1, 0x4f, 0xea, 0x83, 0x1f, 0xd6, 0xbf, 0x0f, 0x4c,
  0x3c, 0x0f, 0xd7, 0xeb, 0x55, 0x38, 0x8e, 0xc3, 0x66, 0xb3, 0x61, 0xb7,
  0xdb, 0x51, 0x2a, 0x95, 0xa8, 0xd7, 0xeb, 0x18, 0x86, 0x41, 0x32, 0x99,
  0x94, 0xe2, 0x02, 0x25, 0x21, 0x04, 0x97, 0xcb, 0x45, 0x4c, 0xa7, 0x53,
  0x6c, 0xdb, 0xfe, 0x62, 0xe8, 0x74, 0x3a, 0xf4, 0x7a, 0x3d, 0x52, 0xa9,
  0x94, 0x14, 0x3b, 0xc3, 0xc5, 0x62, 0x81, 0x6d, 0xdb, 0x94, 0xcb, 0x65,
  0x5a, 0xad, 0x16, 0x95, 0x4a, 0x85, 0xed, 0x76, 0xcb, 0x72, 0xb9, 0xc4,
  0x71, 0x1c, 0xb2, 0xd9, 0x2c, 0xdd, 0x6e, 0x37, 0x7e, 0xc9, 0xae, 0xeb,
  0x02, 0xd0, 0x6e, 0xb7, 0x31, 0x0c, 0x03, 0x55, 0x55, 0xa5, 0x74, 0x3a,
  0x2d, 0x64, 0x59, 0xc6, 0xf7, 0x7d, 0x5c, 0xd7, 0x8d, 0x0d, 0xfc, 0x00,
  0xd8, 0xef, 0xf7, 0x00, 0x34, 0x9b, 0x4d, 0x54, 0x55, 0x95, 0x00, 0x54,
  0x55, 0x95, 0x1a, 0x8d, 0x06, 0x7f, 0xc6, 0x63, 0x03, 0x8b, 0xc5, 0x22,
  0x00, 0x9e, 0xe7, 0x01, 0x3c, 0x27, 0x5d, 0xfc, 0xbe, 0xbf, 0xe2, 0xb1,
  0x81, 0xb5, 0x5a, 0x0d, 0x00, 0xd3, 0x34, 0xb1, 0x2c, 0x8b, 0x20, 0x08,
  0x84, 0x65, 0x59, 0x98, 0xa6, 0x09, 0x80, 0xae, 0xeb, 0xaf, 0x07, 0xf3,
  0xf9, 0xfc, 0x7d, 0x97, 0x7d, 0xdf, 0x17, 0xb3, 0xd9, 0x8c, 0xd5, 0x6a,
  0xf5, 0x57, 0x53, 0x26, 0x93, 0x61, 0x34, 0x1a, 0xa1, 0x69, 0x9a, 0x14,
  0x6b, 0x6c, 0xa2, 0x28, 0x12, 0x87, 0xc3, 0x81, 0xf5, 0x7a, 0x8d, 0xe7,
  0x79, 0x1c, 0x8f, 0x47, 0x0a, 0x85, 0x02, 0xba, 0xae, 0xe3, 0xba, 0x2e,
  0x97, 0xcb, 0x85, 0x5c, 0x2e, 0xc7, 0x70, 0x38, 0x7c, 0x0b, 0x95, 0x9e,
  0xcb, 0x21, 0x8a, 0x22, 0x11, 0x04, 0x01, 0x51, 0x14, 0x71, 0xbf, 0xdf,
  0x49, 0x24, 0x12, 0xc8, 0xb2, 0xcc, 0xed, 0x76, 0x63, 0x32, 0x99, 0x70,
  0x3e, 0x9f, 0xc9, 0xe7, 0xf3, 0x0c, 0x06, 0x83, 0x6f, 0xa1, 0xaf, 0x9f,
  0xa2, 0x28, 0x8a, 0xa4, 0x28, 0xca, 0x17, 0xc3, 0xe7, 0xe7, 0xa7, 0xe8,
  0xf7, 0xfb, 0x8c, 0xc7, 0x63, 0xc2, 0x30, 0x24, 0x0c, 0x43, 0x34, 0x4d,
  0x7b, 0x9f, 0xe1, 0x77, 0x0a, 0x82, 0x40, 0x9c, 0x4e, 0x27, 0x1e, 0x8f,
  0x07, 0xd5, 0x6a, 0x35, 0x5e, 0xc9, 0xff, 0xcf, 0xfa, 0xfa, 0x35, 0x00,
  0x70, 0xf3, 0xae, 0xcb, 0x89, 0xcd, 0xd2, 0x46, 0x00, 0x00, 0x00, 0x00,
  0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};
unsigned int mag_png_len = 524;

unsigned char search_l_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
  0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x13,
  0x08, 0x06, 0x00, 0x00, 0x00, 0x90, 0x8c, 0x2d, 0xb5, 0x00, 0x00, 0x00,
  0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0b, 0x13, 0x00, 0x00, 0x0b,
  0x13, 0x01, 0x00, 0x9a, 0x9c, 0x18, 0x00, 0x00, 0x00, 0x20, 0x63, 0x48,
  0x52, 0x4d, 0x00, 0x00, 0x6d, 0x98, 0x00, 0x00, 0x73, 0x8e, 0x00, 0x00,
  0xe0, 0x38, 0x00, 0x00, 0x82, 0xd5, 0x00, 0x00, 0x7a, 0x07, 0x00, 0x00,
  0xca, 0xb4, 0x00, 0x00, 0x33, 0x44, 0x00, 0x00, 0x1c, 0x76, 0x84, 0x36,
  0x2a, 0xbd, 0x00, 0x00, 0x01, 0xe2, 0x49, 0x44, 0x41, 0x54, 0x78, 0xda,
  0xac, 0x54, 0x3d, 0xab, 0xda, 0x50, 0x18, 0x7e, 0xce, 0xc9, 0x39, 0x31,
  0x4d, 0xfc, 0x40, 0x30, 0x46, 0x14, 0xec, 0x50, 0x44, 0x17, 0x2f, 0x9d,
  0xba, 0x15, 0xda, 0xd1, 0xa1, 0x2e, 0xdd, 0x3b, 0x14, 0x4a, 0xa1, 0x7f,
  0xa6, 0x74, 0xbd, 0x43, 0xff, 0x84, 0xfd, 0x05, 0x82, 0xda, 0xa5, 0x83,
  0x1d, 0xdc, 0x8a, 0x88, 0xa0, 0x44, 0x83, 0xc6, 0x28, 0xad, 0x1f, 0x49,
  0xde, 0x2e, 0x8d, 0x78, 0x6f, 0xaf, 0x34, 0x68, 0x9f, 0xed, 0xbc, 0x70,
  0x1e, 0x9e, 0x8f, 0xf7, 0x1c, 0x46, 0x44, 0x38, 0x45, 0xaf, 0xd7, 0x63,
  0xb6, 0x6d, 0xe7, 0x6d, 0xdb, 0x6e, 0xba, 0xae, 0xfb, 0x6e, 0xb3, 0xd9,
  0xdc, 0x6c, 0xb7, 0xdb, 0x04, 0xe7, 0x1c, 0x8c, 0x31, 0xfc, 0x0b, 0x2c,
  0x22, 0xec, 0x76, 0xbb, 0xcc, 0xf3, 0xbc, 0xcc, 0x68, 0x34, 0x7a, 0xed,
  0xba, 0xee, 0x87, 0x6c, 0x36, 0x7b, 0x93, 0xcb, 0xe5, 0x44, 0x3a, 0x9d,
  0x86, 0xa6, 0x69, 0x50, 0x14, 0x25, 0x3e, 0x61, 0xa7, 0xd3, 0x61, 0xf3,
  0xf9, 0xfc, 0xc9, 0x78, 0x3c, 0xbe, 0xd5, 0x75, 0xfd, 0x79, 0xa5, 0x52,
  0x11, 0xa6, 0x69, 0x22, 0x95, 0x4a, 0x41, 0xd3, 0x34, 0x08, 0x21, 0xc0,
  0x18, 0x8b, 0x45, 0x28, 0x00, 0x60, 0xb5, 0x5a, 0xa5, 0x27, 0x93, 0xc9,
  0xa7, 0x62, 0xb1, 0xf8, 0xb2, 0x5a, 0xad, 0x22, 0x9f, 0xcf, 0xc3, 0x30,
  0x0c, 0x48, 0x29, 0xc1, 0x39, 0x47, 0x5c, 0xbb, 0x00, 0x20, 0xda, 0xed,
  0x36, 0x9f, 0x4e, 0xa7, 0xaf, 0x4c, 0xd3, 0x7c, 0x51, 0xaf, 0xd7, 0x61,
  0x59, 0x16, 0x74, 0x5d, 0x87, 0x94, 0x12, 0x97, 0x40, 0x2c, 0x16, 0x0b,
  0x93, 0x88, 0xde, 0xd6, 0x6a, 0x35, 0xdd, 0xb2, 0x2c, 0x18, 0x86, 0x01,
  0x21, 0x04, 0x2e, 0x05, 0xf7, 0x3c, 0xaf, 0x59, 0x2e, 0x97, 0x9f, 0x45,
  0xca, 0x38, 0xe7, 0xb8, 0x06, 0x3c, 0x08, 0x82, 0x46, 0xa1, 0x50, 0x78,
  0x74, 0xad, 0xb2, 0x23, 0xa1, 0x94, 0xf2, 0x69, 0x26, 0x93, 0xe1, 0x51,
  0x66, 0xf7, 0xf7, 0xd2, 0xf7, 0xfd, 0x07, 0x2f, 0x9e, 0x9b, 0x73, 0x55,
  0x55, 0xb3, 0x91, 0x55, 0xc6, 0x18, 0xc2, 0x30, 0xbc, 0x1b, 0xf2, 0x19,
  0xd5, 0xe7, 0xe6, 0x5c, 0x4a, 0x39, 0x06, 0x70, 0x5c, 0x8b, 0xb8, 0xeb,
  0x71, 0xd6, 0x32, 0x11, 0x75, 0xf6, 0xfb, 0xfd, 0xd1, 0xea, 0xd5, 0xa5,
  0x10, 0xd1, 0xb7, 0xf5, 0x7a, 0x1d, 0x84, 0x61, 0x08, 0x22, 0xba, 0x9e,
  0x50, 0x51, 0x94, 0xaf, 0x8e, 0xe3, 0xfc, 0xdc, 0xed, 0x76, 0xf8, 0x1f,
  0xe0, 0x89, 0x44, 0xe2, 0xc7, 0x72, 0xb9, 0xfc, 0xee, 0x38, 0x0e, 0x7c,
  0xdf, 0x3f, 0x5a, 0xbf, 0xdf, 0x76, 0x6c, 0xc2, 0x46, 0xa3, 0xf1, 0x2b,
  0x08, 0x82, 0xdb, 0xe1, 0x70, 0xe8, 0x2c, 0x16, 0x0b, 0x04, 0x41, 0x00,
  0x22, 0xba, 0xb8, 0x1c, 0xfe, 0x67, 0x05, 0xbe, 0x78, 0x9e, 0xf7, 0x79,
  0x30, 0x18, 0x8c, 0x67, 0xb3, 0x19, 0x45, 0x25, 0x9d, 0x53, 0x49, 0x44,
  0x38, 0x1c, 0x0e, 0x38, 0x2d, 0xf3, 0xce, 0x6f, 0x03, 0x60, 0x29, 0x84,
  0xf8, 0xe8, 0x79, 0x9e, 0xdb, 0xef, 0xf7, 0xdf, 0x97, 0x4a, 0xa5, 0xc7,
  0xd1, 0x53, 0x54, 0x55, 0x15, 0x52, 0xca, 0xbf, 0x14, 0x0b, 0x21, 0x1e,
  0x8c, 0x87, 0x9d, 0x1e, 0x5a, 0xad, 0x96, 0x00, 0x50, 0x27, 0xa2, 0x37,
  0xaa, 0xaa, 0x36, 0x0d, 0xc3, 0x28, 0x26, 0x93, 0x49, 0xa1, 0x69, 0x9a,
  0xc2, 0x39, 0x8f, 0x95, 0xc1, 0xef, 0x01, 0x00, 0x35, 0xe5, 0xd5, 0x5e,
  0xd0, 0xed, 0x0c, 0xfd, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44,
  0xae, 0x42, 0x60, 0x82
};
unsigned int search_l_png_len = 604;

unsigned char search_m_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
  0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x13,
  0x08, 0x02, 0x00, 0x00, 0x00, 0x35, 0x5e, 0x4b, 0x4d, 0x00, 0x00, 0x00,
  0x04, 0x67, 0x41, 0x4d, 0x41, 0x00, 0x00, 0xd6, 0xd8, 0xd4, 0x4f, 0x58,
  0x32, 0x00, 0x00, 0x00, 0x19, 0x74, 0x45, 0x58, 0x74, 0x53, 0x6f, 0x66,
  0x74, 0x77, 0x61, 0x72, 0x65, 0x00, 0x41, 0x64, 0x6f, 0x62, 0x65, 0x20,
  0x49, 0x6d, 0x61, 0x67, 0x65, 0x52, 0x65, 0x61, 0x64, 0x79, 0x71, 0xc9,
  0x65, 0x3c, 0x00, 0x00, 0x00, 0x30, 0x49, 0x44, 0x41, 0x54, 0x78, 0xda,
  0x62, 0x2c, 0x2f, 0x2f, 0x67, 0x60, 0x60, 0x60, 0x3c, 0x7e, 0xfc, 0x38,
  0x88, 0xfa, 0xf8, 0xf1, 0x23, 0x88, 0xfa, 0xff, 0xff, 0x3f, 0x90, 0x62,
  0x62, 0x00, 0x03, 0x5a, 0x50, 0x2c, 0x10, 0x1b, 0x58, 0x6e, 0xdd, 0xba,
  0x05, 0xa4, 0x00, 0x02, 0x0c, 0x00, 0xa5, 0x07, 0x0f, 0x3c, 0x7e, 0xe1,
  0x45, 0xa7, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42,
  0x60, 0x82
};
unsigned int search_m_png_len = 158;

unsigned char search_r_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
  0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x13,
  0x08, 0x06, 0x00, 0x00, 0x00, 0x9d, 0x92, 0x5d, 0xf2, 0x00, 0x00, 0x00,
  0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0b, 0x13, 0x00, 0x00, 0x0b,
  0x13, 0x01, 0x00, 0x9a, 0x9c, 0x18, 0x00, 0x00, 0x00, 0x20, 0x63, 0x48,
  0x52, 0x4d, 0x00, 0x00, 0x6d, 0x98, 0x00, 0x00, 0x73, 0x8e, 0x00, 0x00,
  0xe0, 0x38, 0x00, 0x00, 0x82, 0xd5, 0x00, 0x00, 0x7a, 0x07, 0x00, 0x00,
  0xca, 0xb4, 0x00, 0x00, 0x33, 0x44, 0x00, 0x00, 0x1c, 0x76, 0x84, 0x36,
  0x2a, 0xbd, 0x00, 0x00, 0x01, 0xea, 0x49, 0x44, 0x41, 0x54, 0x78, 0xda,
  0xa4, 0xd4, 0xbf, 0xaa, 0x1a, 0x41, 0x14, 0x06, 0xf0, 0x6f, 0xf6, 0x9f,
  0xb2, 0x0a, 0x6b, 0xa5, 0x56, 0x8b, 0xa4, 0x92, 0xd4, 0x69, 0x7c, 0x03,
  0xb1, 0x59, 0x49, 0x11, 0x52, 0xdf, 0xbc, 0x43, 0xcc, 0x2b, 0xa4, 0x4c,
  0x97, 0x67, 0x08, 0xa4, 0x11, 0x2c, 0x52, 0x5c, 0x42, 0x24, 0x60, 0x8a,
  0x34, 0x29, 0x42, 0x50, 0x41, 0x21, 0xa0, 0x97, 0xd5, 0x55, 0xb3, 0xbb,
  0xee, 0xb2, 0xce, 0xee, 0xcc, 0x49, 0x91, 0x28, 0xc2, 0x0d, 0xe6, 0xaa,
  0xa7, 0x9d, 0xc3, 0x8f, 0x73, 0x98, 0xf9, 0x86, 0x75, 0x3a, 0x1d, 0xc2,
  0x89, 0x12, 0x42, 0x24, 0xf9, 0x7c, 0x7e, 0x5a, 0x2c, 0x16, 0x3f, 0x96,
  0x4a, 0xa5, 0x5e, 0xb5, 0x5a, 0xfd, 0x52, 0x2e, 0x97, 0xfd, 0x46, 0xa3,
  0x21, 0x8e, 0xfb, 0xd8, 0x60, 0x30, 0x38, 0x09, 0x65, 0x59, 0x86, 0x24,
  0x49, 0x10, 0x04, 0x81, 0xf0, 0x3c, 0x6f, 0xb3, 0xd9, 0x6c, 0x7e, 0x58,
  0x96, 0x75, 0x5b, 0xab, 0xd5, 0xde, 0x34, 0x9b, 0xcd, 0x5f, 0x07, 0xc8,
  0xf7, 0xfd, 0x93, 0x90, 0x94, 0xf2, 0x80, 0x85, 0x61, 0x88, 0xe5, 0x72,
  0x49, 0xe3, 0xf1, 0x58, 0xc6, 0x71, 0xfc, 0xc1, 0xb6, 0xed, 0xe7, 0x8e,
  0xe3, 0x84, 0x00, 0xc0, 0xa4, 0x94, 0x27, 0x21, 0x22, 0x82, 0x94, 0x12,
  0x52, 0x4a, 0xa4, 0x69, 0x8a, 0x28, 0x8a, 0xb0, 0x58, 0x2c, 0x30, 0x1c,
  0x0e, 0x85, 0xeb, 0xba, 0xef, 0x6b, 0xb5, 0xda, 0x4d, 0xab, 0xd5, 0x8a,
  0x34, 0xc6, 0xd8, 0x29, 0x07, 0x8c, 0x31, 0x28, 0x8a, 0x02, 0x22, 0x82,
  0xae, 0xeb, 0x30, 0x0c, 0x03, 0xb9, 0x5c, 0x0e, 0x86, 0x61, 0xa8, 0x52,
  0xca, 0xa7, 0xf3, 0xf9, 0xfc, 0x67, 0xbf, 0xdf, 0x7f, 0xa5, 0xe0, 0x81,
  0xc5, 0x18, 0x03, 0x63, 0x0c, 0x9a, 0xa6, 0xa1, 0x50, 0x28, 0xa0, 0x52,
  0xa9, 0xa0, 0x5e, 0xaf, 0x6b, 0x00, 0x5e, 0xac, 0xd7, 0xeb, 0x47, 0x0f,
  0x86, 0x8e, 0x41, 0x55, 0x55, 0x61, 0x9a, 0x26, 0x2a, 0x95, 0x0a, 0x6c,
  0xdb, 0xb6, 0x82, 0x20, 0x78, 0x76, 0x36, 0xb4, 0xc7, 0xf6, 0x93, 0x55,
  0xab, 0x55, 0x26, 0x84, 0x78, 0xac, 0x1c, 0x5f, 0xf3, 0xb9, 0xa5, 0xeb,
  0x3a, 0x2c, 0xcb, 0x82, 0xae, 0xeb, 0xbb, 0x03, 0xa4, 0x69, 0xda, 0xd9,
  0x53, 0x29, 0x8a, 0x02, 0xd3, 0x34, 0x99, 0x61, 0x18, 0xcb, 0x8b, 0x56,
  0x3b, 0xc6, 0xfe, 0x4e, 0x76, 0x77, 0x15, 0x44, 0x44, 0xe0, 0x9c, 0x0b,
  0x22, 0xfa, 0xaa, 0x5c, 0x83, 0x48, 0x29, 0x11, 0x86, 0xe1, 0x86, 0x88,
  0xbe, 0x5f, 0x35, 0xd1, 0x6e, 0xb7, 0x83, 0xe7, 0x79, 0x3d, 0x55, 0x55,
  0x7d, 0xd0, 0x05, 0x25, 0xa5, 0x24, 0xce, 0x39, 0x4d, 0x26, 0x93, 0x45,
  0xb7, 0xdb, 0x7d, 0x42, 0x44, 0x50, 0x2e, 0x59, 0x49, 0x08, 0x81, 0xf5,
  0x7a, 0x9d, 0x4c, 0xa7, 0xd3, 0x77, 0x42, 0x88, 0x6f, 0x00, 0xa0, 0xed,
  0x0f, 0xb3, 0x2c, 0x3b, 0xe4, 0xe9, 0x5f, 0xf9, 0x23, 0xfa, 0x93, 0x6d,
  0xce, 0x39, 0x56, 0xab, 0x95, 0x18, 0x0e, 0x87, 0x9f, 0x82, 0x20, 0x78,
  0xdd, 0x6e, 0xb7, 0xd3, 0x7b, 0xe9, 0x27, 0xa2, 0x7b, 0x08, 0x11, 0x21,
  0x4d, 0x53, 0x70, 0xce, 0x11, 0xc7, 0xb1, 0x74, 0x5d, 0xd7, 0x9f, 0xcd,
  0x66, 0x3d, 0xce, 0xf9, 0x4b, 0xc7, 0x71, 0xee, 0x0e, 0xef, 0x70, 0x34,
  0x1a, 0xe1, 0x7f, 0xff, 0x51, 0x92, 0x24, 0xd8, 0x6e, 0xb7, 0x61, 0x14,
  0x45, 0x9f, 0x39, 0xe7, 0x6f, 0x19, 0x63, 0xb7, 0x8e, 0xe3, 0x44, 0xc7,
  0x7d, 0xbf, 0x07, 0x00, 0x5f, 0x77, 0x46, 0x8c, 0x30, 0x2c, 0xd8, 0x9d,
  0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};
unsigned int search_r_png_len = 612;

static unsigned char close_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
  0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x0b, 0x00, 0x00, 0x00, 0x0b,
  0x08, 0x06, 0x00, 0x00, 0x00, 0xa9, 0xac, 0x77, 0x26, 0x00, 0x00, 0x00,
  0xd8, 0x49, 0x44, 0x41, 0x54, 0x18, 0x19, 0x75, 0x51, 0xbd, 0x12, 0x46,
  0x40, 0x0c, 0xdc, 0x18, 0x15, 0x0a, 0x14, 0x14, 0x1a, 0x43, 0xeb, 0x35,
  0xbc, 0x7f, 0xa7, 0x43, 0x67, 0x06, 0x33, 0x28, 0xd0, 0xde, 0x77, 0x7b,
  0x23, 0x2a, 0xdf, 0x16, 0x97, 0x9f, 0xdb, 0xcb, 0x26, 0x39, 0xc1, 0x83,
  0x7d, 0xdf, 0xcd, 0xb2, 0x2c, 0xd8, 0xb6, 0x0d, 0xe7, 0x79, 0x22, 0x8a,
  0x22, 0xc4, 0x71, 0x8c, 0x3c, 0xcf, 0x91, 0xa6, 0xa9, 0x90, 0xe6, 0x8e,
  0x69, 0x9a, 0xcc, 0x38, 0x8e, 0xb8, 0xae, 0x4b, 0xdf, 0xbe, 0x36, 0x0c,
  0x43, 0x94, 0x65, 0x89, 0xa2, 0x28, 0xc4, 0x3b, 0x8e, 0xe3, 0x2f, 0x91,
  0x2f, 0xa8, 0xc2, 0x42, 0x56, 0xd1, 0x78, 0xf3, 0x3c, 0xbb, 0x04, 0x2f,
  0xda, 0xb6, 0x45, 0x55, 0x55, 0x74, 0x9d, 0x65, 0x2c, 0x22, 0xb8, 0xef,
  0x1b, 0xeb, 0xba, 0xc2, 0x67, 0x8f, 0x4c, 0x10, 0x7d, 0xdf, 0xa3, 0xae,
  0x6b, 0xe7, 0xd3, 0x32, 0x56, 0x90, 0xe7, 0x53, 0x46, 0x31, 0x0c, 0x83,
  0x73, 0x95, 0xa8, 0x31, 0x93, 0x9c, 0xc7, 0xe3, 0xd4, 0x0a, 0xb6, 0xa0,
  0x44, 0x5a, 0xc6, 0xc6, 0x18, 0x77, 0xcd, 0x41, 0xbd, 0x24, 0x49, 0x94,
  0xfb, 0x12, 0x59, 0x51, 0x5b, 0xd2, 0x16, 0xed, 0xfa, 0x20, 0xdc, 0x6f,
  0xd7, 0x75, 0x9f, 0x6b, 0xd3, 0x2a, 0x41, 0x10, 0xa0, 0x69, 0x1a, 0x57,
  0x59, 0x28, 0x47, 0x99, 0x2f, 0x30, 0xcf, 0x7b, 0xfb, 0x41, 0xcf, 0x1a,
  0x2c, 0xeb, 0xeb, 0x07, 0x29, 0x9d, 0x65, 0x19, 0x6c, 0xab, 0x6e, 0x5d,
  0x3f, 0x07, 0x0a, 0x79, 0x90, 0x0e, 0x11, 0x45, 0xc2, 0x00, 0x00, 0x00,
  0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};
static unsigned int close_png_len = 273;


static unsigned char closed_png[81] =
{
  0,  0,  0,  0,142,  0,  0,  0,  0,
  0,  0,  0,  0,142,142,  0,  0,  0,
  0,  0,  0,  0,142,142,142,  0,  0,
  0,  0,  0,  0,142,142,142,142,  0,
  0,  0,  0,  0,142,142,142,142,142,
  0,  0,  0,  0,142,142,142,142,  0,
  0,  0,  0,  0,142,142,142,  0,  0,
  0,  0,  0,  0,142,142,  0,  0,  0,
  0,  0,  0,  0,142,  0,  0,  0,  0
};

static unsigned char closed_a_png[81] =
{
  0,  0,  0,  0,255,  0,  0,  0,  0,
  0,  0,  0,  0,255,255,  0,  0,  0,
  0,  0,  0,  0,255,255,255,  0,  0,
  0,  0,  0,  0,255,255,255,255,  0,
  0,  0,  0,  0,255,255,255,255,255,
  0,  0,  0,  0,255,255,255,255,  0,
  0,  0,  0,  0,255,255,255,  0,  0,
  0,  0,  0,  0,255,255,  0,  0,  0,
  0,  0,  0,  0,255,  0,  0,  0,  0
};

static unsigned char open_png[81] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,
  142,142,142,142,142,142,142,142,142,
    0,142,142,142,142,142,142,142,  0,
    0,  0,142,142,142,142,142,  0,  0,
    0,  0,  0,142,142,142,  0,  0,  0,
    0,  0,  0,  0,142,  0,  0,  0,  0
};

static unsigned char open_a_png[81] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,
  255,255,255,255,255,255,255,255,255,
    0,255,255,255,255,255,255,255,  0,
    0,  0,255,255,255,255,255,  0,  0,
    0,  0,  0,255,255,255,  0,  0,  0,
    0,  0,  0,  0,255,  0,  0,  0,  0
};

static unsigned char bdwn_png[7*8] =
{
    0,  0,  0,142,  0,  0,  0,
    0,  0,  0,142,  0,  0,  0,
    0,  0,  0,142,  0,  0,  0,
  142,  0,  0,142,  0,  0,142,
  142,142,  0,142,  0,142,142,
  142,142,142,142,142,142,142,
    0,142,142,142,142,142,  0,
    0,  0,142,142,142,  0,  0,
};

static unsigned char bdwn_a_png[7*8] =
{
    0,  0,  0,255,  0,  0,  0,
    0,  0,  0,255,  0,  0,  0,
    0,  0,  0,255,  0,  0,  0,
  128,  0,  0,255,  0,  0,128,
  255,128,  0,255,  0,128,255,
  128,255,128,255,128,255,128,
    0,128,255,255,255,128,  0,
    0,  0,128,255,128,  0,  0,
};

static unsigned char sync_on_png[576] =
{
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,138,128,128,128,128,133,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,129,205,186,128,128,128,128,160,210,134,128,128,128,128,128,128,128,
  128,128,128,128,128,128,139,217,255,181,128,128,128,128,152,255,229,147,128,128,128,128,128,128,
  128,128,128,128,128,156,236,255,255,181,128,128,128,128,152,255,255,243,164,128,128,128,128,128,
  128,128,128,128,175,249,255,255,255,223,196,198,198,197,211,255,255,255,253,186,128,128,128,128,
  128,128,133,202,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,214,137,128,128,
  128,128,135,217,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,225,140,128,128,
  128,128,128,128,189,255,255,255,255,238,224,225,225,224,232,255,255,255,255,201,131,128,128,128,
  128,128,128,128,128,167,245,255,255,183,128,128,128,128,155,255,255,250,179,128,128,128,128,128,
  128,128,128,128,128,128,150,231,255,188,128,128,128,128,161,255,238,158,128,128,128,128,128,128,
  128,128,128,128,128,128,128,136,216,188,128,128,128,128,161,223,142,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,130,141,128,128,128,128,135,132,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
};

static unsigned char sync_off_png[576] =
{
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,138,128,128,128,128,128,128,133,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,129,205,186,128,128,128,128,128,128,160,210,134,128,128,128,128,128,128,
  128,128,128,128,128,139,217,255,181,128,128,128,128,128,128,152,255,229,147,128,128,128,128,128,
  128,128,128,128,156,236,255,255,181,128,128,128,128,128,128,152,255,255,243,164,128,128,128,128,
  128,128,128,175,249,255,255,255,223,196,198,198,128,128,197,211,255,255,255,253,186,128,128,128,
  128,128,202,255,255,255,255,255,255,255,255,225,128,128,255,255,255,255,255,255,255,214,128,128,
  128,128,217,255,255,255,255,255,255,255,255,128,128,198,255,255,255,255,255,255,255,225,128,128,
  128,128,128,189,255,255,255,255,238,224,225,128,128,225,224,232,255,255,255,255,201,128,128,128,
  128,128,128,128,167,245,255,255,183,128,128,128,128,128,128,155,255,255,250,179,128,128,128,128,
  128,128,128,128,128,150,231,255,188,128,128,128,128,128,128,161,255,238,158,128,128,128,128,128,
  128,128,128,128,128,128,136,216,188,128,128,128,128,128,128,161,223,142,128,128,128,128,128,128,
  128,128,128,128,128,128,128,130,141,128,128,128,128,128,128,135,132,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,
  128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
};

static unsigned char sync_a_png[576] =
{
    0,  0,  0,  0,  0,  0,  0, 29, 98,157,207,231,234,211,164,104, 38,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0, 21,143,234,255,255,255,255,255,255,255,255,244,155, 33,  0,  0,  0,  0,  0,
    0,  0,  0,  0, 70,221,255,255,255,255,255,255,255,255,255,255,255,255,235, 93,  0,  0,  0,  0,
    0,  0,  0, 92,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,116,  0,  0,  0,
    0,  0, 68,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255, 96,  0,  0,
    0, 20,225,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,243, 41,  0,
    0,143,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,172,  1,
   28,238,255,255,255,255,255,253,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255, 42,
   99,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,133,
  160,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,204,
  212,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,224,
  234,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,237,255,236,
  235,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,230,255,236,
  216,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,226,
  168,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,208,
  107,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,147,
   39,245,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255, 53,
    0,159,255,255,255,255,255,255,251,255,255,255,255,255,255,255,255,255,255,255,255,255,190,  3,
    0, 31,239,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,249, 54,  0,
    0,  0, 91,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,119,  0,  0,
    0,  0,  0,116,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,145,  0,  0,  0,
    0,  0,  0,  0, 98,240,255,255,255,255,255,255,255,255,255,255,255,255,248,119,  0,  0,  0,  0,
    0,  0,  0,  0,  0, 45,168,252,255,255,255,255,255,255,255,255,255,184, 58,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0, 45,131,201,222,234,236,224,204,142, 54,  0,  0,  0,  0,  0,  0,  0
};


//------------------------------------------------------------------------

static const char tabs_css[] = 
".tabs, .tabs2, .tabs3 {\n"
"    background-image: url('tab_b.png');\n"
"    width: 100%;\n"
"    z-index: 101;\n"
"    font-size: 13px;\n"
"    font-family: 'Lucida Grande',Geneva,Helvetica,Arial,sans-serif;\n"
"}\n"
"\n"
".tabs2 {\n"
"    font-size: 10px;\n"
"}\n"
".tabs3 {\n"
"    font-size: 9px;\n"
"}\n"
"\n"
".tablist {\n"
"    margin: 0;\n"
"    padding: 0;\n"
"    display: table;\n"
"}\n"
"\n"
".tablist li {\n"
"    float: left;\n"
"    display: table-cell;\n"
"    background-image: url('tab_b.png');\n"
"    line-height: 36px;\n"
"    list-style: none;\n"
"}\n"
"\n"
".tablist a {\n"
"    display: block;\n"
"    padding: 0 20px;\n"
"    font-weight: bold;\n"
"    background-image:url('tab_s.png');\n"
"    background-repeat:no-repeat;\n"
"    background-position:right;\n"
"    color: ##30;\n"
"    text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.9);\n"
"    text-decoration: none;\n"
"    outline: none;\n"
"}\n"
"\n"
".tabs3 .tablist a {\n"
"    padding: 0 10px;\n"
"}\n"
"\n"
".tablist a:hover {\n"
"    background-image: url('tab_h.png');\n"
"    background-repeat:repeat-x;\n"
"    color: #fff;\n"
"    text-shadow: 0px 1px 1px rgba(0, 0, 0, 1.0);\n"
"    text-decoration: none;\n"
"}\n"
"\n"
".tablist li.current a {\n"
"    background-image: url('tab_a.png');\n"
"    background-repeat:repeat-x;\n"
"    color: #fff;\n"
"    text-shadow: 0px 1px 1px rgba(0, 0, 0, 1.0);\n"
"}\n"
;

struct img_data_item
{
  const char *name;
  unsigned char *content;
  unsigned int len;
};


static void writeImgData(const char *dir,img_data_item *data)
{
  while (data->name)
  {
    QCString fileName;
    fileName=(QCString)dir+"/"+data->name;
    QFile f(fileName);
    if (f.open(IO_WriteOnly))
    {
      f.writeBlock((char*)data->content,
                    data->len>0 ? data->len : qstrlen((char*)data->content));
    }
    else
    {
      fprintf(stderr,"Warning: Cannot open file %s for writing\n",data->name);
    }
    Doxygen::indexList->addImageFile(QCString("/search/")+data->name);
    data++;
  }
}

static ColoredImgDataItem colored_tab_data[] =
{
  // file_name      W   H  luma_data        alpha_data
  { "tab_a.png",    1, 36, tab_a_png,       0 },
  { "tab_b.png",    1, 36, tab_b_png,       0 },
  { "tab_h.png",    1, 36, tab_h_png,       0 },
  { "tab_s.png",    1, 36, tab_s_png,       0 },
  { "nav_h.png",    1, 12, header_png,      0 },
  { "nav_f.png",    1, 56, func_header_png, 0 },
  { "bc_s.png",     8, 30, bc_s_png,        bc_s_a_png },
  { "doxygen.png", 104,31, doxygen_png,     doxygen_a_png },
  { "closed.png",   9,  9, closed_png,      closed_a_png },
  { "open.png",     9,  9, open_png,        open_a_png },
  { "bdwn.png",     7,  8, bdwn_png,        bdwn_a_png },
  { "sync_on.png", 24, 24, sync_on_png,     sync_a_png },
  { "sync_off.png",24, 24, sync_off_png,    sync_a_png },
  { 0, 0, 0, 0, 0 }
};

static img_data_item search_client_data[] =
{
  // file_name          raw_data          num bytes
  { "mag_sel.png",      mag_sel_png,      mag_sel_png_len  },
  { "search_l.png",     search_l_png,     search_l_png_len },
  { "search_m.png",     search_m_png,     search_m_png_len },
  { "search_r.png",     search_r_png,     search_r_png_len },
  { "close.png",        close_png,        close_png_len    },
  { 0, 0, 0 }
};

static img_data_item search_server_data[] =
{
  // file_name          raw_data          num bytes
  { "mag.png",          mag_png,          mag_png_len      },
  { "search_l.png",     search_l_png,     search_l_png_len },
  { "search_m.png",     search_m_png,     search_m_png_len },
  { "search_r.png",     search_r_png,     search_r_png_len },
  { 0, 0, 0 }
};

//------------------------------------------------------------------------

static void writeClientSearchBox(FTextStream &t,const char *relPath)
{
  t << "        <div id=\"MSearchBox\" class=\"MSearchBoxInactive\">\n";
  t << "        <span class=\"left\">\n";
  t << "          <img id=\"MSearchSelect\" src=\"" << relPath << "search/mag_sel.png\"\n";
  t << "               onmouseover=\"return searchBox.OnSearchSelectShow()\"\n";
  t << "               onmouseout=\"return searchBox.OnSearchSelectHide()\"\n";
  t << "               alt=\"\"/>\n";
  t << "          <input type=\"text\" id=\"MSearchField\" value=\"" 
    << theTranslator->trSearch() << "\" accesskey=\"S\"\n";
  t << "               onfocus=\"searchBox.OnSearchFieldFocus(true)\" \n";
  t << "               onblur=\"searchBox.OnSearchFieldFocus(false)\" \n";
  t << "               onkeyup=\"searchBox.OnSearchFieldChange(event)\"/>\n";
  t << "          </span><span class=\"right\">\n";
  t << "            <a id=\"MSearchClose\" href=\"javascript:searchBox.CloseResultsWindow()\">"
    << "<img id=\"MSearchCloseImg\" border=\"0\" src=\"" << relPath << "search/close.png\" alt=\"\"/></a>\n";
  t << "          </span>\n";
  t << "        </div>\n";
}

static void writeServerSearchBox(FTextStream &t,const char *relPath,bool highlightSearch)
{
  static bool externalSearch = Config_getBool("EXTERNAL_SEARCH");
  t << "        <div id=\"MSearchBox\" class=\"MSearchBoxInactive\">\n";
  t << "          <div class=\"left\">\n";
  t << "            <form id=\"FSearchBox\" action=\"" << relPath;
  if (externalSearch)
  {
    t << "search" << Doxygen::htmlFileExtension;
  }
  else
  {
    t << "search.php";
  }
  t << "\" method=\"get\">\n";
  t << "              <img id=\"MSearchSelect\" src=\"" << relPath << "search/mag.png\" alt=\"\"/>\n";
  if (!highlightSearch)
  {
    t << "              <input type=\"text\" id=\"MSearchField\" name=\"query\" value=\""
      << theTranslator->trSearch() << "\" size=\"20\" accesskey=\"S\" \n";
    t << "                     onfocus=\"searchBox.OnSearchFieldFocus(true)\" \n";
    t << "                     onblur=\"searchBox.OnSearchFieldFocus(false)\"/>\n";
    t << "            </form>\n";
    t << "          </div><div class=\"right\"></div>\n";
    t << "        </div>\n";
  }
}

//------------------------------------------------------------------------

/// substitute all occurrences of \a src in \a s by \a dst
QCString substitute(const char *s,const char *src,const char *dst)
{
  if (s==0 || src==0) return s;
  const char *p, *q;
  int srcLen = qstrlen(src);
  int dstLen = dst ? qstrlen(dst) : 0;
  int resLen;
  if (srcLen!=dstLen)
  {
    int count;
    for (count=0, p=s; (q=strstr(p,src))!=0; p=q+srcLen) count++;
    resLen = (int)(p-s)+qstrlen(p)+count*(dstLen-srcLen);
  }
  else // result has same size as s
  {
    resLen = qstrlen(s);
  }
  QCString result(resLen+1);
  char *r;
  for (r=result.data(), p=s; (q=strstr(p,src))!=0; p=q+srcLen)
  {
    int l = (int)(q-p);
    memcpy(r,p,l);
    r+=l;
    if (dst) memcpy(r,dst,dstLen);
    r+=dstLen;
  }
  qstrcpy(r,p);
  //printf("substitute(%s,%s,%s)->%s\n",s,src,dst,result.data());
  return result;
}
//----------------------------------------------------------------------

/// Clear a text block \a s from \a begin to \a end markers
QCString clearBlock(const char *s,const char *begin,const char *end)
{
  if (s==0 || begin==0 || end==0) return s;
  const char *p, *q;
  int beginLen = qstrlen(begin);
  int endLen = qstrlen(end);
  int resLen = 0;
  for (p=s; (q=strstr(p,begin))!=0; p=q+endLen)
  {
    resLen+=(int)(q-p);
    p=q+beginLen;
    if ((q=strstr(p,end))==0)
    {
      resLen+=beginLen;
      break;
    }
  }
  resLen+=qstrlen(p);
  // resLen is the length of the string without the marked block

  QCString result(resLen+1);
  char *r;
  for (r=result.data(), p=s; (q=strstr(p,begin))!=0; p=q+endLen)
  {
    int l = (int)(q-p);
    memcpy(r,p,l);
    r+=l;
    p=q+beginLen;
    if ((q=strstr(p,end))==0)
    {
      memcpy(r,begin,beginLen);
      r+=beginLen;
      break;
    }
  }
  qstrcpy(r,p);
  return result;
}
//----------------------------------------------------------------------

QCString selectBlock(const QCString& s,const QCString &name,bool enable)
{
  const QCString begin = "<!--BEGIN " + name + "-->";
  const QCString end = "<!--END " + name + "-->";
  const QCString nobegin = "<!--BEGIN !" + name + "-->";
  const QCString noend = "<!--END !" + name + "-->";

  QCString result = s;
  if (enable)
  {
    result = substitute(result, begin, "");
    result = substitute(result, end, "");
    result = clearBlock(result, nobegin, noend);
  }
  else
  {
    result = substitute(result, nobegin, "");
    result = substitute(result, noend, "");
    result = clearBlock(result, begin, end);
  }

  return result;
}

static QCString getSearchBox(bool serverSide, QCString relPath, bool highlightSearch)
{
  QGString result;
  FTextStream t(&result);
  if (serverSide) {
    writeServerSearchBox(t, relPath, highlightSearch);
  }
  else {
     writeClientSearchBox(t, relPath);
  }
  return QCString(result);
}

static QCString removeEmptyLines(const QCString &s)
{
  BufStr out(s.length()+1);
  char *p=s.data();
  if (p)
  {
    char c;
    while ((c=*p++))
    {
      if (c=='\n')
      {
        char *e = p;
        while (*e==' ' || *e=='\t') e++;
        if (*e=='\n') 
        {
          p=e;
        }
        else out.addChar(c);
      }
      else
      {
        out.addChar(c);
      }
    }
  }
  out.addChar('\0');
  //printf("removeEmptyLines(%s)=%s\n",s.data(),out.data());
  return out.data();
}

static QCString substituteHtmlKeywords(const QCString &s,
                                       const QCString &title,
                                       const QCString &relPath,
                                       const QCString &navPath=QCString())
{
  // Build CSS/Javascript tags depending on treeview, search engine settings
  QCString cssFile;
  QCString extraCssFile;
  QCString generatedBy;
  QCString treeViewCssJs;
  QCString searchCssJs;
  QCString searchBox;
  QCString mathJaxJs;
  QCString extraCssText;

  static QCString projectName = Config_getString("PROJECT_NAME");
  static bool timeStamp = Config_getBool("HTML_TIMESTAMP");
  static bool treeView = Config_getBool("GENERATE_TREEVIEW");
  static bool searchEngine = Config_getBool("SEARCHENGINE");
  static bool serverBasedSearch = Config_getBool("SERVER_BASED_SEARCH");
  static bool mathJax = Config_getBool("USE_MATHJAX");
  static QCString mathJaxFormat = Config_getEnum("MATHJAX_FORMAT");
  static bool disableIndex = Config_getBool("DISABLE_INDEX");
  static bool hasProjectName = !projectName.isEmpty();
  static bool hasProjectNumber = !Config_getString("PROJECT_NUMBER").isEmpty();
  static bool hasProjectBrief = !Config_getString("PROJECT_BRIEF").isEmpty();
  static bool hasProjectLogo = !Config_getString("PROJECT_LOGO").isEmpty();
  static bool titleArea = (hasProjectName || hasProjectBrief || hasProjectLogo || (disableIndex && searchEngine));

  cssFile = Config_getString("HTML_STYLESHEET");
  if (cssFile.isEmpty())
  {
    cssFile = "doxygen.css";
  }
  else
  {
    QFileInfo cssfi(cssFile);
    if (cssfi.exists())
    {
      cssFile = cssfi.fileName().utf8();
    }
    else
    {
      cssFile = "doxygen.css";
    }
  }
  extraCssFile = Config_getString("HTML_EXTRA_STYLESHEET");
  if (!extraCssFile.isEmpty())
  {
    extraCssText = "<link href=\"$relpath^"+stripPath(extraCssFile)+"\" rel=\"stylesheet\" type=\"text/css\"/>\n";
  }

  if (timeStamp) {
    generatedBy = theTranslator->trGeneratedAt(dateToString(TRUE), Config_getString("PROJECT_NAME"));
  }
  else {
    generatedBy = theTranslator->trGeneratedBy();
  }

  if (treeView)
  {
    treeViewCssJs = "<link href=\"$relpath^navtree.css\" rel=\"stylesheet\" type=\"text/css\"/>\n"
                    "<script type=\"text/javascript\" src=\"$relpath^resize.js\"></script>\n"
                    "<script type=\"text/javascript\" src=\"$relpath^navtree.js\"></script>\n"
                    "<script type=\"text/javascript\">\n"
                    "  $(document).ready(initResizable);\n"
                    "  $(window).load(resizeHeight);\n"
                    "</script>";
  }

  if (searchEngine)
  {
    searchCssJs = "<link href=\"$relpath^search/search.css\" rel=\"stylesheet\" type=\"text/css\"/>\n";
    searchCssJs += "<script type=\"text/javascript\" src=\"$relpath^search/search.js\"></script>\n";

    if (!serverBasedSearch) 
    {
      searchCssJs += "<script type=\"text/javascript\">\n"
                     "  $(document).ready(function() { searchBox.OnSelectItem(0); });\n"
                     "</script>";
    }
    else 
    {
      searchCssJs += "<script type=\"text/javascript\">\n"
                     "  $(document).ready(function() {\n"
                     "    if ($('.searchresults').length > 0) { searchBox.DOMSearchField().focus(); }\n"
                     "  });\n"
                     "</script>\n";

      // OPENSEARCH_PROVIDER {
      searchCssJs += "<link rel=\"search\" href=\"" + relPath +
                     "search-opensearch.php?v=opensearch.xml\" "
                     "type=\"application/opensearchdescription+xml\" title=\"" +
                     (hasProjectName ? projectName : QCString("Doxygen")) + 
                     "\"/>";
      // OPENSEARCH_PROVIDER }
    }
    searchBox = getSearchBox(serverBasedSearch, relPath, FALSE);
  }

  if (mathJax)
  {
    QCString path = Config_getString("MATHJAX_RELPATH");  
    if (!path.isEmpty() && path.at(path.length()-1)!='/')  
    {   
      path+="/";   
    }   
    if (path.isEmpty() || path.left(2)=="..") // relative path  
    {  
      path.prepend(relPath);   
    }  
    mathJaxJs = "<script type=\"text/x-mathjax-config\">\n"
                "  MathJax.Hub.Config({\n"
                "    extensions: [\"tex2jax.js\"";
    QStrList &mathJaxExtensions = Config_getList("MATHJAX_EXTENSIONS");
    const char *s = mathJaxExtensions.first();
    while (s)
    {
      mathJaxJs+= ", \""+QCString(s)+".js\"";
      s = mathJaxExtensions.next();
    }
    if (mathJaxFormat.isEmpty())
    {
      mathJaxFormat = "HTML-CSS";
    }
    mathJaxJs += "],\n"
                 "    jax: [\"input/TeX\",\"output/"+mathJaxFormat+"\"],\n"
                 "});\n"
                 "</script>";
    mathJaxJs += "<script src=\"" + path + "MathJax.js\"></script>\n";
  }

  // first substitute generic keywords
  QCString result = substituteKeywords(s,title,
        convertToHtml(Config_getString("PROJECT_NAME")),
        convertToHtml(Config_getString("PROJECT_NUMBER")),
        convertToHtml(Config_getString("PROJECT_BRIEF")));

  // additional HTML only keywords
  result = substitute(result,"$navpath",navPath);
  result = substitute(result,"$stylesheet",cssFile);
  result = substitute(result,"$treeview",treeViewCssJs);
  result = substitute(result,"$searchbox",searchBox);
  result = substitute(result,"$search",searchCssJs);
  result = substitute(result,"$mathjax",mathJaxJs);
  result = substitute(result,"$generatedby",generatedBy);
  result = substitute(result,"$extrastylesheet",extraCssText);
  result = substitute(result,"$relpath$",relPath); //<-- obsolete: for backwards compatibility only
  result = substitute(result,"$relpath^",relPath); //<-- must be last
  
  // additional HTML only conditional blocks
  result = selectBlock(result,"DISABLE_INDEX",disableIndex);
  result = selectBlock(result,"GENERATE_TREEVIEW",treeView);
  result = selectBlock(result,"SEARCHENGINE",searchEngine);
  result = selectBlock(result,"TITLEAREA",titleArea);
  result = selectBlock(result,"PROJECT_NAME",hasProjectName);
  result = selectBlock(result,"PROJECT_NUMBER",hasProjectNumber);
  result = selectBlock(result,"PROJECT_BRIEF",hasProjectBrief);
  result = selectBlock(result,"PROJECT_LOGO",hasProjectLogo);

  result = removeEmptyLines(result);

  return result;
}

//--------------------------------------------------------------------------

HtmlCodeGenerator::HtmlCodeGenerator()
   : m_streamSet(FALSE), m_col(0)
{
}

HtmlCodeGenerator::HtmlCodeGenerator(FTextStream &t,const QCString &relPath) 
   : m_col(0), m_relPath(relPath)
{
  setTextStream(t);
}

void HtmlCodeGenerator::setTextStream(FTextStream &t)
{
  m_streamSet = t.device()!=0;
  m_t.setDevice(t.device());
}

void HtmlCodeGenerator::setRelativePath(const QCString &path)
{
  m_relPath = path;
}

void HtmlCodeGenerator::codify(const char *str)
{
  static int tabSize = Config_getInt("TAB_SIZE");
  if (str)
  { 
    const char *p=str;
    char c;
    int spacesToNextTabStop;
    while (*p)
    {
      c=*p++;
      switch(c)
      {
        case '\t': spacesToNextTabStop = 
                         tabSize - (m_col%tabSize); 
                   m_t << Doxygen::spaces.left(spacesToNextTabStop); 
                   m_col+=spacesToNextTabStop; 
                   break; 
        case '\n': m_t << "\n"; m_col=0; 
                   break;
        case '\r': break;
        case '<':  m_t << "&lt;"; m_col++; 
                   break;
        case '>':  m_t << "&gt;"; m_col++; 
                   break;
        case '&':  m_t << "&amp;"; m_col++; 
                   break;
        case '\'': m_t << "&#39;"; m_col++; // &apos; is not valid XHTML
                   break;
        case '"':  m_t << "&quot;"; m_col++;
                   break;
        case '\\':
                   if (*p=='<')
                     { m_t << "&lt;"; p++; }
                   else if (*p=='>')
                     { m_t << "&gt;"; p++; }
                   else
                     m_t << "\\";
                   m_col++;
                   break;
        default:   p=writeUtf8Char(m_t,p-1);    
                   m_col++;                    
                   break;
      }
    }
  }
}

void HtmlCodeGenerator::docify(const char *str)
{
  if (str)
  {
    const char *p=str;
    char c;
    while (*p)
    {
      c=*p++;
      switch(c)
      {
        case '<':  m_t << "&lt;"; break;
        case '>':  m_t << "&gt;"; break;
        case '&':  m_t << "&amp;"; break;
        case '"':  m_t << "&quot;"; break;
        case '\\':
                   if (*p=='<')
                     { m_t << "&lt;"; p++; }
                   else if (*p=='>')
                     { m_t << "&gt;"; p++; }
                   else
                     m_t << "\\";
                   break;
        default:   m_t << c; 
      }
    }
  }
}

void HtmlCodeGenerator::writeLineNumber(const char *ref,const char *filename,
                                    const char *anchor,int l)
{
  QCString lineNumber,lineAnchor;
  lineNumber.sprintf("%5d",l);
  lineAnchor.sprintf("l%05d",l);

  if (filename)
  {
    startCodeAnchor(lineAnchor);
    writeCodeLink(ref,filename,anchor,lineNumber,0);
    endCodeAnchor();
  }
  else
  {
    startCodeAnchor(lineAnchor);
    codify(lineNumber);
    endCodeAnchor();
  }
  m_t << "&#160;";
}


void HtmlCodeGenerator::writeCodeLink(const char *ref,const char *f,
                                      const char *anchor, const char *name,
                                      const char *tooltip)
{
  if (!m_streamSet) return;
  //printf("writeCodeLink(ref=%s,f=%s,anchor=%s,name=%s,tooltip=%s)\n",ref,f,anchor,name,tooltip);
  if (ref) 
  {
    m_t << "<a class=\"codeRef\" ";
    m_t << externalLinkTarget() << externalRef(m_relPath,ref,FALSE);
  }
  else
  {
    m_t << "<a class=\"code\" ";
  }
  m_t << "href=\"";
  m_t << externalRef(m_relPath,ref,TRUE);
  if (f) m_t << f << Doxygen::htmlFileExtension;
  if (anchor) m_t << "#" << anchor;
  m_t << "\"";
  if (tooltip) m_t << " title=\"" << tooltip << "\"";
  m_t << ">";
  docify(name);
  m_t << "</a>";
  m_col+=qstrlen(name);
}

void HtmlCodeGenerator::startCodeLine(bool hasLineNumbers) 
{ 
  if (!hasLineNumbers) m_t << "<div class=\"line\">";
  m_col=0; 
}

void HtmlCodeGenerator::endCodeLine() 
{ 
  m_t << "</div>\n";
}

void HtmlCodeGenerator::startCodeAnchor(const char *label) 
{ 
  m_t << "<div class=\"line\">";
  m_t << "<a name=\"" << label << "\"></a><span class=\"lineno\">"; 
}

void HtmlCodeGenerator::endCodeAnchor() 
{ 
  m_t << "</span>"; 
}


void HtmlCodeGenerator::startFontClass(const char *s) 
{ 
  if (m_streamSet) m_t << "<span class=\"" << s << "\">"; 
}

void HtmlCodeGenerator::endFontClass() 
{ 
  if (m_streamSet) m_t << "</span>"; 
}

void HtmlCodeGenerator::writeCodeAnchor(const char *anchor) 
{ 
  if (m_streamSet) m_t << "<a name=\"" << anchor << "\"></a>"; 
}

//--------------------------------------------------------------------------

HtmlGenerator::HtmlGenerator() : OutputGenerator()
{
  dir=Config_getString("HTML_OUTPUT");
  m_emptySection=FALSE;
}

HtmlGenerator::~HtmlGenerator()
{
  //printf("HtmlGenerator::~HtmlGenerator()\n");
}

void HtmlGenerator::init()
{
  QCString dname=Config_getString("HTML_OUTPUT");
  QDir d(dname);
  if (!d.exists() && !d.mkdir(dname))
  {
    err("Could not create output directory %s\n",dname.data());
    exit(1);
  }
  //writeLogo(dname);
  if (!Config_getString("HTML_HEADER").isEmpty()) 
  {
    g_header=fileToString(Config_getString("HTML_HEADER"));
    //printf("g_header='%s'\n",g_header.data());
  }
  else 
  {
    g_header = defaultHtmlHeader;
  }

  if (!Config_getString("HTML_FOOTER").isEmpty()) 
  {
    g_footer=fileToString(Config_getString("HTML_FOOTER"));
    //printf("g_footer='%s'\n",g_footer.data());
  }
  else 
  {
    g_footer = defaultHtmlFooter;
  }
  createSubDirs(d);

  QCString fileName=dname+"/tabs.css";
  QFile f(fileName);
  if (f.open(IO_WriteOnly))
  {
    FTextStream t(&f);
    t << replaceColorMarkers(tabs_css);
  }
  else
  {
    fprintf(stderr,"Warning: Cannot open file %s for writing\n",fileName.data());
  }

  {
    QFile f(dname+"/jquery.js");
    if (f.open(IO_WriteOnly))
    {
      FTextStream t(&f);
      t << search_jquery_script1 << search_jquery_script2 << search_jquery_script3;
      if (Config_getBool("GENERATE_TREEVIEW"))
      {
        t << search_jquery_script4 << search_jquery_script5;
      }
    }
  }

  if (Config_getBool("INTERACTIVE_SVG"))
  {
    QFile f(dname+"/svgpan.js");
    if (f.open(IO_WriteOnly))
    {
      FTextStream t(&f);
      t << svgpan_script;
    }
  }

  {
    QFile f(dname+"/dynsections.js");
    if (f.open(IO_WriteOnly))
    {
      FTextStream t(&f);
      t << dynsections_script;
    }
  }
}

/// Additional initialization after indices have been created
void HtmlGenerator::writeTabData()
{
  Doxygen::indexList->addStyleSheetFile("tabs.css");
  QCString dname=Config_getString("HTML_OUTPUT");
  writeColoredImgData(dname,colored_tab_data);

  {
    unsigned char shadow[6] = { 5, 5, 5, 5, 5, 5 };
    unsigned char shadow_alpha[6]  = { 80, 60, 40, 20, 10, 0 };
    ColoredImage img(1,6,shadow,shadow_alpha,0,0,100);
    img.save(dname+"/nav_g.png");
  }
}

void HtmlGenerator::writeSearchData(const char *dir)
{
  static bool serverBasedSearch = Config_getBool("SERVER_BASED_SEARCH");
  writeImgData(dir,serverBasedSearch ? search_server_data : search_client_data);
  QCString searchDirName = Config_getString("HTML_OUTPUT")+"/search";
  QFile f(searchDirName+"/search.css");
  if (f.open(IO_WriteOnly))
  {
    FTextStream t(&f);
    QCString searchCss = replaceColorMarkers(search_styleSheet);
    searchCss = substitute(searchCss,"$doxygenversion",versionString);
    if (Config_getBool("DISABLE_INDEX"))
    {
      // move up the search box if there are no tabs
      searchCss = substitute(searchCss,"margin-top: 8px;","margin-top: 0px;");
    }
    t << searchCss;
  }
  Doxygen::indexList->addStyleSheetFile("search/search.css");
}

void HtmlGenerator::writeStyleSheetFile(QFile &file)
{
  FTextStream t(&file);
  t << replaceColorMarkers(substitute(defaultStyleSheet,"$doxygenversion",versionString));
}

void HtmlGenerator::writeHeaderFile(QFile &file, const char * /*cssname*/)
{
  FTextStream t(&file);
  t << "<!-- HTML header for doxygen " << versionString << "-->" << endl;
  QCString contents(defaultHtmlHeader);
  t << contents;
}

void HtmlGenerator::writeFooterFile(QFile &file)
{
  FTextStream t(&file);
  t << "<!-- HTML footer for doxygen " << versionString << "-->" <<  endl;
  QCString contents(defaultHtmlFooter);
  t << contents;
}

void HtmlGenerator::startFile(const char *name,const char *,
                              const char *title)
{
  //printf("HtmlGenerator::startFile(%s)\n",name);
  QCString fileName=name;
  lastTitle=title;
  relPath = relativePathToRoot(fileName);

  if (fileName.right(Doxygen::htmlFileExtension.length())!=Doxygen::htmlFileExtension) 
  {
    fileName+=Doxygen::htmlFileExtension;
  }
  startPlainFile(fileName);
  m_codeGen.setTextStream(t);
  m_codeGen.setRelativePath(relPath);
  Doxygen::indexList->addIndexFile(fileName);
  
  lastFile = fileName;
  t << substituteHtmlKeywords(g_header,convertToHtml(title),relPath);

  t << "<!-- " << theTranslator->trGeneratedBy() << " Doxygen " 
    << versionString << " -->" << endl;
  //static bool generateTreeView = Config_getBool("GENERATE_TREEVIEW");
  static bool searchEngine = Config_getBool("SEARCHENGINE");
  if (searchEngine /*&& !generateTreeView*/)
  {
    t << "<script type=\"text/javascript\">\n";
    t << "var searchBox = new SearchBox(\"searchBox\", \""
      << relPath<< "search\",false,'" << theTranslator->trSearch() << "');\n";
    t << "</script>\n";
  }
  //generateDynamicSections(t,relPath);
  m_sectionCount=0;
}

void HtmlGenerator::writeSearchInfo(FTextStream &t,const QCString &relPath)
{
  static bool searchEngine      = Config_getBool("SEARCHENGINE");
  static bool serverBasedSearch = Config_getBool("SERVER_BASED_SEARCH");
  if (searchEngine && !serverBasedSearch)
  {
    (void)relPath;
    t << "<!-- window showing the filter options -->\n";
    t << "<div id=\"MSearchSelectWindow\"\n";
    t << "     onmouseover=\"return searchBox.OnSearchSelectShow()\"\n";
    t << "     onmouseout=\"return searchBox.OnSearchSelectHide()\"\n";
    t << "     onkeydown=\"return searchBox.OnSearchSelectKey(event)\">\n";
    writeSearchCategories(t);
    t << "</div>\n";
    t << "\n";
    t << "<!-- iframe showing the search results (closed by default) -->\n";
    t << "<div id=\"MSearchResultsWindow\">\n";
    t << "<iframe src=\"javascript:void(0)\" frameborder=\"0\" \n";
    t << "        name=\"MSearchResults\" id=\"MSearchResults\">\n";
    t << "</iframe>\n";
    t << "</div>\n";
    t << "\n";
  }
}

void HtmlGenerator::writeSearchInfo()
{
  writeSearchInfo(t,relPath);
}


QCString HtmlGenerator::writeLogoAsString(const char *path)
{
  static bool timeStamp = Config_getBool("HTML_TIMESTAMP");
  QCString result;
  if (timeStamp)
  {
    result += theTranslator->trGeneratedAt(
               dateToString(TRUE),
               Config_getString("PROJECT_NAME")
              );
  }
  else
  {
    result += theTranslator->trGeneratedBy();
  }
  result += "&#160;\n<a href=\"http://www.doxygen.org/index.html\">\n"
            "<img class=\"footer\" src=\"";
  result += path;
  result += "doxygen.png\" alt=\"doxygen\"/></a> ";
  result += versionString;
  result += " ";
  return result;
}

void HtmlGenerator::writeLogo()
{
  t << writeLogoAsString(relPath);
}

void HtmlGenerator::writePageFooter(FTextStream &t,const QCString &lastTitle,
                              const QCString &relPath,const QCString &navPath)
{
  t << substituteHtmlKeywords(g_footer,convertToHtml(lastTitle),relPath,navPath);
}

void HtmlGenerator::writeFooter(const char *navPath)
{
  writePageFooter(t,lastTitle,relPath,navPath);
}

void HtmlGenerator::endFile()
{
  endPlainFile();
}

void HtmlGenerator::startProjectNumber()
{
  t << "<h3 class=\"version\">";
}

void HtmlGenerator::endProjectNumber()
{
  t << "</h3>";
}

void HtmlGenerator::writeStyleInfo(int part)
{
  //printf("writeStyleInfo(%d)\n",part);
  if (part==0)
  {
    if (Config_getString("HTML_STYLESHEET").isEmpty()) // write default style sheet
    {
      //printf("write doxygen.css\n");
      startPlainFile("doxygen.css"); 
      
      // alternative, cooler looking titles
      //t << "H1 { text-align: center; border-width: thin none thin none;" << endl;
      //t << "     border-style : double; border-color : blue; padding-left : 1em; padding-right : 1em }" << endl;

      t << replaceColorMarkers(substitute(defaultStyleSheet,"$doxygenversion",versionString));
      endPlainFile();
      Doxygen::indexList->addStyleSheetFile("doxygen.css");
    }
    else // write user defined style sheet
    {
      QCString cssname=Config_getString("HTML_STYLESHEET");
      QFileInfo cssfi(cssname);
      if (!cssfi.exists() || !cssfi.isFile() || !cssfi.isReadable())
      {
        err("error: style sheet %s does not exist or is not readable!", Config_getString("HTML_STYLESHEET").data());
      }
      else
      {
        // convert style sheet to string
        QCString fileStr = fileToString(cssname);
        // write the string into the output dir
        startPlainFile(cssfi.fileName().utf8());
        t << fileStr;
        endPlainFile();
      }
      Doxygen::indexList->addStyleSheetFile(cssfi.fileName().utf8());
    }
    static QCString extraCssFile = Config_getString("HTML_EXTRA_STYLESHEET");
    if (!extraCssFile.isEmpty())
    {
      QFileInfo fi(extraCssFile);
      if (fi.exists())
      {
        Doxygen::indexList->addStyleSheetFile(fi.fileName().utf8());
      }
    }
  }
}

void HtmlGenerator::startDoxyAnchor(const char *,const char *,
                                    const char *anchor, const char *,
                                    const char *)
{
  t << "<a class=\"anchor\" id=\"" << anchor << "\"></a>";
}

void HtmlGenerator::endDoxyAnchor(const char *,const char *)
{
}

//void HtmlGenerator::newParagraph()
//{
//  t << endl << "<p>" << endl;
//}

void HtmlGenerator::startParagraph()
{
  t << endl << "<p>";
}

void HtmlGenerator::endParagraph()
{
  t << "</p>" << endl;
}

void HtmlGenerator::writeString(const char *text)
{
  t << text;
}

void HtmlGenerator::startIndexListItem()
{
  t << "<li>";
}

void HtmlGenerator::endIndexListItem()
{
  t << "</li>" << endl;
}

void HtmlGenerator::startIndexItem(const char *ref,const char *f)
{
  //printf("HtmlGenerator::startIndexItem(%s,%s)\n",ref,f);
  if (ref || f)
  {
    if (ref) 
    {
      t << "<a class=\"elRef\" ";
      t << externalLinkTarget() << externalRef(relPath,ref,FALSE);
    }
    else
    {
      t << "<a class=\"el\" ";
    }
    t << "href=\"";
    t << externalRef(relPath,ref,TRUE);
    if (f) t << f << Doxygen::htmlFileExtension << "\">";
  }
  else
  {
    t << "<b>";
  }
}

void HtmlGenerator::endIndexItem(const char *ref,const char *f)
{
  //printf("HtmlGenerator::endIndexItem(%s,%s,%s)\n",ref,f,name);
  if (ref || f)
  {
    t << "</a>";
  }
  else
  {
    t << "</b>";
  }
}

void HtmlGenerator::writeStartAnnoItem(const char *,const char *f,
                                       const char *path,const char *name)
{
  t << "<li>";
  if (path) docify(path);
  t << "<a class=\"el\" href=\"" << f << Doxygen::htmlFileExtension << "\">";
  docify(name);
  t << "</a> ";
}

void HtmlGenerator::writeObjectLink(const char *ref,const char *f,
                                    const char *anchor, const char *name)
{
  if (ref) 
  {
    t << "<a class=\"elRef\" ";
    t << externalLinkTarget() << externalRef(relPath,ref,FALSE);
  }
  else
  {
    t << "<a class=\"el\" ";
  }
  t << "href=\"";
  t << externalRef(relPath,ref,TRUE);
  if (f) t << f << Doxygen::htmlFileExtension;
  if (anchor) t << "#" << anchor;
  t << "\">";
  docify(name);
  t << "</a>";
}

void HtmlGenerator::startTextLink(const char *f,const char *anchor)
{
  t << "<a href=\"";
  if (f)   t << relPath << f << Doxygen::htmlFileExtension;
  if (anchor) t << "#" << anchor;
  t << "\">"; 
}

void HtmlGenerator::endTextLink()
{
  t << "</a>";
}

void HtmlGenerator::startHtmlLink(const char *url)
{
  static bool generateTreeView = Config_getBool("GENERATE_TREEVIEW");
  t << "<a ";
  if (generateTreeView) t << "target=\"top\" ";
  t << "href=\"";
  if (url) t << url;
  t << "\">"; 
}

void HtmlGenerator::endHtmlLink()
{
  t << "</a>";
}

void HtmlGenerator::startGroupHeader(int extraIndentLevel)
{
  if (extraIndentLevel==2)
  {
    t << "<h4 class=\"groupheader\">";
  }
  else if (extraIndentLevel==1)
  {
    t << "<h3 class=\"groupheader\">";
  }
  else // extraIndentLevel==0
  {
    t << "<h2 class=\"groupheader\">";
  }
}

void HtmlGenerator::endGroupHeader(int extraIndentLevel)
{
  if (extraIndentLevel==2)
  {
    t << "</h4>" << endl;
  }
  else if (extraIndentLevel==1)
  {
    t << "</h3>" << endl;
  }
  else
  {
    t << "</h2>" << endl;
  }
}

void HtmlGenerator::startSection(const char *lab,const char *,SectionInfo::SectionType type)
{
  switch(type)
  {
    case SectionInfo::Page:          t << "\n\n<h1>"; break;
    case SectionInfo::Section:       t << "\n\n<h2>"; break;
    case SectionInfo::Subsection:    t << "\n\n<h3>"; break;
    case SectionInfo::Subsubsection: t << "\n\n<h4>"; break;
    case SectionInfo::Paragraph:     t << "\n\n<h5>"; break;
    default: ASSERT(0); break;
  }
  t << "<a class=\"anchor\" id=\"" << lab << "\"></a>";
}

void HtmlGenerator::endSection(const char *,SectionInfo::SectionType type)
{
  switch(type)
  {
    case SectionInfo::Page:          t << "</h1>"; break;
    case SectionInfo::Section:       t << "</h2>"; break;
    case SectionInfo::Subsection:    t << "</h3>"; break;
    case SectionInfo::Subsubsection: t << "</h4>"; break;
    case SectionInfo::Paragraph:     t << "</h5>"; break;
    default: ASSERT(0); break;
  }
}

void HtmlGenerator::docify(const char *str)
{
  docify(str,FALSE);
}

void HtmlGenerator::docify(const char *str,bool inHtmlComment)
{
  if (str)
  {
    const char *p=str;
    char c;
    while (*p)
    {
      c=*p++;
      switch(c)
      {
        case '<':  t << "&lt;"; break;
        case '>':  t << "&gt;"; break;
        case '&':  t << "&amp;"; break;
        case '"':  t << "&quot;"; break;
        case '-':  if (inHtmlComment) t << "&#45;"; else t << "-"; break;
        case '\\':
                   if (*p=='<')
                     { t << "&lt;"; p++; }
                   else if (*p=='>')
                     { t << "&gt;"; p++; }
                   else
                     t << "\\";
                   break;
        default:   t << c; 
      }
    }
  }
}

void HtmlGenerator::writeChar(char c)
{
  char cs[2];
  cs[0]=c;
  cs[1]=0;
  docify(cs);
}

//--- helper function for dynamic sections -------------------------

static void startSectionHeader(FTextStream &t,
                               const QCString &relPath,int sectionCount)
{
  //t << "<!-- startSectionHeader -->";
  static bool dynamicSections = Config_getBool("HTML_DYNAMIC_SECTIONS");
  if (dynamicSections)
  {
    t << "<div id=\"dynsection-" << sectionCount << "\" "
         "onclick=\"return toggleVisibility(this)\" "
         "class=\"dynheader closed\" "
         "style=\"cursor:pointer;\">" << endl;
    t << "  <img id=\"dynsection-" << sectionCount << "-trigger\" src=\"" 
      << relPath << "closed.png\" alt=\"+\"/> ";
  }
  else
  {
    t << "<div class=\"dynheader\">" << endl;
  }
}

static void endSectionHeader(FTextStream &t)
{
  //t << "<!-- endSectionHeader -->";
  t << "</div>" << endl;
}

static void startSectionSummary(FTextStream &t,int sectionCount)
{
  //t << "<!-- startSectionSummary -->";
  static bool dynamicSections = Config_getBool("HTML_DYNAMIC_SECTIONS");
  if (dynamicSections)
  {
    t << "<div id=\"dynsection-" << sectionCount << "-summary\" "
         "class=\"dynsummary\" "
         "style=\"display:block;\">" << endl;
  }
}

static void endSectionSummary(FTextStream &t)
{
  //t << "<!-- endSectionSummary -->";
  static bool dynamicSections = Config_getBool("HTML_DYNAMIC_SECTIONS");
  if (dynamicSections)
  {
    t << "</div>" << endl;
  }
}

static void startSectionContent(FTextStream &t,int sectionCount)
{
  //t << "<!-- startSectionContent -->";
  static bool dynamicSections = Config_getBool("HTML_DYNAMIC_SECTIONS");
  if (dynamicSections)
  {
    t << "<div id=\"dynsection-" << sectionCount << "-content\" "
         "class=\"dyncontent\" "
         "style=\"display:none;\">" << endl;
  }
  else
  {
    t << "<div class=\"dyncontent\">" << endl;
  }
}

static void endSectionContent(FTextStream &t)
{
  //t << "<!-- endSectionContent -->";
  t << "</div>" << endl;
}

//----------------------------

void HtmlGenerator::startClassDiagram()
{
  startSectionHeader(t,relPath,m_sectionCount);
}

void HtmlGenerator::endClassDiagram(const ClassDiagram &d,
                                const char *fileName,const char *name)
{
  endSectionHeader(t);
  startSectionSummary(t,m_sectionCount);
  endSectionSummary(t);
  startSectionContent(t,m_sectionCount);
  t << " <div class=\"center\">" << endl;
  t << "  <img src=\"";
  t << relPath << fileName << ".png\" usemap=\"#";
  docify(name);
  t << "_map\" alt=\"\"/>" << endl;
  t << "  <map id=\"";
  docify(name);
  t << "_map\" name=\"";
  docify(name);
  t << "_map\">" << endl;

  d.writeImage(t,dir,relPath,fileName);
  t << " </div>";
  endSectionContent(t);
  m_sectionCount++;
}


void HtmlGenerator::startMemberList()  
{ 
  DBG_HTML(t << "<!-- startMemberList -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
  //}
  //else
  //{
  //  t << "<ul>" << endl; 
  //}
}

void HtmlGenerator::endMemberList()    
{ 
  DBG_HTML(t << "<!-- endMemberList -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
  //}
  //else
  //{
  //  t << "</ul>" << endl; 
  //}
}

// anonymous type:
//  0 = single column right aligned
//  1 = double column left aligned
//  2 = single column left aligned
void HtmlGenerator::startMemberItem(const char *anchor,int annoType,const char *inheritId) 
{ 
  DBG_HTML(t << "<!-- startMemberItem() -->" << endl)
  if (m_emptySection)
  {
    t << "<table class=\"memberdecls\">" << endl;
    m_emptySection=FALSE;
  }
  t << "<tr class=\"memitem:" << anchor;
  if (inheritId)
  {
    t << " inherit " << inheritId;
  }
  t << "\">";
  switch(annoType)
  {
    case 0:  t << "<td class=\"memItemLeft\" align=\"right\" valign=\"top\">"; break;
    case 1:  t << "<td class=\"memItemLeft\" >"; break;
    case 2:  t << "<td class=\"memItemLeft\" valign=\"top\">"; break;
    default: t << "<td class=\"memTemplParams\" colspan=\"2\">"; break;
  }
}

void HtmlGenerator::endMemberItem() 
{ 
  t << "</td></tr>"; 
  t << endl; 
}

void HtmlGenerator::startMemberTemplateParams()
{
}

void HtmlGenerator::endMemberTemplateParams(const char *anchor,const char *inheritId)
{
  t << "</td></tr>" << endl;
  t << "<tr class=\"memitem:" << anchor;
  if (inheritId)
  {
    t << " inherit " << inheritId;
  }
  t << "\"><td class=\"memTemplItemLeft\" align=\"right\" valign=\"top\">";
}


void HtmlGenerator::insertMemberAlign(bool templ) 
{ 
  DBG_HTML(t << "<!-- insertMemberAlign -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    QCString className = templ ? "memTemplItemRight" : "memItemRight";
    t << "&#160;</td><td class=\"" << className << "\" valign=\"bottom\">"; 
  //}
}

void HtmlGenerator::startMemberDescription(const char *anchor,const char *inheritId) 
{ 
  DBG_HTML(t << "<!-- startMemberDescription -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    if (m_emptySection)
    {
      t << "<table class=\"memberdecls\">" << endl;
      m_emptySection=FALSE;
    }
    t << "<tr class=\"memdesc:" << anchor;
    if (inheritId)
    {
      t << " inherit " << inheritId;
    }
    t << "\"><td class=\"mdescLeft\">&#160;</td><td class=\"mdescRight\">"; 
  //}
  //else
  //{
  //  t << "<dl class=\"el\"><dd class=\"mdescRight\">";
  //}
}

void HtmlGenerator::endMemberDescription()   
{ 
  DBG_HTML(t << "<!-- endMemberDescription -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    t << "<br/></td></tr>" << endl; 
  //}
  //else
  //{
  //  t << "<br/></dl>";
  //}
}

void HtmlGenerator::startMemberSections()
{
  DBG_HTML(t << "<!-- startMemberSections -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    m_emptySection=TRUE; // we postpone writing <table> until we actually
                         // write a row to prevent empty tables, which 
                         // are not valid XHTML!
  //}
}

void HtmlGenerator::endMemberSections()
{
  DBG_HTML(t << "<!-- endMemberSections -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    if (!m_emptySection)
    {
      t << "</table>" << endl;
    }
  //}
}

void HtmlGenerator::startMemberHeader(const char *anchor)
{
  DBG_HTML(t << "<!-- startMemberHeader -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    if (!m_emptySection)
    {
      t << "</table>";
      m_emptySection=TRUE;
    }
    if (m_emptySection)
    {
      t << "<table class=\"memberdecls\">" << endl;
      m_emptySection=FALSE;
    }
    t << "<tr class=\"heading\"><td colspan=\"2\"><h2 class=\"groupheader\">";
  //}
  //else
  //{
  //  startGroupHeader(FALSE);
  //}
  if (anchor)
  {
    t << "<a name=\"" << anchor << "\"></a>" << endl;
  }
}

void HtmlGenerator::endMemberHeader()
{
  DBG_HTML(t << "<!-- endMemberHeader -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS"))
  //{
    t << "</h2></td></tr>" << endl;
  //}
  //else
  //{
  //  endGroupHeader(FALSE);
  //}
}

void HtmlGenerator::startMemberSubtitle()
{
  DBG_HTML(t << "<!-- startMemberSubtitle -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS")) 
    t << "<tr><td class=\"ititle\" colspan=\"2\">";
}

void HtmlGenerator::endMemberSubtitle()
{
  DBG_HTML(t << "<!-- endMemberSubtitle -->" << endl)
  //if (Config_getBool("HTML_ALIGN_MEMBERS")) 
    t << "</td></tr>" << endl;
}

void HtmlGenerator::startIndexList() 
{ 
  t << "<table>"  << endl; 
}

void HtmlGenerator::endIndexList()
{
  t << "</table>" << endl;
}

void HtmlGenerator::startIndexKey() 
{ 
  // inserted 'class = ...', 02 jan 2002, jh
  t << "  <tr><td class=\"indexkey\">"; 
}

void HtmlGenerator::endIndexKey()
{
  t << "</td>";
}

void HtmlGenerator::startIndexValue(bool) 
{ 
  // inserted 'class = ...', 02 jan 2002, jh
  t << "<td class=\"indexvalue\">"; 
}

void HtmlGenerator::endIndexValue(const char *,bool)
{
  t << "</td></tr>" << endl;
}

void HtmlGenerator::startMemberDocList()
{
  DBG_HTML(t << "<!-- startMemberDocList -->" << endl;)
}

void HtmlGenerator::endMemberDocList()
{
  DBG_HTML(t << "<!-- endMemberDocList -->" << endl;)
}

void HtmlGenerator::startMemberDoc(const char *,const char *,const char *,const char *,bool) 
{ 
  DBG_HTML(t << "<!-- startMemberDoc -->" << endl;)
 
  t << "\n<div class=\"memitem\">" << endl;
  t << "<div class=\"memproto\">" << endl;
}

void HtmlGenerator::startMemberDocPrefixItem()
{
  DBG_HTML(t << "<!-- startMemberDocPrefixItem -->" << endl;)
  t << "<div class=\"memtemplate\">" << endl;
}

void HtmlGenerator::endMemberDocPrefixItem()
{
  DBG_HTML(t << "<!-- endMemberDocPrefixItem -->" << endl;)
  t << "</div>" << endl;
}

void HtmlGenerator::startMemberDocName(bool /*align*/)
{
  DBG_HTML(t << "<!-- startMemberDocName -->" << endl;)

  t << "      <table class=\"memname\">" << endl;
    
  t << "        <tr>" << endl;
  t << "          <td class=\"memname\">";
}

void HtmlGenerator::endMemberDocName()
{
  DBG_HTML(t << "<!-- endMemberDocName -->" << endl;)
  t << "</td>" << endl;
}

void HtmlGenerator::startParameterList(bool openBracket)
{
  DBG_HTML(t << "<!-- startParameterList -->" << endl;)
  t << "          <td>";
  if (openBracket) t << "(";
  t << "</td>" << endl;
}

void HtmlGenerator::startParameterType(bool first,const char *key)
{
  if (first)
  {
    DBG_HTML(t << "<!-- startFirstParameterType -->" << endl;)
    t << "          <td class=\"paramtype\">";
  }
  else
  {
    DBG_HTML(t << "<!-- startParameterType -->" << endl;)
    t << "        <tr>" << endl;
    t << "          <td class=\"paramkey\">";
    if (key) t << key;
    t << "</td>" << endl;
    t << "          <td></td>" << endl;
    t << "          <td class=\"paramtype\">";
  }
}

void HtmlGenerator::endParameterType()
{
  DBG_HTML(t << "<!-- endParameterType -->" << endl;)
  t << "&#160;</td>" << endl;
}

void HtmlGenerator::startParameterName(bool /*oneArgOnly*/)
{
  DBG_HTML(t << "<!-- startParameterName -->" << endl;)
  t << "          <td class=\"paramname\">";
}

void HtmlGenerator::endParameterName(bool last,bool emptyList,bool closeBracket)
{
  DBG_HTML(t << "<!-- endParameterName -->" << endl;)
  if (last)
  {
    if (emptyList)
    {
      if (closeBracket) t << "</td><td>)";
      t << "</td>" << endl;
      t << "          <td>";
    }
    else
    {
      t << "&#160;</td>" << endl;
      t << "        </tr>" << endl;
      t << "        <tr>" << endl;
      t << "          <td></td>" << endl;
      t << "          <td>";
      if (closeBracket) t << ")";
      t << "</td>" << endl;
      t << "          <td></td><td>";
    }
  }
  else
  {
    t << "</td>" << endl;
    t << "        </tr>" << endl;
  }
}

void HtmlGenerator::endParameterList()
{
  DBG_HTML(t << "<!-- endParameterList -->" << endl;)
  t << "</td>" << endl;
  t << "        </tr>" << endl;
}

void HtmlGenerator::endMemberDoc(bool hasArgs)     
{ 
  DBG_HTML(t << "<!-- endMemberDoc -->" << endl;)
  if (!hasArgs)
  {
    t << "        </tr>" << endl;
  }
  t << "      </table>" << endl;
 // t << "</div>" << endl;
}

void HtmlGenerator::startDotGraph()
{
  startSectionHeader(t,relPath,m_sectionCount);
}

void HtmlGenerator::endDotGraph(const DotClassGraph &g)
{
  bool generateLegend = Config_getBool("GENERATE_LEGEND");
  bool umlLook = Config_getBool("UML_LOOK");
  endSectionHeader(t);
  startSectionSummary(t,m_sectionCount);
  endSectionSummary(t);
  startSectionContent(t,m_sectionCount);

  g.writeGraph(t,BITMAP,dir,fileName,relPath,TRUE,TRUE,m_sectionCount);
  if (generateLegend && !umlLook)
  {
    t << "<center><span class=\"legend\">[";
    startHtmlLink(relPath+"graph_legend"+Doxygen::htmlFileExtension);
    t << theTranslator->trLegend();
    endHtmlLink();
    t << "]</span></center>";
  }

  endSectionContent(t);
  m_sectionCount++;
}

void HtmlGenerator::startInclDepGraph()
{
  startSectionHeader(t,relPath,m_sectionCount);
}

void HtmlGenerator::endInclDepGraph(const DotInclDepGraph &g)
{
  endSectionHeader(t);
  startSectionSummary(t,m_sectionCount);
  endSectionSummary(t);
  startSectionContent(t,m_sectionCount);

  g.writeGraph(t,BITMAP,dir,fileName,relPath,TRUE,m_sectionCount);

  endSectionContent(t);
  m_sectionCount++;
}

void HtmlGenerator::startGroupCollaboration()
{
  startSectionHeader(t,relPath,m_sectionCount);
}

void HtmlGenerator::endGroupCollaboration(const DotGroupCollaboration &g)
{
  endSectionHeader(t);
  startSectionSummary(t,m_sectionCount);
  endSectionSummary(t);
  startSectionContent(t,m_sectionCount);

  g.writeGraph(t,BITMAP,dir,fileName,relPath,TRUE,m_sectionCount);

  endSectionContent(t);
  m_sectionCount++;
}

void HtmlGenerator::startCallGraph()
{
  startSectionHeader(t,relPath,m_sectionCount);
}

void HtmlGenerator::endCallGraph(const DotCallGraph &g)
{
  endSectionHeader(t);
  startSectionSummary(t,m_sectionCount);
  endSectionSummary(t);
  startSectionContent(t,m_sectionCount);

  g.writeGraph(t,BITMAP,dir,fileName,relPath,TRUE,m_sectionCount);

  endSectionContent(t);
  m_sectionCount++;
}

void HtmlGenerator::startDirDepGraph()
{
  startSectionHeader(t,relPath,m_sectionCount);
}

void HtmlGenerator::endDirDepGraph(const DotDirDeps &g)
{
  endSectionHeader(t);
  startSectionSummary(t,m_sectionCount);
  endSectionSummary(t);
  startSectionContent(t,m_sectionCount);

  g.writeGraph(t,BITMAP,dir,fileName,relPath,TRUE,m_sectionCount);

  endSectionContent(t);
  m_sectionCount++;
}

void HtmlGenerator::writeGraphicalHierarchy(const DotGfxHierarchyTable &g)
{
  g.writeGraph(t,dir,fileName);
}

void HtmlGenerator::startMemberGroupHeader(bool)
{
  t << "<tr><td colspan=\"2\"><div class=\"groupHeader\">";
}

void HtmlGenerator::endMemberGroupHeader()
{
  t << "</div></td></tr>" << endl;
}

void HtmlGenerator::startMemberGroupDocs()
{
  t << "<tr><td colspan=\"2\"><div class=\"groupText\">";
}

void HtmlGenerator::endMemberGroupDocs()
{
  t << "</div></td></tr>" << endl;
}

void HtmlGenerator::startMemberGroup()
{
}

void HtmlGenerator::endMemberGroup(bool)
{
}

void HtmlGenerator::startIndent()        
{ 
  DBG_HTML(t << "<!-- startIndent -->" << endl;)

  t << "<div class=\"memdoc\">\n";
}

void HtmlGenerator::endIndent()          
{ 
  DBG_HTML(t << "<!-- endIndent -->" << endl;)
  t << endl << "</div>" << endl << "</div>" << endl; 
}

void HtmlGenerator::addIndexItem(const char *,const char *)
{
}

void HtmlGenerator::writeNonBreakableSpace(int n)
{
  int i;
  for (i=0;i<n;i++)
  {
    t << "&#160;";
  }
}

void HtmlGenerator::startSimpleSect(SectionTypes,
                                const char *filename,const char *anchor,
                                const char *title)
{
  t << "<dl><dt><b>";
  if (filename)
  {
    writeObjectLink(0,filename,anchor,title);
  }
  else
  {
    docify(title);
  }
  t << "</b></dt>";
}

void HtmlGenerator::endSimpleSect()
{
  t << "</dl>"; 
}

void HtmlGenerator::startParamList(ParamListTypes,
                                const char *title)
{
  t << "<dl><dt><b>";
  docify(title);
  t << "</b></dt>";
}

void HtmlGenerator::endParamList()
{
  t << "</dl>"; 
}

void HtmlGenerator::writeDoc(DocNode *n,Definition *ctx,MemberDef *md)
{
  HtmlDocVisitor *visitor = new HtmlDocVisitor(t,m_codeGen,ctx,md);
  n->accept(visitor);
  delete visitor; 
}

//---------------- helpers for index generation -----------------------------

static void startQuickIndexList(FTextStream &t,bool compact,bool topLevel=TRUE)
{
  if (compact) 
  {
    if (topLevel)
    {
      t << "  <div id=\"navrow1\" class=\"tabs\">\n";
    }
    else
    {
      t << "  <div id=\"navrow2\" class=\"tabs2\">\n";
    }
    t << "    <ul class=\"tablist\">\n"; 
  }
  else 
  {
    t << "<ul>";
  }
}

static void endQuickIndexList(FTextStream &t,bool compact)
{
  if (compact) 
  {
    t << "    </ul>\n";
    t << "  </div>\n";
  }
  else 
  {
    t << "</ul>\n";
  }
}

static void startQuickIndexItem(FTextStream &t,const char *l,
                                bool hl,bool /*compact*/,
                                const QCString &relPath)
{
  t << "      <li"; 
  if (hl) 
  {
    t << " class=\"current\"";
  }
  t << ">";
  if (l) t << "<a href=\"" << correctURL(l,relPath) << "\">";
  t << "<span>";
}

static void endQuickIndexItem(FTextStream &t,const char *l)
{
  t << "</span>";
  if (l) t << "</a>";
  t << "</li>\n";
}

static QCString fixSpaces(const QCString &s)
{
  return substitute(s," ","&#160;");
}

static bool quickLinkVisible(LayoutNavEntry::Kind kind)
{
  static bool showFiles = Config_getBool("SHOW_FILES");
  static bool showNamespaces = Config_getBool("SHOW_NAMESPACES");
  switch (kind)
  {
    case LayoutNavEntry::MainPage:         return TRUE; 
    case LayoutNavEntry::User:             return TRUE;                                           
    case LayoutNavEntry::UserGroup:        return TRUE;                                           
    case LayoutNavEntry::Pages:            return indexedPages>0;
    case LayoutNavEntry::Modules:          return documentedGroups>0;
    case LayoutNavEntry::Namespaces:       return documentedNamespaces>0 && showNamespaces;
    case LayoutNavEntry::NamespaceList:    return documentedNamespaces>0 && showNamespaces;
    case LayoutNavEntry::NamespaceMembers: return documentedNamespaceMembers[NMHL_All]>0;
    case LayoutNavEntry::Classes:          return annotatedClasses>0;
    case LayoutNavEntry::ClassList:        return annotatedClasses>0; 
    case LayoutNavEntry::ClassIndex:       return annotatedClasses>0; 
    case LayoutNavEntry::ClassHierarchy:   return hierarchyClasses>0;
    case LayoutNavEntry::ClassMembers:     return documentedClassMembers[CMHL_All]>0;
    case LayoutNavEntry::Files:            return documentedHtmlFiles>0 && showFiles;
    case LayoutNavEntry::FileList:         return documentedHtmlFiles>0 && showFiles;
    case LayoutNavEntry::FileGlobals:      return documentedFileMembers[FMHL_All]>0;
    //case LayoutNavEntry::Dirs:             return documentedDirs>0;
    case LayoutNavEntry::Examples:         return Doxygen::exampleSDict->count()>0;
  }
  return FALSE;
}

static void renderQuickLinksAsTree(FTextStream &t,const QCString &relPath,LayoutNavEntry *root)

{
  QListIterator<LayoutNavEntry> li(root->children());
  LayoutNavEntry *entry;
  int count=0;
  for (li.toFirst();(entry=li.current());++li)
  {
    if (entry->visible() && quickLinkVisible(entry->kind())) count++;
  }
  if (count>0) // at least one item is visible
  {
    startQuickIndexList(t,FALSE);
    for (li.toFirst();(entry=li.current());++li)
    {
      if (entry->visible() && quickLinkVisible(entry->kind()))
      {
        QCString url = entry->url();
        t << "<li><a href=\"" << relPath << url << "\"><span>";
        t << fixSpaces(entry->title());
        t << "</span></a>\n";
        // recursive into child list
        renderQuickLinksAsTree(t,relPath,entry);
        t << "</li>";
      }
    }
    endQuickIndexList(t,FALSE);
  }
}


static void renderQuickLinksAsTabs(FTextStream &t,const QCString &relPath,
                             LayoutNavEntry *hlEntry,LayoutNavEntry::Kind kind,
                             bool highlightParent,bool highlightSearch)
{
  if (hlEntry->parent()) // first draw the tabs for the parent of hlEntry
  {
    renderQuickLinksAsTabs(t,relPath,hlEntry->parent(),kind,highlightParent,highlightSearch);
  }
  if (hlEntry->parent() && hlEntry->parent()->children().count()>0) // draw tabs for row containing hlEntry
  {
    bool topLevel = hlEntry->parent()->parent()==0;
    QListIterator<LayoutNavEntry> li(hlEntry->parent()->children());
    LayoutNavEntry *entry;

    int count=0;
    for (li.toFirst();(entry=li.current());++li)
    {
      if (entry->visible() && quickLinkVisible(entry->kind())) count++;
    }
    if (count>0) // at least one item is visible
    {
      startQuickIndexList(t,TRUE,topLevel);
      for (li.toFirst();(entry=li.current());++li)
      {
        if (entry->visible() && quickLinkVisible(entry->kind()))
        {
          QCString url = entry->url();
          startQuickIndexItem(t,url,
              entry==hlEntry  && 
              (entry->children().count()>0 || 
               (entry->kind()==kind && !highlightParent)
              ),
              TRUE,relPath);
          t << fixSpaces(entry->title());
          endQuickIndexItem(t,url);
        }
      }
      if (hlEntry->parent()==LayoutDocManager::instance().rootNavEntry()) // first row is special as it contains the search box
      {
        static bool searchEngine      = Config_getBool("SEARCHENGINE");
        static bool serverBasedSearch = Config_getBool("SERVER_BASED_SEARCH");
        if (searchEngine)
        {
          t << "      <li>\n";
          if (!serverBasedSearch) // pure client side search
          {
            writeClientSearchBox(t,relPath);
            t << "      </li>\n";
          }
          else // server based search
          {
            writeServerSearchBox(t,relPath,highlightSearch);
            if (!highlightSearch)
            {
              t << "      </li>\n";
            }
          }
        }
        if (!highlightSearch) // on the search page the index will be ended by the
          // page itself
        {
          endQuickIndexList(t,TRUE);
        }
      }
      else // normal case for other rows than first one
      {
        endQuickIndexList(t,TRUE);
      }
    }
  }
}

static void writeDefaultQuickLinks(FTextStream &t,bool compact,
                                   HighlightedItem hli,
                                   const char *file,
                                   const QCString &relPath)
{
  LayoutNavEntry *root = LayoutDocManager::instance().rootNavEntry();
  LayoutNavEntry::Kind kind = (LayoutNavEntry::Kind)-1;
  LayoutNavEntry::Kind altKind = (LayoutNavEntry::Kind)-1; // fall back for the old layout file
  bool highlightParent=FALSE;
  switch (hli) // map HLI enums to LayoutNavEntry::Kind enums
  {
    case HLI_Main:             kind = LayoutNavEntry::MainPage;         break;
    case HLI_Modules:          kind = LayoutNavEntry::Modules;          break;
    //case HLI_Directories:      kind = LayoutNavEntry::Dirs;             break;
    case HLI_Namespaces:       kind = LayoutNavEntry::NamespaceList;    altKind = LayoutNavEntry::Namespaces;  break;
    case HLI_Hierarchy:        kind = LayoutNavEntry::ClassHierarchy;   break;
    case HLI_Classes:          kind = LayoutNavEntry::ClassIndex;       altKind = LayoutNavEntry::Classes;     break;
    case HLI_Annotated:        kind = LayoutNavEntry::ClassList;        altKind = LayoutNavEntry::Classes;     break;
    case HLI_Files:            kind = LayoutNavEntry::FileList;         altKind = LayoutNavEntry::Files;       break;
    case HLI_NamespaceMembers: kind = LayoutNavEntry::NamespaceMembers; break;
    case HLI_Functions:        kind = LayoutNavEntry::ClassMembers;     break;
    case HLI_Globals:          kind = LayoutNavEntry::FileGlobals;      break;
    case HLI_Pages:            kind = LayoutNavEntry::Pages;            break;
    case HLI_Examples:         kind = LayoutNavEntry::Examples;         break;
    case HLI_UserGroup:        kind = LayoutNavEntry::UserGroup;        break;
    case HLI_ClassVisible:     kind = LayoutNavEntry::ClassList;        altKind = LayoutNavEntry::Classes;          
                               highlightParent = TRUE; break;
    case HLI_NamespaceVisible: kind = LayoutNavEntry::NamespaceList;    altKind = LayoutNavEntry::Namespaces;       
                               highlightParent = TRUE; break;
    case HLI_FileVisible:      kind = LayoutNavEntry::FileList;         altKind = LayoutNavEntry::Files;            
                               highlightParent = TRUE; break;
    case HLI_None:   break;
    case HLI_Search: break;
  }
  
  if (compact)
  {
    // find highlighted index item
    LayoutNavEntry *hlEntry = root->find(kind,kind==LayoutNavEntry::UserGroup ? file : 0);
    if (!hlEntry && altKind!=(LayoutNavEntry::Kind)-1) { hlEntry=root->find(altKind); kind=altKind; }
    if (!hlEntry) // highlighted item not found in the index! -> just show the level 1 index...
    {
      highlightParent=TRUE;
      hlEntry = root->children().getFirst();
      if (hlEntry==0) 
      {
        return; // argl, empty index!
      }
    }
    if (kind==LayoutNavEntry::UserGroup)
    {
      LayoutNavEntry *e = hlEntry->children().getFirst();
      if (e)
      {
        hlEntry = e; 
      }
    }
    renderQuickLinksAsTabs(t,relPath,hlEntry,kind,highlightParent,hli==HLI_Search);
  }
  else
  {
    renderQuickLinksAsTree(t,relPath,root);
  }
}

void HtmlGenerator::endQuickIndices()
{
  t << "</div><!-- top -->" << endl;
}

QCString HtmlGenerator::writeSplitBarAsString(const char *name,const char *relpath)
{
  static bool generateTreeView = Config_getBool("GENERATE_TREEVIEW");
  QCString result;
  // write split bar
  if (generateTreeView)
  {
    result = QCString(
    "<div id=\"side-nav\" class=\"ui-resizable side-nav-resizable\">\n"
    "  <div id=\"nav-tree\">\n"
    "    <div id=\"nav-tree-contents\">\n"
    "      <div id=\"nav-sync\" class=\"sync\"></div>\n"
    "    </div>\n"
    "  </div>\n"
    "  <div id=\"splitbar\" style=\"-moz-user-select:none;\" \n"
    "       class=\"ui-resizable-handle\">\n"
    "  </div>\n"
    "</div>\n"
    "<script type=\"text/javascript\">\n"
    "$(document).ready(function(){initNavTree('") + 
    QCString(name) + Doxygen::htmlFileExtension + 
    QCString("','") + relpath +
    QCString("');});\n"
    "</script>\n"
    "<div id=\"doc-content\">\n");
  }
  return result;
}

void HtmlGenerator::writeSplitBar(const char *name)
{
  t << writeSplitBarAsString(name,relPath);
}

void HtmlGenerator::writeNavigationPath(const char *s)
{
  t << substitute(s,"$relpath^",relPath);
}

void HtmlGenerator::startContents()
{
  t << "<div class=\"contents\">" << endl;
}

void HtmlGenerator::endContents()
{
  t << "</div><!-- contents -->" << endl;
}

void HtmlGenerator::writeQuickLinks(bool compact,HighlightedItem hli,const char *file)
{
  writeDefaultQuickLinks(t,compact,hli,file,relPath);
}

// PHP based search script
void HtmlGenerator::writeSearchPage()
{
  static bool generateTreeView = Config_getBool("GENERATE_TREEVIEW");
  static bool disableIndex = Config_getBool("DISABLE_INDEX");
  static QCString projectName = Config_getString("PROJECT_NAME");

  // OPENSEARCH_PROVIDER {
  QCString configFileName = Config_getString("HTML_OUTPUT")+"/search-config.php";
  QFile cf(configFileName);
  if (cf.open(IO_WriteOnly))
  {
    FTextStream t(&cf);
    t << "<script language=\"php\">\n\n";
    t << "$config = array(\n";
    t << "  'PROJECT_NAME' => \"" << projectName << "\",\n";
    t << "  'GENERATE_TREEVIEW' => " << (generateTreeView?"true":"false") << ",\n";
    t << "  'DISABLE_INDEX' => " << (disableIndex?"true":"false") << ",\n";
    t << ");\n\n";
    t << "$translator = array(\n";
    t << "  'search_results_title' => \"" << theTranslator->trSearchResultsTitle() << "\",\n";
    t << "  'search_results' => array(\n";  
    t << "    0 => \"" << theTranslator->trSearchResults(0) << "\",\n";     
    t << "    1 => \"" << theTranslator->trSearchResults(1) << "\",\n";     
    t << "    2 => \"" << substitute(theTranslator->trSearchResults(2), "$", "\\$") << "\",\n";     
    t << "  ),\n";
    t << "  'search_matches' => \"" << theTranslator->trSearchMatches() << "\",\n";
    t << "  'search' => \"" << theTranslator->trSearch() << "\",\n";
    t << "  'split_bar' => \"" << substitute(substitute(writeSplitBarAsString("search",""), "\"","\\\""), "\n","\\n") << "\",\n";
    t << "  'logo' => \"" << substitute(substitute(writeLogoAsString(""), "\"","\\\""), "\n","\\n") << "\",\n";
    t << ");\n\n";
    t << "</script>\n";
  }

  QCString functionsFileName = Config_getString("HTML_OUTPUT")+"/search-functions.php";
  QFile ff(functionsFileName);
  if (ff.open(IO_WriteOnly))
  {
    FTextStream t(&ff);
    // Write stuff from search_functions.php source file...
    t << search_functions_script;
  }

  QCString opensearchFileName = Config_getString("HTML_OUTPUT")+"/search-opensearch.php";
  QFile of(opensearchFileName);
  if (of.open(IO_WriteOnly))
  {
    FTextStream t(&of);
    // Write stuff from search_opensearch.php source file...
    t << search_opensearch_script;
  }
  // OPENSEARCH_PROVIDER }

  QCString fileName = Config_getString("HTML_OUTPUT")+"/search.php";
  QFile f(fileName);
  if (f.open(IO_WriteOnly))
  {
    FTextStream t(&f);
    t << substituteHtmlKeywords(g_header,"Search","");

    t << "<!-- " << theTranslator->trGeneratedBy() << " Doxygen " 
      << versionString << " -->" << endl;
    t << "<script type=\"text/javascript\">\n";
    t << "var searchBox = new SearchBox(\"searchBox\", \""
      << "search\",false,'" << theTranslator->trSearch() << "');\n";
    t << "</script>\n";
    if (!Config_getBool("DISABLE_INDEX"))
    {
      writeDefaultQuickLinks(t,TRUE,HLI_Search,0,"");
    }
    else
    {
      t << "</div>" << endl;
    }

    t << "<script language=\"php\">\n";
    t << "require_once \"search-functions.php\";\n";
    t << "main();\n";
    t << "</script>\n";

    // Write empty navigation path, to make footer connect properly
    if (generateTreeView)
    {
      t << "</div><!-- doc-contents -->\n";
      //t << "<div id=\"nav-path\" class=\"navpath\">\n";
      //t << "  <ul>\n";
    }

    writePageFooter(t,"Search","","");
  }
  QCString scriptName = Config_getString("HTML_OUTPUT")+"/search/search.js";
  QFile sf(scriptName);
  if (sf.open(IO_WriteOnly))
  {
    FTextStream t(&sf);
    t << extsearch_script;
  }
  else
  {
     err("Failed to open file '%s' for writing...\n",scriptName.data());
  }
}

void HtmlGenerator::writeExternalSearchPage()
{
  static bool generateTreeView = Config_getBool("GENERATE_TREEVIEW");
  QCString fileName = Config_getString("HTML_OUTPUT")+"/search"+Doxygen::htmlFileExtension;
  QFile f(fileName);
  if (f.open(IO_WriteOnly))
  {
    FTextStream t(&f);
    t << substituteHtmlKeywords(g_header,"Search","");

    t << "<!-- " << theTranslator->trGeneratedBy() << " Doxygen " 
      << versionString << " -->" << endl;
    t << "<script type=\"text/javascript\">\n";
    t << "var searchBox = new SearchBox(\"searchBox\", \""
      << "search\",false,'" << theTranslator->trSearch() << "');\n";
    t << "</script>\n";
    if (!Config_getBool("DISABLE_INDEX"))
    {
      writeDefaultQuickLinks(t,TRUE,HLI_Search,0,"");
      t << "            <input type=\"text\" id=\"MSearchField\" name=\"query\" value=\"\" size=\"20\" accesskey=\"S\" onfocus=\"searchBox.OnSearchFieldFocus(true)\" onblur=\"searchBox.OnSearchFieldFocus(false)\"/>\n";
      t << "            </form>\n";
      t << "          </div><div class=\"right\"></div>\n";
      t << "        </div>\n";
      t << "      </li>\n";
      t << "    </ul>\n";
      t << "  </div>\n";
      t << "</div>\n";
    }
    else
    {
      t << "</div>" << endl;
    }
    t << writeSplitBarAsString("search","");
    t << "<div class=\"header\">" << endl;
    t << "  <div class=\"headertitle\">" << endl;
    t << "    <div class=\"title\">" << theTranslator->trSearchResultsTitle() << "</div>" << endl;
    t << "  </div>" << endl;
    t << "</div>" << endl;
    t << "<div class=\"contents\">" << endl;

    t << "<div id=\"searchresults\"></div>" << endl;
    t << "</div>" << endl;

    if (generateTreeView)
    {
      t << "</div><!-- doc-contents -->" << endl;
    }

    writePageFooter(t,"Search","","");
  }
  QCString scriptName = Config_getString("HTML_OUTPUT")+"/search/search.js";
  QFile sf(scriptName);
  if (sf.open(IO_WriteOnly))
  {
    FTextStream t(&sf);
    t << "var searchResultsText=["
      << "\"" << theTranslator->trSearchResults(0) << "\","
      << "\"" << theTranslator->trSearchResults(1) << "\","
      << "\"" << theTranslator->trSearchResults(2) << "\"];" << endl;
    t << "var serverUrl=\"" << Config_getString("SEARCHENGINE_URL") << "\";" << endl;
    t << "var tagMap = {" << endl;
    bool first=TRUE;
    // add search mappings
    QStrList &extraSearchMappings = Config_getList("EXTRA_SEARCH_MAPPINGS");
    char *ml=extraSearchMappings.first();
    while (ml)
    {
      QCString mapLine = ml;
      int eqPos = mapLine.find('=');
      if (eqPos!=-1) // tag command contains a destination
      {
        QCString tagName = mapLine.left(eqPos).stripWhiteSpace();
        QCString destName = mapLine.right(mapLine.length()-eqPos-1).stripWhiteSpace();
        if (!tagName.isEmpty())
        {
          if (!first) t << "," << endl;
          t << "  \"" << tagName << "\": \"" << destName << "\"";
          first=FALSE;
        }
      }
      ml=extraSearchMappings.next();
    }
    if (!first) t << endl;
    t << "};" << endl << endl;
    t << extsearch_script;
    t << endl;
    t << "$(document).ready(function() {" << endl;
    t << "  var query = trim(getURLParameter('query'));" << endl;
    t << "  if (query) {" << endl;
    t << "    searchFor(query,0,20);" << endl;
    t << "  } else {" << endl;
    t << "    var results = $('#results');" << endl;
    t << "    results.html('<p>" << theTranslator->trSearchResults(0) << "</p>');" << endl;
    t << "  }" << endl;
    t << "});" << endl;
  }
  else
  {
     err("Failed to open file '%s' for writing...\n",scriptName.data());
  }
}

void HtmlGenerator::startConstraintList(const char *header)
{
  t << "<div class=\"typeconstraint\">" << endl;
  t << "<dl><dt><b>" << header << "</b><dt><dd>" << endl;
  t << "<table border=\"0\" cellspacing=\"2\" cellpadding=\"0\">" << endl;
}

void HtmlGenerator::startConstraintParam()
{
  t << "<tr><td valign=\"top\"><em>";
}

void HtmlGenerator::endConstraintParam()
{
  t << "</em></td>";
}

void HtmlGenerator::startConstraintType()
{
  t << "<td>&#160;:</td><td valign=\"top\"><em>";
}

void HtmlGenerator::endConstraintType()
{
  t << "</em></td>";
}

void HtmlGenerator::startConstraintDocs()
{
  t << "<td>&#160;";
}

void HtmlGenerator::endConstraintDocs()
{
  t << "</td></tr>" << endl;
}

void HtmlGenerator::endConstraintList()
{
  t << "</table>" << endl;
  t << "</dl>" << endl;
  t << "</div>" << endl;
}

void HtmlGenerator::lineBreak(const char *style)
{
  if (style)
  {
    t << "<br class=\"" << style << "\"/>" << endl;
  }
  else
  {
    t << "<br/>" << endl;
  }
}

void HtmlGenerator::startHeaderSection()
{
  t << "<div class=\"header\">" << endl;
}

void HtmlGenerator::startTitleHead(const char *) 
{ 
  t << "  <div class=\"headertitle\">" << endl;
  startTitle(); 
}

void HtmlGenerator::endTitleHead(const char *,const char *) 
{ 
  endTitle(); 
  t << "  </div>" << endl;
}

void HtmlGenerator::endHeaderSection()
{
  t << "</div><!--header-->" << endl;
}

void HtmlGenerator::startInlineHeader()
{
  if (m_emptySection)
  {
    t << "<table class=\"memberdecls\">" << endl;
    m_emptySection=FALSE;
  }
  t << "<tr><td colspan=\"2\"><h3>";
}

void HtmlGenerator::endInlineHeader()
{
  t << "</h3></td></tr>" << endl;
}

void HtmlGenerator::startMemberDocSimple()
{
  DBG_HTML(t << "<!-- startMemberDocSimple -->" << endl;)
  t << "<table class=\"fieldtable\">" << endl;
  t << "<tr><th colspan=\"3\">" << theTranslator->trCompoundMembers() << "</th></tr>" << endl;
}

void HtmlGenerator::endMemberDocSimple()
{
  DBG_HTML(t << "<!-- endMemberDocSimple -->" << endl;)
  t << "</table>" << endl;
}

void HtmlGenerator::startInlineMemberType()
{
  DBG_HTML(t << "<!-- startInlineMemberType -->" << endl;)
  t << "<tr><td class=\"fieldtype\">" << endl;
}

void HtmlGenerator::endInlineMemberType()
{
  DBG_HTML(t << "<!-- endInlineMemberType -->" << endl;)
  t << "</td>" << endl;
}

void HtmlGenerator::startInlineMemberName()
{
  DBG_HTML(t << "<!-- startInlineMemberName -->" << endl;)
  t << "<td class=\"fieldname\">" << endl;
}

void HtmlGenerator::endInlineMemberName()
{
  DBG_HTML(t << "<!-- endInlineMemberName -->" << endl;)
  t << "</td>" << endl;
}

void HtmlGenerator::startInlineMemberDoc()
{
  DBG_HTML(t << "<!-- startInlineMemberDoc -->" << endl;)
  t << "<td class=\"fielddoc\">" << endl;
}

void HtmlGenerator::endInlineMemberDoc()
{
  DBG_HTML(t << "<!-- endInlineMemberDoc -->" << endl;)
  t << "</td></tr>" << endl;
}

void HtmlGenerator::startLabels()
{
  DBG_HTML(t << "<!-- startLabels -->" << endl;)
  t << "<span class=\"mlabels\">";
}

void HtmlGenerator::writeLabel(const char *l,bool /*isLast*/)
{
  DBG_HTML(t << "<!-- writeLabel(" << l << ") -->" << endl;)
  //t << "<tt>[" << l << "]</tt>";
  //if (!isLast) t << ", ";
  t << "<span class=\"mlabel\">" << l << "</span>";
}

void HtmlGenerator::endLabels()
{
  DBG_HTML(t << "<!-- endLabels -->" << endl;)
  t << "</span>";
}

void HtmlGenerator::writeInheritedSectionTitle(
                  const char *id,    const char *ref, 
                  const char *file,  const char *anchor,
                  const char *title, const char *name)
{
  DBG_HTML(t << "<!-- writeInheritedSectionTitle -->" << endl;)
  QCString a = anchor;
  if (!a.isEmpty()) a.prepend("#");
  QCString classLink = QCString("<a class=\"el\" href=\"");
  if (ref)
  {
    classLink+= externalLinkTarget() + externalRef(relPath,ref,TRUE);
  }
  else
  {
    classLink+=relPath;
  }
  classLink+=file+Doxygen::htmlFileExtension+a;
  classLink+=QCString("\">")+convertToHtml(name,FALSE)+"</a>";
  t << "<tr class=\"inherit_header " << id << "\">"
    << "<td colspan=\"2\" onclick=\"javascript:toggleInherit('" << id << "')\">"
    << "<img src=\"" << relPath << "closed.png\" alt=\"-\"/>&#160;" 
    << theTranslator->trInheritedFrom(convertToHtml(title,FALSE),classLink)
    << "</td></tr>" << endl;
}

void HtmlGenerator::writeSummaryLink(const char *file,const char *anchor,const char *title,bool first)
{
  if (first)
  {
    t << "  <div class=\"summary\">\n";
  }
  else
  {
    t << " &#124;\n";
  }
  t << "<a href=\"";
  if (file)
  {
    t << relPath << file;
    t << Doxygen::htmlFileExtension;
  }
  else
  {
    t << "#";
    t << anchor;
  }
  t << "\">";
  t << title;
  t << "</a>";
}

void HtmlGenerator::endMemberDeclaration(const char *anchor,const char *inheritId)
{
  t << "<tr class=\"separator:" << anchor;
  if (inheritId)
  {
    t << " inherit " << inheritId;
  }
  t << "\"><td class=\"memSeparator\" colspan=\"2\">&#160;</td></tr>\n";
}

void HtmlGenerator::setCurrentDoc(Definition *context,const char *anchor,bool isSourceFile)
{
  if (Doxygen::searchIndex)
  {
    Doxygen::searchIndex->setCurrentDoc(context,anchor,isSourceFile);
  }
}

void HtmlGenerator::addWord(const char *word,bool hiPriority)
{
  if (Doxygen::searchIndex)
  {
    Doxygen::searchIndex->addWord(word,hiPriority);
  }
}

