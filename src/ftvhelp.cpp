/******************************************************************************
 * ftvhelp.cpp,v 1.0 2000/09/06 16:09:00
 *
 * Copyright (C) 1997-2013 by Dimitri van Heesch.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation under the terms of the GNU General Public License is hereby 
 * granted. No representations are made about the suitability of this software 
 * for any purpose. It is provided "as is" without express or implied warranty.
 * See the GNU General Public License for more details.
 *
 * Documents produced by Doxygen are derivative works derived from the
 * input used in their production; they are not affected by this license.
 *
 * Original version contributed by Kenney Wong <kwong@ea.com>
 * Modified by Dimitri van Heesch
 *
 * Folder Tree View for offline help on browsers that do not support HTML Help.
 */

#include <stdio.h>
#include <stdlib.h>
#include <qlist.h>
#include <qdict.h>
#include <qfileinfo.h>

#include "ftvhelp.h"
#include "config.h"
#include "message.h"
#include "doxygen.h"
#include "language.h"
#include "htmlgen.h"
#include "layout.h"
#include "pagedef.h"
#include "docparser.h"
#include "htmldocvisitor.h"
#include "filedef.h"
#include "util.h"

#define MAX_INDENT 1024


static const char navtree_script[]=
// #include "navtree_js.h"
"var navTreeSubIndices = new Array();\n"
"\n"
"function getData(varName)\n"
"{\n"
"  var i = varName.lastIndexOf('/');\n"
"  var n = i>=0 ? varName.substring(i+1) : varName;\n"
"  return eval(n.replace(/\\-/g,'_'));\n"
"}\n"
"\n"
"function stripPath(uri)\n"
"{\n"
"  return uri.substring(uri.lastIndexOf('/')+1);\n"
"}\n"
"\n"
"function stripPath2(uri)\n"
"{\n"
"  var i = uri.lastIndexOf('/');\n"
"  var s = uri.substring(i+1);\n"
"  var m = uri.substring(0,i+1).match(/\\/d\\w\\/d\\w\\w\\/$/);\n"
"  return m ? uri.substring(i-6) : s;\n"
"}\n"
"\n"
"function localStorageSupported()\n"
"{\n"
"  try {\n"
"    return 'localStorage' in window && window['localStorage'] !== null && window.localStorage.getItem;\n"
"  }\n"
"  catch(e) {\n"
"    return false;\n"
"  }\n"
"}\n"
"\n"
"\n"
"function storeLink(link)\n"
"{\n"
"  if (!$(\"#nav-sync\").hasClass('sync') && localStorageSupported()) {\n"
"      window.localStorage.setItem('navpath',link);\n"
"  }\n"
"}\n"
"\n"
"function deleteLink()\n"
"{\n"
"  if (localStorageSupported()) {\n"
"    window.localStorage.setItem('navpath','');\n"
"  } \n"
"}\n"
"\n"
"function cachedLink()\n"
"{\n"
"  if (localStorageSupported()) {\n"
"    return window.localStorage.getItem('navpath');\n"
"  } else {\n"
"    return '';\n"
"  }\n"
"}\n"
"\n"
"function getScript(scriptName,func,show)\n"
"{\n"
"  var head = document.getElementsByTagName(\"head\")[0]; \n"
"  var script = document.createElement('script');\n"
"  script.id = scriptName;\n"
"  script.type = 'text/javascript';\n"
"  script.onload = func; \n"
"  script.src = scriptName+'.js'; \n"
"  if ($.browser.msie && $.browser.version<=8) { \n"
"    // script.onload does not work with older versions of IE\n"
"    script.onreadystatechange = function() {\n"
"      if (script.readyState=='complete' || script.readyState=='loaded') { \n"
"        func(); if (show) showRoot(); \n"
"      }\n"
"    }\n"
"  }\n"
"  head.appendChild(script); \n"
"}\n"
"\n"
"function createIndent(o,domNode,node,level)\n"
"{\n"
"  var level=-1;\n"
"  var n = node;\n"
"  while (n.parentNode) { level++; n=n.parentNode; }\n"
"  var imgNode = document.createElement(\"img\");\n"
"  imgNode.style.paddingLeft=(16*level).toString()+'px';\n"
"  imgNode.width  = 16;\n"
"  imgNode.height = 22;\n"
"  imgNode.border = 0;\n"
"  if (node.childrenData) {\n"
"    node.plus_img = imgNode;\n"
"    node.expandToggle = document.createElement(\"a\");\n"
"    node.expandToggle.href = \"javascript:void(0)\";\n"
"    node.expandToggle.onclick = function() {\n"
"      if (node.expanded) {\n"
"        $(node.getChildrenUL()).slideUp(\"fast\");\n"
"        node.plus_img.src = node.relpath+\"ftv2pnode.png\";\n"
"        node.expanded = false;\n"
"      } else {\n"
"        expandNode(o, node, false, false);\n"
"      }\n"
"    }\n"
"    node.expandToggle.appendChild(imgNode);\n"
"    domNode.appendChild(node.expandToggle);\n"
"    imgNode.src = node.relpath+\"ftv2pnode.png\";\n"
"  } else {\n"
"    imgNode.src = node.relpath+\"ftv2node.png\";\n"
"    domNode.appendChild(imgNode);\n"
"  } \n"
"}\n"
"\n"
"var animationInProgress = false;\n"
"\n"
"function gotoAnchor(anchor,aname,updateLocation)\n"
"{\n"
"  var pos, docContent = $('#doc-content');\n"
"  if (anchor.parent().attr('class')=='memItemLeft' ||\n"
"      anchor.parent().attr('class')=='fieldtype' ||\n"
"      anchor.parent().is(':header')) \n"
"  {\n"
"    pos = anchor.parent().position().top;\n"
"  } else if (anchor.position()) {\n"
"    pos = anchor.position().top;\n"
"  }\n"
"  if (pos) {\n"
"    var dist = Math.abs(Math.min(\n"
"               pos-docContent.offset().top,\n"
"               docContent[0].scrollHeight-\n"
"               docContent.height()-docContent.scrollTop()));\n"
"    animationInProgress=true;\n"
"    docContent.animate({\n"
"      scrollTop: pos + docContent.scrollTop() - docContent.offset().top\n"
"    },Math.max(50,Math.min(500,dist)),function(){\n"
"      if (updateLocation) window.location.href=aname;\n"
"      animationInProgress=false;\n"
"    });\n"
"  }\n"
"}\n"
"\n"
"function newNode(o, po, text, link, childrenData, lastNode)\n"
"{\n"
"  var node = new Object();\n"
"  node.children = Array();\n"
"  node.childrenData = childrenData;\n"
"  node.depth = po.depth + 1;\n"
"  node.relpath = po.relpath;\n"
"  node.isLast = lastNode;\n"
"\n"
"  node.li = document.createElement(\"li\");\n"
"  po.getChildrenUL().appendChild(node.li);\n"
"  node.parentNode = po;\n"
"\n"
"  node.itemDiv = document.createElement(\"div\");\n"
"  node.itemDiv.className = \"item\";\n"
"\n"
"  node.labelSpan = document.createElement(\"span\");\n"
"  node.labelSpan.className = \"label\";\n"
"\n"
"  createIndent(o,node.itemDiv,node,0);\n"
"  node.itemDiv.appendChild(node.labelSpan);\n"
"  node.li.appendChild(node.itemDiv);\n"
"\n"
"  var a = document.createElement(\"a\");\n"
"  node.labelSpan.appendChild(a);\n"
"  node.label = document.createTextNode(text);\n"
"  node.expanded = false;\n"
"  a.appendChild(node.label);\n"
"  if (link) {\n"
"    var url;\n"
"    if (link.substring(0,1)=='^') {\n"
"      url = link.substring(1);\n"
"      link = url;\n"
"    } else {\n"
"      url = node.relpath+link;\n"
"    }\n"
"    a.className = stripPath(link.replace('#',':'));\n"
"    if (link.indexOf('#')!=-1) {\n"
"      var aname = '#'+link.split('#')[1];\n"
"      var srcPage = stripPath($(location).attr('pathname'));\n"
"      var targetPage = stripPath(link.split('#')[0]);\n"
"      a.href = srcPage!=targetPage ? url : \"javascript:void(0)\"; \n"
"      a.onclick = function(){\n"
"        storeLink(link);\n"
"        if (!$(a).parent().parent().hasClass('selected'))\n"
"        {\n"
"          $('.item').removeClass('selected');\n"
"          $('.item').removeAttr('id');\n"
"          $(a).parent().parent().addClass('selected');\n"
"          $(a).parent().parent().attr('id','selected');\n"
"        }\n"
"        var anchor = $(aname);\n"
"        gotoAnchor(anchor,aname,true);\n"
"      };\n"
"    } else {\n"
"      a.href = url;\n"
"      a.onclick = function() { storeLink(link); }\n"
"    }\n"
"  } else {\n"
"    if (childrenData != null) \n"
"    {\n"
"      a.className = \"nolink\";\n"
"      a.href = \"javascript:void(0)\";\n"
"      a.onclick = node.expandToggle.onclick;\n"
"    }\n"
"  }\n"
"\n"
"  node.childrenUL = null;\n"
"  node.getChildrenUL = function() {\n"
"    if (!node.childrenUL) {\n"
"      node.childrenUL = document.createElement(\"ul\");\n"
"      node.childrenUL.className = \"children_ul\";\n"
"      node.childrenUL.style.display = \"none\";\n"
"      node.li.appendChild(node.childrenUL);\n"
"    }\n"
"    return node.childrenUL;\n"
"  };\n"
"\n"
"  return node;\n"
"}\n"
"\n"
"function showRoot()\n"
"{\n"
"  var headerHeight = $(\"#top\").height();\n"
"  var footerHeight = $(\"#nav-path\").height();\n"
"  var windowHeight = $(window).height() - headerHeight - footerHeight;\n"
"  (function (){ // retry until we can scroll to the selected item\n"
"    try {\n"
"      var navtree=$('#nav-tree');\n"
"      navtree.scrollTo('#selected',0,{offset:-windowHeight/2});\n"
"    } catch (err) {\n"
"      setTimeout(arguments.callee, 0);\n"
"    }\n"
"  })();\n"
"}\n"
"\n"
"function expandNode(o, node, imm, showRoot)\n"
"{\n"
"  if (node.childrenData && !node.expanded) {\n"
"    if (typeof(node.childrenData)==='string') {\n"
"      var varName    = node.childrenData;\n"
"      getScript(node.relpath+varName,function(){\n"
"        node.childrenData = getData(varName);\n"
"        expandNode(o, node, imm, showRoot);\n"
"      }, showRoot);\n"
"    } else {\n"
"      if (!node.childrenVisited) {\n"
"        getNode(o, node);\n"
"      } if (imm || ($.browser.msie && $.browser.version>8)) { \n"
"        // somehow slideDown jumps to the start of tree for IE9 :-(\n"
"        $(node.getChildrenUL()).show();\n"
"      } else {\n"
"        $(node.getChildrenUL()).slideDown(\"fast\");\n"
"      }\n"
"      if (node.isLast) {\n"
"        node.plus_img.src = node.relpath+\"ftv2mlastnode.png\";\n"
"      } else {\n"
"        node.plus_img.src = node.relpath+\"ftv2mnode.png\";\n"
"      }\n"
"      node.expanded = true;\n"
"    }\n"
"  }\n"
"}\n"
"\n"
"function glowEffect(n,duration)\n"
"{\n"
"  n.addClass('glow').delay(duration).queue(function(next){\n"
"    $(this).removeClass('glow');next();\n"
"  });\n"
"}\n"
"\n"
"function highlightAnchor()\n"
"{\n"
"  var aname = $(location).attr('hash');\n"
"  var anchor = $(aname);\n"
"  if (anchor.parent().attr('class')=='memItemLeft'){\n"
"    var rows = $('.memberdecls tr[class$=\"'+\n"
"               window.location.hash.substring(1)+'\"]');\n"
"    glowEffect(rows.children(),300); // member without details\n"
"  } else if (anchor.parents().slice(2).prop('tagName')=='TR') {\n"
"    glowEffect(anchor.parents('div.memitem'),1000); // enum value\n"
"  } else if (anchor.parent().attr('class')=='fieldtype'){\n"
"    glowEffect(anchor.parent().parent(),1000); // struct field\n"
"  } else if (anchor.parent().is(\":header\")) {\n"
"    glowEffect(anchor.parent(),1000); // section header\n"
"  } else {\n"
"    glowEffect(anchor.next(),1000); // normal member\n"
"  }\n"
"  gotoAnchor(anchor,aname,false);\n"
"}\n"
"\n"
"function selectAndHighlight(hash,n)\n"
"{\n"
"  var a;\n"
"  if (hash) {\n"
"    var link=stripPath($(location).attr('pathname'))+':'+hash.substring(1);\n"
"    a=$('.item a[class$=\"'+link+'\"]');\n"
"  }\n"
"  if (a && a.length) {\n"
"    a.parent().parent().addClass('selected');\n"
"    a.parent().parent().attr('id','selected');\n"
"    highlightAnchor();\n"
"  } else if (n) {\n"
"    $(n.itemDiv).addClass('selected');\n"
"    $(n.itemDiv).attr('id','selected');\n"
"  }\n"
"  if ($('#nav-tree-contents .item:first').hasClass('selected')) {\n"
"    $('#nav-sync').css('top','30px');\n"
"  } else {\n"
"    $('#nav-sync').css('top','5px');\n"
"  }\n"
"  showRoot();\n"
"}\n"
"\n"
"function showNode(o, node, index, hash)\n"
"{\n"
"  if (node && node.childrenData) {\n"
"    if (typeof(node.childrenData)==='string') {\n"
"      var varName    = node.childrenData;\n"
"      getScript(node.relpath+varName,function(){\n"
"        node.childrenData = getData(varName);\n"
"        showNode(o,node,index,hash);\n"
"      },true);\n"
"    } else {\n"
"      if (!node.childrenVisited) {\n"
"        getNode(o, node);\n"
"      }\n"
"      $(node.getChildrenUL()).show();\n"
"      if (node.isLast) {\n"
"        node.plus_img.src = node.relpath+\"ftv2mlastnode.png\";\n"
"      } else {\n"
"        node.plus_img.src = node.relpath+\"ftv2mnode.png\";\n"
"      }\n"
"      node.expanded = true;\n"
"      var n = node.children[o.breadcrumbs[index]];\n"
"      if (index+1<o.breadcrumbs.length) {\n"
"        showNode(o,n,index+1,hash);\n"
"      } else {\n"
"        if (typeof(n.childrenData)==='string') {\n"
"          var varName = n.childrenData;\n"
"          getScript(n.relpath+varName,function(){\n"
"            n.childrenData = getData(varName);\n"
"            node.expanded=false;\n"
"            showNode(o,node,index,hash); // retry with child node expanded\n"
"          },true);\n"
"        } else {\n"
"          var rootBase = stripPath(o.toroot.replace(/\\..+$/, ''));\n"
"          if (rootBase==\"index\" || rootBase==\"pages\" || rootBase==\"search\") {\n"
"            expandNode(o, n, true, true);\n"
"          }\n"
"          selectAndHighlight(hash,n);\n"
"        }\n"
"      }\n"
"    }\n"
"  } else {\n"
"    selectAndHighlight(hash);\n"
"  }\n"
"}\n"
"\n"
"function getNode(o, po)\n"
"{\n"
"  po.childrenVisited = true;\n"
"  var l = po.childrenData.length-1;\n"
"  for (var i in po.childrenData) {\n"
"    var nodeData = po.childrenData[i];\n"
"    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],\n"
"      i==l);\n"
"  }\n"
"}\n"
"\n"
"function gotoNode(o,subIndex,root,hash,relpath)\n"
"{\n"
"  var nti = navTreeSubIndices[subIndex][root+hash];\n"
"  o.breadcrumbs = $.extend(true, [], nti ? nti : navTreeSubIndices[subIndex][root]);\n"
"  if (!o.breadcrumbs && root!=NAVTREE[0][1]) { // fallback: show index\n"
"    navTo(o,NAVTREE[0][1],\"\",relpath);\n"
"    $('.item').removeClass('selected');\n"
"    $('.item').removeAttr('id');\n"
"  }\n"
"  if (o.breadcrumbs) {\n"
"    o.breadcrumbs.unshift(0); // add 0 for root node\n"
"    showNode(o, o.node, 0, hash);\n"
"  }\n"
"}\n"
"\n"
"function navTo(o,root,hash,relpath)\n"
"{\n"
"  var link = cachedLink();\n"
"  if (link) {\n"
"    var parts = link.split('#');\n"
"    root = parts[0];\n"
"    if (parts.length>1) hash = '#'+parts[1];\n"
"    else hash='';\n"
"  }\n"
"  if (hash.match(/^#l\\d+$/)) {\n"
"    var anchor=$('a[name='+hash.substring(1)+']');\n"
"    glowEffect(anchor.parent(),1000); // line number\n"
"    hash=''; // strip line number anchors\n"
"    //root=root.replace(/_source\\./,'.'); // source link to doc link\n"
"  }\n"
"  var url=root+hash;\n"
"  var i=-1;\n"
"  while (NAVTREEINDEX[i+1]<=url) i++;\n"
"  if (i==-1) { i=0; root=NAVTREE[0][1]; } // fallback: show index\n"
"  if (navTreeSubIndices[i]) {\n"
"    gotoNode(o,i,root,hash,relpath)\n"
"  } else {\n"
"    getScript(relpath+'navtreeindex'+i,function(){\n"
"      navTreeSubIndices[i] = eval('NAVTREEINDEX'+i);\n"
"      if (navTreeSubIndices[i]) {\n"
"        gotoNode(o,i,root,hash,relpath);\n"
"      }\n"
"    },true);\n"
"  }\n"
"}\n"
"\n"
"function showSyncOff(n,relpath)\n"
"{\n"
"    n.html('<img src=\"'+relpath+'sync_off.png\" title=\"'+SYNCOFFMSG+'\"/>');\n"
"}\n"
"\n"
"function showSyncOn(n,relpath)\n"
"{\n"
"    n.html('<img src=\"'+relpath+'sync_on.png\" title=\"'+SYNCONMSG+'\"/>');\n"
"}\n"
"\n"
"function toggleSyncButton(relpath)\n"
"{\n"
"  var navSync = $('#nav-sync');\n"
"  if (navSync.hasClass('sync')) {\n"
"    navSync.removeClass('sync');\n"
"    showSyncOff(navSync,relpath);\n"
"    storeLink(stripPath2($(location).attr('pathname'))+$(location).attr('hash'));\n"
"  } else {\n"
"    navSync.addClass('sync');\n"
"    showSyncOn(navSync,relpath);\n"
"    deleteLink();\n"
"  }\n"
"}\n"
"\n"
"function initNavTree(toroot,relpath)\n"
"{\n"
"  var o = new Object();\n"
"  o.toroot = toroot;\n"
"  o.node = new Object();\n"
"  o.node.li = document.getElementById(\"nav-tree-contents\");\n"
"  o.node.childrenData = NAVTREE;\n"
"  o.node.children = new Array();\n"
"  o.node.childrenUL = document.createElement(\"ul\");\n"
"  o.node.getChildrenUL = function() { return o.node.childrenUL; };\n"
"  o.node.li.appendChild(o.node.childrenUL);\n"
"  o.node.depth = 0;\n"
"  o.node.relpath = relpath;\n"
"  o.node.expanded = false;\n"
"  o.node.isLast = true;\n"
"  o.node.plus_img = document.createElement(\"img\");\n"
"  o.node.plus_img.src = relpath+\"ftv2pnode.png\";\n"
"  o.node.plus_img.width = 16;\n"
"  o.node.plus_img.height = 22;\n"
"\n"
"  if (localStorageSupported()) {\n"
"    var navSync = $('#nav-sync');\n"
"    if (cachedLink()) {\n"
"      showSyncOff(navSync,relpath);\n"
"      navSync.removeClass('sync');\n"
"    } else {\n"
"      showSyncOn(navSync,relpath);\n"
"    }\n"
"    navSync.click(function(){ toggleSyncButton(relpath); });\n"
"  }\n"
"\n"
"  navTo(o,toroot,window.location.hash,relpath);\n"
"\n"
"  $(window).bind('hashchange', function(){\n"
"     if (window.location.hash && window.location.hash.length>1){\n"
"       var a;\n"
"       if ($(location).attr('hash')){\n"
"         var clslink=stripPath($(location).attr('pathname'))+':'+\n"
"                               $(location).attr('hash').substring(1);\n"
"         a=$('.item a[class$=\"'+clslink+'\"]');\n"
"       }\n"
"       if (a==null || !$(a).parent().parent().hasClass('selected')){\n"
"         $('.item').removeClass('selected');\n"
"         $('.item').removeAttr('id');\n"
"       }\n"
"       var link=stripPath2($(location).attr('pathname'));\n"
"       navTo(o,link,$(location).attr('hash'),relpath);\n"
"     } else if (!animationInProgress) {\n"
"       $('#doc-content').scrollTop(0);\n"
"       $('.item').removeClass('selected');\n"
"       $('.item').removeAttr('id');\n"
"       navTo(o,toroot,window.location.hash,relpath);\n"
"     }\n"
"  })\n"
"\n"
"  $(window).load(showRoot);\n"
"}\n"
"\n"
;

static const char resize_script[]=
// #include "resize_js.h"
"var cookie_namespace = 'doxygen'; \n"
"var sidenav,navtree,content,header;\n"
"\n"
"function readCookie(cookie) \n"
"{\n"
"  var myCookie = cookie_namespace+\"_\"+cookie+\"=\";\n"
"  if (document.cookie) \n"
"  {\n"
"    var index = document.cookie.indexOf(myCookie);\n"
"    if (index != -1) \n"
"    {\n"
"      var valStart = index + myCookie.length;\n"
"      var valEnd = document.cookie.indexOf(\";\", valStart);\n"
"      if (valEnd == -1) \n"
"      {\n"
"        valEnd = document.cookie.length;\n"
"      }\n"
"      var val = document.cookie.substring(valStart, valEnd);\n"
"      return val;\n"
"    }\n"
"  }\n"
"  return 0;\n"
"}\n"
"\n"
"function writeCookie(cookie, val, expiration) \n"
"{\n"
"  if (val==undefined) return;\n"
"  if (expiration == null) \n"
"  {\n"
"    var date = new Date();\n"
"    date.setTime(date.getTime()+(10*365*24*60*60*1000)); // default expiration is one week\n"
"    expiration = date.toGMTString();\n"
"  }\n"
"  document.cookie = cookie_namespace + \"_\" + cookie + \"=\" + val + \"; expires=\" + expiration+\"; path=/\";\n"
"}\n"
" \n"
"function resizeWidth() \n"
"{\n"
"  var windowWidth = $(window).width() + \"px\";\n"
"  var sidenavWidth = $(sidenav).outerWidth();\n"
"  content.css({marginLeft:parseInt(sidenavWidth)+6+\"px\"}); //account for 6px-wide handle-bar\n"
"  writeCookie('width',sidenavWidth, null);\n"
"}\n"
"\n"
"function restoreWidth(navWidth)\n"
"{\n"
"  var windowWidth = $(window).width() + \"px\";\n"
"  content.css({marginLeft:parseInt(navWidth)+6+\"px\"});\n"
"  sidenav.css({width:navWidth + \"px\"});\n"
"}\n"
"\n"
"function resizeHeight() \n"
"{\n"
"  var headerHeight = header.outerHeight();\n"
"  var footerHeight = footer.outerHeight();\n"
"  var windowHeight = $(window).height() - headerHeight - footerHeight;\n"
"  content.css({height:windowHeight + \"px\"});\n"
"  navtree.css({height:windowHeight + \"px\"});\n"
"  sidenav.css({height:windowHeight + \"px\",top: headerHeight+\"px\"});\n"
"}\n"
"\n"
"function initResizable()\n"
"{\n"
"  header  = $(\"#top\");\n"
"  sidenav = $(\"#side-nav\");\n"
"  content = $(\"#doc-content\");\n"
"  navtree = $(\"#nav-tree\");\n"
"  footer  = $(\"#nav-path\");\n"
"  $(\".side-nav-resizable\").resizable({resize: function(e, ui) { resizeWidth(); } });\n"
"  $(window).resize(function() { resizeHeight(); });\n"
"  var width = readCookie('width');\n"
"  if (width) { restoreWidth(width); } else { resizeWidth(); }\n"
"  resizeHeight();\n"
"  var url = location.href;\n"
"  var i=url.indexOf(\"#\");\n"
"  if (i>=0) window.location.hash=url.substr(i);\n"
"  var _preventDefault = function(evt) { evt.preventDefault(); };\n"
"  $(\"#splitbar\").bind(\"dragstart\", _preventDefault).bind(\"selectstart\", _preventDefault);\n"
"  $(document).bind('touchmove',function(e){\n"
"    try {\n"
"      var target = e.target;\n"
"      while (target) {\n"
"        if ($(target).css('-webkit-overflow-scrolling')=='touch') return;\n"
"        target = target.parentNode;\n"
"      }\n"
"      e.preventDefault();\n"
"    } catch(err) {\n"
"      e.preventDefault();\n"
"    }\n"
"  });\n"
"}\n"
"\n"
"\n"
;

static const char navtree_css[]=
// #include "navtree_css.h"
"#nav-tree .children_ul {\n"
"  margin:0;\n"
"  padding:4px;\n"
"}\n"
"\n"
"#nav-tree ul {\n"
"  list-style:none outside none;\n"
"  margin:0px;\n"
"  padding:0px;\n"
"}\n"
"\n"
"#nav-tree li {\n"
"  white-space:nowrap;\n"
"  margin:0px;\n"
"  padding:0px;\n"
"}\n"
"\n"
"#nav-tree .plus {\n"
"  margin:0px;\n"
"}\n"
"\n"
"#nav-tree .selected {\n"
"  background-image: url('tab_a.png');\n"
"  background-repeat:repeat-x;\n"
"  color: #fff;\n"
"  text-shadow: 0px 1px 1px rgba(0, 0, 0, 1.0);\n"
"}\n"
"\n"
"#nav-tree img {\n"
"  margin:0px;\n"
"  padding:0px;\n"
"  border:0px;\n"
"  vertical-align: middle;\n"
"}\n"
"\n"
"#nav-tree a {\n"
"  text-decoration:none;\n"
"  padding:0px;\n"
"  margin:0px;\n"
"  outline:none;\n"
"}\n"
"\n"
"#nav-tree .label {\n"
"  margin:0px;\n"
"  padding:0px;\n"
"  font: 12px 'Lucida Grande',Geneva,Helvetica,Arial,sans-serif;\n"
"}\n"
"\n"
"#nav-tree .label a {\n"
"  padding:2px;\n"
"}\n"
"\n"
"#nav-tree .selected a {\n"
"  text-decoration:none;\n"
"  color:#fff;\n"
"}\n"
"\n"
"#nav-tree .children_ul {\n"
"  margin:0px;\n"
"  padding:0px;\n"
"}\n"
"\n"
"#nav-tree .item {\n"
"  margin:0px;\n"
"  padding:0px;\n"
"}\n"
"\n"
"#nav-tree {\n"
"  padding: 0px 0px;\n"
"  background-color: #FAFAFF; \n"
"  font-size:14px;\n"
"  overflow:auto;\n"
"}\n"
"\n"
"#doc-content {\n"
"  overflow:auto;\n"
"  display:block;\n"
"  padding:0px;\n"
"  margin:0px;\n"
"  -webkit-overflow-scrolling : touch; /* iOS 5+ */\n"
"}\n"
"\n"
"#side-nav {\n"
"  padding:0 6px 0 0;\n"
"  margin: 0px;\n"
"  display:block;\n"
"  position: absolute;\n"
"  left: 0px;\n"
"  width: $width;\n"
"}\n"
"\n"
".ui-resizable .ui-resizable-handle {\n"
"  display:block;\n"
"}\n"
"\n"
".ui-resizable-e {\n"
"  background:url(\"ftv2splitbar.png\") repeat scroll right center transparent;\n"
"  cursor:e-resize;\n"
"  height:100%;\n"
"  right:0;\n"
"  top:0;\n"
"  width:6px;\n"
"}\n"
"\n"
".ui-resizable-handle {\n"
"  display:none;\n"
"  font-size:0.1px;\n"
"  position:absolute;\n"
"  z-index:1;\n"
"}\n"
"\n"
"#nav-tree-contents {\n"
"  margin: 6px 0px 0px 0px;\n"
"}\n"
"\n"
"#nav-tree {\n"
"  background-image:url('nav_h.png');\n"
"  background-repeat:repeat-x;\n"
"  background-color: ##FA;\n"
"  -webkit-overflow-scrolling : touch; /* iOS 5+ */\n"
"}\n"
"\n"
"#nav-sync {\n"
"  position:absolute;\n"
"  top:5px;\n"
"  right:24px;\n"
"  z-index:0;\n"
"}\n"
"\n"
"#nav-sync img {\n"
"  opacity:0.3;\n"
"}\n"
"\n"
"#nav-sync img:hover {\n"
"  opacity:0.9;\n"
"}\n"
"\n"
"@media print\n"
"{\n"
"  #nav-tree { display: none; }\n"
"  div.ui-resizable-handle { display: none; position: relative; }\n"
"}\n"
"\n"
;

static unsigned char blank_png[352] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

static unsigned char folderopen_png[528] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,228,195,193,190,187,218,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,195,215,221,225,225,178,176,176,175,176,178,180,255,255,255,255,255,255,
  255,255,255,255,255,255,189,206,215,219,226,220,214,212,207,204,200,176,255,255,255,255,255,255,
  255,255,255,255,168,154,153,153,152,152,151,149,150,150,149,147,146,145,145,167,255,255,255,255,
  255,255,255,255,146,187,187,188,187,187,185,183,183,182,179,178,175,173,174,145,255,255,255,255,
  255,255,255,255,146,180,182,182,181,181,179,178,176,174,173,171,169,170,168,144,255,255,255,255,
  255,255,255,255,144,173,176,176,177,175,175,174,171,170,168,168,166,166,164,143,255,255,255,255,
  255,255,255,255,142,168,170,171,170,170,169,168,166,166,165,163,163,164,162,142,255,255,255,255,
  255,255,255,255,141,162,166,164,164,165,163,163,161,161,161,161,161,160,159,141,255,255,255,255,
  255,255,255,255,138,157,159,159,158,158,158,157,157,157,157,156,157,157,155,138,255,255,255,255,
  255,255,255,255,137,154,153,154,154,153,154,154,154,153,154,154,154,154,154,137,255,255,255,255,
  255,255,255,255,137,154,154,154,154,154,154,154,153,154,154,153,153,153,154,137,255,255,255,255,
  255,255,255,255,137,125,125,125,125,124,125,124,124,125,124,124,125,124,125,138,255,255,255,255,
  255,255,255,255,212,209,204,199,193,190,186,183,180,181,185,188,192,197,202,203,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char folderopen_a_png[528] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,148,148,148,148,148,148,148,148,148,148,148,148,148,148,148,148,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

static unsigned char folderclosed_png[528] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,197,155,155,155,155,196,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,155,191,191,191,192,155,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,168,144,180,180,181,180,145,145,146,145,146,146,146,146,145,167,255,255,255,255,
  255,255,255,255,147,225,226,226,225,226,225,221,221,219,215,214,212,211,213,145,255,255,255,255,
  255,255,255,255,147,212,211,211,210,211,210,205,206,205,201,201,199,196,201,145,255,255,255,255,
  255,255,255,255,146,204,203,204,203,203,202,200,200,197,197,196,195,194,196,145,255,255,255,255,
  255,255,255,255,146,202,200,201,201,200,199,198,198,195,194,194,193,192,194,145,255,255,255,255,
  255,255,255,255,145,200,196,196,196,195,195,193,192,192,190,189,189,189,191,143,255,255,255,255,
  255,255,255,255,143,192,191,190,190,189,189,188,186,187,186,185,185,185,187,142,255,255,255,255,
  255,255,255,255,142,186,184,183,182,183,182,183,180,181,181,181,181,181,182,141,255,255,255,255,
  255,255,255,255,138,177,175,176,176,177,177,176,175,174,175,175,175,174,176,138,255,255,255,255,
  255,255,255,255,138,173,169,170,168,170,169,170,170,169,171,171,171,171,174,137,255,255,255,255,
  255,255,255,255,138,166,163,163,162,162,162,162,162,162,164,163,163,163,166,137,255,255,255,255,
  255,255,255,255,137,124,124,124,125,124,124,124,125,125,124,124,125,124,125,138,255,255,255,255,
  255,255,255,255,231,231,228,225,222,220,218,216,214,215,217,219,221,224,227,226,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char folderclosed_a_png[528] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,
    0,  0,  0,  0,148,148,148,148,148,148,148,148,148,148,148,148,148,148,148,148,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

static unsigned char doc_png[528] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,218,214,208,208,204,191,179,190,197,209,231,255,255,255,255,255,255,255,255,
  255,255,255,255,255,195,224,226,226,222,214,204,181,203,229,188,225,255,255,255,255,255,255,255,
  255,255,255,255,255,198,226,228,227,227,224,215,203,180,252,229,184,224,255,255,255,255,255,255,
  255,255,255,255,255,198,229,230,229,229,228,224,214,154,252,252,229,187,235,255,255,255,255,255,
  255,255,255,255,255,198,232,233,233,232,231,230,223,176,154,144,165,177,216,255,255,255,255,255,
  255,255,255,255,255,198,236,236,216,226,238,219,232,225,209,190,189,166,193,255,255,255,255,255,
  255,255,255,255,255,198,239,240,178,177,230,175,169,184,188,219,208,189,187,255,255,255,255,255,
  255,255,255,255,255,198,241,242,240,218,237,236,240,235,241,244,221,208,182,255,255,255,255,255,
  255,255,255,255,255,198,243,243,188,154,183,158,166,140,185,198,231,219,177,255,255,255,255,255,
  255,255,255,255,255,198,243,245,248,228,241,241,226,249,237,227,239,232,177,255,255,255,255,255,
  255,255,255,255,255,198,244,246,213,172,163,149,171,200,167,149,242,239,177,255,255,255,255,255,
  255,255,255,255,255,198,249,248,240,218,237,236,240,235,241,244,244,242,177,255,255,255,255,255,
  255,255,255,255,255,198,249,251,188,155,184,158,166,140,185,198,246,244,177,255,255,255,255,255,
  255,255,255,255,255,198,251,253,248,228,241,241,226,249,237,227,249,246,177,255,255,255,255,255,
  255,255,255,255,255,196,253,252,252,252,252,251,251,250,250,249,249,248,175,255,255,255,255,255,
  255,255,255,255,255,194, 64, 30, 37, 37, 37, 37, 37, 37, 37, 37, 30, 64,188,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char doc_a_png[528] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

static unsigned char module_png[528] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,255,193,128,136,255,255,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,255,213,128,170,255,255,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,247,247,128,196,255,247,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,213,255,153,230,255,213,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,187,255,187,255,230,204,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,153,255,247,255,196,204,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,128,247,255,255,170,204,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,128,213,255,255,136,204,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,255,255,128,187,255,230,138,204,255,217,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,255,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char namespace_png[528] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,226,128,128,198,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,255,189,128,198,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,255,244,141,198,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,255,255,220,198,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,226,255,255,220,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,198,220,255,255,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,198,141,250,255,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,198,128,198,255,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,157,255,255,198,128,128,226,255,255,157,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,255,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char class_png[528] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,187,247,255,255,230,170,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,196,255,255,255,255,255,255,170,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,145,255,255,230,128,136,230,247,179,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,179,255,255,170,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,179,255,255,162,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,179,255,255,170,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,145,255,255,221,128,128,221,255,179,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,196,255,255,255,255,255,255,187,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,187,247,255,255,240,179,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,
  255,255,128,128,128,128,128,128,128,128,128,128,128,128,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};


static unsigned char letter_a_png[528] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0, 60,156,204,204,204,204,204,204,204,204,156, 51,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0, 78,255,255,255,255,255,255,255,255,255,255,255,252, 72,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,210,255,255,255,255,255,255,255,255,255,255,255,255,207,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,240,255,255,255,255,255,255,255,255,255,255,255,255,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,222,255,255,255,255,255,255,255,255,255,255,255,255,219,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,111,255,255,255,255,255,255,255,255,255,255,255,255, 99,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0, 99,198,204,204,204,204,204,204,204,204,195, 90,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};


static unsigned char arrow_right_png[352] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,152,152,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,152,152,152,152,255,255,255,255,255,255,255,255,255,
  255,255,255,152,152,152,152,152,255,255,255,255,255,255,255,255,
  255,255,255,152,152,152,152,152,152,152,255,255,255,255,255,255,
  255,255,255,152,152,152,152,152,152,152,152,255,255,255,255,255,
  255,255,255,152,152,152,152,152,152,152,255,255,255,255,255,255,
  255,255,255,152,152,152,152,152,255,255,255,255,255,255,255,255,
  255,255,255,152,152,152,152,255,255,255,255,255,255,255,255,255,
  255,255,255,152,152,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char arrow_right_a_png[352] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,223, 75,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,176, 33,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,255,248,117,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,255,255,255,211, 60,  0,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,255,255,255,255,255, 77,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,255,255,255,211, 60,  0,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,255,248,117,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,255,255,176, 33,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,223, 75,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

static unsigned char arrow_down_png[352] =
{
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,152,152,152,152,152,152,152,152,152,255,255,255,255,
  255,255,255,152,152,152,152,152,152,152,152,152,255,255,255,255,
  255,255,255,255,152,152,152,152,152,152,152,255,255,255,255,255,
  255,255,255,255,152,152,152,152,152,152,152,255,255,255,255,255,
  255,255,255,255,255,152,152,152,152,152,255,255,255,255,255,255,
  255,255,255,255,255,255,152,152,152,255,255,255,255,255,255,255,
  255,255,255,255,255,255,152,152,152,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,152,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static unsigned char arrow_down_a_png[352] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,231,255,255,255,255,255,255,255,216,  0,  0,  0,  0,
    0,  0,  0, 87,255,255,255,255,255,255,255, 65,  0,  0,  0,  0,
    0,  0,  0,  0,186,255,255,255,255,255,164,  0,  0,  0,  0,  0,
    0,  0,  0,  0, 38,251,255,255,255,241, 25,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,127,255,255,255,107,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,221,255,204,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0, 72,253, 52,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0, 77,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

#define SPLITBAR_LINE 170,242,224,202,183,170
#define SPLITBAR_BLOCK2  SPLITBAR_LINE    , SPLITBAR_LINE 
#define SPLITBAR_BLOCK4  SPLITBAR_BLOCK2  , SPLITBAR_BLOCK2 
#define SPLITBAR_BLOCK8  SPLITBAR_BLOCK4  , SPLITBAR_BLOCK4 
#define SPLITBAR_BLOCK16 SPLITBAR_BLOCK8  , SPLITBAR_BLOCK8 
#define SPLITBAR_BLOCK32 SPLITBAR_BLOCK16 , SPLITBAR_BLOCK16

#define SPLITBAR_ALTLINE1 170,242,170,202,170,170
#define SPLITBAR_ALTLINE2 170,243,224,255,183,255
#define SPLITBAR_ALTBLOCK2 SPLITBAR_ALTLINE1  , SPLITBAR_ALTLINE2 
#define SPLITBAR_ALTBLOCK4 SPLITBAR_ALTBLOCK2 , SPLITBAR_ALTBLOCK2 
#define SPLITBAR_ALTBLOCK8 SPLITBAR_ALTBLOCK4 , SPLITBAR_ALTBLOCK4

static unsigned char splitbar_png[32*32*6] =
{
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK8,
  SPLITBAR_BLOCK8,
  SPLITBAR_ALTBLOCK8,
  SPLITBAR_BLOCK8,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32,
  SPLITBAR_BLOCK32
};

struct FTVImageInfo
{
  const char *alt;
  const char *name;
  const unsigned char *data;
  //unsigned int len;
  unsigned short width, height;
};

//extern FTVImageInfo image_info[];

#define FTVIMG_blank        0
#define FTVIMG_doc          1
#define FTVIMG_folderclosed 2
#define FTVIMG_folderopen   3
#define FTVIMG_lastnode     4
#define FTVIMG_link         5
#define FTVIMG_mlastnode    6
#define FTVIMG_mnode        7
#define FTVIMG_node         8
#define FTVIMG_plastnode    9
#define FTVIMG_pnode       10
#define FTVIMG_vertline    11
#define FTVIMG_ns          12
#define FTVIMG_cl          13
#define FTVIMG_mo          14

#define FTV_S(name) #name
#define FTV_ICON_FILE(name) "ftv2" FTV_S(name) ".png"
#define FTVIMG_INDEX(name) FTVIMG_ ## name
#define FTV_INFO(name) ( image_info[FTVIMG_INDEX(name)] )
#define FTV_IMGATTRIBS(name) \
    "src=\"" FTV_ICON_FILE(name) "\" " \
    "alt=\"" << FTV_INFO(name).alt << "\" " \
    "width=\"" << FTV_INFO(name).width << "\" " \
    "height=\"" << FTV_INFO(name).height << "\" "


static FTVImageInfo image_info[] =
{
  { "&#160;", "ftv2blank.png",    0 /*ftv2blank_png*/        /*,174*/,16,22 },
  { "*",  "ftv2doc.png",          0 /*ftv2doc_png*/          /*,255*/,24,22 },
  { "+",  "ftv2folderclosed.png", 0 /*ftv2folderclosed_png*/ /*,259*/,24,22 },
  { "-",  "ftv2folderopen.png",   0 /*ftv2folderopen_png*/   /*,261*/,24,22 },
  { "\\", "ftv2lastnode.png",     0 /*ftv2lastnode_png*/     /*,233*/,16,22 },
  { "-",  "ftv2link.png",         0 /*ftv2link_png*/         /*,358*/,24,22 },
  { "\\", "ftv2mlastnode.png",    0 /*ftv2mlastnode_png*/    /*,160*/,16,22 },
  { "o",  "ftv2mnode.png",        0 /*ftv2mnode_png*/        /*,194*/,16,22 },
  { "o",  "ftv2node.png",         0 /*ftv2node_png*/         /*,235*/,16,22 },
  { "\\", "ftv2plastnode.png",    0 /*ftv2plastnode_png*/    /*,165*/,16,22 },
  { "o",  "ftv2pnode.png",        0 /*ftv2pnode_png*/        /*,200*/,16,22 },
  { "|",  "ftv2vertline.png",     0 /*ftv2vertline_png*/     /*,229*/,16,22 },
  { "N",  "ftv2ns.png",           0 /*ftv2vertline_png*/     /*,352*/,24,22 },
  { "C",  "ftv2cl.png",           0 /*ftv2vertline_png*/     /*,352*/,24,22 },
  { "M",  "ftv2mo.png",           0 /*ftv2vertline_png*/     /*,352*/,24,22 },
  {   0,  0,                      0                          /*,  0*/, 0, 0 }
};

static ColoredImgDataItem ftv_image_data[] =
{
  { "ftv2blank.png",        16,  22, blank_png,        blank_png          },
  { "ftv2doc.png",          24,  22, doc_png,          doc_a_png          },
  { "ftv2folderclosed.png", 24,  22, folderclosed_png, folderclosed_a_png },
  { "ftv2folderopen.png",   24,  22, folderopen_png,   folderopen_a_png   },
  { "ftv2ns.png",           24,  22, namespace_png,    letter_a_png       },
  { "ftv2mo.png",           24,  22, module_png,       letter_a_png       },
  { "ftv2cl.png",           24,  22, class_png,        letter_a_png       },
  { "ftv2lastnode.png",     16,  22, blank_png,        blank_png          },
  { "ftv2link.png",         24,  22, doc_png,          doc_a_png          },
  { "ftv2mlastnode.png",    16,  22, arrow_down_png,   arrow_down_a_png   },
  { "ftv2mnode.png",        16,  22, arrow_down_png,   arrow_down_a_png   },
  { "ftv2node.png",         16,  22, blank_png,        blank_png          },
  { "ftv2plastnode.png",    16,  22, arrow_right_png,  arrow_right_a_png  },
  { "ftv2pnode.png",        16,  22, arrow_right_png,  arrow_right_a_png  },
  { "ftv2vertline.png",     16,  22, blank_png,        blank_png          },
  { "ftv2splitbar.png",      6,1024, splitbar_png,     0                  },
  { 0,                       0,   0, 0,                0                  }
};

static int folderId=1;

struct FTVNode
{
  FTVNode(bool dir,const char *r,const char *f,const char *a,
          const char *n,bool sepIndex,bool navIndex,Definition *df)
    : isLast(TRUE), isDir(dir),ref(r),file(f),anchor(a),name(n), index(0),
      parent(0), separateIndex(sepIndex), addToNavIndex(navIndex),
      def(df) { children.setAutoDelete(TRUE); }
  int computeTreeDepth(int level) const;
  int numNodesAtLevel(int level,int maxLevel) const;
  bool isLast;
  bool isDir;
  QCString ref;
  QCString file;
  QCString anchor;
  QCString name;
  int index;
  QList<FTVNode> children;
  FTVNode *parent;
  bool separateIndex;
  bool addToNavIndex;
  Definition *def;
};

int FTVNode::computeTreeDepth(int level) const
{
  int maxDepth=level;
  QListIterator<FTVNode> li(children);
  FTVNode *n;
  for (;(n=li.current());++li)
  {
    if (n->children.count()>0)
    {
      int d = n->computeTreeDepth(level+1);
      if (d>maxDepth) maxDepth=d;
    }
  }
  return maxDepth;
}

int FTVNode::numNodesAtLevel(int level,int maxLevel) const
{
  int num=0;
  if (level<maxLevel)
  {
    num++; // this node
    QListIterator<FTVNode> li(children);
    FTVNode *n;
    for (;(n=li.current());++li)
    {
      num+=n->numNodesAtLevel(level+1,maxLevel);
    }
  }
  return num;
}

//----------------------------------------------------------------------------

/*! Constructs an ftv help object. 
 *  The object has to be \link initialize() initialized\endlink before it can 
 *  be used.
 */
FTVHelp::FTVHelp(bool TLI) 
{
  /* initial depth */
  m_indentNodes = new QList<FTVNode>[MAX_INDENT];
  m_indentNodes[0].setAutoDelete(TRUE);
  m_indent=0;
  m_topLevelIndex = TLI;
}

/*! Destroys the ftv help object. */
FTVHelp::~FTVHelp()
{
  delete[] m_indentNodes;
}

/*! This will create a folder tree view table of contents file (tree.js).
 *  \sa finalize()
 */
void FTVHelp::initialize()
{
}

/*! Finalizes the FTV help. This will finish and close the
 *  contents file (index.js).
 *  \sa initialize()
 */
void FTVHelp::finalize()
{
  generateTreeView();
}

/*! Increase the level of the contents hierarchy. 
 *  This will start a new sublist in contents file.
 *  \sa decContentsDepth()
 */
void FTVHelp::incContentsDepth()
{
  //printf("incContentsDepth() indent=%d\n",m_indent);
  m_indent++;
  ASSERT(m_indent<MAX_INDENT);
}

/*! Decrease the level of the contents hierarchy.
 *  This will end the current sublist.
 *  \sa incContentsDepth()
 */
void FTVHelp::decContentsDepth()
{
  //printf("decContentsDepth() indent=%d\n",m_indent);
  ASSERT(m_indent>0);
  if (m_indent>0)
  {
    m_indent--;
    QList<FTVNode> *nl = &m_indentNodes[m_indent];
    FTVNode *parent = nl->getLast();
    if (parent)
    {
      QList<FTVNode> *children = &m_indentNodes[m_indent+1];
      while (!children->isEmpty())
      {
        parent->children.append(children->take(0));
      }
    }
  }
}

/*! Add a list item to the contents file.
 *  \param isDir TRUE if the item is a directory, FALSE if it is a text
 *  \param ref  the URL of to the item.
 *  \param file the file containing the definition of the item
 *  \param anchor the anchor within the file.
 *  \param name the name of the item.
 *  \param separateIndex put the entries in a separate index file
 *  \param addToNavIndex add this entry to the quick navigation index
 *  \param def Definition corresponding to this entry
 */
void FTVHelp::addContentsItem(bool isDir,
                              const char *name,
                              const char *ref,
                              const char *file,
                              const char *anchor,
                              bool separateIndex,
                              bool addToNavIndex,
                              Definition *def
                              )
{
  //printf("%p: m_indent=%d addContentsItem(%s,%s,%s,%s)\n",this,m_indent,name,ref,file,anchor);
  QList<FTVNode> *nl = &m_indentNodes[m_indent];
  FTVNode *newNode = new FTVNode(isDir,ref,file,anchor,name,separateIndex,addToNavIndex,def);
  if (!nl->isEmpty())
  {
    nl->getLast()->isLast=FALSE;
  }
  nl->append(newNode);
  newNode->index = nl->count()-1;
  if (m_indent>0)
  {
    QList<FTVNode> *pnl = &m_indentNodes[m_indent-1];
    newNode->parent = pnl->getLast();
  }
  
}

static QCString node2URL(FTVNode *n,bool overruleFile=FALSE,bool srcLink=FALSE)
{
  QCString url = n->file;
  if (!url.isEmpty() && url.at(0)=='!')  // relative URL
  {
    // remove leading !
    url = url.mid(1);
  }
  else if (!url.isEmpty() && url.at(0)=='^') // absolute URL
  {
    // skip, keep ^ in the output
  }
  else // local file (with optional anchor)
  {
    if (overruleFile && n->def && n->def->definitionType()==Definition::TypeFile)
    {
      FileDef *fd = (FileDef*)n->def;
      if (srcLink)
      {
        url = fd->getSourceFileBase();
      }
      else
      {
        url = fd->getOutputFileBase();
      }
    }
    url+=Doxygen::htmlFileExtension;
    if (!n->anchor.isEmpty()) url+="#"+n->anchor;
  }
  return url;
}

QCString FTVHelp::generateIndentLabel(FTVNode *n,int level)
{
  QCString result;
  if (n->parent)
  {
    result=generateIndentLabel(n->parent,level+1);
  }
  result+=QCString().sprintf("%d_",n->index);
  return result;
}

void FTVHelp::generateIndent(FTextStream &t, FTVNode *n,int level, bool opened)
{
  if (n->parent)
  {
    generateIndent(t,n->parent,level+1,opened);
  }
  // from the root up to node n do...
  if (level==0) // item before a dir or document
  {
    if (n->isLast)
    {
      if (n->isDir)
      {
        t << "<img id=\"arr_" << generateIndentLabel(n,0) 
          << "\" ";
        if (opened)
          t << FTV_IMGATTRIBS(mlastnode);
        else 
          t << FTV_IMGATTRIBS(plastnode);
        t << "onclick=\"toggleFolder('" 
          << generateIndentLabel(n,0) 
          << "')\"/>";
      }
      else
      {
        t << "<img " << FTV_IMGATTRIBS(lastnode) << "/>";
      }
    }
    else
    {
      if (n->isDir)
      {
        t << "<img id=\"arr_" << generateIndentLabel(n,0)
          << "\" ";
        if (opened)
          t << FTV_IMGATTRIBS(mnode);
        else
          t << FTV_IMGATTRIBS(pnode);
        t << "onclick=\"toggleFolder('" 
          << generateIndentLabel(n,0)
          << "')\"/>";
      }
      else
      {
        t << "<img " << FTV_IMGATTRIBS(node) << "/>";
      }
    }
  }
  else // item at another level
  {
    if (n->isLast)
    {
      t << "<img " << FTV_IMGATTRIBS(blank) << "/>";
    }
    else
    {
      t << "<img " << FTV_IMGATTRIBS(vertline) << "/>";
    }
  }
}

void FTVHelp::generateLink(FTextStream &t,FTVNode *n)
{
  //printf("FTVHelp::generateLink(ref=%s,file=%s,anchor=%s\n",
  //    n->ref.data(),n->file.data(),n->anchor.data());
  if (n->file.isEmpty()) // no link
  {
    t << "<b>" << convertToHtml(n->name) << "</b>";
  }
  else // link into other frame
  {
    if (!n->ref.isEmpty()) // link to entity imported via tag file
    {
      t << "<a class=\"elRef\" ";
      t << externalLinkTarget() << externalRef("",n->ref,FALSE);
    }
    else // local link
    {
      t << "<a class=\"el\" ";
    }
    t << "href=\"";
    t << externalRef("",n->ref,TRUE);
    t << node2URL(n);
    if (m_topLevelIndex)
      t << "\" target=\"basefrm\">";
    else
      t << "\" target=\"_self\">";
    t << convertToHtml(n->name);
    t << "</a>";
    if (!n->ref.isEmpty())
    {
      t << "&#160;[external]";
    }
  }
}

static void generateBriefDoc(FTextStream &t,Definition *def)
{
  QCString brief = def->briefDescription(TRUE);
  if (!brief.isEmpty())
  {
    DocNode *root = validatingParseDoc(def->briefFile(),def->briefLine(),
        def,0,brief,FALSE,FALSE,0,TRUE,TRUE);
    QCString relPath = relativePathToRoot(def->getOutputFileBase());
    HtmlCodeGenerator htmlGen(t,relPath);
    HtmlDocVisitor *visitor = new HtmlDocVisitor(t,htmlGen,def,0);
    root->accept(visitor);
    delete visitor;
    delete root;
  }
}

void FTVHelp::generateTree(FTextStream &t, const QList<FTVNode> &nl,int level,int maxLevel,int &index)
{
  QListIterator<FTVNode> nli(nl);
  FTVNode *n;
  for (nli.toFirst();(n=nli.current());++nli)
  {
    t << "<tr id=\"row_" << generateIndentLabel(n,0) << "\"";
    if ((index&1)==0) // even row
      t << " class=\"even\"";
    if (level>=maxLevel) // item invisible by default
      t << " style=\"display:none;\"";
    else // item visible by default
      index++;
    t << "><td class=\"entry\">";
    bool nodeOpened = level+1<maxLevel;
    generateIndent(t,n,0,nodeOpened);
    if (n->isDir)
    {
      if (n->def && n->def->definitionType()==Definition::TypeGroup)
      {
        // no icon
      }
      else if (n->def && n->def->definitionType()==Definition::TypePage)
      {
        // no icon
      }
      else if (n->def && n->def->definitionType()==Definition::TypeNamespace)
      {
        t << "<img ";
        t << FTV_IMGATTRIBS(ns);
        t << "/>";
      }
      else if (n->def && n->def->definitionType()==Definition::TypeClass)
      {
        t << "<img ";
        t << FTV_IMGATTRIBS(cl);
        t << "/>";
      }
      else
      {
        t << "<img ";
        t << "id=\"img_" << generateIndentLabel(n,0) 
          << "\" ";
        if (nodeOpened)
          t << FTV_IMGATTRIBS(folderopen);
        else
          t << FTV_IMGATTRIBS(folderclosed);
        t << "onclick=\"toggleFolder('"
          << generateIndentLabel(n,0)
          << "')\"";
        t << "/>";
      }
      generateLink(t,n);
      t << "</td><td class=\"desc\">";
      if (n->def)
      {
        generateBriefDoc(t,n->def);
      }
      t << "</td></tr>" << endl;
      folderId++;
      generateTree(t,n->children,level+1,maxLevel,index);
    }
    else // leaf node
    {
      FileDef *srcRef=0;
      if (n->def && n->def->definitionType()==Definition::TypeFile &&
          ((FileDef*)n->def)->generateSourceFile())
      {
        srcRef = (FileDef*)n->def;
      }
      if (srcRef)
      {
        t << "<a href=\"" << srcRef->getSourceFileBase()
          << Doxygen::htmlFileExtension 
          << "\">";
      }
      if (n->def && n->def->definitionType()==Definition::TypeGroup)
      {
        // no icon
      }
      else if (n->def && n->def->definitionType()==Definition::TypePage)
      {
        // no icon
      }
      else if (n->def && n->def->definitionType()==Definition::TypeNamespace)
      {
        t << "<img ";
        t << FTV_IMGATTRIBS(ns);
        t << "/>";
      }
      else if (n->def && n->def->definitionType()==Definition::TypeClass)
      {
        t << "<img ";
        t << FTV_IMGATTRIBS(cl);
        t << "/>";
      }
      else
      {
        t << "<img ";
        t << FTV_IMGATTRIBS(doc);
        t << "/>";
      }
      if (srcRef)
      {
        t << "</a>";
      }
      generateLink(t,n);
      t << "</td><td class=\"desc\">";
      if (n->def)
      {
        generateBriefDoc(t,n->def);
      }
      t << "</td></tr>" << endl;
    }
  }
}

//-----------------------------------------------------------

struct NavIndexEntry
{
  NavIndexEntry(const QCString &u,const QCString &p) : url(u), path(p) {}
  QCString url;
  QCString path;
};

class NavIndexEntryList : public QList<NavIndexEntry> 
{
  public:
    NavIndexEntryList() : QList<NavIndexEntry>() { setAutoDelete(TRUE); }
   ~NavIndexEntryList() {}
    int compareItems(QCollection::Item item1,QCollection::Item item2)
    {
      // sort list based on url
      return qstrcmp(((NavIndexEntry*)item1)->url,((NavIndexEntry*)item2)->url);
    }
};

static QCString pathToNode(FTVNode *leaf,FTVNode *n)
{
  QCString result;
  if (n->parent)
  {
    result+=pathToNode(leaf,n->parent);
  }
  result+=QCString().setNum(n->index);
  if (leaf!=n) result+=",";
  return result;
}

static bool dupOfParent(const FTVNode *n)
{
  if (n->parent==0) return FALSE;
  if (n->file==n->parent->file) return TRUE;
  return FALSE;
}

static void generateJSLink(FTextStream &t,FTVNode *n)
{
  if (n->file.isEmpty()) // no link
  {
    t << "\"" << convertToJSString(n->name) << "\", null, ";
  }
  else // link into other page
  {
    t << "\"" << convertToJSString(n->name) << "\", \"";
    t << externalRef("",n->ref,TRUE);
    t << node2URL(n);
    t << "\", ";
  }
}

static QCString convertFileId2Var(const QCString &fileId)
{
  QCString varId = fileId;
  int i=varId.findRev('/');
  if (i>=0) varId = varId.mid(i+1);
  return substitute(varId,"-","_");
}

static bool generateJSTree(NavIndexEntryList &navIndex,FTextStream &t, 
                           const QList<FTVNode> &nl,int level,bool &first)
{
  static QCString htmlOutput = Config_getString("HTML_OUTPUT");
  QCString indentStr;
  indentStr.fill(' ',level*2);
  bool found=FALSE;
  QListIterator<FTVNode> nli(nl);
  FTVNode *n;
  for (nli.toFirst();(n=nli.current());++nli)
  {
    // terminate previous entry
    if (!first) t << "," << endl;
    first=FALSE;

    // start entry
    if (!found)
    {
      t << "[" << endl;
    }
    found=TRUE;

    if (n->addToNavIndex) // add entry to the navigation index
    {
      if (n->def && n->def->definitionType()==Definition::TypeFile)
      {
        FileDef *fd = (FileDef*)n->def;
        bool doc,src;
        doc = fileVisibleInIndex(fd,src);
        if (doc)
        {
          navIndex.append(new NavIndexEntry(node2URL(n,TRUE,FALSE),pathToNode(n,n)));
        }
        if (src)
        {
          navIndex.append(new NavIndexEntry(node2URL(n,TRUE,TRUE),pathToNode(n,n)));
        }
      }
      else
      {
        navIndex.append(new NavIndexEntry(node2URL(n),pathToNode(n,n)));
      }
    }

    if (n->separateIndex) // store items in a separate file for dynamic loading
    {
      bool firstChild=TRUE;
      t << indentStr << "  [ ";
      generateJSLink(t,n);
      if (n->children.count()>0) // write children to separate file for dynamic loading
      {
        QCString fileId = n->file;
        if (dupOfParent(n)) fileId+="_dup";
        QFile f(htmlOutput+"/"+fileId+".js");
        if (f.open(IO_WriteOnly))
        {
          FTextStream tt(&f);
          tt << "var " << convertFileId2Var(fileId) << " =" << endl;
          generateJSTree(navIndex,tt,n->children,1,firstChild);
          tt << endl << "];"; 
        }
        t << "\"" << fileId << "\" ]";
      }
      else // no children
      {
        t << "null ]";
      }
    }
    else // show items in this file
    {
      bool firstChild=TRUE;
      t << indentStr << "  [ ";
      generateJSLink(t,n);
      bool emptySection = !generateJSTree(navIndex,t,n->children,level+1,firstChild);
      if (emptySection)
        t << "null ]";
      else
        t << endl << indentStr << "  ] ]"; 
    }
  }
  return found;
}

static void generateJSNavTree(const QList<FTVNode> &nodeList)
{
  QCString htmlOutput = Config_getString("HTML_OUTPUT");
  QFile f(htmlOutput+"/navtree.js");
  NavIndexEntryList navIndex;
  if (f.open(IO_WriteOnly) /*&& fidx.open(IO_WriteOnly)*/)
  {
    //FTextStream tidx(&fidx);
    //tidx << "var NAVTREEINDEX =" << endl;
    //tidx << "{" << endl;
    FTextStream t(&f);
    t << "var NAVTREE =" << endl;
    t << "[" << endl;
    t << "  [ ";
    QCString &projName = Config_getString("PROJECT_NAME");
    if (projName.isEmpty())
    {
      if (Doxygen::mainPage && !Doxygen::mainPage->title().isEmpty()) // Use title of main page as root
      {
        t << "\"" << convertToJSString(Doxygen::mainPage->title()) << "\", ";
      }
      else // Use default section title as root
      {
        LayoutNavEntry *lne = LayoutDocManager::instance().rootNavEntry()->find(LayoutNavEntry::MainPage);
        t << "\"" << convertToJSString(lne->title()) << "\", ";
      }
    }
    else // use PROJECT_NAME as root tree element
    {
      t << "\"" << convertToJSString(projName) << "\", ";
    }
    t << "\"index" << Doxygen::htmlFileExtension << "\", ";

    // add special entry for index page
    navIndex.append(new NavIndexEntry("index"+Doxygen::htmlFileExtension,""));
    // related page index is written as a child of index.html, so add this as well
    navIndex.append(new NavIndexEntry("pages"+Doxygen::htmlFileExtension,""));

    bool first=TRUE;
    generateJSTree(navIndex,t,nodeList,1,first);

    if (first) 
      t << "]" << endl;
    else 
      t << endl << "  ] ]" << endl;
    t << "];" << endl << endl;

    // write the navigation index (and sub-indices)
    navIndex.sort();
    int subIndex=0;
    int elemCount=0;
    const int maxElemCount=250;
    //QFile fidx(htmlOutput+"/navtreeindex.js");
    QFile fsidx(htmlOutput+"/navtreeindex0.js");
    if (/*fidx.open(IO_WriteOnly) &&*/ fsidx.open(IO_WriteOnly))
    {
      //FTextStream tidx(&fidx);
      FTextStream tsidx(&fsidx);
      t << "var NAVTREEINDEX =" << endl;
      t << "[" << endl;
      tsidx << "var NAVTREEINDEX" << subIndex << " =" << endl;
      tsidx << "{" << endl;
      QListIterator<NavIndexEntry> li(navIndex);
      NavIndexEntry *e;
      bool first=TRUE;
      for (li.toFirst();(e=li.current());) // for each entry
      {
        if (elemCount==0)
        {
          if (!first)
          {
            t << "," << endl;
          }
          else
          {
            first=FALSE;
          }
          t << "\"" << e->url << "\"";
        }
        tsidx << "\"" << e->url << "\":[" << e->path << "]";
        ++li;
        if (li.current() && elemCount<maxElemCount-1) tsidx << ","; // not last entry
        tsidx << endl;
  
        elemCount++;
        if (li.current() && elemCount>=maxElemCount) // switch to new sub-index
        {
          tsidx << "};" << endl;
          elemCount=0;
          fsidx.close();
          subIndex++;
          fsidx.setName(htmlOutput+"/navtreeindex"+QCString().setNum(subIndex)+".js");
          if (!fsidx.open(IO_WriteOnly)) break;
          tsidx.setDevice(&fsidx);
          tsidx << "var NAVTREEINDEX" << subIndex << " =" << endl;
          tsidx << "{" << endl;
        }
      }
      tsidx << "};" << endl;
      t << endl << "];" << endl;
    }
    t << endl << "var SYNCONMSG = '"  << theTranslator->trPanelSynchronisationTooltip(FALSE) << "';"; 
    t << endl << "var SYNCOFFMSG = '" << theTranslator->trPanelSynchronisationTooltip(TRUE)  << "';"; 
    t << endl << navtree_script;
  }
}

//-----------------------------------------------------------

// new style images
void FTVHelp::generateTreeViewImages()
{
  QCString dname=Config_getString("HTML_OUTPUT");
  writeColoredImgData(dname,ftv_image_data);
}

// new style scripts
void FTVHelp::generateTreeViewScripts()
{
  QCString htmlOutput = Config_getString("HTML_OUTPUT");

  // generate navtree.js & navtreeindex.js
  generateJSNavTree(m_indentNodes[0]);

  // generate resize.js
  {
    QFile f(htmlOutput+"/resize.js");
    if (f.open(IO_WriteOnly))
    {
      FTextStream t(&f);
      t << resize_script;
    }
  }
  // generate navtree.css
  {
    QFile f(htmlOutput+"/navtree.css");
    if (f.open(IO_WriteOnly))
    {
      FTextStream t(&f);
      t << substitute(
              replaceColorMarkers(navtree_css),
              "$width",
              QCString().setNum(Config_getInt("TREEVIEW_WIDTH"))+"px"
             );
    }
  }
}

// write tree inside page
void FTVHelp::generateTreeViewInline(FTextStream &t)
{
  int preferredNumEntries = Config_getInt("HTML_INDEX_NUM_ENTRIES");
  t << "<div class=\"directory\">\n";
  QListIterator<FTVNode> li(m_indentNodes[0]);
  FTVNode *n;
  int d=1, depth=1;
  for (;(n=li.current());++li)
  {
    if (n->children.count()>0)
    {
      d = n->computeTreeDepth(2);
      if (d>depth) depth=d;
    }
  }
  int preferredDepth = depth;
  // write level selector
  if (depth>1)
  {
    t << "<div class=\"levels\">[";
    t << theTranslator->trDetailLevel(); 
    t << " ";
    int i;
    for (i=1;i<=depth;i++)
    {
      t << "<span onclick=\"javascript:toggleLevel(" << i << ");\">" << i << "</span>";
    }
    t << "]</div>";

    if (preferredNumEntries>0)
    {
      preferredDepth=1;
      for (int i=1;i<=depth;i++)
      {
        int num=0;
        QListIterator<FTVNode> li(m_indentNodes[0]);
        FTVNode *n;
        for (;(n=li.current());++li)
        {
          num+=n->numNodesAtLevel(0,i);
        }
        if (num<=preferredNumEntries)
        {
          preferredDepth=i;
        }
        else
        {
          break;
        }
      }
    }
  }
  //printf("preferred depth=%d\n",preferredDepth);

  t << "<table class=\"directory\">\n";
  int index=0;
  generateTree(t,m_indentNodes[0],0,preferredDepth,index);
  t << "</table>\n";

  t << "</div><!-- directory -->\n";
}

// write old style index.html and tree.html
void FTVHelp::generateTreeView()
{
  generateTreeViewImages();
  generateTreeViewScripts();
}

