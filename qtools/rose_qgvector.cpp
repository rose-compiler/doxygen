/****************************************************************************
** 
**
** Implementation of QGVector class
**
** Created : 930907
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#define	 QGVECTOR_CPP
#include "qgvector.h"
#include "qglist.h"
#include "qstring.h"
#include "qdatastream.h"
#include <stdlib.h>
#define USE_MALLOC				// comment to use new/delete
#undef NEW
#undef DELETE
#if defined(USE_MALLOC)
#define NEW(type,size)	((type*)malloc(size*sizeof(type)))
#define DELETE(array)	(free((char*)array))
#else
#define NEW(type,size)	(new type[size])
#define DELETE(array)	(delete[] array)
#define DONT_USE_REALLOC			// comment to use realloc()
#endif
// NOT REVISED
/*!
  \class QGVector qgvector.h
  \brief The QGVector class is an internal class for implementing Qt
  collection classes.
  QGVector is a strictly internal class that acts as a base class for
  the QVector collection class.
  QGVector has some virtual functions that may be reimplemented in
  subclasses to to customize behavior.
  <ul>
  <li> compareItems() compares two collection/vector items.
  <li> read() reads a collection/vector item from a QDataStream.
  <li> write() writes a collection/vector item to a QDataStream.
  </ul>
*/
/*****************************************************************************
  Default implementation of virtual functions
 *****************************************************************************/
/*!
  This virtual function compares two list items.
  Returns:
  <ul>
  <li> 0 if \a item1 == \a item2
  <li> non-zero if \a item1 != \a item2
  </ul>
  This function returns \e int rather than \e bool so that
  reimplementations can return one of three values and use it to sort
  by:
  <ul>
  <li> 0 if \e item1 == \e item2
  <li> \> 0 (positive integer) if \a item1 \> \a item2
  <li> \< 0 (negative integer) if \a item1 \< \a item2
  </ul>
  The QVector::sort() and QVector::bsearch() functions require that
  compareItems() is implemented as described here.
  This function should not modify the vector because some const
  functions call compareItems().
*/

int QGVector::compareItems(Item d1,Item d2)
{
// compare pointers
  return (d1 != d2);
}
#ifndef QT_NO_DATASTREAM
/*!
  Reads a collection/vector item from the stream \a s and returns a reference
  to the stream.
  The default implementation sets \e item to 0.
  \sa write()
*/

QDataStream &QGVector::read(class QDataStream &s,Item &d)
// read item from stream
{
  d = 0;
  return s;
}
/*!
  Writes a collection/vector item to the stream \a s and returns a reference
  to the stream.
  The default implementation does nothing.
  \sa read()
*/

QDataStream &QGVector::write(class QDataStream &s,Item ) const
// write item to stream
{
  return s;
}
#endif // QT_NO_DATASTREAM
/*****************************************************************************
  QGVector member functions
 *****************************************************************************/
/*!
  \internal
*/
// create empty vector

QGVector::QGVector()
{
  (this) -> vec = 0;
  (this) -> len = (this) -> numItems = 0;
}
/*!
  \internal
*/
// create vectors with nullptrs

QGVector::QGVector(uint size)
{
  (this) -> len = size;
  (this) -> numItems = 0;
// zero length
  if ((this) -> len == 0) {
    (this) -> vec = 0;
    return ;
  }
  (this) -> vec = ((Item *)(malloc(((this) -> len) * sizeof(Item ))));
  qt_check_pointer((this) -> vec == 0,"qgvector.cpp",172);
// fill with nulls
  memset(((void *)((this) -> vec)),0,((this) -> len) * sizeof(Item ));
}
/*!
  \internal
*/
// make copy of other vector

QGVector::QGVector(const class ::QGVector &a) : QCollection(a)
{
  (this) -> len = a . len;
  (this) -> numItems = a . numItems;
  (this) -> vec = ((Item *)(malloc(((this) -> len) * sizeof(Item ))));
  qt_check_pointer((this) -> vec == 0,"qgvector.cpp",186);
  for (uint i = 0; i < (this) -> len; i++) {
    (this) -> vec[i] = (a . vec[i]?(this) ->  newItem (a . vec[i]) : 0);
    qt_check_pointer((this) -> vec[i] == 0,"qgvector.cpp",189);
  }
}
/*!
  \internal
*/

QGVector::~QGVector()
{
  (this) ->  clear ();
}
/*!
  \internal
*/

QGVector &QGVector::operator=(const class ::QGVector &v)
// assign from other vector
{
// first delete old vector
  (this) ->  clear ();
  (this) -> len = v . len;
  (this) -> numItems = v . numItems;
// create new vector
  (this) -> vec = ((Item *)(malloc(((this) -> len) * sizeof(Item ))));
  qt_check_pointer((this) -> vec == 0,"qgvector.cpp",213);
// copy elements
  for (uint i = 0; i < (this) -> len; i++) {
    (this) -> vec[i] = (v . vec[i]?(this) ->  newItem (v . vec[i]) : 0);
    qt_check_pointer((this) -> vec[i] == 0,"qgvector.cpp",216);
  }
  return  *(this);
}
/*!
  \fn Item *QGVector::data() const
  \internal
*/
/*!
  \fn uint QGVector::size() const
  \internal
*/
/*!
  \fn uint QGVector::count() const
  \internal
*/
/*!
  \fn Item QGVector::at( uint index ) const
  \internal
*/
/*!
  \internal
*/
// insert item at index

bool QGVector::insert(uint index,Item d)
{
#if defined(CHECK_RANGE)
// range error
  if (index >= (this) -> len) {
    qWarning("QGVector::insert: Index %d out of range",index);
    return FALSE;
  }
#endif
// remove old item
  if ((this) -> vec[index]) {
    (this) ->  deleteItem ((this) -> vec[index]);
    (this) -> numItems--;
  }
  if (d) {
    (this) -> vec[index] = (this) ->  newItem (d);
    qt_check_pointer((this) -> vec[index] == 0,"qgvector.cpp",260);
    (this) -> numItems++;
    return (this) -> vec[index] != 0;
  }
  else {
// reset item
    (this) -> vec[index] = 0;
  }
  return TRUE;
}
/*!
  \internal
*/
// remove item at index

bool QGVector::remove(uint index)
{
#if defined(CHECK_RANGE)
// range error
  if (index >= (this) -> len) {
    qWarning("QGVector::remove: Index %d out of range",index);
    return FALSE;
  }
#endif
// valid item
  if ((this) -> vec[index]) {
// delete it
    (this) ->  deleteItem ((this) -> vec[index]);
// reset pointer
    (this) -> vec[index] = 0;
    (this) -> numItems--;
  }
  return TRUE;
}
/*!
  \internal
*/
// take out item

QCollection::Item QGVector::take(uint index)
{
#if defined(CHECK_RANGE)
// range error
  if (index >= (this) -> len) {
    qWarning("QGVector::take: Index %d out of range",index);
    return 0;
  }
#endif
// don't delete item
  Item d = (this) -> vec[index];
  if (d) {
    (this) -> numItems--;
  }
  (this) -> vec[index] = 0;
  return d;
}
/*!
  \internal
*/
// clear vector

void QGVector::clear()
{
  if (((this) -> vec)) {
// delete each item
    for (uint i = 0; i < (this) -> len; i++) {
      if ((this) -> vec[i]) {
        (this) ->  deleteItem ((this) -> vec[i]);
      }
    }
    free(((char *)((this) -> vec)));
    (this) -> vec = 0;
    (this) -> len = (this) -> numItems = 0;
  }
}
/*!
  \internal
*/
// resize array

bool QGVector::resize(uint newsize)
{
// nothing to do
  if (newsize == (this) -> len) {
    return TRUE;
  }
// existing data
  if (((this) -> vec)) {
// shrink vector
    if (newsize < (this) -> len) {
      uint i = newsize;
// delete lost items
      while(i < (this) -> len){
        if ((this) -> vec[i]) {
          (this) ->  deleteItem ((this) -> vec[i]);
          (this) -> numItems--;
        }
        i++;
      }
    }
// vector becomes empty
    if (newsize == 0) {
      free(((char *)((this) -> vec)));
      (this) -> vec = 0;
      (this) -> len = (this) -> numItems = 0;
      return TRUE;
    }
#if defined(DONT_USE_REALLOC)
// manual realloc
#else
    (this) -> vec = ((Item *)(realloc(((char *)((this) -> vec)),newsize * sizeof(Item ))));
#endif
// create new vector
  }
  else {
    (this) -> vec = ((Item *)(malloc(newsize * sizeof(Item ))));
    (this) -> len = (this) -> numItems = 0;
  }
  qt_check_pointer((this) -> vec == 0,"qgvector.cpp",363);
// no memory
  if (!((this) -> vec)) {
    return FALSE;
  }
// init extra space added
  if (newsize > (this) -> len) {
    memset(((void *)(&(this) -> vec[(this) -> len])),0,(newsize - (this) -> len) * sizeof(Item ));
  }
  (this) -> len = newsize;
  return TRUE;
}
/*!
  \internal
*/
// resize and fill vector

bool QGVector::fill(Item d,int flen)
{
  if (flen < 0) {
// default: use vector length
    flen = ((this) -> len);
  }
  else {
    if (!(this) ->  resize (flen)) {
      return FALSE;
    }
  }
// insert d at every index
  for (uint i = 0; i < ((uint )flen); i++) 
    (this) ->  insert (i,d);
  return TRUE;
}
// current sort vector
static class QGVector *sort_vec = 0;
#if defined(Q_C_CALLBACKS)
extern "C" {
#endif

static int cmp_vec(const void *n1,const void *n2)
{
  return sort_vec ->  compareItems ( *((QCollection::Item *)n1), *((QCollection::Item *)n2));
}
#if defined(Q_C_CALLBACKS)
}
#endif
/*!
  \internal
*/
// sort vector

void QGVector::sort()
{
// no elements
  if ((this) ->  count () == 0) {
    return ;
  }
  register Item *start = &(this) -> vec[0];
  register Item *end = &(this) -> vec[(this) -> len - 1];
  Item tmp;
// put all zero elements behind
  while(TRUE){
    while(start < end &&  *start != 0)
      start++;
    while(end > start &&  *end == 0)
      end--;
    if (start < end) {
      tmp =  *start;
       *start =  *end;
       *end = tmp;
    }
    else {
      break; 
    }
  }
  sort_vec = ((class ::QGVector *)(this));
  qsort(((this) -> vec),((this) ->  count ()),sizeof(Item ),cmp_vec);
  sort_vec = 0;
}
/*!
  \internal
*/
// binary search; when sorted

int QGVector::bsearch(Item d) const
{
  if (!((this) -> len)) {
    return -1;
  }
  if (!d) {
#if defined(CHECK_NULL)
    qWarning("QGVector::bsearch: Cannot search for null object");
#endif
    return -1;
  }
  int n1 = 0;
  int n2 = ((this) -> len - 1);
  int mid = 0;
  bool found = FALSE;
  while(n1 <= n2){
    int res;
    mid = (n1 + n2) / 2;
// null item greater
    if ((this) -> vec[mid] == 0) {
      res = -1;
    }
    else {
      res = ((class ::QGVector *)(this)) ->  compareItems (d,(this) -> vec[mid]);
    }
    if (res < 0) {
      n2 = mid - 1;
    }
    else {
      if (res > 0) {
        n1 = mid + 1;
      }
      else 
// found it
{
        found = TRUE;
        break; 
      }
    }
  }
  if (!found) {
    return -1;
  }
// search to first of equal items
  while(mid - 1 >= 0 && !(((class ::QGVector *)(this)) ->  compareItems (d,(this) -> vec[mid - 1])))
    mid--;
  return mid;
}
/*!
  \internal
*/
// find exact item in vector

int QGVector::findRef(Item d,uint index) const
{
#if defined(CHECK_RANGE)
// range error
  if (index >= (this) -> len) {
    qWarning("QGVector::findRef: Index %d out of range",index);
    return -1;
  }
#endif
  for (uint i = index; i < (this) -> len; i++) {
    if ((this) -> vec[i] == d) {
      return i;
    }
  }
  return -1;
}
/*!
  \internal
*/
// find equal item in vector

int QGVector::find(Item d,uint index) const
{
#if defined(CHECK_RANGE)
// range error
  if (index >= (this) -> len) {
    qWarning("QGVector::find: Index %d out of range",index);
    return -1;
  }
#endif
  for (uint i = index; i < (this) -> len; i++) {
// found null item
    if ((this) -> vec[i] == 0 && d == 0) {
      return i;
    }
    if ((this) -> vec[i] && ((class ::QGVector *)(this)) ->  compareItems ((this) -> vec[i],d) == 0) {
      return i;
    }
  }
  return -1;
}
/*!
  \internal
*/
// get number of exact matches

uint QGVector::containsRef(Item d) const
{
  uint count = 0;
  for (uint i = 0; i < (this) -> len; i++) {
    if ((this) -> vec[i] == d) {
      count++;
    }
  }
  return count;
}
/*!
  \internal
*/
// get number of equal matches

uint QGVector::contains(Item d) const
{
  uint count = 0;
  for (uint i = 0; i < (this) -> len; i++) {
// count null items
    if ((this) -> vec[i] == 0 && d == 0) {
      count++;
    }
    if ((this) -> vec[i] && ((class ::QGVector *)(this)) ->  compareItems ((this) -> vec[i],d) == 0) {
      count++;
    }
  }
  return count;
}
/*!
  \internal
*/
// insert and grow if necessary

bool QGVector::insertExpand(uint index,Item d)
{
  if (index >= (this) -> len) {
// no memory
    if (!(this) ->  resize (index + 1)) {
      return FALSE;
    }
  }
  (this) ->  insert (index,d);
  return TRUE;
}
/*!
  \internal
*/
// store items in list

void QGVector::toList(class QGList *list) const
{
  list ->  clear ();
  for (uint i = 0; i < (this) -> len; i++) {
    if ((this) -> vec[i]) {
      list ->  append ((this) -> vec[i]);
    }
  }
}

void QGVector::warningIndexRange(uint i)
{
#if defined(CHECK_RANGE)
  qWarning("QGVector::operator[]: Index %d out of range",i);
#else
#endif
}
/*****************************************************************************
  QGVector stream functions
 *****************************************************************************/
#ifndef QT_NO_DATASTREAM

class QDataStream &operator>>(class QDataStream &s,class QGVector &vec)
// read vector
{
  return vec .  read (s);
}

class QDataStream &operator<<(class QDataStream &s,const class QGVector &vec)
// write vector
{
  return vec .  write (s);
}
/*!
  \internal
*/
// read vector from stream

QDataStream &QGVector::read(class QDataStream &s)
{
  uint num;
// read number of items
  s >> num;
// clear vector
  (this) ->  clear ();
  (this) ->  resize (num);
// read all items
  for (uint i = 0; i < num; i++) {
    Item d;
    (this) ->  read (s,d);
    qt_check_pointer(d == 0,"qgvector.cpp",615);
// no memory
    if (!d) {
      break; 
    }
    (this) -> vec[i] = d;
  }
  return s;
}
/*!
  \internal
*/

QDataStream &QGVector::write(class QDataStream &s) const
// write vector to stream
{
  uint num = (this) ->  count ();
// number of items to write
  s << num;
  num = (this) ->  size ();
// write non-null items
  for (uint i = 0; i < num; i++) {
    if ((this) -> vec[i]) {
      (this) ->  write (s,(this) -> vec[i]);
    }
  }
  return s;
}
#endif // QT_NO_DATASTREAM
