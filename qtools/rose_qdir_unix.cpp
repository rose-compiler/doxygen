/****************************************************************************
** 
**
** Implementation of QDirclass
**
** Created : 950628
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses for Unix/X11 or for Qt/Embedded may use this file in accordance
** with the Qt Commercial License Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qglobal.h"
#include "qdir.h"
#ifndef QT_NO_DIR
#include "qfileinfo.h"
#include "qfiledefs_p.h"
#include "qregexp.h"
#include "qstringlist.h"
#include <stdlib.h>
#include <ctype.h>
extern class QStringList qt_makeFilterList(const class QString &filter);
extern int qt_cmp_si_sortSpec;
#if defined(Q_C_CALLBACKS)
extern "C" {
#endif
extern int qt_cmp_si(const void *,const void *);
#if defined(Q_C_CALLBACKS)
}
#endif

void QDir::slashify(class QString &)
{
}

QString QDir::homeDirPath()
{
  class QString d;
  d = QFile:: decodeName ((getenv("HOME")));
   slashify (d);
  if (d .  isNull ()) {
    d =  rootDirPath ();
  }
  return (d);
}

QString QDir::canonicalPath() const
{
  class QString r;
  char cur[4096];
  char tmp[4096];
  if ((getcwd(cur,4096))) {
    if (chdir((QFile:: encodeName ((this) -> dPath) .  operator const char * ())) >= 0) {
      if ((getcwd(tmp,4096))) {
        r = QFile:: decodeName (tmp);
      }
      (void )(chdir(cur));
    }
  }
   slashify (r);
  return (r);
}

bool QDir::mkdir(const class QString &dirName,bool acceptAbsPath) const
{
  return ::mkdir((QFile:: encodeName ((this) ->  filePath (dirName,acceptAbsPath)) .  operator const char * ()),0777) == 0;
}

bool QDir::rmdir(const class QString &dirName,bool acceptAbsPath) const
{
  return ::rmdir((QFile:: encodeName ((this) ->  filePath (dirName,acceptAbsPath)) .  operator const char * ())) == 0;
}

bool QDir::isReadable() const
{
  return access((QFile:: encodeName ((this) -> dPath) .  operator const char * ()),4 | 1) == 0;
}

bool QDir::isRoot() const
{
  return (this) -> dPath==QString:: fromLatin1 ("/");
}

bool QDir::rename(const class QString &name,const class QString &newName,bool acceptAbsPaths)
{
  if (name .  isEmpty () || newName .  isEmpty ()) {
#if defined(CHECK_NULL)
    qWarning("QDir::rename: Empty or null file name(s)");
#endif
    return FALSE;
  }
  class QString fn1 = (this) ->  filePath (name,acceptAbsPaths);
  class QString fn2 = (this) ->  filePath (newName,acceptAbsPaths);
  return ::rename((QFile:: encodeName (fn1) .  operator const char * ()),(QFile:: encodeName (fn2) .  operator const char * ())) == 0;
}

bool QDir::setCurrent(const class QString &path)
{
  int r;
  r = chdir((QFile:: encodeName (path) .  operator const char * ()));
  return r >= 0;
}

QString QDir::currentDirPath()
{
  class QString result;
  struct stat st;
  if (stat(".",&st) == 0) {
    char currentName[4096];
    if (getcwd(currentName,4096) != 0) {
      result = QFile:: decodeName (currentName);
    }
#if defined(DEBUG)
    if (result .  isNull ()) {
      qWarning("QDir::currentDirPath: getcwd() failed");
    }
#endif
  }
  else {
#if defined(DEBUG)
    qWarning("QDir::currentDirPath: stat(\".\") failed");
#endif
  }
   slashify (result);
  return (result);
}

QString QDir::rootDirPath()
{
  class QString d = QString:: fromLatin1 ("/");
  return (d);
}

bool QDir::isRelativePath(const class QString &path)
{
  int len = (path .  length ());
  if (len == 0) {
    return TRUE;
  }
  return (path[0]!='/');
}

bool QDir::readDirEntries(const class QString &nameFilter,int filterSpec,int sortSpec)
{
  int i;
  if (!((this) -> fList)) {
    (this) -> fList = (new QStringList ());
    qt_check_pointer((this) -> fList == 0,"qdir_unix.cpp",183);
    (this) -> fiList = (new QFileInfoList ());
    qt_check_pointer((this) -> fiList == 0,"qdir_unix.cpp",185);
    ((this) -> fiList) ->  setAutoDelete (TRUE);
  }
  else {
    ((this) -> fList) ->  clear ();
    (this) -> fiList ->  clear ();
  }
  class QStringList filters = qt_makeFilterList(nameFilter);
  bool doDirs = (filterSpec & Dirs) != 0;
  bool doFiles = (filterSpec & Files) != 0;
  bool noSymLinks = (filterSpec & NoSymLinks) != 0;
  bool doReadable = (filterSpec & Readable) != 0;
  bool doWritable = (filterSpec & Writable) != 0;
  bool doExecable = (filterSpec & Executable) != 0;
  bool doHidden = (filterSpec & Hidden) != 0;
#if defined(_OS_OS2EMX_)
//QRegExp   wc( nameFilter, FALSE, TRUE );	// wild card, case insensitive
#else
//QRegExp   wc( nameFilter, TRUE, TRUE );	// wild card, case sensitive
#endif
  class QFileInfo fi;
  DIR *dir;
  struct dirent *file;
  dir = opendir((QFile:: encodeName ((this) -> dPath) .  operator const char * ()));
  if (!dir) {
#if defined(CHECK_NULL)
    qWarning("QDir::readDirEntries: Cannot read the directory: %s",QFile:: encodeName ((this) -> dPath) .  data ());
#endif
    return FALSE;
  }
  while((file = readdir(dir))){
    class QString fn = QFile:: decodeName ((file -> dirent::d_name));
    fi .  setFile ( *(this),fn);
    if (! match (filters,fn) && !(((this) -> allDirs) && fi .  isDir ())) {
      continue; 
    }
    if (doDirs && fi .  isDir () || doFiles && fi .  isFile ()) {
      if (noSymLinks && fi .  isSymLink ()) {
        continue; 
      }
      if ((filterSpec & RWEMask) != 0) {
        if (doReadable && !fi .  isReadable () || doWritable && !fi .  isWritable () || doExecable && !fi .  isExecutable ()) {
          continue; 
        }
      }
      if (!doHidden && ((fn[0] .  operator QChar ())=='.') && fn!=QString:: fromLatin1 (".") && fn!=QString:: fromLatin1 ("..")) {
        continue; 
      }
      (this) -> fiList ->  append ((new QFileInfo (fi)));
    }
  }
  if (closedir(dir) != 0) {
#if defined(CHECK_NULL)
    qWarning("QDir::readDirEntries: Cannot close the directory: %s",(this) -> dPath .  local8Bit () .  data ());
#endif
  }
// Sort...
  if ((((this) -> fiList) ->  count ())) {
    struct QDirSortItem *si = new QDirSortItem [(((this) -> fiList) ->  count ()) * 16UL];
    class QFileInfo *itm;
    i = 0;
    for (itm = (this) -> fiList ->  first (); itm; itm = (this) -> fiList ->  next ()) 
      si[i++] . QDirSortItem::item = itm;
    qt_cmp_si_sortSpec = sortSpec;
    qsort(si,i,sizeof(si[0]),qt_cmp_si);
// put them back in the list
    ((this) -> fiList) ->  setAutoDelete (FALSE);
    (this) -> fiList ->  clear ();
    int j;
    for (j = 0; j < i; j++) {
      (this) -> fiList ->  append (si[j] . QDirSortItem::item);
      ((this) -> fList) ->  append (si[j] . QDirSortItem::item ->  fileName ());
    }
    delete []si;
    ((this) -> fiList) ->  setAutoDelete (TRUE);
  }
  if (filterSpec == ((enum FilterSpec )((this) -> filtS)) && sortSpec == ((enum SortSpec )((this) -> sortS)) && nameFilter==(this) -> nameFilt) {
    (this) -> dirty = FALSE;
  }
  else {
    (this) -> dirty = TRUE;
  }
  return TRUE;
}

const QFileInfoList *QDir::drives()
{
// at most one instance of QFileInfoList is leaked, and this variable
// points to that list
  static QFileInfoList *knownMemoryLeak = 0;
  if (!knownMemoryLeak) {
    knownMemoryLeak = (new QFileInfoList ());
// non-win32 versions both use just one root directory
    knownMemoryLeak ->  append ((new QFileInfo ( rootDirPath ())));
  }
  return knownMemoryLeak;
}
#endif //QT_NO_DIR
