/****************************************************************************
** 
**
** Implementation of QFileInfo class
**
** Created : 950628
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses for Unix/X11 or for Qt/Embedded may use this file in accordance
** with the Qt Commercial License Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qglobal.h"
#include "qfile.h"
#include "qfiledefs_p.h"
#if (defined(_OS_MAC_) && (!defined(_OS_UNIX_))) || defined(_OS_MSDOS_) || defined(_OS_WIN32_) || defined(_OS_OS2_) || defined(_OS_CYGWIN_)
# define HAS_TEXT_FILEMODE			// has translate/text filemode
#endif
#if defined(O_NONBLOCK)
# define HAS_ASYNC_FILEMODE
# define OPEN_ASYNC O_NONBLOCK
#elif defined(O_NDELAY)
# define HAS_ASYNC_FILEMODE
# define OPEN_ASYNC O_NDELAY
#endif

bool qt_file_access(const class QString &fn,int t)
{
  if (fn .  isEmpty ()) {
    return FALSE;
  }
  return access((QFile:: encodeName (fn) .  operator const char * ()),t) == 0;
}
/*!
  Removes the file \a fileName.
  Returns TRUE if successful, otherwise FALSE.
*/

bool QFile::remove(const class QString &fileName)
{
  if (fileName .  isEmpty ()) {
#if defined(CHECK_NULL)
    qWarning("QFile::remove: Empty or null file name");
#endif
    return FALSE;
  }
  return unlink(( encodeName (fileName) .  operator const char * ())) == 0;
// unlink more common in UNIX
}
#if defined(O_NONBLOCK)
# define HAS_ASYNC_FILEMODE
# define OPEN_ASYNC O_NONBLOCK
#elif defined(O_NDELAY)
# define HAS_ASYNC_FILEMODE
# define OPEN_ASYNC O_NDELAY
#endif
/*!
  Opens the file specified by the file name currently set, using the mode \e m.
  Returns TRUE if successful, otherwise FALSE.
  The mode parameter \e m must be a combination of the following flags:
  <ul>
  <li>\c IO_Raw specified raw (non-buffered) file access.
  <li>\c IO_ReadOnly opens the file in read-only mode.
  <li>\c IO_WriteOnly opens the file in write-only mode (and truncates).
  <li>\c IO_ReadWrite opens the file in read/write mode, equivalent to
  \c (IO_ReadOnly|IO_WriteOnly).
  <li>\c IO_Append opens the file in append mode. This mode is very useful
  when you want to write something to a log file. The file index is set to
  the end of the file. Note that the result is undefined if you position the
  file index manually using at() in append mode.
  <li>\c IO_Truncate truncates the file.
  <li>\c IO_Translate enables carriage returns and linefeed translation
  for text files under MS-DOS, Windows and OS/2.
  </ul>
  The raw access mode is best when I/O is block-operated using 4kB block size
  or greater. Buffered access works better when reading small portions of
  data at a time.
  <strong>Important:</strong> When working with buffered files, data may
  not be written to the file at once. Call \link flush() flush\endlink
  to make sure the data is really written.
  \warning We have experienced problems with some C libraries when a buffered
  file is opened for both reading and writing. If a read operation takes place
  immediately after a write operation, the read buffer contains garbage data.
  Worse, the same garbage is written to the file. Calling flush() before
  readBlock() solved this problem.
  If the file does not exist and \c IO_WriteOnly or \c IO_ReadWrite is
  specified, it is created.
  Example:
  \code
    QFile f1( "/tmp/data.bin" );
    QFile f2( "readme.txt" );
    f1.open( IO_Raw | IO_ReadWrite | IO_Append );
    f2.open( IO_ReadOnly | IO_Translate );
  \endcode
  \sa name(), close(), isOpen(), flush()
*/

bool QFile::open(int m)
{
// file already open
  if ((this) ->  isOpen ()) {
#if defined(CHECK_STATE)
    qWarning("QFile::open: File already open");
#endif
    return FALSE;
  }
// no file name defined
  if ((this) -> fn .  isNull ()) {
#if defined(CHECK_NULL)
    qWarning("QFile::open: No file name specified");
#endif
    return FALSE;
  }
// reset params
  (this) ->  init ();
  (this) ->  setMode (m);
  if (!((this) ->  isReadable () || (this) ->  isWritable ())) {
#if defined(CHECK_RANGE)
    qWarning("QFile::open: File access not specified");
#endif
    return FALSE;
  }
  bool ok = TRUE;
  struct stat st;
// raw file I/O
  if ((this) ->  isRaw ()) {
    int oflags = 00;
    if ((this) ->  isReadable () && (this) ->  isWritable ()) {
      oflags = 0x0002;
    }
    else {
      if ((this) ->  isWritable ()) {
        oflags = 0x0001;
      }
    }
// append to end of file?
    if (((this) ->  flags () & 0x0004)) {
      if (((this) ->  flags () & 0x0008)) {
        oflags |= 0x0040 | 0x0200;
      }
      else {
        oflags |= 02000 | 0x0040;
      }
// append implies write
      (this) ->  setFlags ((this) ->  flags () | 0x0002);
// create/trunc if writable
    }
    else {
      if ((this) ->  isWritable ()) {
        if (((this) ->  flags () & 0x0008)) {
          oflags |= 0x0040 | 0x0200;
        }
        else {
          oflags |= 0x0040;
        }
      }
    }
#if defined(HAS_TEXT_FILEMODE)
#ifdef __CYGWIN__
/* Do nothing, allowing the Cygwin mount mode to take effect. */
#else
#endif
#endif
#if defined(HAS_ASYNC_FILEMODE)
    if ((this) ->  isAsynchronous ()) {
      oflags |= 04000;
    }
#endif
    (this) -> fd = ::open(( encodeName ((this) -> fn) .  operator const char * ()),oflags,0666);
// open successful
    if ((this) -> fd != -1) {
// get the stat for later usage
      fstat((this) -> fd,&st);
    }
    else {
      ok = FALSE;
    }
// buffered file I/O
  }
  else {
    class QCString perm;
    char perm2[4];
    bool try_create = FALSE;
// append to end of file?
    if (((this) ->  flags () & 0x0004)) {
// append implies write
      (this) ->  setFlags ((this) ->  flags () | 0x0002);
      perm = ((this) ->  isReadable ()?"a+" : "a");
    }
    else {
      if ((this) ->  isReadWrite ()) {
        if (((this) ->  flags () & 0x0008)) {
          perm = "w+";
        }
        else {
          perm = "r+";
// try to create if not exists
          try_create = TRUE;
        }
      }
      else {
        if ((this) ->  isReadable ()) {
          perm = "r";
        }
        else {
          if ((this) ->  isWritable ()) {
            perm = "w";
          }
        }
      }
    }
    qstrcpy(perm2,(perm .  operator const char * ()));
#if defined(HAS_TEXT_FILEMODE)
#ifdef __CYGWIN__
/* Do nothing, allowing the Cygwin mount mode to take effect. */
#else
#endif
#endif
// At most twice
    while(1){
      (this) -> fh = fopen(( encodeName ((this) -> fn) .  operator const char * ()),perm2);
      if (!((this) -> fh) && try_create) {
// try "w+" instead of "r+"
        perm2[0] = 'w';
        try_create = FALSE;
      }
      else {
        break; 
      }
    }
    if (((this) -> fh)) {
// get the stat for later usage
      fstat(fileno((this) -> fh),&st);
    }
    else {
      ok = FALSE;
    }
  }
  if (ok) {
    (this) ->  setState (0x1000);
// on successful open the file stat was got; now test what type
// of file we have
    if ((st . stat::st_mode & 0xf000) != 0100000) {
// non-seekable
      (this) ->  setType (0x0200);
      (this) -> length = 2147483647;
      (this) -> ioIndex = (((this) ->  flags () & 0x0004) == 0?0 : (this) -> length);
    }
    else {
      (this) -> length = ((int )st . stat::st_size);
      (this) -> ioIndex = (((this) ->  flags () & 0x0004) == 0?0 : (this) -> length);
      if (!((this) ->  flags () & 0x0008) && (this) -> length == 0 && (this) ->  isReadable ()) {
// try if you can read from it (if you can, it's a sequential
// device; e.g. a file in the /proc filesystem)
        int c = (this) ->  getch ();
        if (c != -1) {
          (this) ->  ungetch (c);
          (this) ->  setType (0x0200);
          (this) -> length = 2147483647;
        }
      }
    }
  }
  else {
    (this) ->  init ();
// no more file handles/descrs
    if ( *__errno_location() == 24) {
      (this) ->  setStatus (4);
    }
    else {
      (this) ->  setStatus (5);
    }
  }
  return ok;
}
/*!
  Opens a file in the mode \e m using an existing file handle \e f.
  Returns TRUE if successful, otherwise FALSE.
  Example:
  \code
    #include <stdio.h>
    void printError( const char* msg )
    {
	QFile f;
	f.open( IO_WriteOnly, stderr );
	f.writeBlock( msg, qstrlen(msg) );	// write to stderr
	f.close();
    }
  \endcode
  When a QFile is opened using this function, close() does not actually
  close the file, only flushes it.
  \warning If \e f is \c stdin, \c stdout, \c stderr, you may not
  be able to seek.  See QIODevice::isSequentialAccess() for more
  information.
  \sa close()
*/

bool QFile::open(int m,FILE *f)
{
  if ((this) ->  isOpen ()) {
#if defined(CHECK_RANGE)
    qWarning("QFile::open: File already open");
#endif
    return FALSE;
  }
  (this) ->  init ();
  (this) ->  setMode (m & ~0x0040);
  (this) ->  setState (0x1000);
  (this) -> fh = f;
  (this) -> ext_f = TRUE;
  struct stat st;
  fstat(fileno((this) -> fh),&st);
  (this) -> ioIndex = ((int )(ftell((this) -> fh)));
//stdin is non seekable
  if ((st . stat::st_mode & 0xf000) != 0100000 || f == stdin) {
// non-seekable
    (this) ->  setType (0x0200);
    (this) -> length = 2147483647;
  }
  else {
    (this) -> length = ((int )st . stat::st_size);
    if (!((this) ->  flags () & 0x0008) && (this) -> length == 0 && (this) ->  isReadable ()) {
// try if you can read from it (if you can, it's a sequential
// device; e.g. a file in the /proc filesystem)
      int c = (this) ->  getch ();
      if (c != -1) {
        (this) ->  ungetch (c);
        (this) ->  setType (0x0200);
        (this) -> length = 2147483647;
      }
    }
  }
  return TRUE;
}
/*!
  Opens a file in the mode \e m using an existing file descriptor \e f.
  Returns TRUE if successful, otherwise FALSE.
  When a QFile is opened using this function, close() does not actually
  close the file.
  \warning If \e f is one of 0 (stdin), 1 (stdout) or 2 (stderr), you may not
  be able to seek. size() is set to \c INT_MAX (in limits.h).
  \sa close()
*/

bool QFile::open(int m,int f)
{
  if ((this) ->  isOpen ()) {
#if defined(CHECK_RANGE)
    qWarning("QFile::open: File already open");
#endif
    return FALSE;
  }
  (this) ->  init ();
  (this) ->  setMode (m | 0x0040);
  (this) ->  setState (0x1000);
  (this) -> fd = f;
  (this) -> ext_f = TRUE;
  struct stat st;
  fstat((this) -> fd,&st);
  (this) -> ioIndex = ((int )(lseek((this) -> fd,0,1)));
// stdin is not seekable...
  if ((st . stat::st_mode & 0xf000) != 0100000 || f == 0) {
// non-seekable
    (this) ->  setType (0x0200);
    (this) -> length = 2147483647;
  }
  else {
    (this) -> length = ((int )st . stat::st_size);
    if ((this) -> length == 0 && (this) ->  isReadable ()) {
// try if you can read from it (if you can, it's a sequential
// device; e.g. a file in the /proc filesystem)
      int c = (this) ->  getch ();
      if (c != -1) {
        (this) ->  ungetch (c);
        (this) ->  setType (0x0200);
        (this) -> length = 2147483647;
      }
      (this) ->  resetStatus ();
    }
  }
  return TRUE;
}
/*!
  Returns the file size.
  \sa at()
*/

uint QFile::size() const
{
  struct stat st;
  if ((this) ->  isOpen ()) {
    fstat((((this) -> fh)?fileno((this) -> fh) : (this) -> fd),&st);
  }
  else {
    stat(( encodeName ((this) -> fn) .  operator const char * ()),&st);
  }
  return (uint )st . stat::st_size;
}
/*!
  \fn int QFile::at() const
  Returns the file index.
  \sa size()
*/
/*!
  Sets the file index to \e pos. Returns TRUE if successful, otherwise FALSE.
  Example:
  \code
    QFile f( "data.bin" );
    f.open( IO_ReadOnly );			// index set to 0
    f.at( 100 );				// set index to 100
    f.at( f.at()+50 );				// set index to 150
    f.at( f.size()-80 );			// set index to 80 before EOF
    f.close();
  \endcode
  \warning The result is undefined if the file was \link open() opened\endlink
  using the \c IO_Append specifier.
  \sa size(), open()
*/

bool QFile::at(int pos)
{
  if (!(this) ->  isOpen ()) {
#if defined(CHECK_STATE)
    qWarning("QFile::at: File is not open");
#endif
    return FALSE;
  }
  bool ok;
// raw file
  if ((this) ->  isRaw ()) {
    pos = ((int )(lseek((this) -> fd,pos,0)));
    ok = pos != -1;
// buffered file
  }
  else {
    ok = fseek((this) -> fh,pos,0) == 0;
  }
  if (ok) {
    (this) -> ioIndex = pos;
  }
  else {
#if defined(CHECK_RANGE)
    qWarning("QFile::at: Cannot set file position %d",pos);
  }
#endif
  return ok;
}
/*!
  Reads at most \e len bytes from the file into \e p and returns the
  number of bytes actually read.
  Returns -1 if a serious error occurred.
  \warning We have experienced problems with some C libraries when a buffered
  file is opened for both reading and writing. If a read operation takes place
  immediately after a write operation, the read buffer contains garbage data.
  Worse, the same garbage is written to the file. Calling flush() before
  readBlock() solved this problem.
  \sa writeBlock()
*/

int QFile::readBlock(char *p,uint len)
{
#if defined(CHECK_NULL)
  if (!p) {
    qWarning("QFile::readBlock: Null pointer error");
  }
#endif
#if defined(CHECK_STATE)
// file not open
  if (!(this) ->  isOpen ()) {
    qWarning("QFile::readBlock: File not open");
    return -1;
  }
// reading not permitted
  if (!(this) ->  isReadable ()) {
    qWarning("QFile::readBlock: Read operation not permitted");
    return -1;
  }
#endif
// number of bytes read
  int nread = 0;
  if (!(this) -> ungetchBuffer .  isEmpty ()) {
// need to add these to the returned string.
    int l = ((this) -> ungetchBuffer .  length ());
    while(nread < l){
       *p = (this) -> ungetchBuffer[(l - nread - 1)];
      p++;
      nread++;
    }
    (this) -> ungetchBuffer .  truncate ((l - nread));
  }
  if (nread < ((int )len)) {
// raw file
    if ((this) ->  isRaw ()) {
      nread += ((int )(read((this) -> fd,p,(len - nread))));
      if (len && nread <= 0) {
        nread = 0;
        (this) ->  setStatus (1);
      }
// buffered file
    }
    else {
      nread += ((int )(fread(p,1,(len - nread),(this) -> fh)));
      if (((uint )nread) != len) {
        if ((ferror((this) -> fh)) || nread == 0) {
          (this) ->  setStatus (1);
        }
      }
    }
  }
  (this) -> ioIndex += nread;
  return nread;
}
/*! \overload int writeBlock( const QByteArray& data )
*/
/*! \reimp
  Writes \e len bytes from \e p to the file and returns the number of
  bytes actually written.
  Returns -1 if a serious error occurred.
  \warning When working with buffered files, data may not be written
  to the file at once. Call flush() to make sure the data is really
  written.
  \sa readBlock()
*/

int QFile::writeBlock(const char *p,uint len)
{
#if defined(CHECK_NULL)
  if (p == 0 && len != 0) {
    qWarning("QFile::writeBlock: Null pointer error");
  }
#endif
#if defined(CHECK_STATE)
// file not open
  if (!(this) ->  isOpen ()) {
    qWarning("QFile::writeBlock: File not open");
    return -1;
  }
// writing not permitted
  if (!(this) ->  isWritable ()) {
    qWarning("QFile::writeBlock: Write operation not permitted");
    return -1;
  }
#endif
// number of bytes written
  int nwritten;
// raw file
  if ((this) ->  isRaw ()) {
    nwritten = ((int )(write((this) -> fd,p,len)));
  }
  else {
// buffered file
    nwritten = ((int )(fwrite(p,1,len,(this) -> fh)));
  }
// write error
  if (nwritten != ((int )len)) {
// disk is full
    if ( *__errno_location() == 28) {
      (this) ->  setStatus (4);
    }
    else {
      (this) ->  setStatus (2);
    }
// recalc file position
    if ((this) ->  isRaw ()) {
      (this) -> ioIndex = ((int )(lseek((this) -> fd,0,1)));
    }
    else {
      (this) -> ioIndex = fseek((this) -> fh,0,1);
    }
  }
  else {
    (this) -> ioIndex += nwritten;
  }
// update file length
  if ((this) -> ioIndex > (this) -> length) {
    (this) -> length = (this) -> ioIndex;
  }
  return nwritten;
}
/*!
  Returns the file handle of the file.
  This is a small positive integer, suitable for use with C library
  functions such as fdopen() and fcntl(), as well as with QSocketNotifier.
  If the file is not open or there is an error, handle() returns -1.
  \sa QSocketNotifier
*/

int QFile::handle() const
{
  if (!(this) ->  isOpen ()) {
    return -1;
  }
  else {
    if (((this) -> fh)) {
      return fileno((this) -> fh);
    }
    else {
      return (this) -> fd;
    }
  }
}
/*!
  Closes an open file.
  The file is not closed if it was opened with an existing file handle.
  If the existing file handle is a \c FILE*, the file is flushed.
  If the existing file handle is an \c int file descriptor, nothing
  is done to the file.
  Some "write-behind" filesystems may report an unspecified error on
  closing the file. These errors only indicate that something may
  have gone wrong since the previous open(). In such a case status()
  reports IO_UnspecifiedError after close(), otherwise IO_Ok.
  \sa open(), flush()
*/

void QFile::close()
{
  bool ok = FALSE;
// file is not open
  if ((this) ->  isOpen ()) {
// buffered file
    if (((this) -> fh)) {
      if ((this) -> ext_f) {
// flush instead of closing
        ok = fflush((this) -> fh) != -1;
      }
      else {
        ok = fclose((this) -> fh) != -1;
      }
// raw file
    }
    else {
      if ((this) -> ext_f) {
// cannot close
        ok = TRUE;
      }
      else {
        ok = ::close((this) -> fd) != -1;
      }
    }
// restore internal state
    (this) ->  init ();
  }
  if (!ok) {
    (this) ->  setStatus (8);
  }
  return ;
}

int64 QFile::pos() const
{
  if ((this) ->  isOpen ()) {
// TODO: support 64 bit size
    return (ftell((this) -> fh));
  }
  return (-1);
}

int64 QFile::toEnd()
{
  if ((this) ->  isOpen ()) {
// TODO: support 64 bit size
    if (fseek((this) -> fh,0,2) != -1) {
      return (ftell((this) -> fh));
    }
  }
  return (-1);
}

bool QFile::seek(int64 pos)
{
  if ((this) ->  isOpen ()) {
// TODO: support 64 bit size
    return fseek((this) -> fh,((long )pos),0) != -1;
  }
  return FALSE;
}
