/****************************************************************************
** 
**
** Implementation of QRegExp class
**
** Created : 950126
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qregexp.h"
#include <ctype.h>
#include <stdlib.h>
// NOT REVISED
/*!
  \class QRegExp qregexp.h
  \ingroup tools
  \ingroup misc
  \brief The QRegExp class provides pattern matching using regular
  expressions or wildcards.
  QRegExp knows these regexp primitives:
  <ul plain>
  <li><dfn>c</dfn> matches the character 'c'
  <li><dfn>.</dfn> matches any character
  <li><dfn>^</dfn> matches start of input
  <li><dfn>$</dfn>  matches end of input
  <li><dfn>[]</dfn> matches a defined set of characters - see below.
  <li><dfn>a*</dfn> matches a sequence of zero or more a's
  <li><dfn>a+</dfn> matches a sequence of one or more a's
  <li><dfn>a?</dfn> matches an optional a
  <li><dfn>\c</dfn> escape code for matching special characters such
  as \, [, *, +, . etc.
  <li><dfn>\t</dfn> matches the TAB character (9)
  <li><dfn>\n</dfn> matches newline (10)
  <li><dfn>\r</dfn> matches return (13)
  <li><dfn>\s</dfn> matches a white space (defined as any character
  for which QChar::isSpace() returns TRUE. This includes at least
  ASCII characters 9 (TAB), 10 (LF), 11 (VT), 12(FF), 13 (CR) and 32
  (Space)).
  <li><dfn>\d</dfn> matches a digit (defined as any character for
  which QChar::isDigit() returns TRUE. This includes at least ASCII
  characters '0'-'9').
  <li><dfn>\x1f6b</dfn> matches the character with unicode point U1f6b
  (hexadecimal 1f6b). \x0012 will match the ASCII/Latin1 character
  0x12 (18 decimal, 12 hexadecimal).
  <li><dfn>\022</dfn> matches the ASCII/Latin1 character 022 (18
  decimal, 22 octal).
  </ul>
  In wildcard mode, it only knows four primitives:
  <ul plain>
  <li><dfn>c</dfn> matches the character 'c'
  <li><dfn>?</dfn> matches any character
  <li><dfn>*</dfn> matches any sequence of characters
  <li><dfn>[]</dfn> matches a defined set of characters - see below.
  </ul>
  QRegExp supports Unicode both in the pattern strings and in the
  strings to be matched.
  When writing regular expressions in C++ code, remember that C++
  processes \ characters.  So in order to match e.g. a "." character,
  you must write "\\." in C++ source, not "\.".
  A character set matches a defined set of characters. For example,
  [BSD] matches any of 'B', 'D' and 'S'. Within a character set, the
  special characters '.', '*', '?', '^', '$', '+' and '[' lose their
  special meanings. The following special characters apply:
  <ul plain>
  <li><dfn>^</dfn> When placed first in the list, changes the
  character set to match any character \e not in the list. To include
  the character '^' itself in the set, escape it or place it anywhere
  but first.
  <li><dfn>-</dfn> Defines a range of characters. To include the
  character '-' itself in the set, escape it or place it last.
  <li><dfn>]</dfn> Ends the character set definition. To include the
  character ']' itself in the set, escape it or place it first (but
  after the negation operator '^', if present)
  </ul>
  Thus, [a-zA-Z0-9.] matches upper and lower case ASCII letters,
  digits and dot; and [^\s] matches everything except white space.
  \bug Case insensitive matching is not supported for non-ASCII/Latin1
  (non-8bit) characters. Any character with a non-zero QChar.row() is
  matched case sensitively even if the QRegExp is in case insensitive
  mode.
  \note In Qt 3.0, the language of regular expressions will contain
  five more special characters, namely '(', ')', '{', '|' and '}'. To
  ease porting, it's a good idea to escape these characters with a
  backslash in all the regular expressions you'll write from now on.
*/
//
// The regexp pattern is internally represented as an array of uints,
// each element containing an 16-bit character or a 32-bit code
// (listed below).  User-defined character classes (e.g. [a-zA-Z])
// are encoded as this:
// uint no:	1		2		3		...
// value:	CCL | n		from | to	from | to
//
// where n is the (16-bit) number of following range definitions and
// from and to define the ranges inclusive. from <= to is always true,
// otherwise it is a built-in charclass (Pxx, eg \s - PWS). Single
// characters in the class are coded as from==to.  Negated classes
// (e.g. [^a-z]) use CCN instead of CCL.
const uint END = 0;
// predef charclass: whitespace (\s)
const uint PWS = 0x10010000;
// predef charclass: digit (\d)
const uint PDG = 0x10020000;
// character class	[]
const uint CCL = 0x20010000;
// neg character class	[^]
const uint CCN = 0x20020000;
// character
const uint CHR = 0x40000000;
// beginning of line	^
const uint BOL = 0x80010000;
// end of line		$
const uint EOL = 0x80020000;
// beginning of word	\<
const uint BOW = 0x80030000;
// end of word		\>
const uint EOW = 0x80040000;
// any character	.
const uint ANY = 0x80050000;
// Kleene closure	*
const uint CLO = 0x80070000;
// Optional closure	?
const uint OPT = 0x80080000;
// character class bitmask
const uint MCC = 0x20000000;
// code mask
const uint MCD = 0xffff0000;
// value mask
const uint MVL = 0x0000ffff;
//
// QRegExp::error codes (internal)
//
// pattern ok
const int PatOk = 0;
// no pattern defined
const int PatNull = 1;
// pattern syntax error
const int PatSyntax = 2;
// pattern too long
const int PatOverflow = 4;
/*****************************************************************************
  QRegExp member functions
 *****************************************************************************/
/*!
  Constructs an empty regular expression.
*/

QRegExp::QRegExp()
{
  (this) -> rxdata = 0;
  (this) -> cs = TRUE;
  (this) -> wc = FALSE;
  (this) -> error = PatOk;
}
/*!
  Constructs a regular expression.
  \arg \e pattern is the regular expression pattern string.
  \arg \e caseSensitive specifies whether or not to use case sensitive
  matching.
  \arg \e wildcard specifies whether the pattern string should be used for
  wildcard matching (also called globbing expression), normally used for
  matching file names.
  \sa setWildcard()
*/

QRegExp::QRegExp(const class QCString &pattern,bool caseSensitive,bool wildcard)
{
  (this) -> rxstring = pattern;
  (this) -> rxdata = 0;
  (this) -> cs = caseSensitive;
  (this) -> wc = wildcard;
  (this) ->  compile ();
}
/*!
  Constructs a regular expression which is a copy of \e r.
  \sa operator=(const QRegExp&)
*/

QRegExp::QRegExp(const class ::QRegExp &r)
{
  (this) -> rxstring = r .  pattern ();
  (this) -> rxdata = 0;
  (this) -> cs = r .  caseSensitive ();
  (this) -> wc = r .  wildcard ();
  (this) ->  compile ();
}
/*!
  Destructs the regular expression and cleans up its internal data.
*/

QRegExp::~QRegExp()
{
// Avoid purify complaints
  if (((this) -> rxdata)) {
    delete []((this) -> rxdata);
  }
}
/*!
  Copies the regexp \e r and returns a reference to this regexp.
  The case sensitivity and wildcard options are copied, as well.
*/

QRegExp &QRegExp::operator=(const class ::QRegExp &r)
{
  (this) -> rxstring = r . rxstring;
  (this) -> cs = r . cs;
  (this) -> wc = r . wc;
  (this) ->  compile ();
  return  *(this);
}
/*!
  \obsolete
  Consider using setPattern() instead of this method.
  Sets the pattern string to \e pattern and returns a reference to this regexp.
  The case sensitivity or wildcard options do not change.
*/

QRegExp &QRegExp::operator=(const class QCString &pattern)
{
  (this) -> rxstring = pattern;
  (this) ->  compile ();
  return  *(this);
}
/*!
  Returns TRUE if this regexp is equal to \e r.
  Two regexp objects are equal if they have equal pattern strings,
  case sensitivity options and wildcard options.
*/

bool QRegExp::operator==(const class ::QRegExp &r) const
{
  return (this) -> rxstring==r . rxstring && ((this) -> cs) == r . cs && ((this) -> wc) == r . wc;
}
/*!
  \fn bool QRegExp::operator!=( const QRegExp &r ) const
  Returns TRUE if this regexp is \e not equal to \e r.
  \sa operator==()
*/
/*!
  \fn bool QRegExp::isEmpty() const
  Returns TRUE if the regexp is empty.
*/
/*!
  \fn bool QRegExp::isValid() const
  Returns TRUE if the regexp is valid, or FALSE if it is invalid.
  The pattern "[a-z" is an example of an invalid pattern, since it lacks a
  closing bracket.
*/
/*!
  \fn bool QRegExp::wildcard() const
  Returns TRUE if wildcard mode is on, otherwise FALSE. \sa setWildcard().
*/
/*!
  Sets the wildcard option for the regular expression.	The default
  is FALSE.
  Setting \e wildcard to TRUE makes it convenient to match filenames
  instead of plain text.
  For example, "qr*.cpp" matches the string "qregexp.cpp" in wildcard mode,
  but not "qicpp" (which would be matched in normal mode).
  \sa wildcard()
*/

void QRegExp::setWildcard(bool wildcard)
{
  if (wildcard != ((this) -> wc)) {
    (this) -> wc = wildcard;
    (this) ->  compile ();
  }
}
/*!
  \fn bool QRegExp::caseSensitive() const
  Returns TRUE if case sensitivity is enabled, otherwise FALSE.	 The
  default is TRUE.
  \sa setCaseSensitive()
*/
/*!
  Enables or disables case sensitive matching.
  In case sensitive mode, "a.e" matches "axe" but not "Axe".
  See also: caseSensitive()
*/

void QRegExp::setCaseSensitive(bool enable)
{
  if (((this) -> cs) != enable) {
    (this) -> cs = enable;
    (this) ->  compile ();
  }
}
/*!
  \fn QCString QRegExp::pattern() const
  Returns the pattern string of the regexp.
*/
/*!
  \fn void QRegExp::setPattern(const QCString & pattern)
  Sets the pattern string to \a pattern and returns a reference to this regexp.
  The case sensitivity or wildcard options do not change.
*/

inline static bool iswordchar(int x)
{
//# Only 8-bit support
  return (isalnum(x)) || x == '_';
}
/*!
  \internal
  Match character class
*/

static bool matchcharclass(uint *rxd,char c)
{
  uint *d = rxd;
  uint clcode =  *d & MCD;
  bool neg = clcode == CCN;
  if (clcode != CCL && clcode != CCN) {
    qWarning("QRegExp: Internal error, please report to qt-bugs@trolltech.com");
  }
  uint numFields =  *d & MVL;
//(((uint)(c.row())) << 8) | ((uint)c.cell());
  uint cval = (uint )c;
  bool found = FALSE;
  for (int i = 0; i < ((int )numFields); i++) {
    d++;
    if ( *d == PWS && (isspace(c))) {
      found = TRUE;
      break; 
    }
    if ( *d == PDG && (isdigit(c))) {
      found = TRUE;
      break; 
    }
    else {
      uint from = ( *d & MCD) >> 16;
      uint to =  *d & MVL;
      if (cval >= from && cval <= to) {
        found = TRUE;
        break; 
      }
    }
  }
  return neg?!found : found;
}
/*
  Internal: Recursively match string.
*/

static int matchstring(uint *rxd,const char *str,uint strlength,const char *bol,bool cs)
{
  const char *p = str;
  const char *start = p;
  uint pl = strlength;
  uint *d = rxd;
//### in all cases here: handle pl == 0! (don't read past strlen)
  while(( *d)){
// match char
    if (( *d & CHR)) {
      if (!pl) {
        return -1;
      }
      char c = ( *d);
/*&& !c.row()*/
// case insensitive, #Only 8bit
      if (!cs) {
        if (tolower(( *p)) != c) {
          return -1;
        }
        p++;
        pl--;
// case insensitive
      }
      else {
        if (( *p) != c) {
          return -1;
        }
        p++;
        pl--;
      }
      d++;
    }
    else {
// match char class
      if (( *d & MCC)) {
        if (!pl) {
          return -1;
        }
        if (!matchcharclass(d, *p)) {
          return -1;
        }
        p++;
        pl--;
        d += ( *d & MVL) + 1;
      }
      else {
        switch( *(d++)){
          case PWS:
{
// match whitespace
            if (!pl || !(isspace(( *p)))) {
              return -1;
            }
            p++;
            pl--;
            break; 
          }
          case PDG:
{
// match digits
            if (!pl || !(isdigit(( *p)))) {
              return -1;
            }
            p++;
            pl--;
            break; 
          }
          case ANY:
{
// match anything
            if (!pl) {
              return -1;
            }
            p++;
            pl--;
            break; 
          }
          case BOL:
{
// match beginning of line
            if (p != bol) {
              return -1;
            }
            break; 
          }
          case EOL:
{
// match end of line
            if (pl) {
              return -1;
            }
            break; 
          }
          case BOW:
{
// match beginning of word
            if (!iswordchar(( *p)) || p > bol && iswordchar(( *(p - 1)))) {
              return -1;
            }
            break; 
          }
          case EOW:
{
// match end of word
            if (iswordchar(( *p)) || p == bol || !iswordchar(( *(p - 1)))) {
              return -1;
            }
            break; 
          }
          case CLO:
{
// Kleene closure
{
              const char *first_p = p;
// match char
              if (( *d & CHR)) {
                char c = ( *d);
/*&& !c.row()*/
// case insensitive, #only 8bit
                if (!cs) {
/*&& !p->row()*/
                  while(pl && tolower(( *p)) == c){
                    p++;
                    pl--;
                  }
                }
                else 
// case sensitive
{
                  while(pl && ( *p) == c){
                    p++;
                    pl--;
                  }
                }
                d++;
              }
              else {
// match char class
                if (( *d & MCC)) {
                  while(pl && matchcharclass(d, *p)){
                    p++;
                    pl--;
                  }
                  d += ( *d & MVL) + 1;
                }
                else {
                  if ( *d == PWS) {
                    while(pl && (isspace(( *p)))){
                      p++;
                      pl--;
                    }
                    d++;
                  }
                  else {
                    if ( *d == PDG) {
                      while(pl && (isdigit(( *p)))){
                        p++;
                        pl--;
                      }
                      d++;
                    }
                    else {
                      if ( *d == ANY) {
                        p += pl;
                        pl = 0;
                        d++;
                      }
                      else {
// error
                        return -1;
                      }
                    }
                  }
                }
              }
// skip CLO's END
              d++;
// go backwards
              while(p >= first_p){
                int end = matchstring(d,p,pl,bol,cs);
                if (end >= 0) {
                  return ((int )(p - start)) + end;
                }
                if (!p) {
                  return -1;
                }
                --p;
                ++pl;
              }
            }
            return -1;
          }
          case OPT:
{
// optional closure
{
              const char *first_p = p;
// match char
              if (( *d & CHR)) {
                char c = ( *d);
/*&& !c.row()*/
// case insensitive, #only 8bit
                if (!cs) {
/*!p->row() &&*/
                  if (pl && tolower(( *p)) == c) {
                    p++;
                    pl--;
                  }
                }
                else 
// case sensitive
{
                  if (pl && ( *p) == c) {
                    p++;
                    pl--;
                  }
                }
                d++;
              }
              else {
// match char class
                if (( *d & MCC)) {
                  if (pl && matchcharclass(d, *p)) {
                    p++;
                    pl--;
                  }
                  d += ( *d & MVL) + 1;
                }
                else {
                  if ( *d == PWS) {
                    if (pl && (isspace(( *p)))) {
                      p++;
                      pl--;
                    }
                    d++;
                  }
                  else {
                    if ( *d == PDG) {
                      if (pl && (isdigit(( *p)))) {
                        p++;
                        pl--;
                      }
                      d++;
                    }
                    else {
                      if ( *d == ANY) {
                        if (pl) {
                          p++;
                          pl--;
                        }
                        d++;
                      }
                      else {
// error
                        return -1;
                      }
                    }
                  }
                }
              }
// skip OPT's END
              d++;
// go backwards
              while(p >= first_p){
                int end = matchstring(d,p,pl,bol,cs);
                if (end >= 0) {
                  return ((int )(p - start)) + end;
                }
                if (!p) {
                  return -1;
                }
                --p;
                ++pl;
              }
            }
            return -1;
          }
          default:
// error
          return -1;
        }
      }
    }
  }
  return (int )(p - start);
}
/*!
  \internal
  Recursively match string.
*/
// This is obsolete now, but since it is protected (not private), it
// is still implemented on the off-chance that somebody has made a
// class derived from QRegExp and calls this directly.
// Qt 3.0: Remove this?
#if 0
#endif
/*!
  Attempts to match in \e str, starting from position \e index.
  Returns the position of the match, or -1 if there was no match.
  If \e len is not a null pointer, the length of the match is stored in
  \e *len.
  If \e indexIsStart is TRUE (the default), the position \e index in
  the string will match the start-of-input primitive (^) in the
  regexp, if present. Otherwise, position 0 in \e str will match.
  Example:
  \code
    QRegExp r("[0-9]*\\.[0-9]+");		// matches floating point
    int len;
    r.match("pi = 3.1416", 0, &len);		// returns 5, len == 6
  \endcode
  \note In Qt 3.0, this function will be replaced by find().
*/

int QRegExp::match(const class QCString &str,int index,int *len,bool indexIsStart) const
{
  if (!(this) ->  isValid () || (this) ->  isEmpty ()) {
    return -1;
  }
  if (str .  length () < ((uint )index)) {
    return -1;
  }
  const char *start = (str .  data ());
  const char *p = start + index;
  uint pl = str .  length () - index;
  uint *d = (this) -> rxdata;
  int ep = -1;
// match from beginning of line
  if ( *d == BOL) {
    ep = matchstring(d,p,pl,(indexIsStart?p : start),(this) -> cs);
  }
  else {
    if (( *d & CHR)) {
      char c = ( *d);
/*&& !c.row()*/
// case sensitive, # only 8bit
      if (!(this) -> cs) {
/*p->row() ||*/
        while(pl && tolower(( *p)) != c){
          p++;
          pl--;
        }
// case insensitive
      }
      else {
        while(pl && ( *p) != c){
          p++;
          pl--;
        }
      }
    }
// regular match
    while(1){
      ep = matchstring(d,p,pl,(indexIsStart?start + index : start),(this) -> cs);
      if (ep >= 0) {
        break; 
      }
      if (!pl) {
        break; 
      }
      p++;
      pl--;
    }
  }
  if (len) {
// No match -> 0, for historical reasons
     *len = (ep >= 0?ep : 0);
  }
// return index;
  return ep >= 0?((int )(p - start)) : -1;
}
/*! \fn int QRegExp::find( const QCString& str, int index )
  Attempts to match in \e str, starting from position \e index.
  Returns the position of the match, or -1 if there was no match.
  \sa match()
*/
//
// Translate wildcard pattern to standard regexp pattern.
// Ex:	 *.cpp	==> ^.*\.cpp$
//

static class QCString wc2rx(const class QCString &pattern)
{
  int patlen = (int )(pattern .  length ());
  class QCString wcpattern("^");
  char c;
  for (int i = 0; i < patlen; i++) {
    c = pattern[i];
    switch(((char )c)){
      case '*':
{
// '*' ==> '.*'
        wcpattern += '.';
        break; 
      }
      case '?':
{
// '?' ==> '.'
        c = '.';
        break; 
      }
      case '.':
{
      }
      case '+':
{
      }
      case '\\':
{
      }
      case '$':
{
      }
      case '^':
{
// quote special regexp chars
        wcpattern += '\\';
        break; 
      }
      case '[':
{
// don't quote '^' after '['
        if (((char )(pattern[(i + 1)])) == '^') {
          wcpattern += '[';
          c = pattern[(i + 1)];
          i++;
        }
        break; 
      }
    }
    wcpattern += c;
  }
  wcpattern += '$';
// return new regexp pattern
  return (wcpattern);
}
//
// Internal: Get char value and increment pointer.
//
// get char value

static uint char_val(const char **str,uint *strlength)
{
  const char *p =  *str;
  uint pl =  *strlength;
  uint len = 1;
  uint v = 0;
// escaped code
  if (((char )( *p)) == '\\') {
    p++;
    pl--;
// it is just a '\'
    if (!pl) {
      ( *str)++;
      ( *strlength)--;
      return '\\';
    }
// length at least 2
    len++;
    int i;
    char c;
    char ch = (tolower(((char )( *p))));
    switch(ch){
      case 'b':
{
// bell
        v = '\b';
        break; 
      }
      case 'f':
{
// form feed
        v = '\f';
        break; 
      }
      case 'n':
{
// newline
        v = '\n';
        break; 
      }
      case 'r':
{
// return
        v = '\r';
        break; 
      }
      case 't':
{
// tab
        v = '\t';
        break; 
      }
      case 's':
{
// whitespace charclass
        v = PWS;
        break; 
      }
      case 'd':
{
// digit charclass
        v = PDG;
        break; 
      }
      case '<':
{
// word beginning matcher
        v = BOW;
        break; 
      }
      case '>':
{
// word ending matcher
        v = EOW;
        break; 
      }
      case 'x':
{
// hex code
{
          p++;
          pl--;
//up to 4 hex digits
          for (i = 0; i < 4 && pl; i++) {
            c = (tolower(((char )( *p))));
            bool a = c >= 'a' && c <= 'f';
            if (c >= '0' && c <= '9' || a) {
              v <<= 4;
              v += ((a?10 + c - 'a' : c - '0'));
              len++;
            }
            else {
              break; 
            }
            p++;
            pl--;
          }
        }
        break; 
      }
      default:
{
//octal code
        if (ch >= '0' && ch <= '7') {
          len--;
// up to 3 oct digits
          for (i = 0; i < 3 && pl; i++) {
            c = ((char )( *p));
            if (c >= '0' && c <= '7') {
              v <<= 3;
              v += (c - '0');
              len++;
            }
            else {
              break; 
            }
            p++;
            pl--;
          }
        }
        else 
// not an octal number
{
//(((uint)(p->row())) << 8) | ((uint)p->cell());
          v = ((uint )( *p));
        }
      }
    }
  }
  else {
//(((uint)(p->row())) << 8) | ((uint)p->cell());
    v = ((uint )( *p));
  }
   *str += len;
   *strlength -= len;
  return v;
}
#if 0 //defined(DEBUG)
//(((uint)(uc.row())) << 8) | ((uint)uc.cell());
#endif // DEBUG
// max length of regexp array
static const int maxlen = 1024;
// tmp regexp array
static uint rxarray[maxlen];
/*!
  \internal
  Compiles the regular expression and stores the result in rxdata.
  The 'error' flag is set to non-zero if an error is detected.
  NOTE! This function is not reentrant!
*/

void QRegExp::compile()
{
// delete old data
  if (((this) -> rxdata)) {
    delete []((this) -> rxdata);
    (this) -> rxdata = 0;
  }
// no regexp pattern set
  if ((this) -> rxstring .  isEmpty ()) {
    (this) -> error = PatNull;
    return ;
  }
// assume pattern is ok
  (this) -> error = PatOk;
  class QCString pattern;
  if ((this) -> wc) {
    pattern = wc2rx((this) -> rxstring);
  }
  else {
    pattern = ((this) -> rxstring);
  }
// pattern pointer
  const char *start = (pattern .  data ());
// pattern pointer
  const char *p = start;
  uint pl = pattern .  length ();
// data pointer
  uint *d = rxarray;
  uint *prev_d = 0;
#define GEN(x)	*d++ = (x)
  while(pl){
    char ch = (char )( *p);
    switch(ch){
      case '^':
{
// beginning of line
        prev_d = d;
         *(d++) = (p == start?BOL : CHR | ch);
        p++;
        pl--;
        break; 
      }
      case '$':
{
// end of line
        prev_d = d;
         *(d++) = (pl == 1?EOL : CHR | ch);
        p++;
        pl--;
        break; 
      }
      case '.':
{
// any char
        prev_d = d;
         *(d++) = ANY;
        p++;
        pl--;
        break; 
      }
      case '[':
{
// character class
{
          prev_d = d;
          p++;
          pl--;
          if (!pl) {
            (this) -> error = PatSyntax;
            return ;
          }
          bool firstIsEscaped = ((char )( *p)) == '\\';
          uint cch = char_val(&p,&pl);
// negate!
          if (cch == '^' && !firstIsEscaped) {
             *(d++) = CCN;
            if (!pl) {
              (this) -> error = PatSyntax;
              return ;
            }
            cch = char_val(&p,&pl);
          }
          else {
             *(d++) = CCL;
          }
          uint numFields = 0;
          while(pl){
            if (pl > 2 && ((char )( *p)) == '-' && ((char )( *(p + 1))) != ']') {
// Found a range
// Read the '-'
              char_val(&p,&pl);
// Read the range end
              uint cch2 = char_val(&p,&pl);
// swap start and stop
              if (cch > cch2) {
                int tmp = cch;
                cch = cch2;
                cch2 = tmp;
              }
// from < to
               *(d++) = cch << 16 | cch2;
              numFields++;
            }
            else {
// Found a single character
// It's a code; will not be mistaken
              if ((cch & MCD)) {
// for a range, since from > to
                 *(d++) = cch;
              }
              else {
// from == to range
                 *(d++) = cch << 16 | cch;
              }
              numFields++;
            }
// pattern too long
            if (d >= rxarray + maxlen) {
              (this) -> error = PatOverflow;
              return ;
            }
// At least ']' should be left
            if (!pl) {
              (this) -> error = PatSyntax;
              return ;
            }
            bool nextIsEscaped = ((char )( *p)) == '\\';
            cch = char_val(&p,&pl);
            if (cch == ((uint )']') && !nextIsEscaped) {
              break; 
            }
// End, should have seen ']'
            if (!pl) {
              (this) -> error = PatSyntax;
              return ;
            }
          }
// Store number of fields
           *prev_d |= numFields;
        }
        break; 
      }
      case '*':
{
      }
      case '+':
{
      }
      case '?':
{
// Kleene closure, or
// positive closure, or
// optional closure
{
// no previous expression
          if (prev_d == 0) {
// empty closure
            (this) -> error = PatSyntax;
            return ;
          }
// test if invalid closure
          switch( *prev_d){
            case BOL:
{
            }
            case BOW:
{
            }
            case EOW:
{
            }
            case CLO:
{
            }
            case OPT:
{
              (this) -> error = PatSyntax;
              return ;
            }
          }
          int ddiff = (int )(d - prev_d);
// convert to Kleene closure
          if (( *p) == '+') {
            if (d + ddiff >= rxarray + maxlen) {
// pattern too long
              (this) -> error = PatOverflow;
              return ;
            }
            memcpy(d,prev_d,ddiff * sizeof(uint ));
            d += ddiff;
            prev_d += ddiff;
          }
          memmove((prev_d + 1),prev_d,ddiff * sizeof(uint ));
           *prev_d = (ch == '?'?OPT : CLO);
          d++;
           *(d++) = END;
          p++;
          pl--;
        }
        break; 
      }
      default:
{
        prev_d = d;
        uint cv = char_val(&p,&pl);
// It's a code
        if ((cv & MCD)) {
           *(d++) = cv;
        }
        else {
// #only 8bit support
          if (!(this) -> cs && cv <= 0xff) {
            cv = (tolower(cv));
          }
           *(d++) = CHR | cv;
        }
      }
    }
// oops!
    if (d >= rxarray + maxlen) {
// pattern too long
      (this) -> error = PatOverflow;
      return ;
    }
  }
   *(d++) = END;
  int len = (int )(d - rxarray);
// copy from rxarray to rxdata
  (this) -> rxdata = (new uint [len * 4UL]);
  qt_check_pointer((this) -> rxdata == 0,"qregexp.cpp",1087);
  memcpy(((this) -> rxdata),rxarray,len * sizeof(uint ));
#if defined(DEBUG)
//dump( rxdata );	// uncomment this line for debugging
#endif
}
