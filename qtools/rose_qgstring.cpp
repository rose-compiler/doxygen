/******************************************************************************
 *
 * Copyright (C) 1997-2004 by Dimitri van Heesch.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation under the terms of the GNU General Public License is hereby 
 * granted. No representations are made about the suitability of this software 
 * for any purpose. It is provided "as is" without express or implied warranty.
 * See the GNU General Public License for more details.
 *
 * Documents produced by Doxygen are derivative works derived from the
 * input used in their production; they are not affected by this license.
 *
 */
#include <stdio.h>
#include "qgstring.h"
#include <assert.h>
#define BLOCK_SIZE 64
#define ROUND_SIZE(x) ((x)+BLOCK_SIZE-1)&~(BLOCK_SIZE-1)
#define DBG_STR(x) do { } while(0)
//#define DBG_STR(x) printf x
// make null string

QGString::QGString() : m_data(0), m_len(0), m_memSize(0)
{
  do {
  }while (0);
}

QGString::QGString(uint size)
{
  if (size == 0) {
    (this) -> m_data = 0;
    (this) -> m_len = 0;
  }
  else {
    (this) -> m_memSize = size + 1 + 64 - 1 & (~(64 - 1));
    (this) -> m_data = ((char *)(malloc(((this) -> m_memSize))));
    memset(((this) -> m_data),' ',size);
    (this) -> m_data[size] = '\0';
    (this) -> m_len = size;
  }
  do {
  }while (0);
}

QGString::QGString(const class ::QGString &s)
{
  if (s . m_memSize == 0) {
    (this) -> m_data = 0;
    (this) -> m_len = 0;
    (this) -> m_memSize = 0;
  }
  else {
    (this) -> m_data = ((char *)(malloc(s . m_memSize)));
    (this) -> m_len = s . m_len;
    (this) -> m_memSize = s . m_memSize;
    qstrcpy((this) -> m_data,s . m_data);
  }
  do {
  }while (0);
}

QGString::QGString(const char *str)
{
  if (str == 0) {
    (this) -> m_data = 0;
    (this) -> m_len = 0;
    (this) -> m_memSize = 0;
  }
  else {
    (this) -> m_len = qstrlen(str);
    (this) -> m_memSize = (this) -> m_len + 1 + 64 - 1 & (~(64 - 1));
    (this) -> m_memSize >= (this) -> m_len + 1?((void )0) : ((__assert_fail("m_memSize>=m_len+1","qgstring.cpp",82,__PRETTY_FUNCTION__) , ((void )0)));
    (this) -> m_data = ((char *)(malloc(((this) -> m_memSize))));
    qstrcpy((this) -> m_data,str);
  }
  do {
  }while (0);
}

QGString::~QGString()
{
  free(((this) -> m_data));
  (this) -> m_data = 0;
  do {
  }while (0);
}

bool QGString::resize(uint newlen)
{
  (this) -> m_len = 0;
  if (newlen == 0) {
    if (((this) -> m_data)) {
      free(((this) -> m_data));
      (this) -> m_data = 0;
    }
    (this) -> m_memSize = 0;
    do {
    }while (0);
    return TRUE;
  }
  (this) -> m_memSize = newlen + 1 + 64 - 1 & (~(64 - 1));
  (this) -> m_memSize >= newlen + 1?((void )0) : ((__assert_fail("m_memSize>=newlen+1","qgstring.cpp",107,__PRETTY_FUNCTION__) , ((void )0)));
  if ((this) -> m_data == 0) {
    (this) -> m_data = ((char *)(malloc(((this) -> m_memSize))));
  }
  else {
    (this) -> m_data = ((char *)(realloc(((this) -> m_data),((this) -> m_memSize))));
  }
  if ((this) -> m_data == 0) {
    do {
    }while (0);
    return FALSE;
  }
  (this) -> m_data[newlen] = '\0';
  (this) -> m_len = qstrlen(((this) -> m_data));
  do {
  }while (0);
  return TRUE;
}

bool QGString::enlarge(uint newlen)
{
  if (newlen == 0) {
    if (((this) -> m_data)) {
      free(((this) -> m_data));
      (this) -> m_data = 0;
    }
    (this) -> m_memSize = 0;
    (this) -> m_len = 0;
    return TRUE;
  }
  uint newMemSize = newlen + 1 + 64 - 1 & (~(64 - 1));
  if (newMemSize == (this) -> m_memSize) {
    return TRUE;
  }
  (this) -> m_memSize = newMemSize;
  if ((this) -> m_data == 0) {
    (this) -> m_data = ((char *)(malloc(((this) -> m_memSize))));
  }
  else {
    (this) -> m_data = ((char *)(realloc(((this) -> m_data),((this) -> m_memSize))));
  }
  if ((this) -> m_data == 0) {
    return FALSE;
  }
  (this) -> m_data[newlen - 1] = '\0';
  if ((this) -> m_len > newlen) {
    (this) -> m_len = newlen;
  }
  return TRUE;
}

void QGString::setLen(uint newlen)
{
  (this) -> m_len = (newlen <= (this) -> m_memSize?newlen : (this) -> m_memSize);
}

QGString &QGString::operator=(const class ::QGString &s)
{
  if (((this) -> m_data)) {
    free(((this) -> m_data));
  }
// null string
  if (s . m_memSize == 0) {
    (this) -> m_data = 0;
    (this) -> m_len = 0;
    (this) -> m_memSize = 0;
  }
  else {
    (this) -> m_len = s . m_len;
    (this) -> m_memSize = s . m_memSize;
    (this) -> m_data = ((char *)(malloc(((this) -> m_memSize))));
    qstrcpy((this) -> m_data,s . m_data);
  }
  do {
  }while (0);
  return  *(this);
}

QGString &QGString::operator=(const char *str)
{
  if (((this) -> m_data)) {
    free(((this) -> m_data));
  }
// null string
  if (str == 0) {
    (this) -> m_data = 0;
    (this) -> m_len = 0;
    (this) -> m_memSize = 0;
  }
  else {
    (this) -> m_len = qstrlen(str);
    (this) -> m_memSize = (this) -> m_len + 1 + 64 - 1 & (~(64 - 1));
    (this) -> m_memSize >= (this) -> m_len + 1?((void )0) : ((__assert_fail("m_memSize>=m_len+1","qgstring.cpp",195,__PRETTY_FUNCTION__) , ((void )0)));
    (this) -> m_data = ((char *)(malloc(((this) -> m_memSize))));
    qstrcpy((this) -> m_data,str);
  }
  do {
  }while (0);
  return  *(this);
}

QGString &QGString::operator+=(const class ::QGString &s)
{
  if (s . m_memSize == 0) {
    return  *(this);
  }
  uint len1 = (this) ->  length ();
  uint len2 = s .  length ();
  uint memSize = len1 + len2 + 1 + 64 - 1 & (~(64 - 1));
  memSize >= len1 + len2 + 1?((void )0) : ((__assert_fail("memSize>=len1+len2+1","qgstring.cpp",209,__PRETTY_FUNCTION__) , ((void )0)));
  char *newData = memSize != (this) -> m_memSize?((char *)(realloc(((this) -> m_data),memSize))) : (this) -> m_data;
  (this) -> m_memSize = memSize;
  if (((this) -> m_data)) {
    (this) -> m_data = newData;
    memcpy(((this) -> m_data + len1),(s .  operator const char * ()),(len2 + 1));
  }
  (this) -> m_len = len1 + len2;
  do {
  }while (0);
  return  *(this);
}

QGString &QGString::operator+=(const char *str)
{
  if (!str) {
    return  *(this);
  }
  uint len1 = (this) ->  length ();
  uint len2 = qstrlen(str);
  uint memSize = len1 + len2 + 1 + 64 - 1 & (~(64 - 1));
  memSize >= len1 + len2 + 1?((void )0) : ((__assert_fail("memSize>=len1+len2+1","qgstring.cpp",228,__PRETTY_FUNCTION__) , ((void )0)));
  char *newData = memSize != (this) -> m_memSize?((char *)(realloc(((this) -> m_data),memSize))) : (this) -> m_data;
  (this) -> m_memSize = memSize;
  if (newData) {
    (this) -> m_data = newData;
    memcpy(((this) -> m_data + len1),str,(len2 + 1));
  }
  (this) -> m_len += len2;
  do {
  }while (0);
  return  *(this);
}

QGString &QGString::operator+=(char c)
{
  uint len = (this) -> m_len;
  uint memSize = len + 2 + 64 - 1 & (~(64 - 1));
  memSize >= len + 2?((void )0) : ((__assert_fail("memSize>=len+2","qgstring.cpp",245,__PRETTY_FUNCTION__) , ((void )0)));
  char *newData = memSize != (this) -> m_memSize?((char *)(realloc(((this) -> m_data),memSize))) : (this) -> m_data;
  (this) -> m_memSize = memSize;
  if (newData) {
    (this) -> m_data = newData;
    (this) -> m_data[len] = c;
    (this) -> m_data[len + 1] = '\0';
  }
  (this) -> m_len++;
  do {
  }while (0);
  return  *(this);
}
