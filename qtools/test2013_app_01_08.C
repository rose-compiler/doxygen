
#define Q_UINT32 int

template<class type> class QArray // : public QGArray
   {
     public:
          QArray() {}
   };

class QCString 
   {
     public:
          QCString() {}		// make null string
          QCString( const QCString &s );
   };

class QDataStream
   {
     public:
          QDataStream();
          QDataStream &operator>>( Q_UINT32 &i );
   };


inline QDataStream &QDataStream::operator>>( Q_UINT32 &i )
   { 
     return *this >> (Q_UINT32&)i; 
   }


QDataStream &operator>>( QDataStream &s, QArray<char> &a )
   {
     Q_UINT32 len;
     s >> len;
     return s;
   }

