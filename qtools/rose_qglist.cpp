/****************************************************************************
** 
**
** Implementation of QGList and QGListIterator classes
**
** Created : 920624
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qglist.h"
#include "qgvector.h"
#include "qdatastream.h"
// NOT REVISED
/*!
  \class QLNode qglist.h
  \brief The QLNode class is an internal class for the QList template collection.
  QLNode is a doubly linked list node; it has three pointers:
  <ol>
  <li> Pointer to the previous node.
  <li> Pointer to the next node.
  <li> Pointer to the actual data.
  </ol>
  Sometimes it might be practical to have direct access to the list nodes
  in a QList, but it is seldom required.
  \warning Be very careful if you want to access the list nodes. The heap
  can easily get corrupted if you make a mistake.
  \sa QList::currentNode(), QList::removeNode(), QList::takeNode()
*/
/*!
  \fn QCollection::Item QLNode::getData()
  Returns a pointer (\c void*) to the actual data in the list node.
*/
/*!
  \class QGList qglist.h
  \brief The QGList class is an internal class for implementing Qt collection classes.
  QGList is a strictly internal class that acts as a base class for several
  \link collection.html collection classes\endlink; QList, QQueue and
  QStack.
  QGList has some virtual functions that can be reimplemented to customize
  the subclasses.
  <ul>
  <li> compareItems() compares two collection/list items.
  <li> read() reads a collection/list item from a QDataStream.
  <li> write() writes a collection/list item to a QDataStream.
  </ul>
  Normally, you do not have to reimplement any of these functions.
  If you still want to reimplement them, see the QStrList class (qstrlist.h),
  which is a good example.
*/
/*****************************************************************************
  Default implementation of virtual functions
 *****************************************************************************/
/*!
  This virtual function compares two list items.
  Returns:
  <ul>
  <li> 0 if \e item1 == \e item2
  <li> non-zero if \e item1 != \e item2
  </ul>
  This function returns \e int rather than \e bool so that
  reimplementations can return three values and use it to sort by:
  <ul>
  <li> 0 if \e item1 == \e item2
  <li> \> 0 (positive integer) if \e item1 \> \e item2
  <li> \< 0 (negative integer) if \e item1 \< \e item2
  </ul>
  The QList::inSort() function requires that compareItems() is implemented
  as described here.
  This function should not modify the list because some const functions
  call compareItems().
  The default implementation compares the pointers:
  \code
  \endcode
*/

int QGList::compareItems(Item item1,Item item2)
{
// compare pointers
  return (item1 != item2);
}
#ifndef QT_NO_DATASTREAM
/*!
  Reads a collection/list item from the stream \a s and returns a reference
  to the stream.
  The default implementation sets \a item to 0.
  \sa write()
*/

QDataStream &QGList::read(class QDataStream &s,Item &item)
{
  item = 0;
  return s;
}
/*!
  Writes a collection/list item to the stream \a s and returns a reference
  to the stream.
  The default implementation does nothing.
  \sa read()
*/

QDataStream &QGList::write(class QDataStream &s,Item ) const
{
  return s;
}
#endif // QT_NO_DATASTREAM
/*****************************************************************************
  QGList member functions
 *****************************************************************************/
/*!
  \internal
  Constructs an empty list.
*/

QGList::QGList()
{
// initialize list
  (this) -> firstNode = (this) -> lastNode = (this) -> curNode = 0;
  (this) -> numNodes = 0;
  (this) -> curIndex = -1;
// initialize iterator list
  (this) -> iterators = 0;
}
/*!
  \internal
  Constructs a copy of \e list.
*/

QGList::QGList(const class ::QGList &list) : QCollection(list)
{
// initialize list
  (this) -> firstNode = (this) -> lastNode = (this) -> curNode = 0;
  (this) -> numNodes = 0;
  (this) -> curIndex = -1;
// initialize iterator list
  (this) -> iterators = 0;
  class QLNode *n = list . firstNode;
// copy all items from list
  while(n){
    (this) ->  append (n -> QLNode::data);
    n = n -> QLNode::next;
  }
}
/*!
  \internal
  Removes all items from the list and destroys the list.
*/

QGList::~QGList()
{
  (this) ->  clear ();
// no iterators for this list
  if (!((this) -> iterators)) {
    return ;
  }
  class QGListIterator *i = (class QGListIterator *)((this) -> iterators ->  first ());
// notify all iterators that
  while(i){
// this list is deleted
    i -> QGListIterator::list = 0;
    i -> QGListIterator::curNode = 0;
    i = ((class QGListIterator *)((this) -> iterators ->  next ()));
  }
  delete ((this) -> iterators);
}
/*!
  \internal
  Assigns \e list to this list.
*/

QGList &QGList::operator=(const class ::QGList &list)
{
  (this) ->  clear ();
  if (list .  count () > 0) {
    class QLNode *n = list . firstNode;
// copy all items from list
    while(n){
      (this) ->  append (n -> QLNode::data);
      n = n -> QLNode::next;
    }
    (this) -> curNode = (this) -> firstNode;
    (this) -> curIndex = 0;
  }
  return  *(this);
}
/*!
  Compares this list with \a list. Retruns TRUE if the lists
  contain the same data, else FALSE.
*/

bool QGList::operator==(const class ::QGList &list) const
{
  if ((this) ->  count () != list .  count ()) {
    return FALSE;
  }
  if ((this) ->  count () == 0) {
    return TRUE;
  }
  class QLNode *n1 = (this) -> firstNode;
  class QLNode *n2 = list . firstNode;
  while(n1 && n2){
// should be mutable
    if (((class ::QGList *)(this)) ->  compareItems (n1 -> QLNode::data,n2 -> QLNode::data) != 0) {
      return FALSE;
    }
    n1 = n1 -> QLNode::next;
    n2 = n2 -> QLNode::next;
  }
  return TRUE;
}
/*!
  \fn uint QGList::count() const
  \internal
  Returns the number of items in the list.
*/
/*!
  \internal
  Returns the node at position \e index.  Sets this node to current.
*/

QLNode *QGList::locate(uint index)
{
// current node ?
  if (index == ((uint )((this) -> curIndex))) {
    return (this) -> curNode;
  }
// set current node
  if (!((this) -> curNode) && ((this) -> firstNode)) {
    (this) -> curNode = (this) -> firstNode;
    (this) -> curIndex = 0;
  }
  register class QLNode *node;
// node distance to cur node
  int distance = (index - ((this) -> curIndex));
// direction to traverse
  bool forward;
  if (index >= (this) -> numNodes) {
#if defined(CHECK_RANGE)
    qWarning("QGList::locate: Index %d out of range",index);
#endif
    return 0;
  }
  if (distance < 0) {
    distance = -distance;
  }
  if (((uint )distance) < index && ((uint )distance) < (this) -> numNodes - index) {
// start from current node
    node = (this) -> curNode;
    forward = index > ((uint )((this) -> curIndex));
// start from first node
  }
  else {
    if (index < (this) -> numNodes - index) {
      node = (this) -> firstNode;
      distance = index;
      forward = TRUE;
// start from last node
    }
    else {
      node = (this) -> lastNode;
      distance = ((this) -> numNodes - index - 1);
      if (distance < 0) {
        distance = 0;
      }
      forward = FALSE;
    }
  }
// now run through nodes
  if (forward) {
    while((distance--))
      node = node -> QLNode::next;
  }
  else {
    while((distance--))
      node = node -> QLNode::prev;
  }
// must update index
  (this) -> curIndex = index;
  return (this) -> curNode = node;
}
/*!
  \internal
  Inserts an item at its sorted position in the list.
*/

void QGList::inSort(Item d)
{
  int index = 0;
  register class QLNode *n = (this) -> firstNode;
// find position in list
  while(n && (this) ->  compareItems (n -> QLNode::data,d) < 0){
    n = n -> QLNode::next;
    index++;
  }
  (this) ->  insertAt (index,d);
}
/*!
  \internal
  Inserts an item at the start of the list.
*/

void QGList::prepend(Item d)
{
  register class QLNode *n = new QLNode ((this) ->  newItem (d));
  qt_check_pointer(n == 0,"qglist.cpp",347);
  n -> QLNode::prev = 0;
// list is not empty
  if ((n -> QLNode::next = (this) -> firstNode)) {
    (this) -> firstNode -> QLNode::prev = n;
  }
  else {
// initialize list
    (this) -> lastNode = n;
  }
// curNode affected
  (this) -> firstNode = (this) -> curNode = n;
  (this) -> numNodes++;
  (this) -> curIndex = 0;
}
/*!
  \internal
  Inserts an item at the end of the list.
*/

void QGList::append(Item d)
{
  register class QLNode *n = new QLNode ((this) ->  newItem (d));
  qt_check_pointer(n == 0,"qglist.cpp",367);
  n -> QLNode::next = 0;
// list is not empty
  if ((n -> QLNode::prev = (this) -> lastNode)) {
    (this) -> lastNode -> QLNode::next = n;
  }
  else {
// initialize list
    (this) -> firstNode = n;
  }
// curNode affected
  (this) -> lastNode = (this) -> curNode = n;
  (this) -> curIndex = ((this) -> numNodes);
  (this) -> numNodes++;
}
/*!
  \internal
  Inserts an item at position \e index in the list.
*/

bool QGList::insertAt(uint index,Item d)
{
// insert at head of list
  if (index == 0) {
    (this) ->  prepend (d);
    return TRUE;
// append at tail of list
  }
  else {
    if (index == (this) -> numNodes) {
      (this) ->  append (d);
      return TRUE;
    }
  }
  class QLNode *nextNode = (this) ->  locate (index);
// illegal position
  if (!nextNode) {
    return FALSE;
  }
  class QLNode *prevNode = nextNode -> QLNode::prev;
  register class QLNode *n = new QLNode ((this) ->  newItem (d));
  qt_check_pointer(n == 0,"qglist.cpp",398);
  nextNode -> QLNode::prev = n;
  prevNode -> QLNode::next = n;
// link new node into list
  n -> QLNode::prev = prevNode;
  n -> QLNode::next = nextNode;
// curIndex set by locate()
  (this) -> curNode = n;
  (this) -> numNodes++;
  return TRUE;
}
/*!
  \internal
  Relinks node \e n and makes it the first node in the list.
*/

void QGList::relinkNode(class QLNode *n)
{
// already first
  if (n == (this) -> firstNode) {
    return ;
  }
  (this) -> curNode = n;
  (this) ->  unlink ();
  n -> QLNode::prev = 0;
// list is not empty
  if ((n -> QLNode::next = (this) -> firstNode)) {
    (this) -> firstNode -> QLNode::prev = n;
  }
  else {
// initialize list
    (this) -> lastNode = n;
  }
// curNode affected
  (this) -> firstNode = (this) -> curNode = n;
  (this) -> numNodes++;
  (this) -> curIndex = 0;
}
/*!
  \internal
  Unlinks the current list node and returns a pointer to this node.
*/

QLNode *QGList::unlink()
{
// null current node
  if ((this) -> curNode == 0) {
    return 0;
  }
// unlink this node
  register class QLNode *n = (this) -> curNode;
// removing first node ?
  if (n == (this) -> firstNode) {
    if (((this) -> firstNode = n -> QLNode::next)) {
      (this) -> firstNode -> QLNode::prev = 0;
    }
    else {
// list becomes empty
      (this) -> lastNode = (this) -> curNode = 0;
      (this) -> curIndex = -1;
    }
  }
  else {
// removing last node ?
    if (n == (this) -> lastNode) {
      (this) -> lastNode = n -> QLNode::prev;
      (this) -> lastNode -> QLNode::next = 0;
// neither last nor first node
    }
    else {
      n -> QLNode::prev -> QLNode::next = n -> QLNode::next;
      n -> QLNode::next -> QLNode::prev = n -> QLNode::prev;
    }
  }
// change current node
  if ((n -> QLNode::next)) {
    (this) -> curNode = n -> QLNode::next;
  }
  else {
    if ((n -> QLNode::prev)) {
      (this) -> curNode = n -> QLNode::prev;
      (this) -> curIndex--;
    }
  }
// update iterators
  if (((this) -> iterators) && (((this) -> iterators) ->  count ())) {
    class QGListIterator *i = (class QGListIterator *)((this) -> iterators ->  first ());
// fix all iterators that
    while(i){
// refers to pending node
      if (i -> QGListIterator::curNode == n) {
        i -> QGListIterator::curNode = (this) -> curNode;
      }
      i = ((class QGListIterator *)((this) -> iterators ->  next ()));
    }
  }
  (this) -> numNodes--;
  return n;
}
/*!
  \internal
  Removes the node \e n from the list.
*/

bool QGList::removeNode(class QLNode *n)
{
#if defined(CHECK_NULL)
  if (n == 0 || (n -> QLNode::prev) && n -> QLNode::prev -> QLNode::next != n || (n -> QLNode::next) && n -> QLNode::next -> QLNode::prev != n) {
    qWarning("QGList::removeNode: Corrupted node");
    return FALSE;
  }
#endif
  (this) -> curNode = n;
// unlink node
  (this) ->  unlink ();
// deallocate this node
  (this) ->  deleteItem (n -> QLNode::data);
  delete n;
  (this) -> curNode = (this) -> firstNode;
  (this) -> curIndex = (((this) -> curNode)?0 : -1);
  return TRUE;
}
/*!
  \internal
  Removes the item \e d from the list.	Uses compareItems() to find the item.
*/

bool QGList::remove(Item d)
{
// find the item
  if (d) {
    if ((this) ->  find (d) == -1) {
      return FALSE;
    }
  }
// unlink node
  class QLNode *n = (this) ->  unlink ();
  if (!n) {
    return FALSE;
  }
// deallocate this node
  (this) ->  deleteItem (n -> QLNode::data);
  delete n;
  return TRUE;
}
/*!
  \internal
  Removes the item \e d from the list.
*/

bool QGList::removeRef(Item d)
{
// find the item
  if (d) {
    if ((this) ->  findRef (d) == -1) {
      return FALSE;
    }
  }
// unlink node
  class QLNode *n = (this) ->  unlink ();
  if (!n) {
    return FALSE;
  }
// deallocate this node
  (this) ->  deleteItem (n -> QLNode::data);
  delete n;
  return TRUE;
}
/*!
  \fn bool QGList::removeFirst()
  \internal
  Removes the first item in the list.
*/
/*!
  \fn bool QGList::removeLast()
  \internal
  Removes the last item in the list.
*/
/*!
  \internal
  Removes the item at position \e index from the list.
*/

bool QGList::removeAt(uint index)
{
  if (!((this) ->  locate (index))) {
    return FALSE;
  }
// unlink node
  class QLNode *n = (this) ->  unlink ();
  if (!n) {
    return FALSE;
  }
// deallocate this node
  (this) ->  deleteItem (n -> QLNode::data);
  delete n;
  return TRUE;
}
/*!
  \internal
  Takes the node \e n out of the list.
*/

QCollection::Item QGList::takeNode(class QLNode *n)
{
#if defined(CHECK_NULL)
  if (n == 0 || (n -> QLNode::prev) && n -> QLNode::prev -> QLNode::next != n || (n -> QLNode::next) && n -> QLNode::next -> QLNode::prev != n) {
    qWarning("QGList::takeNode: Corrupted node");
    return 0;
  }
#endif
  (this) -> curNode = n;
// unlink node
  (this) ->  unlink ();
  Item d = n -> QLNode::data;
// delete the node, not data
  delete n;
  (this) -> curNode = (this) -> firstNode;
  (this) -> curIndex = (((this) -> curNode)?0 : -1);
  return d;
}
/*!
  \internal
  Takes the current item out of the list.
*/

QCollection::Item QGList::take()
{
// unlink node
  class QLNode *n = (this) ->  unlink ();
  Item d = n?n -> QLNode::data : 0;
// delete node, keep contents
  delete n;
  return d;
}
/*!
  \internal
  Takes the item at position \e index out of the list.
*/

QCollection::Item QGList::takeAt(uint index)
{
  if (!((this) ->  locate (index))) {
    return 0;
  }
// unlink node
  class QLNode *n = (this) ->  unlink ();
  Item d = n?n -> QLNode::data : 0;
// delete node, keep contents
  delete n;
  return d;
}
/*!
  \internal
  Takes the first item out of the list.
*/

QCollection::Item QGList::takeFirst()
{
  (this) ->  first ();
// unlink node
  class QLNode *n = (this) ->  unlink ();
  Item d = n?n -> QLNode::data : 0;
  delete n;
  return d;
}
/*!
  \internal
  Takes the last item out of the list.
*/

QCollection::Item QGList::takeLast()
{
  (this) ->  last ();
// unlink node
  class QLNode *n = (this) ->  unlink ();
  Item d = n?n -> QLNode::data : 0;
  delete n;
  return d;
}
/*!
  \internal
  Removes all items from the list.
*/

void QGList::clear()
{
  register class QLNode *n = (this) -> firstNode;
// initialize list
  (this) -> firstNode = (this) -> lastNode = (this) -> curNode = 0;
  (this) -> numNodes = 0;
  (this) -> curIndex = -1;
  if (((this) -> iterators) && (((this) -> iterators) ->  count ())) {
    class QGListIterator *i = (class QGListIterator *)((this) -> iterators ->  first ());
// notify all iterators that
    while(i){
// this list is empty
      i -> QGListIterator::curNode = 0;
      i = ((class QGListIterator *)((this) -> iterators ->  next ()));
    }
  }
  class QLNode *prevNode;
// for all nodes ...
  while(n){
// deallocate data
    (this) ->  deleteItem (n -> QLNode::data);
    prevNode = n;
    n = n -> QLNode::next;
// deallocate node
    delete prevNode;
  }
}
/*!
  \internal
  Finds an item in the list.
*/

int QGList::findRef(Item d,bool fromStart)
{
  register class QLNode *n;
  int index;
// start from first node
  if (fromStart) {
    n = (this) -> firstNode;
    index = 0;
// start from current node
  }
  else {
    n = (this) -> curNode;
    index = (this) -> curIndex;
  }
// find exact match
  while(n && n -> QLNode::data != d){
    n = n -> QLNode::next;
    index++;
  }
  (this) -> curNode = n;
  (this) -> curIndex = (n?index : -1);
// return position of item
  return (this) -> curIndex;
}
/*!
  \internal
  Finds an item in the list.  Uses compareItems().
*/

int QGList::find(Item d,bool fromStart)
{
  register class QLNode *n;
  int index;
// start from first node
  if (fromStart) {
    n = (this) -> firstNode;
    index = 0;
// start from current node
  }
  else {
    n = (this) -> curNode;
    index = (this) -> curIndex;
  }
// find equal match
  while(n && ((this) ->  compareItems (n -> QLNode::data,d))){
    n = n -> QLNode::next;
    index++;
  }
  (this) -> curNode = n;
  (this) -> curIndex = (n?index : -1);
// return position of item
  return (this) -> curIndex;
}
/*!
  \internal
  Counts the number an item occurs in the list.
*/

uint QGList::containsRef(Item d) const
{
  register class QLNode *n = (this) -> firstNode;
  uint count = 0;
// for all nodes...
  while(n){
// count # exact matches
    if (n -> QLNode::data == d) {
      count++;
    }
    n = n -> QLNode::next;
  }
  return count;
}
/*!
  \internal
  Counts the number an item occurs in the list.	 Uses compareItems().
*/

uint QGList::contains(Item d) const
{
  register class QLNode *n = (this) -> firstNode;
  uint count = 0;
// mutable for compareItems()
  class ::QGList *that = (class ::QGList *)(this);
// for all nodes...
  while(n){
// count # equal matches
    if (!(that ->  compareItems (n -> QLNode::data,d))) {
      count++;
    }
    n = n -> QLNode::next;
  }
  return count;
}
/*!
  \fn QCollection::Item QGList::at( uint index )
  \internal
  Sets the item at position \e index to the current item.
*/
/*!
  \fn int QGList::at() const
  \internal
  Returns the current index.
*/
/*!
  \fn QLNode *QGList::currentNode() const
  \internal
  Returns the current node.
*/
/*!
  \fn QCollection::Item QGList::get() const
  \internal
  Returns the current item.
*/
/*!
  \fn QCollection::Item QGList::cfirst() const
  \internal
  Returns the first item in the list.
*/
/*!
  \fn QCollection::Item QGList::clast() const
  \internal
  Returns the last item in the list.
*/
/*!
  \internal
  Returns the first list item.	Sets this to current.
*/

QCollection::Item QGList::first()
{
  if (((this) -> firstNode)) {
    (this) -> curIndex = 0;
    return ((this) -> curNode = (this) -> firstNode) -> QLNode::data;
  }
  return 0;
}
/*!
  \internal
  Returns the last list item.  Sets this to current.
*/

QCollection::Item QGList::last()
{
  if (((this) -> lastNode)) {
    (this) -> curIndex = ((this) -> numNodes - 1);
    return ((this) -> curNode = (this) -> lastNode) -> QLNode::data;
  }
  return 0;
}
/*!
  \internal
  Returns the next list item (after current).  Sets this to current.
*/

QCollection::Item QGList::next()
{
  if (((this) -> curNode)) {
    if (((this) -> curNode -> QLNode::next)) {
      (this) -> curIndex++;
      (this) -> curNode = (this) -> curNode -> QLNode::next;
      return (this) -> curNode -> QLNode::data;
    }
    (this) -> curIndex = -1;
    (this) -> curNode = 0;
  }
  return 0;
}
/*!
  \internal
  Returns the previous list item (before current).  Sets this to current.
*/

QCollection::Item QGList::prev()
{
  if (((this) -> curNode)) {
    if (((this) -> curNode -> QLNode::prev)) {
      (this) -> curIndex--;
      (this) -> curNode = (this) -> curNode -> QLNode::prev;
      return (this) -> curNode -> QLNode::data;
    }
    (this) -> curIndex = -1;
    (this) -> curNode = 0;
  }
  return 0;
}
/*!
  \internal
  Converts the list to a vector.
*/

void QGList::toVector(class QGVector *vector) const
{
  vector ->  clear ();
  if (!vector ->  resize ((this) ->  count ())) {
    return ;
  }
  register class QLNode *n = (this) -> firstNode;
  uint i = 0;
  while(n){
    vector ->  insert (i,n -> QLNode::data);
    n = n -> QLNode::next;
    i++;
  }
}

void QGList::heapSortPushDown(Item *heap,int first,int last)
{
  int r = first;
  while(r <= last / 2){
// Node r has only one child ?
    if (last == 2 * r) {
// Need for swapping ?
      if ((this) ->  compareItems (heap[r],heap[2 * r]) > 0) {
        Item tmp = heap[r];
        heap[r] = heap[2 * r];
        heap[2 * r] = tmp;
      }
// That's it ...
      r = last;
    }
    else {
// Node has two children
      if ((this) ->  compareItems (heap[r],heap[2 * r]) > 0 && (this) ->  compareItems (heap[2 * r],heap[2 * r + 1]) <= 0) {
// Swap with left child
        Item tmp = heap[r];
        heap[r] = heap[2 * r];
        heap[2 * r] = tmp;
        r *= 2;
      }
      else {
        if ((this) ->  compareItems (heap[r],heap[2 * r + 1]) > 0 && (this) ->  compareItems (heap[2 * r + 1],heap[2 * r]) < 0) {
// Swap with right child
          Item tmp = heap[r];
          heap[r] = heap[2 * r + 1];
          heap[2 * r + 1] = tmp;
          r = 2 * r + 1;
        }
        else {
// We are done
          r = last;
        }
      }
    }
  }
}
/*! Sorts the list by the result of the virtual compareItems() function.
  The Heap-Sort algorithm is used for sorting.  It sorts n items with
  O(n*log n) compares.  This is the asymptotic optimal solution of the
  sorting problem.
*/

void QGList::sort()
{
  uint n = (this) ->  count ();
  if (n < 2) {
    return ;
  }
// Create the heap
  Item *realheap = new Item [n * 8UL];
// Wow, what a fake. But I want the heap to be indexed as 1...n
  Item *heap = realheap - 1;
  int size = 0;
  class QLNode *insert = (this) -> firstNode;
  for (; insert != 0; insert = insert -> QLNode::next) {
    heap[++size] = insert -> QLNode::data;
    int i = size;
    while(i > 1 && (this) ->  compareItems (heap[i],heap[i / 2]) < 0){
      Item tmp = heap[i];
      heap[i] = heap[i / 2];
      heap[i / 2] = tmp;
      i /= 2;
    }
  }
  insert = (this) -> firstNode;
// Now do the sorting
  for (int i = n; i > 0; i--) {
    insert -> QLNode::data = heap[1];
    insert = insert -> QLNode::next;
    if (i > 1) {
      heap[1] = heap[i];
      (this) ->  heapSortPushDown (heap,1,i - 1);
    }
  }
  delete []realheap;
}
/*****************************************************************************
  QGList stream functions
 *****************************************************************************/
#ifndef QT_NO_DATASTREAM

class QDataStream &operator>>(class QDataStream &s,class QGList &list)
// read list
{
  return list .  read (s);
}

class QDataStream &operator<<(class QDataStream &s,const class QGList &list)
// write list
{
  return list .  write (s);
}
/*!
  \internal
  Reads a list from the stream \e s.
*/

QDataStream &QGList::read(class QDataStream &s)
{
  uint num;
// read number of items
  s >> num;
// clear list
  (this) ->  clear ();
// read all items
  while((num--)){
    Item d;
    (this) ->  read (s,d);
    qt_check_pointer(d == 0,"qglist.cpp",1000);
// no memory
    if (!d) {
      break; 
    }
    class QLNode *n = new QLNode (d);
    qt_check_pointer(n == 0,"qglist.cpp",1004);
// no memory
    if (!n) {
      break; 
    }
    n -> QLNode::next = 0;
// list is not empty
    if ((n -> QLNode::prev = (this) -> lastNode)) {
      (this) -> lastNode -> QLNode::next = n;
    }
    else {
// initialize list
      (this) -> firstNode = n;
    }
    (this) -> lastNode = n;
    (this) -> numNodes++;
  }
  (this) -> curNode = (this) -> firstNode;
  (this) -> curIndex = (((this) -> curNode)?0 : -1);
  return s;
}
/*!
  \internal
  Writes the list to the stream \e s.
*/

QDataStream &QGList::write(class QDataStream &s) const
{
// write number of items
  s << (this) ->  count ();
  class QLNode *n = (this) -> firstNode;
// write all items
  while(n){
    (this) ->  write (s,n -> QLNode::data);
    n = n -> QLNode::next;
  }
  return s;
}
#endif // QT_NO_DATASTREAM
/*****************************************************************************
  QGListIterator member functions
 *****************************************************************************/
/*!
  \class QGListIterator qglist.h
  \brief The QGListIterator class is an internal class for implementing QListIterator.
  QGListIterator is a strictly internal class that does the heavy work for
  QListIterator.
*/
/*!
  \internal
  Constructs an iterator that operates on the list \e l.
*/

QGListIterator::QGListIterator(const class QGList &l)
{
// get reference to list
  (this) -> list = ((class QGList *)(&l));
// set to first node
  (this) -> curNode = (this) -> list -> QGList::firstNode;
  if (!((this) -> list -> QGList::iterators)) {
// create iterator list
    (this) -> list -> QGList::iterators = (new QGList ());
    qt_check_pointer((this) -> list -> QGList::iterators == 0,"qglist.cpp",1061);
  }
// attach iterator to list
  (this) -> list -> QGList::iterators ->  append ((this));
}
/*!
  \internal
  Constructs a copy of the iterator \e it.
*/

QGListIterator::QGListIterator(const class ::QGListIterator &it)
{
  (this) -> list = it . list;
  (this) -> curNode = it . curNode;
  if (((this) -> list)) {
// attach iterator to list
    (this) -> list -> QGList::iterators ->  append ((this));
  }
}
/*!
  \internal
  Assigns a copy of the iterator \e it and returns a reference to this
  iterator.
*/

QGListIterator &QGListIterator::operator=(const class ::QGListIterator &it)
{
// detach from old list
  if (((this) -> list)) {
    (this) -> list -> QGList::iterators ->  removeRef ((this));
  }
  (this) -> list = it . list;
  (this) -> curNode = it . curNode;
  if (((this) -> list)) {
// attach to new list
    (this) -> list -> QGList::iterators ->  append ((this));
  }
  return  *(this);
}
/*!
  \internal
  Destroys the iterator.
*/

QGListIterator::~QGListIterator()
{
// detach iterator from list
  if (((this) -> list)) {
    (this) -> list -> QGList::iterators ->  removeRef ((this));
  }
}
/*!
  \fn bool QGListIterator::atFirst() const
  \internal
  Returns TRUE if the iterator points to the first item, otherwise FALSE.
*/
/*!
  \fn bool QGListIterator::atLast() const
  \internal
  Returns TRUE if the iterator points to the last item, otherwise FALSE.
*/
/*!
  \internal
  Sets the list iterator to point to the first item in the list.
*/

QCollection::Item QGListIterator::toFirst()
{
  if (!((this) -> list)) {
#if defined(CHECK_NULL)
    qWarning("QGListIterator::toFirst: List has been deleted");
#endif
    return 0;
  }
  return ((this) -> list -> QGList::firstNode)?((this) -> curNode = (this) -> list -> QGList::firstNode) ->  getData () : 0;
}
/*!
  \internal
  Sets the list iterator to point to the last item in the list.
*/

QCollection::Item QGListIterator::toLast()
{
  if (!((this) -> list)) {
#if defined(CHECK_NULL)
    qWarning("QGListIterator::toLast: List has been deleted");
#endif
    return 0;
  }
  return ((this) -> list -> QGList::lastNode)?((this) -> curNode = (this) -> list -> QGList::lastNode) ->  getData () : 0;
}
/*!
  \fn QCollection::Item QGListIterator::get() const
  \internal
  Returns the iterator item.
*/
/*!
  \internal
  Moves to the next item (postfix).
*/

QCollection::Item QGListIterator::operator()()
{
  if (!((this) -> curNode)) {
    return 0;
  }
  QCollection::Item d = (this) -> curNode ->  getData ();
  (this) -> curNode = (this) -> curNode -> QLNode::next;
  return d;
}
/*!
  \internal
  Moves to the next item (prefix).
*/

QCollection::Item QGListIterator::operator++()
{
  if (!((this) -> curNode)) {
    return 0;
  }
  (this) -> curNode = (this) -> curNode -> QLNode::next;
  return ((this) -> curNode)?(this) -> curNode ->  getData () : 0;
}
/*!
  \internal
  Moves \e jumps positions forward.
*/

QCollection::Item QGListIterator::operator+=(uint jumps)
{
  while(((this) -> curNode) && (jumps--))
    (this) -> curNode = (this) -> curNode -> QLNode::next;
  return ((this) -> curNode)?(this) -> curNode ->  getData () : 0;
}
/*!
  \internal
  Moves to the previous item (prefix).
*/

QCollection::Item QGListIterator::operator--()
{
  if (!((this) -> curNode)) {
    return 0;
  }
  (this) -> curNode = (this) -> curNode -> QLNode::prev;
  return ((this) -> curNode)?(this) -> curNode ->  getData () : 0;
}
/*!
  \internal
  Moves \e jumps positions backward.
*/

QCollection::Item QGListIterator::operator-=(uint jumps)
{
  while(((this) -> curNode) && (jumps--))
    (this) -> curNode = (this) -> curNode -> QLNode::prev;
  return ((this) -> curNode)?(this) -> curNode ->  getData () : 0;
}
