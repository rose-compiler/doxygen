/****************************************************************************
** 
**
** Implementation of QXmlSimpleReader and related classes.
**
** Created : 000518
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the XML module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition licenses may use this
** file in accordance with the Qt Commercial License Agreement provided
** with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#define QT_XML_CPP
#include "qxml.h"
#include "qtextcodec.h"
#include "qbuffer.h"
#ifndef QT_NO_XML
// NOT REVISED
// Error strings for the XML reader
#define XMLERR_OK                         "no error occured"
#define XMLERR_TAGMISMATCH                "tag mismatch"
#define XMLERR_UNEXPECTEDEOF              "unexpected end of file"
#define XMLERR_FINISHEDPARSINGWHILENOTEOF "parsing is finished but end of file is not reached"
#define XMLERR_LETTEREXPECTED             "letter is expected"
#define XMLERR_ERRORPARSINGELEMENT        "error while parsing element"
#define XMLERR_ERRORPARSINGPROLOG         "error while parsing prolog"
#define XMLERR_ERRORPARSINGMAINELEMENT    "error while parsing main element"
#define XMLERR_ERRORPARSINGCONTENT        "error while parsing content"
#define XMLERR_ERRORPARSINGNAME           "error while parsing name"
#define XMLERR_ERRORPARSINGNMTOKEN        "error while parsing Nmtoken"
#define XMLERR_ERRORPARSINGATTRIBUTE      "error while parsing attribute"
#define XMLERR_ERRORPARSINGMISC           "error while parsing misc"
#define XMLERR_ERRORPARSINGCHOICE         "error while parsing choice or seq"
#define XMLERR_ERRORBYCONSUMER            "error triggered by consumer"
#define XMLERR_UNEXPECTEDCHARACTER        "unexpected character"
#define XMLERR_EQUALSIGNEXPECTED          "expected '=' but not found"
#define XMLERR_QUOTATIONEXPECTED          "expected \" or ' but not found"
#define XMLERR_ERRORPARSINGREFERENCE      "error while parsing reference"
#define XMLERR_ERRORPARSINGPI             "error while parsing processing instruction"
#define XMLERR_ERRORPARSINGATTLISTDECL    "error while parsing attribute list declaration"
#define XMLERR_ERRORPARSINGATTTYPE        "error while parsing attribute type declaration"
#define XMLERR_ERRORPARSINGATTVALUE       "error while parsing attribute value declaration"
#define XMLERR_ERRORPARSINGELEMENTDECL    "error while parsing element declaration"
#define XMLERR_ERRORPARSINGENTITYDECL     "error while parsing entity declaration"
#define XMLERR_ERRORPARSINGNOTATIONDECL   "error while parsing notation declaration"
#define XMLERR_ERRORPARSINGEXTERNALID     "error while parsing external id"
#define XMLERR_ERRORPARSINGCOMMENT        "error while parsing comment"
#define XMLERR_ERRORPARSINGENTITYVALUE    "error while parsing entity value declaration"
#define XMLERR_CDSECTHEADEREXPECTED       "expected the header for a cdata section"
#define XMLERR_MORETHANONEDOCTYPE         "more than one document type definition"
#define XMLERR_ERRORPARSINGDOCTYPE        "error while parsing document type definition"
#define XMLERR_INVALIDNAMEFORPI           "invalid name for processing instruction"
#define XMLERR_VERSIONEXPECTED            "version expected while reading the XML declaration"
#define XMLERR_EDECLORSDDECLEXPECTED      "EDecl or SDDecl expected while reading the XML declaration"
#define XMLERR_SDDECLEXPECTED             "SDDecl expected while reading the XML declaration"
#define XMLERR_WRONGVALUEFORSDECL         "wrong value for standalone declaration"
#define XMLERR_UNPARSEDENTITYREFERENCE    "unparsed entity reference in wrong context"
#define XMLERR_INTERNALGENERALENTITYINDTD "internal general entity reference not allowed in DTD"
#define XMLERR_EXTERNALGENERALENTITYINDTD "external parsed general entity reference not allowed in DTD"
#define XMLERR_EXTERNALGENERALENTITYINAV  "external parsed general entity reference not allowed in attribute value"
// the constants for the lookup table
// white space
static const signed char cltWS = 0;
// %
static const signed char cltPer = 1;
// &
static const signed char cltAmp = 2;
// >
static const signed char cltGt = 3;
// <
static const signed char cltLt = 4;
// /
static const signed char cltSlash = 5;
// ?
static const signed char cltQm = 6;
// !
static const signed char cltEm = 7;
// -
static const signed char cltDash = 8;
// ]
static const signed char cltCB = 9;
// [
static const signed char cltOB = 10;
// =
static const signed char cltEq = 11;
// "
static const signed char cltDq = 12;
// '
static const signed char cltSq = 13;
static const signed char cltUnknown = 14;
// character lookup table
static const signed char charLookupTable[256] = {(cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltWS), (cltWS), (cltUnknown), (cltUnknown), (cltWS), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltWS), (cltEm), (cltDq), (cltUnknown), (cltUnknown), (cltPer), (cltAmp), (cltSq), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltDash), (cltUnknown), (cltSlash), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltLt), (cltEq), (cltGt), (cltQm), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltOB), (cltUnknown), (cltCB), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown), (cltUnknown)};
// 0x00 - 0x07
// 0x08
// 0x09 \t
// 0x0A \n
// 0x0B
// 0x0C
// 0x0D \r
// 0x0E
// 0x0F
// 0x17 - 0x16
// 0x18 - 0x1F
// 0x20 Space
// 0x21 !
// 0x22 "
// 0x23
// 0x24
// 0x25 %
// 0x26 &
// 0x27 '
// 0x28
// 0x29
// 0x2A
// 0x2B
// 0x2C
// 0x2D -
// 0x2E
// 0x2F /
// 0x30 - 0x37
// 0x38
// 0x39
// 0x3A
// 0x3B
// 0x3C <
// 0x3D =
// 0x3E >
// 0x3F ?
// 0x40 - 0x47
// 0x48 - 0x4F
// 0x50 - 0x57
// 0x58
// 0x59
// 0x5A
// 0x5B [
// 0x5C
// 0x5D ]
// 0x5E
// 0x5F
// 0x60 - 0x67
// 0x68 - 0x6F
// 0x70 - 0x77
// 0x78 - 0x7F
// 0x80 - 0x87
// 0x88 - 0x8F
// 0x90 - 0x97
// 0x98 - 0x9F
// 0xA0 - 0xA7
// 0xA8 - 0xAF
// 0xB0 - 0xB7
// 0xB8 - 0xBF
// 0xC0 - 0xC7
// 0xC8 - 0xCF
// 0xD0 - 0xD7
// 0xD8 - 0xDF
// 0xE0 - 0xE7
// 0xE8 - 0xEF
// 0xF0 - 0xF7
// 0xF8 - 0xFF

class QXmlNamespaceSupportPrivate 
{
}
;

class QXmlAttributesPrivate 
{
}
;

class QXmlInputSourcePrivate 
{
}
;

class QXmlParseExceptionPrivate 
{
}
;

class QXmlLocatorPrivate 
{
}
;

class QXmlDefaultHandlerPrivate 
{
}
;
#if defined(Q_FULL_TEMPLATE_INSTANTIATION)
#endif
/*!
  \class QXmlParseException qxml.h
  \brief The QXmlParseException class is used to report errors with the
  QXmlErrorHandler interface.
  \module XML
  \sa QXmlErrorHandler
*/
/*!
  \fn QXmlParseException::QXmlParseException( const QString& name, int c, int l, const QString& p, const QString& s )
  Constructs a parse exception with the error string \a name in the column
  \a c and line \a l for the public identifier \a p and the system identifier
  \a s.
*/
/*!
  Returns the error message.
*/

QString QXmlParseException::message() const
{
  return ((this) -> msg);
}
/*!
  Returns the column number the error occured.
*/

int QXmlParseException::columnNumber() const
{
  return (this) -> column;
}
/*!
  Returns the line number the error occured.
*/

int QXmlParseException::lineNumber() const
{
  return (this) -> line;
}
/*!
  Returns the public identifier the error occured.
*/

QString QXmlParseException::publicId() const
{
  return ((this) -> pub);
}
/*!
  Returns the system identifier the error occured.
*/

QString QXmlParseException::systemId() const
{
  return ((this) -> sys);
}
/*!
  \class QXmlLocator qxml.h
  \brief The QXmlLocator class provides the XML handler classes with
  information about the actual parsing position.
  \module XML
  The reader reports a QXmlLocator to the content handler before he starts to
  parse the document. This is done with the
  QXmlContentHandler::setDocumentLocator() function. The handler classes can
  now use this locator to get the actual position the reader is at.
*/
/*!
    \fn QXmlLocator::QXmlLocator( QXmlSimpleReader* parent )
    Constructor.
*/
/*!
    \fn QXmlLocator::~QXmlLocator()
    Destructor.
*/
/*!
    Gets the column number (starting with 1) or -1 if there is no column number
    available.
*/

int QXmlLocator::columnNumber()
{
  return (this) -> reader -> QXmlSimpleReader::columnNr == -1?-1 : (this) -> reader -> QXmlSimpleReader::columnNr + 1;
}
/*!
    Gets the line number (starting with 1) or -1 if there is no line number
    available.
*/

int QXmlLocator::lineNumber()
{
  return (this) -> reader -> QXmlSimpleReader::lineNr == -1?-1 : (this) -> reader -> QXmlSimpleReader::lineNr + 1;
}
/*********************************************
 *
 * QXmlNamespaceSupport
 *
 *********************************************/
/*!
  \class QXmlNamespaceSupport qxml.h
  \brief The QXmlNamespaceSupport class is a helper class for XML readers which
  want to include namespace support.
  \module XML
  It provides some functions that makes it easy to handle namespaces. Its main
  use is for subclasses of QXmlReader which want to provide namespace
  support.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/
/*!
  Constructs a QXmlNamespaceSupport.
*/

QXmlNamespaceSupport::QXmlNamespaceSupport()
{
  (this) ->  reset ();
}
/*!
  Destructs a QXmlNamespaceSupport.
*/

QXmlNamespaceSupport::~QXmlNamespaceSupport()
{
}
/*!
  This function declares a prefix in the current namespace context; the prefix
  will remain in force until this context is popped, unless it is shadowed in a
  descendant context.
  Note that there is an asymmetry in this library: while prefix() will not
  return the default "" prefix, even if you have declared one; to check for a
  default prefix, you have to look it up explicitly using uri(). This
  asymmetry exists to make it easier to look up prefixes for attribute names,
  where the default prefix is not allowed.
*/

void QXmlNamespaceSupport::setPrefix(const class QString &pre,const class QString &uri)
{
  if (pre .  isNull ()) {
    (this) -> ns .  insert ("",uri);
  }
  else {
    (this) -> ns .  insert (pre,uri);
  }
}
/*!
  Returns one of the prefixes mapped to a namespace URI.
  If more than one prefix is currently mapped to the same URI, this function
  will make an arbitrary selection; if you want all of the prefixes, use the
  prefixes() function instead.
  Note: this will never return the empty (default) prefix; to check for a
  default prefix, use the uri() function with an argument of "".
*/

QString QXmlNamespaceSupport::prefix(const class QString &uri) const
{
  QMap< QString ,QString > ::ConstIterator itc;
  QMap< QString ,QString > ::ConstIterator it = (this) -> ns .  begin ();
  while((itc = it) != (this) -> ns .  end ()){
     ++ it;
    if (itc .  data ()==uri && !itc .  key () .  isEmpty ()) {
      return (itc .  key ());
    }
  }
  return ("");
}
/*!
  Looks up a prefix in the current context and returns the currently-mapped
  namespace URI. Use the empty string ("") for the default namespace.
*/

QString QXmlNamespaceSupport::uri(const class QString &prefix) const
{
  const class QString &returi = (this) -> ns[prefix];
  return (returi);
}
/*!
  Splits the name at the ':' and returns the prefix and the local name.
*/

void QXmlNamespaceSupport::splitName(const class QString &qname,class QString &prefix,class QString &localname) const
{
  uint pos;
// search the ':'
  for (pos = 0; pos < qname .  length (); pos++) {
    if ((qname .  at (pos)==':')) {
      break; 
    }
  }
// and split
  prefix = qname .  left (pos);
  localname = qname .  mid ((pos + 1));
}
/*!
  Processes a raw XML 1.0 name in the current context by removing the prefix
  and looking it up among the prefixes currently declared.
  First parameter is the raw XML 1.0 name to be processed. The second parameter
  is a flag wheter the name is the name of an attribute (TRUE) or not (FALSE).
  The return values will be stored in the last two parameters as follows:
  <ul>
  <li> The namespace URI, or an empty string if none is in use.
  <li> The local name (without prefix).
  </ul>
  If the raw name has a prefix that has not been declared, then the return
  value will be empty.
  Note that attribute names are processed differently than element names: an
  unprefixed element name will received the default namespace (if any), while
  an unprefixed element name will not
*/

void QXmlNamespaceSupport::processName(const class QString &qname,bool isAttribute,class QString &nsuri,class QString &localname) const
{
  uint pos;
// search the ':'
  for (pos = 0; pos < qname .  length (); pos++) {
    if ((qname .  at (pos)==':')) {
      break; 
    }
  }
  if (pos < qname .  length ()) {
// there was a ':'
    nsuri = (this) ->  uri (qname .  left (pos));
    localname = qname .  mid ((pos + 1));
  }
  else {
// there was no ':'
    if (isAttribute) {
// attributes don't take default namespace
      nsuri = "";
    }
    else {
// get default namespace
      nsuri = (this) ->  uri ("");
    }
    localname = qname;
  }
}
/*!
  Returns an enumeration of all prefixes currently declared.
  Note: if there is a default prefix, it will not be returned in this
  enumeration; check for the default prefix using uri() with an argument
  of "".
*/

QStringList QXmlNamespaceSupport::prefixes() const
{
  class QStringList list;
  QMap< QString ,QString > ::ConstIterator itc;
  QMap< QString ,QString > ::ConstIterator it = (this) -> ns .  begin ();
  while((itc = it) != (this) -> ns .  end ()){
     ++ it;
    if (!itc .  key () .  isEmpty ()) {
      list .  append (itc .  key ());
    }
  }
  return (list);
}
/*!
  Returns a list of all prefixes currently declared for a URI.
  The xml: prefix will be included. If you want only one prefix that's
  mapped to the namespace URI, and you don't care which one you get, use the
  prefix() function instead.
  Note: the empty (default) prefix is never included in this enumeration; to
  check for the presence of a default namespace, use uri() with an
  argument of "".
*/

QStringList QXmlNamespaceSupport::prefixes(const class QString &uri) const
{
  class QStringList list;
  QMap< QString ,QString > ::ConstIterator itc;
  QMap< QString ,QString > ::ConstIterator it = (this) -> ns .  begin ();
  while((itc = it) != (this) -> ns .  end ()){
     ++ it;
    if (itc .  data ()==uri && !itc .  key () .  isEmpty ()) {
      list .  append (itc .  key ());
    }
  }
  return (list);
}
/*!
  Starts a new namespace context.
  Normally, you should push a new context at the beginning of each XML element:
  the new context will automatically inherit the declarations of its parent
  context, but it will also keep track of which declarations were made within
  this context.
*/

void QXmlNamespaceSupport::pushContext()
{
  (this) -> nsStack .  push (((this) -> ns));
}
/*!
  Reverts to the previous namespace context.
  Normally, you should pop the context at the end of each XML element.  After
  popping the context, all namespace prefix mappings that were previously in
  force are restored.
*/

void QXmlNamespaceSupport::popContext()
{
  if (!((this) -> nsStack) .  isEmpty ()) {
    (this) -> ns = (this) -> nsStack .  pop ();
  }
}
/*!
  Resets this namespace support object for reuse.
*/

void QXmlNamespaceSupport::reset()
{
  ((this) -> nsStack) .  clear ();
  (this) -> ns .  clear ();
// the XML namespace
  (this) -> ns .  insert ("xml","http://www.w3.org/XML/1998/namespace");
}
/*********************************************
 *
 * QXmlAttributes
 *
 *********************************************/
/*!
  \class QXmlAttributes qxml.h
  \brief The QXmlAttributes class provides XML attributes.
  \module XML
  If attributes are reported by QXmlContentHandler::startElement() this
  class is used to pass the attribute values. It provides you with different
  functions to access the attribute names and values.
*/
/*!
  \fn QXmlAttributes::QXmlAttributes()
  Constructs an empty attribute list.
*/
/*!
  \fn QXmlAttributes::~QXmlAttributes()
  Destructs attributes.
*/
/*!
  Look up the index of an attribute by an XML 1.0 qualified name.
  Returns the index of the attribute (starting with 0) or -1 if it wasn't
  found.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

int QXmlAttributes::index(const class QString &qName) const
{
  return ((this) -> qnameList) .  findIndex (qName);
}
/*!
  Looks up the index of an attribute by a namespace name.
  \a uri specifies the namespace URI, or the empty string if the name has no
  namespace URI. \a localPart specifies the attribute's local name.
  Returns the index of the attribute (starting with 0) or -1 if it wasn't
  found.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

int QXmlAttributes::index(const class QString &uri,const class QString &localPart) const
{
  uint count = ((this) -> uriList) .  count ();
  for (uint i = 0; i < count; i++) {
    if (((this) -> uriList)[i]==uri && ((this) -> localnameList)[i]==localPart) {
      return i;
    }
  }
  return -1;
}
/*!
  Returns the number of attributes in the list.
*/

int QXmlAttributes::length() const
{
  return (((this) -> valueList) .  count ());
}
/*!
  Looks up an attribute's local name by index (starting with 0).
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

QString QXmlAttributes::localName(int index) const
{
  return (((this) -> localnameList)[index]);
}
/*!
  Looks up an attribute's XML 1.0 qualified name by index (starting with 0).
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

QString QXmlAttributes::qName(int index) const
{
  return (((this) -> qnameList)[index]);
}
/*!
  Looks up an attribute's namespace URI by index (starting with 0).
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

QString QXmlAttributes::uri(int index) const
{
  return (((this) -> uriList)[index]);
}
/*!
  Looks up an attribute's type by index (starting with 0).
  At the moment only 'CDATA' is returned.
*/

QString QXmlAttributes::type(int ) const
{
  return ("CDATA");
}
/*!
  Looks up an attribute's type by XML 1.0 qualified name.
  At the moment only 'CDATA' is returned.
*/

QString QXmlAttributes::type(const class QString &) const
{
  return ("CDATA");
}
/*!
  Looks up an attribute's type by namespace name.
  The first parameter specifies the namespace URI, or the empty string if
  the name has no namespace URI. The second parameter specifies the
  attribute's local name.
  At the moment only 'CDATA' is returned.
*/

QString QXmlAttributes::type(const class QString &,const class QString &) const
{
  return ("CDATA");
}
/*!
  Looks up an attribute's value by index (starting with 0).
*/

QString QXmlAttributes::value(int index) const
{
  return (((this) -> valueList)[index]);
}
/*!
  Looks up an attribute's value by XML 1.0 qualified name.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

QString QXmlAttributes::value(const class QString &qName) const
{
  int i = (this) ->  index (qName);
  if (i == -1) {
    return (QString::null);
  }
  return (((this) -> valueList)[i]);
}
/*!
  Looks up an attribute's value by namespace name.
  \a uri specifies the namespace URI, or the empty string if the name has no
  namespace URI. \a localName specifies the attribute's local name.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
*/

QString QXmlAttributes::value(const class QString &uri,const class QString &localName) const
{
  int i = (this) ->  index (uri,localName);
  if (i == -1) {
    return (QString::null);
  }
  return (((this) -> valueList)[i]);
}
/*********************************************
 *
 * QXmlInputSource
 *
 *********************************************/
/*!
  \class QXmlInputSource qxml.h
  \brief The QXmlInputSource class is the source where XML data is read from.
  \module XML
  All subclasses of QXmlReader read the input from this class.
*/
/*!
  Returns all the data this input source contains.
*/

const QString &QXmlInputSource::data() const
{
  return (this) -> input;
}
/*!
  Constructs a input source which contains no data.
*/

QXmlInputSource::QXmlInputSource()
{
  (this) -> input = "";
}
/*!
  Constructs a input source and get the data from the text stream.
*/

QXmlInputSource::QXmlInputSource(class QTextStream &stream)
{
  QByteArray rawData;
  if ((stream .  device ()) ->  isDirectAccess ()) {
    rawData = stream .  device () ->  readAll ();
  }
  else {
    int nread = 0;
    const int bufsize = 512;
    while(!(stream .  device ()) ->  atEnd ()){
      rawData .  resize ((nread + bufsize));
      nread += stream .  device () ->  readBlock (rawData .  data () + nread,bufsize);
    }
    rawData .  resize (nread);
  }
  (this) ->  readInput (rawData);
}
/*!
  Constructs a input source and get the data from a file. If the file cannot be
  read the input source is empty.
*/

QXmlInputSource::QXmlInputSource(class QFile &file)
{
  if (!file .  open (0x0001)) {
    (this) -> input = "";
    return ;
  }
  QByteArray rawData = file .  readAll ();
  (this) ->  readInput (rawData);
  file .  close ();
}
/*!
  Destructor.
*/

QXmlInputSource::~QXmlInputSource()
{
}
/*!
  Sets the data of the input source to \a dat.
*/

void QXmlInputSource::setData(const class QString &dat)
{
  (this) -> input = dat;
}
/*!
  Read the XML file from the byte array; try to recoginize the encoding.
*/
// ### The input source should not do the encoding detection!

void QXmlInputSource::readInput(QByteArray &rawData)
{
  class QBuffer buf((rawData));
  buf .  open (0x0001);
  class QTextStream *stream = new QTextStream ((&buf));
  class QChar tmp;
// assume UTF8 or UTF16 at first
  stream ->  setEncoding (QTextStream::UnicodeUTF8);
  (this) -> input = "";
// read the first 5 characters
  for (int i = 0; i < 5; i++) {
    ( *stream) >> tmp;
    (this) -> input += tmp;
  }
// starts the document with an XML declaration?
  if ((this) -> input=="<?xml") {
// read the whole XML declaration
    do {
      ( *stream) >> tmp;
      (this) -> input += tmp;
    }while (((tmp)!='>'));
// and try to find out if there is an encoding
    int pos = (this) -> input .  find ("encoding");
    if (pos != -1) {
      class QString encoding;
      do {
        pos++;
        if (pos > ((int )((this) -> input .  length ()))) {
          goto finished;
        }
      }while ((((this) -> input[pos] .  operator QChar ())!='"') && (((this) -> input[pos] .  operator QChar ())!='\''));
      pos++;
      while((((this) -> input[pos] .  operator QChar ())!='"') && (((this) -> input[pos] .  operator QChar ())!='\'')){
        encoding += ((this) -> input[pos] .  operator QChar ());
        pos++;
        if (pos > ((int )((this) -> input .  length ()))) {
          goto finished;
        }
      }
      delete stream;
      stream = (new QTextStream ((&buf)));
      stream ->  setCodec (QTextCodec:: codecForName ((encoding .  utf8 () .  operator const char * ())));
      buf .  reset ();
      (this) -> input = "";
    }
  }
  finished:
  (this) -> input += stream ->  read ();
  delete stream;
  buf .  close ();
}
/*********************************************
 *
 * QXmlDefaultHandler
 *
 *********************************************/
/*!
  \class QXmlContentHandler qxml.h
  \brief The QXmlContentHandler class provides an interface to report logical
  content of XML data.
  \module XML
  If the application needs to be informed of basic parsing events, it
  implements this interface and sets it with QXmlReader::setContentHandler().
  The reader reports basic document-related events like the start and end of
  elements and character data through this interface.
  The order of events in this interface is very important, and mirrors the
  order of information in the document itself. For example, all of an element's
  content (character data, processing instructions, and/or subelements) will
  appear, in order, between the startElement() event and the corresponding
  endElement() event.
  The class QXmlDefaultHandler gives a default implementation for this
  interface; subclassing from this class is very convenient if you want only be
  informed of some parsing events.
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDTDHandler QXmlDeclHandler QXmlEntityResolver QXmlErrorHandler
  QXmlLexicalHandler
*/
/*!
  \fn void QXmlContentHandler::setDocumentLocator( QXmlLocator* locator )
  The reader calls this function before he starts parsing the document. The
  argument \a locator is a pointer to a QXmlLocator which allows the
  application to get the actual position of the parsing in the document.
  Do not destroy the \a locator; it is destroyed when the reader is destroyed
  (do not use the \a locator after the reader got destroyed).
*/
/*!
  \fn bool QXmlContentHandler::startDocument()
  The reader calls this function when he starts parsing the document.
  The reader will call this function only once before any other functions in
  this class or in the QXmlDTDHandler class are called (except
  QXmlContentHandler::setDocumentLocator()).
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  \sa endDocument()
*/
/*!
  \fn bool QXmlContentHandler::endDocument()
  The reader calls this function after he has finished the parsing. It
  is only called once. It is the last function of all handler functions that is
  called. It is called after the reader has read all input or has abandoned
  parsing because of a fatal error.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  \sa startDocument()
*/
/*!
  \fn bool QXmlContentHandler::startPrefixMapping( const QString& prefix, const QString& uri )
  The reader calls this function to signal the begin of a prefix-URI
  namespace mapping scope. This information is not necessary for normal
  namespace processing since the reader automatically replaces prefixes for
  element and attribute names.
  Note that startPrefixMapping and endPrefixMapping calls are not guaranteed to
  be properly nested relative to each-other: all startPrefixMapping events will
  occur before the corresponding startElement event, and all endPrefixMapping
  events will occur after the corresponding endElement event, but their order
  is not otherwise guaranteed.
  The argument \a prefix is the namespace prefix being declared and the
  argument \a uri is the namespace URI the prefix is mapped to.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
  \sa endPrefixMapping()
*/
/*!
  \fn bool QXmlContentHandler::endPrefixMapping( const QString& prefix )
  The reader calls this function to signal the end of a prefix mapping.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
  \sa startPrefixMapping()
*/
/*!
  \fn bool QXmlContentHandler::startElement( const QString& namespaceURI, const QString& localName, const QString& qName, const QXmlAttributes& atts )
  The reader calls this function when he has parsed a start element tag.
  There will be a corresponding endElement() call when the corresponding end
  element tag was read. The startElement() and endElement() calls are always
  nested correctly. Empty element tags (e.g. &lt;a/&gt;) are reported by
  startElement() directly followed by a call to endElement().
  The attribute list provided will contain only attributes with explicit
  values. The attribute list will contain attributes used for namespace
  declaration (i.e. attributes starting with xmlns) only if the
  namespace-prefix property of the reader is TRUE.
  The argument \a uri is the namespace URI, or the empty string if the element
  has no namespace URI or if namespace processing is not being performed, \a
  localName is the local name (without prefix), or the empty string if
  namespace processing is not being performed, \a qName is the qualified name
  (with prefix), or the empty string if qualified names are not available and
  \a atts are the attributes attached to the element. If there are no
  attributes, \a atts is an empty attributes object
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
  \sa endElement()
*/
/*!
  \fn bool QXmlContentHandler::endElement( const QString& namespaceURI, const QString& localName, const QString& qName )
  The reader calls this function when he has parsed an end element tag.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  See also the <a href="xml-sax.html#namespaces">namespace description</a>.
  \sa startElement()
*/
/*!
  \fn bool QXmlContentHandler::characters( const QString& ch )
  The reader calls this function when he has parsed a chunk of character
  data (either normal character data or character data inside a CDATA section;
  if you have to distinguish between those two types you have to use
  QXmlLexicalHandler::startCDATA() and QXmlLexicalHandler::endCDATA() in
  addition).
  Some readers will report whitespace in element content using the
  ignorableWhitespace() function rather than this one (QXmlSimpleReader will
  do it not though).
  A reader is allowed to report the character data of an element in more than
  one chunk; e.g. a reader might want to report "a &amp;lt; b" in three
  characters() events ("a ", "<" and " b").
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlContentHandler::ignorableWhitespace( const QString& ch )
  Some readers may use this function to report each chunk of whitespace in
  element content (QXmlSimpleReader does not though).
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlContentHandler::processingInstruction( const QString& target, const QString& data )
  The reader calls this function when he has parsed a processing
  instruction.
  \a target is the target name of the processing instruction and \a data is the
  data of the processing instruction.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlContentHandler::skippedEntity( const QString& name )
  Some readers may skip entities if they have not seen the declarations (e.g.
  because they are in an external DTD). If they do so they will report it by
  calling this function.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn QString QXmlContentHandler::errorString()
  The reader calls this function to get an error string if any of the handler
  functions returns FALSE to him.
*/
/*!
  \class QXmlErrorHandler qxml.h
  \brief The QXmlErrorHandler class provides an interface to report errors in
  XML data.
  \module XML
  If the application is interested in reporting errors to the user or any other
  customized error handling, you should subclass this class.
  You can set the error handler with QXmlReader::setErrorHandler().
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDTDHandler QXmlDeclHandler QXmlContentHandler QXmlEntityResolver
  QXmlLexicalHandler
*/
/*!
  \fn bool QXmlErrorHandler::warning( const QXmlParseException& exception )
  A reader might use this function to report a warning. Warnings are conditions
  that are not errors or fatal errors as defined by the XML 1.0 specification.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlErrorHandler::error( const QXmlParseException& exception )
  A reader might use this function to report a recoverable error. A recoverable
  error corresponds to the definiton of "error" in section 1.2 of the XML 1.0
  specification.
  The reader must continue to provide normal parsing events after invoking this
  function.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlErrorHandler::fatalError( const QXmlParseException& exception )
  A reader must use this function to report a non-recoverable error.
  If this function returns TRUE the reader might try to go on parsing and
  reporting further errors; but no regular parsing events are reported.
*/
/*!
  \fn QString QXmlErrorHandler::errorString()
  The reader calls this function to get an error string if any of the handler
  functions returns FALSE to him.
*/
/*!
  \class QXmlDTDHandler qxml.h
  \brief The QXmlDTDHandler class provides an interface to report DTD content
  of XML data.
  \module XML
  If an application needs information about notations and unparsed entities,
  then the application implements this interface and registers an instance with
  QXmlReader::setDTDHandler().
  Note that this interface includes only those DTD events that the XML
  recommendation requires processors to report: notation and unparsed entity
  declarations.
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDeclHandler QXmlContentHandler QXmlEntityResolver QXmlErrorHandler
  QXmlLexicalHandler
*/
/*!
  \fn bool QXmlDTDHandler::notationDecl( const QString& name, const QString& publicId, const QString& systemId )
  The reader calls this function when he has parsed a notation
  declaration.
  The argument \a name is the notation name, \a publicId is the notations's
  public identifier and \a systemId is the notations's system identifier.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlDTDHandler::unparsedEntityDecl( const QString& name, const QString& publicId, const QString& systemId, const QString& notationName )
  The reader calls this function when he finds an unparsed entity declaration.
  The argument \a name is the unparsed entity's name, \a publicId is the
  entity's public identifier, \a systemId is the entity's system identifier and
  \a notation is the name of the associated notation.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn QString QXmlDTDHandler::errorString()
  The reader calls this function to get an error string if any of the handler
  functions returns FALSE to him.
*/
/*!
  \class QXmlEntityResolver qxml.h
  \brief The QXmlEntityResolver class provides an interface to resolve extern
  entities contained in XML data.
  \module XML
  If an application needs to implement customized handling for external
  entities, it must implement this interface and register it with
  QXmlReader::setEntityResolver().
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDTDHandler QXmlDeclHandler QXmlContentHandler QXmlErrorHandler
  QXmlLexicalHandler
*/
/*!
  \fn bool QXmlEntityResolver::resolveEntity( const QString& publicId, const QString& systemId, QXmlInputSource* ret )
  The reader will call this function before he opens any external entity,
  except the top-level document entity. The application may request the reader
  to resolve the entity itself (\a ret is 0) or to use an entirely different
  input source (\a ret points to the input source).
  The reader will delete the input source \a ret when he no longer needs it. So
  you should allocate it on the heap with \c new.
  The argument \a publicId is the public identifier of the external entity, \a
  systemId is the system identifier of the external entity and \a ret is the
  return value of this function: if it is 0 the reader should resolve the
  entity itself, if it is non-zero it must point to an input source which the
  reader will use instead.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn QString QXmlEntityResolver::errorString()
  The reader calls this function to get an error string if any of the handler
  functions returns FALSE to him.
*/
/*!
  \class QXmlLexicalHandler qxml.h
  \brief The QXmlLexicalHandler class provides an interface to report lexical
  content of XML data.
  \module XML
  The events in the lexical handler apply to the entire document, not just to
  the document element, and all lexical handler events appear between the
  content handler's startDocument and endDocument events.
  You can set the lexical handler with QXmlReader::setLexicalHandler().
  This interface is designed after the SAX2 extension LexicalHandler. The
  functions startEntity() and endEntity() are not included though.
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDTDHandler QXmlDeclHandler QXmlContentHandler QXmlEntityResolver
  QXmlErrorHandler
*/
/*!
  \fn bool QXmlLexicalHandler::startDTD( const QString& name, const QString& publicId, const QString& systemId )
  The reader calls this function to report the start of a DTD declaration, if
  any.
  All declarations reported through QXmlDTDHandler or QXmlDeclHandler appear
  between the startDTD() and endDTD() calls.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  \sa endDTD()
*/
/*!
  \fn bool QXmlLexicalHandler::endDTD()
  The reader calls this function to report the end of a DTD declaration, if
  any.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  \sa startDTD()
*/
/*!
  \fn bool QXmlLexicalHandler::startCDATA()
  The reader calls this function to report the start of a CDATA section. The
  content of the CDATA section will be reported through the regular
  QXmlContentHandler::characters(). This function is intended only to report
  the boundary.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  \sa endCDATA()
*/
/*!
  \fn bool QXmlLexicalHandler::endCDATA()
  The reader calls this function to report the end of a CDATA section.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
  \sa startCDATA()
*/
/*!
  \fn bool QXmlLexicalHandler::comment( const QString& ch )
  The reader calls this function to report an XML comment anywhere in the
  document.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn QString QXmlLexicalHandler::errorString()
  The reader calls this function to get an error string if any of the handler
  functions returns FALSE to him.
*/
/*!
  \class QXmlDeclHandler qxml.h
  \brief The QXmlDeclHandler class provides an interface to report declaration
  content of XML data.
  \module XML
  You can set the declaration handler with QXmlReader::setDeclHandler().
  This interface is designed after the SAX2 extension DeclHandler.
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDTDHandler QXmlContentHandler QXmlEntityResolver QXmlErrorHandler
  QXmlLexicalHandler
*/
/*!
  \fn bool QXmlDeclHandler::attributeDecl( const QString& eName, const QString& aName, const QString& type, const QString& valueDefault, const QString& value )
  The reader calls this function to report an attribute type declaration. Only
  the effective (first) declaration for an attribute will be reported.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlDeclHandler::internalEntityDecl( const QString& name, const QString& value )
  The reader calls this function to report an internal entity declaration. Only
  the effective (first) declaration will be reported.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn bool QXmlDeclHandler::externalEntityDecl( const QString& name, const QString& publicId, const QString& systemId )
  The reader calls this function to report a parsed external entity
  declaration. Only the effective (first) declaration for each entity will be
  reported.
  If this function returns FALSE the reader will stop parsing and will report
  an error. The reader will use the function errorString() to get the error
  message that will be used for reporting the error.
*/
/*!
  \fn QString QXmlDeclHandler::errorString()
  The reader calls this function to get an error string if any of the handler
  functions returns FALSE to him.
*/
/*!
  \class QXmlDefaultHandler qxml.h
  \brief The QXmlDefaultHandler class provides a default implementation of all
  XML handler classes.
  \module XML
  Very often you are only interested in parts of the things that that the
  reader reports to you. This class simply implements a default behaviour of
  the handler classes (most of the time: do nothing). Normally this is the
  class you subclass for implementing your customized handler.
  See also the <a href="xml.html#introSAX2">Introduction to SAX2</a>.
  \sa QXmlDTDHandler QXmlDeclHandler QXmlContentHandler QXmlEntityResolver
  QXmlErrorHandler QXmlLexicalHandler
*/
/*!
  \fn QXmlDefaultHandler::QXmlDefaultHandler()
  Constructor.
*/
/*!
  \fn QXmlDefaultHandler::~QXmlDefaultHandler()
  Destructor.
*/
/*!
  Does nothing.
*/

void QXmlDefaultHandler::setDocumentLocator(class QXmlLocator *)
{
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::startDocument()
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::endDocument()
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::startPrefixMapping(const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::endPrefixMapping(const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::startElement(const class QString &,const class QString &,const class QString &,const class QXmlAttributes &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::endElement(const class QString &,const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::characters(const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::ignorableWhitespace(const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::processingInstruction(const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::skippedEntity(const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::warning(const class QXmlParseException &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::error(const class QXmlParseException &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::fatalError(const class QXmlParseException &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::notationDecl(const class QString &,const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::unparsedEntityDecl(const class QString &,const class QString &,const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Always sets \a ret to 0, so that the reader will use the system identifier
  provided in the XML document.
*/

bool QXmlDefaultHandler::resolveEntity(const class QString &,const class QString &,class QXmlInputSource *&ret)
{
  ret = 0;
  return TRUE;
}
/*!
  Returns the default error string.
*/

QString QXmlDefaultHandler::errorString()
{
  return QString::QString("error triggered by consumer");
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::startDTD(const class QString &,const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::endDTD()
{
  return TRUE;
}
#if 0
/*!
  Does nothing.
*/
/*!
  Does nothing.
*/
#endif
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::startCDATA()
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::endCDATA()
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::comment(const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::attributeDecl(const class QString &,const class QString &,const class QString &,const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::internalEntityDecl(const class QString &,const class QString &)
{
  return TRUE;
}
/*!
  Does nothing.
*/

bool QXmlDefaultHandler::externalEntityDecl(const class QString &,const class QString &,const class QString &)
{
  return TRUE;
}
/*********************************************
 *
 * QXmlSimpleReaderPrivate
 *
 *********************************************/

class QXmlSimpleReaderPrivate 
{
// constructor
  

  private: inline QXmlSimpleReaderPrivate()
{
  }
// used for entity declarations
  

  struct ExternParameterEntity 
{
    

    inline ExternParameterEntity()
{
    }
    

    inline ExternParameterEntity(const class QString &p,const class QString &s) : publicId(p), systemId(s)
{
    }
    class QString publicId;
    class QString systemId;
  }
;
  

  struct ExternEntity 
{
    

    inline ExternEntity()
{
    }
    

    inline ExternEntity(const class QString &p,const class QString &s,const class QString &n) : publicId(p), systemId(s), notation(n)
{
    }
    class QString publicId;
    class QString systemId;
    class QString notation;
  }
;
  class QMap< QString  , class QXmlSimpleReaderPrivate::ExternParameterEntity  > externParameterEntities;
  class QMap< QString  , QString  > parameterEntities;
  class QMap< QString  , class QXmlSimpleReaderPrivate::ExternEntity  > externEntities;
  class QMap< QString  , QString  > entities;
// used for standalone declaration
  enum Standalone {Yes=0,No=1,Unknown=2} ;
// only used for the doctype
  class QString doctype;
// only used to store the version information
  class QString xmlVersion;
// only used to store the encoding
  class QString encoding;
// used to store the value of the standalone declaration
  enum Standalone standalone;
// used by parseExternalID() to store the public ID
  class QString publicId;
// used by parseExternalID() to store the system ID
  class QString systemId;
// use by parseAttlistDecl()
  class QString attDeclEName;
// use by parseAttlistDecl()
  class QString attDeclAName;
// flags for some features support
  bool useNamespaces;
  bool useNamespacePrefixes;
  bool reportWhitespaceCharData;
// used to build the attribute list
  class QXmlAttributes attList;
// helper classes
  class QXmlLocator *locator;
  class QXmlNamespaceSupport namespaceSupport;
// error string
  class QString error;
// friend declarations
  public: friend class QXmlSimpleReader ;
}
;
/*********************************************
 *
 * QXmlSimpleReader
 *
 *********************************************/
/*!
  \class QXmlReader qxml.h
  \brief The QXmlReader class provides an interface for XML readers (i.e.
  parsers).
  \module XML
  This abstract class describes an interface for all XML readers in Qt. At the
  moment there is only one implementation of a reader included in the XML
  module of Qt (QXmlSimpleReader). In future releases there might be more
  readers with different properties available (e.g. a validating parser).
  The design of the XML classes follow the
  <a href="http://www.megginson.com/SAX/">SAX2 java interface</a>.
  It was adopted to fit into the Qt naming conventions; so it should be very
  easy for anybody who has worked with SAX2 to get started with the Qt XML
  classes.
  All readers use the class QXmlInputSource to read the input document from.
  Since you are normally interested in certain contents of the XML document,
  the reader reports those contents through special handler classes
  (QXmlDTDHandler, QXmlDeclHandler, QXmlContentHandler, QXmlEntityResolver,
  QXmlErrorHandler and QXmlLexicalHandler).
  You have to subclass these classes. Since the handler classes describe only
  interfaces you must implement all functions; there is a class
  (QXmlDefaultHandler) to make this easier; it implements a default behaviour
  (do nothing) for all functions.
  For getting started see also the
  <a href="xml-sax.html#quickStart">Quick start</a>.
  \sa QXmlSimpleReader
*/
/*!
  \fn bool QXmlReader::feature( const QString& name, bool *ok ) const
  If the reader has the feature \a name, this function returns the value of the
  feature.
  If the reader has not the feature \a name, the return value may be anything.
  If \a ok is not 0, then \a ok  is set to TRUE if the reader has the feature
  \a name, otherwise \a ok is set to FALSE.
  \sa setFeature() hasFeature()
*/
/*!
  \fn void QXmlReader::setFeature( const QString& name, bool value )
  Sets the feature \a name to \a value. If the reader has not the feature \a
  name, this value is ignored.
  \sa feature() hasFeature()
*/
/*!
  \fn bool QXmlReader::hasFeature( const QString& name ) const
  Returns \c TRUE if the reader has the feature \a name, otherwise FALSE.
  \sa feature() setFeature()
*/
/*!
  \fn void* QXmlReader::property( const QString& name, bool *ok ) const
  If the reader has the property \a name, this function returns the value of
  the property.
  If the reader has not the property \a name, the return value is 0.
  If \a ok is not 0, then \a ok  is set to TRUE if the reader has the property
  \a name, otherwise \a ok is set to FALSE.
  \sa setProperty() hasProperty()
*/
/*!
  \fn void QXmlReader::setProperty( const QString& name, void* value )
  Sets the property \a name to \a value. If the reader has not the property \a
  name, this value is ignored.
  \sa property() hasProperty()
*/
/*!
  \fn bool QXmlReader::hasProperty( const QString& name ) const
  Returns TRUE if the reader has the property \a name, otherwise FALSE.
  \sa property() setProperty()
*/
/*!
  \fn void QXmlReader::setEntityResolver( QXmlEntityResolver* handler )
  Sets the entity resolver to \a handler.
  \sa entityResolver()
*/
/*!
  \fn QXmlEntityResolver* QXmlReader::entityResolver() const
  Returns the entity resolver or 0 if none was set.
  \sa setEntityResolver()
*/
/*!
  \fn void QXmlReader::setDTDHandler( QXmlDTDHandler* handler )
  Sets the DTD handler to \a handler.
  \sa DTDHandler()
*/
/*!
  \fn QXmlDTDHandler* QXmlReader::DTDHandler() const
  Returns the DTD handler or 0 if none was set.
  \sa setDTDHandler()
*/
/*!
  \fn void QXmlReader::setContentHandler( QXmlContentHandler* handler )
  Sets the content handler to \a handler.
  \sa contentHandler()
*/
/*!
  \fn QXmlContentHandler* QXmlReader::contentHandler() const
  Returns the content handler or 0 if none was set.
  \sa setContentHandler()
*/
/*!
  \fn void QXmlReader::setErrorHandler( QXmlErrorHandler* handler )
  Sets the error handler to \a handler.
  \sa errorHandler()
*/
/*!
  \fn QXmlErrorHandler* QXmlReader::errorHandler() const
  Returns the error handler or 0 if none was set
  \sa setErrorHandler()
*/
/*!
  \fn void QXmlReader::setLexicalHandler( QXmlLexicalHandler* handler )
  Sets the lexical handler to \a handler.
  \sa lexicalHandler()
*/
/*!
  \fn QXmlLexicalHandler* QXmlReader::lexicalHandler() const
  Returns the lexical handler or 0 if none was set.
  \sa setLexicalHandler()
*/
/*!
  \fn void QXmlReader::setDeclHandler( QXmlDeclHandler* handler )
  Sets the declaration handler to \a handler.
  \sa declHandler()
*/
/*!
  \fn QXmlDeclHandler* QXmlReader::declHandler() const
  Returns the declaration handler or 0 if none was set.
  \sa setDeclHandler()
*/
/*!
  \fn bool QXmlReader::parse( const QXmlInputSource& input )
  Parses the XML document \a input. Returns TRUE if the parsing was successful,
  otherwise FALSE.
*/
/*!
  \fn bool QXmlReader::parse( const QString& systemId )
  Parses the XML document at the location \a systemId. Returns TRUE if the
  parsing was successful, otherwise FALSE.
*/
/*!
  \class QXmlSimpleReader qxml.h
  \brief The QXmlSimpleReader class provides an implementation of a simple XML
  reader (i.e. parser).
  \module XML
  This XML reader is sufficient for simple parsing tasks. Here is a short list
  of the properties of this reader:
  <ul>
  <li> well-formed parser
  <li> does not parse any external entities
  <li> can do namespace processing
  </ul>
  For getting started see also the
  <a href="xml-sax.html#quickStart">Quick start</a>.
*/
//guaranteed not to be a characater
const class QChar QXmlSimpleReader::QEOF = QChar::QChar(((ushort )0xffff));
/*!
  Constructs a simple XML reader.
*/

QXmlSimpleReader::QXmlSimpleReader()
{
  (this) -> d = (new QXmlSimpleReaderPrivate ());
  (this) -> d -> QXmlSimpleReaderPrivate::locator = (new QXmlLocator ((this)));
  (this) -> entityRes = 0;
  (this) -> dtdHnd = 0;
  (this) -> contentHnd = 0;
  (this) -> errorHnd = 0;
  (this) -> lexicalHnd = 0;
  (this) -> declHnd = 0;
// default feature settings
  (this) -> d -> QXmlSimpleReaderPrivate::useNamespaces = TRUE;
  (this) -> d -> QXmlSimpleReaderPrivate::useNamespacePrefixes = FALSE;
  (this) -> d -> QXmlSimpleReaderPrivate::reportWhitespaceCharData = TRUE;
}
/*!
  Destroys a simple XML reader.
*/

QXmlSimpleReader::~QXmlSimpleReader()
{
  delete ((this) -> d -> QXmlSimpleReaderPrivate::locator);
  delete ((this) -> d);
}
/*!
  Gets the state of a feature.
  \sa setFeature() hasFeature()
*/

bool QXmlSimpleReader::feature(const class QString &name,bool *ok) const
{
  if (ok != 0) {
     *ok = TRUE;
  }
  if (name=="http://xml.org/sax/features/namespaces") {
    return (this) -> d -> QXmlSimpleReaderPrivate::useNamespaces;
  }
  else {
    if (name=="http://xml.org/sax/features/namespace-prefixes") {
      return (this) -> d -> QXmlSimpleReaderPrivate::useNamespacePrefixes;
    }
    else {
      if (name=="http://trolltech.com/xml/features/report-whitespace-only-CharData") {
        return (this) -> d -> QXmlSimpleReaderPrivate::reportWhitespaceCharData;
      }
      else {
        qWarning("Unknown feature %s",name .  ascii ());
        if (ok != 0) {
           *ok = FALSE;
        }
      }
    }
  }
  return FALSE;
}
/*!
  Sets the state of a feature.
  Supported features are:
  <ul>
  <li> http://xml.org/sax/features/namespaces:
       if this feature is TRUE, namespace processing is performed
  <li> http://xml.org/sax/features/namespace-prefixes:
       if this feature is TRUE, the the original prefixed names and attributes
       used for namespace declarations are reported
  <li> http://trolltech.com/xml/features/report-whitespace-only-CharData:
       if this feature is TRUE, CharData that consists only of whitespace (and
       no other characters) is not reported via
       QXmlContentHandler::characters()
  </ul>
  \sa feature() hasFeature()
*/

void QXmlSimpleReader::setFeature(const class QString &name,bool value)
{
  if (name=="http://xml.org/sax/features/namespaces") {
    (this) -> d -> QXmlSimpleReaderPrivate::useNamespaces = value;
  }
  else {
    if (name=="http://xml.org/sax/features/namespace-prefixes") {
      (this) -> d -> QXmlSimpleReaderPrivate::useNamespacePrefixes = value;
    }
    else {
      if (name=="http://trolltech.com/xml/features/report-whitespace-only-CharData") {
        (this) -> d -> QXmlSimpleReaderPrivate::reportWhitespaceCharData = value;
      }
      else {
        qWarning("Unknown feature %s",name .  ascii ());
      }
    }
  }
}
/*!
  Returns TRUE if the class has a feature named \a feature, otherwise FALSE.
  \sa setFeature() feature()
*/

bool QXmlSimpleReader::hasFeature(const class QString &name) const
{
  if (name=="http://xml.org/sax/features/namespaces" || name=="http://xml.org/sax/features/namespace-prefixes" || name=="http://trolltech.com/xml/features/report-whitespace-only-CharData") {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
/*!
  Returns 0 since this class does not support any properties.
*/

void *QXmlSimpleReader::property(const class QString &,bool *ok) const
{
  if (ok != 0) {
     *ok = FALSE;
  }
  return 0;
}
/*!
  Does nothing since this class does not support any properties.
*/

void QXmlSimpleReader::setProperty(const class QString &,void *)
{
}
/*!
  Returns FALSE since this class does not support any properties.
*/

bool QXmlSimpleReader::hasProperty(const class QString &) const
{
  return FALSE;
}
/*! \reimp */

void QXmlSimpleReader::setEntityResolver(class QXmlEntityResolver *handler)
{
  (this) -> entityRes = handler;
}
/*! \reimp */

QXmlEntityResolver *QXmlSimpleReader::entityResolver() const
{
  return (this) -> entityRes;
}
/*! \reimp */

void QXmlSimpleReader::setDTDHandler(class QXmlDTDHandler *handler)
{
  (this) -> dtdHnd = handler;
}
/*! \reimp */

QXmlDTDHandler *QXmlSimpleReader::DTDHandler() const
{
  return (this) -> dtdHnd;
}
/*! \reimp */

void QXmlSimpleReader::setContentHandler(class QXmlContentHandler *handler)
{
  (this) -> contentHnd = handler;
}
/*! \reimp */

QXmlContentHandler *QXmlSimpleReader::contentHandler() const
{
  return (this) -> contentHnd;
}
/*! \reimp */

void QXmlSimpleReader::setErrorHandler(class QXmlErrorHandler *handler)
{
  (this) -> errorHnd = handler;
}
/*! \reimp */

QXmlErrorHandler *QXmlSimpleReader::errorHandler() const
{
  return (this) -> errorHnd;
}
/*! \reimp */

void QXmlSimpleReader::setLexicalHandler(class QXmlLexicalHandler *handler)
{
  (this) -> lexicalHnd = handler;
}
/*! \reimp */

QXmlLexicalHandler *QXmlSimpleReader::lexicalHandler() const
{
  return (this) -> lexicalHnd;
}
/*! \reimp */

void QXmlSimpleReader::setDeclHandler(class QXmlDeclHandler *handler)
{
  (this) -> declHnd = handler;
}
/*! \reimp */

QXmlDeclHandler *QXmlSimpleReader::declHandler() const
{
  return (this) -> declHnd;
}
/*! \reimp */

bool QXmlSimpleReader::parse(const class QXmlInputSource &input)
{
  (this) ->  init (input);
// call the handler
  if (((this) -> contentHnd)) {
    (this) -> contentHnd ->  setDocumentLocator ((this) -> d -> QXmlSimpleReaderPrivate::locator);
    if (!(this) -> contentHnd ->  startDocument ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
      goto parseError;
    }
  }
// parse prolog
  if (!(this) ->  parseProlog ()) {
    (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing prolog";
    goto parseError;
  }
// parse element
  if (!(this) ->  parseElement ()) {
    (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing main element";
    goto parseError;
  }
// parse Misc*
  while(!(this) ->  atEnd ()){
    if (!(this) ->  parseMisc ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing misc";
      goto parseError;
    }
  }
// is stack empty?
  if (!((this) -> tags) .  isEmpty ()) {
    (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
    goto parseError;
  }
// call the handler
  if (((this) -> contentHnd)) {
    if (!(this) -> contentHnd ->  endDocument ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
      goto parseError;
    }
  }
  return TRUE;
// error handling
  parseError:
  (this) ->  reportParseError ();
  ((this) -> tags) .  clear ();
  return FALSE;
}
/*!
  Parses the prolog [22].
*/

bool QXmlSimpleReader::parseProlog()
{
  bool xmldecl_possible = TRUE;
  bool doctype_read = FALSE;
  const signed char Init = 0;
// eat white spaces
  const signed char EatWS = 1;
// '<' read
  const signed char Lt = 2;
// '!' read
  const signed char Em = 3;
// read doctype
  const signed char DocType = 4;
// read comment
  const signed char Comment = 5;
// read PI
  const signed char PI = 6;
  const signed char Done = 7;
  const signed char InpWs = 0;
// <
  const signed char InpLt = 1;
// ?
  const signed char InpQm = 2;
// !
  const signed char InpEm = 3;
// D
  const signed char InpD = 4;
// -
  const signed char InpDash = 5;
  const signed char InpUnknown = 6;
// use some kind of state machine for parsing
  static signed char table[7][7] = {
/*  InpWs   InpLt  InpQm  InpEm  InpD      InpDash  InpUnknown */
// Init
{(EatWS), (Lt), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// EatWS
{((-1)), (Lt), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Lt
{((-1)), ((-1)), (PI), (Em), (Done), ((-1)), (Done)}, 
// Em
{((-1)), ((-1)), ((-1)), ((-1)), (DocType), (Comment), ((-1))}, 
// DocType
{(EatWS), (Lt), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Comment
{(EatWS), (Lt), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// PI
{(EatWS), (Lt), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// read input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='<')) {
        input = InpLt;
      }
      else {
        if ((((this) -> c)=='?')) {
          input = InpQm;
        }
        else {
          if ((((this) -> c)=='!')) {
            input = InpEm;
          }
          else {
            if ((((this) -> c)=='D')) {
              input = InpD;
            }
            else {
              if ((((this) -> c)=='-')) {
                input = InpDash;
              }
              else {
                input = InpUnknown;
              }
            }
          }
        }
      }
    }
// get new state
    state = table[state][input];
// in some cases do special actions depending on state
    switch(state){
      case EatWS:
{
// XML declaration only on first position possible
        xmldecl_possible = FALSE;
// eat white spaces
        (this) ->  eat_ws ();
        break; 
      }
      case Lt:
{
// next character
        (this) ->  next ();
        break; 
      }
      case Em:
{
// XML declaration only on first position possible
        xmldecl_possible = FALSE;
// next character
        (this) ->  next ();
        break; 
      }
      case DocType:
{
        parseOk = (this) ->  parseDoctype ();
        break; 
      }
      case Comment:
{
        parseOk = (this) ->  parseComment ();
        break; 
      }
      case PI:
{
        parseOk = (this) ->  parsePI (xmldecl_possible);
        break; 
      }
    }
// no input is read after this
    switch(state){
      case DocType:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing prolog";
          goto parseError;
        }
        if (doctype_read) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "more than one document type definition";
          goto parseError;
        }
        else {
          doctype_read = FALSE;
        }
        break; 
      }
      case Comment:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing prolog";
          goto parseError;
        }
        if (((this) -> lexicalHnd)) {
          if (!(this) -> lexicalHnd ->  comment ((this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
            goto parseError;
          }
        }
        break; 
      }
      case PI:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing prolog";
          goto parseError;
        }
// call the handler
        if (((this) -> contentHnd)) {
          if (xmldecl_possible && !(this) -> d -> QXmlSimpleReaderPrivate::xmlVersion .  isEmpty ()) {
            class QString value("version = '");
            value += ((this) -> d -> QXmlSimpleReaderPrivate::xmlVersion);
            value += "'";
            if (!(this) -> d -> QXmlSimpleReaderPrivate::encoding .  isEmpty ()) {
              value += " encoding = '";
              value += ((this) -> d -> QXmlSimpleReaderPrivate::encoding);
              value += "'";
            }
            if (((this) -> d -> QXmlSimpleReaderPrivate::standalone) == QXmlSimpleReaderPrivate::Yes) {
              value += " standalone = 'yes'";
            }
            else {
              if (((this) -> d -> QXmlSimpleReaderPrivate::standalone) == QXmlSimpleReaderPrivate::No) {
                value += " standalone = 'no'";
              }
            }
            if (!(this) -> contentHnd ->  processingInstruction ("xml",value)) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
              goto parseError;
            }
          }
          else {
            if (!(this) -> contentHnd ->  processingInstruction ((this) ->  name (),(this) ->  string ())) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
              goto parseError;
            }
          }
        }
// XML declaration only on first position possible
        xmldecl_possible = FALSE;
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
        (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing element";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse an element [39].
  Precondition: the opening '<' is already read.
*/

bool QXmlSimpleReader::parseElement()
{
  static class QString uri;
  static class QString lname;
  static class QString prefix;
  static bool t;
  const signed char Init = 0;
  const signed char ReadName = 1;
  const signed char Ws1 = 2;
  const signed char STagEnd = 3;
  const signed char STagEnd2 = 4;
  const signed char ETagBegin = 5;
  const signed char ETagBegin2 = 6;
  const signed char Ws2 = 7;
  const signed char EmptyTag = 8;
  const signed char Attribute = 9;
  const signed char Ws3 = 10;
  const signed char Done = 11;
// whitespace
  const signed char InpWs = 0;
// is_NameBeginning()
  const signed char InpNameBe = 1;
// >
  const signed char InpGt = 2;
// /
  const signed char InpSlash = 3;
  const signed char InpUnknown = 4;
// use some kind of state machine for parsing
  static signed char table[11][5] = {
/*  InpWs      InpNameBe    InpGt        InpSlash     InpUnknown */
// Init
{((-1)), (ReadName), ((-1)), ((-1)), ((-1))}, 
// ReadName
{(Ws1), (Attribute), (STagEnd), (EmptyTag), ((-1))}, 
// Ws1
{((-1)), (Attribute), (STagEnd), (EmptyTag), ((-1))}, 
// STagEnd
{(STagEnd2), (STagEnd2), (STagEnd2), (STagEnd2), (STagEnd2)}, 
// STagEnd2
{((-1)), ((-1)), ((-1)), (ETagBegin), ((-1))}, 
// ETagBegin
{((-1)), (ETagBegin2), ((-1)), ((-1)), ((-1))}, 
// ETagBegin2
{(Ws2), ((-1)), (Done), ((-1)), ((-1))}, 
// Ws2
{((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// EmptyTag
{((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// Attribute
{(Ws3), (Attribute), (STagEnd), (EmptyTag), ((-1))}, 
// Ws3
{((-1)), (Attribute), (STagEnd), (EmptyTag), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// read input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((this) ->  is_NameBeginning ((this) -> c)) {
        input = InpNameBe;
      }
      else {
        if ((((this) -> c)=='>')) {
          input = InpGt;
        }
        else {
          if ((((this) -> c)=='/')) {
            input = InpSlash;
          }
          else {
            input = InpUnknown;
          }
        }
      }
    }
// get new state
//qDebug( "%d -%d(%c)-> %d", state, input, c.latin1(), table[state][input] );
    state = table[state][input];
// in some cases do special actions depending on state
    switch(state){
      case ReadName:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws1:
{
      }
      case Ws2:
{
      }
      case Ws3:
{
        (this) ->  eat_ws ();
        break; 
      }
      case STagEnd:
{
// call the handler
        if (((this) -> contentHnd)) {
          if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
            (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  processName ((this) -> tags .  top (),FALSE,uri,lname);
            t = (this) -> contentHnd ->  startElement (uri,lname,(this) -> tags .  top (),(this) -> d -> QXmlSimpleReaderPrivate::attList);
          }
          else {
            t = (this) -> contentHnd ->  startElement ("","",(this) -> tags .  top (),(this) -> d -> QXmlSimpleReaderPrivate::attList);
          }
          if (!t) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
            goto parseError;
          }
        }
        (this) ->  next ();
        break; 
      }
      case STagEnd2:
{
        parseOk = (this) ->  parseContent ();
        break; 
      }
      case ETagBegin:
{
        (this) ->  next ();
        break; 
      }
      case ETagBegin2:
{
// get the name of the tag
        parseOk = (this) ->  parseName ();
        break; 
      }
      case EmptyTag:
{
        if (((this) -> tags) .  isEmpty ()) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "tag mismatch";
          goto parseError;
        }
        if (!(this) ->  parseElementEmptyTag (t,uri,lname)) {
          goto parseError;
        }
// next character
        (this) ->  next ();
        break; 
      }
      case Attribute:
{
// get name and value of attribute
        parseOk = (this) ->  parseAttribute ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case ReadName:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
// store it on the stack
        (this) -> tags .  push ((this) ->  name ());
// empty the attributes
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::qnameList .  clear ();
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::uriList .  clear ();
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::localnameList .  clear ();
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::valueList .  clear ();
// namespace support?
        if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
          (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  pushContext ();
        }
        break; 
      }
      case STagEnd2:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing content";
          goto parseError;
        }
        break; 
      }
      case ETagBegin2:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        if (!(this) ->  parseElementETagBegin2 (uri,lname)) {
          goto parseError;
        }
        break; 
      }
      case Attribute:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing attribute";
          goto parseError;
        }
        if (!(this) ->  parseElementAttribute (prefix,uri,lname)) {
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
        (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing element";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Helper to break down the size of the code in the case statement.
  Return FALSE on error, otherwise TRUE.
*/

bool QXmlSimpleReader::parseElementEmptyTag(bool &t,class QString &uri,class QString &lname)
{
// pop the stack and call the handler
  if (((this) -> contentHnd)) {
// report startElement first...
    if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
      (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  processName ((this) -> tags .  top (),FALSE,uri,lname);
      t = (this) -> contentHnd ->  startElement (uri,lname,(this) -> tags .  top (),(this) -> d -> QXmlSimpleReaderPrivate::attList);
    }
    else {
      t = (this) -> contentHnd ->  startElement ("","",(this) -> tags .  top (),(this) -> d -> QXmlSimpleReaderPrivate::attList);
    }
    if (!t) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
      return FALSE;
    }
// ... followed by endElement
    if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
      if (!(this) -> contentHnd ->  endElement (uri,lname,(this) -> tags .  pop ())) {
        (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
        return FALSE;
      }
    }
    else {
      if (!(this) -> contentHnd ->  endElement ("","",(this) -> tags .  pop ())) {
        (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
        return FALSE;
      }
    }
// namespace support?
    if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
      class QStringList prefixesBefore;
      class QStringList prefixesAfter;
      if (((this) -> contentHnd)) {
        prefixesBefore = (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  prefixes ();
      }
      (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  popContext ();
// call the handler for prefix mapping
      if (((this) -> contentHnd)) {
        prefixesAfter = (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  prefixes ();
        for (QValueList< QString > ::Iterator it = prefixesBefore .  begin (); it != prefixesBefore .  end ();  ++ it) {
          if (prefixesAfter .  contains ( * it) == 0) {
            if (!(this) -> contentHnd ->  endPrefixMapping ( * it)) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
              return FALSE;
            }
          }
        }
      }
    }
  }
  else {
    (this) -> tags .  pop ();
  }
  return TRUE;
}
/*!
  Helper to break down the size of the code in the case statement.
  Return FALSE on error, otherwise TRUE.
*/

bool QXmlSimpleReader::parseElementETagBegin2(class QString &uri,class QString &lname)
{
// pop the stack and compare it with the name
  if ((this) -> tags .  pop ()!=(this) ->  name ()) {
    (this) -> d -> QXmlSimpleReaderPrivate::error = "tag mismatch";
    return FALSE;
  }
// call the handler
  if (((this) -> contentHnd)) {
    if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
      (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  processName ((this) ->  name (),FALSE,uri,lname);
      if (!(this) -> contentHnd ->  endElement (uri,lname,(this) ->  name ())) {
        (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
        return FALSE;
      }
    }
    else {
      if (!(this) -> contentHnd ->  endElement ("","",(this) ->  name ())) {
        (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
        return FALSE;
      }
    }
  }
// namespace support?
  if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
    class QStringList prefixesBefore;
    class QStringList prefixesAfter;
    if (((this) -> contentHnd)) {
      prefixesBefore = (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  prefixes ();
    }
    (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  popContext ();
// call the handler for prefix mapping
    if (((this) -> contentHnd)) {
      prefixesAfter = (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  prefixes ();
      for (QValueList< QString > ::Iterator it = prefixesBefore .  begin (); it != prefixesBefore .  end ();  ++ it) {
        if (prefixesAfter .  contains ( * it) == 0) {
          if (!(this) -> contentHnd ->  endPrefixMapping ( * it)) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
            return FALSE;
          }
        }
      }
    }
  }
  return TRUE;
}
/*!
  Helper to break down the size of the code in the case statement.
  Return FALSE on error, otherwise TRUE.
*/

bool QXmlSimpleReader::parseElementAttribute(class QString &prefix,class QString &uri,class QString &lname)
{
// add the attribute to the list
  if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespaces) {
// is it a namespace declaration?
    (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  splitName ((this) ->  name (),prefix,lname);
    if (prefix=="xmlns") {
// namespace declaration
      (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  setPrefix (lname,(this) ->  string ());
      if ((this) -> d -> QXmlSimpleReaderPrivate::useNamespacePrefixes) {
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::qnameList .  append ((this) ->  name ());
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::uriList .  append ("");
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::localnameList .  append ("");
        (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::valueList .  append ((this) ->  string ());
      }
// call the handler for prefix mapping
      if (((this) -> contentHnd)) {
        if (!(this) -> contentHnd ->  startPrefixMapping (lname,(this) ->  string ())) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
          return FALSE;
        }
      }
    }
    else {
// no namespace delcaration
      (this) -> d -> QXmlSimpleReaderPrivate::namespaceSupport .  processName ((this) ->  name (),TRUE,uri,lname);
      (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::qnameList .  append ((this) ->  name ());
      (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::uriList .  append (uri);
      (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::localnameList .  append (lname);
      (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::valueList .  append ((this) ->  string ());
    }
  }
  else {
// no namespace support
    (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::qnameList .  append ((this) ->  name ());
    (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::uriList .  append ("");
    (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::localnameList .  append ("");
    (this) -> d -> QXmlSimpleReaderPrivate::attList . QXmlAttributes::valueList .  append ((this) ->  string ());
  }
  return TRUE;
}
/*!
  Parse a content [43].
  A content is only used between tags. If a end tag is found the < is already
  read and the head stand on the '/' of the end tag '</name>'.
*/

bool QXmlSimpleReader::parseContent()
{
  bool charDataRead = FALSE;
  const signed char Init = 0;
// CharData
  const signed char ChD = 1;
// CharData help state
  const signed char ChD1 = 2;
// CharData help state
  const signed char ChD2 = 3;
// Reference
  const signed char Ref = 4;
// '<' read
  const signed char Lt = 5;
// PI
  const signed char PI = 6;
// Element
  const signed char Elem = 7;
// '!' read
  const signed char Em = 8;
// Comment
  const signed char Com = 9;
// CDSect
  const signed char CDS = 10;
// read a CDSect
  const signed char CDS1 = 11;
// read a CDSect (help state)
  const signed char CDS2 = 12;
// read a CDSect (help state)
  const signed char CDS3 = 13;
// finished reading content
  const signed char Done = 14;
// <
  const signed char InpLt = 0;
// >
  const signed char InpGt = 1;
// /
  const signed char InpSlash = 2;
// ?
  const signed char InpQMark = 3;
// !
  const signed char InpEMark = 4;
// &
  const signed char InpAmp = 5;
// -
  const signed char InpDash = 6;
// [
  const signed char InpOpenB = 7;
// ]
  const signed char InpCloseB = 8;
  const signed char InpUnknown = 9;
  static signed char mapCLT2FSMChar[] = {(InpUnknown), (InpUnknown), (InpAmp), (InpGt), (InpLt), (InpSlash), (InpQMark), (InpEMark), (InpDash), (InpCloseB), (InpOpenB), (InpUnknown), (InpUnknown), (InpUnknown), (InpUnknown)};
// white space
// %
// &
// >
// <
// /
// ?
// !
// -
// ]
// [
// =
// "
// '
// unknown
// use some kind of state machine for parsing
  static const signed char table[14][10] = {
/*  InpLt  InpGt  InpSlash  InpQMark  InpEMark  InpAmp  InpDash  InpOpenB  InpCloseB  InpUnknown */
// Init
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD1), (ChD)}, 
// ChD
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD1), (ChD)}, 
// ChD1
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD2), (ChD)}, 
// ChD2
{(Lt), ((-1)), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD2), (ChD)}, 
// Ref (same as Init)
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD), (ChD)}, 
// Lt
{((-1)), ((-1)), (Done), (PI), (Em), ((-1)), ((-1)), ((-1)), ((-1)), (Elem)}, 
// PI (same as Init)
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD), (ChD)}, 
// Elem (same as Init)
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD), (ChD)}, 
// Em
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Com), (CDS), ((-1)), ((-1))}, 
// Com (same as Init)
{(Lt), (ChD), (ChD), (ChD), (ChD), (Ref), (ChD), (ChD), (ChD), (ChD)}, 
// CDS
{(CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS2), (CDS1)}, 
// CDS1
{(CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS2), (CDS1)}, 
// CDS2
{(CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS3), (CDS1)}, 
// CDS3
{(CDS1), (Init), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS1), (CDS3), (CDS1)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input (use lookup-table instead of nested ifs for performance
// reasons)
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if (((this) -> c .  row ())) {
      input = InpUnknown;
    }
    else {
      input = mapCLT2FSMChar[charLookupTable[(this) -> c .  cell ()]];
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Init:
{
// next character
        (this) ->  next ();
        break; 
      }
      case ChD:
{
// on first call: clear string
        if (!charDataRead) {
          charDataRead = TRUE;
          (this) ->  stringClear ();
        }
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case ChD1:
{
// on first call: clear string
        if (!charDataRead) {
          charDataRead = TRUE;
          (this) ->  stringClear ();
        }
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case ChD2:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case Ref:
{
        if (!charDataRead) {
// reference may be CharData; so clear string to be safe
          (this) ->  stringClear ();
          parseOk = (this) ->  parseReference (charDataRead,InContent);
        }
        else {
          bool tmp;
          parseOk = (this) ->  parseReference (tmp,InContent);
        }
        break; 
      }
      case Lt:
{
// call the handler for CharData
        if (((this) -> contentHnd)) {
          if (charDataRead) {
            if ((this) -> d -> QXmlSimpleReaderPrivate::reportWhitespaceCharData || !(this) ->  string () .  simplifyWhiteSpace () .  isEmpty ()) {
              if (!(this) -> contentHnd ->  characters ((this) ->  string ())) {
                (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
                goto parseError;
              }
            }
          }
        }
        charDataRead = FALSE;
// next character
        (this) ->  next ();
        break; 
      }
      case PI:
{
        parseOk = (this) ->  parsePI ();
        break; 
      }
      case Elem:
{
        parseOk = (this) ->  parseElement ();
        break; 
      }
      case Em:
{
// next character
        (this) ->  next ();
        break; 
      }
      case Com:
{
        parseOk = (this) ->  parseComment ();
        break; 
      }
      case CDS:
{
        parseOk = (this) ->  parseString ("[CDATA[");
        break; 
      }
      case CDS1:
{
// read one character and add it
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case CDS2:
{
// skip ']'
        (this) ->  next ();
        break; 
      }
      case CDS3:
{
// skip ']'...
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Ref:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing reference";
          goto parseError;
        }
        break; 
      }
      case PI:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing processing instruction";
          goto parseError;
        }
// call the handler
        if (((this) -> contentHnd)) {
          if (!(this) -> contentHnd ->  processingInstruction ((this) ->  name (),(this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
            goto parseError;
          }
        }
        break; 
      }
      case Elem:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing element";
          goto parseError;
        }
        break; 
      }
      case Com:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing comment";
          goto parseError;
        }
        if (((this) -> lexicalHnd)) {
          if (!(this) -> lexicalHnd ->  comment ((this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
            goto parseError;
          }
        }
        break; 
      }
      case CDS:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "expected the header for a cdata section";
          goto parseError;
        }
// empty string
        (this) ->  stringClear ();
        break; 
      }
      case CDS2:
{
        if ((((this) -> c)!=']')) {
          (this) ->  stringAddC (']');
        }
        break; 
      }
      case CDS3:
{
// test if this skipping was legal
        if ((((this) -> c)=='>')) {
// the end of the CDSect
          if (((this) -> lexicalHnd)) {
            if (!(this) -> lexicalHnd ->  startCDATA ()) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
              goto parseError;
            }
          }
          if (((this) -> contentHnd)) {
            if (!(this) -> contentHnd ->  characters ((this) ->  string ())) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
              goto parseError;
            }
          }
          if (((this) -> lexicalHnd)) {
            if (!(this) -> lexicalHnd ->  endCDATA ()) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        else {
          if ((((this) -> c)==']')) {
// three or more ']'
            (this) ->  stringAddC (']');
          }
          else {
// after ']]' comes another character
            (this) ->  stringAddC (']');
            (this) ->  stringAddC (']');
          }
        }
        break; 
      }
      case Done:
{
// call the handler for CharData
        if (((this) -> contentHnd)) {
          if (charDataRead) {
            if ((this) -> d -> QXmlSimpleReaderPrivate::reportWhitespaceCharData || !(this) ->  string () .  simplifyWhiteSpace () .  isEmpty ()) {
              if (!(this) -> contentHnd ->  characters ((this) ->  string ())) {
                (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
                goto parseError;
              }
            }
          }
        }
// Done
        return TRUE;
      }
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing content";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse Misc [27].
*/

bool QXmlSimpleReader::parseMisc()
{
  const signed char Init = 0;
// '<' was read
  const signed char Lt = 1;
// read comment
  const signed char Comment = 2;
// eat whitespaces
  const signed char eatWS = 3;
// read PI
  const signed char PI = 4;
// read comment
  const signed char Comment2 = 5;
// S
  const signed char InpWs = 0;
// <
  const signed char InpLt = 1;
// ?
  const signed char InpQm = 2;
// !
  const signed char InpEm = 3;
  const signed char InpUnknown = 4;
// use some kind of state machine for parsing
  static signed char table[3][5] = {
/*  InpWs   InpLt  InpQm  InpEm     InpUnknown */
// Init
{(eatWS), (Lt), ((-1)), ((-1)), ((-1))}, 
// Lt
{((-1)), ((-1)), (PI), (Comment), ((-1))}, 
// Comment
{((-1)), ((-1)), ((-1)), ((-1)), (Comment2)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='<')) {
        input = InpLt;
      }
      else {
        if ((((this) -> c)=='?')) {
          input = InpQm;
        }
        else {
          if ((((this) -> c)=='!')) {
            input = InpEm;
          }
          else {
            input = InpUnknown;
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case eatWS:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Lt:
{
        (this) ->  next ();
        break; 
      }
      case PI:
{
        parseOk = (this) ->  parsePI ();
        break; 
      }
      case Comment:
{
        (this) ->  next ();
        break; 
      }
      case Comment2:
{
        parseOk = (this) ->  parseComment ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case eatWS:
      return TRUE;
      case PI:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing processing instruction";
          goto parseError;
        }
        if (((this) -> contentHnd)) {
          if (!(this) -> contentHnd ->  processingInstruction ((this) ->  name (),(this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
            goto parseError;
          }
        }
        return TRUE;
      }
      case Comment2:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing comment";
          goto parseError;
        }
        if (((this) -> lexicalHnd)) {
          if (!(this) -> lexicalHnd ->  comment ((this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
            goto parseError;
          }
        }
        return TRUE;
      }
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a processing instruction [16].
  If xmldec is TRUE, it tries to parse a PI or a XML declaration [23].
  Precondition: the beginning '<' of the PI is already read and the head stand
  on the '?' of '<?'.
  If this funktion was successful, the head-position is on the first
  character after the PI.
*/

bool QXmlSimpleReader::parsePI(bool xmldecl)
{
  const signed char Init = 0;
// ? was read
  const signed char QmI = 1;
// read Name
  const signed char Name = 2;
// read XMLDecl
  const signed char XMLDecl = 3;
// eat ws after "xml" of XMLDecl
  const signed char Ws1 = 4;
// read PI
  const signed char PI = 5;
// eat ws after Name of PI
  const signed char Ws2 = 6;
// read versionInfo
  const signed char Version = 7;
// eat ws after versionInfo
  const signed char Ws3 = 8;
// read EDecl or SDDecl
  const signed char EorSD = 9;
// eat ws after EDecl or SDDecl
  const signed char Ws4 = 10;
// read SDDecl
  const signed char SD = 11;
// eat ws after SDDecl
  const signed char Ws5 = 12;
// almost done
  const signed char ADone = 13;
// Char was read
  const signed char Char = 14;
// Qm was read
  const signed char Qm = 15;
// finished reading content
  const signed char Done = 16;
// whitespace
  const signed char InpWs = 0;
// is_nameBeginning()
  const signed char InpNameBe = 1;
// >
  const signed char InpGt = 2;
// ?
  const signed char InpQm = 3;
  const signed char InpUnknown = 4;
// use some kind of state machine for parsing
  static signed char table[16][5] = {
/*  InpWs,  InpNameBe  InpGt  InpQm   InpUnknown  */
// Init
{((-1)), ((-1)), ((-1)), (QmI), ((-1))}, 
// QmI
{((-1)), (Name), ((-1)), ((-1)), ((-1))}, 
// Name (this state is left not through input)
{((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// XMLDecl
{(Ws1), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws1
{((-1)), (Version), ((-1)), ((-1)), ((-1))}, 
// PI
{(Ws2), ((-1)), ((-1)), (Qm), ((-1))}, 
// Ws2
{(Char), (Char), (Char), (Qm), (Char)}, 
// Version
{(Ws3), ((-1)), ((-1)), (ADone), ((-1))}, 
// Ws3
{((-1)), (EorSD), ((-1)), (ADone), ((-1))}, 
// EorSD
{(Ws4), ((-1)), ((-1)), (ADone), ((-1))}, 
// Ws4
{((-1)), (SD), ((-1)), (ADone), ((-1))}, 
// SD
{(Ws5), ((-1)), ((-1)), (ADone), ((-1))}, 
// Ws5
{((-1)), ((-1)), ((-1)), (ADone), ((-1))}, 
// ADone
{((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// Char
{(Char), (Char), (Char), (Qm), (Char)}, 
// Qm
{(Char), (Char), (Done), (Qm), (Char)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((this) ->  is_NameBeginning ((this) -> c)) {
        input = InpNameBe;
      }
      else {
        if ((((this) -> c)=='>')) {
          input = InpGt;
        }
        else {
          if ((((this) -> c)=='?')) {
            input = InpQm;
          }
          else {
            input = InpUnknown;
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case QmI:
{
        (this) ->  next ();
        break; 
      }
      case Name:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws1:
{
      }
      case Ws2:
{
      }
      case Ws3:
{
      }
      case Ws4:
{
      }
      case Ws5:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Version:
{
        parseOk = (this) ->  parseAttribute ();
        break; 
      }
      case EorSD:
{
        parseOk = (this) ->  parseAttribute ();
        break; 
      }
      case SD:
{
// get the SDDecl (syntax like an attribute)
        if (((this) -> d -> QXmlSimpleReaderPrivate::standalone) != QXmlSimpleReaderPrivate::Unknown) {
// already parsed the standalone declaration
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        parseOk = (this) ->  parseAttribute ();
        break; 
      }
      case ADone:
{
        (this) ->  next ();
        break; 
      }
      case Char:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case Qm:
{
// skip the '?'
        (this) ->  next ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Name:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
// test what name was read and determine the next state
// (not very beautiful, I admit)
        if ((this) ->  name () .  lower ()=="xml") {
          if (xmldecl && (this) ->  name ()=="xml") {
            state = XMLDecl;
          }
          else {
            (this) -> d -> QXmlSimpleReaderPrivate::error = "invalid name for processing instruction";
            goto parseError;
          }
        }
        else {
          state = PI;
          (this) ->  stringClear ();
        }
        break; 
      }
      case Version:
{
// get version (syntax like an attribute)
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "version expected while reading the XML declaration";
          goto parseError;
        }
        if ((this) ->  name ()!="version") {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "version expected while reading the XML declaration";
          goto parseError;
        }
        (this) -> d -> QXmlSimpleReaderPrivate::xmlVersion = (this) ->  string ();
        break; 
      }
      case EorSD:
{
// get the EDecl or SDDecl (syntax like an attribute)
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "EDecl or SDDecl expected while reading the XML declaration";
          goto parseError;
        }
        if ((this) ->  name ()=="standalone") {
          if ((this) ->  string ()=="yes") {
            (this) -> d -> QXmlSimpleReaderPrivate::standalone = QXmlSimpleReaderPrivate::Yes;
          }
          else {
            if ((this) ->  string ()=="no") {
              (this) -> d -> QXmlSimpleReaderPrivate::standalone = QXmlSimpleReaderPrivate::No;
            }
            else {
              (this) -> d -> QXmlSimpleReaderPrivate::error = "wrong value for standalone declaration";
              goto parseError;
            }
          }
        }
        else {
          if ((this) ->  name ()=="encoding") {
            (this) -> d -> QXmlSimpleReaderPrivate::encoding = (this) ->  string ();
          }
          else {
            (this) -> d -> QXmlSimpleReaderPrivate::error = "EDecl or SDDecl expected while reading the XML declaration";
            goto parseError;
          }
        }
        break; 
      }
      case SD:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "SDDecl expected while reading the XML declaration";
          goto parseError;
        }
        if ((this) ->  name ()!="standalone") {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "SDDecl expected while reading the XML declaration";
          goto parseError;
        }
        if ((this) ->  string ()=="yes") {
          (this) -> d -> QXmlSimpleReaderPrivate::standalone = QXmlSimpleReaderPrivate::Yes;
        }
        else {
          if ((this) ->  string ()=="no") {
            (this) -> d -> QXmlSimpleReaderPrivate::standalone = QXmlSimpleReaderPrivate::No;
          }
          else {
            (this) -> d -> QXmlSimpleReaderPrivate::error = "wrong value for standalone declaration";
            goto parseError;
          }
        }
        break; 
      }
      case Qm:
{
// test if the skipping was legal
        if ((((this) -> c)!='>')) {
          (this) ->  stringAddC ('?');
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a document type definition (doctypedecl [28]).
  Precondition: the beginning '<!' of the doctype is already read the head
  stands on the 'D' of '<!DOCTYPE'.
  If this funktion was successful, the head-position is on the first
  character after the document type definition.
*/

bool QXmlSimpleReader::parseDoctype()
{
// some init-stuff
  (this) -> d -> QXmlSimpleReaderPrivate::systemId = QString::null;
  (this) -> d -> QXmlSimpleReaderPrivate::publicId = QString::null;
  const signed char Init = 0;
// read the doctype
  const signed char Doctype = 1;
// eat_ws
  const signed char Ws1 = 2;
// read the doctype, part 2
  const signed char Doctype2 = 3;
// eat_ws
  const signed char Ws2 = 4;
// read SYSTEM
  const signed char Sys = 5;
// eat_ws
  const signed char Ws3 = 6;
// markupdecl or PEReference
  const signed char MP = 7;
// PERReference
  const signed char PER = 8;
// markupdecl
  const signed char Mup = 9;
// eat_ws
  const signed char Ws4 = 10;
// end of markupdecl or PEReference
  const signed char MPE = 11;
  const signed char Done = 12;
  const signed char InpWs = 0;
// 'D'
  const signed char InpD = 1;
// 'S' or 'P'
  const signed char InpS = 2;
// [
  const signed char InpOB = 3;
// ]
  const signed char InpCB = 4;
// %
  const signed char InpPer = 5;
// >
  const signed char InpGt = 6;
  const signed char InpUnknown = 7;
// use some kind of state machine for parsing
  static signed char table[12][8] = {
/*  InpWs,  InpD       InpS       InpOB  InpCB  InpPer InpGt  InpUnknown */
// Init
{((-1)), (Doctype), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Doctype
{(Ws1), (Doctype2), (Doctype2), ((-1)), ((-1)), ((-1)), ((-1)), (Doctype2)}, 
// Ws1
{((-1)), (Doctype2), (Doctype2), ((-1)), ((-1)), ((-1)), ((-1)), (Doctype2)}, 
// Doctype2
{(Ws2), ((-1)), (Sys), (MP), ((-1)), ((-1)), (Done), ((-1))}, 
// Ws2
{((-1)), ((-1)), (Sys), (MP), ((-1)), ((-1)), (Done), ((-1))}, 
// Sys
{(Ws3), ((-1)), ((-1)), (MP), ((-1)), ((-1)), (Done), ((-1))}, 
// Ws3
{((-1)), ((-1)), ((-1)), (MP), ((-1)), ((-1)), (Done), ((-1))}, 
// MP
{((-1)), ((-1)), ((-1)), ((-1)), (MPE), (PER), ((-1)), (Mup)}, 
// PER
{(Ws4), ((-1)), ((-1)), ((-1)), (MPE), (PER), ((-1)), (Mup)}, 
// Mup
{(Ws4), ((-1)), ((-1)), ((-1)), (MPE), (PER), ((-1)), (Mup)}, 
// Ws4
{((-1)), ((-1)), ((-1)), ((-1)), (MPE), (PER), ((-1)), (Mup)}, 
// MPE
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Done), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='D')) {
        input = InpD;
      }
      else {
        if ((((this) -> c)=='S')) {
          input = InpS;
        }
        else {
          if ((((this) -> c)=='P')) {
            input = InpS;
          }
          else {
            if ((((this) -> c)=='[')) {
              input = InpOB;
            }
            else {
              if ((((this) -> c)==']')) {
                input = InpCB;
              }
              else {
                if ((((this) -> c)=='%')) {
                  input = InpPer;
                }
                else {
                  if ((((this) -> c)=='>')) {
                    input = InpGt;
                  }
                  else {
                    input = InpUnknown;
                  }
                }
              }
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Doctype:
{
        parseOk = (this) ->  parseString ("DOCTYPE");
        break; 
      }
      case Ws1:
{
      }
      case Ws2:
{
      }
      case Ws3:
{
      }
      case Ws4:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Doctype2:
{
        (this) ->  parseName ();
        break; 
      }
      case Sys:
{
        parseOk = (this) ->  parseExternalID ();
        break; 
      }
      case MP:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case PER:
{
        parseOk = (this) ->  parsePEReference (InDTD);
        break; 
      }
      case Mup:
{
        parseOk = (this) ->  parseMarkupdecl ();
        break; 
      }
      case MPE:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case Done:
{
        if (((this) -> lexicalHnd)) {
          if (!(this) -> lexicalHnd ->  endDTD ()) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
            goto parseError;
          }
        }
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Doctype:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
          goto parseError;
        }
        if (!(this) ->  is_S ((this) -> c)) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
          goto parseError;
        }
        break; 
      }
      case Doctype2:
{
        (this) -> d -> QXmlSimpleReaderPrivate::doctype = (this) ->  name ();
        if (((this) -> lexicalHnd)) {
          if (!(this) -> lexicalHnd ->  startDTD ((this) -> d -> QXmlSimpleReaderPrivate::doctype,(this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId)) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
            goto parseError;
          }
        }
        break; 
      }
      case Sys:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
          goto parseError;
        }
        break; 
      }
      case PER:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
          goto parseError;
        }
        break; 
      }
      case Mup:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a ExternalID [75].
  If allowPublicID is TRUE parse ExternalID [75] or PublicID [83].
*/

bool QXmlSimpleReader::parseExternalID(bool allowPublicID)
{
// some init-stuff
  (this) -> d -> QXmlSimpleReaderPrivate::systemId = QString::null;
  (this) -> d -> QXmlSimpleReaderPrivate::publicId = QString::null;
  const signed char Init = 0;
// parse 'SYSTEM'
  const signed char Sys = 1;
// parse the whitespace after 'SYSTEM'
  const signed char SysWS = 2;
// parse SystemLiteral with '
  const signed char SysSQ = 3;
// parse SystemLiteral with '
  const signed char SysSQ2 = 4;
// parse SystemLiteral with "
  const signed char SysDQ = 5;
// parse SystemLiteral with "
  const signed char SysDQ2 = 6;
// parse 'PUBLIC'
  const signed char Pub = 7;
// parse the whitespace after 'PUBLIC'
  const signed char PubWS = 8;
// parse PubidLiteral with '
  const signed char PubSQ = 9;
// parse PubidLiteral with '
  const signed char PubSQ2 = 10;
// parse PubidLiteral with "
  const signed char PubDQ = 11;
// parse PubidLiteral with "
  const signed char PubDQ2 = 12;
// finished parsing the PubidLiteral
  const signed char PubE = 13;
// parse the whitespace after the PubidLiteral
  const signed char PubWS2 = 14;
// done if allowPublicID is TRUE
  const signed char PDone = 15;
  const signed char Done = 16;
// '
  const signed char InpSQ = 0;
// "
  const signed char InpDQ = 1;
// S
  const signed char InpS = 2;
// P
  const signed char InpP = 3;
// white space
  const signed char InpWs = 4;
  const signed char InpUnknown = 5;
// use some kind of state machine for parsing
  static signed char table[15][6] = {
/*  InpSQ    InpDQ    InpS     InpP     InpWs     InpUnknown */
// Init
{((-1)), ((-1)), (Sys), (Pub), ((-1)), ((-1))}, 
// Sys
{((-1)), ((-1)), ((-1)), ((-1)), (SysWS), ((-1))}, 
// SysWS
{(SysSQ), (SysDQ), ((-1)), ((-1)), ((-1)), ((-1))}, 
// SysSQ
{(Done), (SysSQ2), (SysSQ2), (SysSQ2), (SysSQ2), (SysSQ2)}, 
// SysSQ2
{(Done), (SysSQ2), (SysSQ2), (SysSQ2), (SysSQ2), (SysSQ2)}, 
// SysDQ
{(SysDQ2), (Done), (SysDQ2), (SysDQ2), (SysDQ2), (SysDQ2)}, 
// SysDQ2
{(SysDQ2), (Done), (SysDQ2), (SysDQ2), (SysDQ2), (SysDQ2)}, 
// Pub
{((-1)), ((-1)), ((-1)), ((-1)), (PubWS), ((-1))}, 
// PubWS
{(PubSQ), (PubDQ), ((-1)), ((-1)), ((-1)), ((-1))}, 
// PubSQ
{(PubE), ((-1)), (PubSQ2), (PubSQ2), (PubSQ2), (PubSQ2)}, 
// PubSQ2
{(PubE), ((-1)), (PubSQ2), (PubSQ2), (PubSQ2), (PubSQ2)}, 
// PubDQ
{((-1)), (PubE), (PubDQ2), (PubDQ2), (PubDQ2), (PubDQ2)}, 
// PubDQ2
{((-1)), (PubE), (PubDQ2), (PubDQ2), (PubDQ2), (PubDQ2)}, 
// PubE
{(PDone), (PDone), (PDone), (PDone), (PubWS2), (PDone)}, 
// PubWS2
{(SysSQ), (SysDQ), (PDone), (PDone), (PDone), (PDone)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='\'')) {
        input = InpSQ;
      }
      else {
        if ((((this) -> c)=='"')) {
          input = InpDQ;
        }
        else {
          if ((((this) -> c)=='S')) {
            input = InpS;
          }
          else {
            if ((((this) -> c)=='P')) {
              input = InpP;
            }
            else {
              input = InpUnknown;
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Sys:
{
        parseOk = (this) ->  parseString ("SYSTEM");
        break; 
      }
      case SysWS:
{
        (this) ->  eat_ws ();
        break; 
      }
      case SysSQ:
{
      }
      case SysDQ:
{
        (this) ->  stringClear ();
        (this) ->  next ();
        break; 
      }
      case SysSQ2:
{
      }
      case SysDQ2:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case Pub:
{
        parseOk = (this) ->  parseString ("PUBLIC");
        break; 
      }
      case PubWS:
{
        (this) ->  eat_ws ();
        break; 
      }
      case PubSQ:
{
      }
      case PubDQ:
{
        (this) ->  stringClear ();
        (this) ->  next ();
        break; 
      }
      case PubSQ2:
{
      }
      case PubDQ2:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case PubE:
{
        (this) ->  next ();
        break; 
      }
      case PubWS2:
{
        (this) -> d -> QXmlSimpleReaderPrivate::publicId = (this) ->  string ();
        (this) ->  eat_ws ();
        break; 
      }
      case Done:
{
        (this) -> d -> QXmlSimpleReaderPrivate::systemId = (this) ->  string ();
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Sys:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Pub:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case PDone:
{
        if (allowPublicID) {
          (this) -> d -> QXmlSimpleReaderPrivate::publicId = (this) ->  string ();
          return TRUE;
        }
        else {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a markupdecl [29].
*/

bool QXmlSimpleReader::parseMarkupdecl()
{
  const signed char Init = 0;
// < was read
  const signed char Lt = 1;
// ! was read
  const signed char Em = 2;
// E was read
  const signed char CE = 3;
// ? was read
  const signed char Qm = 4;
// - was read
  const signed char Dash = 5;
// A was read
  const signed char CA = 6;
// EL was read
  const signed char CEL = 7;
// EN was read
  const signed char CEN = 8;
// N was read
  const signed char CN = 9;
  const signed char Done = 10;
// <
  const signed char InpLt = 0;
// ?
  const signed char InpQm = 1;
// !
  const signed char InpEm = 2;
// -
  const signed char InpDash = 3;
// A
  const signed char InpA = 4;
// E
  const signed char InpE = 5;
// L
  const signed char InpL = 6;
// N
  const signed char InpN = 7;
  const signed char InpUnknown = 8;
// use some kind of state machine for parsing
  static signed char table[4][9] = {
/*  InpLt  InpQm  InpEm  InpDash  InpA   InpE   InpL   InpN   InpUnknown */
// Init
{(Lt), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Lt
{((-1)), (Qm), (Em), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Em
{((-1)), ((-1)), ((-1)), (Dash), (CA), (CE), ((-1)), (CN), ((-1))}, 
// CE
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (CEL), (CEN), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((((this) -> c)=='<')) {
      input = InpLt;
    }
    else {
      if ((((this) -> c)=='?')) {
        input = InpQm;
      }
      else {
        if ((((this) -> c)=='!')) {
          input = InpEm;
        }
        else {
          if ((((this) -> c)=='-')) {
            input = InpDash;
          }
          else {
            if ((((this) -> c)=='A')) {
              input = InpA;
            }
            else {
              if ((((this) -> c)=='E')) {
                input = InpE;
              }
              else {
                if ((((this) -> c)=='L')) {
                  input = InpL;
                }
                else {
                  if ((((this) -> c)=='N')) {
                    input = InpN;
                  }
                  else {
                    input = InpUnknown;
                  }
                }
              }
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Lt:
{
        (this) ->  next ();
        break; 
      }
      case Em:
{
        (this) ->  next ();
        break; 
      }
      case CE:
{
        (this) ->  next ();
        break; 
      }
      case Qm:
{
        parseOk = (this) ->  parsePI ();
        break; 
      }
      case Dash:
{
        parseOk = (this) ->  parseComment ();
        break; 
      }
      case CA:
{
        parseOk = (this) ->  parseAttlistDecl ();
        break; 
      }
      case CEL:
{
        parseOk = (this) ->  parseElementDecl ();
        break; 
      }
      case CEN:
{
        parseOk = (this) ->  parseEntityDecl ();
        break; 
      }
      case CN:
{
        parseOk = (this) ->  parseNotationDecl ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Qm:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing processing instruction";
          goto parseError;
        }
        if (((this) -> contentHnd)) {
          if (!(this) -> contentHnd ->  processingInstruction ((this) ->  name (),(this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
            goto parseError;
          }
        }
        return TRUE;
      }
      case Dash:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing comment";
          goto parseError;
        }
        if (((this) -> lexicalHnd)) {
          if (!(this) -> lexicalHnd ->  comment ((this) ->  string ())) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> lexicalHnd ->  errorString ();
            goto parseError;
          }
        }
        return TRUE;
      }
      case CA:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing attribute list declaration";
          goto parseError;
        }
        return TRUE;
      }
      case CEL:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing element declaration";
          goto parseError;
        }
        return TRUE;
      }
      case CEN:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing entity declaration";
          goto parseError;
        }
        return TRUE;
      }
      case CN:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing notation declaration";
          goto parseError;
        }
        return TRUE;
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a PEReference [69]
*/

bool QXmlSimpleReader::parsePEReference(enum EntityRecognitionContext context)
{
  const signed char Init = 0;
  const signed char Next = 1;
  const signed char Name = 2;
  const signed char Done = 3;
// ;
  const signed char InpSemi = 0;
// %
  const signed char InpPer = 1;
  const signed char InpUnknown = 2;
// use some kind of state machine for parsing
  static signed char table[3][3] = {
/*  InpSemi  InpPer  InpUnknown */
// Init
{((-1)), (Next), ((-1))}, 
// Next
{((-1)), ((-1)), (Name)}, 
// Name
{(Done), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((((this) -> c)==';')) {
      input = InpSemi;
    }
    else {
      if ((((this) -> c)=='%')) {
        input = InpPer;
      }
      else {
        input = InpUnknown;
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Next:
{
        (this) ->  next ();
        break; 
      }
      case Name:
{
        parseOk = (this) ->  parseName (TRUE);
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Name:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        if ((this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  find ((this) ->  ref ()) == (this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  end ()) {
// ### skip it???
          if (((this) -> contentHnd)) {
            if (!(this) -> contentHnd ->  skippedEntity (QString::QString("%")+(this) ->  ref ())) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        else {
          if (context == InEntityValue) {
// Included in literal
            (this) -> xmlRef = (this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  find ((this) ->  ref ()) .  data () .  replace (QRegExp::QRegExp(("\"")),"&quot;") .  replace (QRegExp::QRegExp(("'")),"&apos;")+(this) -> xmlRef;
          }
          else {
            if (context == InDTD) {
// Included as PE
              (this) -> xmlRef = QString::QString(" ")+(this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  find ((this) ->  ref ()) .  data ()+QString::QString(" ")+(this) -> xmlRef;
            }
          }
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a AttlistDecl [52].
  Precondition: the beginning '<!' is already read and the head
  stands on the 'A' of '<!ATTLIST'
*/

bool QXmlSimpleReader::parseAttlistDecl()
{
  const signed char Init = 0;
// parse the string "ATTLIST"
  const signed char Attlist = 1;
// whitespace read
  const signed char Ws = 2;
// parse name
  const signed char Name = 3;
// whitespace read
  const signed char Ws1 = 4;
// parse the AttDef
  const signed char Attdef = 5;
// whitespace read
  const signed char Ws2 = 6;
// parse the AttType
  const signed char Atttype = 7;
// whitespace read
  const signed char Ws3 = 8;
// DefaultDecl with #
  const signed char DDecH = 9;
// parse the string "REQUIRED"
  const signed char DefReq = 10;
// parse the string "IMPLIED"
  const signed char DefImp = 11;
// parse the string "FIXED"
  const signed char DefFix = 12;
// parse the AttValue
  const signed char Attval = 13;
// whitespace read
  const signed char Ws4 = 14;
  const signed char Done = 15;
// white space
  const signed char InpWs = 0;
// >
  const signed char InpGt = 1;
// #
  const signed char InpHash = 2;
// A
  const signed char InpA = 3;
// I
  const signed char InpI = 4;
// F
  const signed char InpF = 5;
// R
  const signed char InpR = 6;
  const signed char InpUnknown = 7;
// use some kind of state machine for parsing
  static signed char table[15][8] = {
/*  InpWs    InpGt    InpHash  InpA      InpI     InpF     InpR     InpUnknown */
// Init
{((-1)), ((-1)), ((-1)), (Attlist), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Attlist
{(Ws), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws
{((-1)), ((-1)), ((-1)), (Name), (Name), (Name), (Name), (Name)}, 
// Name
{(Ws1), (Done), (Attdef), (Attdef), (Attdef), (Attdef), (Attdef), (Attdef)}, 
// Ws1
{((-1)), (Done), (Attdef), (Attdef), (Attdef), (Attdef), (Attdef), (Attdef)}, 
// Attdef
{(Ws2), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws2
{((-1)), (Atttype), (Atttype), (Atttype), (Atttype), (Atttype), (Atttype), (Atttype)}, 
// Attype
{(Ws3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws3
{((-1)), (Attval), (DDecH), (Attval), (Attval), (Attval), (Attval), (Attval)}, 
// DDecH
{((-1)), ((-1)), ((-1)), ((-1)), (DefImp), (DefFix), (DefReq), ((-1))}, 
// DefReq
{(Ws4), (Ws4), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// DefImp
{(Ws4), (Ws4), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// DefFix
{(Ws3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Attval
{(Ws4), (Ws4), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws4
{((-1)), (Done), (Attdef), (Attdef), (Attdef), (Attdef), (Attdef), (Attdef)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='>')) {
        input = InpGt;
      }
      else {
        if ((((this) -> c)=='#')) {
          input = InpHash;
        }
        else {
          if ((((this) -> c)=='A')) {
            input = InpA;
          }
          else {
            if ((((this) -> c)=='I')) {
              input = InpI;
            }
            else {
              if ((((this) -> c)=='F')) {
                input = InpF;
              }
              else {
                if ((((this) -> c)=='R')) {
                  input = InpR;
                }
                else {
                  input = InpUnknown;
                }
              }
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Attlist:
{
        parseOk = (this) ->  parseString ("ATTLIST");
        break; 
      }
      case Ws:
{
      }
      case Ws1:
{
      }
      case Ws2:
{
      }
      case Ws3:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Name:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Attdef:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Atttype:
{
        parseOk = (this) ->  parseAttType ();
        break; 
      }
      case DDecH:
{
        (this) ->  next ();
        break; 
      }
      case DefReq:
{
        parseOk = (this) ->  parseString ("REQUIRED");
        break; 
      }
      case DefImp:
{
        parseOk = (this) ->  parseString ("IMPLIED");
        break; 
      }
      case DefFix:
{
        parseOk = (this) ->  parseString ("FIXED");
        break; 
      }
      case Attval:
{
        parseOk = (this) ->  parseAttValue ();
        break; 
      }
      case Ws4:
{
        if (((this) -> declHnd)) {
// TODO: not all values are computed yet...
          if (!(this) -> declHnd ->  attributeDecl ((this) -> d -> QXmlSimpleReaderPrivate::attDeclEName,(this) -> d -> QXmlSimpleReaderPrivate::attDeclAName,"","","")) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> declHnd ->  errorString ();
            goto parseError;
          }
        }
        (this) ->  eat_ws ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Attlist:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Name:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        (this) -> d -> QXmlSimpleReaderPrivate::attDeclEName = (this) ->  name ();
        break; 
      }
      case Attdef:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        (this) -> d -> QXmlSimpleReaderPrivate::attDeclAName = (this) ->  name ();
        break; 
      }
      case Atttype:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing attribute type declaration";
          goto parseError;
        }
        break; 
      }
      case DefReq:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case DefImp:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case DefFix:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Attval:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing attribute value declaration";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a AttType [54]
*/

bool QXmlSimpleReader::parseAttType()
{
  const signed char Init = 0;
// StringType
  const signed char ST = 1;
// TokenizedType starting with 'I'
  const signed char TTI = 2;
// TokenizedType helpstate
  const signed char TTI2 = 3;
// TokenizedType helpstate
  const signed char TTI3 = 4;
// TokenizedType starting with 'E'
  const signed char TTE = 5;
// TokenizedType starting with 'ENTITY'
  const signed char TTEY = 6;
// TokenizedType starting with 'ENTITI'
  const signed char TTEI = 7;
// N read (TokenizedType or Notation)
  const signed char N = 8;
// TokenizedType starting with 'NM'
  const signed char TTNM = 9;
// TokenizedType helpstate
  const signed char TTNM2 = 10;
// Notation
  const signed char NO = 11;
// Notation helpstate
  const signed char NO2 = 12;
// Notation helpstate
  const signed char NO3 = 13;
// Notation, read name
  const signed char NOName = 14;
// Notation helpstate
  const signed char NO4 = 15;
// Enumeration
  const signed char EN = 16;
// Enumeration, read Nmtoken
  const signed char ENNmt = 17;
// Enumeration helpstate
  const signed char EN2 = 18;
// almost done (make next and accept)
  const signed char ADone = 19;
  const signed char Done = 20;
// whitespace
  const signed char InpWs = 0;
// (
  const signed char InpOp = 1;
// )
  const signed char InpCp = 2;
// |
  const signed char InpPipe = 3;
// C
  const signed char InpC = 4;
// E
  const signed char InpE = 5;
// I
  const signed char InpI = 6;
// M
  const signed char InpM = 7;
// N
  const signed char InpN = 8;
// O
  const signed char InpO = 9;
// R
  const signed char InpR = 10;
// S
  const signed char InpS = 11;
// Y
  const signed char InpY = 12;
  const signed char InpUnknown = 13;
// use some kind of state machine for parsing
  static signed char table[19][14] = {
/*  InpWs    InpOp    InpCp    InpPipe  InpC     InpE     InpI     InpM     InpN     InpO     InpR     InpS     InpY     InpUnknown */
// Init
{((-1)), (EN), ((-1)), ((-1)), (ST), (TTE), (TTI), ((-1)), (N), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// ST
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done)}, 
// TTI
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (TTI2), (Done), (Done), (Done)}, 
// TTI2
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (TTI3), (Done), (Done)}, 
// TTI3
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done)}, 
// TTE
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (TTEI), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (TTEY), ((-1))}, 
// TTEY
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done)}, 
// TTEI
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done)}, 
// N
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (TTNM), ((-1)), (NO), ((-1)), ((-1)), ((-1)), ((-1))}, 
// TTNM
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (TTNM2), (Done), (Done)}, 
// TTNM2
{(Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done), (Done)}, 
// NO
{(NO2), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// NO2
{((-1)), (NO3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// NO3
{(NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName), (NOName)}, 
// NOName
{(NO4), ((-1)), (ADone), (NO3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// NO4
{((-1)), ((-1)), (ADone), (NO3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// EN
{((-1)), ((-1)), (ENNmt), ((-1)), (ENNmt), (ENNmt), (ENNmt), (ENNmt), (ENNmt), (ENNmt), (ENNmt), (ENNmt), (ENNmt), (ENNmt)}, 
// ENNmt
{(EN2), ((-1)), (ADone), (EN), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// EN2
{((-1)), ((-1)), (ADone), (EN), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='(')) {
        input = InpOp;
      }
      else {
        if ((((this) -> c)==')')) {
          input = InpCp;
        }
        else {
          if ((((this) -> c)=='|')) {
            input = InpPipe;
          }
          else {
            if ((((this) -> c)=='C')) {
              input = InpC;
            }
            else {
              if ((((this) -> c)=='E')) {
                input = InpE;
              }
              else {
                if ((((this) -> c)=='I')) {
                  input = InpI;
                }
                else {
                  if ((((this) -> c)=='M')) {
                    input = InpM;
                  }
                  else {
                    if ((((this) -> c)=='N')) {
                      input = InpN;
                    }
                    else {
                      if ((((this) -> c)=='O')) {
                        input = InpO;
                      }
                      else {
                        if ((((this) -> c)=='R')) {
                          input = InpR;
                        }
                        else {
                          if ((((this) -> c)=='S')) {
                            input = InpS;
                          }
                          else {
                            if ((((this) -> c)=='Y')) {
                              input = InpY;
                            }
                            else {
                              input = InpUnknown;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case ST:
{
        parseOk = (this) ->  parseString ("CDATA");
        break; 
      }
      case TTI:
{
        parseOk = (this) ->  parseString ("ID");
        break; 
      }
      case TTI2:
{
        parseOk = (this) ->  parseString ("REF");
        break; 
      }
      case TTI3:
{
// S
        (this) ->  next ();
        break; 
      }
      case TTE:
{
        parseOk = (this) ->  parseString ("ENTIT");
        break; 
      }
      case TTEY:
{
// Y
        (this) ->  next ();
        break; 
      }
      case TTEI:
{
        parseOk = (this) ->  parseString ("IES");
        break; 
      }
      case N:
{
// N
        (this) ->  next ();
        break; 
      }
      case TTNM:
{
        parseOk = (this) ->  parseString ("MTOKEN");
        break; 
      }
      case TTNM2:
{
// S
        (this) ->  next ();
        break; 
      }
      case NO:
{
        parseOk = (this) ->  parseString ("OTATION");
        break; 
      }
      case NO2:
{
        (this) ->  eat_ws ();
        break; 
      }
      case NO3:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case NOName:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case NO4:
{
        (this) ->  eat_ws ();
        break; 
      }
      case EN:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case ENNmt:
{
        parseOk = (this) ->  parseNmtoken ();
        break; 
      }
      case EN2:
{
        (this) ->  eat_ws ();
        break; 
      }
      case ADone:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case ST:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case TTI:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case TTI2:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case TTE:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case TTEI:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case TTNM:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case NO:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case NOName:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case ENNmt:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing Nmtoken";
          goto parseError;
        }
        break; 
      }
      case ADone:
      return TRUE;
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a AttValue [10]
  Precondition: the head stands on the beginning " or '
  If this function was successful, the head stands on the first
  character after the closing " or ' and the value of the attribute
  is in string().
*/

bool QXmlSimpleReader::parseAttValue()
{
  bool tmp;
  const signed char Init = 0;
// double quotes were read
  const signed char Dq = 1;
// read references in double quotes
  const signed char DqRef = 2;
// signed character read in double quotes
  const signed char DqC = 3;
// single quotes were read
  const signed char Sq = 4;
// read references in single quotes
  const signed char SqRef = 5;
// signed character read in single quotes
  const signed char SqC = 6;
  const signed char Done = 7;
// "
  const signed char InpDq = 0;
// '
  const signed char InpSq = 1;
// &
  const signed char InpAmp = 2;
// <
  const signed char InpLt = 3;
  const signed char InpUnknown = 4;
// use some kind of state machine for parsing
  static signed char table[7][5] = {
/*  InpDq  InpSq  InpAmp  InpLt InpUnknown */
// Init
{(Dq), (Sq), ((-1)), ((-1)), ((-1))}, 
// Dq
{(Done), (DqC), (DqRef), ((-1)), (DqC)}, 
// DqRef
{(Done), (DqC), (DqRef), ((-1)), (DqC)}, 
// DqC
{(Done), (DqC), (DqRef), ((-1)), (DqC)}, 
// Sq
{(SqC), (Done), (SqRef), ((-1)), (SqC)}, 
// SqRef
{(SqC), (Done), (SqRef), ((-1)), (SqC)}, 
// SqRef
{(SqC), (Done), (SqRef), ((-1)), (SqC)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((((this) -> c)=='"')) {
      input = InpDq;
    }
    else {
      if ((((this) -> c)=='\'')) {
        input = InpSq;
      }
      else {
        if ((((this) -> c)=='&')) {
          input = InpAmp;
        }
        else {
          if ((((this) -> c)=='<')) {
            input = InpLt;
          }
          else {
            input = InpUnknown;
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Dq:
{
      }
      case Sq:
{
        (this) ->  stringClear ();
        (this) ->  next ();
        break; 
      }
      case DqRef:
{
      }
      case SqRef:
{
        parseOk = (this) ->  parseReference (tmp,InAttributeValue);
        break; 
      }
      case DqC:
{
      }
      case SqC:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case DqRef:
{
      }
      case SqRef:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing reference";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a elementdecl [45].
  Precondition: the beginning '<!E' is already read and the head
  stands on the 'L' of '<!ELEMENT'
*/

bool QXmlSimpleReader::parseElementDecl()
{
  const signed char Init = 0;
// parse the beginning string
  const signed char Elem = 1;
// whitespace required
  const signed char Ws1 = 2;
// parse Name
  const signed char Nam = 3;
// whitespace required
  const signed char Ws2 = 4;
// read EMPTY
  const signed char Empty = 5;
// read ANY
  const signed char Any = 6;
// read contentspec (except ANY or EMPTY)
  const signed char Cont = 7;
// read Mixed
  const signed char Mix = 8;
//
  const signed char Mix2 = 9;
//
  const signed char Mix3 = 10;
//
  const signed char MixN1 = 11;
//
  const signed char MixN2 = 12;
//
  const signed char MixN3 = 13;
//
  const signed char MixN4 = 14;
// parse cp
  const signed char Cp = 15;
//
  const signed char Cp2 = 16;
// eat whitespace before Done
  const signed char WsD = 17;
  const signed char Done = 18;
  const signed char InpWs = 0;
// >
  const signed char InpGt = 1;
// |
  const signed char InpPipe = 2;
// (
  const signed char InpOp = 3;
// )
  const signed char InpCp = 4;
// #
  const signed char InpHash = 5;
// ?
  const signed char InpQm = 6;
// *
  const signed char InpAst = 7;
// +
  const signed char InpPlus = 8;
// A
  const signed char InpA = 9;
// E
  const signed char InpE = 10;
// L
  const signed char InpL = 11;
  const signed char InpUnknown = 12;
// use some kind of state machine for parsing
  static signed char table[18][13] = {
/*  InpWs   InpGt  InpPipe  InpOp  InpCp   InpHash  InpQm  InpAst  InpPlus  InpA    InpE    InpL    InpUnknown */
// Init
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Elem), ((-1))}, 
// Elem
{(Ws1), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws1
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Nam), (Nam), (Nam), (Nam)}, 
// Nam
{(Ws2), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws2
{((-1)), ((-1)), ((-1)), (Cont), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Any), (Empty), ((-1)), ((-1))}, 
// Empty
{(WsD), (Done), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Any
{(WsD), (Done), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Cont
{((-1)), ((-1)), ((-1)), (Cp), (Cp), (Mix), ((-1)), ((-1)), ((-1)), (Cp), (Cp), (Cp), (Cp)}, 
// Mix
{(Mix2), ((-1)), (MixN1), ((-1)), (Mix3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Mix2
{((-1)), ((-1)), (MixN1), ((-1)), (Mix3), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Mix3
{(WsD), (Done), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (WsD), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// MixN1
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (MixN2), (MixN2), (MixN2), (MixN2)}, 
// MixN2
{(MixN3), ((-1)), (MixN1), ((-1)), (MixN4), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// MixN3
{((-1)), ((-1)), (MixN1), ((-1)), (MixN4), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// MixN4
{((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (WsD), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Cp
{(WsD), (Done), ((-1)), ((-1)), ((-1)), ((-1)), (Cp2), (Cp2), (Cp2), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Cp2
{(WsD), (Done), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// WsD
{((-1)), (Done), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// read input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='>')) {
        input = InpGt;
      }
      else {
        if ((((this) -> c)=='|')) {
          input = InpPipe;
        }
        else {
          if ((((this) -> c)=='(')) {
            input = InpOp;
          }
          else {
            if ((((this) -> c)==')')) {
              input = InpCp;
            }
            else {
              if ((((this) -> c)=='#')) {
                input = InpHash;
              }
              else {
                if ((((this) -> c)=='?')) {
                  input = InpQm;
                }
                else {
                  if ((((this) -> c)=='*')) {
                    input = InpAst;
                  }
                  else {
                    if ((((this) -> c)=='+')) {
                      input = InpPlus;
                    }
                    else {
                      if ((((this) -> c)=='A')) {
                        input = InpA;
                      }
                      else {
                        if ((((this) -> c)=='E')) {
                          input = InpE;
                        }
                        else {
                          if ((((this) -> c)=='L')) {
                            input = InpL;
                          }
                          else {
                            input = InpUnknown;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
// get new state
//qDebug( "%d -%d(%c)-> %d", state, input, c.latin1(), table[state][input] );
    state = table[state][input];
// in some cases do special actions depending on state
    switch(state){
      case Elem:
{
        parseOk = (this) ->  parseString ("LEMENT");
        break; 
      }
      case Ws1:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Nam:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws2:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Empty:
{
        parseOk = (this) ->  parseString ("EMPTY");
        break; 
      }
      case Any:
{
        parseOk = (this) ->  parseString ("ANY");
        break; 
      }
      case Cont:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case Mix:
{
        parseOk = (this) ->  parseString ("#PCDATA");
        break; 
      }
      case Mix2:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Mix3:
{
        (this) ->  next ();
        break; 
      }
      case MixN1:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case MixN2:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case MixN3:
{
        (this) ->  eat_ws ();
        break; 
      }
      case MixN4:
{
        (this) ->  next ();
        break; 
      }
      case Cp:
{
        parseOk = (this) ->  parseChoiceSeq ();
        break; 
      }
      case Cp2:
{
        (this) ->  next ();
        break; 
      }
      case WsD:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Elem:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Nam:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case Empty:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Any:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Mix:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case MixN2:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case Cp:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing choice or seq";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a NotationDecl [82].
  Precondition: the beginning '<!' is already read and the head
  stands on the 'N' of '<!NOTATION'
*/

bool QXmlSimpleReader::parseNotationDecl()
{
  const signed char Init = 0;
// read NOTATION
  const signed char Not = 1;
// eat whitespaces
  const signed char Ws1 = 2;
// read Name
  const signed char Nam = 3;
// eat whitespaces
  const signed char Ws2 = 4;
// parse ExternalID
  const signed char ExtID = 5;
// eat whitespaces
  const signed char Ws3 = 6;
  const signed char Done = 7;
  const signed char InpWs = 0;
// >
  const signed char InpGt = 1;
// N
  const signed char InpN = 2;
  const signed char InpUnknown = 3;
// use some kind of state machine for parsing
  static signed char table[7][4] = {
/*  InpWs   InpGt  InpN    InpUnknown */
// Init
{((-1)), ((-1)), (Not), ((-1))}, 
// Not
{(Ws1), ((-1)), ((-1)), ((-1))}, 
// Ws1
{((-1)), ((-1)), (Nam), (Nam)}, 
// Nam
{(Ws2), (Done), ((-1)), ((-1))}, 
// Ws2
{((-1)), (Done), (ExtID), (ExtID)}, 
// ExtID
{(Ws3), (Done), ((-1)), ((-1))}, 
// Ws3
{((-1)), (Done), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='>')) {
        input = InpGt;
      }
      else {
        if ((((this) -> c)=='N')) {
          input = InpN;
        }
        else {
          input = InpUnknown;
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Not:
{
        parseOk = (this) ->  parseString ("NOTATION");
        break; 
      }
      case Ws1:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Nam:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws2:
{
        (this) ->  eat_ws ();
        break; 
      }
      case ExtID:
{
        parseOk = (this) ->  parseExternalID (TRUE);
        break; 
      }
      case Ws3:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Not:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Nam:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case ExtID:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing external id";
          goto parseError;
        }
// call the handler
        if (((this) -> dtdHnd)) {
          if (!(this) -> dtdHnd ->  notationDecl ((this) ->  name (),(this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId)) {
            (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> dtdHnd ->  errorString ();
            goto parseError;
          }
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse choice [49] or seq [50].
  Precondition: the beginning '('S? is already read and the head
  stands on the first non-whitespace character after it.
*/

bool QXmlSimpleReader::parseChoiceSeq()
{
  const signed char Init = 0;
// eat whitespace
  const signed char Ws1 = 1;
// choice or set
  const signed char CS_ = 2;
// eat whitespace
  const signed char Ws2 = 3;
// more cp to read
  const signed char More = 4;
// read name
  const signed char Name = 5;
//
  const signed char Done = 6;
// S
  const signed char InpWs = 0;
// (
  const signed char InpOp = 1;
// )
  const signed char InpCp = 2;
// ?
  const signed char InpQm = 3;
// *
  const signed char InpAst = 4;
// +
  const signed char InpPlus = 5;
// |
  const signed char InpPipe = 6;
// ,
  const signed char InpComm = 7;
  const signed char InpUnknown = 8;
// use some kind of state machine for parsing
  static signed char table[6][9] = {
/*  InpWs   InpOp  InpCp  InpQm  InpAst  InpPlus  InpPipe  InpComm  InpUnknown */
// Init
{((-1)), (Ws1), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Name)}, 
// Ws1
{((-1)), (CS_), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (CS_)}, 
// CS_
{(Ws2), ((-1)), (Done), (Ws2), (Ws2), (Ws2), (More), (More), ((-1))}, 
// Ws2
{((-1)), ((-1)), (Done), ((-1)), ((-1)), ((-1)), (More), (More), ((-1))}, 
// More (same as Init)
{((-1)), (Ws1), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), (Name)}, 
// Name (same as CS_)
{(Ws2), ((-1)), (Done), (Ws2), (Ws2), (Ws2), (More), (More), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='(')) {
        input = InpOp;
      }
      else {
        if ((((this) -> c)==')')) {
          input = InpCp;
        }
        else {
          if ((((this) -> c)=='?')) {
            input = InpQm;
          }
          else {
            if ((((this) -> c)=='*')) {
              input = InpAst;
            }
            else {
              if ((((this) -> c)=='+')) {
                input = InpPlus;
              }
              else {
                if ((((this) -> c)=='|')) {
                  input = InpPipe;
                }
                else {
                  if ((((this) -> c)==',')) {
                    input = InpComm;
                  }
                  else {
                    input = InpUnknown;
                  }
                }
              }
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Ws1:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case CS_:
{
        parseOk = (this) ->  parseChoiceSeq ();
        break; 
      }
      case Ws2:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case More:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case Name:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case CS_:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing choice or seq";
          goto parseError;
        }
        break; 
      }
      case Name:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a EntityDecl [70].
  Precondition: the beginning '<!E' is already read and the head
  stand on the 'N' of '<!ENTITY'
*/

bool QXmlSimpleReader::parseEntityDecl()
{
  const signed char Init = 0;
// parse "ENTITY"
  const signed char Ent = 1;
// white space read
  const signed char Ws1 = 2;
// parse name
  const signed char Name = 3;
// white space read
  const signed char Ws2 = 4;
// parse entity value
  const signed char EValue = 5;
// parse ExternalID
  const signed char ExtID = 6;
// white space read
  const signed char Ws3 = 7;
// parse "NDATA"
  const signed char Ndata = 8;
// white space read
  const signed char Ws4 = 9;
// parse name
  const signed char NNam = 10;
// parse PEDecl
  const signed char PEDec = 11;
// white space read
  const signed char Ws6 = 12;
// parse name
  const signed char PENam = 13;
// white space read
  const signed char Ws7 = 14;
// parse entity value
  const signed char PEVal = 15;
// parse ExternalID
  const signed char PEEID = 16;
// white space read
  const signed char WsE = 17;
// done, but also report an external, unparsed entity decl
  const signed char EDDone = 19;
  const signed char Done = 18;
// white space
  const signed char InpWs = 0;
// %
  const signed char InpPer = 1;
// " or '
  const signed char InpQuot = 2;
// >
  const signed char InpGt = 3;
// N
  const signed char InpN = 4;
  const signed char InpUnknown = 5;
// use some kind of state machine for parsing
  static signed char table[18][6] = {
/*  InpWs  InpPer  InpQuot  InpGt  InpN    InpUnknown */
// Init
{((-1)), ((-1)), ((-1)), ((-1)), (Ent), ((-1))}, 
// Ent
{(Ws1), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws1
{((-1)), (PEDec), ((-1)), ((-1)), (Name), (Name)}, 
// Name
{(Ws2), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws2
{((-1)), ((-1)), (EValue), ((-1)), ((-1)), (ExtID)}, 
// EValue
{(WsE), ((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// ExtID
{(Ws3), ((-1)), ((-1)), (EDDone), ((-1)), ((-1))}, 
// Ws3
{((-1)), ((-1)), ((-1)), (EDDone), (Ndata), ((-1))}, 
// Ndata
{(Ws4), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws4
{((-1)), ((-1)), ((-1)), ((-1)), (NNam), (NNam)}, 
// NNam
{(WsE), ((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// PEDec
{(Ws6), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws6
{((-1)), ((-1)), ((-1)), ((-1)), (PENam), (PENam)}, 
// PENam
{(Ws7), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// Ws7
{((-1)), ((-1)), (PEVal), ((-1)), ((-1)), (PEEID)}, 
// PEVal
{(WsE), ((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// PEEID
{(WsE), ((-1)), ((-1)), (Done), ((-1)), ((-1))}, 
// WsE
{((-1)), ((-1)), ((-1)), (Done), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_S ((this) -> c)) {
      input = InpWs;
    }
    else {
      if ((((this) -> c)=='%')) {
        input = InpPer;
      }
      else {
        if ((((this) -> c)=='"') || (((this) -> c)=='\'')) {
          input = InpQuot;
        }
        else {
          if ((((this) -> c)=='>')) {
            input = InpGt;
          }
          else {
            if ((((this) -> c)=='N')) {
              input = InpN;
            }
            else {
              input = InpUnknown;
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Ent:
{
        parseOk = (this) ->  parseString ("NTITY");
        break; 
      }
      case Ws1:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Name:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws2:
{
        (this) ->  eat_ws ();
        break; 
      }
      case EValue:
{
        parseOk = (this) ->  parseEntityValue ();
        break; 
      }
      case ExtID:
{
        parseOk = (this) ->  parseExternalID ();
        break; 
      }
      case Ws3:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Ndata:
{
        parseOk = (this) ->  parseString ("NDATA");
        break; 
      }
      case Ws4:
{
        (this) ->  eat_ws ();
        break; 
      }
      case NNam:
{
        parseOk = (this) ->  parseName (TRUE);
        break; 
      }
      case PEDec:
{
        (this) ->  next ();
        break; 
      }
      case Ws6:
{
        (this) ->  eat_ws ();
        break; 
      }
      case PENam:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws7:
{
        (this) ->  eat_ws ();
        break; 
      }
      case PEVal:
{
        parseOk = (this) ->  parseEntityValue ();
        break; 
      }
      case PEEID:
{
        parseOk = (this) ->  parseExternalID ();
        break; 
      }
      case WsE:
{
        (this) ->  eat_ws ();
        break; 
      }
      case EDDone:
{
        (this) ->  next ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Ent:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case Name:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case EValue:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing entity value declaration";
          goto parseError;
        }
        if (!(this) ->  entityExist ((this) ->  name ())) {
          (this) -> d -> QXmlSimpleReaderPrivate::entities .  insert ((this) ->  name (),(this) ->  string ());
          if (((this) -> declHnd)) {
            if (!(this) -> declHnd ->  internalEntityDecl ((this) ->  name (),(this) ->  string ())) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> declHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        break; 
      }
      case ExtID:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing external id";
          goto parseError;
        }
        break; 
      }
      case Ndata:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
          goto parseError;
        }
        break; 
      }
      case NNam:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        if (!(this) ->  entityExist ((this) ->  name ())) {
          (this) -> d -> QXmlSimpleReaderPrivate::externEntities .  insert ((this) ->  name (),QXmlSimpleReaderPrivate::ExternEntity::ExternEntity((this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId,(this) ->  ref ()));
          if (((this) -> dtdHnd)) {
            if (!(this) -> dtdHnd ->  unparsedEntityDecl ((this) ->  name (),(this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId,(this) ->  ref ())) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> declHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        break; 
      }
      case PENam:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case PEVal:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing entity value declaration";
          goto parseError;
        }
        if (!(this) ->  entityExist ((this) ->  name ())) {
          (this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  insert ((this) ->  name (),(this) ->  string ());
          if (((this) -> declHnd)) {
            if (!(this) -> declHnd ->  internalEntityDecl (QString::QString("%")+(this) ->  name (),(this) ->  string ())) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> declHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        break; 
      }
      case PEEID:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing external id";
          goto parseError;
        }
        if (!(this) ->  entityExist ((this) ->  name ())) {
          (this) -> d -> QXmlSimpleReaderPrivate::externParameterEntities .  insert ((this) ->  name (),QXmlSimpleReaderPrivate::ExternParameterEntity::ExternParameterEntity((this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId));
          if (((this) -> declHnd)) {
            if (!(this) -> declHnd ->  externalEntityDecl (QString::QString("%")+(this) ->  name (),(this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId)) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> declHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        break; 
      }
      case EDDone:
{
        if (!(this) ->  entityExist ((this) ->  name ())) {
          (this) -> d -> QXmlSimpleReaderPrivate::externEntities .  insert ((this) ->  name (),QXmlSimpleReaderPrivate::ExternEntity::ExternEntity((this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId,(QString::null)));
          if (((this) -> declHnd)) {
            if (!(this) -> declHnd ->  externalEntityDecl ((this) ->  name (),(this) -> d -> QXmlSimpleReaderPrivate::publicId,(this) -> d -> QXmlSimpleReaderPrivate::systemId)) {
              (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> declHnd ->  errorString ();
              goto parseError;
            }
          }
        }
        return TRUE;
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a EntityValue [9]
*/

bool QXmlSimpleReader::parseEntityValue()
{
  bool tmp;
  const signed char Init = 0;
// EntityValue is double quoted
  const signed char Dq = 1;
// signed character
  const signed char DqC = 2;
// PERefence
  const signed char DqPER = 3;
// Reference
  const signed char DqRef = 4;
// EntityValue is double quoted
  const signed char Sq = 5;
// signed character
  const signed char SqC = 6;
// PERefence
  const signed char SqPER = 7;
// Reference
  const signed char SqRef = 8;
  const signed char Done = 9;
// "
  const signed char InpDq = 0;
// '
  const signed char InpSq = 1;
// &
  const signed char InpAmp = 2;
// %
  const signed char InpPer = 3;
  const signed char InpUnknown = 4;
// use some kind of state machine for parsing
  static signed char table[9][5] = {
/*  InpDq  InpSq  InpAmp  InpPer  InpUnknown */
// Init
{(Dq), (Sq), ((-1)), ((-1)), ((-1))}, 
// Dq
{(Done), (DqC), (DqRef), (DqPER), (DqC)}, 
// DqC
{(Done), (DqC), (DqRef), (DqPER), (DqC)}, 
// DqPER
{(Done), (DqC), (DqRef), (DqPER), (DqC)}, 
// DqRef
{(Done), (DqC), (DqRef), (DqPER), (DqC)}, 
// Sq
{(SqC), (Done), (SqRef), (SqPER), (SqC)}, 
// SqC
{(SqC), (Done), (SqRef), (SqPER), (SqC)}, 
// SqPER
{(SqC), (Done), (SqRef), (SqPER), (SqC)}, 
// SqRef
{(SqC), (Done), (SqRef), (SqPER), (SqC)}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((((this) -> c)=='"')) {
      input = InpDq;
    }
    else {
      if ((((this) -> c)=='\'')) {
        input = InpSq;
      }
      else {
        if ((((this) -> c)=='&')) {
          input = InpAmp;
        }
        else {
          if ((((this) -> c)=='%')) {
            input = InpPer;
          }
          else {
            input = InpUnknown;
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Dq:
{
      }
      case Sq:
{
        (this) ->  stringClear ();
        (this) ->  next ();
        break; 
      }
      case DqC:
{
      }
      case SqC:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case DqPER:
{
      }
      case SqPER:
{
        parseOk = (this) ->  parsePEReference (InEntityValue);
        break; 
      }
      case DqRef:
{
      }
      case SqRef:
{
        parseOk = (this) ->  parseReference (tmp,InEntityValue);
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case DqPER:
{
      }
      case SqPER:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing document type definition";
          goto parseError;
        }
        break; 
      }
      case DqRef:
{
      }
      case SqRef:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing reference";
          goto parseError;
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a comment [15].
  Precondition: the beginning '<!' of the comment is already read and the head
  stands on the first '-' of '<!--'.
  If this funktion was successful, the head-position is on the first
  character after the comment.
*/

bool QXmlSimpleReader::parseComment()
{
  const signed char Init = 0;
// the first dash was read
  const signed char Dash1 = 1;
// the second dash was read
  const signed char Dash2 = 2;
// read comment
  const signed char Com = 3;
// read comment (help state)
  const signed char Com2 = 4;
// finished reading comment
  const signed char ComE = 5;
  const signed char Done = 6;
// -
  const signed char InpDash = 0;
// >
  const signed char InpGt = 1;
  const signed char InpUnknown = 2;
// use some kind of state machine for parsing
  static signed char table[6][3] = {
/*  InpDash  InpGt  InpUnknown */
// Init
{(Dash1), ((-1)), ((-1))}, 
// Dash1
{(Dash2), ((-1)), ((-1))}, 
// Dash2
{(Com2), (Com), (Com)}, 
// Com
{(Com2), (Com), (Com)}, 
// Com2
{(ComE), (Com), (Com)}, 
// ComE
{((-1)), (Done), ((-1))}};
  signed char state = Init;
  signed char input;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((((this) -> c)=='-')) {
      input = InpDash;
    }
    else {
      if ((((this) -> c)=='>')) {
        input = InpGt;
      }
      else {
        input = InpUnknown;
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Dash1:
{
        (this) ->  next ();
        break; 
      }
      case Dash2:
{
        (this) ->  next ();
        break; 
      }
      case Com:
{
        (this) ->  stringAddC ();
        (this) ->  next ();
        break; 
      }
      case Com2:
{
        (this) ->  next ();
        break; 
      }
      case ComE:
{
        (this) ->  next ();
        break; 
      }
      case Done:
{
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Dash2:
{
        (this) ->  stringClear ();
        break; 
      }
      case Com2:
{
// if next character is not a dash than don't skip it
        if ((((this) -> c)!='-')) {
          (this) ->  stringAddC ('-');
        }
        break; 
      }
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing comment";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a Attribute [41].
  Precondition: the head stands on the first character of the name of the
  attribute (i.e. all whitespaces are already parsed).
  The head stand on the next character after the end quotes. The variable name
  contains the name of the attribute and the variable string contains the value
  of the attribute.
*/

bool QXmlSimpleReader::parseAttribute()
{
  const signed char Init = 0;
// parse name
  const signed char PName = 1;
// eat ws
  const signed char Ws = 2;
// the '=' was read
  const signed char Eq = 3;
// " or ' were read
  const signed char Quotes = 4;
  const signed char InpNameBe = 0;
// =
  const signed char InpEq = 1;
// "
  const signed char InpDq = 2;
// '
  const signed char InpSq = 3;
  const signed char InpUnknown = 4;
// use some kind of state machine for parsing
  static signed char table[4][5] = {
/*  InpNameBe  InpEq  InpDq    InpSq    InpUnknown */
// Init
{(PName), ((-1)), ((-1)), ((-1)), ((-1))}, 
// PName
{((-1)), (Eq), ((-1)), ((-1)), (Ws)}, 
// Ws
{((-1)), (Eq), ((-1)), ((-1)), ((-1))}, 
// Eq
{((-1)), ((-1)), (Quotes), (Quotes), ((-1))}};
  signed char state = Init;
  signed char input;
  bool parseOk = TRUE;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_NameBeginning ((this) -> c)) {
      input = InpNameBe;
    }
    else {
      if ((((this) -> c)=='=')) {
        input = InpEq;
      }
      else {
        if ((((this) -> c)=='"')) {
          input = InpDq;
        }
        else {
          if ((((this) -> c)=='\'')) {
            input = InpSq;
          }
          else {
            input = InpUnknown;
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case PName:
{
        parseOk = (this) ->  parseName ();
        break; 
      }
      case Ws:
{
        (this) ->  eat_ws ();
        break; 
      }
      case Eq:
{
        (this) ->  next_eat_ws ();
        break; 
      }
      case Quotes:
{
        parseOk = (this) ->  parseAttValue ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case PName:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing name";
          goto parseError;
        }
        break; 
      }
      case Quotes:
{
        if (!parseOk) {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing attribute value declaration";
          goto parseError;
        }
// Done
        return TRUE;
      }
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a Name [5] and store the name in name or ref (if useRef is TRUE).
*/

bool QXmlSimpleReader::parseName(bool useRef)
{
  const signed char Init = 0;
// parse first signed character of the name
  const signed char Name1 = 1;
// parse name
  const signed char Name = 2;
  const signed char Done = 3;
// name beginning signed characters
  const signed char InpNameBe = 0;
// NameChar without InpNameBe
  const signed char InpNameCh = 1;
  const signed char InpUnknown = 2;
// use some kind of state machine for parsing
  static signed char table[3][3] = {
/*  InpNameBe  InpNameCh  InpUnknown */
// Init
{(Name1), ((-1)), ((-1))}, 
// Name1
{(Name), (Name), (Done)}, 
// Name
{(Name), (Name), (Done)}};
  signed char state = Init;
  signed char input;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_NameBeginning ((this) -> c)) {
      input = InpNameBe;
    }
    else {
      if ((this) ->  is_NameChar ((this) -> c)) {
        input = InpNameCh;
      }
      else {
        input = InpUnknown;
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case Name1:
{
        if (useRef) {
          (this) ->  refClear ();
          (this) ->  refAddC ();
        }
        else {
          (this) ->  nameClear ();
          (this) ->  nameAddC ();
        }
        (this) ->  next ();
        break; 
      }
      case Name:
{
        if (useRef) {
          (this) ->  refAddC ();
        }
        else {
          (this) ->  nameAddC ();
        }
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a Nmtoken [7] and store the name in name.
*/

bool QXmlSimpleReader::parseNmtoken()
{
  const signed char Init = 0;
  const signed char NameF = 1;
  const signed char Name = 2;
  const signed char Done = 3;
// NameChar without InpNameBe
  const signed char InpNameCh = 0;
  const signed char InpUnknown = 1;
// use some kind of state machine for parsing
  static signed char table[3][2] = {
/*  InpNameCh  InpUnknown */
// Init
{(NameF), ((-1))}, 
// NameF
{(Name), (Done)}, 
// Name
{(Name), (Done)}};
  signed char state = Init;
  signed char input;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((this) ->  is_NameChar ((this) -> c)) {
      input = InpNameCh;
    }
    else {
      input = InpUnknown;
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case NameF:
{
        (this) ->  nameClear ();
        (this) ->  nameAddC ();
        (this) ->  next ();
        break; 
      }
      case Name:
{
        (this) ->  nameAddC ();
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case Done:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "letter is expected";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Parse a Reference [67].
  charDataRead is set to TRUE if the reference must not be parsed. The
  character(s) which the reference mapped to are appended to string. The
  head stands on the first character after the reference.
  charDataRead is set to FALSE if the reference must be parsed. The
  charachter(s) which the reference mapped to are inserted at the reference
  position. The head stands on the first character of the replacement).
*/

bool QXmlSimpleReader::parseReference(bool &charDataRead,enum EntityRecognitionContext context)
{
// temporary variables
  uint tmp;
  bool ok;
  const signed char Init = 0;
// start of a reference
  const signed char SRef = 1;
// parse CharRef
  const signed char ChRef = 2;
// parse CharRef decimal
  const signed char ChDec = 3;
// start CharRef hexadecimal
  const signed char ChHexS = 4;
// parse CharRef hexadecimal
  const signed char ChHex = 5;
// parse name
  const signed char Name = 6;
// done CharRef decimal
  const signed char DoneD = 7;
// done CharRef hexadecimal
  const signed char DoneH = 8;
// done EntityRef
  const signed char DoneN = 9;
// &
  const signed char InpAmp = 0;
// ;
  const signed char InpSemi = 1;
// #
  const signed char InpHash = 2;
// x
  const signed char InpX = 3;
// 0-9
  const signed char InpNum = 4;
// a-f A-F
  const signed char InpHex = 5;
  const signed char InpUnknown = 6;
// use some kind of state machine for parsing
  static signed char table[8][7] = {
/*  InpAmp  InpSemi  InpHash  InpX     InpNum  InpHex  InpUnknown */
// Init
{(SRef), ((-1)), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}, 
// SRef
{((-1)), ((-1)), (ChRef), (Name), (Name), (Name), (Name)}, 
// ChRef
{((-1)), ((-1)), ((-1)), (ChHexS), (ChDec), ((-1)), ((-1))}, 
// ChDec
{((-1)), (DoneD), ((-1)), ((-1)), (ChDec), ((-1)), ((-1))}, 
// ChHexS
{((-1)), ((-1)), ((-1)), ((-1)), (ChHex), (ChHex), ((-1))}, 
// ChHex
{((-1)), (DoneH), ((-1)), ((-1)), (ChHex), (ChHex), ((-1))}, 
// Name
{((-1)), (DoneN), ((-1)), ((-1)), ((-1)), ((-1)), ((-1))}};
  signed char state = Init;
  signed char input;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if (((this) -> c .  row ())) {
      input = InpUnknown;
    }
    else {
      if (((this) -> c .  cell ()) == '&') {
        input = InpAmp;
      }
      else {
        if (((this) -> c .  cell ()) == ';') {
          input = InpSemi;
        }
        else {
          if (((this) -> c .  cell ()) == '#') {
            input = InpHash;
          }
          else {
            if (((this) -> c .  cell ()) == 'x') {
              input = InpX;
            }
            else {
              if ('0' <= ((this) -> c .  cell ()) && ((this) -> c .  cell ()) <= '9') {
                input = InpNum;
              }
              else {
                if ('a' <= ((this) -> c .  cell ()) && ((this) -> c .  cell ()) <= 'f') {
                  input = InpHex;
                }
                else {
                  if ('A' <= ((this) -> c .  cell ()) && ((this) -> c .  cell ()) <= 'F') {
                    input = InpHex;
                  }
                  else {
                    input = InpUnknown;
                  }
                }
              }
            }
          }
        }
      }
    }
// set state according to input
    state = table[state][input];
// do some actions according to state
    switch(state){
      case SRef:
{
        (this) ->  refClear ();
        (this) ->  next ();
        break; 
      }
      case ChRef:
{
        (this) ->  next ();
        break; 
      }
      case ChDec:
{
        (this) ->  refAddC ();
        (this) ->  next ();
        break; 
      }
      case ChHexS:
{
        (this) ->  next ();
        break; 
      }
      case ChHex:
{
        (this) ->  refAddC ();
        (this) ->  next ();
        break; 
      }
      case Name:
{
// read the name into the ref
        (this) ->  parseName (TRUE);
        break; 
      }
      case DoneD:
{
        tmp = (this) ->  ref () .  toUInt (&ok,10);
        if (ok) {
          (this) ->  stringAddC (QChar::QChar(tmp));
        }
        else {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing reference";
          goto parseError;
        }
        charDataRead = TRUE;
        (this) ->  next ();
        break; 
      }
      case DoneH:
{
        tmp = (this) ->  ref () .  toUInt (&ok,16);
        if (ok) {
          (this) ->  stringAddC (QChar::QChar(tmp));
        }
        else {
          (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing reference";
          goto parseError;
        }
        charDataRead = TRUE;
        (this) ->  next ();
        break; 
      }
      case DoneN:
{
        if (!(this) ->  processReference (charDataRead,context)) {
          goto parseError;
        }
        (this) ->  next ();
        break; 
      }
    }
// no input is read after this
    switch(state){
      case DoneD:
      return TRUE;
      case DoneH:
      return TRUE;
      case DoneN:
      return TRUE;
      case -1:
{
// Error
        (this) -> d -> QXmlSimpleReaderPrivate::error = "error while parsing reference";
        goto parseError;
      }
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Helper function for parseReference()
*/

bool QXmlSimpleReader::processReference(bool &charDataRead,enum EntityRecognitionContext context)
{
  class QString reference((this) ->  ref ());
  if (reference=="amp") {
    if (context == InEntityValue) {
// Bypassed
      (this) ->  stringAddC ('&');
      (this) ->  stringAddC ('a');
      (this) ->  stringAddC ('m');
      (this) ->  stringAddC ('p');
      (this) ->  stringAddC (';');
    }
    else {
// Included or Included in literal
      (this) ->  stringAddC ('&');
    }
    charDataRead = TRUE;
  }
  else {
    if (reference=="lt") {
      if (context == InEntityValue) {
// Bypassed
        (this) ->  stringAddC ('&');
        (this) ->  stringAddC ('l');
        (this) ->  stringAddC ('t');
        (this) ->  stringAddC (';');
      }
      else {
// Included or Included in literal
        (this) ->  stringAddC ('<');
      }
      charDataRead = TRUE;
    }
    else {
      if (reference=="gt") {
        if (context == InEntityValue) {
// Bypassed
          (this) ->  stringAddC ('&');
          (this) ->  stringAddC ('g');
          (this) ->  stringAddC ('t');
          (this) ->  stringAddC (';');
        }
        else {
// Included or Included in literal
          (this) ->  stringAddC ('>');
        }
        charDataRead = TRUE;
      }
      else {
        if (reference=="apos") {
          if (context == InEntityValue) {
// Bypassed
            (this) ->  stringAddC ('&');
            (this) ->  stringAddC ('a');
            (this) ->  stringAddC ('p');
            (this) ->  stringAddC ('o');
            (this) ->  stringAddC ('s');
            (this) ->  stringAddC (';');
          }
          else {
// Included or Included in literal
            (this) ->  stringAddC ('\'');
          }
          charDataRead = TRUE;
        }
        else {
          if (reference=="quot") {
            if (context == InEntityValue) {
// Bypassed
              (this) ->  stringAddC ('&');
              (this) ->  stringAddC ('q');
              (this) ->  stringAddC ('u');
              (this) ->  stringAddC ('o');
              (this) ->  stringAddC ('t');
              (this) ->  stringAddC (';');
            }
            else {
// Included or Included in literal
              (this) ->  stringAddC ('"');
            }
            charDataRead = TRUE;
          }
          else {
            QMap< QString ,QString > ::Iterator it;
            it = (this) -> d -> QXmlSimpleReaderPrivate::entities .  find (reference);
            if (it != (this) -> d -> QXmlSimpleReaderPrivate::entities .  end ()) {
// "Internal General"
              switch(context){
                case InContent:
{
// Included
                  (this) -> xmlRef = it .  data ()+(this) -> xmlRef;
                  charDataRead = FALSE;
                  break; 
                }
                case InAttributeValue:
{
// Included in literal
                  (this) -> xmlRef = it .  data () .  replace (QRegExp::QRegExp(("\"")),"&quot;") .  replace (QRegExp::QRegExp(("'")),"&apos;")+(this) -> xmlRef;
                  charDataRead = FALSE;
                  break; 
                }
                case InEntityValue:
{
{
// Bypassed
                    (this) ->  stringAddC ('&');
                    for (int i = 0; i < ((int )(reference .  length ())); i++) {
                      (this) ->  stringAddC ((reference[i] .  operator QChar ()));
                    }
                    (this) ->  stringAddC (';');
                    charDataRead = TRUE;
                  }
                  break; 
                }
                case InDTD:
{
// Forbidden
                  (this) -> d -> QXmlSimpleReaderPrivate::error = "internal general entity reference not allowed in DTD";
                  charDataRead = FALSE;
                  break; 
                }
              }
            }
            else {
              QMap< QString ,class QXmlSimpleReaderPrivate::ExternEntity > ::Iterator itExtern;
              itExtern = (this) -> d -> QXmlSimpleReaderPrivate::externEntities .  find (reference);
              if (itExtern == (this) -> d -> QXmlSimpleReaderPrivate::externEntities .  end ()) {
// entity not declared
// ### check this case for conformance
                if (context == InEntityValue) {
// Bypassed
                  (this) ->  stringAddC ('&');
                  for (int i = 0; i < ((int )(reference .  length ())); i++) {
                    (this) ->  stringAddC ((reference[i] .  operator QChar ()));
                  }
                  (this) ->  stringAddC (';');
                  charDataRead = TRUE;
                }
                else {
                  if (((this) -> contentHnd)) {
                    if (!(this) -> contentHnd ->  skippedEntity (reference)) {
                      (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
// error
                      return FALSE;
                    }
                  }
                }
              }
              else {
                if (( * itExtern) . QXmlSimpleReaderPrivate::ExternEntity::notation .  isNull ()) {
// "External Parsed General"
                  switch(context){
                    case InContent:
{
// Included if validating
                      if (((this) -> contentHnd)) {
                        if (!(this) -> contentHnd ->  skippedEntity (reference)) {
                          (this) -> d -> QXmlSimpleReaderPrivate::error = (this) -> contentHnd ->  errorString ();
// error
                          return FALSE;
                        }
                      }
                      charDataRead = FALSE;
                      break; 
                    }
                    case InAttributeValue:
{
// Forbidden
                      (this) -> d -> QXmlSimpleReaderPrivate::error = "external parsed general entity reference not allowed in attribute value";
                      charDataRead = FALSE;
                      break; 
                    }
                    case InEntityValue:
{
{
// Bypassed
                        (this) ->  stringAddC ('&');
                        for (int i = 0; i < ((int )(reference .  length ())); i++) {
                          (this) ->  stringAddC ((reference[i] .  operator QChar ()));
                        }
                        (this) ->  stringAddC (';');
                        charDataRead = TRUE;
                      }
                      break; 
                    }
                    case InDTD:
{
// Forbidden
                      (this) -> d -> QXmlSimpleReaderPrivate::error = "external parsed general entity reference not allowed in DTD";
                      charDataRead = FALSE;
                      break; 
                    }
                  }
                }
                else {
// "Unparsed"
// ### notify for "Occurs as Attribute Value" missing (but this is no refence, anyway)
// Forbidden
                  (this) -> d -> QXmlSimpleReaderPrivate::error = "unparsed entity reference in wrong context";
                  charDataRead = FALSE;
// error
                  return FALSE;
                }
              }
            }
          }
        }
      }
    }
  }
// no error
  return TRUE;
}
/*!
  Parse over a simple string.
  After the string was successfully parsed, the head is on the first
  character after the string.
*/

bool QXmlSimpleReader::parseString(const class QString &s)
{
  signed char Done = (s .  length ());
// the character that was expected
  const signed char InpCharExpected = 0;
  const signed char InpUnknown = 1;
// state in this function is the position in the string s
  signed char state = 0;
  signed char input;
  while(TRUE){
// get input
    if ((this) ->  atEnd ()) {
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected end of file";
      goto parseError;
    }
    if ((((this) -> c)==s[((int )state)])) {
      input = InpCharExpected;
    }
    else {
      input = InpUnknown;
    }
// set state according to input
    if (input == InpCharExpected) {
      state++;
    }
    else {
// Error
      (this) -> d -> QXmlSimpleReaderPrivate::error = "unexpected character";
      goto parseError;
    }
// do some actions according to state
    (this) ->  next ();
// no input is read after this
    if (state == Done) {
      return TRUE;
    }
  }
  return TRUE;
  parseError:
  (this) ->  reportParseError ();
  return FALSE;
}
/*!
  Inits the data values.
*/

void QXmlSimpleReader::init(const class QXmlInputSource &i)
{
  (this) -> xml = i .  data ();
  (this) -> xmlLength = ((this) -> xml .  length ());
  (this) -> xmlRef = "";
  (this) -> d -> QXmlSimpleReaderPrivate::externParameterEntities .  clear ();
  (this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  clear ();
  (this) -> d -> QXmlSimpleReaderPrivate::externEntities .  clear ();
  (this) -> d -> QXmlSimpleReaderPrivate::entities .  clear ();
  ((this) -> tags) .  clear ();
  (this) -> d -> QXmlSimpleReaderPrivate::doctype = "";
  (this) -> d -> QXmlSimpleReaderPrivate::xmlVersion = "";
  (this) -> d -> QXmlSimpleReaderPrivate::encoding = "";
  (this) -> d -> QXmlSimpleReaderPrivate::standalone = QXmlSimpleReaderPrivate::Unknown;
  (this) -> lineNr = 0;
  (this) -> columnNr = -1;
  (this) -> pos = 0;
  (this) ->  next ();
  (this) -> d -> QXmlSimpleReaderPrivate::error = "no error occured";
}
/*!
  Returns TRUE if a entity with the name \a e exists,
  otherwise returns FALSE.
*/

bool QXmlSimpleReader::entityExist(const class QString &e) const
{
  if ((this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  find (e) == (this) -> d -> QXmlSimpleReaderPrivate::parameterEntities .  end () && (this) -> d -> QXmlSimpleReaderPrivate::externParameterEntities .  find (e) == (this) -> d -> QXmlSimpleReaderPrivate::externParameterEntities .  end ()) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

void QXmlSimpleReader::reportParseError()
{
  if (((this) -> errorHnd)) {
    (this) -> errorHnd ->  fatalError (QXmlParseException::QXmlParseException((this) -> d -> QXmlSimpleReaderPrivate::error,(this) -> columnNr + 1,(this) -> lineNr + 1));
  }
}
#endif //QT_NO_XML
