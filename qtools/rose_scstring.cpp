/******************************************************************************
 *
 * Copyright (C) 1997-2004 by Dimitri van Heesch.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation under the terms of the GNU General Public License is hereby 
 * granted. No representations are made about the suitability of this software 
 * for any purpose. It is provided "as is" without express or implied warranty.
 * See the GNU General Public License for more details.
 *
 * Documents produced by Doxygen are derivative works derived from the
 * input used in their production; they are not affected by this license.
 *
 */
// with this switch you can choose between the original qcstring implementation,
// which implicitly shares data so copying is faster, but requires at least 12 bytes, and
// the new implementation in this file, which has a smaller footprint (only 4 bytes for
// an empty string), but always copies strings.
#define SMALLSTRING
#include "qcstring.h"
#ifndef SMALLSTRING
#include "qcstring.cpp"
#else
#define SCString QCString
#include <qstring.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <qregexp.h>
#include <qdatastream.h>

QCString::QCString(int size)
{
  if (size > 0) {
    (this) -> m_data = ((char *)(malloc(size)));
    if (((this) -> m_data)) {
      if (size > 1) {
        memset(((this) -> m_data),' ',(size - 1));
      }
      (this) -> m_data[size - 1] = '\0';
    }
  }
  else {
    (this) -> m_data = 0;
  }
}

QCString::QCString(const class ::QCString &s)
{
  (this) ->  duplicate (s);
}

QCString::QCString(const char *str)
{
  (this) ->  duplicate (str);
}

QCString::QCString(const char *str,uint maxlen)
{
  uint l;
  if (str && (l = (qstrlen(str) < maxlen?qstrlen(str) : maxlen))) {
    (this) -> m_data = ((char *)(malloc((l + 1))));
    strncpy((this) -> m_data,str,(l + 1));
    (this) -> m_data[l] = '\0';
  }
  else {
    (this) -> m_data = 0;
  }
}

QCString::~QCString()
{
  if (((this) -> m_data)) {
    free(((this) -> m_data));
  }
  (this) -> m_data = 0;
}

QCString &QCString::assign(const char *str)
{
  if (((this) -> m_data) == str) {
    return  *(this);
  }
  if (((this) -> m_data)) {
    free(((this) -> m_data));
  }
  (this) ->  duplicate (str);
  return  *(this);
}

bool QCString::resize(uint newlen)
{
  if (newlen == 0) {
    if (((this) -> m_data)) {
      free(((this) -> m_data));
      (this) -> m_data = 0;
    }
    return TRUE;
  }
// newlen>0
  if ((this) -> m_data == 0) {
    (this) -> m_data = ((char *)(malloc(newlen)));
  }
  else {
    (this) -> m_data = ((char *)(realloc(((this) -> m_data),newlen)));
  }
  if ((this) -> m_data == 0) {
    return FALSE;
  }
  (this) -> m_data[newlen - 1] = '\0';
  return TRUE;
}

bool QCString::fill(char c,int len)
{
  uint l = (this) ->  length ();
  if (len < 0) {
    len = l;
  }
  if (((uint )len) != l) {
    if (((this) -> m_data)) {
      free(((this) -> m_data));
    }
    if (len > 0) {
      (this) -> m_data = ((char *)(malloc((len + 1))));
      if ((this) -> m_data == 0) {
        return FALSE;
      }
      (this) -> m_data[len] = '\0';
    }
    else {
      (this) -> m_data = 0;
    }
  }
  if (len > 0) {
    uint i;
    for (i = 0; i < ((uint )len); i++) 
      (this) -> m_data[i] = c;
  }
  return TRUE;
}

QCString &QCString::sprintf(const char *format,... )
{
  va_list ap;
  va_start(ap,format);
  uint l = (this) ->  length ();
  const uint minlen = 4095;
  if (l < minlen) {
    if (((this) -> m_data)) {
      (this) -> m_data = ((char *)(realloc(((this) -> m_data),(minlen + 1))));
    }
    else {
      (this) -> m_data = ((char *)(malloc((minlen + 1))));
    }
    (this) -> m_data[minlen] = '\0';
  }
  vsnprintf((this) -> m_data,minlen,format,ap);
// truncate
  (this) ->  resize (qstrlen(((this) -> m_data)) + 1);
  va_end(ap);
  return  *(this);
}

int QCString::find(char c,int index,bool cs) const
{
  uint len = (this) ->  length ();
// index outside string
  if ((this) -> m_data == 0 || ((uint )index) > len) {
    return -1;
  }
  register const char *d;
// case sensitive
  if (cs) {
    d = (strchr(((this) -> m_data + index),c));
  }
  else {
    d = ((this) -> m_data + index);
    c = (tolower(((uchar )c)));
    while(( *d) && tolower(((uchar )( *d))) != c)
      d++;
// not found
    if (!( *d) && c) {
      d = 0;
    }
  }
  return d?((int )(d - ((this) -> m_data))) : -1;
}

int QCString::find(const char *str,int index,bool cs) const
{
  uint l = (this) ->  length ();
// index outside string
  if ((this) -> m_data == 0 || ((uint )index) > l) {
    return -1;
  }
// no search string
  if (!str) {
    return -1;
  }
// zero-length search string
  if (!( *str)) {
    return index;
  }
  register const char *d;
// case sensitive
  if (cs) {
    d = (strstr(((this) -> m_data + index),str));
  }
  else 
// case insensitive
{
    d = ((this) -> m_data + index);
    int len = (qstrlen(str));
    while(( *d)){
      if (qstrnicmp(d,str,len) == 0) {
        break; 
      }
      d++;
    }
// not found
    if (!( *d)) {
      d = 0;
    }
  }
  return d?((int )(d - ((this) -> m_data))) : -1;
}

int QCString::find(const class ::QCString &str,int index,bool cs) const
{
  return (this) ->  find ((str .  data ()),index,cs);
}

int QCString::find(const class QRegExp &rx,int index) const
{
  class QString d = QString:: fromLatin1 (((this) -> m_data));
  return d .  find (rx,index);
}

int QCString::findRev(char c,int index,bool cs) const
{
  const char *b = ((this) -> m_data);
  const char *d;
  uint len = (this) ->  length ();
// empty string
  if (b == 0) {
    return -1;
  }
// neg index ==> start from end
  if (index < 0) {
    if (len == 0) {
      return -1;
    }
    if (cs) {
      d = (strrchr(b,c));
      return d?((int )(d - b)) : -1;
    }
    index = len;
  }
  else {
// bad index
    if (((uint )index) > len) {
      return -1;
    }
  }
  d = b + index;
// case sensitive
  if (cs) {
    while(d >= b && ( *d) != c)
      d--;
  }
  else 
// case insensitive
{
    c = (tolower(((uchar )c)));
    while(d >= b && tolower(((uchar )( *d))) != c)
      d--;
  }
  return d >= b?((int )(d - b)) : -1;
}

int QCString::findRev(const char *str,int index,bool cs) const
{
  int slen = (qstrlen(str));
  uint len = (this) ->  length ();
// neg index ==> start from end
  if (index < 0) {
    index = (len - slen);
  }
  else {
// bad index
    if (((uint )index) > len) {
      return -1;
    }
    else {
// str would be too long
      if (((uint )(index + slen)) > len) {
        index = (len - slen);
      }
    }
  }
  if (index < 0) {
    return -1;
  }
  register char *d = (this) -> m_data + index;
// case sensitive 
  if (cs) {
    for (int i = index; i >= 0; i--) 
      if (qstrncmp((d--),str,slen) == 0) {
        return i;
      }
  }
  else 
// case insensitive
{
    for (int i = index; i >= 0; i--) 
      if (qstrnicmp((d--),str,slen) == 0) {
        return i;
      }
  }
  return -1;
}

int QCString::findRev(const class QRegExp &rx,int index) const
{
  class QString d = QString:: fromLatin1 (((this) -> m_data));
  return d .  findRev (rx,index);
}

int QCString::contains(char c,bool cs) const
{
  int count = 0;
  char *d = (this) -> m_data;
  if (!d) {
    return 0;
  }
// case sensitive
  if (cs) {
    while(( *d))
      if (( *(d++)) == c) {
        count++;
      }
  }
  else 
// case insensitive
{
    c = (tolower(((uchar )c)));
    while(( *d)){
      if (tolower(((uchar )( *d))) == c) {
        count++;
      }
      d++;
    }
  }
  return count;
}

int QCString::contains(const char *str,bool cs) const
{
  int count = 0;
  char *d = (this) ->  data ();
  if (!d) {
    return 0;
  }
  int len = (qstrlen(str));
// counts overlapping strings
  while(( *d)){
    if (cs) {
      if (qstrncmp(d,str,len) == 0) {
        count++;
      }
    }
    else {
      if (qstrnicmp(d,str,len) == 0) {
        count++;
      }
    }
    d++;
  }
  return count;
}

int QCString::contains(const class QRegExp &rx) const
{
  class QString d = QString:: fromLatin1 (((this) -> m_data));
  return d .  contains (rx);
}

QCString QCString::left(uint len) const
{
  if ((this) ->  isEmpty ()) {
    return QCString();
  }
  else {
    if (len >= (this) ->  length ()) {
      return ( *(this));
    }
    else {
      class ::QCString s((len + 1));
      strncpy(s .  data (),((this) -> m_data),len);
       *(s .  data () + len) = '\0';
      return (s);
    }
  }
}

QCString QCString::right(uint len) const
{
  if ((this) ->  isEmpty ()) {
    return QCString();
  }
  else {
    uint l = (this) ->  length ();
    if (len > l) {
      len = l;
    }
    char *p = (this) -> m_data + (l - len);
    return ::QCString::QCString(p);
  }
}

QCString QCString::mid(uint index,uint len) const
{
  uint slen = (this) ->  length ();
  if (len == 0xffffffff) {
    len = slen - index;
  }
  if ((this) ->  isEmpty () || index >= slen) {
    return QCString();
  }
  else {
    register char *p = (this) ->  data () + index;
    class ::QCString s((len + 1));
    strncpy(s .  data (),p,len);
     *(s .  data () + len) = '\0';
    return (s);
  }
}

QCString QCString::lower() const
{
  class ::QCString s(((this) -> m_data));
  register char *p = s .  data ();
  if (p) {
    while(( *p)){
       *p = (tolower(((uchar )( *p))));
      p++;
    }
  }
  return (s);
}

QCString QCString::upper() const
{
  class ::QCString s(((this) -> m_data));
  register char *p = s .  data ();
  if (p) {
    while(( *p)){
       *p = (toupper(((uchar )( *p))));
      p++;
    }
  }
  return (s);
}

QCString QCString::stripWhiteSpace() const
{
// nothing to do
  if ((this) ->  isEmpty ()) {
    return ( *(this));
  }
  register char *s = (this) -> m_data;
  int reslen = ((this) ->  length ());
  if (!(isspace(((uchar )s[0]))) && !(isspace(((uchar )s[reslen - 1])))) {
// returns a copy
    return ( *(this));
  }
  class ::QCString result(s);
  s = result .  data ();
  int start = 0;
  int end = reslen - 1;
// skip white space from start
  while((isspace(((uchar )s[start]))))
    start++;
  if (s[start] == '\0') 
// only white space
{
    return QCString();
  }
// skip white space from end
  while(end && (isspace(((uchar )s[end]))))
    end--;
  end -= start - 1;
  memmove((result .  data ()),(&s[start]),end);
  result .  resize ((end + 1));
  return (result);
}

QCString QCString::simplifyWhiteSpace() const
{
// nothing to do
  if ((this) ->  isEmpty ()) {
    return ( *(this));
  }
  class ::QCString result(((this) ->  length () + 1));
  char *from = (this) ->  data ();
  char *to = result .  data ();
  char *first = to;
  while(TRUE){
    while(( *from) && (isspace(((uchar )( *from)))))
      from++;
    while(( *from) && !(isspace(((uchar )( *from)))))
       *(to++) =  *(from++);
    if (( *from)) {
// ' '
       *(to++) = 0x20;
    }
    else {
      break; 
    }
  }
  if (to > first && ( *(to - 1)) == 0x20) {
    to--;
  }
   *to = '\0';
  result .  resize ((((int )(((long )to) - ((long )(result .  data ())))) + 1));
  return (result);
}

QCString &QCString::insert(uint index,const char *s)
{
  int len = (qstrlen(s));
  if (len == 0) {
    return  *(this);
  }
  uint olen = (this) ->  length ();
  int nlen = (olen + len);
// insert after end of string
  if (index >= olen) {
    (this) -> m_data = ((char *)(realloc(((this) -> m_data),(nlen + index - olen + 1))));
    if (((this) -> m_data)) {
      memset(((this) -> m_data + olen),' ',(index - olen));
      memcpy(((this) -> m_data + index),s,(len + 1));
    }
  }
  else {
// normal insert
    if (((this) -> m_data = ((char *)(realloc(((this) -> m_data),(nlen + 1)))))) {
      memmove(((this) -> m_data + index + len),((this) -> m_data + index),(olen - index + 1));
      memcpy(((this) -> m_data + index),s,len);
    }
  }
  return  *(this);
}
// insert char

QCString &QCString::insert(uint index,char c)
{
  char buf[2];
  buf[0] = c;
  buf[1] = '\0';
  return (this) ->  insert (index,buf);
}

QCString &QCString::operator+=(const char *str)
{
// nothing to append
  if (!str) {
    return  *(this);
  }
  uint len1 = (this) ->  length ();
  uint len2 = qstrlen(str);
  char *newData = (char *)(realloc(((this) -> m_data),(len1 + len2 + 1)));
  if (newData) {
    (this) -> m_data = newData;
    memcpy(((this) -> m_data + len1),str,(len2 + 1));
  }
  return  *(this);
}

QCString &QCString::operator+=(char c)
{
  uint len = (this) ->  length ();
  char *newData = (char *)(realloc(((this) -> m_data),((this) ->  length () + 2)));
  if (newData) {
    (this) -> m_data = newData;
    (this) -> m_data[len] = c;
    (this) -> m_data[len + 1] = '\0';
  }
  return  *(this);
}

QCString &QCString::remove(uint index,uint len)
{
  uint olen = (this) ->  length ();
// range problems
  if (index + len >= olen) {
// index ok
    if (index < olen) {
      (this) ->  resize (index + 1);
    }
  }
  else {
    if (len != 0) {
      memmove(((this) -> m_data + index),((this) -> m_data + index + len),(olen - index - len + 1));
      (this) ->  resize (olen - len + 1);
    }
  }
  return  *(this);
}

QCString &QCString::replace(uint index,uint len,const char *s)
{
  (this) ->  remove (index,len);
  (this) ->  insert (index,s);
  return  *(this);
}

QCString &QCString::replace(const class QRegExp &rx,const char *str)
{
  class QString d = QString:: fromLatin1 (((this) -> m_data));
  class QString r = QString:: fromLatin1 (str);
  d .  replace (rx,r);
  return (this) ->  assign (d .  ascii ());
}

long QCString::toLong(bool *ok) const
{
  class QString s(((this) -> m_data));
  return s .  toLong (ok);
}

ulong QCString::toULong(bool *ok) const
{
  class QString s(((this) -> m_data));
  return s .  toULong (ok);
}

short QCString::toShort(bool *ok) const
{
  class QString s(((this) -> m_data));
  return s .  toShort (ok);
}

ushort QCString::toUShort(bool *ok) const
{
  class QString s(((this) -> m_data));
  return s .  toUShort (ok);
}

int QCString::toInt(bool *ok) const
{
  class QString s(((this) -> m_data));
  return s .  toInt (ok);
}

uint QCString::toUInt(bool *ok) const
{
  class QString s(((this) -> m_data));
  return s .  toUInt (ok);
}

QCString &QCString::setNum(long n)
{
  char buf[20];
  register char *p = &buf[19];
  bool neg;
  if (n < 0) {
    neg = TRUE;
    n = -n;
  }
  else {
    neg = FALSE;
  }
   *p = '\0';
  do {
     *(--p) = (((int )(n % 10)) + '0');
    n /= 10;
  }while (n);
  if (neg) {
     *(--p) = '-';
  }
  (this) ->  operator= (p);
  return  *(this);
}

QCString &QCString::setNum(ulong n)
{
  char buf[20];
  register char *p = &buf[19];
   *p = '\0';
  do {
     *(--p) = (((int )(n % 10)) + '0');
    n /= 10;
  }while (n);
  (this) ->  operator= (p);
  return  *(this);
}

void QCString::msg_index(uint index)
{
#if defined(CHECK_RANGE)
  qWarning("SCString::at: Absolute index %d out of range",index);
#else
#endif
}

bool QCString::stripPrefix(const char *prefix)
{
  if (prefix == 0) {
    return FALSE;
  }
  uint plen = qstrlen(prefix);
// prefix matches
  if (((this) -> m_data) && qstrncmp(prefix,((this) -> m_data),plen) == 0) {
    uint len = qstrlen(((this) -> m_data));
    uint newlen = len - plen + 1;
    qmemmove(((this) -> m_data),((this) -> m_data + plen),newlen);
    (this) ->  resize (newlen);
    return TRUE;
  }
  return FALSE;
}
//---------------------------------------------------------------------------

void *qmemmove(void *dst,const void *src,uint len)
{
  register char *d;
  register char *s;
  if (dst > src) {
    d = ((char *)dst) + len - 1;
    s = ((char *)src) + len - 1;
    while((len--))
       *(d--) =  *(s--);
  }
  else {
    if (dst < src) {
      d = ((char *)dst);
      s = ((char *)src);
      while((len--))
         *(d++) =  *(s++);
    }
  }
  return dst;
}

char *qstrdup(const char *str)
{
  if (!str) {
    return 0;
  }
  char *dst = new char [strlen(str) + 1];
  qt_check_pointer(dst == 0,"scstring.cpp",699);
  return strcpy(dst,str);
}

char *qstrncpy(char *dst,const char *src,uint len)
{
  if (!src) {
    return 0;
  }
  strncpy(dst,src,len);
  if (len > 0) {
    dst[len - 1] = '\0';
  }
  return dst;
}

int qstricmp(const char *str1,const char *str2)
{
  register const uchar *s1 = (const uchar *)str1;
  register const uchar *s2 = (const uchar *)str2;
  int res;
  uchar c;
  if (!s1 || !s2) {
    return s1 == s2?0 : ((int )(((long )s2) - ((long )s1)));
  }
  for (; !(res = (c = (tolower(( *s1)))) - tolower(( *s2))); (s1++ , s2++)) 
// strings are equal
    if (!c) {
      break; 
    }
  return res;
}

int qstrnicmp(const char *str1,const char *str2,uint len)
{
  register const uchar *s1 = (const uchar *)str1;
  register const uchar *s2 = (const uchar *)str2;
  int res;
  uchar c;
  if (!s1 || !s2) {
    return (int )(((long )s2) - ((long )s1));
  }
  for (; (len--); (s1++ , s2++)) {
    if ((res = (c = (tolower(( *s1)))) - tolower(( *s2)))) {
      return res;
    }
// strings are equal
    if (!c) {
      break; 
    }
  }
  return 0;
}
#ifndef QT_NO_DATASTREAM

class QDataStream &operator<<(class QDataStream &s,const QByteArray &a)
{
  return s .  writeBytes ((a .  data ()),a .  size ());
}

class QDataStream &operator>>(class QDataStream &s,QByteArray &a)
{
  Q_UINT32 len;
// read size of array
  s >> len;
// end of file reached
  if (len == 0 || s .  eof ()) {
    a .  resize (0);
    return s;
  }
// resize array
  if (!a .  resize (((uint )len))) {
#if defined(CHECK_NULL)
    qWarning("QDataStream: Not enough memory to read QByteArray");
#endif
    len = 0;
  }
// not null array
  if (len > 0) {
    s .  readRawBytes (a .  data (),((uint )len));
  }
  return s;
}

class QDataStream &operator<<(class QDataStream &s,const class QCString &str)
{
  return s .  writeBytes ((str .  data ()),str .  size ());
}

class QDataStream &operator>>(class QDataStream &s,class QCString &str)
{
  Q_UINT32 len;
// read size of string
  s >> len;
// end of file reached
  if (len == 0 || s .  eof ()) {
    str .  resize (0);
    return s;
  }
// resize string
  if (!str .  resize (((uint )len))) {
#if defined(CHECK_NULL)
    qWarning("QDataStream: Not enough memory to read QCString");
#endif
    len = 0;
  }
// not null array
  if (len > 0) {
    s .  readRawBytes (str .  data (),((uint )len));
  }
  return s;
}
#endif //QT_NO_DATASTREAM
#endif
