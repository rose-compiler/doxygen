/****************************************************************************
** 
**
** Implementation of the QString class and related Unicode functions
**
** Created : 920722
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
// Don't define it while compiling this module, or USERS of Qt will
// not be able to link.
#ifdef QT_NO_CAST_ASCII
#undef QT_NO_CAST_ASCII
#endif
#include "qstring.h"
#include "qregexp.h"
#include "qdatastream.h"
#include "qtextcodec.h"
#include "qstack.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <limits.h>
/* -------------------------------------------------------------------------
 * unicode information
 * these tables are generated from the unicode reference file
 * ftp://ftp.unicode.org/Public/3.0-Update/UnicodeData-3.0.0.html
 *
 * Lars Knoll <knoll@mpi-hd.mpg.de>
 * -------------------------------------------------------------------------
 */
/* Perl script to generate (run perl -x tools/qstring.cpp)
#!perl
sub numberize
{
    my(%r, $n, $id);
    for $id ( @_ ) {
	$id="" if $id eq "EMPTY";
	$r{$id}=$n++;
    }
    return %r;
}
# Code to integer mappings...
#
%category_code = numberize(qw{
      EMPTY
      Mn Mc Me
      Nd Nl No
      Zs Zl Zp
      Cc Cf Cs Co Cn
      Lu Ll Lt Lm Lo
      Pc Pd Ps Pe Pi Pf Po
      Sm Sc Sk So
});
%bidi_category_code = numberize(qw{
      L R EN ES ET AN CS B S WS ON LRE LRO AL RLE RLO PDF NSM BN});
%character_decomposition_tag = numberize(qw{
      <single> <canonical> <font> <noBreak> <initial> <medial>
      <final> <isolated> <circle> <super> <sub> <vertical>
      <wide> <narrow> <small> <square> <compat> <fraction>
});
%mirrored_code = numberize(qw{N Y});
%joining_code = numberize(qw{U D R C});
# Read data into hashes...
#
open IN, "UnicodeData.txt";
$position = 1;
while (<IN>) {
    @fields = split /;/;
    $code = shift @fields;
    for $n (qw{
      name category combining_class bidi_category
      character_decomposition decimal_digit_value digit_value
      numeric_value mirrored oldname comment
      uppercase lowercase titlecase})
    {
	$id = shift @fields;
	$codes = "${n}_code";
	if ( defined %$codes && defined $$codes{$id} ) {
	    $id = $$codes{$id};
	}
	${$n}{$code}=$id;
    }
    $decomp = $character_decomposition{$code};
    if ( length $decomp == 0 ) {
	$decomp = "<single>";
    }
    if (substr($decomp, 0, 1) ne '<') {
	$decomp = "<canonical> " . $decomp;
    }
    @fields = split(" ", $decomp);
    $tag = shift @fields;
    $tag = $character_decomposition_tag{$tag};
    $decomp = join( ", 0x", @fields );
    $decomp = "0x".$decomp;
    $decomposition{$code} = $decomp;
    $decomposition_tag{$code} = $tag;
    $decomposition_pos{$code} = $position;
    $len = scalar(@fields);
    $decomposition_len{$code} = $len;
#   we use canonical decompositions longer than 1 char
#   and all arabic ligatures for the ligature table
    if(($len > 1 and $tag == 1) or ($tag > 3 and $tag < 8)) {
#      ligature to add...
	$start = shift @fields;
	$ligature{$start} = $ligature{$start}." ".$code;
    }
#   adjust position
    if($len != 0) {
	$position += $len + 3;
    }
}
open IN2, "ArabicShaping.txt";
$position = 1;
while (<IN2>) {
    @fields = split /;/;
    $code = shift @fields;
    $dummy = shift @fields;
    $join = shift @fields;
    $join =~ s/ //g;
    $join = $joining_code{$join};
    $joining{$code}=$join;
}
# Build pages...
#
$rowtable_txt =
    "static const Q_UINT8 * const unicode_info[256] = {";
for $row ( 0..255 ) {
    $nonzero=0;
    $txt = "";
    for $cell ( 0..255 ) {
	$code = sprintf("%02X%02X",$row,$cell);
	$info = $category{$code};
	$info = 0 if !defined $info;
	$txt .= "\n    " if $cell%8 == 0;
	$txt .= "$info, ";
    }
    $therow = $row{$txt};
    if ( !defined $therow ) {
	$size+=256;
	$therow = "ui_".sprintf("%02X",$row);
	$rowtext{$therow} =
	    "static const Q_UINT8 ${therow}[] = {$txt\n};\n\n";
	$row{$txt}=$therow;
    }
    $rowtable_txt .= "\n    " if $row%8 == 0;
    $rowtable_txt .= "$therow, ";
}
print "// START OF GENERATED DATA\n\n";
print "#ifndef QT_NO_UNICODETABLES\n\n";
# Print pages...
#
for $r ( sort keys %rowtext ) {
    print $rowtext{$r};
}
print "$rowtable_txt\n};\n";
$size += 256*4;
print "// $size bytes\n\n";
# Build decomposition tables
#
$rowtable_txt =
    "static const Q_UINT16 * const decomposition_info[256] = {";
$table_txt =
    "static const Q_UINT16 decomposition_map[] = {\n    0,\n";
for $row ( 0..255 ) {
    $nonzero=0;
    $txt = "";
    for $cell ( 0..255 ) {
	$code = sprintf("%02X%02X",$row,$cell);
	$txt .= "\n   " if $cell%8 == 0;
	if( $decomposition_tag{$code} != 0 ) {
	    $txt .= " $decomposition_pos{$code},";
	    $table_txt .= "    $decomposition_tag{$code},";
	    $table_txt .= " 0x$code,";
	    $table_txt .= " $decomposition{$code}, 0,\n";
	    $size += 2 * $decomposition_len{$code} + 6;
	} else {
	    $txt .= " 0,";
	}
    }
    $therow = $row{$txt};
    if ( !defined $therow ) {
	$size+=512;
	$therow = "di_".sprintf("%02X",$row);
	$dec_rowtext{$therow} =
	    "static const Q_UINT16 ${therow}[] = {$txt\n};\n\n";
	$row{$txt}=$therow;
    }
    $rowtable_txt .= "\n    " if $row%8 == 0;
    $rowtable_txt .= "$therow, ";
}
# Print decomposition tables
#
print "$table_txt\n};\n\n";
for $r ( sort keys %dec_rowtext ) {
    print $dec_rowtext{$r};
}
print "$rowtable_txt\n};\n";
$size += 256*4;
print "// $size bytes\n\n";
# build ligature tables
#
$size = 0;
$position = 1;
$rowtable_txt =
    "static const Q_UINT16 * const ligature_info[256] = {";
$table_txt =
    "static const Q_UINT16 ligature_map[] = {\n    0,\n";
for $lig_row ( 0..255 ) {
    $nonzero=0;
    $txt = "";
    for $cell ( 0..255 ) {
	$code = sprintf("%02X%02X",$lig_row,$cell);
	$txt .= "\n   " if $cell%8 == 0;
	if( defined $ligature{$code} ) {
	    $txt .= " $position,";
	    @ligature = split(" ", $ligature{$code});
#           we need to sort ligatures according to their length.
#           long ones have to come first!
	    @ligature_sort = sort { $decomposition_len{$b} <=>  $decomposition_len{$a} } @ligature;
#           now replace each code by it's position in
#           the decomposition map.
	    undef(@lig_pos);
	    for $n (@ligature_sort) {
		push(@lig_pos, $decomposition_pos{$n});
	    }
#           debug info
	    if( 0 ) {
		print "ligatures: $ligature{$code}\n";
		$sort = join(" ", @ligature_sort);
		print "sorted   : $sort\n";
	    }
	    $lig = join(", ", @lig_pos);
	    $table_txt .= "    $lig, 0,\n";
	    $size += 2 * scalar(@ligature) + 2;
	    $position += scalar(@ligature) + 1;
	} else {
	    $txt .= " 0,";
	}
    }
    $therow = $lig_row{$txt};
    if ( !defined $therow ) {
	$size+=512;
	$therow = "li_".sprintf("%02X",$lig_row);
	$lig_rowtext{$therow} =
	    "static const Q_UINT16 ${therow}[] = {$txt\n};\n\n";
	$lig_row{$txt}=$therow;
    }
    $rowtable_txt .= "\n    " if $lig_row%8 == 0;
    $rowtable_txt .= "$therow, ";
}
# Print ligature tables
#
print "$table_txt\n};\n\n";
for $r ( sort keys %lig_rowtext ) {
    print $lig_rowtext{$r};
}
print "$rowtable_txt\n};\n";
$size += 256*4;
print "// $size bytes\n\n";
# Build direction/joining/mirrored pages...
#
$rowtable_txt =
    "static const Q_UINT8 * const direction_info[256] = {";
for $dir_row ( 0..255 ) {
    $nonzero=0;
    $txt = "";
    for $cell ( 0..255 ) {
	$code = sprintf("%02X%02X",$dir_row,$cell);
	$dir = $bidi_category{$code};
	$dir = 0 if !defined $dir;
	$join = $joining{$code};
	$join = 0 if !defined $join;
	$mirr = $mirrored{$code};
	$mirr = 0 if !defined $mirr;
	$info = $dir + 32*$join + 128*$mirr;
	$txt .= "\n    " if $cell%8 == 0;
	$txt .= "$info, ";
    }
    $therow = $dir_row{$txt};
    if ( !defined $therow ) {
	$size+=256;
	$therow = "dir_".sprintf("%02X",$dir_row);
	$dir_rowtext{$therow} =
	    "static const Q_UINT8 ${therow}[] = {$txt\n};\n\n";
	$dir_row{$txt}=$therow;
    }
    $rowtable_txt .= "\n    " if $dir_row%8 == 0;
    $rowtable_txt .= "$therow, ";
}
# Print pages...
#
for $r ( sort keys %dir_rowtext ) {
    print $dir_rowtext{$r};
}
print "$rowtable_txt\n};\n";
$size += 256*4;
print "// $size bytes\n\n";
print "#endif\n\n";
print "// END OF GENERATED DATA\n\n";
__END__
*/
// START OF GENERATED DATA
static const Q_UINT8 ui_00[] = {(10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (7), (26), (26), (26), (28), (26), (26), (26), (22), (23), (26), (27), (26), (21), (26), (26), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (26), (26), (27), (27), (27), (26), (26), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (22), (26), (23), (29), (20), (29), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (22), (27), (23), (27), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (7), (26), (28), (28), (28), (28), (30), (30), (29), (30), (16), (24), (27), (21), (30), (29), (30), (27), (6), (6), (29), (16), (30), (26), (29), (6), (16), (25), (6), (6), (6), (26), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (15), (27), (15), (15), (15), (15), (15), (15), (15), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (16), (27), (16), (16), (16), (16), (16), (16), (16), (16)};
#ifndef QT_NO_UNICODETABLES
// 15616 bytes
// 68080 bytes
// 16188 bytes
// 26940 bytes
#endif
// END OF GENERATED DATA
// This is generated too. Script?
#ifndef QT_NO_UNICODETABLES
/*
 * ----------------------------------------------------------------------
 * End of unicode tables
 * ----------------------------------------------------------------------
 */
#endif

static int ucstrcmp(const class QString &as,const class QString &bs)
{
  const class QChar *a = as .  unicode ();
  const class QChar *b = bs .  unicode ();
  if (a == b) {
    return 0;
  }
  if (a == 0) {
    return 1;
  }
  if (b == 0) {
    return -1;
  }
  int l = (as .  length () < bs .  length ()?as .  length () : bs .  length ());
  while((l--) && (( *a)==( *b)))
    (a++ , b++);
  if (l == -1) {
    return (as .  length () - bs .  length ());
  }
  return (a ->  unicode ()) - (b ->  unicode ());
}

static int ucstrncmp(const class QChar *a,const class QChar *b,int l)
{
  while((l--) && (( *a)==( *b)))
    (a++ , b++);
  if (l == -1) {
    return 0;
  }
  return (a ->  unicode ()) - (b ->  unicode ());
}

static int ucstrnicmp(const class QChar *a,const class QChar *b,int l)
{
  while((l--) && (a ->  lower ()==b ->  lower ()))
    (a++ , b++);
  if (l == -1) {
    return 0;
  }
  class QChar al = a ->  lower ();
  class QChar bl = b ->  lower ();
  return (al .  unicode ()) - (bl .  unicode ());
}
// NOT REVISED
/*! \class QCharRef qstring.h
  \brief The QCharRef class is a helper class for QString.
  It provides the ability to work on characters in a QString in a natural
  fashion.
  When you get an object of type QCharRef, you can assign to it, which
  will operate on the string from which you got it.  That is its whole
  purpose in life.  It becomes invalid once further modifications are
  made to the string: If you want to keep it, copy it into a QChar.
  Most of the QChar member functions also exist in QCharRef.  However,
  they are not explicitly documented here.
  \sa QString::operator[]() QString::at() QChar
*/
/*! \class QChar qstring.h
\brief The QChar class provides a light-weight Unicode character.
Unicode characters are (so far) 16-bit entities without any markup or
structure.  This class represents such an entity.  It is rather
light-weight, so it can be used everywhere.  Most compilers treat it
approximately like "short int".  (In a few years, it may be necessary
to make QChar 32-bit, once more than 65536 Unicode code points have
been defined and come into use.)
QChar provides a full complement of testing/classification functions,
conversion to and from other formats, from composed to decomposed
unicode, and will try to compare and case-convert if you ask it to.
The classification functions include functions like those in ctype.h,
but operating on the full range of unicode characters.  They all
return TRUE if the character is a certain type of character, and FALSE
otherwise.
These functions are: isNull() (returns TRUE if the character is
U+0000), isPrint() (TRUE if the character is any sort of printable
character, including whitespace), isPunct() (any sort of punctation),
isMark() (Unicode Marks), isLetter (letters), isNumber() (any sort of
numeric characters), isLetterOrNumber(),  and isDigit() (decimal digits).
All of these are wrappers around category(), which returns the
unicode-defined category of each character.
QChar further provides direction(), which indicates the "natural"
writing direction of this character, joining(), which indicates how
this character joins with its neighbors (needed mostly for Arabic)
and finally mirrored(), which indicates whether this character needs
to be mirrored when it is printed in its unnatural writing
direction.
Composed Unicode characters (like &aring;) can be converted to
decomposed Unicode ("a" followed by "ring above") using
decomposition().
In Unicode, comparison is not necessarily possible, and case
conversion is at best very hard.  Unicode, covering the "entire"
globe, also includes a globe-sized collection of case and sorting
problems.  Qt tries, but not very hard: operator== and friends will do
comparison based purely on the numeric Unicode value (code point) of
the characters, and upper() and lower() will do case changes when the
character has a well-defined upper/lower-case equivalent. There is no
provision for locale-dependent case folding rules or comparison: These
functions are meant to be fast, so they can be used unambiguously in
data structures.
The conversion functions include unicode() (to a scalar), latin1() (to
scalar, but converts all non-Latin1 characters to 0), row() (gives the
Unicode row), cell() (gives the unicode cell), digitValue() (gives the
integer value of any of the numerous digit characters), and a host of
constructors.
\sa QString QCharRef \link unicode.html About Unicode \endlink
*/
/*! \enum QChar::Category
This enum maps the Unicode character categories.  The currently known
categories are: <ul>
<li> \c NoCategory - used when Qt is dazed and confused and cannot
make sense of anything.
<li> \c Mark_NonSpacing - (Mn) -
<li> \c Mark_SpacingCombining - (Mc) -
<li> \c Mark_Enclosing - (Me) -
<li> \c Number_DecimalDigit - (Nd) -
<li> \c Number_Letter - (Nl) -
<li> \c Number_Other - (No) -
<li> \c Separator_Space - (Zs) -
<li> \c Separator_Line - (Zl) -
<li> \c Separator_Paragraph - (Zp) -
<li> \c Other_Control - (Cc) -
<li> \c Other_Format - (Cf) -
<li> \c Other_Surrogate - (Cs) -
<li> \c Other_PrivateUse - (Co) -
<li> \c Other_NotAssigned - (Cn) -
<li> \c Letter_Uppercase - (Lu) -
<li> \c Letter_Lowercase - (Ll) -
<li> \c Letter_Titlecase - (Lt) -
<li> \c Letter_Modifier - (Lm) -
<li> \c Letter_Other - (Lo) -
<li> \c Punctuation_Connector - (Pc) -
<li> \c Punctuation_Dask - (Pd) -
<li> \c Punctuation_Open - (Ps) -
<li> \c Punctuation_Close - (Pe) -
<li> \c Punctuation_InitialQuote - (Pi) -
<li> \c Punctuation_FinalQuote - (Pf) -
<li> \c Punctuation_Other - (Po) -
<li> \c Symbol_Math - (Sm) -
<li> \c Symbol_Currency - (Sc) -
<li> \c Symbol_Modifier - (Sk) -
<li> \c Symbol_Other - (So) -
</ul>
*/
/*! \enum QChar::Direction
  This enum type defines the Unicode direction attributes.
  See <a href="http://www.unicode.org">the Unicode Standard</a>
  for a description of the values.
  In order to conform to C/C++ naming conventions "Dir" is
  prepended to the codes used in The Unicode Standard.
*/
/*! \enum QChar::Decomposition
  This enum type defines the Unicode decomposition attributes.
  See <a href="http://www.unicode.org">the Unicode Standard</a>
  for a description of the values.
*/
/*! \enum QChar::Joining
  This enum type defines the Unicode decomposition attributes.
  See <a href="http://www.unicode.org">the Unicode Standard</a>
  for a description of the values.
*/
/*! \fn QChar::QChar()
Constructs a null QChar (one that isNull()).
*/
/*! \fn QChar::QChar( char c )
Constructs a QChar corresponding to ASCII/Latin1 character \a c.
*/
/*! \fn QChar::QChar( uchar c )
Constructs a QChar corresponding to ASCII/Latin1 character \a c.
*/
/*! \fn QChar::QChar( uchar c, uchar r )
Constructs a QChar for Unicode cell \a c in row \a r.
*/
/*! \fn QChar::QChar( const QChar& c )
Constructs a copy of \a c.  This is a deep copy, if such a
light-weight object can be said to have deep copies.
*/
/*! \fn QChar::QChar( ushort rc )
Constructs a QChar for the character with Unicode code point \a rc.
*/
/*! \fn QChar::QChar( short rc )
Constructs a QChar for the character with Unicode code point \a rc.
*/
/*! \fn QChar::QChar( uint rc )
Constructs a QChar for the character with Unicode code point \a rc.
*/
/*! \fn QChar::QChar( int rc )
Constructs a QChar for the character with Unicode code point \a rc.
*/
/*! \fn bool  QChar::networkOrdered ()
  Returns TRUE if this character is in network byte order (MSB first),
  and FALSE if it is not.  This is a platform-dependent property, so
  we strongly advise against using this function in portable code.
*/
/*!
  \fn bool QChar::isNull() const
  Returns TRUE if the characters is the unicode character 0x0000,
  ie. ASCII NUL.
*/
/*!
  \fn uchar QChar::cell () const
  Returns the cell (least significant byte) of the Unicode character.
*/
/*!
  \fn uchar QChar::row () const
  Returns the row (most significant byte) of the Unicode character.
*/
/*!
  \fn uchar& QChar::cell ()
  Returns a reference to the cell (least significant byte) of the Unicode character.
*/
/*!
  \fn uchar& QChar::row ()
  Returns a reference to the row (most significant byte) of the Unicode character.
*/
/*!
  Returns whether the character is a printable character.  This is
  any character not of category Cc or Cn.  Note that this gives no indication
  of whether the character is available in some font.
*/

bool QChar::isPrint() const
{
  enum Category c = (this) ->  category ();
  return !(c == Other_Control || c == Other_NotAssigned);
}
/*!
  Returns whether the character is a separator
  character (Separator_* categories).
*/

bool QChar::isSpace() const
{
  if (!((this) ->  row ())) {
    if (((this) ->  cell ()) >= 9 && ((this) ->  cell ()) <= 13) {
      return TRUE;
    }
  }
  enum Category c = (this) ->  category ();
  return c >= Separator_Space && c <= Separator_Paragraph;
}
/*!
  Returns whether the character is a mark (Mark_* categories).
*/

bool QChar::isMark() const
{
  enum Category c = (this) ->  category ();
  return c >= Mark_NonSpacing && c <= Mark_Enclosing;
}
/*!
  Returns whether the character is punctuation (Punctuation_* categories).
*/

bool QChar::isPunct() const
{
  enum Category c = (this) ->  category ();
  return c >= Punctuation_Connector && c <= Punctuation_Other;
}
/*!
  Returns whether the character is a letter (Letter_* categories).
*/

bool QChar::isLetter() const
{
  enum Category c = (this) ->  category ();
  return c >= Letter_Uppercase && c <= Letter_Other;
}
/*!
  Returns whether the character is a number (of any sort - Number_* categories).
  \sa isDigit()
*/

bool QChar::isNumber() const
{
  enum Category c = (this) ->  category ();
  return c >= Number_DecimalDigit && c <= Number_Other;
}
/*!
  Returns whether the character is a letter or number (Letter_* or Number_* categories).
*/

bool QChar::isLetterOrNumber() const
{
  enum Category c = (this) ->  category ();
  return c >= Letter_Uppercase && c <= Letter_Other || c >= Number_DecimalDigit && c <= Number_Other;
}
/*!
  Returns whether the character is a decimal digit (Number_DecimalDigit).
  */

bool QChar::isDigit() const
{
  return ((this) ->  category ()) == Number_DecimalDigit;
}
/*!
  Returns the numeric value of the digit, or -1 if the character is not
  a digit.
*/

int QChar::digitValue() const
{
#ifndef QT_NO_UNICODETABLES
#else
// ##### just latin1
  if (((this) -> rw) != 0 || ((this) -> cl) < '0' || ((this) -> cl) > '9') {
    return -1;
  }
  else {
    return ((this) -> cl) - '0';
  }
#endif
}
/*!
  Returns the character category.
  \sa Category
*/

QChar::Category QChar::category() const
{
#ifndef QT_NO_UNICODETABLES
#else
// ### just ASCII
  if (((this) -> rw) == 0) {
    return (enum Category )ui_00[(this) ->  cell ()];
  }
//#######
  return Letter_Uppercase;
#endif
}
/*!
  Returns the characters directionality.
  \sa Direction
*/

QChar::Direction QChar::direction() const
{
#ifndef QT_NO_UNICODETABLES
#else
  return DirL;
#endif
}
/*!
  This function is not supported (it may change to use Unicode
  character classes).
  Returns information about the joining properties of the
  character (needed for arabic).
*/

QChar::Joining QChar::joining() const
{
#ifndef QT_NO_UNICODETABLES
#else
  return OtherJoining;
#endif
}
/*!
  Returns whether the character is a mirrored character (one that
  should be reversed if the text direction is reversed).
*/

bool QChar::mirrored() const
{
#ifndef QT_NO_UNICODETABLES
#else
  return FALSE;
#endif
}
/*!
  Returns the mirrored char if this character is a mirrored char, the char
  itself otherwise
*/

QChar QChar::mirroredChar() const
{
#ifndef QT_NO_UNICODETABLES
#else
  return ( *(this));
#endif
}
/*!
  Decomposes a character into its parts. Returns QString::null if
  no decomposition exists.
*/

QString QChar::decomposition() const
{
#ifndef QT_NO_UNICODETABLES
#else
  return ((null));
#endif
}
/*!
  Returns the tag defining the composition of the character.
  Returns QChar::Single if no decomposition exists.
*/

QChar::Decomposition QChar::decompositionTag() const
{
#ifndef QT_NO_UNICODETABLES
#else
// ########### FIX eg. just latin1
  return Single;
#endif
}
/*!
  Returns the lowercase equivalent if the character is uppercase,
  or the character itself otherwise.
*/

QChar QChar::lower() const
{
#ifndef QT_NO_UNICODETABLES
#else
  if (((this) ->  row ())) {
    return ( *(this));
  }
  else {
    return ::QChar::QChar(tolower(((this) ->  latin1 ())));
  }
#endif
}
/*!
  Returns the uppercase equivalent if the character is lowercase,
  or the character itself otherwise.
*/

QChar QChar::upper() const
{
#ifndef QT_NO_UNICODETABLES
#else
  if (((this) ->  row ())) {
    return ( *(this));
  }
  else {
    return ::QChar::QChar(toupper(((this) ->  latin1 ())));
  }
#endif
}
/*!
  \fn QChar::operator char() const
  Returns the Latin1 character equivalent to the QChar,
  or 0.  This is mainly useful for non-internationalized software.
  \sa unicode()
*/
/*!
  \fn ushort QChar::unicode() const
  Returns the numeric Unicode value equal to the QChar.  Normally, you
  should use QChar objects as they are equivalent, but for some low-level
  tasks (eg. indexing into an array of Unicode information), this function
  is useful.
*/
/*****************************************************************************
  Documentation of QChar related functions
 *****************************************************************************/
/*!
  \fn int operator==( QChar c1, QChar c2 )
  \relates QChar
  Returns TRUE if \a c1 and \a c2 are the same Unicode character.
*/
/*!
  \fn int operator==( char ch, QChar c )
  \relates QChar
  Returns TRUE if \a c is the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator==( QChar c, char ch )
  \relates QChar
  Returns TRUE if \a c is the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator!=( QChar c1, QChar c2 )
  \relates QChar
  Returns TRUE if \a c1 and \a c2 are not the same Unicode character.
*/
/*!
  \fn int operator!=( char ch, QChar c )
  \relates QChar
  Returns TRUE if \a c is not the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator!=( QChar c, char ch )
  \relates QChar
  Returns TRUE if \a c is not the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator<=( QChar c1, QChar c2 )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c1 is less than that
  of \a c2, or they are the same Unicode character.
*/
/*!
  \fn int operator<=( QChar c, char ch )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c is less than or
  equal to that of the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator<=( char ch, QChar c )
  \relates QChar
  Returns TRUE if the numeric Unicode value of the ASCII/Latin1
  character \a ch is less than or equal to that of \a c.
*/
/*!
  \fn int operator>=( QChar c1, QChar c2 )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c1 is greater than that
  of \a c2, or they are the same Unicode character.
*/
/*!
  \fn int operator>=( QChar c, char ch )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c is greater than or
  equal to that of the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator>=( char ch, QChar c )
  \relates QChar
  Returns TRUE if the numeric Unicode value of the ASCII/Latin1
  character \a ch is greater than or equal to that of \a c.
*/
/*!
  \fn int operator<( QChar c1, QChar c2 )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c1 is less than that
  of \a c2.
*/
/*!
  \fn int operator<( QChar c, char ch )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c is less than that
  of the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator<( char ch, QChar c )
  \relates QChar
  Returns TRUE if the numeric Unicode value of the ASCII/Latin1
  character \a ch is less than that of \a c.
*/
/*!
  \fn int operator>( QChar c1, QChar c2 )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c1 is greater than
  that of \a c2.
*/
/*!
  \fn int operator>( QChar c, char ch )
  \relates QChar
  Returns TRUE if the numeric Unicode value of \a c is greater than
  that of the ASCII/Latin1 character \a ch.
*/
/*!
  \fn int operator>( char ch, QChar c )
  \relates QChar
  Returns TRUE if the numeric Unicode value of the ASCII/Latin1
  character \a ch is greater than that of \a c.
*/
#ifndef QT_NO_UNICODETABLES
// small class used internally in QString::Compose()
#endif
// this function is just used in QString::compose()

inline static bool format(enum QChar::Decomposition tag,class QString &str,int index,int len)
{
  unsigned int l = (index + len);
  unsigned int r = index;
  bool left = FALSE;
  bool right = FALSE;
  left = l < str .  length () && ((str[((int )l)] .  joining ()) == QChar::Dual || (str[((int )l)] .  joining ()) == QChar::Right);
  if (r > 0) {
    r--;
//printf("joining(right) = %d\n", str[(int)r].joining());
    right = (str[((int )r)] .  joining ()) == QChar::Dual;
  }
  switch(tag){
    case QChar::Medial:
    return (left & right);
    case QChar::Initial:
    return left && !right;
    case QChar::Final:
// && !left);
    return right;
    case QChar::Isolated:
{
    }
    default:
    return !right && !left;
  }
// format()
}
/*
  QString::compose() and visual() were developed by Gordon Tisher
  <tisher@uniserve.ca>, with input from Lars Knoll <knoll@mpi-hd.mpg.de>,
  who developed the unicode data tables.
*/
/*!
  Note that this function is not supported in Qt 2.0, and is merely
  for experimental and illustrative purposes.  It is mainly of interest
  to those experimenting with Arabic and other composition-rich texts.
  Applies possible ligatures to a QString, useful when composition-rich
  text requires rendering with glyph-poor fonts, but also
  makes compositions such as QChar(0x0041) ('A') and QChar(0x0308)
  (Unicode accent diaresis) giving QChar(0x00c4) (German A Umlaut).
*/

void QString::compose()
{
#ifndef QT_NO_UNICODETABLES
//printf("\n\nligature for 0x%x:\n", code.unicode());
// we exclude Arabic presentation forms A and a few
// other ligatures, which are undefined in most fonts
// joining info is only needed for arabic
//printf("using ligature 0x%x, len=%d\n",code,len);
// replace letter
// we continue searching in case we have a final
// form because medial ones are preferred.
#endif
}
static class QChar LRM(((ushort )0x200e));
static class QChar RLM(((ushort )0x200f));
static class QChar LRE(((ushort )0x202a));
static class QChar RLE(((ushort )0x202b));
static class QChar RLO(((ushort )0x202e));
static class QChar LRO(((ushort )0x202d));
static class QChar PDF(((ushort )0x202c));
#if 0
#endif

inline static bool is_neutral(unsigned short dir)
{
  return dir == QChar::DirB || dir == QChar::DirS || dir == QChar::DirWS || dir == QChar::DirON || dir == QChar::DirNSM;
}
/*!
  This function returns the basic directionality of the string (QChar::DirR for
  right to left and QChar::DirL for left to right). Useful to find the right
  alignment.
  */

QChar::Direction QString::basicDirection()
{
#ifndef QT_NO_UNICODETABLES
// find base direction
// not R and not L
#endif
  return QChar::DirL;
}
#ifndef QT_NO_UNICODETABLES
// reverses part of the QChar array to get visual ordering
// called from QString::visual()
//
// small class used for the ordering algorithm in QString::visual()
// matrix for resolving neutral types
#define NEG1 (QChar::Direction)(-1)
#endif
/*!
  This function returns the QString ordered visually. Useful for
  painting the string or when transforming to a visually ordered
  encoding.
*/

QString QString::visual(int index,int len)
{
#ifndef QT_NO_UNICODETABLES
// #### This needs much more optimizing - it is called for
// #### every text operation.
// check bounds
// find base direction
// not R and not L
// is there any BiDi char at all?
// explicit override pass
//unsigned int code_count = 0;
//code_count++;
//code_count++;
//code_count++;
//code_count++;
//code_count++;
// TODO: catch block separators (newlines, paras, etc.)
// weak type pass
// neutral type pass
// declaring l here shadowed previous l
// implicit level pass
// do nothing
// fall through
// now do the work!
#else
  return (this) ->  mid (index,len);
#endif
}
// These macros are used for efficient allocation of QChar strings.
// IMPORTANT! If you change these, make sure you also change the
// "delete unicode" statement in ~QStringData() in qstring.h correspondingly!
#define QT_ALLOC_QCHAR_VEC( N ) (QChar*) new char[ sizeof(QChar)*( N ) ]
#define QT_DELETE_QCHAR_VEC( P ) delete[] ((char*)( P ))
/*!
  This utility function converts the 8-bit string
  \a ba to Unicode, returning the result.
  The caller is responsible for deleting the return value with delete[].
*/

QChar *QString::asciiToUnicode(const QByteArray &ba,uint *len)
{
  if (ba .  isNull ()) {
     *len = 0;
    return 0;
  }
  int l = 0;
  while(l < ((int )(ba .  size ())) && (ba[l]))
    l++;
  char *str = ba .  data ();
// Can't use macro, since function is public
  class QChar *uc = new QChar [l * 2UL];
  class QChar *result = uc;
  if (len) {
     *len = l;
  }
  while((l--))
     *(uc++) = ( *(str++));
  return result;
}

static class QChar *internalAsciiToUnicode(const QByteArray &ba,uint *len)
{
  if (ba .  isNull ()) {
     *len = 0;
    return 0;
  }
  int l = 0;
  while(l < ((int )(ba .  size ())) && (ba[l]))
    l++;
  char *str = ba .  data ();
  class QChar *uc = (class QChar *)(new char [sizeof(QChar ) * l]);
  class QChar *result = uc;
  if (len) {
     *len = l;
  }
  while((l--))
     *(uc++) = ( *(str++));
  return result;
}
/*!
  This utility function converts the NUL-terminated 8-bit string
  \a str to Unicode, returning the result and setting \a len to
  the length of the Unicode string.
  The caller is responsible for deleting the return value with delete[].
*/

QChar *QString::asciiToUnicode(const char *str,uint *len,uint maxlen)
{
  class QChar *result = 0;
  uint l = 0;
  if (str) {
    if (maxlen != ((uint )(-1))) {
      while(l < maxlen && str[l])
        l++;
    }
    else {
// Faster?
      l = qstrlen(str);
    }
// Can't use macro since function is public
    class QChar *uc = new QChar [l * 2UL];
    result = uc;
    uint i = l;
    while((i--))
       *(uc++) = ( *(str++));
  }
  if (len) {
     *len = l;
  }
  return result;
}

static class QChar *internalAsciiToUnicode(const char *str,uint *len,uint maxlen = (uint )(-1))
{
  class QChar *result = 0;
  uint l = 0;
  if (str) {
    if (maxlen != ((uint )(-1))) {
      while(l < maxlen && str[l])
        l++;
    }
    else {
// Faster?
      l = qstrlen(str);
    }
    class QChar *uc = (class QChar *)(new char [sizeof(QChar ) * l]);
    result = uc;
    uint i = l;
    while((i--))
       *(uc++) = ( *(str++));
  }
  if (len) {
     *len = l;
  }
  return result;
}
/*!
  This utility function converts \a l 16-bit characters from
  \a uc to ASCII, returning a NUL-terminated string.
  The caller is responsible for deleting the string with delete[].
*/

char *QString::unicodeToAscii(const class QChar *uc,uint l)
{
  if (!uc) {
    return 0;
  }
  char *a = new char [(l + 1)];
  char *result = a;
  while((l--))
     *(a++) = (( *(uc++)) .  operator char ());
   *a = '\0';
  return result;
}

static uint computeNewMax(uint len)
{
  if (len >= 0x80000000) {
    return len;
  }
  uint newMax = 4;
  while(newMax < len)
    newMax *= 2;
// try to save some memory
  if (newMax >= (1024 * 1024) && len <= newMax - (newMax >> 2)) {
    newMax -= newMax >> 2;
  }
  return newMax;
}
/*!
  Returns the QString as a zero terminated array of unsigned shorts
  if the string is not null; otherwise returns zero.
  The result remains valid so long as one unmodified
  copy of the source string exists.
 */

const unsigned short *QString::ucs2() const
{
  if (!((this) -> d -> QStringData::unicode)) {
    return 0;
  }
  unsigned int len = (this) -> d -> QStringData::len;
  if (((this) -> d -> QStringData::maxl) < len + 1) {
// detach, grow or shrink
    uint newMax = computeNewMax(len + 1);
    class QChar *nd = (class QChar *)(new char [sizeof(QChar ) * newMax]);
    if (nd) {
      if (((this) -> d -> QStringData::unicode)) {
        memcpy(nd,((this) -> d -> QStringData::unicode),sizeof(class QChar ) * len);
      }
      ((class ::QString *)(this)) ->  deref ();
      ((class ::QString *)(this)) -> d = (new QStringData (nd,len,newMax));
    }
  }
  (this) -> d -> QStringData::unicode[len] = (0);
  return ((unsigned short *)((this) -> d -> QStringData::unicode));
}
/*!
    Constructs a string that is a deep copy of \a str, interpreted as a
    UCS2 encoded, zero terminated, Unicode string.
    If \a str is 0, then a null string is created.
    \sa isNull()
 */

QString QString::fromUcs2(const unsigned short *str)
{
  if (!str) {
    return (null);
  }
  else {
    int length = 0;
    while(str[length] != 0)
      length++;
    class QChar *uc = (class QChar *)(new char [sizeof(QChar ) * length]);
    memcpy(uc,str,length * sizeof(class QChar ));
    return ::QString::QString((new QStringData (uc,length,length)),TRUE);
  }
}
/*****************************************************************************
  QString member functions
 *****************************************************************************/
/*!
  \class QString qstring.h
  \brief The QString class provides an abstraction of Unicode text and
	  the classic C null-terminated char array (<var>char*</var>).
  \ingroup tools
  \ingroup shared
  QString uses \link shclass.html implicit sharing\endlink, and so it
  is very efficient and easy to use.
  In all QString methods that take <var>const char*</var> parameters,
  the <var>const char*</var> is interpreted as a classic C-style
  0-terminated ASCII string. It is legal for the <var>const
  char*</var> parameter to be 0. The results are undefined if the
  <var>const char*</var> string is not 0-terminated. Functions that
  copy classic C strings into a QString will not copy the terminating
  0-character. The QChar array of the QString (as returned by
  unicode()) is not terminated by a null.
  A QString that has not been assigned to anything is \a null, i.e. both
  the length and data pointer is 0. A QString that references the empty
  string ("", a single '\0' char) is \a empty.	Both null and empty
  QStrings are legal parameters to the methods. Assigning <var>const char
  * 0</var> to QString gives a null QString.
  Note that if you find that you are mixing usage of QCString, QString,
  and QByteArray, this causes lots of unnecessary copying and might
  indicate that the true nature of the data you are dealing with is
  uncertain.  If the data is NUL-terminated 8-bit data, use QCString;
  if it is unterminated (ie. contains NULs) 8-bit data, use QByteArray;
  if it is text, use QString.
  \sa QChar \link shclass.html Shared classes\endlink
*/
struct QStringData *QString::shared_null = 0;
//QT_STATIC_CONST_IMPL QString QString::null;
const class QChar QChar::null;
const class QChar QChar::replacement(((ushort )0xfffd));
const class QChar QChar::byteOrderMark(((ushort )0xfeff));
const class QChar QChar::byteOrderSwapped(((ushort )0xfffe));
const class QChar QChar::nbsp(((ushort )0x00a0));
#if defined(_CC_MSVC_) && _MSC_VER <= 1300
#else
const struct QString::Null QString::null = {};
#endif

QStringData *QString::makeSharedNull()
{
  return shared_null = (new QStringData ());
}
// Uncomment this to get some useful statistics.
// #define Q2HELPER(x) x
#ifdef Q2HELPER
#else
#define Q2HELPER(x)
#endif
/*!
  \fn QString::QString()
  Constructs a null string.
  \sa isNull()
*/
/*!
  Constructs a string containing the one character \a ch.
*/

QString::QString(class QChar ch)
{
  (this) -> d = (new QStringData (((QChar *)(new char [2UL * 1UL])),1,1));
  (this) -> d -> QStringData::unicode[0] = ch;
}
/*!
  Constructs an implicitly-shared copy of \a s.
*/

QString::QString(const class ::QString &s) : d(s . d)
{
  ((this) -> d) ->  ref ();
}
/*!
  Private function.
  Constructs a string with preallocated space for \a size characters.
  The string is empty.
  \sa isNull()
*/
/*dummy*/

QString::QString(int size,bool )
{
  if (size) {
    int l = size;
    class QChar *uc = (class QChar *)(new char [sizeof(QChar ) * l]);
    (this) -> d = (new QStringData (uc,0,l));
  }
  else {
    (this) -> d = (shared_null?shared_null : (shared_null = (new QStringData ())));
    ((this) -> d) ->  ref ();
  }
}
/*!
  Constructs a string that is a deep copy of \a ba interpreted as
  a classic C string.
*/

QString::QString(const QByteArray &ba)
{
  uint l;
  class QChar *uc = internalAsciiToUnicode(ba,&l);
  (this) -> d = (new QStringData (uc,l,l));
}

QString::QString(const class QCString &ba)
{
//Q2HELPER(stat_construct_ba++)
//uint l;
//QChar *uc = internalAsciiToUnicode(ba,&l);
//d = new QStringData(uc,l,l);
  class ::QString s =  fromUtf8 ((ba .  data ()),(ba .  length ()));
  (this) -> d = s . d;
  ((this) -> d) ->  ref ();
}
/*!
  Constructs a string that is a deep copy of the
  first \a length QChar in the array \a unicode.
  If \a unicode and \a length are 0, a null string is created.
  If only \a unicode is 0, the string is empty, but has
  \a length characters of space preallocated - QString expands
  automatically anyway, but this may speed some cases up a little.
  \sa isNull()
*/

QString::QString(const class QChar *unicode,uint length)
{
  if (!unicode && !length) {
    (this) -> d = (shared_null?shared_null :  makeSharedNull ());
    ((this) -> d) ->  ref ();
  }
  else {
    class QChar *uc = (class QChar *)(new char [sizeof(QChar ) * length]);
    if (unicode) {
      memcpy(uc,unicode,length * sizeof(class QChar ));
    }
    (this) -> d = (new QStringData (uc,(unicode?length : 0),length));
  }
}
/*!
  Constructs a string that is a deep copy of \a str, interpreted as a
  classic C string.
  If \a str is 0 a null string is created.
  This is a cast constructor, but it is perfectly safe: converting a Latin1
  const char* to QString preserves all the information.
  You can disable this constructor by
  defining QT_NO_CAST_ASCII when you compile your applications.
  You can also make QString objects by using setLatin1()/fromLatin1(), or
  fromLocal8Bit(), fromUtf8(), or whatever encoding is appropriate for
  the 8-bit data you have.
  \sa isNull()
*/

QString::QString(const char *str)
{
//Q2HELPER(stat_construct_charstar++)
//uint l;
//QChar *uc = internalAsciiToUnicode(str,&l);
//Q2HELPER(stat_construct_charstar_size+=l)
//d = new QStringData(uc,l,l);
  class ::QString s =  fromUtf8 (str);
  (this) -> d = s . d;
  ((this) -> d) ->  ref ();
}
/*! \fn QString::~QString()
Destroys the string and frees the "real" string, if this was the last
copy of that string.
*/
/*!
  Deallocates any space reserved solely by this QString.
*/

void QString::real_detach()
{
  (this) ->  setLength ((this) ->  length ());
}

void QString::deref()
{
  if (((this) -> d) ->  deref ()) {
    if ((this) -> d == shared_null) {
      shared_null = 0;
    }
    delete ((this) -> d);
// helps debugging
    (this) -> d = 0;
  }
}

void QStringData::deleteSelf()
{
  delete (this);
}
/*!
  \fn QString& QString::operator=( QChar c )
  Sets the string to contain just the single character \a c.
*/
/*!
  \fn QString& QString::operator=( char c )
  Sets the string to contain just the single character \a c.
*/
/*!
  Assigns a shallow copy of \a s to this string and returns a
  reference to this string.
*/

QString &QString::operator=(const class ::QString &s)
{
  s . d ->  ref ();
  (this) ->  deref ();
  (this) -> d = s . d;
  return  *(this);
}
/*!
  Assigns a deep copy of \a cs, interpreted as a classic C string, to
  this string and returns a reference to this string.
*/

QString &QString::operator=(const class QCString &cs)
{
  return (this) ->  setLatin1 ((cs .  operator const char * ()));
}
/*!
  Assigns a deep copy of \a str, interpreted as a classic C string,
  to this string and returns a reference to this string.
  If \a str is 0 a null string is created.
  \sa isNull()
*/

QString &QString::operator=(const char *str)
{
  return (this) ->  setLatin1 (str);
}
/*!
  \fn bool QString::isNull() const
  Returns TRUE if the string is null.
  A null string is also an empty string.
  Example:
  \code
    QString a;		// a.unicode() == 0,  a.length() == 0
    QString b = "";	// b.unicode() == "", b.length() == 0
    a.isNull();		// TRUE, because a.unicode() == 0
    a.isEmpty();	// TRUE, because a.length() == 0
    b.isNull();		// FALSE, because b.unicode() != 0
    b.isEmpty();	// TRUE, because b.length() == 0
  \endcode
  \sa isEmpty(), length()
*/
/*!
  \fn bool QString::isEmpty() const
  Returns TRUE if the string is empty, i.e. if length() == 0.
  An empty string is not always a null string.
  See example in isNull().
  \sa isNull(), length()
*/
/*!
  \fn uint QString::length() const
  Returns the length of the string.
  Null strings and empty strings have zero length.
  \sa isNull(), isEmpty()
*/
/*!
  Truncates the string at position \a newLen if newLen is less than the
  current length . Otherwise, nothing happens.
  Example:
  \code
    QString s = "truncate this string";
    s.truncate( 5 );				// s == "trunc"
  \endcode
  In Qt 1.x, it was possible to "truncate" a string to a longer
  length.  This is no longer possible.
*/

void QString::truncate(uint newLen)
{
  if (newLen < (this) -> d -> QStringData::len) {
    (this) ->  setLength (newLen);
  }
}
/*### Make this public in 3.0
  Ensures that at least \a newLen characters are allocated, and
  sets the length to \a newLen.  This function always detaches the
  string from other references to the same data.  Any new space
  allocated is \e not defined.
  If \a newLen is 0, this string becomes empty, unless this string is
  null, in which case it remains null.
  \sa truncate(), isNull(), isEmpty()
*/

void QString::setLength(uint newLen)
{
// detach, grow, or
  if (((this) -> d) -> QShared::count != 1 || newLen > ((this) -> d -> QStringData::maxl) || newLen * 4 < ((this) -> d -> QStringData::maxl) && ((this) -> d -> QStringData::maxl) > 4) 
// shrink
{
    uint newMax = 4;
    while(newMax < newLen)
      newMax *= 2;
    class QChar *nd = (class QChar *)(new char [sizeof(QChar ) * newMax]);
    uint len = (this) -> d -> QStringData::len < newLen?(this) -> d -> QStringData::len : newLen;
    if (((this) -> d -> QStringData::unicode)) {
      memcpy(nd,((this) -> d -> QStringData::unicode),sizeof(class QChar ) * len);
    }
    (this) ->  deref ();
    (this) -> d = (new QStringData (nd,newLen,newMax));
  }
  else {
    (this) -> d -> QStringData::len = newLen;
    (this) -> d -> QStringData::dirtyascii = 1;
  }
}
/*!  Returns a string equal to this one, but with the lowest-numbered
  occurrence of \c %i (for a positive integer i) replaced by \a a.
  \code
    label.setText( tr("Rename %1 to %2?").arg(oldName).arg(newName) );
  \endcode
  \a fieldwidth is the minimum amount of space \a a is padded to.  A
  positive value produces right-aligned text, while a negative value
  produces left aligned text.
  \warning Using arg() for constructing "real" sentences
  programmatically is likely to lead to translation problems.
  Inserting objects like numbers or file names is fairly safe.
  \warning Relying on spaces to create alignment is prone to lead to
  translation problems.
  If there is no \c %i pattern, a warning message (qWarning()) is
  printed and the text as appended at the end of the string.  This is
  error recovery and should not occur in correct code.
  \sa QObject::tr()
*/

QString QString::arg(const class ::QString &a,int fieldwidth) const
{
  int pos;
  int len;
  class ::QString r( *(this));
  if (!(this) ->  findArg (pos,len)) {
    qWarning("QString::arg(): Argument missing: %s, %s",((const char *)((this) ->  ascii ())),((const char *)((this) ->  ascii ())));
// Make sure the text at least appears SOMEWHERE
    r += ' ';
    pos = (r .  length ());
    len = 0;
  }
  r .  replace (pos,len,a);
  if (fieldwidth < 0) {
    class ::QString s;
    while(((uint )(-fieldwidth)) > a .  length ()){
      s += ' ';
      fieldwidth++;
    }
    r .  insert ((pos + a .  length ()),s);
  }
  else {
    if (fieldwidth) {
      class ::QString s;
      while(((uint )fieldwidth) > a .  length ()){
        s += ' ';
        fieldwidth--;
      }
      r .  insert (pos,s);
    }
  }
  return (r);
}
/*! \overload
  \a a is expressed in to \a base notation, which is decimal by
  default and must be in the range 2-36 inclusive.
*/

QString QString::arg(long a,int fieldwidth,int base) const
{
  return (this) ->  arg ( number (a,base),fieldwidth);
}
/*! \overload
  \a a is expressed in to \a base notation, which is decimal by
  default and must be in the range 2-36 inclusive.
*/

QString QString::arg(ulong a,int fieldwidth,int base) const
{
  return (this) ->  arg (::QString:: number (a,base),fieldwidth);
}
/*!
  \overload QString QString::arg(int a, int fieldwidth, int base) const
  \a a is expressed in to \a base notation, which is decimal by
  default and must be in the range 2-36 inclusive.
*/
/*!
  \overload QString QString::arg(uint a, int fieldwidth, int base) const
  \a a is expressed in to \a base notation, which is decimal by
  default and must be in the range 2-36 inclusive.
*/
/*!
  \overload QString QString::arg(short a, int fieldwidth, int base) const
  \a a is expressed in to \a base notation, which is decimal by
  default and must be in the range 2-36 inclusive.
*/
/*!
  \overload QString QString::arg(ushort a, int fieldwidth, int base) const
  \a a is expressed in to \a base notation, which is decimal by
  default and must be in the range 2-36 inclusive.
*/
/*! \overload
  \a a is assumed to be in the Latin1 character set.
*/

QString QString::arg(char a,int fieldwidth) const
{
  class ::QString c;
  c += a;
  return (this) ->  arg (c,fieldwidth);
}
/*! \overload
*/

QString QString::arg(class QChar a,int fieldwidth) const
{
  class ::QString c;
  c += a;
  return (this) ->  arg (c,fieldwidth);
}
/*! \overload
  \a is formatted according to the \a fmt format specified, which is
  'g' by default and can be any of 'f', 'F', 'e', 'E', 'g' or 'G', all
  of which have the same meaning as for sprintf().  \a prec determines
  the precision, just as for number() and sprintf().
*/

QString QString::arg(double a,int fieldwidth,char fmt,int prec) const
{
  return (this) ->  arg (::QString:: number (a,fmt,prec),fieldwidth);
}
/*!
  Just 1-digit arguments.
*/

bool QString::findArg(int &pos,int &len) const
{
  char lowest = 0;
  for (uint i = 0; i < (this) ->  length (); i++) {
    if (((this) ->  at (i)=='%') && i + 1 < (this) ->  length ()) {
      char dig = (this) ->  at (i + 1) .  operator char ();
      if (dig >= '0' && dig <= '9') {
        if (!lowest || dig < lowest) {
          lowest = dig;
          pos = i;
          len = 2;
        }
      }
    }
  }
  return lowest != 0;
}
/*!
  Safely builds a formatted string from a format string and an
  arbitrary list of arguments.  The format string supports all
  the escape sequences of printf() in the standard C library.
  The %s escape sequence expects a utf8() encoded string. 
  The format string \e cformat is expected to be in latin1. If you need a unicode
  format string, use QString::arg() instead. For typesafe
  string building, with full Unicode support, you can use QTextOStream
  like this:
  \code
    QString str;
    QString s = ...;
    int x = ...;
    QTextOStream(&str) << s << " : " << x;
  \endcode
  For \link QObject::tr() translations,\endlink especially if the
  strings contains more than one escape sequence, you should consider
  using the arg() function instead.  This allows the order of the
  replacements to be controlled by the translator, and has Unicode
  support.
  \sa arg()
*/

QString &QString::sprintf(const char *cformat,... )
{
  va_list ap;
  va_start(ap,cformat);
  if (!cformat || !( *cformat)) {
// Qt 1.x compat
    ( *(this)) =  fromLatin1 ("");
    return  *(this);
  }
  class ::QString format =  fromLatin1 (cformat);
  static class QRegExp *escape = 0;
  if (!escape) {
    escape = (new QRegExp (("%#?0?-? ?\\+?'?[0-9*]*\\.?[0-9*]*h?l?L?q?Z?")));
  }
  class ::QString result;
  uint last = 0;
  int len = 0;
  int pos;
  while(1){
    pos = escape ->  match (cformat,last,&len);
// Non-escaped text
    if (pos > ((int )last)) {
      result += format .  mid (last,(pos - last));
    }
    if (pos < 0) {
// The rest
      if (last < format .  length ()) {
        result += format .  mid (last);
      }
      break; 
    }
    last = (pos + len + 1);
// Escape
    class ::QString f = format .  mid (pos,len);
    uint width;
    uint decimals;
    int params = 0;
    int wpos = f .  find ('*');
    if (wpos >= 0) {
      params++;
      width = (va_arg(ap,int ));
      if (f .  find ('*',(wpos + 1)) >= 0) {
        decimals = (va_arg(ap,int ));
        params++;
      }
      else {
        decimals = 0;
      }
    }
    else {
      decimals = width = 0;
    }
    class ::QString replacement;
    if (((format[(pos + len)] .  operator QChar ())=='s') || ((format[(pos + len)] .  operator QChar ())=='S') || ((format[(pos + len)] .  operator QChar ())=='c')) {
      bool rightjust = f .  find ('-') < 0;
// Yes, %-5s really means left adjust in sprintf
      if (wpos < 0) {
        class QRegExp num(("[0-9]+"));
        class QRegExp dot(("\\."));
        int nlen;
        int p = num .  match (f .  data (),0,&nlen);
        int q = dot .  match (f .  data (),0);
        if (q < 0 || p < q && p >= 0) {
          width = (f .  mid (p,nlen) .  toInt ());
        }
        if (q >= 0) {
          p = num .  match (f .  data (),q);
// "decimals" is used to specify string truncation
          if (p >= 0) {
            decimals = (f .  mid (p,nlen) .  toInt ());
          }
        }
      }
      if (((format[(pos + len)] .  operator QChar ())=='s')) {
#ifndef QT_NO_TEXTCODEC
        class ::QString s =  fromUtf8 ((va_arg(ap,char *)));
#else
#endif
        if (decimals <= 0) {
          replacement = s;
        }
        else {
          replacement = s .  left (decimals);
        }
      }
      else {
        int ch = va_arg(ap,int );
        replacement = QChar::QChar(((ushort )ch));
      }
      if (replacement .  length () < width) {
        replacement = (rightjust?replacement .  rightJustify (width) : replacement .  leftJustify (width));
      }
    }
    else {
      if (((format[(pos + len)] .  operator QChar ())=='%')) {
        replacement = '%';
      }
      else {
        if (((format[(pos + len)] .  operator QChar ())=='n')) {
          int *n = va_arg(ap,int *);
           *n = (result .  length ());
        }
        else {
          char in[64];
          char out[330] = "";
          strncpy(in,f .  latin1 (),63);
          char fch = format[(pos + len)] .  latin1 ();
          in[f .  length ()] = fch;
          switch(fch){
            case 'd':
{
            }
            case 'i':
{
            }
            case 'o':
{
            }
            case 'u':
{
            }
            case 'x':
{
            }
            case 'X':
{
{
                int value = va_arg(ap,int );
                switch(params){
                  case 0:
{
                    ::sprintf(out,in,value);
                    break; 
                  }
                  case 1:
{
                    ::sprintf(out,in,width,value);
                    break; 
                  }
                  case 2:
{
                    ::sprintf(out,in,width,decimals,value);
                    break; 
                  }
                }
              }
              break; 
            }
            case 'e':
{
            }
            case 'E':
{
            }
            case 'f':
{
            }
            case 'g':
{
            }
            case 'G':
{
{
                double value = va_arg(ap,double );
                switch(params){
                  case 0:
{
                    ::sprintf(out,in,value);
                    break; 
                  }
                  case 1:
{
                    ::sprintf(out,in,width,value);
                    break; 
                  }
                  case 2:
{
                    ::sprintf(out,in,width,decimals,value);
                    break; 
                  }
                }
              }
              break; 
            }
            case 'p':
{
{
                void *value = va_arg(ap,void *);
                switch(params){
                  case 0:
{
                    ::sprintf(out,in,value);
                    break; 
                  }
                  case 1:
{
                    ::sprintf(out,in,width,value);
                    break; 
                  }
                  case 2:
{
                    ::sprintf(out,in,width,decimals,value);
                    break; 
                  }
                }
              }
              break; 
            }
          }
          replacement =  fromLatin1 (out);
        }
      }
    }
    result += replacement;
  }
  ( *(this)) = result;
  va_end(ap);
  return  *(this);
}
/*!
  Fills the string with \a len characters of value \a c.
  If \a len is negative, the current string length is used.
*/

void QString::fill(class QChar c,int len)
{
  if (len < 0) {
    len = ((this) ->  length ());
  }
  if (len == 0) {
    ( *(this)) = "";
  }
  else {
    (this) ->  deref ();
    class QChar *nd = (class QChar *)(new char [sizeof(QChar ) * len]);
    (this) -> d = (new QStringData (nd,len,len));
    while((len--))
       *(nd++) = c;
  }
}
/*!
  \fn QString QString::copy() const
  \obsolete
  Returns a deep copy of this string.
  Doing this is redundant in Qt 2.x, since QString is implicitly
  shared, and so will automatically be deeply copied as necessary.
*/
/*!
  Finds the first occurrence of the character \a c, starting at
  position \a index. If \a index is -1, the search starts at the
  last character; if -2, at the next to last character; etc.
  The search is case sensitive if \a cs is TRUE, or case insensitive
  if \a cs is FALSE.
  Returns the position of \a c, or -1 if \a c could not be found.
*/

int QString::find(class QChar c,int index,bool cs) const
{
  if (index < 0) {
    index += (this) ->  length ();
  }
// index outside string
  if (((uint )index) >= (this) ->  length ()) {
    return -1;
  }
  register const class QChar *uc;
  uc = (this) ->  unicode () + index;
  int n = ((this) ->  length () - index);
  if (cs) {
    while((n--) && (( *uc)!=(c)))
      uc++;
  }
  else {
    c = c .  lower ();
    while((n--) && (uc ->  lower ()!=(c)))
      uc++;
  }
  if (((uint )(uc - (this) ->  unicode ())) >= (this) ->  length ()) {
    return -1;
  }
  return (int )(uc - (this) ->  unicode ());
}
/*!
  Finds the first occurrence of the string \a str, starting at position
  \a index. If \a index is -1, the search starts at the last character;
  if -2, at the next to last character; etc.
  The search is case sensitive if \a cs is TRUE, or case insensitive if
  \a cs is FALSE.
  Returns the position of \a str, or -1 if \a str could not be found.
*/

int QString::find(const class ::QString &str,int index,bool cs) const
{
/*
      We use some weird hashing for efficiency's sake.  Instead of
      comparing strings, we compare the hash value of str with that of
      a part of this QString.  Only if that matches, we call ucstrncmp
      or ucstrnicmp.
      The hash value of a string is the sum of the cells of its
      QChars.
    */
  if (index < 0) {
    index += (this) ->  length ();
  }
  int lstr = (str .  length ());
  int lthis = ((this) ->  length () - index);
  if (((uint )lthis) > (this) ->  length ()) {
    return -1;
  }
  int delta = lthis - lstr;
  if (delta < 0) {
    return -1;
  }
  const class QChar *uthis = (this) ->  unicode () + index;
  const class QChar *ustr = str .  unicode ();
  uint hthis = 0;
  uint hstr = 0;
  int i;
  if (cs) {
    for (i = 0; i < lstr; i++) {
      hthis += (uthis[i] .  cell ());
      hstr += (ustr[i] .  cell ());
    }
    i = 0;
    while(TRUE){
      if (hthis == hstr && ucstrncmp(uthis + i,ustr,lstr) == 0) {
        return index + i;
      }
      if (i == delta) {
        return -1;
      }
      hthis += (uthis[i + lstr] .  cell ());
      hthis -= (uthis[i] .  cell ());
      i++;
    }
  }
  else {
    for (i = 0; i < lstr; i++) {
      hthis += (uthis[i] .  lower () .  cell ());
      hstr += (ustr[i] .  lower () .  cell ());
    }
    i = 0;
    while(TRUE){
      if (hthis == hstr && ucstrnicmp(uthis + i,ustr,lstr) == 0) {
        return index + i;
      }
      if (i == delta) {
        return -1;
      }
      hthis += (uthis[i + lstr] .  lower () .  cell ());
      hthis -= (uthis[i] .  lower () .  cell ());
      i++;
    }
  }
#if defined(Q_SPURIOUS_NON_VOID_WARNING)
#endif
}
/*!
  \fn int QString::findRev( const char* str, int index ) const
  Equivalent to findRev(QString(str), index).
*/
/*!
  \fn int QString::find( const char* str, int index ) const
  Equivalent to find(QString(str), index).
*/
/*!
  Finds the first occurrence of the character \a c, starting at
  position \a index and searching backwards. If \a index is -1,
  the search starts at the last character; if -2, at the next to
  last character; etc.
  The search is case sensitive if \a cs is TRUE, or case insensitive if \a
  cs is FALSE.
  Returns the position of \a c, or -1 if \a c could not be found.
*/

int QString::findRev(class QChar c,int index,bool cs) const
{
  class ::QString t((c));
  return (this) ->  findRev (t,index,cs);
}
/*!
  Finds the first occurrence of the string \a str, starting at
  position \a index and searching backwards. If \a index is -1,
  the search starts at the last character; -2, at the next to last
  character; etc.
  The search is case sensitive if \a cs is TRUE, or case insensitive if \e
  cs is FALSE.
  Returns the position of \a str, or -1 if \a str could not be found.
*/

int QString::findRev(const class ::QString &str,int index,bool cs) const
{
/*
      See QString::find() for explanations.
    */
  int lthis = ((this) ->  length ());
  if (index < 0) {
    index += lthis;
  }
  int lstr = (str .  length ());
  int delta = lthis - lstr;
  if (index < 0 || index > lthis || delta < 0) {
    return -1;
  }
  if (index > delta) {
    index = delta;
  }
  const class QChar *uthis = (this) ->  unicode ();
  const class QChar *ustr = str .  unicode ();
  uint hthis = 0;
  uint hstr = 0;
  int i;
  if (cs) {
    for (i = 0; i < lstr; i++) {
      hthis += (uthis[index + i] .  cell ());
      hstr += (ustr[i] .  cell ());
    }
    i = index;
    while(TRUE){
      if (hthis == hstr && ucstrncmp(uthis + i,ustr,lstr) == 0) {
        return i;
      }
      if (i == 0) {
        return -1;
      }
      i--;
      hthis -= (uthis[i + lstr] .  cell ());
      hthis += (uthis[i] .  cell ());
    }
  }
  else {
    for (i = 0; i < lstr; i++) {
      hthis += (uthis[index + i] .  lower () .  cell ());
      hstr += (ustr[i] .  lower () .  cell ());
    }
    i = index;
    while(TRUE){
      if (hthis == hstr && ucstrnicmp(uthis + i,ustr,lstr) == 0) {
        return i;
      }
      if (i == 0) {
        return -1;
      }
      i--;
      hthis -= (uthis[i + lstr] .  lower () .  cell ());
      hthis += (uthis[i] .  lower () .  cell ());
    }
  }
#if defined(Q_SPURIOUS_NON_VOID_WARNING)
#endif
}
/*!
  Returns the number of times the character \a c occurs in the string.
  The match is case sensitive if \a cs is TRUE, or case insensitive if \a cs
  is FALSE.
*/

int QString::contains(class QChar c,bool cs) const
{
  int count = 0;
  const class QChar *uc = (this) ->  unicode ();
  if (!uc) {
    return 0;
  }
  int n = ((this) ->  length ());
// case sensitive
  if (cs) {
    while((n--))
      if ((( *(uc++))==(c))) {
        count++;
      }
// case insensitive
  }
  else {
    c = c .  lower ();
    while((n--)){
      if ((uc ->  lower ()==(c))) {
        count++;
      }
      uc++;
    }
  }
  return count;
}
/*!
  \overload
*/

int QString::contains(const char *str,bool cs) const
{
  return (this) ->  contains (::QString::QString(str),cs);
}
/*!
  \overload int QString::contains (char c, bool cs) const
*/
/*!
  \overload int QString::find (char c, int index, bool cs) const
*/
/*!
  \overload int QString::findRev (char c, int index, bool cs) const
*/
/*!
  Returns the number of times \a str occurs in the string.
  The match is case sensitive if \a cs is TRUE, or case insensitive if \e
  cs is FALSE.
  This function counts overlapping substrings, for example, "banana"
  contains two occurrences of "ana".
  \sa findRev()
*/

int QString::contains(const class ::QString &str,bool cs) const
{
  int count = 0;
  const class QChar *uc = (this) ->  unicode ();
  if (!uc) {
    return 0;
  }
  int len = (str .  length ());
  int n = ((this) ->  length ());
// counts overlapping strings
  while((n--)){
// ### Doesn't account for length of this - searches over "end"
    if (cs) {
      if (ucstrncmp(uc,str .  unicode (),len) == 0) {
        count++;
      }
    }
    else {
      if (ucstrnicmp(uc,str .  unicode (),len) == 0) {
        count++;
      }
    }
    uc++;
  }
  return count;
}
/*!
  Returns a substring that contains the \a len leftmost characters
  of the string.
  The whole string is returned if \a len exceeds the length of the
  string.
  Example:
  \code
    QString s = "Pineapple";
    QString t = s.left( 4 );	// t == "Pine"
  \endcode
  \sa right(), mid(), isEmpty()
*/

QString QString::left(uint len) const
{
  if ((this) ->  isEmpty ()) {
    return QString();
// ## just for 1.x compat:
  }
  else {
    if (len == 0) {
      return  fromLatin1 ("");
    }
    else {
      if (len > (this) ->  length ()) {
        return ( *(this));
      }
      else {
        class ::QString s(len,TRUE);
        memcpy((s . d -> QStringData::unicode),((this) -> d -> QStringData::unicode),len * sizeof(class QChar ));
        s . d -> QStringData::len = len;
        return (s);
      }
    }
  }
}
/*!
  Returns a substring that contains the \a len rightmost characters
  of the string.
  The whole string is returned if \a len exceeds the length of the
  string.
  Example:
  \code
    QString s = "Pineapple";
    QString t = s.right( 5 );	// t == "apple"
  \endcode
  \sa left(), mid(), isEmpty()
*/

QString QString::right(uint len) const
{
  if ((this) ->  isEmpty ()) {
    return QString();
// ## just for 1.x compat:
  }
  else {
    if (len == 0) {
      return  fromLatin1 ("");
    }
    else {
      uint l = (this) ->  length ();
      if (len > l) {
        len = l;
      }
      class ::QString s(len,TRUE);
      memcpy((s . d -> QStringData::unicode),((this) -> d -> QStringData::unicode + (l - len)),len * sizeof(class QChar ));
      s . d -> QStringData::len = len;
      return (s);
    }
  }
}
/*!
  Returns a substring that contains the \a len characters of this
  string, starting at position \a index.
  Returns a null string if the string is empty or \a index is out
  of range.  Returns the whole string from \a index if \a index+len exceeds
  the length of the string.
  Example:
  \code
    QString s = "Five pineapples";
    QString t = s.mid( 5, 4 );			// t == "pine"
  \endcode
  \sa left(), right()
*/

QString QString::mid(uint index,uint len) const
{
  uint slen = (this) ->  length ();
  if ((this) ->  isEmpty () || index >= slen) {
    return QString();
// ## just for 1.x compat:
  }
  else {
    if (len == 0) {
      return  fromLatin1 ("");
    }
    else {
      if (len > slen - index) {
        len = slen - index;
      }
      if (index == 0 && len == (this) ->  length ()) {
        return ( *(this));
      }
      register const class QChar *p = (this) ->  unicode () + index;
      class ::QString s(len,TRUE);
      memcpy((s . d -> QStringData::unicode),p,len * sizeof(class QChar ));
      s . d -> QStringData::len = len;
      return (s);
    }
  }
}
/*!
  Returns a string of length \a width that contains this
  string and padded by the \a fill character.
  If the length of the string exceeds \a width and \a truncate is FALSE,
  then the returned string is a copy of the string.
  If the length of the string exceeds \a width and \a truncate is TRUE,
  then the returned string is a left(\a width).
  Example:
  \code
    QString s("apple");
    QString t = s.leftJustify(8, '.');		// t == "apple..."
  \endcode
  \sa rightJustify()
*/

QString QString::leftJustify(uint width,class QChar fill,bool truncate) const
{
  class ::QString result;
  int len = ((this) ->  length ());
  int padlen = (width - len);
  if (padlen > 0) {
    result .  setLength ((len + padlen));
    if (len) {
      memcpy((result . d -> QStringData::unicode),((this) ->  unicode ()),sizeof(class QChar ) * len);
    }
    class QChar *uc = result . d -> QStringData::unicode + len;
    while((padlen--))
       *(uc++) = fill;
  }
  else {
    if (truncate) {
      result = (this) ->  left (width);
    }
    else {
      result =  *(this);
    }
  }
  return (result);
}
/*!
  Returns a string of length \a width that contains pad
  characters followed by the string.
  If the length of the string exceeds \a width and \a truncate is FALSE,
  then the returned string is a copy of the string.
  If the length of the string exceeds \a width and \a truncate is TRUE,
  then the returned string is a left(\a width).
  Example:
  \code
    QString s("pie");
    QString t = s.rightJustify(8, '.');		// t == ".....pie"
  \endcode
  \sa leftJustify()
*/

QString QString::rightJustify(uint width,class QChar fill,bool truncate) const
{
  class ::QString result;
  int len = ((this) ->  length ());
  int padlen = (width - len);
  if (padlen > 0) {
    result .  setLength ((len + padlen));
    class QChar *uc = result . d -> QStringData::unicode;
    while((padlen--))
       *(uc++) = fill;
    if (len) {
      memcpy(uc,((this) ->  unicode ()),sizeof(class QChar ) * len);
    }
  }
  else {
    if (truncate) {
      result = (this) ->  left (width);
    }
    else {
      result =  *(this);
    }
  }
  return (result);
}
/*!
  Returns a new string that is the string converted to lower case.
  Example:
  \code
    QString s("TeX");
    QString t = s.lower();	// t == "tex"
  \endcode
  \sa upper()
*/

QString QString::lower() const
{
  class ::QString s( *(this));
  int l = ((this) ->  length ());
  if (l) {
// could do this only when we find a change
    s .  real_detach ();
    register class QChar *p = s . d -> QStringData::unicode;
    if (p) {
      while((l--)){
         *p = p ->  lower ();
        p++;
      }
    }
  }
  return (s);
}
/*!
  Returns a new string that is the string converted to upper case.
  Example:
  \code
    QString s("TeX");
    QString t = s.upper();			// t == "TEX"
  \endcode
  \sa lower()
*/

QString QString::upper() const
{
  class ::QString s( *(this));
  int l = ((this) ->  length ());
  if (l) {
// could do this only when we find a change
    s .  real_detach ();
    register class QChar *p = s . d -> QStringData::unicode;
    if (p) {
      while((l--)){
         *p = p ->  upper ();
        p++;
      }
    }
  }
  return (s);
}
/*!
  Returns a new string that has white space removed from the start and the end.
  White space means any character for which QChar::isSpace() returns
  TRUE. This includes ASCII characters 9 (TAB), 10 (LF), 11 (VT), 12
  (FF), 13 (CR), and 32 (Space).
  Example:
  \code
    QString s = " space ";
    QString t = s.stripWhiteSpace();		// t == "space"
  \endcode
  \sa simplifyWhiteSpace()
*/

QString QString::stripWhiteSpace() const
{
// nothing to do
  if ((this) ->  isEmpty ()) {
    return ( *(this));
  }
  if (!(this) ->  at (0) .  isSpace () && !(this) ->  at ((this) ->  length () - 1) .  isSpace ()) {
    return ( *(this));
  }
  register const class QChar *s = (this) ->  unicode ();
  class ::QString result =  fromLatin1 ("");
  int start = 0;
  int end = ((this) ->  length () - 1);
// skip white space from start
  while(start <= end && s[start] .  isSpace ())
    start++;
// only white space
  if (start > end) {
    return (result);
  }
// skip white space from end
  while(end && s[end] .  isSpace ())
    end--;
  int l = end - start + 1;
  result .  setLength (l);
  if (l) {
    memcpy((result . d -> QStringData::unicode),(&s[start]),sizeof(class QChar ) * l);
  }
  return (result);
}
/*!
  Returns a new string that has white space removed from the start and the end,
  plus any sequence of internal white space replaced with a single space
  (ASCII 32).
  White space means any character for which QChar::isSpace() returns
  TRUE. This includes ASCII characters 9 (TAB), 10 (LF), 11 (VT), 12
  (FF), 13 (CR), and 32 (Space).
  \code
    QString s = "  lots\t of\nwhite    space ";
    QString t = s.simplifyWhiteSpace();		// t == "lots of white space"
  \endcode
  \sa stripWhiteSpace()
*/

QString QString::simplifyWhiteSpace() const
{
// nothing to do
  if ((this) ->  isEmpty ()) {
    return ( *(this));
  }
  class ::QString result;
  result .  setLength ((this) ->  length ());
  const class QChar *from = (this) ->  unicode ();
  const class QChar *fromend = from + (this) ->  length ();
  int outc = 0;
  class QChar *to = result . d -> QStringData::unicode;
  while(TRUE){
    while(from != fromend && from ->  isSpace ())
      from++;
    while(from != fromend && !from ->  isSpace ())
      to[outc++] =  *(from++);
    if (from != fromend) {
      to[outc++] = (' ');
    }
    else {
      break; 
    }
  }
  if (outc > 0 && ((to[outc - 1])==' ')) {
    outc--;
  }
  result .  truncate (outc);
  return (result);
}
/*!
  Insert \a s into the string before position \a index.
  If \a index is beyond the end of the string, the string is extended with
  spaces (ASCII 32) to length \a index and \a s is then appended.
  \code
    QString s = "I like fish";
    s.insert( 2, "don't ");	// s == "I don't like fish"
    s = "x";
    s.insert( 3, "yz" );	// s == "x  yz"
  \endcode
*/

QString &QString::insert(uint index,const class ::QString &s)
{
// the sub function takes care of &s == this case.
  return (this) ->  insert (index,s .  unicode (),s .  length ());
}
/*!
  Insert \a len units of QChar data from \a s into the string before
  position \a index.
*/

QString &QString::insert(uint index,const class QChar *s,uint len)
{
  if (len == 0) {
    return  *(this);
  }
  uint olen = (this) ->  length ();
  int nlen = (olen + len);
// ### pointer subtraction, cast down to int
  int df = (int )(s - ((this) -> d -> QStringData::unicode));
  if (df >= 0 && ((uint )df) < ((this) -> d -> QStringData::maxl)) {
// Part of me - take a copy.
    class QChar *tmp = (class QChar *)(new char [sizeof(QChar ) * len]);
    memcpy(tmp,s,len * sizeof(class QChar ));
    (this) ->  insert (index,tmp,len);
    delete []((char *)tmp);
    return  *(this);
  }
// insert after end of string
  if (index >= olen) {
    (this) ->  setLength (len + index);
    int n = (index - olen);
    class QChar *uc = (this) -> d -> QStringData::unicode + olen;
    while((n--))
       *(uc++) = (' ');
    memcpy(((this) -> d -> QStringData::unicode + index),s,sizeof(class QChar ) * len);
// normal insert
  }
  else {
    (this) ->  setLength (nlen);
    memmove(((this) -> d -> QStringData::unicode + index + len),((this) ->  unicode () + index),sizeof(class QChar ) * (olen - index));
    memcpy(((this) -> d -> QStringData::unicode + index),s,sizeof(class QChar ) * len);
  }
  return  *(this);
}
/*!
  Insert \a c into the string at (before) position \a index and returns
  a reference to the string.
  If \a index is beyond the end of the string, the string is extended with
  spaces (ASCII 32) to length \a index and \a c is then appended.
  Example:
  \code
    QString s = "Ys";
    s.insert( 1, 'e' );		// s == "Yes"
    s.insert( 3, '!');		// s == "Yes!"
  \endcode
  \sa remove(), replace()
*/
// insert char

QString &QString::insert(uint index,class QChar c)
{
  class ::QString s((c));
  return (this) ->  insert (index,s);
}
/*!
  \overload QString& QString::insert( uint index, char c )
*/
/*!
  \fn QString &QString::prepend( const QString &s )
  Prepend \a s to the string. Equivalent to insert(0,s).
  \sa insert()
*/
/*!
  \fn QString& QString::prepend( char ch )
  Prepends \a ch to the string and returns a reference to the result.
  \sa insert()
 */
/*!
  \fn QString& QString::prepend( QChar ch )
  Prepends \a ch to the string and returns a reference to the result.
  \sa insert()
 */
/*!
  Removes \a len characters starting at position \a index from the
  string and returns a reference to the string.
  If \a index is too big, nothing happens.  If \a index is valid, but
  \a len is too large, the rest of the string is removed.
  \code
    QString s = "Montreal";
    s.remove( 1, 4 );
    // s == "Meal"
  \endcode
  \sa insert(), replace()
*/

QString &QString::remove(uint index,uint len)
{
  uint olen = (this) ->  length ();
// range problems
  if (index + len >= olen) {
// index ok
    if (index < olen) {
      (this) ->  setLength (index);
    }
  }
  else {
    if (len != 0) {
      (this) ->  real_detach ();
      memmove(((this) -> d -> QStringData::unicode + index),((this) -> d -> QStringData::unicode + index + len),sizeof(class QChar ) * (olen - index - len));
      (this) ->  setLength (olen - len);
    }
  }
  return  *(this);
}
/*!
  Replaces \a len characters starting at position \a index from the
  string with \a s, and returns a reference to the string.
  If \a index is too big, nothing is deleted and \a s is inserted at the
  end of the string.  If \a index is valid, but \a len is too large, \e
  str replaces the rest of the string.
  \code
    QString s = "Say yes!";
    s.replace( 4, 3, "NO" );			// s == "Say NO!"
  \endcode
  \sa insert(), remove()
*/

QString &QString::replace(uint index,uint len,const class ::QString &s)
{
  return (this) ->  replace (index,len,s .  unicode (),s .  length ());
}
/*!
  Replaces \a len characters starting at position \a index by
  \a slen units ot QChar data from \a s, and returns a reference to the string.
  \sa insert(), remove()
*/

QString &QString::replace(uint index,uint len,const class QChar *s,uint slen)
{
  if (len == slen && index + len <= (this) ->  length ()) {
// Optimized common case: replace without size change
    (this) ->  real_detach ();
    memcpy(((this) -> d -> QStringData::unicode + index),s,len * sizeof(class QChar ));
  }
  else {
// ### pointer subtraction, cast down to int
    int df = (int )(s - ((this) -> d -> QStringData::unicode));
    if (df >= 0 && ((uint )df) < ((this) -> d -> QStringData::maxl)) {
// Part of me - take a copy.
      class QChar *tmp = (class QChar *)(new char [sizeof(QChar ) * slen]);
      memcpy(tmp,s,slen * sizeof(class QChar ));
      (this) ->  replace (index,len,tmp,slen);
      delete []((char *)tmp);
      return  *(this);
    }
    (this) ->  remove (index,len);
    (this) ->  insert (index,s,slen);
  }
  return  *(this);
}
/*!
  Finds the first occurrence of the regular expression \a rx, starting at
  position \a index. If \a index is -1, the search starts at the last
  character; if -2, at the next to last character; etc.
  Returns the position of the next match, or -1 if \a rx was not found.
  \sa findRev() replace() contains()
*/

int QString::find(const class QRegExp &rx,int index) const
{
  if (index < 0) {
    index += (this) ->  length ();
  }
  return rx .  match ((this) ->  data (),index);
}
/*!
  Finds the first occurrence of the regular expression \a rx, starting at
  position \a index and searching backwards. If \a index is -1, the
  search starts at the last character; if -2, at the next to last
  character; etc.
  Returns the position of the next match (backwards), or -1 if \a rx was not
  found.
  \sa find()
*/

int QString::findRev(const class QRegExp &rx,int index) const
{
// neg index ==> start from end
  if (index < 0) {
    index += (this) ->  length ();
  }
// bad index
  if (((uint )index) > (this) ->  length ()) {
    return -1;
  }
  while(index >= 0){
    if (rx .  match ((this) ->  data (),index) == index) {
      return index;
    }
    index--;
  }
  return -1;
}
/*!
  Counts the number of overlapping occurrences of \a rx in the string.
  Example:
  \code
    QString s = "banana and panama";
    QRegExp r = QRegExp("a[nm]a", TRUE, FALSE);
    s.contains( r );				// 4 matches
  \endcode
  \sa find() findRev()
*/

int QString::contains(const class QRegExp &rx) const
{
  if ((this) ->  isEmpty ()) {
    return rx .  match ((this) ->  data ()) < 0?0 : 1;
  }
  int count = 0;
  int index = -1;
  int len = ((this) ->  length ());
// count overlapping matches
  while(index < len - 1){
    index = rx .  match ((this) ->  data (),(index + 1));
    if (index < 0) {
      break; 
    }
    count++;
  }
  return count;
}
/*!
  Replaces every occurrence of \a rx in the string with \a str.
  Returns a reference to the string.
  Examples:
  \code
    QString s = "banana";
    s.replace( QRegExp("a.*a"), "" );		// becomes "b"
    QString s = "banana";
    s.replace( QRegExp("^[bn]a"), " " );	// becomes " nana"
    QString s = "banana";
    s.replace( QRegExp("^[bn]a"), "" );		// NOTE! becomes ""
  \endcode
  \sa find() findRev()
*/

QString &QString::replace(const class QRegExp &rx,const class ::QString &str)
{
  if ((this) ->  isEmpty ()) {
    return  *(this);
  }
  int index = 0;
  int slen = (str .  length ());
  int len;
  while(index < ((int )((this) ->  length ()))){
    index = rx .  match ((this) ->  data (),index,&len,FALSE);
    if (index >= 0) {
      (this) ->  replace (index,len,str);
      index += slen;
      if (!len) {
// Avoid infinite loop on 0-length matches, e.g. [a-z]*
        break; 
      }
    }
    else {
      break; 
    }
  }
  return  *(this);
}

static bool ok_in_base(class QChar c,int base)
{
  if (base <= 10) {
    return c .  isDigit () && c .  digitValue () < base;
  }
  else {
    return c .  isDigit () || ((c)>='a') && ((c)<((char )('a' + base - 10))) || ((c)>='A') && ((c)<((char )('A' + base - 10)));
  }
}
/*!
  Returns the string converted to a <code>long</code> value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all, or if
  it has trailing garbage.
*/

long QString::toLong(bool *ok,int base) const
{
  const class QChar *p = (this) ->  unicode ();
  long val = 0;
  int l = ((this) ->  length ());
  const long max_mult = (2147483647 / base);
  bool is_ok = FALSE;
  int neg = 0;
  if (!p) {
    goto bye;
  }
// skip leading space
  while(l && p ->  isSpace ())
    (l-- , p++);
  if (l && (( *p)=='-')) {
    l--;
    p++;
    neg = 1;
  }
  else {
    if ((( *p)=='+')) {
      l--;
      p++;
    }
  }
// NOTE: toULong() code is similar
  if (!l || !ok_in_base( *p,base)) {
    goto bye;
  }
  while(l && ok_in_base( *p,base)){
    l--;
    int dv;
    if (p ->  isDigit ()) {
      dv = p ->  digitValue ();
    }
    else {
      if ((( *p)>='a') && (( *p)<='z')) {
        dv = (( *p) .  operator char ()) - 'a' + 10;
      }
      else {
        dv = (( *p) .  operator char ()) - 'A' + 10;
      }
    }
    if (val > max_mult || val == max_mult && dv > 2147483647 % base + neg) {
      goto bye;
    }
    val = base * val + dv;
    p++;
  }
  if (neg) {
    val = -val;
  }
// skip trailing space
  while(l && p ->  isSpace ())
    (l-- , p++);
  if (!l) {
    is_ok = TRUE;
  }
  bye:
  if (ok) {
     *ok = is_ok;
  }
  return is_ok?val : 0;
}
/*!
  Returns the string converted to an <code>unsigned long</code>
  value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all,
  or if it has trailing garbage.
*/

ulong QString::toULong(bool *ok,int base) const
{
  const class QChar *p = (this) ->  unicode ();
  ulong val = 0;
  int l = ((this) ->  length ());
// UINT_MAX/10, rounded down
  const ulong max_mult = 429496729;
  bool is_ok = FALSE;
  if (!p) {
    goto bye;
  }
// skip leading space
  while(l && p ->  isSpace ())
    (l-- , p++);
  if ((( *p)=='+')) {
    (l-- , p++);
  }
// NOTE: toLong() code is similar
  if (!l || !ok_in_base( *p,base)) {
    goto bye;
  }
  while(l && ok_in_base( *p,base)){
    l--;
    uint dv;
    if (p ->  isDigit ()) {
      dv = (p ->  digitValue ());
    }
    else {
      if ((( *p)>='a') && (( *p)<='z')) {
        dv = ((( *p) .  operator char ()) - 'a' + 10);
      }
      else {
        dv = ((( *p) .  operator char ()) - 'A' + 10);
      }
    }
    if (val > max_mult || val == max_mult && dv > (2147483647 * 2U + 1U) % base) {
      goto bye;
    }
    val = base * val + dv;
    p++;
  }
// skip trailing space
  while(l && p ->  isSpace ())
    (l-- , p++);
  if (!l) {
    is_ok = TRUE;
  }
  bye:
  if (ok) {
     *ok = is_ok;
  }
  return is_ok?val : 0;
}
/*!
  Returns the string converted to a <code>short</code> value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all, or if
  it has trailing garbage.
*/

short QString::toShort(bool *ok,int base) const
{
  long v = (this) ->  toLong (ok,base);
  if (ok &&  *ok && (v < (-32768) || v > 32767)) {
     *ok = FALSE;
    v = 0;
  }
  return (short )v;
}
/*!
  Returns the string converted to an <code>unsigned short</code> value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all, or if
  it has trailing garbage.
*/

ushort QString::toUShort(bool *ok,int base) const
{
  ulong v = (this) ->  toULong (ok,base);
  if (ok &&  *ok && v > 65535) {
     *ok = FALSE;
    v = 0;
  }
  return (ushort )v;
}
/*!
  Returns the string converted to a <code>int</code> value.
  \code
  QString str("FF");
  bool ok;
  int hex = str.toInt( &ok, 16 ); // will return 255, and ok set to TRUE
  int dec = str.toInt( &ok, 10 ); // will return 0, and ok set to FALSE
  \endcode
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all,
  or if it has trailing garbage.
*/

int QString::toInt(bool *ok,int base) const
{
  return (int )((this) ->  toLong (ok,base));
}
/*!
  Returns the string converted to an <code>unsigned int</code> value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all,
  or if it has trailing garbage.
*/

uint QString::toUInt(bool *ok,int base) const
{
  return (uint )((this) ->  toULong (ok,base));
}
/*!
  Returns the string converted to a <code>double</code> value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no conceivable
  errors, and FALSE if the string is not a number at all, or if it has
  trailing garbage.
*/

double QString::toDouble(bool *ok) const
{
  char *end;
  class QCString a((this) ->  latin1 ());
// Just latin1() is not sufficient, since U0131 would look like '1'.
  for (uint i = 0; i < (this) -> d -> QStringData::len; i++) 
    if (((this) -> d -> QStringData::unicode[i] .  row ())) {
      a[((int )i)] = 'z';
    }
  double val = strtod(((a .  data ())?(a .  data ()) : ""),&end);
  if (ok) {
     *ok = (a .  operator const char * ()) && ( *(a .  operator const char * ())) && (end == 0 || ( *end) == '\0');
  }
  return val;
}
/*!
  Returns the string converted to a <code>float</code> value.
  If \a ok is non-null, \a *ok is set to TRUE if there are no
  conceivable errors, and FALSE if the string is not a number at all,
  or if it has trailing garbage.
*/

float QString::toFloat(bool *ok) const
{
  return (float )((this) ->  toDouble (ok));
}
/*!
  Sets the string to the printed value of \a n and returns a
  reference to the string.
  The value is converted to \a base notation (default is decimal).
  The base must be a value from 2 to 36.
*/

QString &QString::setNum(long n,int base)
{
#if defined(CHECK_RANGE)
  if (base < 2 || base > 36) {
    qWarning("QString::setNum: Invalid base %d",base);
    base = 10;
  }
#endif
  char charbuf[65UL * 2UL];
  class QChar *buf = (class QChar *)charbuf;
  class QChar *p = &buf[64];
  int len = 0;
  bool neg;
  if (n < 0) {
    neg = TRUE;
    if (n == (-2147483647 - 1)) {
// Cannot always negate this special case
      class ::QString s1;
      class ::QString s2;
      s1 .  setNum ((n / base));
      s2 .  setNum ((-(n + base) % base));
      ( *(this)) = s1+s2;
      return  *(this);
    }
    n = -n;
  }
  else {
    neg = FALSE;
  }
  do {
     *(--p) = ("0123456789abcdefghijklmnopqrstuvwxyz"[(int )(n % base)]);
    n /= base;
    len++;
  }while (n);
  if (neg) {
     *(--p) = ('-');
    len++;
  }
  return (this) ->  setUnicode (p,len);
}
/*!
  Sets the string to the printed unsigned value of \a n and
  returns a reference to the string.
  The value is converted to \a base notation (default is decimal).
  The base must be a value from 2 to 36.
*/

QString &QString::setNum(ulong n,int base)
{
#if defined(CHECK_RANGE)
  if (base < 2 || base > 36) {
    qWarning("QString::setNum: Invalid base %d",base);
    base = 10;
  }
#endif
  char charbuf[65UL * 2UL];
  class QChar *buf = (class QChar *)charbuf;
  class QChar *p = &buf[64];
  int len = 0;
  do {
     *(--p) = ("0123456789abcdefghijklmnopqrstuvwxyz"[(int )(n % base)]);
    n /= base;
    len++;
  }while (n);
  return (this) ->  setUnicode (p,len);
}
/*!
  \fn QString &QString::setNum( int n, int base )
  Sets the string to the printed value of \a n and returns a reference
  to the string.
*/
/*!
  \fn QString &QString::setNum( uint n, int base )
  Sets the string to the printed unsigned value of \a n and returns a
  reference to the string.
*/
/*!
  \fn QString &QString::setNum( short n, int base )
  Sets the string to the printed value of \a n and returns a reference
  to the string.
*/
/*!
  \fn QString &QString::setNum( ushort n, int base )
  Sets the string to the printed unsigned value of \a n and returns a
  reference to the string.
*/
/*!  Sets the string to the printed value of \a n, formatted in the \a f
  format with \a prec precision, and returns a reference to the
  string.
  \a f can be 'f', 'F', 'e', 'E', 'g' or 'G', all of which have the
  same meaning as for sprintf().
*/

QString &QString::setNum(double n,char f,int prec)
{
#if defined(CHECK_RANGE)
  if (!(f == 'f' || f == 'F' || f == 'e' || f == 'E' || f == 'g' || f == 'G')) {
    qWarning("QString::setNum: Invalid format char '%c'",f);
    f = 'f';
  }
#endif
  char format[20];
// enough for 99 precision?
  char buf[120];
// generate format string
  char *fs = format;
//   "%.<prec>l<f>"
   *(fs++) = '%';
  if (prec >= 0) {
// buf big enough for precision?
    if (prec > 99) {
      prec = 99;
    }
     *(fs++) = '.';
    if (prec >= 10) {
       *(fs++) = (prec / 10 + '0');
       *(fs++) = (prec % 10 + '0');
    }
    else {
       *(fs++) = (prec + '0');
    }
  }
   *(fs++) = 'l';
   *(fs++) = f;
   *fs = '\0';
  ::sprintf(buf,format,n);
  return (this) ->  setLatin1 (buf);
}
/*!
  \overload QString &QString::setNum( float n, char f, int prec )
*/
/*!
  A convenience factory function that returns a string representation
  of the number \a n.
  \sa setNum()
 */

QString QString::number(long n,int base)
{
  class ::QString s;
  s .  setNum (n,base);
  return (s);
}
/*!
  A convenience factory function that returns a string representation
  of the number \a n.
  \sa setNum()
 */

QString QString::number(ulong n,int base)
{
  class ::QString s;
  s .  setNum (n,base);
  return (s);
}
/*!
  A convenience factory function that returns a string representation
  of the number \a n.
  \sa setNum()
 */

QString QString::number(int n,int base)
{
  class ::QString s;
  s .  setNum (n,base);
  return (s);
}
/*!
  A convenience factory function that returns a string representation
  of the number \a n.
  \sa setNum()
 */

QString QString::number(uint n,int base)
{
  class ::QString s;
  s .  setNum (n,base);
  return (s);
}
/*!
  This static function returns the printed value of \a n, formatted in the
  \a f format with \a prec precision.
  \a f can be 'f', 'F', 'e', 'E', 'g' or 'G', all of which have the
  same meaning as for sprintf().
  \sa setNum()
 */

QString QString::number(double n,char f,int prec)
{
  class ::QString s;
  s .  setNum (n,f,prec);
  return (s);
}
/*! \obsolete
  Sets the character at position \a index to \a c and expands the
  string if necessary, filling with spaces.
  This method is redundant in Qt 2.x, because operator[] will expand
  the string as necessary.
*/

void QString::setExpand(uint index,class QChar c)
{
  int spaces = (index - (this) -> d -> QStringData::len);
  (this) ->  at (index) = c;
  while(spaces-- > 0)
    (this) -> d -> QStringData::unicode[--index] = (' ');
}
/*!
  \fn const char* QString::data() const
  \obsolete
  Returns a pointer to a 0-terminated classic C string.
  In Qt 1.x, this returned a char* allowing direct manipulation of the
  string as a sequence of bytes.  In Qt 2.x where QString is a Unicode
  string, char* conversion constructs a temporary string, and hence
  direct character operations are meaningless.
*/
/*!
  \fn bool QString::operator!() const
  Returns TRUE if it is a null string, otherwise FALSE.  Thus
  you can write:
\code
  QString name = getName();
  if ( !name )
    name = "Rodney";
\endcode
  Note that if you say:
\code
  QString name = getName();
  if ( name )
    doSomethingWith(name);
\endcode
  Then this will call <tt>operator const char*()</tt>, which will do what
  you want, but rather inefficiently - you may wish to define the macro
  QT_NO_ASCII_CAST when writing code which you wish to strictly remain
  Unicode-clean.
  When you want the above semantics, use <tt>!isNull()</tt>
  or even <tt>!!</tt>:
\code
  QString name = getName();
  if ( !!name )
    doSomethingWith(name);
\endcode
*/
/*!
  \fn QString& QString::append( const QString& str )
  Appends \a str to the string and returns a reference to the result.
  Equivalent to operator+=().
 */
/*!
  \fn QString& QString::append( char ch )
  Appends \a ch to the string and returns a reference to the result.
  Equivalent to operator+=().
 */
/*!
  \fn QString& QString::append( QChar ch )
  Appends \a ch to the string and returns a reference to the result.
  Equivalent to operator+=().
 */
/*!
  Appends \a str to the string and returns a reference to the string.
*/

QString &QString::operator+=(const class ::QString &str)
{
  uint len1 = (this) ->  length ();
  uint len2 = str .  length ();
  if (len2) {
    (this) ->  setLength (len1 + len2);
    memcpy(((this) -> d -> QStringData::unicode + len1),(str .  unicode ()),sizeof(class QChar ) * len2);
// ## just for 1.x compat:
  }
  else {
    if ((this) ->  isNull () && !str .  isNull ()) {
      ( *(this)) =  fromLatin1 ("");
    }
  }
  return  *(this);
}
/*!
  Appends \a c to the string and returns a reference to the string.
*/

QString &QString::operator+=(class QChar c)
{
  (this) ->  setLength ((this) ->  length () + 1);
  (this) -> d -> QStringData::unicode[(this) ->  length () - 1] = c;
  return  *(this);
}
/*!
  Appends \a c to the string and returns a reference to the string.
*/

QString &QString::operator+=(char c)
{
  (this) ->  setLength ((this) ->  length () + 1);
  (this) -> d -> QStringData::unicode[(this) ->  length () - 1] = (c);
  return  *(this);
}
/*! \fn char QChar::latin1() const
  Returns a latin-1 copy of this character, if this character is in
  the latin-1 character set.  If not, this function returns 0.
*/
/*!
  Returns a Latin-1 representation of the string. Note that the returned
  value is undefined if the string contains non-Latin-1 characters.  If you
  want to convert strings into formats other than Unicode, see the
  QTextCodec classes.
  This function is mainly useful for boot-strapping legacy code to
  use Unicode.
  The result remains valid so long as one unmodified
  copy of the source string exists.
  \sa utf8(), local8Bit()
*/

const char *QString::latin1() const
{
  if (((this) -> d -> QStringData::ascii)) {
    if (((this) -> d -> QStringData::dirtyascii)) {
      delete []((this) -> d -> QStringData::ascii);
    }
    else {
      return ((this) -> d -> QStringData::ascii);
    }
  }
  static class QTextCodec *codec = QTextCodec:: codecForMib (106);
// we use utf8 coding also for latin1 if possible
  if (codec) {
    class QCString utf8str = codec ->  fromUnicode ( *(this));
    (this) -> d -> QStringData::ascii = (new char [(utf8str .  length () + 1)]);
    if (utf8str .  isEmpty ()) {
// make empty string
      (this) -> d -> QStringData::ascii[0] = '\0';
    }
    else 
// copy string
{
      qstrcpy((this) -> d -> QStringData::ascii,(utf8str .  data ()));
    }
  }
  else 
// fall back to latin1
{
    (this) -> d -> QStringData::ascii =  unicodeToAscii (((this) -> d -> QStringData::unicode),(this) -> d -> QStringData::len);
  }
  class QCString utf8str = (this) ->  utf8 ();
  (this) -> d -> QStringData::dirtyascii = 0;
  return ((this) -> d -> QStringData::ascii);
}
/*! \obsolete
  This functions simply calls latin1() and returns the result.
*/

const char *QString::ascii() const
{
  return (this) ->  latin1 ();
}
#ifndef QT_NO_TEXTCODEC
/*!
  Returns the string encoded in UTF8 format.
  See QTextCodec for more diverse coding/decoding of Unicode strings.
  \sa QString::fromUtf8(), local8Bit(), latin1()
*/

QCString QString::utf8() const
{
  static class QTextCodec *codec = QTextCodec:: codecForMib (106);
  return codec?codec ->  fromUnicode ( *(this)) : QCString::QCString((this) ->  latin1 ());
}
/*!
  Returns the unicode string decoded from the
  first \a len bytes of \a utf8.  If \a len is -1 (the default), the
  length of \a utf8 is used.  If trailing partial characters are in
  \a utf8, they are ignored.
  See QTextCodec for more diverse coding/decoding of Unicode strings.
*/

QString QString::fromUtf8(const char *utf8,int len)
{
  static class QTextCodec *codec = QTextCodec:: codecForMib (106);
  if (len < 0) {
    len = (qstrlen(utf8));
  }
  return codec?codec ->  toUnicode (utf8,len) :  fromLatin1 (utf8,len);
}
#endif // QT_NO_TEXTCODEC
/*!
  Creates a QString from Latin1 text.  This is the same as the
  QString(const char*) constructor, but you can make that constructor
  invisible if you compile with the define QT_NO_CAST_ASCII, in which
  case you can explicitly create a QString from Latin-1 text using
  this function.
*/

QString QString::fromLatin1(const char *chars,int len)
{
  uint l;
  class QChar *uc;
  if (len < 0) {
    uc = ::internalAsciiToUnicode(chars,&l);
  }
  else {
    uc = ::internalAsciiToUnicode(chars,&l,len);
  }
  return ::QString::QString((new QStringData (uc,l,l)),TRUE);
}
/*!
  \fn const QChar* QString::unicode() const
  Returns the Unicode representation of the string.  The result
  remains valid until the string is modified.
*/
/*!
  Returns the string encoded in a locale-specific format.  On X11, this
  is the QTextCodec::codecForLocale().  On Windows, it is a system-defined
  encoding.
  See QTextCodec for more diverse coding/decoding of Unicode strings.
  \sa QString::fromLocal8Bit(), latin1(), utf8()
*/

QCString QString::local8Bit() const
{
#ifdef QT_NO_TEXTCODEC
#else
#ifdef _WS_X11_
  static class QTextCodec *codec = QTextCodec:: codecForLocale ();
  return codec?codec ->  fromUnicode ( *(this)) : QCString::QCString((this) ->  latin1 ());
#endif
#ifdef _WS_MAC_
#endif
#ifdef _WS_WIN_
#endif
#ifdef _WS_QWS_
// ##### if there is ANY 8 bit format supported?
#endif
#endif
}
/*!
  Returns the unicode string decoded from the
  first \a len bytes of \a local8Bit.  If \a len is -1 (the default), the
  length of \a local8Bit is used.  If trailing partial characters are in
  \a local8Bit, they are ignored.
  \a local8Bit is assumed to be encoded in a locale-specific format.
  See QTextCodec for more diverse coding/decoding of Unicode strings.
*/

QString QString::fromLocal8Bit(const char *local8Bit,int len)
{
#ifdef QT_NO_TEXTCODEC
#else
  if (!local8Bit) {
    return (null);
  }
#ifdef _WS_X11_
  static class QTextCodec *codec = QTextCodec:: codecForLocale ();
  if (len < 0) {
    len = (qstrlen(local8Bit));
  }
  return codec?codec ->  toUnicode (local8Bit,len) :  fromLatin1 (local8Bit,len);
#endif
#ifdef _WS_MAC_
#endif
// Should this be OS_WIN32?
#ifdef _WS_WIN_
#endif
#ifdef _WS_QWS_
#endif
#endif // QT_NO_TEXTCODEC
}
/*!
  \fn QString::operator const char *() const
  Returns latin1().  Be sure to see the warnings documented there.
  Note that for new code which you wish to be strictly Unicode-clean,
  you can define the macro QT_NO_ASCII_CAST when compiling your code
  to hide this function so that automatic casts are not done.  This
  has the added advantage that you catch the programming error
  described under operator!().
*/
/*!
  \fn QChar QString::at( uint ) const
  Returns the character at \a i, or 0 if \a i is beyond the length
  of the string.
  Note: If this QString is not const or const&, the non-const at()
  will be used instead, which will expand the string if \a i is beyond
  the length of the string.
*/
/*!
  \fn QChar QString::constref(uint i) const
  Equivalent to at(i), this returns the QChar at \a i by value.
  \sa ref()
*/
/*!
  \fn QChar& QString::ref(uint i)
  Returns the QChar at \a i by reference.
  \sa constref()
*/
/*!
  \fn QChar QString::operator[](int) const
  Returns the character at \a i, or QChar::null if \a i is beyond the
  length of the string.
  Note: If this QString is not const or const&, the non-const operator[]
  will be used instead, which will expand the string if \a i is beyond
  the length of the string.
*/
/*!
  \fn QCharRef QString::operator[](int)
  Returns an object that references the character at \a i.
  This reference
  can then be assigned to, or otherwise used immediately, but
  becomes invalid once further modifications are made to the string.
  The QCharRef internal class can be used much like a constant QChar, but
  if you assign to it, you change the original string (which enlarges
  and detaches itself). You will get compilation errors if you try to
  use the result as anything but a QChar.
*/
/*!
  \fn QCharRef QString::at( uint i )
  Returns a reference to the character at \a i, expanding
  the string with QChar::null if necessary.  The resulting reference
  can then be assigned to, or otherwise used immediately, but
  becomes invalid once further modifications are made to the string.
*/
/*!
  Internal chunk of code to handle the
  uncommon cases of at() above.
*/

void QString::subat(uint i)
{
  uint olen = (this) -> d -> QStringData::len;
  if (i >= olen) {
// i is index; i+1 is needed length
    (this) ->  setLength (i + 1);
    for (uint j = olen; j <= i; j++) 
      (this) -> d -> QStringData::unicode[j] = QChar::null;
  }
  else {
// Just be sure to detach
    (this) ->  real_detach ();
  }
}
/*!
  Resizes the string to \a len unicode characters and copies \a unicode
  into the string.  If \a unicode is null, nothing is copied, but the
  string is resized to \a len anyway. If \a len is zero, the string
  becomes a \link isNull() null\endlink string.
  \sa setLatin1(), isNull()
*/

QString &QString::setUnicode(const class QChar *unicode,uint len)
{
// set to null string
  if (len == 0) {
// beware of nullstring being set to nullstring
    if ((this) -> d != shared_null) {
      (this) ->  deref ();
      (this) -> d = (shared_null?shared_null :  makeSharedNull ());
      ((this) -> d) ->  ref ();
    }
  }
  else {
    if (((this) -> d) -> QShared::count != 1 || len > ((this) -> d -> QStringData::maxl) || len * 4 < ((this) -> d -> QStringData::maxl) && ((this) -> d -> QStringData::maxl) > 4) 
// detach, grown or shrink
{
      uint newMax = 4;
      while(newMax < len)
        newMax *= 2;
      class QChar *nd = (class QChar *)(new char [sizeof(QChar ) * newMax]);
      if (unicode) {
        memcpy(nd,unicode,sizeof(class QChar ) * len);
      }
      (this) ->  deref ();
      (this) -> d = (new QStringData (nd,len,newMax));
    }
    else {
      (this) -> d -> QStringData::len = len;
      (this) -> d -> QStringData::dirtyascii = 1;
      if (unicode) {
        memcpy(((this) -> d -> QStringData::unicode),unicode,sizeof(class QChar ) * len);
      }
    }
  }
  return  *(this);
}
/*!
  Resizes the string to \a len unicode characters and copies
  \a unicode_as_ushorts into the string (on some X11 client
  platforms this will involve a byte-swapping pass).
  If \a unicode is null, nothing is copied, but the
  string is resized to \a len anyway. If \a len is zero, the string
  becomes a \link isNull() null\endlink string.
  \sa setLatin1(), isNull()
*/

QString &QString::setUnicodeCodes(const ushort *unicode_as_ushorts,uint len)
{
  (this) ->  setUnicode (((const class QChar *)unicode_as_ushorts),len);
  class QChar t(0x1234);
  if (unicode_as_ushorts && ( *((ushort *)(&t))) == 0x3412) {
// Need to byteswap
    char *b = (char *)((this) -> d -> QStringData::unicode);
    while((len--)){
      char c = b[0];
      b[0] = b[1];
      b[1] = c;
      b += sizeof(class QChar );
    }
  }
  return  *(this);
}
/*!
  Sets this string to \a str, interpreted as a classic Latin 1 C string.
  If the \a len argument is negative (default), it is set to strlen(str).
  If \a str is 0 a null string is created.  If \a str is "" an empty
  string is created.
  \sa isNull(), isEmpty()
*/

QString &QString::setLatin1(const char *str,int len)
{
  if (str == 0) {
    return (this) ->  setUnicode (0,0);
  }
  if (len < 0) {
    len = (qstrlen(str));
  }
// won't make a null string
  if (len == 0) {
    (this) ->  deref ();
    uint l;
    class QChar *uc = ::internalAsciiToUnicode(str,&l);
    (this) -> d = (new QStringData (uc,l,l));
  }
  else {
// resize but not copy
    (this) ->  setUnicode (0,len);
    class QChar *p = (this) -> d -> QStringData::unicode;
    while((len--))
       *(p++) = ( *(str++));
  }
  return  *(this);
}
/*!
  \fn int QString::compare (const QString & s1, const QString & s2)
  Compare \a s1 to \a s2 returning an integer less than, equal to, or
  greater than zero if s1 is, respectively, lexically less than, equal to,
  or greater than s2.
*/
/*!
  Compares this string to \a s, returning an integer less than, equal to, or
  greater than zero if it is, respectively, lexically less than, equal to,
  or greater than \a s.
*/

int QString::compare(const class ::QString &s) const
{
  return ucstrcmp( *(this),s);
}

bool operator==(const class QString &s1,const class QString &s2)
{
  return s1 .  length () == s2 .  length () && (s1 .  isNull ()) == (s2 .  isNull ()) && memcmp(((char *)(s1 .  unicode ())),((char *)(s2 .  unicode ())),(s1 .  length ()) * sizeof(class QChar )) == 0;
}

bool operator!=(const class QString &s1,const class QString &s2)
{
  return !(s1==s2);
}

bool operator<(const class QString &s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) < 0;
}

bool operator<=(const class QString &s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) <= 0;
}

bool operator>(const class QString &s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) > 0;
}

bool operator>=(const class QString &s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) >= 0;
}

bool operator==(const class QString &s1,const char *s2)
{
  return s1==QString::QString(s2);
}

bool operator==(const char *s1,const class QString &s2)
{
  return QString::QString(s1)==s2;
}

bool operator!=(const class QString &s1,const char *s2)
{
  return !(s1==s2);
}

bool operator!=(const char *s1,const class QString &s2)
{
  return !(s1==s2);
}

bool operator<(const class QString &s1,const char *s2)
{
  return ucstrcmp(s1,s2) < 0;
}

bool operator<(const char *s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) < 0;
}

bool operator<=(const class QString &s1,const char *s2)
{
  return ucstrcmp(s1,s2) <= 0;
}

bool operator<=(const char *s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) <= 0;
}

bool operator>(const class QString &s1,const char *s2)
{
  return ucstrcmp(s1,s2) > 0;
}

bool operator>(const char *s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) > 0;
}

bool operator>=(const class QString &s1,const char *s2)
{
  return ucstrcmp(s1,s2) >= 0;
}

bool operator>=(const char *s1,const class QString &s2)
{
  return ucstrcmp(s1,s2) >= 0;
}
/*****************************************************************************
  Documentation for QString related functions
 *****************************************************************************/
/*!
  \fn bool operator==( const QString &s1, const QString &s2 )
  \relates QString
  Returns TRUE if the two strings are equal, or FALSE if they are different.
  A null string is different from an empty, non-null string.
  Equivalent to <code>qstrcmp(s1,s2) == 0</code>.
*/
/*!
  \fn bool operator==( const QString &s1, const char *s2 )
  \relates QString
  Returns TRUE if the two strings are equal, or FALSE if they are different.
  Equivalent to <code>qstrcmp(s1,s2) == 0</code>.
*/
/*!
  \fn bool operator==( const char *s1, const QString &s2 )
  \relates QString
  Returns TRUE if the two strings are equal, or FALSE if they are different.
  Equivalent to <code>qstrcmp(s1,s2) == 0</code>.
*/
/*!
  \fn bool operator!=( const QString &s1, const QString &s2 )
  \relates QString
  Returns TRUE if the two strings are different, or FALSE if they are equal.
  Equivalent to <code>qstrcmp(s1,s2) != 0</code>.
*/
/*!
  \fn bool operator!=( const QString &s1, const char *s2 )
  \relates QString
  Returns TRUE if the two strings are different, or FALSE if they are equal.
  Equivalent to <code>qstrcmp(s1,s2) != 0</code>.
*/
/*!
  \fn bool operator!=( const char *s1, const QString &s2 )
  \relates QString
  Returns TRUE if the two strings are different, or FALSE if they are equal.
  Equivalent to <code>qstrcmp(s1,s2) != 0</code>.
*/
/*!
  \fn bool operator<( const QString &s1, const char *s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically less than \a s2, otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \< 0</code>.
*/
/*!
  \fn bool operator<( const char *s1, const QString &s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically less than \a s2, otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \< 0</code>.
*/
/*!
  \fn bool operator<=( const QString &s1, const char *s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically less than or equal to \a s2,
  otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \<= 0</code>.
*/
/*!
  \fn bool operator<=( const char *s1, const QString &s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically less than or equal to \a s2,
  otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \<= 0</code>.
*/
/*!
  \fn bool operator>( const QString &s1, const char *s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically greater than \a s2, otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \> 0</code>.
*/
/*!
  \fn bool operator>( const char *s1, const QString &s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically greater than \a s2, otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \> 0</code>.
*/
/*!
  \fn bool operator>=( const QString &s1, const char *s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically greater than or equal to \a s2,
  otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \>= 0</code>.
*/
/*!
  \fn bool operator>=( const char *s1, const QString &s2 )
  \relates QString
  Returns TRUE if \a s1 is alphabetically greater than or equal to \a s2,
  otherwise FALSE.
  Equivalent to <code>qstrcmp(s1,s2) \>= 0</code>.
*/
/*!
  \fn QString operator+( const QString &s1, const QString &s2 )
  \relates QString
  Returns the concatenated string of s1 and s2.
*/
/*!
  \fn QString operator+( const QString &s1, const char *s2 )
  \relates QString
  Returns the concatenated string of s1 and s2.
*/
/*!
  \fn QString operator+( const char *s1, const QString &s2 )
  \relates QString
  Returns the concatenated string of s1 and s2.
*/
/*!
  \fn QString operator+( const QString &s, char c )
  \relates QString
  Returns the concatenated string of s and c.
*/
/*!
  \fn QString operator+( char c, const QString &s )
  \relates QString
  Returns the concatenated string of c and s.
*/
/*****************************************************************************
  QString stream functions
 *****************************************************************************/
#ifndef QT_NO_DATASTREAM
/*!
  \relates QString
  Writes a string to the stream.
  \sa \link datastreamformat.html Format of the QDataStream operators \endlink
*/

class QDataStream &operator<<(class QDataStream &s,const class QString &str)
{
  if (s .  version () == 1) {
    class QCString l(str .  latin1 ());
    s<<l;
  }
  else {
    const char *ub = (const char *)(str .  unicode ());
    if (ub || s .  version () < 3) {
      if ((QChar:: networkOrdered ()) == (s .  byteOrder () == QDataStream::BigEndian)) {
        s .  writeBytes (ub,(((int )(sizeof(class QChar ))) * str .  length ()));
      }
      else {
        static const uint auto_size = 1024;
        char t[auto_size];
        char *b;
        if ((str .  length ()) * sizeof(class QChar ) > auto_size) {
          b = (new char [(str .  length ()) * sizeof(QChar )]);
        }
        else {
          b = t;
        }
        int l = (str .  length ());
        char *c = b;
        while((l--)){
           *(c++) = ub[1];
           *(c++) = ub[0];
          ub += sizeof(class QChar );
        }
        s .  writeBytes (b,(((int )(sizeof(class QChar ))) * str .  length ()));
        if ((str .  length ()) * sizeof(class QChar ) > auto_size) {
          delete []b;
        }
      }
    }
    else {
// write null marker
      s << ((Q_UINT32 )0xffffffff);
    }
  }
  return s;
}
/*!
  \relates QString
  Reads a string from the stream.
  \sa \link datastreamformat.html Format of the QDataStream operators \endlink
*/

class QDataStream &operator>>(class QDataStream &s,class QString &str)
{
#ifdef QT_QSTRING_UCS_4
#if defined(_CC_GNU_)
#warning "operator>> not working properly"
#endif
#endif
  if (s .  version () == 1) {
    class QCString l;
    s>>l;
    str = QString::QString(l);
  }
  else {
    Q_UINT32 bytes;
// read size of string
    s >> bytes;
// null string
    if (bytes == 0xffffffff) {
      str = QString::null;
// not empty
    }
    else {
      if (bytes > 0) {
        str .  setLength ((bytes / 2));
        char *b = (char *)(str . QString::d -> QStringData::unicode);
        s .  readRawBytes (b,bytes);
        if ((QChar:: networkOrdered ()) != (s .  byteOrder () == QDataStream::BigEndian)) {
          bytes /= 2;
          while((bytes--)){
            char c = b[0];
            b[0] = b[1];
            b[1] = c;
            b += 2;
          }
        }
      }
      else {
        str = "";
      }
    }
  }
  return s;
}
#endif // QT_NO_DATASTREAM
/*****************************************************************************
  QConstString member functions
 *****************************************************************************/
/*!
  \class QConstString qstring.h
  \brief A QString which uses constant Unicode data.
  In order to minimize copying, highly optimized applications can use
  QConstString to provide a QString-compatible object from existing
  Unicode data.  It is then the user's responsibility to make sure
  that the Unicode data must exist for the entire lifetime of the
  QConstString object.
*/
/*!
  Constructs a QConstString that uses the first \a length Unicode
  characters in the array \a unicode.  Any attempt to modify
  copies of the string will cause it to create a copy of the
  data, thus it remains forever unmodified.
  Note that \a unicode is \e not \e copied.  The caller \e must be
  able to guarantee that \a unicode will not be deleted or
  modified. Since that is generally not the case with \c const strings
  (they are references), this constructor demands a non-const pointer
  even though it never modifies \a unicode.
*/

QConstString::QConstString(class QChar *unicode,uint length) : QString((new QStringData (unicode,length,length)),TRUE)
{
}
/*!
  Destroys the QConstString, creating a copy of the data if
  other strings are still using it.
*/

QConstString::~QConstString()
{
  if (((this) -> d) -> QShared::count > 1) {
    class QChar *cp = (class QChar *)(new char [sizeof(QChar ) * ((this) -> d -> len)]);
    memcpy(cp,((this) -> d -> QStringData::unicode),((this) -> d -> QStringData::len) * sizeof(class QChar ));
    (this) -> d -> QStringData::unicode = cp;
  }
  else {
    (this) -> d -> QStringData::unicode = 0;
  }
// The original d->unicode is now unlinked.
}
/*!
  \fn const QString& QConstString::string() const
  Returns a constant string referencing the data passed during
  construction.
*/
/*!
  Returns whether the strings starts with \a s, or not.
 */

bool QString::startsWith(const class ::QString &s) const
{
  for (int i = 0; i < ((int )(s .  length ())); i++) {
    if (i >= ((int )((this) ->  length ())) || (((this) -> d -> QStringData::unicode[i])!=s[i])) {
      return FALSE;
    }
  }
  return TRUE;
}
#if defined(_OS_WIN32_)
#include <windows.h>
/*!
  Returns a static Windows TCHAR* from a QString, possibly adding NUL.
  The lifetime of the return value is until the next call to this function.
*/
// So that the return value lives long enough.
#ifdef UNICODE
#define EXTEND if (str.length() > buflen) { delete buf; buf = new TCHAR[buflen=str.length()+1]; }
#if defined(_WS_X11_) || defined(_OS_WIN32_BYTESWAP_)
#else
// Same endianness of TCHAR
#endif
#undef EXTEND
#else
#endif
/*!
  Makes a new null terminated Windows TCHAR* from a QString.
*/
/*!
  Makes a QString from a Windows TCHAR*.
*/
#ifdef UNICODE
#if defined(_WS_X11_) || defined(_OS_WIN32_BYTESWAP_)
#else
// Same endianness of TCHAR
#endif
#undef EXTEND
#else
#endif
// and try again...
// Fail.
// WATCH OUT: mblen must include the NUL (or just use -1)
// and try again...
// Fail.
// len-1: we don't want terminator
#endif // _OS_WIN32_
