/****************************************************************************
** 
**
** Implementation of QBuffer class
**
** Created : 930812
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qbuffer.h"
#include <stdlib.h>
// REVISED: paul
/*!
  \class QBuffer qbuffer.h
  \brief The QBuffer class is an I/O device that operates on a QByteArray
  \ingroup io
  QBuffer allows reading and writing a memory buffer. It is normally
  used together with a QTextStream or a QDataStream.  QBuffer has an
  associated QByteArray which holds the buffer data. The size() of the
  buffer is automatically adjusted as data is written.
  The constructor \c QBuffer(QByteArray) creates a QBuffer with an
  existing byte array.  The byte array can also be set with setBuffer().
  Writing to the QBuffer will modify the original byte array, since
  QByteArray is \link shclass.html explicitly shared.\endlink
  Use open() to open the buffer before use, and to set the mode
  (read-only,write-only, etc.).  close() closes the buffer. The buffer
  must be closed before reopening or calling setBuffer().
  The common way to use QBuffer is through \l QDataStream or \l QTextStream
  which have constructors that take a QBuffer parameter. For
  convenience, there are also QDataStream and QTextStream constructors
  that take a QByteArray parameter.  These constructors create and open
  an internal QBuffer.
  Note that QTextStream can also operate on a QString (a Unicode
  string); a QBuffer cannot.
  You can also use QBuffer directly through the standard QIODevice
  functions readBlock(), writeBlock() readLine(), at(), getch(), putch() and
  ungetch().
  \sa QFile, QDataStream, QTextStream, QByteArray, \link shclass.html Shared Classes\endlink
*/
/*!
  Constructs an empty buffer.
*/

QBuffer::QBuffer()
{
  (this) ->  setFlags (0x0100);
// initial increment
  (this) -> a_inc = 16;
  (this) -> a_len = 0;
  (this) -> ioIndex = 0;
}
/*!
  Constructs a buffer that operates on \a buf.
  If you open the buffer in write mode (\c IO_WriteOnly or
  \c IO_ReadWrite) and write something into the buffer, \a buf
  will be modified.
  Example:
  \code
    QCString str = "abc";
    QBuffer b( str );
    b.open( IO_WriteOnly );
    b.at( 3 );					// position at \0
    b.writeBlock( "def", 4 );			// write including \0
    b.close();
      // Now, str == "abcdef"
  \endcode
  \sa setBuffer()
*/

QBuffer::QBuffer(QByteArray buf) : a(buf)
{
  (this) ->  setFlags (0x0100);
  (this) -> a_len = (this) -> a .  size ();
// initial increment
  (this) -> a_inc = ((this) -> a_len > 512?512 : (this) -> a_len);
  if ((this) -> a_inc < 16) {
    (this) -> a_inc = 16;
  }
  (this) -> ioIndex = 0;
}
/*!
  Destructs the buffer.
*/

QBuffer::~QBuffer()
{
}
/*!
  Replaces the buffer's contents with \a buf.
  This may not be done when isOpen() is TRUE.
  Note that if you open the buffer in write mode (\c IO_WriteOnly or
  IO_ReadWrite) and write something into the buffer, \a buf is also
  modified because QByteArray is an explicitly shared class.
  \sa buffer(), open(), close()
*/

bool QBuffer::setBuffer(QByteArray buf)
{
  if ((this) ->  isOpen ()) {
#if defined(CHECK_STATE)
    qWarning("QBuffer::setBuffer: Buffer is open");
#endif
    return FALSE;
  }
  (this) -> a = buf;
  (this) -> a_len = (this) -> a .  size ();
// initial increment
  (this) -> a_inc = ((this) -> a_len > 512?512 : (this) -> a_len);
  if ((this) -> a_inc < 16) {
    (this) -> a_inc = 16;
  }
  (this) -> ioIndex = 0;
  return TRUE;
}
/*!
  \fn QByteArray QBuffer::buffer() const
  Returns this buffer's byte array.
  \sa setBuffer()
*/
/*!
  \reimp
  Opens the buffer in the mode \a m.  Returns TRUE if successful,
  otherwise FALSE. The buffer must be opened before use.
  The mode parameter \a m must be a combination of the following flags.
  <ul>
  <li>\c IO_ReadOnly opens a buffer in read-only mode.
  <li>\c IO_WriteOnly opens a buffer in write-only mode.
  <li>\c IO_ReadWrite opens a buffer in read/write mode.
  <li>\c IO_Append sets the buffer index to the end of the buffer.
  <li>\c IO_Truncate truncates the buffer.
  </ul>
  \sa close(), isOpen()
*/

bool QBuffer::open(int m)
{
// buffer already open
  if ((this) ->  isOpen ()) {
#if defined(CHECK_STATE)
    qWarning("QBuffer::open: Buffer already open");
#endif
    return FALSE;
  }
  (this) ->  setMode (m);
// truncate buffer
  if ((m & 0x0008)) {
    (this) -> a .  resize (0);
    (this) -> a_len = 0;
  }
// append to end of buffer
  if ((m & 0x0004)) {
    (this) -> ioIndex = ((this) -> a .  size ());
  }
  else {
    (this) -> ioIndex = 0;
  }
  (this) -> a_inc = 16;
  (this) ->  setState (0x1000);
  (this) ->  setStatus (0);
  return TRUE;
}
/*!
  \reimp
  Closes an open buffer.
  \sa open()
*/

void QBuffer::close()
{
  if ((this) ->  isOpen ()) {
    (this) ->  setFlags (0x0100);
    (this) -> ioIndex = 0;
    (this) -> a_inc = 16;
  }
}
/*!
  \reimp
  The flush function does nothing for a QBuffer.
*/

void QBuffer::flush()
{
  return ;
}
/*!
  \fn int QBuffer::at() const
  \reimp
*/
/*!
  \fn uint QBuffer::size() const
  \reimp
*/
/*!
  \reimp
*/

bool QBuffer::at(int pos)
{
#if defined(CHECK_STATE)
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::at: Buffer is not open");
    return FALSE;
  }
#endif
  if (((uint )pos) > (this) -> a_len) {
#if defined(CHECK_RANGE)
    qWarning("QBuffer::at: Index %d out of range",pos);
#endif
    return FALSE;
  }
  (this) -> ioIndex = pos;
  return TRUE;
}
/*!
  \reimp
*/

int QBuffer::readBlock(char *p,uint len)
{
#if defined(CHECK_STATE)
  qt_check_pointer(p == 0,"qbuffer.cpp",277);
// buffer not open
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::readBlock: Buffer not open");
    return -1;
  }
// reading not permitted
  if (!(this) ->  isReadable ()) {
    qWarning("QBuffer::readBlock: Read operation not permitted");
    return -1;
  }
#endif
// overflow
  if (((uint )((this) -> ioIndex)) + len > (this) -> a .  size ()) {
    if (((uint )((this) -> ioIndex)) >= (this) -> a .  size ()) {
      (this) ->  setStatus (1);
      return -1;
    }
    else {
      len = (this) -> a .  size () - ((uint )((this) -> ioIndex));
    }
  }
  memcpy(p,((this) -> a .  data () + (this) -> ioIndex),len);
  (this) -> ioIndex += len;
  return len;
}
/*!
  \reimp
  Writes \a len bytes from \a p into the buffer at the current index,
  overwriting any characters there and extending the buffer if necessary.
  Returns the number of bytes actually written.
  Returns -1 if a serious error occurred.
  \sa readBlock()
*/

int QBuffer::writeBlock(const char *p,uint len)
{
#if defined(CHECK_NULL)
  if (p == 0 && len != 0) {
    qWarning("QBuffer::writeBlock: Null pointer error");
  }
#endif
#if defined(CHECK_STATE)
// buffer not open
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::writeBlock: Buffer not open");
    return -1;
  }
// writing not permitted
  if (!(this) ->  isWritable ()) {
    qWarning("QBuffer::writeBlock: Write operation not permitted");
    return -1;
  }
#endif
// overflow
  if (((uint )((this) -> ioIndex)) + len >= (this) -> a_len) {
    uint new_len = (this) -> a_len + (this) -> a_inc * ((((uint )((this) -> ioIndex)) + len - (this) -> a_len) / (this) -> a_inc + 1);
// could not resize
    if (!(this) -> a .  resize (new_len)) {
#if defined(CHECK_NULL)
      qWarning("QBuffer::writeBlock: Memory allocation error");
#endif
      (this) ->  setStatus (4);
      return -1;
    }
// double increment
    (this) -> a_inc *= 2;
    (this) -> a_len = new_len;
    ((this) -> a) . QGArray::shd -> QGArray::array_data::len = ((uint )((this) -> ioIndex)) + len;
  }
  memcpy(((this) -> a .  data () + (this) -> ioIndex),p,len);
  (this) -> ioIndex += len;
  if (((this) -> a) . QGArray::shd -> QGArray::array_data::len < ((uint )((this) -> ioIndex))) {
// fake (not alloc'd) length
    ((this) -> a) . QGArray::shd -> QGArray::array_data::len = ((uint )((this) -> ioIndex));
  }
  return len;
}
/*!
  \reimp
*/

int QBuffer::readLine(char *p,uint maxlen)
{
#if defined(CHECK_STATE)
  qt_check_pointer(p == 0,"qbuffer.cpp",356);
// buffer not open
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::readLine: Buffer not open");
    return -1;
  }
// reading not permitted
  if (!(this) ->  isReadable ()) {
    qWarning("QBuffer::readLine: Read operation not permitted");
    return -1;
  }
#endif
  if (maxlen == 0) {
    return 0;
  }
  uint start = (uint )((this) -> ioIndex);
  char *d = (this) -> a .  data () + (this) -> ioIndex;
// make room for 0-terminator
  maxlen--;
  if ((this) -> a .  size () - ((uint )((this) -> ioIndex)) < maxlen) {
    maxlen = (this) -> a .  size () - ((uint )((this) -> ioIndex));
  }
  while((maxlen--)){
    if (( *(p++) =  *(d++)) == '\n') {
      break; 
    }
  }
   *p = '\0';
  (this) -> ioIndex = ((int )(d - (this) -> a .  data ()));
  return (((uint )((this) -> ioIndex)) - start);
}
/*!
  \reimp
*/

int QBuffer::getch()
{
#if defined(CHECK_STATE)
// buffer not open
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::getch: Buffer not open");
    return -1;
  }
// reading not permitted
  if (!(this) ->  isReadable ()) {
    qWarning("QBuffer::getch: Read operation not permitted");
    return -1;
  }
#endif
// overflow
  if (((uint )((this) -> ioIndex)) + 1 > (this) -> a .  size ()) {
    (this) ->  setStatus (1);
    return -1;
  }
  return ((uchar )( *((this) -> a .  data () + (this) -> ioIndex++)));
}
/*!
  \reimp
  Writes the character \a ch into the buffer, overwriting
  the character at the current index, extending the buffer
  if necessary.
  Returns \a ch, or -1 if some error occurred.
  \sa getch(), ungetch()
*/

int QBuffer::putch(int ch)
{
#if defined(CHECK_STATE)
// buffer not open
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::putch: Buffer not open");
    return -1;
  }
// writing not permitted
  if (!(this) ->  isWritable ()) {
    qWarning("QBuffer::putch: Write operation not permitted");
    return -1;
  }
#endif
// overflow
  if (((uint )((this) -> ioIndex)) + 1 >= (this) -> a_len) {
    char buf[1];
    buf[0] = ((char )ch);
    if ((this) ->  writeBlock (buf,1) != 1) {
// write error
      return -1;
    }
  }
  else {
     *((this) -> a .  data () + (this) -> ioIndex++) = ((char )ch);
    if (((this) -> a) . QGArray::shd -> QGArray::array_data::len < ((uint )((this) -> ioIndex))) {
      ((this) -> a) . QGArray::shd -> QGArray::array_data::len = ((uint )((this) -> ioIndex));
    }
  }
  return ch;
}
/*!
  \reimp
*/

int QBuffer::ungetch(int ch)
{
#if defined(CHECK_STATE)
// buffer not open
  if (!(this) ->  isOpen ()) {
    qWarning("QBuffer::ungetch: Buffer not open");
    return -1;
  }
// reading not permitted
  if (!(this) ->  isReadable ()) {
    qWarning("QBuffer::ungetch: Read operation not permitted");
    return -1;
  }
#endif
  if (ch != -1) {
    if (((this) -> ioIndex)) {
      (this) -> ioIndex--;
    }
    else {
      ch = -1;
    }
  }
  return ch;
}
