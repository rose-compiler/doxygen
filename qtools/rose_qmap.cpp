/****************************************************************************
** 
**
** Implementation of QMap
**
** Created : 990406
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qmap.h"
typedef struct QMapNodeBase *NodePtr;
typedef struct QMapNodeBase Node;

void QMapPrivateBase::rotateLeft(NodePtr x,NodePtr &root)
{
  NodePtr y = x -> QMapNodeBase::right;
  x -> QMapNodeBase::right = y -> QMapNodeBase::left;
  if (y -> QMapNodeBase::left != 0) {
    y -> QMapNodeBase::left -> QMapNodeBase::parent = x;
  }
  y -> QMapNodeBase::parent = x -> QMapNodeBase::parent;
  if (x == root) {
    root = y;
  }
  else {
    if (x == x -> QMapNodeBase::parent -> QMapNodeBase::left) {
      x -> QMapNodeBase::parent -> QMapNodeBase::left = y;
    }
    else {
      x -> QMapNodeBase::parent -> QMapNodeBase::right = y;
    }
  }
  y -> QMapNodeBase::left = x;
  x -> QMapNodeBase::parent = y;
}

void QMapPrivateBase::rotateRight(NodePtr x,NodePtr &root)
{
  NodePtr y = x -> QMapNodeBase::left;
  x -> QMapNodeBase::left = y -> QMapNodeBase::right;
  if (y -> QMapNodeBase::right != 0) {
    y -> QMapNodeBase::right -> QMapNodeBase::parent = x;
  }
  y -> QMapNodeBase::parent = x -> QMapNodeBase::parent;
  if (x == root) {
    root = y;
  }
  else {
    if (x == x -> QMapNodeBase::parent -> QMapNodeBase::right) {
      x -> QMapNodeBase::parent -> QMapNodeBase::right = y;
    }
    else {
      x -> QMapNodeBase::parent -> QMapNodeBase::left = y;
    }
  }
  y -> QMapNodeBase::right = x;
  x -> QMapNodeBase::parent = y;
}

void QMapPrivateBase::rebalance(NodePtr x,NodePtr &root)
{
  x -> QMapNodeBase::color = QMapNodeBase::Red;
  while(x != root && (x -> QMapNodeBase::parent -> QMapNodeBase::color) == QMapNodeBase::Red){
    if (x -> QMapNodeBase::parent == x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::left) {
      NodePtr y = x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::right;
      if (y && (y -> QMapNodeBase::color) == QMapNodeBase::Red) {
        x -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Black;
        y -> QMapNodeBase::color = QMapNodeBase::Black;
        x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Red;
        x = x -> QMapNodeBase::parent -> QMapNodeBase::parent;
      }
      else {
        if (x == x -> QMapNodeBase::parent -> QMapNodeBase::right) {
          x = x -> QMapNodeBase::parent;
          (this) ->  rotateLeft (x,root);
        }
        x -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Black;
        x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Red;
        (this) ->  rotateRight (x -> QMapNodeBase::parent -> QMapNodeBase::parent,root);
      }
    }
    else {
      NodePtr y = x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::left;
      if (y && (y -> QMapNodeBase::color) == QMapNodeBase::Red) {
        x -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Black;
        y -> QMapNodeBase::color = QMapNodeBase::Black;
        x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Red;
        x = x -> QMapNodeBase::parent -> QMapNodeBase::parent;
      }
      else {
        if (x == x -> QMapNodeBase::parent -> QMapNodeBase::left) {
          x = x -> QMapNodeBase::parent;
          (this) ->  rotateRight (x,root);
        }
        x -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Black;
        x -> QMapNodeBase::parent -> QMapNodeBase::parent -> QMapNodeBase::color = QMapNodeBase::Red;
        (this) ->  rotateLeft (x -> QMapNodeBase::parent -> QMapNodeBase::parent,root);
      }
    }
  }
  root -> QMapNodeBase::color = QMapNodeBase::Black;
}

NodePtr QMapPrivateBase::removeAndRebalance(NodePtr z,NodePtr &root,NodePtr &leftmost,NodePtr &rightmost)
{
  NodePtr y = z;
  NodePtr x;
  NodePtr x_parent;
  if (y -> QMapNodeBase::left == 0) {
    x = y -> QMapNodeBase::right;
  }
  else {
    if (y -> QMapNodeBase::right == 0) {
      x = y -> QMapNodeBase::left;
    }
    else {
      y = y -> QMapNodeBase::right;
      while(y -> QMapNodeBase::left != 0)
        y = y -> QMapNodeBase::left;
      x = y -> QMapNodeBase::right;
    }
  }
  if (y != z) {
    z -> QMapNodeBase::left -> QMapNodeBase::parent = y;
    y -> QMapNodeBase::left = z -> QMapNodeBase::left;
    if (y != z -> QMapNodeBase::right) {
      x_parent = y -> QMapNodeBase::parent;
      if (x) {
        x -> QMapNodeBase::parent = y -> QMapNodeBase::parent;
      }
      y -> QMapNodeBase::parent -> QMapNodeBase::left = x;
      y -> QMapNodeBase::right = z -> QMapNodeBase::right;
      z -> QMapNodeBase::right -> QMapNodeBase::parent = y;
    }
    else {
      x_parent = y;
    }
    if (root == z) {
      root = y;
    }
    else {
      if (z -> QMapNodeBase::parent -> QMapNodeBase::left == z) {
        z -> QMapNodeBase::parent -> QMapNodeBase::left = y;
      }
      else {
        z -> QMapNodeBase::parent -> QMapNodeBase::right = y;
      }
    }
    y -> QMapNodeBase::parent = z -> QMapNodeBase::parent;
// Swap the colors
    enum QMapNodeBase::Color c = y -> QMapNodeBase::color;
    y -> QMapNodeBase::color = z -> QMapNodeBase::color;
    z -> QMapNodeBase::color = c;
    y = z;
  }
  else {
    x_parent = y -> QMapNodeBase::parent;
    if (x) {
      x -> QMapNodeBase::parent = y -> QMapNodeBase::parent;
    }
    if (root == z) {
      root = x;
    }
    else {
      if (z -> QMapNodeBase::parent -> QMapNodeBase::left == z) {
        z -> QMapNodeBase::parent -> QMapNodeBase::left = x;
      }
      else {
        z -> QMapNodeBase::parent -> QMapNodeBase::right = x;
      }
    }
    if (leftmost == z) {
      if (z -> QMapNodeBase::right == 0) {
        leftmost = z -> QMapNodeBase::parent;
      }
      else {
        leftmost = x ->  minimum ();
      }
    }
    if (rightmost == z) {
      if (z -> QMapNodeBase::left == 0) {
        rightmost = z -> QMapNodeBase::parent;
      }
      else {
        rightmost = x ->  maximum ();
      }
    }
  }
  if ((y -> QMapNodeBase::color) != QMapNodeBase::Red) {
    while(x != root && (x == 0 || (x -> QMapNodeBase::color) == QMapNodeBase::Black)){
      if (x == x_parent -> QMapNodeBase::left) {
        NodePtr w = x_parent -> QMapNodeBase::right;
        if ((w -> QMapNodeBase::color) == QMapNodeBase::Red) {
          w -> QMapNodeBase::color = QMapNodeBase::Black;
          x_parent -> QMapNodeBase::color = QMapNodeBase::Red;
          (this) ->  rotateLeft (x_parent,root);
          w = x_parent -> QMapNodeBase::right;
        }
        if ((w -> QMapNodeBase::left == 0 || (w -> QMapNodeBase::left -> QMapNodeBase::color) == QMapNodeBase::Black) && (w -> QMapNodeBase::right == 0 || (w -> QMapNodeBase::right -> QMapNodeBase::color) == QMapNodeBase::Black)) {
          w -> QMapNodeBase::color = QMapNodeBase::Red;
          x = x_parent;
          x_parent = x_parent -> QMapNodeBase::parent;
        }
        else {
          if (w -> QMapNodeBase::right == 0 || (w -> QMapNodeBase::right -> QMapNodeBase::color) == QMapNodeBase::Black) {
            if ((w -> QMapNodeBase::left)) {
              w -> QMapNodeBase::left -> QMapNodeBase::color = QMapNodeBase::Black;
            }
            w -> QMapNodeBase::color = QMapNodeBase::Red;
            (this) ->  rotateRight (w,root);
            w = x_parent -> QMapNodeBase::right;
          }
          w -> QMapNodeBase::color = x_parent -> QMapNodeBase::color;
          x_parent -> QMapNodeBase::color = QMapNodeBase::Black;
          if ((w -> QMapNodeBase::right)) {
            w -> QMapNodeBase::right -> QMapNodeBase::color = QMapNodeBase::Black;
          }
          (this) ->  rotateLeft (x_parent,root);
          break; 
        }
      }
      else {
        NodePtr w = x_parent -> QMapNodeBase::left;
        if ((w -> QMapNodeBase::color) == QMapNodeBase::Red) {
          w -> QMapNodeBase::color = QMapNodeBase::Black;
          x_parent -> QMapNodeBase::color = QMapNodeBase::Red;
          (this) ->  rotateRight (x_parent,root);
          w = x_parent -> QMapNodeBase::left;
        }
        if ((w -> QMapNodeBase::right == 0 || (w -> QMapNodeBase::right -> QMapNodeBase::color) == QMapNodeBase::Black) && (w -> QMapNodeBase::left == 0 || (w -> QMapNodeBase::left -> QMapNodeBase::color) == QMapNodeBase::Black)) {
          w -> QMapNodeBase::color = QMapNodeBase::Red;
          x = x_parent;
          x_parent = x_parent -> QMapNodeBase::parent;
        }
        else {
          if (w -> QMapNodeBase::left == 0 || (w -> QMapNodeBase::left -> QMapNodeBase::color) == QMapNodeBase::Black) {
            if ((w -> QMapNodeBase::right)) {
              w -> QMapNodeBase::right -> QMapNodeBase::color = QMapNodeBase::Black;
            }
            w -> QMapNodeBase::color = QMapNodeBase::Red;
            (this) ->  rotateLeft (w,root);
            w = x_parent -> QMapNodeBase::left;
          }
          w -> QMapNodeBase::color = x_parent -> QMapNodeBase::color;
          x_parent -> QMapNodeBase::color = QMapNodeBase::Black;
          if ((w -> QMapNodeBase::left)) {
            w -> QMapNodeBase::left -> QMapNodeBase::color = QMapNodeBase::Black;
          }
          (this) ->  rotateRight (x_parent,root);
          break; 
        }
      }
    }
    if (x) {
      x -> QMapNodeBase::color = QMapNodeBase::Black;
    }
  }
  return y;
}
