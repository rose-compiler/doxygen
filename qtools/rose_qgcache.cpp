/****************************************************************************
** 
**
** Implementation of QGCache and QGCacheIterator classes
**
** Created : 950208
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qgcache.h"
#include "qlist.h"
#include "qdict.h"
#include "qstring.h"
// NOT REVISED
/*!
  \class QGCache qgcache.h
  \brief The QGCache class is an internal class for implementing QCache template classes.
  QGCache is a strictly internal class that acts as a base class for the
  \link collection.html collection classes\endlink QCache and QIntCache.
*/
/*****************************************************************************
  QGCacheItem class (internal cache item)
 *****************************************************************************/

struct QCacheItem 
{
  

  inline QCacheItem(void *k,QCollection::Item d,int c,short p) : priority(p), skipPriority(p), cost(c), key(k), data(d), node(0)
{
  }
  short priority;
  short skipPriority;
  int cost;
  void *key;
  QCollection::Item data;
  class QLNode *node;
}
;
/*****************************************************************************
  QCList class (internal list of cache items)
 *****************************************************************************/

class QCList : private QList < QCacheItem > 
{
  public: friend class QGCacheIterator ;
  friend class QCListIt ;
  

  inline QCList()
{
  }
  virtual ~QCList();
// insert according to priority
  void insert(struct QCacheItem *ci);
  inline void insert(int i,struct QCacheItem *ci);
  void take(struct QCacheItem *ci);
  inline void reference(struct QCacheItem *ci);
  

  inline void setAutoDelete(bool del)
{
    (this) -> QCollection:: setAutoDelete (del);
  }
  

  inline bool removeFirst()
{
    return (this) -> QList< QCacheItem > :: removeFirst ();
  }
  

  inline bool removeLast()
{
    return (this) -> QList< QCacheItem > :: removeLast ();
  }
  

  inline QCacheItem *first()
{
    return (this) -> QList< QCacheItem > :: first ();
  }
  

  inline QCacheItem *last()
{
    return (this) -> QList< QCacheItem > :: last ();
  }
  

  inline QCacheItem *prev()
{
    return (this) -> QList< QCacheItem > :: prev ();
  }
  

  inline QCacheItem *next()
{
    return (this) -> QList< QCacheItem > :: next ();
  }
#if defined(DEBUG)
// variables for statistics
  int inserts;
  int insertCosts;
  int insertMisses;
  int finds;
  int hits;
  int hitCosts;
  int dumps;
  int dumpCosts;
#endif
}
;

QCList::~QCList()
{
#if defined(DEBUG)
  if (!((this) ->  count () == 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","count() == 0","qgcache.cpp",115);
  }
#endif
}

void QCList::insert(struct QCacheItem *ci)
{
  struct QCacheItem *item = (this) ->  first ();
  while(item && (item -> QCacheItem::skipPriority) > (ci -> QCacheItem::priority)){
    item -> QCacheItem::skipPriority--;
    item = (this) ->  next ();
  }
  if (item) {
    (this) -> QList< QCacheItem > :: insert (((this) ->  at ()),ci);
  }
  else {
    (this) ->  append (ci);
  }
#if defined(DEBUG)
  if (!(ci -> QCacheItem::node == 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","ci->node == 0","qgcache.cpp",132);
  }
#endif
  ci -> QCacheItem::node = (this) ->  currentNode ();
}

inline void QCList::insert(int i,struct QCacheItem *ci)
{
  (this) -> QList< QCacheItem > :: insert (i,ci);
#if defined(DEBUG)
  if (!(ci -> QCacheItem::node == 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","ci->node == 0","qgcache.cpp",141);
  }
#endif
  ci -> QCacheItem::node = (this) ->  currentNode ();
}

void QCList::take(struct QCacheItem *ci)
{
  if (ci) {
#if defined(DEBUG)
    if (!(ci -> QCacheItem::node != 0)) {
      qWarning("ASSERT: \"%s\" in %s (%d)","ci->node != 0","qgcache.cpp",151);
    }
#endif
    (this) ->  takeNode (ci -> QCacheItem::node);
    ci -> QCacheItem::node = 0;
  }
}

inline void QCList::reference(struct QCacheItem *ci)
{
#if defined(DEBUG)
  if (!(ci != 0 && ci -> QCacheItem::node != 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","ci != 0 && ci->node != 0","qgcache.cpp",162);
  }
#endif
  ci -> QCacheItem::skipPriority = ci -> QCacheItem::priority;
// relink as first item
  (this) ->  relinkNode (ci -> QCacheItem::node);
}

class QCListIt : public QListIterator < QCacheItem > 
{
  

  public: inline QCListIt(const class QCList *p) : QListIterator < QCacheItem  > (( *p))
{
  }
  

  inline QCListIt(const class ::QCListIt *p) : QListIterator < QCacheItem  > (( *p))
{
  }
}
;
/*****************************************************************************
  QCDict class (internal dictionary of cache items)
 *****************************************************************************/
//
// Since we need to decide if the dictionary should use an int or const
// char * key (the "bool trivial" argument in the constructor below)
// we cannot use the macro/template dict, but inherit directly from QGDict.
//

class QCDict : public QGDict
{
  

  public: inline QCDict(uint size,uint kt,bool caseSensitive,bool copyKeys) : QGDict(size,((enum QGDict::KeyType )kt),caseSensitive,copyKeys)
{
  }
  

  inline QCacheItem *find_string(const class QString &key) const
{
    return (struct QCacheItem *)(((class ::QCDict *)(this)) ->  look_string (key,0,0));
  }
  

  inline QCacheItem *find_ascii(const char *key) const
{
    return (struct QCacheItem *)(((class ::QCDict *)(this)) ->  look_ascii (key,0,0));
  }
  

  inline QCacheItem *find_int(long key) const
{
    return (struct QCacheItem *)(((class ::QCDict *)(this)) ->  look_int (key,0,0));
  }
  

  inline QCacheItem *take_string(const class QString &key)
{
    return (struct QCacheItem *)((this) -> QGDict:: take_string (key));
  }
  

  inline QCacheItem *take_ascii(const char *key)
{
    return (struct QCacheItem *)((this) -> QGDict:: take_ascii (key));
  }
  

  inline QCacheItem *take_int(long key)
{
    return (struct QCacheItem *)((this) -> QGDict:: take_int (key));
  }
  

  inline bool insert_string(const class QString &key,const struct QCacheItem *ci)
{
    return (this) ->  look_string (key,((Item )ci),1) != 0;
  }
  

  inline bool insert_ascii(const char *key,const struct QCacheItem *ci)
{
    return (this) ->  look_ascii (key,((Item )ci),1) != 0;
  }
  

  inline bool insert_int(long key,const struct QCacheItem *ci)
{
    return (this) ->  look_int (key,((Item )ci),1) != 0;
  }
  

  inline bool remove_string(struct QCacheItem *item)
{
    return (this) -> QGDict:: remove_string ( *((class QString *)(item -> QCacheItem::key)),item);
  }
  

  inline bool remove_ascii(struct QCacheItem *item)
{
    return (this) -> QGDict:: remove_ascii (((const char *)(item -> QCacheItem::key)),item);
  }
  

  inline bool remove_int(struct QCacheItem *item)
{
    return (this) -> QGDict:: remove_int (((intptr_t )(item -> QCacheItem::key)),item);
  }
  

  inline void statistics()
{
    (this) -> QGDict:: statistics ();
  }
}
;
/*****************************************************************************
  QGDict member functions
 *****************************************************************************/
/*!
  \internal
  Constructs a cache.
*/

QGCache::QGCache(int maxCost,uint size,enum KeyType kt,bool caseSensitive,bool copyKeys)
{
  (this) -> keytype = kt;
  (this) -> lruList = (new QCList ());
  qt_check_pointer((this) -> lruList == 0,"qgcache.cpp",239);
  (this) -> lruList ->  setAutoDelete (TRUE);
  (this) -> copyk = ((this) -> keytype) == AsciiKey && copyKeys;
  (this) -> dict = (new QCDict (size,kt,caseSensitive,FALSE));
  qt_check_pointer((this) -> dict == 0,"qgcache.cpp",243);
  (this) -> mCost = maxCost;
  (this) -> tCost = 0;
#if defined(DEBUG)
  (this) -> lruList -> QCList::inserts = 0;
  (this) -> lruList -> QCList::insertCosts = 0;
  (this) -> lruList -> QCList::insertMisses = 0;
  (this) -> lruList -> QCList::finds = 0;
  (this) -> lruList -> QCList::hits = 0;
  (this) -> lruList -> QCList::hitCosts = 0;
  (this) -> lruList -> QCList::dumps = 0;
  (this) -> lruList -> QCList::dumpCosts = 0;
#endif
}
/*!
  \internal
  Cannot copy a cache.
*/

QGCache::QGCache(const class ::QGCache &) : QCollection()
{
#if defined(CHECK_NULL)
  qFatal("QGCache::QGCache(QGCache &): Cannot copy a cache");
#endif
}
/*!
  \internal
  Removes all items from the cache and destroys it.
*/

QGCache::~QGCache()
{
// delete everything first
  (this) ->  clear ();
  delete ((this) -> dict);
  delete ((this) -> lruList);
}
/*!
  \internal
  Cannot assign a cache.
*/

QGCache &QGCache::operator=(const class ::QGCache &)
{
#if defined(CHECK_NULL)
  qFatal("QGCache::operator=: Cannot copy a cache");
#endif
// satisfy the compiler
  return  *(this);
}
/*!
  \fn uint QGCache::count() const
  \internal
  Returns the number of items in the cache.
*/
/*!
  \fn uint QGCache::size() const
  \internal
  Returns the size of the hash array.
*/
/*!
  \fn int QGCache::maxCost() const
  \internal
  Returns the maximum cache cost.
*/
/*!
  \fn int QGCache::totalCost() const
  \internal
  Returns the total cache cost.
*/
/*!
  \internal
  Sets the maximum cache cost.
*/

void QGCache::setMaxCost(int maxCost)
{
  if (maxCost < (this) -> tCost) {
// remove excess cost
    if (!(this) ->  makeRoomFor ((this) -> tCost - maxCost)) {
      return ;
    }
  }
  (this) -> mCost = maxCost;
}
/*!
  \internal
  Inserts an item into the cache.
  \warning If this function returns FALSE, you must delete \a data
  yourself.  Additionally, be very careful about using \a data after
  calling this function, as any other insertions into the cache, from
  anywhere in the application, or within Qt itself, could cause the
  data to be discarded from the cache, and the pointer to become
  invalid.
*/

bool QGCache::insert_string(const class QString &key,Item data,int cost,int priority)
{
  if ((this) -> tCost + cost > (this) -> mCost) {
    if (!(this) ->  makeRoomFor ((this) -> tCost + cost - (this) -> mCost,priority)) {
#if defined(DEBUG)
      (this) -> lruList -> QCList::insertMisses++;
#endif
      return FALSE;
    }
  }
#if defined(DEBUG)
  if (!(((this) -> keytype) == StringKey)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","keytype == StringKey","qgcache.cpp",360);
  }
  (this) -> lruList -> QCList::inserts++;
  (this) -> lruList -> QCList::insertCosts += cost;
#endif
  if (priority < -32768) {
    priority = -32768;
  }
  else {
    if (priority > 32767) {
      priority = 32677;
    }
  }
  struct QCacheItem *ci = new QCacheItem ((new QString (key)),(this) ->  newItem (data),cost,((short )priority));
  qt_check_pointer(ci == 0,"qgcache.cpp",370);
  (this) -> lruList ->  insert (0,ci);
  (this) -> dict ->  insert_string (key,ci);
  (this) -> tCost += cost;
  return TRUE;
}
/*! \internal */

bool QGCache::insert_other(const char *key,Item data,int cost,int priority)
{
  if ((this) -> tCost + cost > (this) -> mCost) {
    if (!(this) ->  makeRoomFor ((this) -> tCost + cost - (this) -> mCost,priority)) {
#if defined(DEBUG)
      (this) -> lruList -> QCList::insertMisses++;
#endif
      return FALSE;
    }
  }
#if defined(DEBUG)
  if (!(((this) -> keytype) != StringKey)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","keytype != StringKey","qgcache.cpp",392);
  }
  (this) -> lruList -> QCList::inserts++;
  (this) -> lruList -> QCList::insertCosts += cost;
#endif
  if (((this) -> keytype) == AsciiKey && (this) -> copyk) {
    key = (qstrdup(key));
  }
  if (priority < -32768) {
    priority = -32768;
  }
  else {
    if (priority > 32767) {
      priority = 32677;
    }
  }
  struct QCacheItem *ci = new QCacheItem (((void *)key),(this) ->  newItem (data),cost,((short )priority));
  qt_check_pointer(ci == 0,"qgcache.cpp",404);
  (this) -> lruList ->  insert (0,ci);
  if (((this) -> keytype) == AsciiKey) {
    (this) -> dict ->  insert_ascii (key,ci);
  }
  else {
    (this) -> dict ->  insert_int (((intptr_t )key),ci);
  }
  (this) -> tCost += cost;
  return TRUE;
}
/*!
  \internal
  Removes an item from the cache.
*/

bool QGCache::remove_string(const class QString &key)
{
  Item d = (this) ->  take_string (key);
  if (d) {
    (this) ->  deleteItem (d);
  }
  return d != 0;
}
/*! \internal */

bool QGCache::remove_other(const char *key)
{
  Item d = (this) ->  take_other (key);
  if (d) {
    (this) ->  deleteItem (d);
  }
  return d != 0;
}
/*!
  \internal
  Takes an item out of the cache (no delete).
*/

QCollection::Item QGCache::take_string(const class QString &key)
{
// take from dict
  struct QCacheItem *ci = (this) -> dict ->  take_string (key);
  Item d;
  if (ci) {
    d = ci -> QCacheItem::data;
    (this) -> tCost -= ci -> QCacheItem::cost;
// take from list
    (this) -> lruList ->  take (ci);
    delete ((class QString *)(ci -> QCacheItem::key));
    delete ci;
  }
  else {
    d = 0;
  }
  return d;
}
/*!
  \internal
  Takes an item out of the cache (no delete).
*/

QCollection::Item QGCache::take_other(const char *key)
{
  struct QCacheItem *ci;
  if (((this) -> keytype) == AsciiKey) {
    ci = (this) -> dict ->  take_ascii (key);
  }
  else {
    ci = (this) -> dict ->  take_int (((intptr_t )key));
  }
  Item d;
  if (ci) {
    d = ci -> QCacheItem::data;
    (this) -> tCost -= ci -> QCacheItem::cost;
// take from list
    (this) -> lruList ->  take (ci);
    if ((this) -> copyk) {
      delete []((char *)(ci -> QCacheItem::key));
    }
    delete ci;
  }
  else {
    d = 0;
  }
  return d;
}
/*!
  \internal
  Clears the cache.
*/

void QGCache::clear()
{
  struct QCacheItem *ci;
  while((ci = (this) -> lruList ->  first ())){
    switch(((this) -> keytype)){
      case StringKey:
{
        (this) -> dict ->  remove_string (ci);
        delete ((class QString *)(ci -> QCacheItem::key));
        break; 
      }
      case AsciiKey:
{
        (this) -> dict ->  remove_ascii (ci);
        if ((this) -> copyk) {
          delete []((char *)(ci -> QCacheItem::key));
        }
        break; 
      }
      case IntKey:
{
        (this) -> dict ->  remove_int (ci);
        break; 
      }
      case PtrKey:
// unused
      break; 
    }
// delete data
    (this) ->  deleteItem (ci -> QCacheItem::data);
// remove from list
    (this) -> lruList ->  removeFirst ();
  }
  (this) -> tCost = 0;
}
/*!
  \internal
  Finds an item in the cache.
*/

QCollection::Item QGCache::find_string(const class QString &key,bool ref) const
{
  struct QCacheItem *ci = ((this) -> dict) ->  find_string (key);
#if defined(DEBUG)
  (this) -> lruList -> QCList::finds++;
#endif
  if (ci) {
#if defined(DEBUG)
    (this) -> lruList -> QCList::hits++;
    (this) -> lruList -> QCList::hitCosts += ci -> QCacheItem::cost;
#endif
    if (ref) {
      (this) -> lruList ->  reference (ci);
    }
    return ci -> QCacheItem::data;
  }
  return 0;
}
/*!
  \internal
  Finds an item in the cache.
*/

QCollection::Item QGCache::find_other(const char *key,bool ref) const
{
  struct QCacheItem *ci = ((this) -> keytype) == AsciiKey?((this) -> dict) ->  find_ascii (key) : ((this) -> dict) ->  find_int (((intptr_t )key));
#if defined(DEBUG)
  (this) -> lruList -> QCList::finds++;
#endif
  if (ci) {
#if defined(DEBUG)
    (this) -> lruList -> QCList::hits++;
    (this) -> lruList -> QCList::hitCosts += ci -> QCacheItem::cost;
#endif
    if (ref) {
      (this) -> lruList ->  reference (ci);
    }
    return ci -> QCacheItem::data;
  }
  return 0;
}
/*!
  \internal
  Allocates cache space for one or more items.
*/

bool QGCache::makeRoomFor(int cost,int priority)
{
// cannot make room for more
  if (cost > (this) -> mCost) {
//   than maximum cost
    return FALSE;
  }
  if (priority == -1) {
    priority = 32767;
  }
  register struct QCacheItem *ci = (this) -> lruList ->  last ();
  int cntCost = 0;
// number of items to dump
  int dumps = 0;
  while(cntCost < cost && ci && (ci -> QCacheItem::skipPriority) <= priority){
    cntCost += ci -> QCacheItem::cost;
    ci = (this) -> lruList ->  prev ();
    dumps++;
  }
// can enough cost be dumped?
  if (cntCost < cost) {
// no
    return FALSE;
  }
#if defined(DEBUG)
  if (!(dumps > 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","dumps > 0","qgcache.cpp",591);
  }
#endif
  while((dumps--)){
    ci = (this) -> lruList ->  last ();
#if defined(DEBUG)
    (this) -> lruList -> QCList::dumps++;
    (this) -> lruList -> QCList::dumpCosts += ci -> QCacheItem::cost;
#endif
    switch(((this) -> keytype)){
      case StringKey:
{
        (this) -> dict ->  remove_string (ci);
        delete ((class QString *)(ci -> QCacheItem::key));
        break; 
      }
      case AsciiKey:
{
        (this) -> dict ->  remove_ascii (ci);
        if ((this) -> copyk) {
          delete []((char *)(ci -> QCacheItem::key));
        }
        break; 
      }
      case IntKey:
{
        (this) -> dict ->  remove_int (ci);
        break; 
      }
      case PtrKey:
// unused
      break; 
    }
// delete data
    (this) ->  deleteItem (ci -> QCacheItem::data);
// remove from list
    (this) -> lruList ->  removeLast ();
  }
  (this) -> tCost -= cntCost;
  return TRUE;
}
/*!
  \internal
  Outputs debug statistics.
*/

void QGCache::statistics() const
{
#if defined(DEBUG)
  class QString line;
  line .  fill ('*',80);
  qDebug("%s",line .  ascii ());
  qDebug("CACHE STATISTICS:");
  qDebug("cache contains %d item%s, with a total cost of %d",(this) ->  count (),((this) ->  count () != 1?"s" : ""),(this) -> tCost);
  qDebug("maximum cost is %d, cache is %d%% full.",(this) -> mCost,(200 * (this) -> tCost + (this) -> mCost) / ((this) -> mCost * 2));
  qDebug("find() has been called %d time%s",(this) -> lruList -> QCList::finds,((this) -> lruList -> QCList::finds != 1?"s" : ""));
  qDebug("%d of these were hits, items found had a total cost of %d.",(this) -> lruList -> QCList::hits,(this) -> lruList -> QCList::hitCosts);
  qDebug("%d item%s %s been inserted with a total cost of %d.",(this) -> lruList -> QCList::inserts,((this) -> lruList -> QCList::inserts != 1?"s" : ""),((this) -> lruList -> QCList::inserts != 1?"have" : "has"),(this) -> lruList -> QCList::insertCosts);
  qDebug("%d item%s %s too large or had too low priority to be inserted.",(this) -> lruList -> QCList::insertMisses,((this) -> lruList -> QCList::insertMisses != 1?"s" : ""),((this) -> lruList -> QCList::insertMisses != 1?"were" : "was"));
  qDebug("%d item%s %s been thrown away with a total cost of %d.",(this) -> lruList -> QCList::dumps,((this) -> lruList -> QCList::dumps != 1?"s" : ""),((this) -> lruList -> QCList::dumps != 1?"have" : "has"),(this) -> lruList -> QCList::dumpCosts);
  qDebug("Statistics from internal dictionary class:");
  (this) -> dict ->  statistics ();
  qDebug("%s",line .  ascii ());
#endif
}

int QGCache::hits() const
{
  return (this) -> lruList -> QCList::hits;
}

int QGCache::misses() const
{
  return (this) -> lruList -> QCList::finds - (this) -> lruList -> QCList::hits;
}
/*****************************************************************************
  QGCacheIterator member functions
 *****************************************************************************/
/*!
  \class QGCacheIterator qgcache.h
  \brief An internal class for implementing QCacheIterator and QIntCacheIterator.
  QGCacheIterator is a strictly internal class that does the heavy work for
  QCacheIterator and QIntCacheIterator.
*/
/*!
  \internal
  Constructs an iterator that operates on the cache \e c.
*/

QGCacheIterator::QGCacheIterator(const class QGCache &c)
{
  (this) -> it = (new QCListIt (c . QGCache::lruList));
#if defined(DEBUG)
  if (!((this) -> it != 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","it != 0","qgcache.cpp",691);
  }
#endif
}
/*!
  \internal
  Constructs an iterator that operates on the same cache as \e ci.
*/

QGCacheIterator::QGCacheIterator(const class ::QGCacheIterator &ci)
{
  (this) -> it = (new QCListIt (ci . it));
#if defined(DEBUG)
  if (!((this) -> it != 0)) {
    qWarning("ASSERT: \"%s\" in %s (%d)","it != 0","qgcache.cpp",704);
  }
#endif
}
/*!
  \internal
  Destroys the iterator.
*/

QGCacheIterator::~QGCacheIterator()
{
  delete ((this) -> it);
}
/*!
  \internal
  Assigns the iterator \e ci to this cache iterator.
*/

QGCacheIterator &QGCacheIterator::operator=(const class ::QGCacheIterator &ci)
{
  ( *(this) -> it) =  *ci . it;
  return  *(this);
}
/*!
  \internal
  Returns the number of items in the cache.
*/

uint QGCacheIterator::count() const
{
  return ((this) -> it) ->  count ();
}
/*!
  \internal
  Returns TRUE if the iterator points to the first item.
*/

bool QGCacheIterator::atFirst() const
{
  return ((this) -> it) ->  atFirst ();
}
/*!
  \internal
  Returns TRUE if the iterator points to the last item.
*/

bool QGCacheIterator::atLast() const
{
  return ((this) -> it) ->  atLast ();
}
/*!
  \internal
  Sets the list iterator to point to the first item in the cache.
*/

QCollection::Item QGCacheIterator::toFirst()
{
  struct QCacheItem *item = ((this) -> it) ->  toFirst ();
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Sets the list iterator to point to the last item in the cache.
*/

QCollection::Item QGCacheIterator::toLast()
{
  struct QCacheItem *item = ((this) -> it) ->  toLast ();
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Returns the current item.
*/

QCollection::Item QGCacheIterator::get() const
{
  struct QCacheItem *item = ((this) -> it) ->  current ();
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Returns the key of the current item.
*/

QString QGCacheIterator::getKeyString() const
{
  struct QCacheItem *item = ((this) -> it) ->  current ();
  return item?( *((class QString *)(item -> QCacheItem::key))) : (QString::null);
}
/*!
  \internal
  Returns the key of the current item, as a \0-terminated C string.
*/

const char *QGCacheIterator::getKeyAscii() const
{
  struct QCacheItem *item = ((this) -> it) ->  current ();
  return item?((const char *)(item -> QCacheItem::key)) : 0;
}
/*!
  \internal
  Returns the key of the current item, as a long.
*/

intptr_t QGCacheIterator::getKeyInt() const
{
  struct QCacheItem *item = ((this) -> it) ->  current ();
  return item?((intptr_t )(item -> QCacheItem::key)) : 0;
}
/*!
  \internal
  Moves to the next item (postfix).
*/

QCollection::Item QGCacheIterator::operator()()
{
  struct QCacheItem *item = ((this) -> it) ->  operator() ();
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Moves to the next item (prefix).
*/

QCollection::Item QGCacheIterator::operator++()
{
  struct QCacheItem *item = ((this) -> it) ->  operator++ ();
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Moves \e jumps positions forward.
*/

QCollection::Item QGCacheIterator::operator+=(uint jump)
{
  struct QCacheItem *item = ((this) -> it) ->  operator+= (jump);
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Moves to the previous item (prefix).
*/

QCollection::Item QGCacheIterator::operator--()
{
  struct QCacheItem *item = ((this) -> it) ->  operator-- ();
  return item?item -> QCacheItem::data : 0;
}
/*!
  \internal
  Moves \e jumps positions backward.
*/

QCollection::Item QGCacheIterator::operator-=(uint jump)
{
  struct QCacheItem *item = ((this) -> it) ->  operator-= (jump);
  return item?item -> QCacheItem::data : 0;
}
