/****************************************************************************
** 
**
** Implementation of QGDict and QGDictIterator classes
**
** Created : 920529
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#include "qgdict.h"
#include "qlist.h"
#include "qstring.h"
#include "qdatastream.h"
#include <ctype.h>
// NOT REVISED
/*!
  \class QGDict qgdict.h
  \brief The QGDict class is an internal class for implementing QDict template classes.
  QGDict is a strictly internal class that acts as a base class for the
  \link collection.html collection classes\endlink QDict and QIntDict.
  QGDict has some virtual functions that can be reimplemented to customize
  the subclasses.
  <ul>
  <li> read() reads a collection/dictionary item from a QDataStream.
  <li> write() writes a collection/dictionary item to a QDataStream.
  </ul>
  Normally, you do not have to reimplement any of these functions.
*/
static const int op_find = 0;
static const int op_insert = 1;
static const int op_replace = 2;

class QGDItList : public QList < QGDictIterator > 
{
  

  public: inline QGDItList() : QList < QGDictIterator  > ()
{
  }
  

  inline QGDItList(const class ::QGDItList &list) : QList < QGDictIterator  > (list)
{
  }
  

  virtual inline ~QGDItList()
{
    (this) ->  clear ();
  }
  

  inline ::QGDItList &operator=(const class ::QGDItList &list)
{
    return (class ::QGDItList &)((this) -> QList< QGDictIterator > :: operator= (list));
  }
}
;
/*****************************************************************************
  Default implementation of special and virtual functions
 *****************************************************************************/
/*!
  \internal
  Returns the hash key for \e key, when key is a string.
*/

int QGDict::hashKeyString(const class QString &key)
{
#if defined(CHECK_NULL)
  if (key .  isNull ()) {
    qWarning("QGDict::hashStringKey: Invalid null key");
  }
#endif
  int i;
  register uint h = 0;
  uint g;
  int len = (key .  length ());
  const class QChar *p = key .  unicode ();
// case sensitive
  if (((this) -> cases)) {
    for (i = 0; i < len; i++) {
      h = (h << 4) + (p[i] .  cell ());
      if ((g = h & 0xf0000000)) {
        h ^= g >> 24;
      }
      h &= ~g;
    }
// case insensitive
  }
  else {
    for (i = 0; i < len; i++) {
      h = (h << 4) + (p[i] .  lower () .  cell ());
      if ((g = h & 0xf0000000)) {
        h ^= g >> 24;
      }
      h &= ~g;
    }
  }
  int index = h;
// adjust index to table size
  if (index < 0) {
    index = -index;
  }
  return index;
}
/*!
  \internal
  Returns the hash key for \a key, which is a C string.
*/

int QGDict::hashKeyAscii(const char *key)
{
#if defined(CHECK_NULL)
  if (key == 0) {
    qWarning("QGDict::hashAsciiKey: Invalid null key");
    return 0;
  }
#endif
  register const char *k = key;
  register uint h = 0;
  uint g;
// case sensitive
  if (((this) -> cases)) {
    while(( *k)){
      h = (h << 4) + ( *(k++));
      if ((g = h & 0xf0000000)) {
        h ^= g >> 24;
      }
      h &= ~g;
    }
// case insensitive
  }
  else {
    while(( *k)){
      h = (h << 4) + (tolower(( *k)));
      if ((g = h & 0xf0000000)) {
        h ^= g >> 24;
      }
      h &= ~g;
      k++;
    }
  }
  int index = h;
// adjust index to table size
  if (index < 0) {
    index = -index;
  }
  return index;
}
#ifndef QT_NO_DATASTREAM
/*!
  Reads a collection/dictionary item from the stream \e s and returns a
  reference to the stream.
  The default implementation sets \e item to 0.
  \sa write()
*/

QDataStream &QGDict::read(class QDataStream &s,Item &item)
{
  item = 0;
  return s;
}
/*!
  Writes a collection/dictionary item to the stream \e s and returns a
  reference to the stream.
  \sa read()
*/

QDataStream &QGDict::write(class QDataStream &s,Item ) const
{
  return s;
}
#endif //QT_NO_DATASTREAM
/*****************************************************************************
  QGDict member functions
 *****************************************************************************/
/*!
  \internal
  Constructs a dictionary.
*/

QGDict::QGDict(uint len,enum KeyType kt,bool caseSensitive,bool copyKeys)
{
  (this) ->  init (len,kt,caseSensitive,copyKeys);
}

void QGDict::init(uint len,enum KeyType kt,bool caseSensitive,bool copyKeys)
{
// allocate hash table
  (this) -> vec = (new QBaseBucket *[((this) -> vlen = len) * 8UL]);
  qt_check_pointer((this) -> vec == 0,"qgdict.cpp",205);
  memset(((char *)((this) -> vec)),0,((this) -> vlen) * sizeof(class QBaseBucket *));
  (this) -> numItems = 0;
  (this) -> iterators = 0;
// The caseSensitive and copyKey options don't make sense for
// all dict types.
  switch((this) -> keytype = ((uint )kt)){
    case StringKey:
{
      (this) -> cases = caseSensitive;
      (this) -> copyk = FALSE;
      break; 
    }
    case AsciiKey:
{
      (this) -> cases = caseSensitive;
      (this) -> copyk = copyKeys;
      break; 
    }
    default:
{
      (this) -> cases = FALSE;
      (this) -> copyk = FALSE;
      break; 
    }
  }
}
/*!
  \internal
  Constructs a copy of \e dict.
*/

QGDict::QGDict(const class ::QGDict &dict) : QCollection(dict)
{
  (this) ->  init (dict . vlen,((enum KeyType )dict . keytype),dict . cases,dict . copyk);
  class QGDictIterator it(dict);
// copy from other dict
  while((it .  get ())){
    switch(((this) -> keytype)){
      case StringKey:
{
        (this) ->  look_string (it .  getKeyString (),it .  get (),op_insert);
        break; 
      }
      case AsciiKey:
{
        (this) ->  look_ascii (it .  getKeyAscii (),it .  get (),op_insert);
        break; 
      }
      case IntKey:
{
        (this) ->  look_int (it .  getKeyInt (),it .  get (),op_insert);
        break; 
      }
      case PtrKey:
{
        (this) ->  look_ptr (it .  getKeyPtr (),it .  get (),op_insert);
        break; 
      }
    }
     ++ it;
  }
}
/*!
  \internal
  Removes all items from the dictionary and destroys it.
*/

QGDict::~QGDict()
{
// delete everything
  (this) ->  clear ();
  delete []((this) -> vec);
// no iterators for this dict
  if (!((this) -> iterators)) {
    return ;
  }
  class QGDictIterator *i = ((this) -> iterators) ->  first ();
// notify all iterators that
  while(i){
// this dict is deleted
    i -> QGDictIterator::dict = 0;
    i = ((this) -> iterators) ->  next ();
  }
  delete ((this) -> iterators);
}
/*!
  \internal
  Assigns \e dict to this dictionary.
*/

QGDict &QGDict::operator=(const class ::QGDict &dict)
{
  (this) ->  clear ();
  class QGDictIterator it(dict);
// copy from other dict
  while((it .  get ())){
    switch(((this) -> keytype)){
      case StringKey:
{
        (this) ->  look_string (it .  getKeyString (),it .  get (),op_insert);
        break; 
      }
      case AsciiKey:
{
        (this) ->  look_ascii (it .  getKeyAscii (),it .  get (),op_insert);
        break; 
      }
      case IntKey:
{
        (this) ->  look_int (it .  getKeyInt (),it .  get (),op_insert);
        break; 
      }
      case PtrKey:
{
        (this) ->  look_ptr (it .  getKeyPtr (),it .  get (),op_insert);
        break; 
      }
    }
     ++ it;
  }
  return  *(this);
}
/*! \fn QCollection::Item QGDictIterator::get() const
  \internal
*/
/*! \fn QString QGDictIterator::getKeyString() const
  \internal
*/
/*! \fn const char * QGDictIterator::getKeyAscii() const
  \internal
*/
/*! \fn void * QGDictIterator::getKeyPtr() const
  \internal
*/
/*! \fn long QGDictIterator::getKeyInt() const
  \internal
*/
/*!
  \fn uint QGDict::count() const
  \internal
  Returns the number of items in the dictionary.
*/
/*!
  \fn uint QGDict::size() const
  \internal
  Returns the size of the hash array.
*/
/*!
  \internal
  The do-it-all function; op is one of op_find, op_insert, op_replace
*/

QCollection::Item QGDict::look_string(const class QString &key,Item d,int op)
{
  class QStringBucket *n;
  int index = (((this) ->  hashKeyString (key)) % (this) -> vlen);
// find
  if (op == op_find) {
    if (((this) -> cases)) {
      for (n = ((class QStringBucket *)(this) -> vec[index]); n; n = ((class QStringBucket *)(n ->  getNext ()))) {
        if (key==n ->  getKey ()) {
// item found
          return n ->  getData ();
        }
      }
    }
    else {
      class QString k = key .  lower ();
      for (n = ((class QStringBucket *)(this) -> vec[index]); n; n = ((class QStringBucket *)(n ->  getNext ()))) {
        if (k==n ->  getKey () .  lower ()) {
// item found
          return n ->  getData ();
        }
      }
    }
// not found
    return 0;
  }
// replace
  if (op == op_replace) {
// maybe something there
    if ((this) -> vec[index] != 0) {
      (this) ->  remove_string (key);
    }
  }
// op_insert or op_replace
  n = (new QStringBucket (key,(this) ->  newItem (d),(this) -> vec[index]));
  qt_check_pointer(n == 0,"qgdict.cpp",383);
#if defined(CHECK_NULL)
  if (n ->  getData () == 0) {
    qWarning("QDict: Cannot insert null item");
  }
#endif
  (this) -> vec[index] = n;
  (this) -> numItems++;
  return n ->  getData ();
}
/*!  \internal */

QCollection::Item QGDict::look_ascii(const char *key,Item d,int op)
{
  class QAsciiBucket *n;
  int index = (((this) ->  hashKeyAscii (key)) % (this) -> vlen);
// find
  if (op == op_find) {
    if (((this) -> cases)) {
      for (n = ((class QAsciiBucket *)(this) -> vec[index]); n; n = ((class QAsciiBucket *)(n ->  getNext ()))) {
        if (qstrcmp(n ->  getKey (),key) == 0) {
// item found
          return n ->  getData ();
        }
      }
    }
    else {
      for (n = ((class QAsciiBucket *)(this) -> vec[index]); n; n = ((class QAsciiBucket *)(n ->  getNext ()))) {
        if (qstricmp(n ->  getKey (),key) == 0) {
// item found
          return n ->  getData ();
        }
      }
    }
// not found
    return 0;
  }
// replace
  if (op == op_replace) {
// maybe something there
    if ((this) -> vec[index] != 0) {
      (this) ->  remove_ascii (key);
    }
  }
// op_insert or op_replace
  n = (new QAsciiBucket ((((this) -> copyk)?(qstrdup(key)) : key),(this) ->  newItem (d),(this) -> vec[index]));
  qt_check_pointer(n == 0,"qgdict.cpp",422);
#if defined(CHECK_NULL)
  if (n ->  getData () == 0) {
    qWarning("QAsciiDict: Cannot insert null item");
  }
#endif
  (this) -> vec[index] = n;
  (this) -> numItems++;
  return n ->  getData ();
}
/*!  \internal */

QCollection::Item QGDict::look_int(long key,Item d,int op)
{
  class QIntBucket *n;
// simple hash
  int index = (int )(((ulong )key) % ((this) -> vlen));
// find
  if (op == op_find) {
    for (n = ((class QIntBucket *)(this) -> vec[index]); n; n = ((class QIntBucket *)(n ->  getNext ()))) {
      if (n ->  getKey () == key) {
// item found
        return n ->  getData ();
      }
    }
// not found
    return 0;
  }
// replace
  if (op == op_replace) {
// maybe something there
    if ((this) -> vec[index] != 0) {
      (this) ->  remove_int (key);
    }
  }
// op_insert or op_replace
  n = (new QIntBucket (key,(this) ->  newItem (d),(this) -> vec[index]));
  qt_check_pointer(n == 0,"qgdict.cpp",453);
#if defined(CHECK_NULL)
  if (n ->  getData () == 0) {
    qWarning("QIntDict: Cannot insert null item");
  }
#endif
  (this) -> vec[index] = n;
  (this) -> numItems++;
  return n ->  getData ();
}
/*!  \internal */

QCollection::Item QGDict::look_ptr(void *key,Item d,int op)
{
  class QPtrBucket *n;
// simple hash
  int index = (int )(((uintptr_t )key) % ((this) -> vlen));
// find
  if (op == op_find) {
    for (n = ((class QPtrBucket *)(this) -> vec[index]); n; n = ((class QPtrBucket *)(n ->  getNext ()))) {
      if (n ->  getKey () == key) {
// item found
        return n ->  getData ();
      }
    }
// not found
    return 0;
  }
// replace
  if (op == op_replace) {
// maybe something there
    if ((this) -> vec[index] != 0) {
      (this) ->  remove_ptr (key);
    }
  }
// op_insert or op_replace
  n = (new QPtrBucket (key,(this) ->  newItem (d),(this) -> vec[index]));
  qt_check_pointer(n == 0,"qgdict.cpp",484);
#if defined(CHECK_NULL)
  if (n ->  getData () == 0) {
    qWarning("QPtrDict: Cannot insert null item");
  }
#endif
  (this) -> vec[index] = n;
  (this) -> numItems++;
  return n ->  getData ();
}
/*!
  \internal
  Changes the size of the hashtable.
  The contents of the dictionary are preserved,
  but all iterators on the dictionary become invalid.
*/

void QGDict::resize(uint newsize)
{
// Save old information
  class QBaseBucket **old_vec = (this) -> vec;
  uint old_vlen = (this) -> vlen;
  bool old_copyk = ((this) -> copyk);
  (this) -> vec = (new QBaseBucket *[((this) -> vlen = newsize) * 8UL]);
  qt_check_pointer((this) -> vec == 0,"qgdict.cpp",509);
  memset(((char *)((this) -> vec)),0,((this) -> vlen) * sizeof(class QBaseBucket *));
  (this) -> numItems = 0;
  (this) -> copyk = FALSE;
// Reinsert every item from vec, deleting vec as we go
  for (uint index = 0; index < old_vlen; index++) {
    switch(((this) -> keytype)){
      case StringKey:
{
{
          class QStringBucket *n = (class QStringBucket *)old_vec[index];
          while(n){
            (this) ->  look_string (n ->  getKey (),n ->  getData (),op_insert);
            class QStringBucket *t = (class QStringBucket *)(n ->  getNext ());
            delete n;
            n = t;
          }
        }
        break; 
      }
      case AsciiKey:
{
{
          class QAsciiBucket *n = (class QAsciiBucket *)old_vec[index];
          while(n){
            (this) ->  look_ascii (n ->  getKey (),n ->  getData (),op_insert);
            class QAsciiBucket *t = (class QAsciiBucket *)(n ->  getNext ());
            delete n;
            n = t;
          }
        }
        break; 
      }
      case IntKey:
{
{
          class QIntBucket *n = (class QIntBucket *)old_vec[index];
          while(n){
            (this) ->  look_int (n ->  getKey (),n ->  getData (),op_insert);
            class QIntBucket *t = (class QIntBucket *)(n ->  getNext ());
            delete n;
            n = t;
          }
        }
        break; 
      }
      case PtrKey:
{
{
          class QPtrBucket *n = (class QPtrBucket *)old_vec[index];
          while(n){
            (this) ->  look_ptr (n ->  getKey (),n ->  getData (),op_insert);
            class QPtrBucket *t = (class QPtrBucket *)(n ->  getNext ());
            delete n;
            n = t;
          }
        }
        break; 
      }
    }
  }
  delete []old_vec;
// Restore state
  (this) -> copyk = old_copyk;
// Invalidate all iterators, since order is lost
  if (((this) -> iterators) && (((this) -> iterators) ->  count ())) {
    class QGDictIterator *i = ((this) -> iterators) ->  first ();
    while(i){
      i ->  toFirst ();
      i = ((this) -> iterators) ->  next ();
    }
  }
}
/*!
  \internal
  Unlinks the bucket with the specified key (and specified data pointer,
  if it is set).
*/

void QGDict::unlink_common(int index,class QBaseBucket *node,class QBaseBucket *prev)
{
// update iterators
  if (((this) -> iterators) && (((this) -> iterators) ->  count ())) {
    class QGDictIterator *i = ((this) -> iterators) ->  first ();
// invalidate all iterators
    while(i){
// referring to pending node
      if (i -> QGDictIterator::curNode == node) {
        i ->  operator++ ();
      }
      i = ((this) -> iterators) ->  next ();
    }
  }
// unlink node
  if (prev) {
    prev ->  setNext (node ->  getNext ());
  }
  else {
    (this) -> vec[index] = node ->  getNext ();
  }
  (this) -> numItems--;
}

QStringBucket *QGDict::unlink_string(const class QString &key,Item d)
{
// nothing in dictionary
  if ((this) -> numItems == 0) {
    return 0;
  }
  class QStringBucket *n;
  class QStringBucket *prev = 0;
  int index = (((this) ->  hashKeyString (key)) % (this) -> vlen);
  if (((this) -> cases)) {
    for (n = ((class QStringBucket *)(this) -> vec[index]); n; n = ((class QStringBucket *)(n ->  getNext ()))) {
      bool found = key==n ->  getKey ();
      if (found && d) {
        found = n ->  getData () == d;
      }
      if (found) {
        (this) ->  unlink_common (index,n,prev);
        return n;
      }
      prev = n;
    }
  }
  else {
    class QString k = key .  lower ();
    for (n = ((class QStringBucket *)(this) -> vec[index]); n; n = ((class QStringBucket *)(n ->  getNext ()))) {
      bool found = k==n ->  getKey () .  lower ();
      if (found && d) {
        found = n ->  getData () == d;
      }
      if (found) {
        (this) ->  unlink_common (index,n,prev);
        return n;
      }
      prev = n;
    }
  }
  return 0;
}

QAsciiBucket *QGDict::unlink_ascii(const char *key,Item d)
{
// nothing in dictionary
  if ((this) -> numItems == 0) {
    return 0;
  }
  class QAsciiBucket *n;
  class QAsciiBucket *prev = 0;
  int index = (((this) ->  hashKeyAscii (key)) % (this) -> vlen);
  for (n = ((class QAsciiBucket *)(this) -> vec[index]); n; n = ((class QAsciiBucket *)(n ->  getNext ()))) {
    bool found = ((((this) -> cases)?qstrcmp(n ->  getKey (),key) : qstricmp(n ->  getKey (),key))) == 0;
    if (found && d) {
      found = n ->  getData () == d;
    }
    if (found) {
      (this) ->  unlink_common (index,n,prev);
      return n;
    }
    prev = n;
  }
  return 0;
}

QIntBucket *QGDict::unlink_int(long key,Item d)
{
// nothing in dictionary
  if ((this) -> numItems == 0) {
    return 0;
  }
  class QIntBucket *n;
  class QIntBucket *prev = 0;
  int index = (int )(((ulong )key) % ((this) -> vlen));
  for (n = ((class QIntBucket *)(this) -> vec[index]); n; n = ((class QIntBucket *)(n ->  getNext ()))) {
    bool found = n ->  getKey () == key;
    if (found && d) {
      found = n ->  getData () == d;
    }
    if (found) {
      (this) ->  unlink_common (index,n,prev);
      return n;
    }
    prev = n;
  }
  return 0;
}

QPtrBucket *QGDict::unlink_ptr(void *key,Item d)
{
// nothing in dictionary
  if ((this) -> numItems == 0) {
    return 0;
  }
  class QPtrBucket *n;
  class QPtrBucket *prev = 0;
  int index = (int )(((uintptr_t )key) % ((this) -> vlen));
  for (n = ((class QPtrBucket *)(this) -> vec[index]); n; n = ((class QPtrBucket *)(n ->  getNext ()))) {
    bool found = n ->  getKey () == key;
    if (found && d) {
      found = n ->  getData () == d;
    }
    if (found) {
      (this) ->  unlink_common (index,n,prev);
      return n;
    }
    prev = n;
  }
  return 0;
}
/*!
  \internal
  Removes the item with the specified key.  If item is non-null,
  the remove will match the \a item as well (used to remove an
  item when several items have the same key).
*/

bool QGDict::remove_string(const class QString &key,Item item)
{
  class QStringBucket *n = (this) ->  unlink_string (key,item);
  if (n) {
    (this) ->  deleteItem (n ->  getData ());
    delete n;
    return TRUE;
  }
  else {
    return FALSE;
  }
}
/*!  \internal */

bool QGDict::remove_ascii(const char *key,Item item)
{
  class QAsciiBucket *n = (this) ->  unlink_ascii (key,item);
  if (n) {
    if (((this) -> copyk)) {
      delete []((char *)(n ->  getKey ()));
    }
    (this) ->  deleteItem (n ->  getData ());
    delete n;
  }
  return n != 0;
}
/*!  \internal */

bool QGDict::remove_int(long key,Item item)
{
  class QIntBucket *n = (this) ->  unlink_int (key,item);
  if (n) {
    (this) ->  deleteItem (n ->  getData ());
    delete n;
  }
  return n != 0;
}
/*!  \internal */

bool QGDict::remove_ptr(void *key,Item item)
{
  class QPtrBucket *n = (this) ->  unlink_ptr (key,item);
  if (n) {
    (this) ->  deleteItem (n ->  getData ());
    delete n;
  }
  return n != 0;
}
/*!  \internal */

QCollection::Item QGDict::take_string(const class QString &key)
{
  class QStringBucket *n = (this) ->  unlink_string (key);
  Item d;
  if (n) {
    d = n ->  getData ();
    delete n;
  }
  else {
    d = 0;
  }
  return d;
}
/*!  \internal */

QCollection::Item QGDict::take_ascii(const char *key)
{
  class QAsciiBucket *n = (this) ->  unlink_ascii (key);
  Item d;
  if (n) {
    if (((this) -> copyk)) {
      delete []((char *)(n ->  getKey ()));
    }
    d = n ->  getData ();
    delete n;
  }
  else {
    d = 0;
  }
  return d;
}
/*!  \internal */

QCollection::Item QGDict::take_int(long key)
{
  class QIntBucket *n = (this) ->  unlink_int (key);
  Item d;
  if (n) {
    d = n ->  getData ();
    delete n;
  }
  else {
    d = 0;
  }
  return d;
}
/*!  \internal */

QCollection::Item QGDict::take_ptr(void *key)
{
  class QPtrBucket *n = (this) ->  unlink_ptr (key);
  Item d;
  if (n) {
    d = n ->  getData ();
    delete n;
  }
  else {
    d = 0;
  }
  return d;
}
/*!
  \internal
  Removes all items from the dictionary.
*/

void QGDict::clear()
{
  if (!((this) -> numItems)) {
    return ;
  }
// disable remove() function
  (this) -> numItems = 0;
// destroy hash table
  for (uint j = 0; j < (this) -> vlen; j++) {
    if ((this) -> vec[j]) {
      switch(((this) -> keytype)){
        case StringKey:
{
{
            class QStringBucket *n = (class QStringBucket *)(this) -> vec[j];
            while(n){
              class QStringBucket *next = (class QStringBucket *)(n ->  getNext ());
              (this) ->  deleteItem (n ->  getData ());
              delete n;
              n = next;
            }
          }
          break; 
        }
        case AsciiKey:
{
{
            class QAsciiBucket *n = (class QAsciiBucket *)(this) -> vec[j];
            while(n){
              class QAsciiBucket *next = (class QAsciiBucket *)(n ->  getNext ());
              if (((this) -> copyk)) {
                delete []((char *)(n ->  getKey ()));
              }
              (this) ->  deleteItem (n ->  getData ());
              delete n;
              n = next;
            }
          }
          break; 
        }
        case IntKey:
{
{
            class QIntBucket *n = (class QIntBucket *)(this) -> vec[j];
            while(n){
              class QIntBucket *next = (class QIntBucket *)(n ->  getNext ());
              (this) ->  deleteItem (n ->  getData ());
              delete n;
              n = next;
            }
          }
          break; 
        }
        case PtrKey:
{
{
            class QPtrBucket *n = (class QPtrBucket *)(this) -> vec[j];
            while(n){
              class QPtrBucket *next = (class QPtrBucket *)(n ->  getNext ());
              (this) ->  deleteItem (n ->  getData ());
              delete n;
              n = next;
            }
          }
          break; 
        }
      }
// detach list of buckets
      (this) -> vec[j] = 0;
    }
  }
// invalidate all iterators
  if (((this) -> iterators) && (((this) -> iterators) ->  count ())) {
    class QGDictIterator *i = ((this) -> iterators) ->  first ();
    while(i){
      i -> QGDictIterator::curNode = 0;
      i = ((this) -> iterators) ->  next ();
    }
  }
}
/*!
  \internal
  Outputs debug statistics.
*/

void QGDict::statistics() const
{
#if defined(DEBUG)
  class QString line;
  line .  fill ('-',60);
  double real;
  double ideal;
  qDebug("%s",line .  ascii ());
  qDebug("DICTIONARY STATISTICS:");
  if ((this) ->  count () == 0) {
    qDebug("Empty!");
    qDebug("%s",line .  ascii ());
    return ;
  }
  real = 0.0;
  ideal = ((float )((this) ->  count ())) / (2.0 * ((this) ->  size ())) * (((this) ->  count ()) + 2.0 * ((this) ->  size ()) - 1);
  uint i = 0;
  while(i < (this) ->  size ()){
    class QBaseBucket *n = (this) -> vec[i];
    int b = 0;
// count number of buckets
    while(n){
      b++;
      n = n ->  getNext ();
    }
    real = real + ((double )b) * (((double )b) + 1.0) / 2.0;
    char buf[80];
    char *pbuf;
    if (b > 78) {
      b = 78;
    }
    pbuf = buf;
    while((b--))
       *(pbuf++) = '*';
     *pbuf = '\0';
    qDebug("%s",buf);
    i++;
  }
  qDebug("Array size = %d",(this) ->  size ());
  qDebug("# items    = %d",(this) ->  count ());
  qDebug("Real dist  = %g",real);
  qDebug("Rand dist  = %g",ideal);
  qDebug("Real/Rand  = %g",real / ideal);
  qDebug("%s",line .  ascii ());
#endif // DEBUG
}
/*****************************************************************************
  QGDict stream functions
 *****************************************************************************/
#ifndef QT_NO_DATASTREAM

class QDataStream &operator>>(class QDataStream &s,class QGDict &dict)
{
  return dict .  read (s);
}

class QDataStream &operator<<(class QDataStream &s,const class QGDict &dict)
{
  return dict .  write (s);
}
#if defined(_CC_DEC_) && defined(__alpha) && (__DECCXX_VER >= 50190001)
#endif
/*!
  \internal
  Reads a dictionary from the stream \e s.
*/

QDataStream &QGDict::read(class QDataStream &s)
{
  uint num;
// read number of items
  s >> num;
// clear dict
  (this) ->  clear ();
// read all items
  while((num--)){
    Item d;
    switch(((this) -> keytype)){
      case StringKey:
{
{
          class QString k;
          s>>k;
          (this) ->  read (s,d);
          (this) ->  look_string (k,d,op_insert);
        }
        break; 
      }
      case AsciiKey:
{
{
          char *k;
          s >> k;
          (this) ->  read (s,d);
          (this) ->  look_ascii (k,d,op_insert);
          if (((this) -> copyk)) {
            delete []k;
          }
        }
        break; 
      }
      case IntKey:
{
{
          Q_UINT32 k;
          s >> k;
          (this) ->  read (s,d);
          (this) ->  look_int (k,d,op_insert);
        }
        break; 
      }
      case PtrKey:
{
{
          Q_UINT32 k;
          s >> k;
          (this) ->  read (s,d);
// ### cannot insert 0 - this renders the thing
// useless since all pointers are written as 0,
// but hey, serializing pointers?  can it be done
// at all, ever?
          if (k) {
            (this) ->  look_ptr (((void *)((uintptr_t )k)),d,op_insert);
          }
        }
        break; 
      }
    }
  }
  return s;
}
/*!
  \internal
  Writes the dictionary to the stream \e s.
*/

QDataStream &QGDict::write(class QDataStream &s) const
{
// write number of items
  s << (this) ->  count ();
  uint i = 0;
  while(i < (this) ->  size ()){
    class QBaseBucket *n = (this) -> vec[i];
// write all buckets
    while(n){
      switch(((this) -> keytype)){
        case StringKey:
{
          s<<((class QStringBucket *)n) ->  getKey ();
          break; 
        }
        case AsciiKey:
{
          s << ((class QAsciiBucket *)n) ->  getKey ();
          break; 
        }
        case IntKey:
{
          s << ((Q_UINT32 )(((class QIntBucket *)n) ->  getKey ()));
          break; 
        }
        case PtrKey:
{
// ### cannot serialize a pointer
          s << ((Q_UINT32 )0);
          break; 
        }
      }
// write data
      (this) ->  write (s,n ->  getData ());
      n = n ->  getNext ();
    }
    i++;
  }
  return s;
}
#endif //QT_NO_DATASTREAM
/*****************************************************************************
  QGDictIterator member functions
 *****************************************************************************/
/*!
  \class QGDictIterator qgdict.h
  \brief An internal class for implementing QDictIterator and QIntDictIterator.
  QGDictIterator is a strictly internal class that does the heavy work for
  QDictIterator and QIntDictIterator.
*/
/*!
  \internal
  Constructs an iterator that operates on the dictionary \e d.
*/

QGDictIterator::QGDictIterator(const class QGDict &d)
{
// get reference to dict
  (this) -> dict = ((class QGDict *)(&d));
// set to first noe
  (this) ->  toFirst ();
  if (!((this) -> dict -> QGDict::iterators)) {
// create iterator list
    (this) -> dict -> QGDict::iterators = (new QGDItList ());
    qt_check_pointer((this) -> dict -> QGDict::iterators == 0,"qgdict.cpp",1081);
  }
// attach iterator to dict
  ((this) -> dict -> QGDict::iterators) ->  append ((this));
}
/*!
  \internal
  Constructs a copy of the iterator \e it.
*/

QGDictIterator::QGDictIterator(const class ::QGDictIterator &it)
{
  (this) -> dict = it . dict;
  (this) -> curNode = it . curNode;
  (this) -> curIndex = it . curIndex;
  if (((this) -> dict)) {
// attach iterator to dict
    ((this) -> dict -> QGDict::iterators) ->  append ((this));
  }
}
/*!
  \internal
  Assigns a copy of the iterator \e it and returns a reference to this
  iterator.
*/

QGDictIterator &QGDictIterator::operator=(const class ::QGDictIterator &it)
{
// detach from old dict
  if (((this) -> dict)) {
    ((this) -> dict -> QGDict::iterators) ->  removeRef ((this));
  }
  (this) -> dict = it . dict;
  (this) -> curNode = it . curNode;
  (this) -> curIndex = it . curIndex;
  if (((this) -> dict)) {
// attach to new list
    ((this) -> dict -> QGDict::iterators) ->  append ((this));
  }
  return  *(this);
}
/*!
  \internal
  Destroys the iterator.
*/

QGDictIterator::~QGDictIterator()
{
// detach iterator from dict
  if (((this) -> dict)) {
    ((this) -> dict -> QGDict::iterators) ->  removeRef ((this));
  }
}
/*!
  \internal
  Sets the iterator to point to the first item in the dictionary.
*/

QCollection::Item QGDictIterator::toFirst()
{
  if (!((this) -> dict)) {
#if defined(CHECK_NULL)
    qWarning("QGDictIterator::toFirst: Dictionary has been deleted");
#endif
    return 0;
  }
// empty dictionary
  if (((this) -> dict) ->  count () == 0) {
    (this) -> curNode = 0;
    return 0;
  }
  register uint i = 0;
  register class QBaseBucket **v = (this) -> dict -> QGDict::vec;
  while(!( *(v++)))
    i++;
  (this) -> curNode = (this) -> dict -> QGDict::vec[i];
  (this) -> curIndex = i;
  return (this) -> curNode ->  getData ();
}
/*!
  \internal
  Moves to the next item (postfix).
*/

QCollection::Item QGDictIterator::operator()()
{
  if (!((this) -> dict)) {
#if defined(CHECK_NULL)
    qWarning("QGDictIterator::operator(): Dictionary has been deleted");
#endif
    return 0;
  }
  if (!((this) -> curNode)) {
    return 0;
  }
  QCollection::Item d = (this) -> curNode ->  getData ();
  (this) ->  operator++ ();
  return d;
}
/*!
  \internal
  Moves to the next item (prefix).
*/

QCollection::Item QGDictIterator::operator++()
{
  if (!((this) -> dict)) {
#if defined(CHECK_NULL)
    qWarning("QGDictIterator::operator++: Dictionary has been deleted");
#endif
    return 0;
  }
  if (!((this) -> curNode)) {
    return 0;
  }
  (this) -> curNode = (this) -> curNode ->  getNext ();
// no next bucket
  if (!((this) -> curNode)) {
// look from next vec element
    register uint i = (this) -> curIndex + 1;
    register class QBaseBucket **v = &(this) -> dict -> QGDict::vec[i];
    while(i < ((this) -> dict) ->  size () && !( *(v++)))
      i++;
// nothing found
    if (i == ((this) -> dict) ->  size ()) {
      (this) -> curNode = 0;
      return 0;
    }
    (this) -> curNode = (this) -> dict -> QGDict::vec[i];
    (this) -> curIndex = i;
  }
  return (this) -> curNode ->  getData ();
}
/*!
  \internal
  Moves \e jumps positions forward.
*/

QCollection::Item QGDictIterator::operator+=(uint jumps)
{
  while(((this) -> curNode) && (jumps--))
    (this) ->  operator++ ();
  return ((this) -> curNode)?(this) -> curNode ->  getData () : 0;
}
