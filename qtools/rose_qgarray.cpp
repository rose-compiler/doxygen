/****************************************************************************
** 
**
** Implementation of QGArray class
**
** Created : 930906
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of the tools module of the Qt GUI Toolkit.
**
** This file may be distributed under the terms of the Q Public License
** as defined by Trolltech AS of Norway and appearing in the file
** LICENSE.QPL included in the packaging of this file.
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** Licensees holding valid Qt Enterprise Edition or Qt Professional Edition
** licenses may use this file in accordance with the Qt Commercial License
** Agreement provided with the Software.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.trolltech.com/pricing.html or email sales@trolltech.com for
**   information about Qt Commercial License Agreements.
** See http://www.trolltech.com/qpl/ for QPL licensing information.
** See http://www.trolltech.com/gpl/ for GPL licensing information.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
**********************************************************************/
#define	 QGARRAY_CPP
#include "qgarray.h"
#include "qstring.h"
#include <stdlib.h>
#define USE_MALLOC				// comment to use new/delete
#undef NEW
#undef DELETE
#if defined(USE_MALLOC)
#define NEW(type,size)	((type*)malloc(size*sizeof(type)))
#define DELETE(array)	(free((char*)array))
#else
#define NEW(type,size)	(new type[size])
#define DELETE(array)	(delete[] array)
#define DONT_USE_REALLOC			// comment to use realloc()
#endif
// NOT REVISED
/*!
  \class QShared qshared.h
  \brief The QShared struct is internally used for implementing shared classes.
  It only contains a reference count and member functions to increment and
  decrement it.
  Shared classes normally have internal classes that inherit QShared and
  add the shared data.
  \sa \link shclass.html Shared Classes\endlink
*/
/*!
  \class QGArray qgarray.h
  \brief The QGArray class is an internal class for implementing the QArray class.
  QGArray is a strictly internal class that acts as base class for the
  QArray template array.
  It contains an array of bytes and has no notion of an array element.
*/
/*!
  \internal
  Constructs a null array.
*/

QGArray::QGArray()
{
  (this) -> shd = (this) ->  newData ();
  qt_check_pointer((this) -> shd == 0,"qgarray.cpp",92);
}
/*!
  \internal
  Dummy constructor; does not allocate any data.
  This constructor does not initialize any array data so subclasses
  must do it. The intention is to make the code more efficient.
*/

QGArray::QGArray(int ,int )
{
}
/*!
  \internal
  Constructs an array with room for \e size bytes.
*/

QGArray::QGArray(int size)
{
  if (size < 0) {
#if defined(CHECK_RANGE)
    qWarning("QGArray: Cannot allocate array with negative length");
#endif
    size = 0;
  }
  (this) -> shd = (this) ->  newData ();
  qt_check_pointer((this) -> shd == 0,"qgarray.cpp",121);
// zero length
  if (size == 0) {
    return ;
  }
  (this) -> shd -> array_data::data = ((char *)(malloc(size * sizeof(char ))));
  qt_check_pointer((this) -> shd -> array_data::data == 0,"qgarray.cpp",125);
  (this) -> shd -> array_data::len = size;
}
/*!
  \internal
  Constructs a shallow copy of \e a.
*/

QGArray::QGArray(const class ::QGArray &a)
{
  (this) -> shd = a . shd;
  ((this) -> shd) ->  ref ();
}
/*!
  \internal
  Dereferences the array data and deletes it if this was the last
  reference.
*/

QGArray::~QGArray()
{
// delete when last reference
  if (((this) -> shd) && ((this) -> shd) ->  deref ()) {
// is lost
    if (((this) -> shd -> array_data::data)) {
      free(((char *)((this) -> shd -> array_data::data)));
    }
    (this) ->  deleteData ((this) -> shd);
  }
}
/*!
  \fn QGArray &QGArray::operator=( const QGArray &a )
  \internal
  Assigns a shallow copy of \e a to this array and returns a reference to
  this array.  Equivalent to assign().
*/
/*!
  \fn void QGArray::detach()
  \internal
  Detaches this array from shared array data.
*/
/*!
  \fn char *QGArray::data() const
  \internal
  Returns a pointer to the actual array data.
*/
/*!
  \fn uint QGArray::nrefs() const
  \internal
  Returns the reference count.
*/
/*!
  \fn uint QGArray::size() const
  \internal
  Returns the size of the array, in bytes.
*/
/*!
  \internal
  Returns TRUE if this array is equal to \e a, otherwise FALSE.
  The comparison is bitwise, of course.
*/

bool QGArray::isEqual(const class ::QGArray &a) const
{
// different size
  if ((this) ->  size () != a .  size ()) {
    return FALSE;
  }
// has same data
  if ((this) ->  data () == a .  data ()) {
    return TRUE;
  }
  return ((((this) ->  size ())?memcmp(((this) ->  data ()),(a .  data ()),((this) ->  size ())) : 0)) == 0;
}
/*!
  \internal
  Resizes the array to \e newsize bytes.
*/

bool QGArray::resize(uint newsize)
{
// nothing to do
  if (newsize == (this) -> shd -> array_data::len) {
    return TRUE;
  }
// remove array
  if (newsize == 0) {
    (this) ->  duplicate (0,0);
    return TRUE;
  }
// existing data
  if (((this) -> shd -> array_data::data)) {
#if defined(DONT_USE_REALLOC)
// manual realloc
#else
    (this) -> shd -> array_data::data = ((char *)(realloc(((this) -> shd -> array_data::data),newsize)));
#endif
  }
  else {
    (this) -> shd -> array_data::data = ((char *)(malloc(newsize * sizeof(char ))));
  }
  qt_check_pointer((this) -> shd -> array_data::data == 0,"qgarray.cpp",229);
// no memory
  if (!((this) -> shd -> array_data::data)) {
    return FALSE;
  }
  (this) -> shd -> array_data::len = newsize;
  return TRUE;
}
/*!
  \internal
  Fills the array with the repeated occurrences of \e d, which is
  \e sz bytes long.
  If \e len is specified as different from -1, then the array will be
  resized to \e len*sz before it is filled.
  Returns TRUE if successful, or FALSE if the memory cannot be allocated
  (only when \e len != -1).
  \sa resize()
*/

bool QGArray::fill(const char *d,int len,uint sz)
{
  if (len < 0) {
// default: use array length
    len = ((this) -> shd -> array_data::len / sz);
  }
  else {
    if (!(this) ->  resize (len * sz)) {
      return FALSE;
    }
  }
// 8 bit elements
  if (sz == 1) {
    memset(((this) ->  data ()),( *d),len);
  }
  else {
// 32 bit elements
    if (sz == 4) {
      register Q_INT32 *x = (Q_INT32 *)((this) ->  data ());
      Q_INT32 v =  *((Q_INT32 *)d);
      while((len--))
         *(x++) = v;
// 16 bit elements
    }
    else {
      if (sz == 2) {
        register Q_INT16 *x = (Q_INT16 *)((this) ->  data ());
        Q_INT16 v =  *((Q_INT16 *)d);
        while((len--))
           *(x++) = v;
// any other size elements
      }
      else {
        register char *x = (this) ->  data ();
// more complicated
        while((len--)){
          memcpy(x,d,sz);
          x += sz;
        }
      }
    }
  }
  return TRUE;
}
/*!
  \internal
  Shallow copy. Dereference the current array and references the data
  contained in \e a instead. Returns a reference to this array.
  \sa operator=()
*/

QGArray &QGArray::assign(const class ::QGArray &a)
{
// avoid 'a = a'
  a . shd ->  ref ();
// delete when last reference
  if (((this) -> shd) ->  deref ()) {
// is lost
    if (((this) -> shd -> array_data::data)) {
      free(((char *)((this) -> shd -> array_data::data)));
    }
    (this) ->  deleteData ((this) -> shd);
  }
  (this) -> shd = a . shd;
  return  *(this);
}
/*!
  \internal
  Shallow copy. Dereference the current array and references the
  array data \e d, which contains \e len bytes.
  Returns a reference to this array.
  Do not delete \e d later, because QGArray takes care of that.
*/

QGArray &QGArray::assign(const char *d,uint len)
{
// disconnect this
  if (((this) -> shd) -> QShared::count > 1) {
    ((this) -> shd) -> QShared::count--;
    (this) -> shd = (this) ->  newData ();
    qt_check_pointer((this) -> shd == 0,"qgarray.cpp",310);
  }
  else {
    if (((this) -> shd -> array_data::data)) {
      free(((char *)((this) -> shd -> array_data::data)));
    }
  }
  (this) -> shd -> array_data::data = ((char *)d);
  (this) -> shd -> array_data::len = len;
  return  *(this);
}
/*!
  \internal
  Deep copy. Dereference the current array and obtains a copy of the data
  contained in \e a instead. Returns a reference to this array.
  \sa assign(), operator=()
*/

QGArray &QGArray::duplicate(const class ::QGArray &a)
{
// a.duplicate(a) !
  if (a . shd == (this) -> shd) {
    if (((this) -> shd) -> QShared::count > 1) {
      ((this) -> shd) -> QShared::count--;
      register struct array_data *n = (this) ->  newData ();
      qt_check_pointer(n == 0,"qgarray.cpp",333);
      if ((n -> array_data::len = (this) -> shd -> array_data::len)) {
        n -> array_data::data = ((char *)(malloc((n -> array_data::len) * sizeof(char ))));
        qt_check_pointer(n -> array_data::data == 0,"qgarray.cpp",336);
        if ((n -> array_data::data)) {
          memcpy((n -> array_data::data),((this) -> shd -> array_data::data),(n -> array_data::len));
        }
      }
      else {
        n -> array_data::data = 0;
      }
      (this) -> shd = n;
    }
    return  *(this);
  }
  char *oldptr = 0;
// disconnect this
  if (((this) -> shd) -> QShared::count > 1) {
    ((this) -> shd) -> QShared::count--;
    (this) -> shd = (this) ->  newData ();
    qt_check_pointer((this) -> shd == 0,"qgarray.cpp",350);
// delete after copy was made
  }
  else {
    oldptr = (this) -> shd -> array_data::data;
  }
// duplicate data
  if ((a . shd -> array_data::len)) {
    (this) -> shd -> array_data::data = ((char *)(malloc((a . shd -> array_data::len) * sizeof(char ))));
    qt_check_pointer((this) -> shd -> array_data::data == 0,"qgarray.cpp",356);
    if (((this) -> shd -> array_data::data)) {
      memcpy(((this) -> shd -> array_data::data),(a . shd -> array_data::data),(a . shd -> array_data::len));
    }
  }
  else {
    (this) -> shd -> array_data::data = 0;
  }
  (this) -> shd -> array_data::len = a . shd -> array_data::len;
  if (oldptr) {
    free(((char *)oldptr));
  }
  return  *(this);
}
/*!
  \internal
  Deep copy. Dereferences the current array and obtains a copy of the
  array data \e d instead.  Returns a reference to this array.
  \sa assign(), operator=()
*/

QGArray &QGArray::duplicate(const char *d,uint len)
{
  char *data;
  if (d == 0 || len == 0) {
    data = 0;
    len = 0;
  }
  else {
    if (((this) -> shd) -> QShared::count == 1 && (this) -> shd -> array_data::len == len) {
// use same buffer
      memcpy(((this) -> shd -> array_data::data),d,len);
      return  *(this);
    }
    data = ((char *)(malloc(len * sizeof(char ))));
    qt_check_pointer(data == 0,"qgarray.cpp",387);
    memcpy(data,d,len);
  }
// detach
  if (((this) -> shd) -> QShared::count > 1) {
    ((this) -> shd) -> QShared::count--;
    (this) -> shd = (this) ->  newData ();
    qt_check_pointer((this) -> shd == 0,"qgarray.cpp",393);
// just a single reference
  }
  else {
    if (((this) -> shd -> array_data::data)) {
      free(((char *)((this) -> shd -> array_data::data)));
    }
  }
  (this) -> shd -> array_data::data = data;
  (this) -> shd -> array_data::len = len;
  return  *(this);
}
/*!
  \internal
  Resizes this array to \e len bytes and copies the \e len bytes at
  address \e into it.
  \warning This function disregards the reference count mechanism.  If
  other QGArrays reference the same data as this, all will be updated.
*/

void QGArray::store(const char *d,uint len)
// store, but not deref
{
  (this) ->  resize (len);
  memcpy(((this) -> shd -> array_data::data),d,len);
}
/*!
  \fn array_data *QGArray::sharedBlock() const
  \internal
  Returns a pointer to the shared array block.
  \warning
  Do not use this function.  Using it is begging for trouble.  We dare
  not remove it, for fear of breaking code, but we \e strongly
  discourage new use of it.
*/
/*!
  \fn void QGArray::setSharedBlock( array_data *p )
  \internal
  Sets the shared array block to \e p.
  \warning
  Do not use this function.  Using it is begging for trouble.  We dare
  not remove it, for fear of breaking code, but we \e strongly
  discourage new use of it.
*/
/*!
  \internal
  Sets raw data and returns a reference to the array.
  Dereferences the current array and sets the new array data to \e d and
  the new array size to \e len.	 Do not attempt to resize or re-assign the
  array data when raw data has been set.
  Call resetRawData(d,len) to reset the array.
  Setting raw data is useful because it set QArray data without allocating
  memory or copying data.
  Example of intended use:
  \code
    static uchar bindata[] = { 231, 1, 44, ... };
    QByteArray	a;
    a.setRawData( bindata, sizeof(bindata) );	// a points to bindata
    QDataStream s( a, IO_ReadOnly );		// open on a's data
    s >> <something>;				// read raw bindata
    s.close();
    a.resetRawData( bindata, sizeof(bindata) ); // finished
  \endcode
  Example of misuse (do not do this):
  \code
    static uchar bindata[] = { 231, 1, 44, ... };
    QByteArray	a, b;
    a.setRawData( bindata, sizeof(bindata) );	// a points to bindata
    a.resize( 8 );				// will crash
    b = a;					// will crash
    a[2] = 123;					// might crash
      // forget to resetRawData - will crash
  \endcode
  \warning If you do not call resetRawData(), QGArray will attempt to
  deallocate or reallocate the raw data, which might not be too good.
  Be careful.
*/

QGArray &QGArray::setRawData(const char *d,uint len)
{
// set null data
  (this) ->  duplicate (0,0);
  (this) -> shd -> array_data::data = ((char *)d);
  (this) -> shd -> array_data::len = len;
  return  *(this);
}
/*!
  \internal
  Resets raw data.
  The arguments must be the data and length that were passed to
  setRawData().  This is for consistency checking.
*/

void QGArray::resetRawData(const char *d,uint len)
{
  if (d != ((this) -> shd -> array_data::data) || len != (this) -> shd -> array_data::len) {
#if defined(CHECK_STATE)
    qWarning("QGArray::resetRawData: Inconsistent arguments");
#endif
    return ;
  }
  (this) -> shd -> array_data::data = 0;
  (this) -> shd -> array_data::len = 0;
}
/*!
  \internal
  Finds the first occurrence of \e d in the array from position \e index,
  where \e sz is the size of the \e d element.
  Note that \e index is given in units of \e sz, not bytes.
  This function only compares whole cells, not bytes.
*/

int QGArray::find(const char *d,uint index,uint sz) const
{
  index *= sz;
  if (index >= (this) -> shd -> array_data::len) {
#if defined(CHECK_RANGE)
    qWarning("QGArray::find: Index %d out of range",index / sz);
#endif
    return -1;
  }
  register uint i;
  uint ii;
  switch(sz){
    case 1:
{
// 8 bit elements
{
        register char *x = (this) ->  data () + index;
        char v =  *d;
        for (i = index; i < (this) -> shd -> array_data::len; i++) {
          if (( *(x++)) == v) {
            break; 
          }
        }
        ii = i;
      }
      break; 
    }
    case 2:
{
// 16 bit elements
{
        register Q_INT16 *x = (Q_INT16 *)((this) ->  data () + index);
        Q_INT16 v =  *((Q_INT16 *)d);
        for (i = index; i < (this) -> shd -> array_data::len; i += 2) {
          if (( *(x++)) == v) {
            break; 
          }
        }
        ii = i / 2;
      }
      break; 
    }
    case 4:
{
// 32 bit elements
{
        register Q_INT32 *x = (Q_INT32 *)((this) ->  data () + index);
        Q_INT32 v =  *((Q_INT32 *)d);
        for (i = index; i < (this) -> shd -> array_data::len; i += 4) {
          if ( *(x++) == v) {
            break; 
          }
        }
        ii = i / 4;
      }
      break; 
    }
    default:
{
// any size elements
{
        for (i = index; i < (this) -> shd -> array_data::len; i += sz) {
          if (memcmp(d,(&(this) -> shd -> array_data::data[i]),sz) == 0) {
            break; 
          }
        }
        ii = i / sz;
      }
      break; 
    }
  }
  return i < (this) -> shd -> array_data::len?((int )ii) : -1;
}
/*!
  \internal
  Returns the number of occurrences of \e d in the array, where \e sz is
  the size of the \e d element.
  This function only compares whole cells, not bytes.
*/

int QGArray::contains(const char *d,uint sz) const
{
  register uint i = (this) -> shd -> array_data::len;
  int count = 0;
  switch(sz){
    case 1:
{
// 8 bit elements
{
        register char *x = (this) ->  data ();
        char v =  *d;
        while((i--)){
          if (( *(x++)) == v) {
            count++;
          }
        }
      }
      break; 
    }
    case 2:
{
// 16 bit elements
{
        register Q_INT16 *x = (Q_INT16 *)((this) ->  data ());
        Q_INT16 v =  *((Q_INT16 *)d);
        i /= 2;
        while((i--)){
          if (( *(x++)) == v) {
            count++;
          }
        }
      }
      break; 
    }
    case 4:
{
// 32 bit elements
{
        register Q_INT32 *x = (Q_INT32 *)((this) ->  data ());
        Q_INT32 v =  *((Q_INT32 *)d);
        i /= 4;
        while((i--)){
          if ( *(x++) == v) {
            count++;
          }
        }
      }
      break; 
    }
    default:
{
// any size elements
{
        for (i = 0; i < (this) -> shd -> array_data::len; i += sz) {
          if (memcmp(d,(&(this) -> shd -> array_data::data[i]),sz) == 0) {
            count++;
          }
        }
      }
      break; 
    }
  }
  return count;
}
static int cmp_item_size = 0;
#if defined(Q_C_CALLBACKS)
extern "C" {
#endif

static int cmp_arr(const void *n1,const void *n2)
{
  return n1 && n2?memcmp(n1,n2,cmp_item_size) : ((int )(((intptr_t )n1) - ((intptr_t )n2)));
// Qt 3.0: Add a virtual compareItems() method and call that instead
}
#if defined(Q_C_CALLBACKS)
}
#endif
/*!
  \internal
  Sort the array.
*/

void QGArray::sort(uint sz)
{
  int numItems = ((this) ->  size () / sz);
  if (numItems < 2) {
    return ;
  }
  cmp_item_size = sz;
  qsort(((this) -> shd -> array_data::data),numItems,sz,cmp_arr);
}
/*!
  \internal
  Binary search; assumes sorted array
*/

int QGArray::bsearch(const char *d,uint sz) const
{
  int numItems = ((this) ->  size () / sz);
  if (!numItems) {
    return -1;
  }
  cmp_item_size = sz;
  char *r = (char *)(::bsearch(d,((this) -> shd -> array_data::data),numItems,sz,cmp_arr));
  if (!r) {
    return -1;
  }
  while(r >= (this) -> shd -> array_data::data + sz && cmp_arr((r - sz),d) == 0)
// search to first of equal elements; bsearch is undef 
    r -= sz;
  return (int )((r - (this) -> shd -> array_data::data) / sz);
}
/*!
  \fn char *QGArray::at( uint index ) const
  \internal
  Returns a pointer to the byte at offset \e index in the array.
*/
/*!
  \internal
  Expand the array if necessary, and copies (the first part of) its
  contents from the \e index*zx bytes at \e d.
  Returns TRUE if the operation succeeds, FALSE if it runs out of
  memory.
  \warning This function disregards the reference count mechanism.  If
  other QGArrays reference the same data as this, all will be changed.
*/

bool QGArray::setExpand(uint index,const char *d,uint sz)
{
  index *= sz;
  if (index >= (this) -> shd -> array_data::len) {
// no memory
    if (!(this) ->  resize (index + sz)) {
      return FALSE;
    }
  }
  memcpy(((this) ->  data () + index),d,sz);
  return TRUE;
}
/*!
  \internal
  Prints a warning message if at() or [] is given a bad index.
*/

void QGArray::msg_index(uint index)
{
#if defined(CHECK_RANGE)
  qWarning("QGArray::at: Absolute index %d out of range",index);
#else
#endif
}
/*!
  \internal
  Returns a new shared array block.
*/

QGArray::array_data *QGArray::newData()
{
  return new array_data ();
}
/*!
  \internal
  Deletes the shared array block.
*/

void QGArray::deleteData(struct array_data *p)
{
  delete p;
  p = 0;
}
