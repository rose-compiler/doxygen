/*
 * This code implements the MD5 message-digest algorithm.
 * The algorithm is due to Ron Rivest.  This code was
 * written by Colin Plumb in 1993, no copyright is claimed.
 * This code is in the public domain; do with it what you wish.
 *
 * Equivalent code is available from RSA Data Security, Inc.
 * This code has been tested against that, and is equivalent,
 * except that you don't need to include two pages of legalese
 * with every copy.
 *
 * To compute the message digest of a chunk of bytes, declare an
 * MD5Context structure, pass it to MD5Init, call MD5Update as
 * needed on buffers full of bytes, and then call MD5Final, which
 * will fill a supplied 16-byte array with the digest.
 *
 * Changed so as no longer to depend on Colin Plumb's `usual.h' header
 * definitions; now uses stuff from dpkg's config.h.
 *  - Ian Jackson <ian@chiark.greenend.org.uk>.
 * Still in the public domain.
 */
#include <string.h>		/* for memcpy() */
#include <sys/types.h>		/* for stupid systems */
#include "md5.h"
void MD5Transform(unsigned int buf[4],const unsigned int in[16]);
int g_bigEndian = 0;
int g_endianessDetected = 0;

static void detectEndianess()
{
  int nl = 0x12345678;
  short ns = 0x1234;
  unsigned char *p = (unsigned char *)(&nl);
  unsigned char *sp = (unsigned char *)(&ns);
  if (g_endianessDetected) {
    return ;
  }
  if (
 /* Inside of unparseBinaryOperator(SgAndOp,&&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAndOp,&&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAndOp,&&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[0] == 0x12 && 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[1] == 0x34 && 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[2] == 0x56 && 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[3] == 0x78) {
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
g_bigEndian = 1;
  }
  else {
    if (
 /* Inside of unparseBinaryOperator(SgAndOp,&&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAndOp,&&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAndOp,&&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[0] == 0x78 && 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[1] == 0x56 && 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[2] == 0x34 && 
 /* Inside of unparseBinaryOperator(SgEqualityOp,==,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[3] == 0x12) {
      
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
g_bigEndian = 0;
    }
    else {
      
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
g_bigEndian = 
 /* Inside of unparseBinaryOperator(SgNotEqualOp,!=,SgUnparse_Info) */ 
 *sp != 0x12;
    }
  }
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
g_endianessDetected = 1;
}

static void byteSwap(unsigned int *buf,unsigned int words)
{
  unsigned char *p;
  if (!g_bigEndian) {
    return ;
  }
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
p = ((unsigned char *)buf);
  do {
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 *(buf++) = 
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
((unsigned int )(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
((unsigned int )
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[3]) << 8 | 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[2])) << 16 | (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
((unsigned int )
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[1]) << 8 | 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
p[0]);
    
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
p += 4;
  }while (--words);
}
/*
 * Start MD5 accumulation.  Set bit count to 0 and buffer to mysterious
 * initialization constants.
 */

void MD5Init(struct MD5Context *ctx)
{
  detectEndianess();
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf[0] = 0x67452301;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf[1] = 0xefcdab89;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf[2] = 0x98badcfe;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf[3] = 0x10325476;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[0] = 0;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[1] = 0;
}
/*
 * Update context to reflect the concatenation of another buffer full
 * of bytes.
 */

void MD5Update(struct MD5Context *ctx,const unsigned char *buf,unsigned int len)
{
  unsigned int t;
/* Update byte count */
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
t = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[0];
  if (
 /* Inside of unparseBinaryOperator(SgLessThanOp,<,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[0] = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
t + len) < t) {
/* Carry from low to high */
    
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[1]++;
  }
/* Space available in ctx->in (at least 1) */
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
t = 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
64 - (
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
t & 0x3f);
  if (
 /* Inside of unparseBinaryOperator(SgGreaterThanOp,>,SgUnparse_Info) */ 
t > len) {
    memcpy(
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
((unsigned char *)(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in)) + 64 - t,buf,len);
    return ;
  }
/* First chunk is an odd size */
  memcpy(
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
((unsigned char *)(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in)) + 64 - t,buf,t);
  byteSwap(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in,16);
  MD5Transform(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf,
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in);
  
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
buf += t;
  
 /* Inside of unparseBinaryOperator(SgMinusAssignOp,-=,SgUnparse_Info) */ 
len -= t;
/* Process data in 64-byte chunks */
  while(
 /* Inside of unparseBinaryOperator(SgGreaterOrEqualOp,>=,SgUnparse_Info) */ 
len >= 64){
    memcpy(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in,buf,64);
    byteSwap(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in,16);
    MD5Transform(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf,
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in);
    
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
buf += 64;
    
 /* Inside of unparseBinaryOperator(SgMinusAssignOp,-=,SgUnparse_Info) */ 
len -= 64;
  }
/* Handle any remaining bytes of data. */
  memcpy(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in,buf,len);
}
/*
 * Final wrapup - pad to 64-byte boundary with the bit pattern 
 * 1 0* (64-bit count of bits processed, MSB-first)
 */

void MD5Final(unsigned char digest[16],struct MD5Context *ctx)
{
/* Number of bytes in ctx->in */
  int count = 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[0] & 0x3f;
  unsigned char *p = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
((unsigned char *)(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in)) + count;
/* Set the first char of padding to 0x80.  There is always room. */
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 *(p++) = 0x80;
/* Bytes of padding needed to make 56 bytes (-8..55) */
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
count = 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
56 - 1 - count;
/* Padding forces an extra block */
  if (
 /* Inside of unparseBinaryOperator(SgLessThanOp,<,SgUnparse_Info) */ 
count < 0) {
    memset(p,0,
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
count + 8);
    byteSwap(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in,16);
    MD5Transform(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf,
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in);
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
p = ((unsigned char *)(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in));
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
count = 56;
  }
  memset(p,0,count);
  byteSwap(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in,14);
/* Append length in bits and transform */
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in[14] = 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[0] << 3;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in[15] = 
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[1] << 3 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> bytes[0] >> 29;
  MD5Transform(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf,
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> in);
  byteSwap(
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf,4);
  memcpy(digest,
 /* Inside of unparseBinaryOperator(SgArrowExp,->,SgUnparse_Info) */ 
ctx -> buf,16);
/* In case it's sensitive */
  memset(ctx,0,sizeof(( *ctx)));
}
#ifndef ASM_MD5
/* The four core functions - F1 is optimized somewhat */
/* #define F1(x, y, z) (x & y | ~x & z) */
#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))
/* This is the central step in the MD5 algorithm. */
#define MD5STEP(f,w,x,y,z,in,s) \
	 (w += f(x,y,z) + in, w = (w<<s | w>>(32-s)) + x)
/*
 * The core of the MD5 algorithm, this alters an existing MD5 hash to
 * reflect the addition of 16 longwords of new data.  MD5Update blocks
 * the data and converts bytes into longwords for this routine.
 */

void MD5Transform(unsigned int buf[4],const unsigned int in[16])
{
  register unsigned int a;
  register unsigned int b;
  register unsigned int c;
  register unsigned int d;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[0];
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[1];
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[2];
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[3];
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[0] + 0xd76aa478 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 7 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 7) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[1] + 0xe8c7b756 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 12 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 12) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[2] + 0x242070db , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 17 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 17) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[3] + 0xc1bdceee , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 22 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 22) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[4] + 0xf57c0faf , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 7 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 7) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[5] + 0x4787c62a , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 12 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 12) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[6] + 0xa8304613 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 17 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 17) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[7] + 0xfd469501 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 22 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 22) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[8] + 0x698098d8 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 7 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 7) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[9] + 0x8b44f7af , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 12 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 12) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[10] + 0xffff5bb1 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 17 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 17) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[11] + 0x895cd7be , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 22 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 22) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[12] + 0x6b901122 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 7 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 7) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[13] + 0xfd987193 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 12 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 12) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[14] + 0xa679438e , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 17 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 17) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[15] + 0x49b40821 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 22 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 22) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[1] + 0xf61e2562 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 5 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 5) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[6] + 0xc040b340 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 9 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 9) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[11] + 0x265e5a51 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 14 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 14) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[0] + 0xe9b6c7aa , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 20 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 20) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[5] + 0xd62f105d , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 5 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 5) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[10] + 0x02441453 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 9 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 9) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[15] + 0xd8a1e681 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 14 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 14) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[4] + 0xe7d3fbc8 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 20 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 20) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[9] + 0x21e1cde6 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 5 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 5) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[14] + 0xc33707d6 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 9 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 9) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[3] + 0xf4d50d87 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 14 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 14) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[8] + 0x455a14ed , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 20 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 20) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
d & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[13] + 0xa9e3e905 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 5 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 5) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
c & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[2] + 0xfcefa3f8 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 9 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 9) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
b & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[7] + 0x676f02d9 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 14 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 14) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ 
 /* Inside of unparseBinaryOperator(SgBitAndOp,&,SgUnparse_Info) */ 
a & (
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[12] + 0x8d2a4c8a , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 20 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 20) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c ^ d) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[5] + 0xfffa3942 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 4 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 4) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b ^ c) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[8] + 0x8771f681 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 11 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 11) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a ^ b) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[11] + 0x6d9d6122 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 16 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 16) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d ^ a) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[14] + 0xfde5380c , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 23 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 23) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c ^ d) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[1] + 0xa4beea44 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 4 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 4) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b ^ c) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[4] + 0x4bdecfa9 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 11 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 11) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a ^ b) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[7] + 0xf6bb4b60 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 16 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 16) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d ^ a) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[10] + 0xbebfbc70 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 23 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 23) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c ^ d) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[13] + 0x289b7ec6 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 4 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 4) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b ^ c) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[0] + 0xeaa127fa , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 11 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 11) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a ^ b) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[3] + 0xd4ef3085 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 16 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 16) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d ^ a) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[6] + 0x04881d05 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 23 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 23) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ c ^ d) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[9] + 0xd9d4d039 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 4 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 4) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ b ^ c) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[12] + 0xe6db99e5 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 11 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 11) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ a ^ b) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[15] + 0x1fa27cf8 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 16 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 16) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ d ^ a) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[2] + 0xc4ac5665 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 23 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 23) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
b | ~d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[0] + 0xf4292244 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 6 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 6) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
a | ~c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[7] + 0x432aff97 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 10 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 10) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
d | ~b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[14] + 0xab9423a7 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 15 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 15) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
c | ~a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[5] + 0xfc93a039 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 21 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 21) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
b | ~d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[12] + 0x655b59c3 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 6 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 6) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
a | ~c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[3] + 0x8f0ccc92 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 10 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 10) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
d | ~b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[10] + 0xffeff47d , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 15 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 15) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
c | ~a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[1] + 0x85845dd1 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 21 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 21) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
b | ~d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[8] + 0x6fa87e4f , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 6 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 6) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
a | ~c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[15] + 0xfe2ce6e0 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 10 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 10) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
d | ~b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[6] + 0xa3014314 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 15 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 15) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
c | ~a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[13] + 0x4e0811a1 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 21 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 21) + c);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
a += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
c ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
b | ~d)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[4] + 0xf7537e82 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
a = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
a << 6 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
a >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 6) + b);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
d += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
b ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
a | ~c)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[11] + 0xbd3af235 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
d = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
d << 10 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
d >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 10) + a);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
c += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
a ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
d | ~b)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[2] + 0x2ad7d2bb , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
c = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
c << 15 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
c >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 15) + d);
  (
 /* Inside of unparseBinaryOperator(SgCommaOpExp,,,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
b += 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitXorOp,^,SgUnparse_Info) */ 
d ^ (
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
c | ~a)) + 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
in[9] + 0xeb86d391 , 
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
b = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
(
 /* Inside of unparseBinaryOperator(SgBitOrOp,|,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgLshiftOp,<<,SgUnparse_Info) */ 
b << 21 | 
 /* Inside of unparseBinaryOperator(SgRshiftOp,>>,SgUnparse_Info) */ 
b >> 
 /* Inside of unparseBinaryOperator(SgSubtractOp,-,SgUnparse_Info) */ 
32 - 21) + c);
  
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[0] += a;
  
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[1] += b;
  
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[2] += c;
  
 /* Inside of unparseBinaryOperator(SgPlusAssignOp,+=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
buf[3] += d;
}
#endif

void MD5Buffer(const unsigned char *buf,unsigned int len,unsigned char (sig)[16])
{
  struct MD5Context md5;
  MD5Init(&md5);
  MD5Update(&md5,buf,len);
  MD5Final(sig,&md5);
}
#define HEX_STRING      "0123456789abcdef"      /* to convert to hex */

void MD5SigToString(unsigned char signature[16],char *str,int len)
{
  unsigned char *sig_p;
  char *str_p;
  char *max_p;
  unsigned int high;
  unsigned int low;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
str_p = str;
  
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
max_p = 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
str + len;
  for (
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
sig_p = ((unsigned char *)signature); 
 /* Inside of unparseBinaryOperator(SgLessThanOp,<,SgUnparse_Info) */ 
sig_p < 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
((unsigned char *)signature) + 16; sig_p++) {
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
high = 
 /* Inside of unparseBinaryOperator(SgDivideOp,/,SgUnparse_Info) */ 
 *sig_p / 16;
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
low = 
 /* Inside of unparseBinaryOperator(SgModOp,%,SgUnparse_Info) */ 
 *sig_p % 16;
/* account for 2 chars */
    if (
 /* Inside of unparseBinaryOperator(SgGreaterOrEqualOp,>=,SgUnparse_Info) */ 
 /* Inside of unparseBinaryOperator(SgAddOp,+,SgUnparse_Info) */ 
str_p + 1 >= max_p) {
      break; 
    }
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 *(str_p++) = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
"0123456789abcdef"[high];
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 *(str_p++) = 
 /* Inside of unparseBinaryOperator(SgPntrArrRefExp,[],SgUnparse_Info) */ 
"0123456789abcdef"[low];
  }
/* account for 2 chars */
  if (
 /* Inside of unparseBinaryOperator(SgLessThanOp,<,SgUnparse_Info) */ 
str_p < max_p) {
    
 /* Inside of unparseBinaryOperator(SgAssignOp,=,SgUnparse_Info) */ 
 *(str_p++) = '\0';
  }
}
