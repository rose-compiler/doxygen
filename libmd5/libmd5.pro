#
# This file was generated from libmd5.pro.in on Wed Mar 27 14:41:59 PDT 2013
#

TEMPLATE	= lib
CONFIG		= warn_on staticlib release
HEADERS		= md5.h md5_loc.h
SOURCES		= md5.c
win32:INCLUDEPATH          += .
win32-g++:TMAKE_CFLAGS     += -D__CYGWIN__ -DALL_STATIC
DESTDIR                    =  ../lib
TARGET                     =  md5
OBJECTS_DIR                =  ../objects

    TMAKE_CXXFLAGS += -D_LARGEFILE_SOURCE
